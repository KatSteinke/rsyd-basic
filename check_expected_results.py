#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO: prettier reporting framework!

"""Check if pipeline test run provides expected results."""

__author__ = "Kat Steinke"

import logging
import pathlib
import re
import sys

from argparse import ArgumentParser
from typing import Any, Dict, Optional

import pandas as pd
import yaml

import input_names
import localization_helpers
import pipeline_config
import set_log
import version

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF


# start logging
logger = logging.getLogger("QA_Test")

sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(workflow_config)


# check if qc is correct - compare against local file
def check_qc_results(test_report: pd.DataFrame, true_report: pd.DataFrame,
                     report_language: str = workflow_config["language"]) -> bool:
    """
    Check whether QC report matches what is expected.

    Arguments:
        test_report:        QC report produced by the current test run
        true_report:        Expected QC report
        report_language:    The language of the report (relevant for columns and warnings)

    Returns:
        True if report matches the expected result, False otherwise.

    Raises:
        NotImplementedError: for unsupported languages
    """
    # TODO: nanopore fudge factor?
    _ = localization_helpers.set_language(report_language)
    report_qc_pass = True
    # note version reported
    pipeline_versions = test_report["pipeline_version"].unique()
    logger.info(f"Pipeline version reported as {pipeline_versions}"
                f" - check this is correct before releasing.")
    # don't compare version; only run 1:1 comparison for a subset of columns
    sample_number = _("proevenr")
    columns_to_compare = [_("godkendt"), _("pipeline_noter"),
                          sample_number, _("Bestilt af"), _("Indikation"), "Grampos/neg",
                          _("MADS species"),
                          _("MADS: kendt længde i bp"), "WGS_species_GTDB_basics",
                          _("Serotypning"),
                          _("GTDB: kendt længde i bp"), "WGS_species_kraken", "WGS_genus_kraken"]
    # run all comparisons
    # need to report only differences and sample numbers where these occur
    # check if there are sample numbers that don't match
    samples_not_in_expected = set(test_report[sample_number]) - set(true_report[sample_number])
    if samples_not_in_expected:
        report_qc_pass = False
        logger.warning(f"Unexpected samples {samples_not_in_expected} in QC report")
    samples_missing = set(true_report[sample_number]) - set(test_report[sample_number])
    if samples_missing:
        report_qc_pass = False
        logger.warning(f"Missing samples {samples_missing} from QC report")
    # compare the remaining sample numbers
    samples_both = set(true_report[sample_number]).intersection(set(test_report[sample_number]))
    test_qc_found_samples = test_report.loc[test_report[sample_number].isin(samples_both)]  # type:pd.DataFrame
    true_qc_found_samples = true_report.loc[true_report[sample_number].isin(samples_both)]  # type:pd.DataFrame
    # select only the columns we want to compare
    test_qc_found_samples_subset = test_qc_found_samples.loc[:, columns_to_compare]
    true_qc_found_samples_subset = true_qc_found_samples.loc[:, columns_to_compare]
    # to ensure the sample numbers stick around, index by them
    test_qc_found_samples_subset = test_qc_found_samples_subset.set_index(sample_number)
    true_qc_found_samples_subset = true_qc_found_samples_subset.set_index(sample_number)
    qc_mismatch = true_qc_found_samples_subset.compare(test_qc_found_samples_subset,
                                                       result_names = ("expected",
                                                                       "found"))
    if not qc_mismatch.empty:
        report_qc_pass = False
        logger.warning(f"One or more results do not match. Relevant sample(s):\n"
                       f"{qc_mismatch.to_string()}")
    # some need to have a range - pack both of them in one df to compare directly
    qc_merged = true_qc_found_samples.merge(test_qc_found_samples, on=sample_number,
                                            suffixes = ("_expected", "_found"))
    # assembly quality: higher N50 and coverage is better - TODO: make this easier and just compare in range?
    run_has_better_assemblies = False
    run_has_worse_assemblies = False
    assembly_metrics = ["N50_basics", "Avg. coverage depth_basics"]
    for assembly_metric in assembly_metrics:
        # drop NA values where both are NA
        qc_no_na = qc_merged.dropna(subset = [f"{assembly_metric}_found",
                                              f"{assembly_metric}_expected"],
                                    how="all")
        metric_match = (qc_no_na[f"{assembly_metric}_found"]
                        > (qc_no_na[f"{assembly_metric}_expected"] * 0.99))
        if not metric_match.all():
            run_has_worse_assemblies = True
            report_qc_pass = False
        else:
            better_assemblies = (qc_merged[f"{assembly_metric}_found"]
                                 > (qc_merged[f"{assembly_metric}_expected"] * 1.01))
            if better_assemblies.any():
                run_has_better_assemblies = True
    test_assembly_params = test_qc_found_samples[[sample_number, "N50_basics",
                                                  "Avg. coverage depth_basics",
                                                  "Total length_basics", "# contigs",
                                                  "# predicted genes (unique)"]]  # type:pd.DataFrame
    true_assembly_params = true_qc_found_samples[[sample_number, "N50_basics",
                                                  "Avg. coverage depth_basics",
                                                  "Total length_basics", "# contigs",
                                                  "# predicted genes (unique)"]]  # type:pd.DataFrame
    test_assembly_params = test_assembly_params.set_index(sample_number)
    true_assembly_params = true_assembly_params.set_index(sample_number)
    compare_assembly_params = true_assembly_params.compare(test_assembly_params,
                                                           result_names = ("expected", "found"))
    if run_has_better_assemblies:
        logger.info(f"Assembly quality may have improved compared to the baseline. "
                    f"Relevant sample(s):\n"
                    f"{compare_assembly_params.to_string()}")
    if run_has_worse_assemblies:
        logger.warning(f"Assembly quality may have decreased compared to the baseline. "
                       f"Relevant sample(s):\n"
                       f"{compare_assembly_params.to_string()}")
    # fewer contigs are better if the genome is better, worse if not
    # size may vary slightly in an already good genome, drastically for one that somehow got better
    # slight variations: allow 1% difference - TODO: logically this should only apply to the improved ones
    # TODO is this just overcomplicating everything?
    if run_has_better_assemblies:
        size_match = ((qc_merged["Total length_basics_found"]
                       > (qc_merged["Total length_basics_expected"] * 0.99)))
    else:
        size_match = ((qc_merged["Total length_basics_found"]
                       > (qc_merged["Total length_basics_expected"] * 0.99)) &
                      (qc_merged["Total length_basics_found"]
                       < (qc_merged["Total length_basics_expected"] * 1.01)))
    if not size_match.all().all():
        logger.warning(f"Assembly size does not match for one or more samples:\n"
                       f"{compare_assembly_params.to_string()}")
        report_qc_pass = False
    # amount of predicted genes?

    # completeness/contamination: higher completeness/lower contamination: better
    test_completeness_contamination = test_qc_found_samples[[sample_number, "Completeness",
                                                             "Contamination"]]
    true_completeness_contamination = true_qc_found_samples[[sample_number, "Completeness",
                                                             "Contamination"]]
    checkm_merged = true_completeness_contamination.merge(test_completeness_contamination,
                                                          on = sample_number,
                                                          suffixes = ("_expected", "_found"))
    # compare in order to report
    test_completeness_contamination = test_completeness_contamination.set_index(sample_number)
    true_completeness_contamination = true_completeness_contamination.set_index(sample_number)
    checkm_mismatch = true_completeness_contamination.compare(test_completeness_contamination,
                                                              result_names = ("expected",
                                                                              "found"))
    # remove NA values
    checkm_merged = checkm_merged.dropna(subset=["Completeness_found", "Completeness_expected"],
                                         how="all")
    checkm_merged = checkm_merged.dropna(subset = ["Contamination_found", "Contamination_expected"],
                                         how="all")
    # completeness and contamination are both in a range of 0-100
    # completeness has lower impact on read acceptance than contamination
    completeness_match = (checkm_merged["Completeness_found"]
                          >= (checkm_merged["Completeness_expected"] - 1)).all().all()
    if not completeness_match:
        logger.warning(f"Completeness/contamination may have worsened compared to the baseline. "
                       f"Relevant sample(s):\n"
                       f"{checkm_mismatch.to_string()}")
        report_qc_pass = False

    contamination_match = (checkm_merged["Contamination_found"]
                           <= (checkm_merged["Contamination_expected"] + 0.5)).all().all()
    if not contamination_match:
        logger.warning(f"Completeness/contamination may have worsened compared to the baseline. "
                       f"Relevant sample(s):\n"
                       f"{checkm_mismatch.to_string()}")
        report_qc_pass = False

    return report_qc_pass


# check serotyping has been run for coli and salmonella
# TODO: right now this is where our localization systems intersect in the ugliest way
# TODO finally move setting languague out to helpers? or a localization module?
def check_serotyping(run_result_dir: pathlib.Path, results: pd.DataFrame,
                     bacteria_categories: pd.DataFrame,
                     report_language: str = workflow_config["language"],
                     bacteria_names: input_names.BacteriaMappingNames = bact_names) -> bool:
    """
    Check if serotyping has been performed on all relevant samples.

    Arguments:
        run_result_dir:         Path to the directory containing all results for the run
        results:                QC report produced by the current run
        bacteria_categories:    Translation of internal bacteria names or numeric codes to species
        report_language:        The language of the report (relevant for columns and warnings)
        bacteria_names:         Column names in the code -> species name mapping

    Returns:
        True if serotyping steps have been performed on all samples where this should be done;
        False otherwise.

    Raises:
        NotImplementedError: for unsupported languages
    """
    _ = localization_helpers.set_language(report_language)
    serotyping_qc = True
    salmonella_samples = results[results[_("MADS species")].str.startswith("Salmonella")][
        _("proevenr")].tolist()
    # E. coli can be called a lot of different things internally - use the existing mapping
    results_with_translations = results.merge(bacteria_categories, how="left",
                                              left_on=_("MADS species"),
                                              right_on=bacteria_names.internal_bacteria_text)
    coli_samples = results_with_translations[results_with_translations[bacteria_names.bacteria_category]
                                             == "Escherichia coli"][_("proevenr")].tolist()
    if salmonella_samples:
        missing_salmo = []
        for salmonella_sample in salmonella_samples:
            if not (run_result_dir / salmonella_sample / "typing" / "SeqSero"
                    / "SeqSero_result.tsv").exists():
                missing_salmo.append(salmonella_sample)
        if missing_salmo:
            serotyping_qc = False
            logger.warning(f"Missing SeqSero results for sample(s) {missing_salmo}")
    if coli_samples:
        missing_coli = []
        for coli_sample in coli_samples:
            if not (run_result_dir / coli_sample / "typing" / "serotypefinder"
                    / "results_tab.tsv").exists():
                missing_coli.append(coli_sample)
        if missing_coli:
            serotyping_qc = False
            logger.warning(f"Missing SerotypeFinder results for sample(s) {missing_coli}")

    return serotyping_qc


# check result file
def check_mads_results(test_mads: pd.DataFrame, true_mads: pd.DataFrame,
                       report_language: str = workflow_config["language"]) -> bool:
    """
    Check whether the result file contains the expected results.

    Arguments:
        test_mads:          Report for entry into the LIS system produced by the current run
        true_mads:          Expected report for entry into the LIS system
        report_language:    The language of the report (relevant for columns)

    Returns:
        True if the expected results are present, False otherwise.

    Raises:
        NotImplementedError: for unsupported languages
    """
    _ = localization_helpers.set_language(report_language)
    mads_qc_pass = True
    samples_not_in_expected = set(test_mads.loc[_("proevenr")]) - set(true_mads.loc[_("proevenr")])
    if samples_not_in_expected:
        mads_qc_pass = False
        logger.warning(f"Unexpected samples {samples_not_in_expected} in report for LIS")
    samples_missing = set(true_mads.loc[_("proevenr")]) - set(test_mads.loc[_("proevenr")])
    if samples_missing:
        mads_qc_pass = False
        logger.warning(f"Missing samples {samples_missing} from report for LIS")
    # compare the remaining sample numbers
    samples_both = set(true_mads.loc[_("proevenr")]).intersection(set(test_mads.loc[_("proevenr")]))
    test_mads_found_samples = test_mads.loc[:,
                                            test_mads.loc[_("proevenr")].isin(samples_both)]  # type:pd.DataFrame
    true_mads_found_samples = true_mads.loc[:,
                                            true_mads.loc[_("proevenr")].isin(samples_both)]  # type:pd.DataFrame
    test_mads_with_samplenumber = test_mads_found_samples.rename(columns = test_mads_found_samples.loc[_("proevenr")])
    true_mads_with_samplenumber = true_mads_found_samples.rename(columns = true_mads_found_samples.loc[_("proevenr")])
    # filter out all rows ending with a digit
    test_mads_constant_categories = test_mads_with_samplenumber.loc[~test_mads_with_samplenumber.index.str.contains(r"_\d+$")]
    true_mads_constant_categories = true_mads_with_samplenumber.loc[~true_mads_with_samplenumber.index.str.contains(r"_\d+$")]
    # ...and compare them separately
    test_mads_var_categories = test_mads_with_samplenumber.loc[
        test_mads_with_samplenumber.index.str.contains(r"_\d+$")]
    true_mads_var_categories = true_mads_with_samplenumber.loc[
        true_mads_with_samplenumber.index.str.contains(r"_\d+$")]
    # split them up by category
    test_mads_category_names = {re.search(r"O_WGS [a-zA-ZæøåÆØÅ./ -]+",
                                          category_name).group()
                                for category_name in test_mads_var_categories.index
                                if re.search(r"O_WGS [a-zA-ZæøåÆØÅ./ -]+",
                                             category_name)}
    true_mads_category_names = {re.search(r"O_WGS [a-zA-ZæøåÆØÅ./ -]+",
                                          category_name).group()
                                for category_name in true_mads_var_categories.index
                                if re.search(r"O_WGS [a-zA-ZæøåÆØÅ./ -]+",
                                             category_name)}
    # check if any of them are missing
    missing_in_test = true_mads_category_names - test_mads_category_names
    if missing_in_test:
        mads_qc_pass = False
        logger.warning(f"Missing categories {missing_in_test} from report for LIS.")
    missing_in_lis = test_mads_category_names - true_mads_category_names
    if missing_in_lis:
        mads_qc_pass = False
        logger.warning(f"Unexpected categories {missing_in_lis} in report for LIS.")
    # for all that are present:
    categories_in_both = true_mads_category_names.intersection(test_mads_category_names)
    for category in categories_in_both:
        # concatenate in alphabetic order per category - TODO: move this out?
        test_mads_category = test_mads_var_categories[test_mads_var_categories.index.str.contains(category)]
        # for plasmid ID we may have several plasmids in one row, so we need to split these
        # back into the original components
        test_mads_category = test_mads_category.apply(lambda column:
                                                      ";".join(sorted({sub_item
                                                                       for item in column
                                                                       if pd.notna(item)
                                                                       for sub_item in
                                                                       item.split(";")
                                                                       }, key=str.casefold
                                                                      ))).to_frame().T
        test_mads_category.index = [category]
        true_mads_category = true_mads_var_categories[
            true_mads_var_categories.index.str.contains(category)]
        true_mads_category = true_mads_category.apply(lambda column:
                                                      ";".join(sorted({sub_item
                                                                       for item in column
                                                                       if pd.notna(item)
                                                                       for sub_item in
                                                                       item.split(";")
                                                                       }, key=str.casefold
                                                                      ))).to_frame().T

        true_mads_category.index = [category]
        var_category_mismatch = true_mads_category.compare(test_mads_category,
                                                           result_names = ("expected", "found"))
        if not var_category_mismatch.empty:
            mads_qc_pass = False
            logger.warning(f"One or more analysis results do not match. Relevant sample(s):\n"
                           f"{var_category_mismatch.to_string()}")
    mads_mismatch = true_mads_constant_categories.compare(test_mads_constant_categories,
                                                          result_names = ("expected", "found"))
    if not mads_mismatch.empty:
        mads_qc_pass = False
        logger.warning(f"One or more analysis results do not match. Relevant sample(s):\n"
                       f"{mads_mismatch.to_string()}")

    return mads_qc_pass


def check_run_dir(run_dir: pathlib.Path, expected_dir: pathlib.Path,
                  active_config: Dict[str, Any] = workflow_config,
                  report_language: Optional[str] = None,
                  bacteria_names: input_names.BacteriaMappingNames = bact_names) \
        -> bool:
    """
    Run all checks on the result directory of a run.

    Arguments:
        run_dir:                Directory containing the current run's results
        expected_dir:           Directory containing expected run results
        active_config:          the config to use
        report_language:        The language of the report (relevant for columns and warnings)
        bacteria_names:         Column names in the code -> species name mapping

    Returns:
        True if everything matches, False otherwise

    Raises:
        FileNotFoundException:  if the current or expected result directories are missing
    """
    if not report_language:
        report_language = active_config["language"]
    _ = localization_helpers.set_language(report_language)
    # get the required settings from the config
    bacteria_categories = active_config["lab_info_system"]["bacteria_codes"]
    sample_format = active_config["sample_number_settings"]["sample_number_format"]
    seq_mode = active_config["sequencing_mode"]
    # TODO: better way to log pass/fail?
    qc_passes = []

    # check if the directories are there
    if not run_dir.exists():
        raise FileNotFoundError("Could not find result directory for current run.")

    if not expected_dir.exists():
        raise FileNotFoundError("Could not find directory with expected results.")

    # do we have everything we need?
    required_files = {"qc_reports": {"QC report": "raw_results/*_all_results.tsv"},
                      "analysis_reports": {"LIS report": "raw_results/*_mads_wide.tsv"},
                      "files_check_presence_only": {"CheckM report":
                                                        "checkm_all_samples/checkm-results.txt",
                                                    "Kraken report": "kraken_all/*_report.tsv",
                                                    "QUAST report":
                                                        "quast_all/"
                                                        f"transposed_report_{seq_mode}.tsv",
                                                    "Read stats":
                                                        "read_stats/all_quast_coverage.tsv"}}
    if seq_mode == "illumina":
        required_files["NG50 results"] = "quast_all/ng50_illumina.tsv"

    # TODO: check for each sample?

    # find results
    test_qc_result_hits = list(run_dir.glob(required_files["qc_reports"]["QC report"]))
    true_qc_result_hits = list(expected_dir.glob(required_files["qc_reports"]["QC report"]))

    if not test_qc_result_hits or not true_qc_result_hits:
        missing_dirs = [str(checked_dir) for checked_dir in [run_dir, expected_dir]
                        if not list(checked_dir.glob(required_files["qc_reports"]["QC report"]))]
        logger.warning(f"Missing QC report from {missing_dirs}. \n"
                       f"Skipping comparison of QC reports.")
        report_qc_pass = False
    else:
        test_qc_result = test_qc_result_hits[0]
        true_qc_result = true_qc_result_hits[0]
        test_qc_data = pd.read_csv(test_qc_result, skiprows = [0], sep="\t",
                                   dtype = {"N50_basics": "Int64",  # has to handle NA
                                            _("MADS: kendt længde i bp"): float,
                                            _("GTDB: kendt længde i bp"): float,
                                            "Total length_basics": "Int64",
                                            "# contigs": "Int64",
                                            "# predicted genes (unique)": "Int64"})
        true_qc_data = pd.read_csv(true_qc_result, skiprows = [0], sep="\t",
                                   dtype = {"N50_basics": "Int64",  # has to handle NA
                                            _("MADS: kendt længde i bp"): float,
                                            _("GTDB: kendt længde i bp"): float,
                                            "Total length_basics": "Int64",
                                            "# contigs": "Int64",
                                            "# predicted genes (unique)": "Int64"})
        report_qc_pass = check_qc_results(test_qc_data, true_qc_data,
                                          report_language = report_language)
    qc_passes.append(report_qc_pass)
    # check if there are result dirs for all samples - base on expected dir?
    # TODO: move this out?
    expected_sample_dirs = {sample_dir.name for sample_dir in expected_dir.iterdir()
                            if sample_dir.is_dir()
                            and re.match(sample_format, sample_dir.name)}
    found_sample_dirs = {sample_dir.name for sample_dir in run_dir.iterdir()
                         if sample_dir.is_dir()
                         and re.match(sample_format, sample_dir.name)}
    if not expected_sample_dirs:
        raise FileNotFoundError("No sample directories found in directory with expected results.")

    if not found_sample_dirs:
        raise FileNotFoundError("No sample directories found in current run dir.")

    samples_not_in_expected = found_sample_dirs - expected_sample_dirs
    sample_dir_pass = True
    if samples_not_in_expected:
        sample_dir_pass = False
        logger.warning(f"Unexpected samples {samples_not_in_expected} in result directory")
    samples_missing = expected_sample_dirs - found_sample_dirs
    if samples_missing:
        sample_dir_pass = False
        logger.warning(f"Missing samples {samples_missing} from result directory")
    qc_passes.append(sample_dir_pass)
    # check the analysis results
    test_lis_result_hits = list(run_dir.glob(required_files["analysis_reports"]["LIS report"]))
    true_lis_result_hits = list(expected_dir.glob(required_files["analysis_reports"]["LIS report"]))

    if not test_lis_result_hits or not true_lis_result_hits:
        missing_dirs = [str(checked_dir) for checked_dir in [run_dir, expected_dir]
                        if not list(checked_dir.glob(required_files["analysis_reports"]["LIS report"]))]
        logger.warning(f"Missing report for LIS from {missing_dirs}. \n"
                       f"Skipping comparison of LIS reports.")
        lis_qc_pass = False
    else:
        test_lis_result = test_lis_result_hits[0]
        true_lis_result = true_lis_result_hits[0]
        test_lis_data = pd.read_csv(test_lis_result, sep="\t", index_col = 0)
        true_lis_data = pd.read_csv(true_lis_result, sep="\t", index_col = 0)
        lis_qc_pass = check_mads_results(test_lis_data, true_lis_data,
                                         report_language = report_language)
    qc_passes.append(lis_qc_pass)
    # check serotyping
    if not test_qc_result_hits:
        logger.warning("Missing QC report from current run dir. \n"
                       "Cannot infer species for serotyping; skipping.")
        serotyping_qc = False
    else:
        test_qc_result = test_qc_result_hits[0]
        test_qc_data = pd.read_csv(test_qc_result, skiprows = [0], sep="\t")
        category_data = pd.read_csv(bacteria_categories, sep = ";", encoding="latin-1")
        serotyping_qc = check_serotyping(run_dir, test_qc_data, category_data,
                                         report_language=report_language,
                                         bacteria_names=bacteria_names)
    qc_passes.append(serotyping_qc)
    # check remaining results are there
    missing_results = []
    for result_name, result_path in required_files["files_check_presence_only"].items():
        if not list(run_dir.glob(result_path)):
            missing_results.append(result_name)
    if missing_results:
        missing_results_pretty = "\n".join(missing_results)
        logger.warning(f"The following file(s) were missing from the results:\n"
                       f"{missing_results_pretty}")
        result_file_pass = False
    else:
        result_file_pass = True
    qc_passes.append(result_file_pass)
    if not all(qc_passes):
        logger.error("One or more QC steps failed. Check log for details.")
    else:
        logger.info("All QC steps passed")
    return all(qc_passes)


if __name__ == "__main__":
    logger = set_log.get_stream_log()
    arg_parser = ArgumentParser(description = "Check whether results of a test run match "
                                              "expected results")
    arg_parser.add_argument("result_dir", help="Directory containing test run results to evaluate")
    arg_parser.add_argument("expected_results", help="Expected result directory")
    arg_parser.add_argument("-l", "--logfile", help="File to write log to "
                                                    "(default: logs/pipeline_qa.log in result dir)",
                            default = None)
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script,"
                                   " can be overridden by commandline options)")
    args = arg_parser.parse_args()
    result_dir = pathlib.Path(args.result_dir)
    expected_results = pathlib.Path(args.expected_results)
    if args.logfile:
        logfile_path = pathlib.Path(args.logfile)
    else:
        logfile_path = result_dir / "logs" / "pipeline_qa.log"
    # log to file
    log_file = logging.FileHandler(logfile_path)
    log_file.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_file.setFormatter(logfile_formatter)
    logger.addHandler(log_file)

    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        (sheet_names,
         lis_names,
         bact_names) = localization_helpers.load_input_from_config(workflow_config)

    all_qc_passes = check_run_dir(result_dir, expected_results, workflow_config,
                                  bacteria_names = bact_names)

    # exit with fail if something fails
    if not all_qc_passes:
        sys.exit(1)
    sys.exit(0)
