"""Identify subsets of samples for specific analyses."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from typing import List, NamedTuple, Union

import pandas as pd
# import NAType, ugly but necessary to hint pd.NA; https://stackoverflow.com/a/74093834/15704972
from pandas._libs.missing import NAType

import input_names
import version


__version__ = version.__version__


class SamplesBySequencer(NamedTuple):
    """Samples by sequencing type used."""
    illumina: List[Union[str, NAType]]
    nanopore: List[Union[str, NAType]]
    hybrid: List[Union[str, NAType]]
    all_illumina: List[Union[str, NAType]]
    all_nanopore: List[Union[str, NAType]]


class SampleByIndication:
    """A sample with sample number, species, and indications.

    Attributes:
        sample_number:  the sample's sample number with year and isolate number
        species:        the sample's species as identified by lab methods
        resistance:     whether resistance genes should be identified (indication or
                        inferred from species)
        identification: whether the sample is being sequenced for identification
        relapse:        whether the sample is used to investigate a relapse
        toxin:          whether toxin genes should be identified
        hai:            whether the sample is a suspected healthcare-associated infection
        environmental:  whether the sample is an environmental sample
        outbreak:       whether the sample is suspected to be part of an outbreak
        surveillance:   whether the sample was taken for surveillance
    """

    def __init__(self, sample_number: str, species: str,
                 resistance: bool = False,
                 identification: bool = False,
                 relapse: bool = False, toxin: bool = False, healthcare_associated: bool = False,
                 environmental: bool = False,
                 outbreak: bool = False, surveillance: bool = False):
        """
        Arguments:
            sample_number:          the sample's sample number with year and isolate number
            species:                the sample's species as identified by lab methods
            resistance:             whether resistance genes should be identified
            identification:         whether the sample is being sequenced for identification
            relapse:                whether the sample is used to investigate a relapse
            toxin:                  whether toxin genes should be identified
            healthcare_associated:  whether the sample is a suspected healthcare-associated
                                    infection
            environmental:          whether the sample is an environmental sample
            outbreak:               whether the sample is suspected to be part of an outbreak
            surveillance:           whether the sample was taken for surveillance
        """
        self.sample_number = sample_number
        self.species = species
        self.resistance = resistance
        self.identification = identification
        self.relapse = relapse
        self.toxin = toxin
        self.hai = healthcare_associated
        self.environmental = environmental
        self.outbreak = outbreak
        self.surveillance = surveillance


def get_samples_by_sequencer(sample_overview: pd.DataFrame) -> SamplesBySequencer:
    """Identify Illumina, Nanopore and hybrid samples in a given sample sheet.
    Arguments:
         sample_overview:   An overview over samples containing the sample name as "sample", the
                            run type (paired-end, ont or hybrid) as "runtype", and paths to reads as
                            "r1" and "r2" for paired-end reads, "extra" for nanopore reads.
    Returns:
        All samples exclusively sequenced with Illumina and Nanopore; all hybrid samples; and
        all samples for which there are Illumina and Nanopore reads (including hybrid samples).
    """
    illumina_samples = sample_overview.query("runtype == 'paired-end'")["sample"].tolist()
    hybrid_samples = sample_overview.query("runtype == 'hybrid'")["sample"].tolist()
    nanopore_samples = sample_overview.query("runtype == 'ont'")["sample"].tolist()
    # Nanopore reads: "extra" has to contain the path to the barcode directory
    all_nanopore_samples = sample_overview.loc[sample_overview["extra"].notna(), "sample"].tolist()
    all_illumina_samples = sample_overview.loc[sample_overview["r1"].notna(), "sample"].tolist()
    samples_by_seq = SamplesBySequencer(illumina = illumina_samples, hybrid = hybrid_samples,
                                        nanopore = nanopore_samples,
                                        all_illumina = all_illumina_samples,
                                        all_nanopore = all_nanopore_samples)
    return samples_by_seq


def get_samples_by_bact_id(sample_overview: pd.DataFrame, lis_report: pd.DataFrame,
                           organism_ids: List[str],
                           lis_data_names: input_names.LISDataNames) -> List[str]:
    """Identify all samples in a current run which have been identified as a certain organism by
    wet lab methods according to the laboratory information system, using a numeric code to identify
    the organism in question.
    Arguments:
        sample_overview:    An overview over samples containing the sample name as "sample"
        lis_report:         the report from the laboratory information system with sample numbers
                            already translated to the format used in the sample overview
        organism_ids:       the identifier(s) for the species to be identified
        lis_data_names:     Column names for LIS data
    Returns:
         All samples in the current run which have been identified as the requested organism.
    """
    # get all samples from the run that are in the report - controls aren't relevant here
    samples_in_run = sample_overview.merge(lis_report, left_on=["sample"], right_on=["proevenr"],
                                           how="inner")
    samples_for_organism = samples_in_run.loc[
        samples_in_run[lis_data_names.bacteria_code].isin(organism_ids),
        "sample"].tolist()
    return samples_for_organism

