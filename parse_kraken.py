"""Extract highest-scoring genus and species from Kraken output and summarize for all samples."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pathlib
import re

from argparse import ArgumentParser
from typing import List

import pandas as pd
import yaml

import pipeline_config
import version

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

__version__ = version.__version__


def parse_kraken_report(report_file: pathlib.Path) -> pd.DataFrame:
    """Parse Kraken2's standard report to remove taxonomic level indentation and add explanatory
    headers.

    Arguments:
        report_file:    Path to Kraken report (*_report.tsv)

    Returns:
        The Kraken report as a plain DataFrame.
    """
    # read Kraken report: set header, split *only* on tab
    kraken_data = pd.read_csv(report_file, sep="\t", header=None, names=["percent_reads_covered",
                                                                         "number_reads_covered",
                                                                         "number_reads_exactly_in_tax",
                                                                         "taxonomic_level",
                                                                         "NCBI_rank_ID",
                                                                         "taxon_name"],
                              dtype = {"NCBI_rank_ID": str, "percent_reads_covered": float})
    kraken_data["taxon_name"] = kraken_data["taxon_name"].str.strip()
    return kraken_data


# for all taxon names, strip out surrounding whitespace
def find_best_match_in_kraken(kraken_report: pd.DataFrame, taxo_level: str) -> pd.DataFrame:
    """Find the taxon which has the highest amount of matching reads for a sample for a given
    taxonomic level.

    Arguments:
        kraken_report:  Kraken's report output as a plain DataFrame
        taxo_level:     Taxonomic level for which to find the best matching taxon (U, R, D, K, P,
                        C, O, F, G, S) optionally followed by a number for a sublevel

    Returns:
        Kraken report for the taxon with the highest amount of matching reads for the given
        taxonomic level.
    """
    # check if we have a legit taxonomic level
    taxo_level_format = r"^[URDKPCOFGS]\d?$"
    if not re.match(taxo_level_format, taxo_level):
        raise ValueError(f"Taxonomic level {taxo_level} is not a valid level. "
                         f"Levels must be specified as U, R, D, K, P, C, O, F, G or S, optionally"
                         f"followed by a number for a sublevel.")
    # filter by taxonomic level
    hits_for_taxo_level = kraken_report.query("taxonomic_level == @taxo_level")
    if hits_for_taxo_level.empty:
        dummy_frame = pd.DataFrame(data = {"percent_reads_covered": [pd.NA],
                                           "number_reads_covered": [pd.NA],
                                           "number_reads_exactly_in_tax": [pd.NA],
                                           "taxonomic_level": [pd.NA],
                                           "NCBI_rank_ID": [pd.NA],
                                           "taxon_name": [pd.NA]})
        return dummy_frame
    # get index of the hit with the highest percentage of reads and return the corresponding row
    top_hit_for_taxo_level = hits_for_taxo_level.loc[[hits_for_taxo_level["percent_reads_covered"].idxmax()]]
    top_hit_for_taxo_level = top_hit_for_taxo_level.reset_index(drop = True)
    return top_hit_for_taxo_level


def make_kraken_overview(kraken_reports: List[pathlib.Path], sample_number_format: str) \
        -> pd.DataFrame:
    """Combine individual Kraken reports into a single overview giving the genus and species with
    the highest percentage of matching reads for each sample. The sample number is inferred from
    the file name.

    Arguments:
         kraken_reports:        Kraken report.tsv files to combine
         sample_number_format:  Format of valid sample numbers as a regex

    Returns:
         The genus and species with the highest percentage of matching reads for each sample, as
         well as the percentage of reads matching this.

    Raises:
         ValueError:  if the sample number of a sample can't be inferred from the result file's name
    """
    best_matches = []
    for kraken_report in kraken_reports:
        report_data = parse_kraken_report(kraken_report)
        best_genus = find_best_match_in_kraken(report_data, taxo_level = "G")
        best_species = find_best_match_in_kraken(report_data, taxo_level = "S")
        best_match = pd.DataFrame()
        best_match["WGS_species_kraken"] = best_species["taxon_name"]
        best_match["WGS_species_kraken_%"] = best_species["percent_reads_covered"]
        best_match["WGS_genus_kraken"] = best_genus["taxon_name"]
        best_match["WGS_genus_kraken_%"] = best_genus["percent_reads_covered"]
        # extract sample number from the file name - TODO: can we get at this more nicely?
        sample_number_in_name = re.search(f"(?P<sample_number>{sample_number_format})_report.tsv",
                                          kraken_report.name)
        # TODO: gather errors instead?
        if not sample_number_in_name:
            raise ValueError(f"Sample number could not be inferred from name for "
                             f"{str(kraken_report)}")
        sample_number = sample_number_in_name.group("sample_number")
        best_match["proevenr"] = sample_number
        best_matches.append(best_match)
    best_matches_for_all = pd.concat(best_matches).reset_index(drop = True)
    return best_matches_for_all


if __name__ == "__main__":
    arg_parser = ArgumentParser(description = "Identify highest-scoring genus and species for a "
                                              "number of samples.")

    arg_parser.add_argument("kraken_reports",
                            help="Paths to Kraken reports for samples to summarize", nargs="+")
    arg_parser.add_argument("--outfile", help="Path to output file"
                                              " (default: all_kraken_report.tsv)",
                            default="all_kraken_report.tsv")
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script, "
                                   "can be overridden by commandline options)")
    args = arg_parser.parse_args()
    outfile = pathlib.Path(args.outfile)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file).resolve()
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
    # TODO: better checks for file absence/presence?
    all_kraken_reports = [pathlib.Path(kraken_report) for kraken_report in args.kraken_reports]
    sample_number_regex = workflow_config["sample_number_settings"]["sample_number_format"]
    overview_for_all = make_kraken_overview(all_kraken_reports,
                                            sample_number_format = sample_number_regex)
    overview_for_all.to_csv(outfile, sep = "\t", index = False)
