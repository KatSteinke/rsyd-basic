��    +      t  ;   �      �     �     �     �     �  
   �  	   �  ?   �     8  	   A     K  
   d     o  
   ~  C   �  	   �     �     �     �  0        A     W     m  "   �     �     �     �     �       V        v     �  	   �  
   �     �     �     �     �  	   �     �     �     �     �           
             /     5     B  A   Y     �     �     �     �     �  
   �  3   �     -	     5	     A	     Y	  2   o	     �	     �	     �	  #   �	     
     (
     @
     Z
     s
  Q   �
     �
     �
  
   �
       
        $     2     C     U     q     w     �     �                           %   *   !             "      )         $   
                        (                                        &            +   	          '                           #                  og  ANI_til_ref ANI_til_ref_basics Andet Bestilt af Brønd_nr Coverage er lavere end grænsen for coverage ({min_coverage}).  DNA_Konc Forskning GTDB: kendt længde i bp ILM_Brønd Identifikation Indikation Ingen data for {fail_samples} - assembly'et er sandsynligvis failet Kommentar MADS species MADS: kendt længde i bp Miljøprøveisolat N50 er lavere end grænsen for N50 ({min_n50}).  O_WGS Serotype/gruppe O_WGS Toxinpåvisning O_WGS identifikation O_WGS res.gen anden beta-lactamase O_WGS res.gen carbapenemase O_WGS res.gen colistin O_WGS res.gen vancomycin O_WGS supplerende type Oprensnings_kørselsnr Over 20% forskel mellem forventet genomstørrelse fra MADS og fundet genomstørrelse.  Overvågningsisolat Relaps Resistens SEK_Run_nr Serotypning Toxingen_ID Udbrudsisolat godkendt køres om noter pipeline_noter proevenr res.gen Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: 2024-07-10 13:12+0200
Last-Translator: Kat Steinke
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
  and  ANI_to_ref ANI_to_ref_basics Other Requested_by Extraction_well_number Coverage is lower than the cutoff for coverage ({min_coverage}).  DNA_concentration Research GTDB: known length in bp ILM_well_number Identification Indication No data for {fail_samples} - assembly likely failed Comment LIS species LIS: known length in bp Environmental isolate N50 is lower than the cutoff for N50 ({min_n50}).  O_WGS Serotype/group O_WGS toxin identification O_WGS identification O_WGS res.gene other beta-lactamase O_WGS res.gene carbapenemase O_WGS res.gene colistin O_WGS res.gene vancomycin O_WGS supplementary type Extraction_run_number Over 20% difference between expected genome size from LIS and genome size found.  Surveillance isolate Relapse Resistance Sequencing_run_number Serotyping Toxin gene ID Outbreak isolate sequence approved should sequencing be redone notes pipeline_notes sample_number res.gene 