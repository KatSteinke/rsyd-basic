��    +      t  ;   �      �     �     �     �     �  
   �  	   �  ?   �     8  	   A     K  
   d     o  
   ~  C   �  	   �     �     �     �  0        A     W     m  "   �     �     �     �     �       V        v     �  	   �  
   �     �     �     �     �  	   �     �     �     �     �                   +     >  
   D  	   O  ?   Y     �  	   �     �  
   �     �  
   �  C   �  	   .	     8	     E	     ^	  0   q	     �	     �	     �	  "   �	     
     "
     9
     R
     i
  V   �
     �
     �
  	   �
  
   �
                    -  	   6     @     F     U     ^                           %   *   !             "      )         $   
                        (                                        &            +   	          '                           #                  og  ANI_til_ref ANI_til_ref_basics Andet Bestilt af Brønd_nr Coverage er lavere end grænsen for coverage ({min_coverage}).  DNA_Konc Forskning GTDB: kendt længde i bp ILM_Brønd Identifikation Indikation Ingen data for {fail_samples} - assembly'et er sandsynligvis failet Kommentar MADS species MADS: kendt længde i bp Miljøprøveisolat N50 er lavere end grænsen for N50 ({min_n50}).  O_WGS Serotype/gruppe O_WGS Toxinpåvisning O_WGS identifikation O_WGS res.gen anden beta-lactamase O_WGS res.gen carbapenemase O_WGS res.gen colistin O_WGS res.gen vancomycin O_WGS supplerende type Oprensnings_kørselsnr Over 20% forskel mellem forventet genomstørrelse fra MADS og fundet genomstørrelse.  Overvågningsisolat Relaps Resistens SEK_Run_nr Serotypning Toxingen_ID Udbrudsisolat godkendt køres om noter pipeline_noter proevenr res.gen Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
  og  ANI_til_ref ANI_til_ref_basics Andet Bestilt af Brønd_nr Coverage er lavere end grænsen for coverage ({min_coverage}).  DNA_Konc Forskning GTDB: kendt længde i bp ILM_Brønd Identifikation Indikation Ingen data for {fail_samples} - assembly'et er sandsynligvis failet Kommentar MADS species MADS: kendt længde i bp Miljøprøveisolat N50 er lavere end grænsen for N50 ({min_n50}).  O_WGS Serotype/gruppe O_WGS Toxinpåvisning O_WGS identifikation O_WGS res.gen anden beta-lactamase O_WGS res.gen carbapenemase O_WGS res.gen colistin O_WGS res.gen vancomycin O_WGS supplerende type Oprensnings_kørselsnr Over 20% forskel mellem forventet genomstørrelse fra MADS og fundet genomstørrelse.  Overvågningsisolat Relaps Resistens SEK_Run_nr Serotypning Toxingen_ID Udbrudsisolat godkendt køres om noter pipeline_noter proevenr res.gen 