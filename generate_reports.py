"""Generate reports for a given species."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
from argparse import ArgumentParser
from typing import Any, Dict, List, Optional

import yaml
import pandas as pd

import get_lis_report
import helpers
import localization_helpers
import parse_analysis
import pipeline_config
import set_log
import version
from get_analysis_subsets import SampleByIndication

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF
__version__ = version.__version__


logger = logging.getLogger("generate_reports")

sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(workflow_config)


def sort_numbered_cols(cols_to_sort: List[str], column_basename: str) -> List[str]:
    """Sort numbered columns by column number numerically.

     Arguments:
         cols_to_sort:      Names of columns to be sorted. Must have a common "base name"
                            followed by the number to sort by.
         column_basename:   The column's base name without the number

    Returns:
          The column names sorted by column number.
     """
    sorted_cols = sorted(cols_to_sort,
                         key = lambda colname:
                         int(re.match(column_basename + r"(?P<col_number>\d+)",
                                      colname).group("col_number")))
    return sorted_cols


def sort_for_mads(input_cols: pd.Index,
                  report_language: str = workflow_config["language"]) -> List[str]:
    """Sort report columns in the output's standard column order (alphabetic by long name,
    resistance and toxin genes grouped).

    Arguments:
        input_cols:         the columns of the report which should be sorted
        report_language:    the language of the report

    Returns:
        The column names in the report's standard sort order.

    Raises:
        KeyError:   if one or more of the required columns are missing
    """
    # set language
    _ = localization_helpers.set_language(report_language)
    # sanity check if all columns are there
    required_cols = {_("proevenr"), "O_WGS CC-type", "O_WGS cgMLST", _("O_WGS identifikation"),
                     _("O_WGS res.gen anden beta-lactamase"), _("O_WGS res.gen carbapenemase"),
                     _("O_WGS res.gen colistin"), _("O_WGS res.gen vancomycin"),
                     "O_WGS MLST", _("O_WGS Serotype/gruppe"), "O_WGS Spatype",
                     "O_WGS ST", _("O_WGS supplerende type")}
    missing_cols = sorted(list(required_cols - set(input_cols)))
    if missing_cols:
        raise KeyError(f"Columns {missing_cols} are missing from sample report.")
    if not any((column.startswith("O_WGS Plasmid-ID_") for column in input_cols)):
        raise KeyError("Report has been initialized without plasmid columns.")
    if not any((column.startswith(_("O_WGS Toxinpåvisning")) for column in input_cols)):
        raise KeyError("Report has been initialized without toxin gene columns.")
    # sort remaining columns
    resistance_columns = [column for column in input_cols
                          if column.startswith(_("O_WGS res.gen"))]
    # sort column names to ensure numbered columns always are grouped
    # TODO: DRY this out?
    plasmid_columns = [column for column in input_cols if column.startswith("O_WGS Plasmid-ID_")]
    plasmid_columns = sort_numbered_cols(plasmid_columns, "O_WGS Plasmid-ID_")
    toxin_columns = [column for column in input_cols
                     if column.startswith(_("O_WGS Toxinpåvisning"))]
    toxin_columns = sort_numbered_cols(toxin_columns, f'{_("O_WGS Toxinpåvisning")}_')
    report_columns = [_("proevenr"), "O_WGS CC-type", "O_WGS cgMLST", _("O_WGS identifikation"),
                      "O_WGS MLST"] \
                     + plasmid_columns \
                     + resistance_columns \
                     + [_("O_WGS Serotype/gruppe"), "O_WGS Spatype", "O_WGS ST",
                        _("O_WGS supplerende type")] \
                     + toxin_columns
    return report_columns


def get_basic_report(sample_number: str, gtdb_report: pathlib.Path, mlst_report: pathlib.Path,
                     target_language: str = workflow_config["language"]) -> pd.DataFrame:
    """Create the basic report for any sample, containing sample number and Kraken2 species ID.

    Arguments:
        sample_number:      The sample's sample number (with year and isolate number)
        gtdb_report:        GTDB-Tk results for all samples
        mlst_report:        MLST typing report from mlst
        target_language:    The language in which to output the report (valid languages: en, da)


    Returns:
        A report giving the sample's number and GTDB species identification.
    """
    _ = localization_helpers.set_language(target_language)
    # TODO: should we handle this here?
    gtdb_genus_species = parse_analysis.parse_gtdbtk(gtdb_report)
    gtdb_genus_species["WGS_species_GTDB"] = gtdb_genus_species["WGS_species_GTDB"].fillna(value="Unclassified")
    # and not all isolates are in the report
    try:
        gtdb_species_id = gtdb_genus_species.loc[gtdb_genus_species["proevenr"] == sample_number,
                                                 "WGS_species_GTDB"].iloc[0]
    except IndexError:
        gtdb_species_id = "Unclassified"
    gtdb_species_id = helpers.clean_gtdb_name(gtdb_species_id)
    # TODO: what if there is no species information - do we give genus as fallback or leave empty?
    mlst_result = parse_analysis.parse_mlst(mlst_report)

    base_report = pd.DataFrame(data = {_("proevenr"): [sample_number],
                                       "O_WGS CC-type": [pd.NA],
                                       "O_WGS cgMLST": [pd.NA],
                                       _("O_WGS identifikation"): gtdb_species_id,
                                       "O_WGS MLST": mlst_result["O_WGS MLST"],
                                       "O_WGS Plasmid-ID_0": [pd.NA],
                                       _("O_WGS res.gen anden beta-lactamase"): [pd.NA],
                                       _("O_WGS res.gen carbapenemase"): [pd.NA],
                                       _("O_WGS res.gen colistin"): [pd.NA],
                                       _("O_WGS res.gen vancomycin"): [pd.NA],
                                       _("O_WGS Serotype/gruppe"): [pd.NA],
                                       "O_WGS Spatype": [pd.NA],
                                       "O_WGS ST": [pd.NA],
                                       _("O_WGS supplerende type"): [pd.NA],
                                       f"{_('O_WGS Toxinpåvisning')}_0": [pd.NA]})
    return base_report


# TODO: how much of the paths should just be slapped on the sample itself?
def make_modular_report(sample_with_indications: SampleByIndication, gtdb_report: pathlib.Path,
                        mlst_report: pathlib.Path, plasmid_report: pathlib.Path,
                        resistance_report: Optional[pathlib.Path] = None,
                        serotype_report: Optional[pathlib.Path] = None,
                        virulence_report: Optional[pathlib.Path] = None,
                        target_language: str = workflow_config["language"]) -> pd.DataFrame:
    """
    Generate a report on a sample depending on indication- and species-specific analyses.

    Arguments:
        sample_with_indications:    The sample to report on, with sample number, species and
                                    requested analyses
        gtdb_report:                GTDB-Tk results for all samples
        mlst_report:                MLST typing report from mlst
        plasmid_report:             PlasmidFinder's tab-separated report
        resistance_report:          the raw abritamr.txt report
        serotype_report:            tab-separated serotyping report - from SeqSero2 for Salmonella
                                    or SerotypeFinder for E. coli
        virulence_report:           the raw virulence summary from abritAMR
        target_language:            the language in which the report should be output
                                    (valid languages: en, da)

    Returns:
        A report with results of all requested analyses for entry in MADS.

    """
    # set language
    _ = localization_helpers.set_language(target_language)
    sample_number = sample_with_indications.sample_number
    # set up a basic report
    basic_report = get_basic_report(sample_number, gtdb_report, mlst_report,
                                    target_language = target_language)
    species_reported = basic_report.at[0, _("O_WGS identifikation")]
    # note here if there is a mismatch between expected and reported species
    if species_reported == "Unclassified":
        logger.warning(f"No species found for {sample_number}")
    elif species_reported != sample_with_indications.species:
        logger.warning(f"Species should be {sample_with_indications.species}, "
                       f"is {species_reported}.")

    additional_reports = []
    plasmid_result = parse_analysis.parse_plasmidfinder(plasmid_report,
                                                        sample_format = sample_number)
    additional_reports.append(plasmid_result)
    # if resistance specifically requested or part of species-specific analyses, add resistance
    if sample_with_indications.resistance:
        if not resistance_report:
            # TODO: raise more specifically?
            raise FileNotFoundError(f"Resistance should be determined for "
                                    f"{sample_with_indications.sample_number}"
                                    f"but no report was supplied.")
        # handle potentially empty
        try:
            abritamr_result = pd.read_csv(resistance_report, sep = "\t")
            resistances = parse_analysis.parse_abritamr(abritamr_result)
            # internal AMR gene classes are in Danish by default so just translate here
            resistances = resistances.rename(mapper = _, axis = "columns")
            additional_reports.append(resistances)
        except pd.errors.EmptyDataError:
            logger.warning(f"No resistance genes reported for sample "
                           f"{sample_number}.")
    # serotyping is implemented for Salmonella (SeqSero2) and E. coli (SerotypeFinder):
    # different programs, requiring different parsing
    # nicer flow so we don't have to translate twice?
    if serotype_report:
        if sample_with_indications.species == "Escherichia coli":
            # if serotyping fails here we also need to handle the empty file
            try:
                serotype_data = pd.read_csv(serotype_report, sep = "\t")
                # we know the sample number so we can just look for that explicitly -
                # TODO: improve serotypefinder parsing?
                serotype = parse_analysis.parse_serotype_finder(serotype_data,
                                                                sample_number)
                serotype = serotype.rename(mapper = _, axis = "columns")
                additional_reports.append(serotype)
            except pd.errors.EmptyDataError:
                logger.warning(f"Serotype report for sample"
                               f" {sample_number} is empty.")
        elif sample_with_indications.species == "Salmonella enterica":
            serotype_data = pd.read_csv(serotype_report, sep = "\t")
            serotype = parse_analysis.parse_seqsero(serotype_data)
            serotype = serotype.rename(mapper = _, axis = "columns")
            additional_reports.append(serotype)
        else:
            # TODO: should this *fail*?
            raise NotImplementedError(f"Serotyping report given for "
                                      f"sample {sample_number} "
                                      f"(species: {sample_with_indications.species})."
                                      f"Serotyping reports are only supported for E. coli "
                                      f"and Salmonella.")

    # add toxins if requested
    if sample_with_indications.toxin:
        if not virulence_report:
            # TODO: raise more specifically?
            raise FileNotFoundError(f"Toxins should be determined for "
                                    f"{sample_number}"
                                    f"but no report was supplied.")
        # handle potentially empty toxin results
        try:
            toxins_result = pd.read_csv(virulence_report, sep = "\t")
            toxins = parse_analysis.parse_toxins(toxins_result, target_language = target_language)
            # since this won't necessarily be *entirely* blank (contains sample name),
            # complain if it succeeds but has no toxin genes reported
            if toxins[f'{_("O_WGS Toxinpåvisning")}_0'].isna().all():
                logger.warning(f"No toxin genes reported for sample "
                               f"{sample_number}.")
            additional_reports.append(toxins)
        except pd.errors.EmptyDataError:
            logger.warning(f"No toxin genes reported for sample "
                           f"{sample_number}.")
    # if we have additional reports, fill out any NA values here
    if additional_reports:
        for report in additional_reports:
            basic_report = basic_report.combine_first(report)
    report_columns = sort_for_mads(basic_report.columns, report_language = target_language)
    return basic_report[report_columns]


# TODO: can we optimize this now it's a separate function? Drop workarounds?
def setup_sample_and_report(sample_name: str, gtdb_report: str, abritamr_report: str,
                            sequence_type: str, plasmid_report: str, mads_report: pd.DataFrame,
                            species_coding: pd.DataFrame, serotyping_report: Optional[str] = None,
                            toxin_report: Optional[str] = None,
                            active_config: Dict[str, Any] = workflow_config) -> pd.DataFrame:
    """Record indications for a sample and generate a report for it.

    Arguments:
        sample_name:        the sample's name
        gtdb_report:        GTDB-Tk results for all samples
        abritamr_report:    the path to the abritAMR report for the sample
        sequence_type:      the path to the MLST report for the sample
        plasmid_report:     the path to the tab-separated PlasmidFinder report for the sample
        mads_report:        the LIS report on sequenced samples, with sample number format matching
                            the one in the run sheet
        species_coding:     "translation" of LIS species codes to species names
        serotyping_report:  the path to serotyping results, if present
        toxin_report:       the path to abritAMR's virulence results, if required
        active_config:      the config to use

    Returns:
        A report with results of all requested analyses for entry in MADS.
    """
    # TODO: too unintuitive to use the config here?
    (runsheet_names,
     lis_data_names,
     bacteria_names) = localization_helpers.load_input_from_config(active_config)
    # TODO: check behavior: should it fail or just code it as a generic thing? Should we fail earlier?
    sample_species = helpers.get_species_by_number(sample_name, mads_report, species_coding,
                                                   lis_data_names = lis_data_names,
                                                   bacteria_names = bacteria_names)
    if pd.isna(sample_species):
        # we only look the code up if we need it to complain
        sample_code = mads_report.loc[mads_report["proevenr"] == sample_name,
                                      lis_data_names.bacteria_code].iloc[0]
        logger.warning(f"Bacteria code {sample_code} not in translation table. "
                       f"Proceeding with generic species for sample {sample_name}.")
        sample_species = "Bacteria"
    # explicitly have optional reports as None to start with
    serotyping_data = None
    toxin_data = None
    # TODO: can we get species more nicely?
    sample_with_indications = SampleByIndication(sample_name, species = sample_species)
    sample_with_indications.resistance = True
    abritamr_report = pathlib.Path(abritamr_report)
    if serotyping_report:
        serotyping_data = pathlib.Path(serotyping_report)
    if toxin_report:
        sample_with_indications.toxin = True
        toxin_data = pathlib.Path(toxin_report)
    sample_report = make_modular_report(sample_with_indications, pathlib.Path(gtdb_report),
                                        pathlib.Path(sequence_type), pathlib.Path(plasmid_report),
                                        resistance_report = abritamr_report,
                                        serotype_report = serotyping_data,
                                        virulence_report = toxin_data,
                                        target_language = active_config["language"])
    return sample_report


if __name__ == "__main__":
    logger = set_log.get_stream_log(level="WARNING")
    logger.setLevel(logging.INFO)
    arg_parser = ArgumentParser(description = "Create report for entry into LIS for one sample")
    arg_parser.add_argument("sample_name", help = "The sample's number or name")
    arg_parser.add_argument("gtdb_report", help = "the path to the GTDB report for the sample")
    arg_parser.add_argument("abritamr_report",
                            help = "the path to the abritAMR report for the sample")
    arg_parser.add_argument("mlst_report", help = "the path to the MLST report for the sample")
    arg_parser.add_argument("plasmid_report",
                            help = "the path to the PlasmidFinder report for the sample")
    arg_parser.add_argument("--mads_report",
                            help = f'the LIS report on sequenced samples, '
                                   f'with sample number format matching the one in the run sheet '
                                   f'(default: as given in config)',
                            default = None)
    arg_parser.add_argument("--species_coding",
                            help = f'"translation" of LIS species codes to species names '
                                   f'(default: as given in config)',
                            default = None)
    arg_parser.add_argument("--serotyping", help = " the path to serotyping results, if present",
                            default = None)
    arg_parser.add_argument("--toxin_report",
                            help = "the path to abritAMR's virulence results, if required",
                            default = None)
    arg_parser.add_argument("--outfile", help = "path to output the file in long format to "
                                                "(default: [SAMPLE]/for_mads/[SAMPLE]_report.tsv)",
                            default = None)
    arg_parser.add_argument("--outfile_wide", help = "path to output the file in wide format to "
                                                     "(default: "
                                                     "[SAMPLE]/for_mads/[SAMPLE]_mads_wide.tsv)",
                            default = None)
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script, "
                                   "can be overridden by commandline options)")
    args = arg_parser.parse_args()
    sample = args.sample_name
    gtdb_results = args.gtdb_report
    abritamr_results = args.abritamr_report
    mlst_results = args.mlst_report
    plasmid_results = args.plasmid_report
    serotyping = args.serotyping  # type: Optional[str]
    toxin_results = args.toxin_report  # type: Optional[str]
    # get everything that depends on the config
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file).resolve()
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        (sheet_names,
         lis_names,
         bact_names) = localization_helpers.load_input_from_config(workflow_config)
    if args.mads_report:
        lis_report = args.mads_report
    else:
        lis_report = workflow_config["lab_info_system"]["lis_report"]
    if args.species_coding:
        species_codes = args.species_coding
    else:
        species_codes = workflow_config["lab_info_system"]["bacteria_codes"]

    if args.outfile:
        outfile_long = pathlib.Path(args.outfile)
    else:
        outfile_long = pathlib.Path(sample) / "for_mads" / f"{sample}_report.tsv"

    if args.outfile_wide:
        outfile_wide = pathlib.Path(args.outfile_wide)
    else:
        outfile_wide = pathlib.Path(sample) / "for_mads" / f"{sample}_mads_wide.tsv"

    # read LIS files
    mads_data = get_lis_report.get_report_with_isolate_number(lis_report = lis_report,
                                                              translate_to = workflow_config[
                                                           "sample_number_settings"][
                                                           "sample_numbers_report"],
                                                              translate_from = workflow_config[
                                                           "sample_number_settings"][
                                                           "sample_numbers_out"],
                                                              from_format = re.compile(workflow_config[
                                                           "sample_number_settings"][
                                                           "format_in_lis"]),
                                                              to_format = re.compile(workflow_config[
                                                           "sample_number_settings"][
                                                           "format_output"]),
                                                              lis_data_names = lis_names
                                                              )
    # get "broad" species coding (all subspecies, serovars etc. collapsed into one)
    species_code_data = pd.read_csv(species_codes, sep = ";",
                                    encoding = "latin-1",
                                    dtype = {bact_names.bacteria_code: str})

    # get report for LIS
    long_for_lis = setup_sample_and_report(sample, gtdb_report = gtdb_results,
                                           abritamr_report = abritamr_results,
                                           sequence_type = mlst_results,
                                           plasmid_report = plasmid_results,
                                           mads_report = mads_data,
                                           species_coding = species_code_data,
                                           serotyping_report = serotyping,
                                           toxin_report = toxin_results,
                                           active_config = workflow_config)
    long_for_lis.to_csv(outfile_long, sep = "\t", index = False)

    # pivot and write to wide
    wide_for_lis = long_for_lis.transpose()
    wide_for_lis.to_csv(path_or_buf = outfile_wide, sep = "\t")
