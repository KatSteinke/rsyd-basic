"""Get average coverage for all Nanopore samples."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pathlib
import re

from argparse import ArgumentParser
from typing import List

import pandas as pd
import yaml

import pipeline_config
import version

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

def get_report_with_sample_number(coverage_report: pathlib.Path,
                                  sample_number_format: str) -> pd.DataFrame:
    """Read coverage report from file and extract sample number from file name.

    Arguments:
        coverage_report:        the path to the flye coverage report
        sample_number_format:   the format used for sample numbers

    Returns:
        The flye coverage report with the sample number added; a blank report for an empty file.

    Raises:
        ValueError: if the file name doesn't contain a valid sample number
    """
    # sanity check sample number format before reading
    find_sample_number = re.search(f"(?P<proevenr>{sample_number_format})"
                                   r"_assembly_info\.txt",
                                   coverage_report.name)
    if not find_sample_number:
        error_msg = (f"Cannot extract sample number from {coverage_report.name}"
                     " - invalid sample number format.")
        raise ValueError(error_msg)

    try:
        coverage_result = pd.read_csv(coverage_report, sep = "\t", dtype = {"cov.": float})
    except pd.errors.EmptyDataError:
        coverage_result = pd.DataFrame(data = {"#seq_name": [pd.NA],
                                               "length": [pd.NA],
                                               "cov.": [pd.NA]})
    coverage_result["proevenr"] = find_sample_number.groups("proevenr")[0]

    return coverage_result




def get_average_coverage_per_sample(coverage_reports: List[pathlib.Path],
                                    sample_number_format: str) -> pd.DataFrame:
    """Concatenate multiple samtools coverage reports and extract average coverage per sample,
    weighted by length.

    Arguments:
        coverage_reports:          paths to the coverage reports to summarize
        sample_number_format:      the format used for sample numbers

    Returns:
        Coverage statistics for all samples, with coverage averaged across
        all contigs of a given sample, weighted by length.

    Raises:
        ValueError: if contig names don't contain a valid sample number
    """
    coverage_data = []
    fail_numbers = []
    for coverage_report in coverage_reports:
        try:
            coverage_from_report = get_report_with_sample_number(coverage_report,
                                                                 sample_number_format)
            coverage_data.append(coverage_from_report)
        except ValueError:
            fail_numbers.append(coverage_report.name)

    if fail_numbers:
        error_msg = (f"Cannot extract sample number from {fail_numbers}"
                     " - invalid sample number format.")
        raise ValueError(error_msg)
    all_reports = pd.concat(coverage_data)
    all_reports["amount_bases_covering"] = all_reports["length"] * all_reports["cov."]
    mean_coverage = all_reports[["proevenr",
                                 "length",
                                 "amount_bases_covering"]].groupby(by="proevenr").apply(lambda sample:
                                                                                        sum(sample["amount_bases_covering"])
                                                                                        /sum(sample["length"]))
    mean_coverage = mean_coverage.rename("Weighted coverage depth").reset_index()
    # we also want the coverage of the longest contig (best case: closed chromosome)
    longest_contig = all_reports.groupby("proevenr").apply(lambda sample:
                                                           sample.sort_values(by=["length",
                                                                                  "cov."],
                                                                              ascending = False).iloc[0])["cov."].reset_index()
    longest_contig = longest_contig.rename(columns = {"cov.": "longest_contig_coverage"})
    longest_contig = longest_contig.drop_duplicates()
    all_cov_stats = mean_coverage.merge(longest_contig, on="proevenr")
    return all_cov_stats[["proevenr", "Weighted coverage depth", "longest_contig_coverage"]]


if __name__ == "__main__":
    arg_parser = ArgumentParser("Merge Nanopore coverage data and average across contigs "
                                "per sample")
    arg_parser.add_argument("coverage_files", help="Coverage file(s) to merge",
                            nargs = "+")
    arg_parser.add_argument("--outfile", help="Output file to write summary to "
                                              "(default: nanopore_coverage.tsv)",
                            default = "nanopore_coverage.tsv")
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script, "
                                   "can be overridden by commandline options)")
    args = arg_parser.parse_args()
    coverage_files = [pathlib.Path(report) for report in args.coverage_files]
    outfile = pathlib.Path(args.outfile)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file).resolve()
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)

    id_pattern = workflow_config["sample_number_settings"]["sample_number_format"]
    coverage_all = get_average_coverage_per_sample(coverage_files, id_pattern)
    coverage_all.to_csv(outfile, sep="\t", index=False)

