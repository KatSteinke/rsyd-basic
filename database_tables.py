"""Table definitions for the result database."""

__author__ = "Kat Steinke"

from typing import Any, Dict, List, Literal, Set, Union, get_args


from sqlalchemy import Column, String, ForeignKey, Integer, Date, Boolean, Float, DateTime, inspect
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.orm.collections import InstrumentedList

import version

__version__ = version.__version__

# establish tables:
Base = declarative_base()

# set up comparison types
_RELATIONSHIP_COMPARISONS = Literal["none", "properties", "strict"]


class ComparisonMixin:
    """Implement different levels of comparison for database objects."""
    relationships: Set[str]

    def to_dict(self) -> Dict:
        """Return a dict representation of the object."""

    def to_property_dict(self) -> Dict:
        """Return a dict without relationships."""

    def check_matches(self, other,
                      check_relationships: _RELATIONSHIP_COMPARISONS = "none") \
            -> bool:
        """Compare two objects (for testing purposes) without interfering with SQLAlchemy's checks.

        Arguments:
            other:                  another object to compare the current object to
            check_relationships:    whether to compare attached relationships
                                    (by properties).
                                    If "none", relationships aren't checked.
                                    If "properties", related objects' properties is checked.
                                    If "strict", related objects' identity is checked.

        Returns:
            True if the object's properties (and related objects or their properties if specified)
            are identical,
            False otherwise.
        """
        if check_relationships not in get_args(_RELATIONSHIP_COMPARISONS):
            raise ValueError(f"{check_relationships} is not a valid comparison type.")
        if not isinstance(other, self.__class__) or not isinstance(self, other.__class__):
            print("wrong class")
            return False
        # for strict identity checks we'll just compare dicts
        if check_relationships == "strict":
            return self.to_dict() == other.to_dict()

        # otherwise we are at most looking at related objects' properties
        relationships = self.relationships
        own_properties = self.to_property_dict()
        other_properties = other.to_property_dict()
        # we're going to get np.nan out of the database and np.nan != np.nan,
        # so handle these separately
        # we can use the NaN != NaN property to filter them out
        own_nan_properties = {key for key, value in own_properties.items()
                              if value != value}
        own_notnan_properties = {key: value for key, value in own_properties.items()
                                 if value == value}
        other_nan_properties = {key for key, value in other_properties.items()
                                if value != value}
        other_notnan_properties = {key: value for key, value in other_properties.items()
                                   if value == value}
        only_nan_in_own = own_nan_properties - other_nan_properties
        only_nan_in_other = other_nan_properties - own_nan_properties
        matches = not (only_nan_in_other or only_nan_in_own)
        matches = matches and (own_notnan_properties == other_notnan_properties)
        # TODO: improve flow here?
        if check_relationships == "properties":
            own_with_rels = self.to_dict()
            other_with_rels = other.to_dict()
            for related_object in relationships:
                if own_with_rels.get(related_object):
                    if not other_with_rels.get(related_object):
                        return False
                    # everything should be the same class
                    if isinstance(own_with_rels[related_object], InstrumentedList):
                        inspect_class = inspect(own_with_rels[related_object][0].__class__)
                        current_key = inspect_class.primary_key[0].name
                        # are there any objects with different ID?
                        own_related_object_ids = {own_object.to_property_dict()[current_key]
                                                  for own_object in own_with_rels[related_object]}
                        other_related_object_ids = {other_object.to_property_dict()[current_key]
                                                    for other_object in
                                                    other_with_rels[related_object]}
                        if (own_related_object_ids - other_related_object_ids) or (
                                other_related_object_ids - own_related_object_ids):
                            return False
                        # for each related object of the two:
                        # compare but only if the primary keys match
                        related_match = all(own_object.check_matches(other_object)
                                            for own_object in own_with_rels[related_object]
                                            for other_object in other_with_rels[related_object]
                                            if
                                            own_object.to_property_dict()[current_key] ==
                                            other_object.to_property_dict()[
                                                current_key])

                    else:
                        related_match = own_with_rels[related_object].check_matches(
                            other_with_rels[related_object])
                    matches = matches and related_match
                else:
                    if other_with_rels.get(related_object):
                        return False
        return matches


# samples: needs sample ID (just MADS?), institution (likely us), lab section (sample type letter)
class Sample(ComparisonMixin, Base):
    """Samples for which isolates have been sequenced.

    Attributes:
        mads_proevenr:  the sample's sample number in the LIS
        institution:    the institution responsible for the sample
        lab_section:    the lab section from which the sample comes
        isolates:       the isolates isolated from this sample
    """
    __tablename__ = "test_sample"

    mads_proevenr = Column(String, primary_key=True)
    institution = Column(String)
    lab_section = Column(String(length = 1))

    # TODO: how to handle type?
    isolates = relationship("Isolate", back_populates = "sample",
                            collection_class = list)  # type: List[Isolate]

    relationships = {"isolates"}

    def __repr__(self):
        return f"Sample(mads_proevenr={self.mads_proevenr}, institution={self.institution}," \
               f" lab_section={self.lab_section}, " \
               f"isolates=" \
               f"{sorted(f'Isolate({isolate.mads_isolatnr})' for isolate in self.isolates)})"

    def to_dict(self) -> Dict[str, Union[str, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"mads_proevenr": self.mads_proevenr, "institution": self.institution,
                "lab_section": self.lab_section, "isolates": self.isolates}

    def to_property_dict(self) -> Dict[str, str]:
        """Convert object to dictionary, removing relationships."""
        return {"mads_proevenr": self.mads_proevenr, "institution": self.institution,
                "lab_section": self.lab_section}


# isolates: when samples have been added: needs sample ID, isolate ID, lab ID if given;
# pipeline organism ID is set if sample is approved; serotype if present; institution (us)
class Isolate(ComparisonMixin, Base):
    """Isolates that have been sequenced, with laboratory ID and the latest approved
    WGS species/serotype ID if present.

    Attributes:
        mads_isolatnr:              the isolate's number in the LIS
        mads_proevenr:              the isolate's parent sample's number in the LIS
        preliminary_organism_name:  the wet lab identification of the organism
        final_organism_name:        the WGS identification of the organism
                                    (if successfully sequenced)
        wgs_serotyping:             any serotype/serovar identified by WGS
        institution:                the institution responsible for the isolate
        sample:                     the isolate's parent Sample
        requests:                   any SeqRequests associated with the isolate
        eluates:                    the Eluates obtained from the isolate
        read_files:                 the read files from the isolate

    """
    __tablename__ = "test_isolate"

    mads_isolatnr = Column(String, primary_key=True)
    mads_proevenr = Column(String, ForeignKey("test_sample.mads_proevenr"), nullable=False)
    preliminary_organism_name = Column(String)
    final_organism_name = Column(String, nullable=True)
    wgs_serotyping = Column(String, nullable=True)
    institution = Column(String)

    sample = relationship("Sample", back_populates = "isolates")
    requests = relationship("SeqRequest", back_populates = "isolate",
                            collection_class = list)  # type: List[SeqRequest]
    eluates = relationship("Eluate", back_populates = "isolate",
                           collection_class = list)  # type: List[Eluate]
    read_files = relationship("ReadFile", back_populates="isolate",
                              collection_class = list)  # type: List[ReadFile]

    relationships = {"sample", "requests", "eluates", "read_files"}

    def __repr__(self):
        return f"Isolate(mads_isolatnr={self.mads_isolatnr}, mads_proevenr={self.mads_proevenr}, " \
               f"preliminary_organism_name={self.preliminary_organism_name}, " \
               f"final_organism_name={self.final_organism_name}, " \
               f"wgs_serotyping={self.wgs_serotyping}, " \
               f"institution={self.institution}, " \
               f"sample=Sample({self.sample.mads_proevenr if self.sample else ''}), " \
               f"requests={sorted(f'SeqRequest({request.request_id})' for request in self.requests)}, " \
               f"eluates={sorted(f'Eluate({eluate.eluat_id})' for eluate in self.eluates)}, " \
               f"read_files={sorted(f'ReadFile({readfile.read_file})' for readfile in self.read_files)})"

    def to_dict(self) -> Dict[str, Union[str, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"mads_isolatnr": self.mads_isolatnr,
                "mads_proevenr": self.mads_proevenr,
                "institution": self.institution,
                "preliminary_organism_name": self.preliminary_organism_name,
                "final_organism_name": self.final_organism_name,
                "wgs_serotyping": self.wgs_serotyping,
                "sample": self.sample,
                "requests": self.requests,
                "eluates": self.eluates,
                "read_files": self.read_files}

    def to_property_dict(self) -> Dict[str, Union[str, List]]:
        """Convert object to dictionary, removing relationships."""
        return {"mads_isolatnr": self.mads_isolatnr,
                "mads_proevenr": self.mads_proevenr,
                "institution": self.institution,
                "preliminary_organism_name": self.preliminary_organism_name,
                "final_organism_name": self.final_organism_name,
                "wgs_serotyping": self.wgs_serotyping}


# sequencing run: needs ID, name, platform (MiSeq/NextSeq/MinION...),
# sequencing type (Illumina/Nanopore), flowcell ID (needs to be registered for Illumina),
# path to runsheet (as given on virtual server), path to data (as given on virtual server),
# lab tech ID, sequencing date, institution
class SequencingRun(ComparisonMixin, Base):
    """Bacterial isolate sequencing runs.

    Attributes:
        seq_run_id:         the run's database identifier
        seq_run_name:       the run's name
        platform:           the sequencing platform (specific sequencer: MiSeq, NextSeq, GridION...)
        sequencing_type:    the sequencing technique (Illumina/Nanopore)
        flowcell_id:        the flowcell's ID number (especially for Nanopore)
        runsheet_path:      the path to the run's full runsheet
        data_path:          the path to all sequencing data generated by the run
        run_user_id:        the user starting the run
        run_date:           the date on which the run was started
        institution:        the institution where sequencing was performed
        read_files:         the read files generated by the run

    """
    __tablename__ = "test_sequencing_run"

    seq_run_id = Column(Integer, primary_key=True)
    seq_run_name = Column(String)
    platform = Column(String)
    sequencing_type = Column(String)
    flowcell_id = Column(String, nullable=True)
    runsheet_path = Column(String)  # TODO: String or Longtext?
    data_path = Column(String)  # TODO: longtext
    run_user_id = Column(String)    # TODO: change in actual DB
    run_date = Column(Date)
    institution = Column(String)

    read_files = relationship("ReadFile", back_populates="sequencing_run")  # type: List[ReadFile]

    relationships = {"read_files"}

    def __repr__(self):
        return f"SequencingRun(seq_run_id={self.seq_run_id}, seq_run_name={self.seq_run_name}, " \
               f"run_date={self.run_date}, run_user_id={self.run_user_id}, " \
               f"platform={self.platform}, sequencing_type={self.sequencing_type}, " \
               f"flowcell_id={self.flowcell_id}, institution={self.institution}, " \
               f"runsheet_path={self.runsheet_path}, data_path={self.data_path}, " \
               f"read_files=" \
               f"{sorted(f'ReadFile({readfile.read_file})' for readfile in self.read_files) if self.read_files else ''})"

    def to_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"seq_run_id": self.seq_run_id,
                "seq_run_name": self.seq_run_name,
                "run_date": self.run_date,
                "run_user_id": self.run_user_id,
                "platform": self.platform,
                "sequencing_type": self.sequencing_type,
                "flowcell_id": self.flowcell_id,
                "institution": self.institution,
                "runsheet_path": self.runsheet_path,
                "data_path": self.data_path,
                "read_files": self.read_files}

    def to_property_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary, removing relationships."""
        return {"seq_run_id": self.seq_run_id,
                "seq_run_name": self.seq_run_name,
                "run_date": self.run_date,
                "run_user_id": self.run_user_id,
                "platform": self.platform,
                "sequencing_type": self.sequencing_type,
                "flowcell_id": self.flowcell_id,
                "institution": self.institution,
                "runsheet_path": self.runsheet_path,
                "data_path": self.data_path}


# requests: needs isolate ID; generate request ID (database key); add MADS sample and isolate ID,
# "requested by", indications (bool -> int), registered date, gram positive/negative (bool -> int),
# comment if present, "registered by" - take from runsheet. Initials to uppercase;
# do we need to verify if they exist?
class SeqRequest(ComparisonMixin, Base):
    """Sequencing requests for a given sample.

    Attributes:
        request_id:                 the request's database ID
        mads_isolatnr:              the LIS number of the isolate for which the request was
                                    submitted
        mads_proevenr:              the LIS number of the isolate's parent sample
        requested_by:               initials of the person requesting sequencing
        resistens:                  was sequencing requested due to AMR patterns?
        identifikation:             was sequencing requested to identify the sample?
        relaps:                     was sequencing requested due to a suspected relapse?
        toxingen_identifikation:    was sequencing requested to identify toxin genes?
        hai:                        was sequencing requested due to a suspected HAI?
        miljoeproeve:               is this an environmental isolate?
        udbrud:                     was sequencing requested due to a suspected outbreak?
        overvaagning:               was sequencing requested for surveillance?
        forskning:                  was sequencing requested as part of a research project?
        andet:                      was sequencing requested for another reason?
        request_date:               the date the request was submitted
        institution:                the institution where the request was submitted
        gram_positiv:              is the organism Gram-positive?
        gram_negativ:              is the organism Gram-negative?
        request_comment:            any comments to the request
        secondary_id:               non-LIS IDs for the isolate
        registered_by:              the person requesting the sequencing
        isolate:                    the Isolate for which this request was made
        assembly:                   the Assembly this request was made for
    """
    __tablename__ = "test_seq_request"

    request_id = Column(Integer, primary_key = True)
    mads_isolatnr = Column(String, ForeignKey("test_isolate.mads_isolatnr"), nullable = False)
    mads_proevenr = Column(String)  # TODO: foreign key?
    requested_by = Column(String)
    # TODO: repr based on boolean values?
    resistens = Column(Boolean)
    identifikation = Column(Boolean)
    relaps = Column(Boolean)
    toxingen_identifikation = Column(Boolean)
    hai = Column(Boolean)
    miljoeproeve = Column(Boolean)
    udbrud = Column(Boolean)
    overvaagning = Column(Boolean)
    forskning = Column(Boolean)
    andet = Column(Boolean)
    request_date = Column(DateTime)
    institution = Column(String)
    gram_positiv = Column(Boolean)
    gram_negativ = Column(Boolean)
    request_comment = Column(String, nullable=True)  # TODO: longtext?
    secondary_id = Column(String, nullable=True)
    registered_by = Column(String)

    isolate = relationship("Isolate", back_populates="requests")
    assembly = relationship("Assembly", back_populates="seq_request")  # type: List[Assembly]

    relationships = {"isolate", "assembly"}

    def get_gram_status(self) -> str:
        """
        Get Gram status.

        Returns:
            positive for samples where only Gram-positive is marked off as True,
            negative for samples where only Gram-negative is marked off as True,
            ambiguous for everything else.
        """
        if self.gram_positiv and not self.gram_negativ:
            gram_status = "positive"
        elif self.gram_negativ and not self.gram_positiv:
            gram_status = "negative"
        else:
            gram_status = "ambiguous"
        return gram_status

    # TODO: get indications?

    def __repr__(self):
        return f"SeqRequest(request_id={self.request_id}, mads_isolatnr={self.mads_isolatnr}, " \
               f"mads_proevenr={self.mads_proevenr}, requested_by={self.requested_by}, " \
               f"request_date={self.request_date}, institution={self.institution}, " \
               f"gram_status={self.get_gram_status()}, request_comment={self.request_comment}, " \
               f"secondary_id={self.secondary_id}, registered_by={self.registered_by}, " \
               f"isolate=Isolate({self.isolate.mads_isolatnr if self.isolate else ''}), " \
               f"assembly=" \
               f"{sorted(f'Assembly({assembly.assembly_file})' for assembly in self.assembly) if self.assembly else ''})"

    def to_dict(self) -> Dict[str, Any]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"request_id": self.request_id,
                "mads_isolatnr": self.mads_isolatnr,
                "mads_proevenr": self.mads_proevenr,
                "requested_by": self.requested_by,
                "resistens": self.resistens,
                "identifikation": self.identifikation,
                "relaps": self.relaps,
                "toxingen_identifikation": self.toxingen_identifikation,
                "hai": self.hai,
                "miljoeproeve": self.miljoeproeve,
                "udbrud": self.udbrud,
                "overvaagning": self.overvaagning,
                "forskning": self.forskning,
                "andet": self.andet,
                "request_date": self.request_date,
                "institution": self.institution,
                "gram_positiv": self.gram_positiv,
                "gram_negativ": self.gram_negativ,
                "request_comment": self.request_comment,
                "secondary_id": self.secondary_id,
                "registered_by": self.registered_by,
                "isolate": self.isolate,
                "assembly": self.assembly}

    def to_property_dict(self) -> Dict[str, Any]:
        """Convert object to dictionary, removing relationships."""
        return {"request_id": self.request_id,
                "mads_isolatnr": self.mads_isolatnr,
                "mads_proevenr": self.mads_proevenr,
                "requested_by": self.requested_by,
                "resistens": self.resistens,
                "identifikation": self.identifikation,
                "relaps": self.relaps,
                "toxingen_identifikation": self.toxingen_identifikation,
                "hai": self.hai,
                "miljoeproeve": self.miljoeproeve,
                "udbrud": self.udbrud,
                "overvaagning": self.overvaagning,
                "forskning": self.forskning,
                "andet": self.andet,
                "request_date": self.request_date,
                "institution": self.institution,
                "gram_positive": self.gram_positiv,
                "gram_negative": self.gram_negativ,
                "request_comment": self.request_comment,
                "secondary_id": self.secondary_id,
                "registered_by": self.registered_by}


# extraction run: needs run ID (taken from QC results or runsheet) and name, run date, run user,
# method and method version (needs to be noted!), institution (us again)

class ExtractRun(ComparisonMixin, Base):
    """DNA extraction runs before sequencing.

    Attributes:
        extract_run_id:     the extraction run's database ID
        extract_run_date:   the date on which DNA extraction was performed
        extract_run_user:   the user performing DNA extraction
        extract_run_name:   the extraction run's name
        method:             the method used for DNA extraction
        method_version:     the version of the extraction protocol
        institution:        the institution at which DNA extraction was performed
        eluates:            the eluates obtained through the extraction run
    """
    __tablename__ = "test_extract_run"

    extract_run_id = Column(Integer, primary_key=True)
    extract_run_date = Column(Date)
    extract_run_user = Column(String)
    extract_run_name = Column(String)
    method = Column(String)
    method_version = Column(String)
    institution = Column(String)

    eluates = relationship("Eluate", back_populates="extract_run")  # type: List[Eluate]

    relationships = {"eluates"}

    def __repr__(self):
        return f"ExtractRun(extract_run_id={self.extract_run_id}, " \
               f"extract_run_date={self.extract_run_date}, " \
               f"extract_run_user={self.extract_run_user}, " \
               f"extract_run_name={self.extract_run_name}, " \
               f"method={self.method}, method_version={self.method_version}, " \
               f"institution={self.institution}, " \
               f"eluates=" \
               f"{sorted(f'Eluate({eluate.eluat_id})' for eluate in self.eluates) if self.eluates else ''})"

    def to_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"extract_run_id": self.extract_run_id,
                "extract_run_date": self.extract_run_date,
                "extract_run_user": self.extract_run_user,
                "extract_run_name": self.extract_run_name,
                "method": self.method,
                "method_version": self.method_version,
                "institution": self.institution,
                "eluates": self.eluates}

    def to_property_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary, removing relationships."""
        return {"extract_run_id": self.extract_run_id,
                "extract_run_date": self.extract_run_date,
                "extract_run_user": self.extract_run_user,
                "extract_run_name": self.extract_run_name,
                "method": self.method,
                "method_version": self.method_version,
                "institution": self.institution}


# eluate: needs extraction run, MADS number; generates eluate ID
class Eluate(ComparisonMixin, Base):
    """Eluates from DNA extraction runs.

    Attributes:
        eluat_id:       the database ID of the eluate
        mads_isolatnr:  the LIS number of the isolate to which this eluate belongs
        extract_run_id: the ID of the DNA extraction run during which the eluate was obtained
        extract_run:    the DNA extraction run during which the eluate was obtained
        isolate:        the isolate to which this eluate belongs
        read_files:     the read files obtained from sequencing this eluate
    """

    __tablename__ = "test_eluate"

    eluat_id = Column(Integer, primary_key=True)
    mads_isolatnr = Column(String, ForeignKey("test_isolate.mads_isolatnr"), nullable=False)
    extract_run_id = Column(Integer, ForeignKey("test_extract_run.extract_run_id"), nullable=False)

    extract_run = relationship("ExtractRun", back_populates="eluates")
    isolate = relationship("Isolate", back_populates="eluates")
    read_files = relationship("ReadFile", back_populates="eluate")  # type: List[ReadFile]

    relationships = {"extract_run", "isolate", "read_files"}

    def __repr__(self):
        return f"Eluate(eluat_id={self.eluat_id}, mads_isolatnr={self.mads_isolatnr}, " \
               f"extract_run_id={self.extract_run_id}, " \
               f"extract_run=" \
               f"ExtractRun({self.extract_run.extract_run_id if self.extract_run else ''}), " \
               f"isolate=" \
               f"Isolate({self.isolate.mads_isolatnr if self.isolate else ''}), " \
               f"read_files=" \
               f"{sorted(f'ReadFile({readfile.read_file})' for readfile in self.read_files) if self.read_files else ''})"

    def to_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"eluat_id": self.eluat_id,
                "mads_isolatnr": self.mads_isolatnr,
                "extract_run_id": self.extract_run_id,
                "extract_run": self.extract_run,
                "isolate": self.isolate,
                "read_files": self.read_files}

    def to_property_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary, removing relationships."""
        return {"eluat_id": self.eluat_id,
                "mads_isolatnr": self.mads_isolatnr,
                "extract_run_id": self.extract_run_id}


# read file: generates read file ID; needs read type (Illumina or nanopore)
# read direction (NA for nanopore), MADS isolate number, path to raw file,
# sequencing batch (ID number), eluate ID, institution
class ReadFile(ComparisonMixin, Base):
    """Individual read files for samples from a sequencing run.

    Attributes:
        readfile_id:        the database ID of the read file
        read_type:          the type of the read (Illumina or Nanopore)
        read_direction:     whether the read is forward or reverse (blank for e.g. Nanopore reads)
        mads_isolatnummer:  the LIS number of the isolate to which the reads belong
        read_file:          the path to the read file
        batch_id:           the database ID of the sequencing run during which reads were obtained
        eluat_id:           the database ID of the eluate that was sequenced to yield these reads
        institution:        the institution at which the reads were obtained
        isolate:            the isolate to which the reads belong
        sequencing_run:     the sequencing run during which reads were obtained
        eluate:             the eluate that was sequenced to yield these reads
        assembly_readfiles: combined read files for an assembly that include this read file
    """

    __tablename__ = "test_readfiles"

    readfile_id = Column(Integer, primary_key=True)
    read_type = Column(String)
    read_direction = Column(String)
    mads_isolatnummer = Column(String, ForeignKey("test_isolate.mads_isolatnr"), nullable=False)
    read_file = Column(String)  # tODO: longtext, path to read file
    batch_id = Column(Integer, ForeignKey("test_sequencing_run.seq_run_id"), nullable=False)
    eluat_id = Column(Integer, ForeignKey("test_eluate.eluat_id"), nullable=False)
    institution = Column(String)

    isolate = relationship("Isolate", back_populates="read_files")
    sequencing_run = relationship("SequencingRun", back_populates="read_files")
    eluate = relationship("Eluate", back_populates="read_files")
    assembly_readfiles = relationship("AssemblyReadFiles",
                                      primaryjoin='or_('
                                                  'ReadFile.readfile_id == AssemblyReadFiles.forward_readfile_id,'
                                                  'ReadFile.readfile_id == AssemblyReadFiles.reverse_readfile_id,'
                                                  'ReadFile.readfile_id == AssemblyReadFiles.long_readfile_id)',
                                      viewonly=True)  # type: List[AssemblyReadFiles]

    relationships = {"isolate", "sequencing_run", "eluate", "assembly_readfiles"}

    def __repr__(self):
        return f"ReadFile(readfile_id={self.readfile_id}, " \
               f"read_type={self.read_type}, " \
               f"read_direction={self.read_direction}, " \
               f"mads_isolatnummer={self.mads_isolatnummer}, read_file={self.read_file}, " \
               f"batch_id={self.batch_id}, eluat_id={self.eluat_id}, " \
               f"institution={self.institution}, " \
               f"isolate=Isolate({self.isolate.mads_isolatnr if self.isolate else ''}), " \
               f"sequencing_run=" \
               f"SequencingRun({self.sequencing_run.seq_run_id if self.sequencing_run else ''}), " \
               f"eluate=Eluate({self.eluate.eluat_id if self.eluate else ''}), " \
               f"assembly_readfiles=" \
               f"{sorted(f'AssemblyReadFiles({assembly_files.assembly_readfiles_id})' for assembly_files in self.assembly_readfiles)})"

    def to_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {
            "readfile_id": self.readfile_id,
            "read_type": self.read_type,
            "read_direction": self.read_direction,
            "mads_isolatnummer": self.mads_isolatnummer,
            "read_file": self.read_file,
            "batch_id": self.batch_id,
            "eluat_id": self.eluat_id,
            "institution": self.institution,
            "isolate": self.isolate,
            "sequencing_run": self.sequencing_run,
            "eluate": self.eluate,
            "assembly_readfiles": self.assembly_readfiles
        }

    def to_property_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary, removing relationships."""
        return {
            "readfile_id": self.readfile_id,
            "read_type": self.read_type,
            "read_direction": self.read_direction,
            "mads_isolatnummer": self.mads_isolatnummer,
            "read_file": self.read_file,
            "batch_id": self.batch_id,
            "eluat_id": self.eluat_id,
            "institution": self.institution
        }


# assembly_readfiles: needs ID for the combination of reads, the combination of reads itself
# (take from FOFN!) - IDs for forward, reverse and long reads
class AssemblyReadFiles(ComparisonMixin, Base):
    """A combination of read files for an assembly.

    Attributes:
        assembly_readfiles_id:  the database ID of the read file combination for the assembly
        forward_readfile_id:    the database ID of the forward read file (if given)
        reverse_readfile_id:    the database ID of the reverse read file (if given)
        long_readfile_id:       the database ID of the long read file (if given)
        forward_read:           the forward read file (if given)
        reverse_read:           the reverse read file (if given)
        long_read:              the long read file (if given)
        assembly:               the assembly obtained from the reads
    """

    __tablename__ = "test_assembly_readfiles"

    assembly_readfiles_id = Column(Integer, primary_key=True)
    forward_readfile_id = Column(Integer, ForeignKey("test_readfiles.readfile_id"), nullable=True)
    reverse_readfile_id = Column(Integer, ForeignKey("test_readfiles.readfile_id"), nullable=True)
    long_readfile_id = Column(Integer, ForeignKey("test_readfiles.readfile_id"), nullable=True)

    forward_read = relationship("ReadFile", foreign_keys = [forward_readfile_id])
    reverse_read = relationship("ReadFile", foreign_keys = [reverse_readfile_id])
    long_read = relationship("ReadFile", foreign_keys = [long_readfile_id])
    # we might technically get multiple assemblies from the same set of read files?
    # Skesa vs spades vs...
    assembly = relationship("Assembly",
                            back_populates = "assembly_readfiles")  # type: List[Assembly]

    relationships = {"forward_read", "reverse_read", "long_read", "assembly"}

    def __repr__(self):
        return f"AssemblyReadFiles(assembly_readfiles_id={self.assembly_readfiles_id}, " \
               f"forward_readfile_id={self.forward_readfile_id}, " \
               f"reverse_readfile_id={self.reverse_readfile_id}, " \
               f"long_readfile_id={self.long_readfile_id}, " \
               f"assembly={sorted(f'Assembly({assembly.assembly_file})' for assembly in self.assembly)})"

    def to_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"assembly_readfiles_id": self.assembly_readfiles_id,
                "forward_readfile_id": self.forward_readfile_id,
                "reverse_readfile_id": self.reverse_readfile_id,
                "long_readfile_id": self.long_readfile_id,
                "forward_read": self.forward_read,
                "reverse_read": self.reverse_read,
                "long_read": self.long_read,
                "assembly": self.assembly,
                }

    def to_property_dict(self) -> Dict[str, Union[str, int, List]]:
        """Convert object to dictionary, removing relationships."""
        return {"assembly_readfiles_id": self.assembly_readfiles_id,
                "forward_readfile_id": self.forward_readfile_id,
                "reverse_readfile_id": self.reverse_readfile_id,
                "long_readfile_id": self.long_readfile_id
                }
        # TODO: override check_matches?


# assembly: needs assembly_readfiles, MADS sample and isolate number,
# assembly type (illumina, nanopore, hybrid), assembler (if shovill, give sub tool
# - just a simple logic setup where it's shovill/skesa if Illumina, flye if Nanopore,
# unicycler if hybrid?), pipeline version, paths to assembly file and directory (as on server),
# n50, maybe longest contig (but we don't report that?), amount of contigs, genome size, coverage,
# release (bool -> int, is recorded that way - check that it is!), analysis user, release user
# (result sheet needs a field to accommodate this!), timestamp, organism name from the pipeline
# (here we probably want to add this even if the pipeline fails - contamination etc), institution,
# request ID
class Assembly(ComparisonMixin, Base):
    """Assemblies produced by the RSYD-BASIC pipeline.

    Attributes:
        assembly_id:                the assembly's database ID
        mads_proevenr:              the assembly's parent sample's LIS number
        mads_isolatnr:              the LIS number of the isolate that was sequenced to yield
                                    the assembly
        db_assembly_readfiles_id:   the database ID of the read files used to create this assembly
        assembly_type:              the type of the assembly (Illumina, Nanopore or hybrid)
        assembler:                  the assembler used to create the assembly
        pipeline_version:           the version of the RSYD-BASIC pipeline used to create
                                    the assembly
        assembly_file:              the path to the file containing the assembly
        assembly_dir:               the directory containing the assembly file (and QC data)
        n50:                        the assembly's N50
        num_contigs:                the amount of contigs in the assembly
        genome_size:                the assembly's size
        coverage:                   the assembly's average coverage
        release:                    whether or not the result should be released
        analysis_user:              the user who started the analysis
        release_user:               the user (molecular biologist) who decided whether the result
                                    should be released
        timestamp:                  the date and time at which the assembly was added to the DB
        pipeline_organism_name:     the species prediction made by the pipeline
        institution:                the institution at which the assembly was performed
        request_id:                 the database ID of the sequencing request for the assembly
        assembly_readfiles:         the combination of read files that yielded this assembly
        seq_request:                the sequencing request for the assembly
    """

    __tablename__ = "test_assembly"

    assembly_id = Column(Integer, primary_key = True)
    mads_proevenr = Column(String)
    mads_isolatnr = Column(String, ForeignKey("test_isolate.mads_isolatnr"), nullable=False)
    db_assembly_readfiles_id = Column(Integer,
                                      ForeignKey("test_assembly_readfiles.assembly_readfiles_id"),
                                      nullable=False)
    assembly_type = Column(String)
    assembler = Column(String)
    pipeline_version = Column(String)
    assembly_file = Column(String)  # TODO: longtext
    assembly_dir = Column(String)  # TODO: longtext
    n50 = Column(Integer)
    num_contigs = Column(Integer)
    genome_size = Column(Integer)
    coverage = Column(Float)
    release = Column(Boolean)
    analysis_user = Column(String)
    release_user = Column(String)
    timestamp = Column(DateTime)
    pipeline_organism_name = Column(String)
    institution = Column(String)
    request_id = Column(Integer, ForeignKey("test_seq_request.request_id"))

    assembly_readfiles = relationship("AssemblyReadFiles",
                                      foreign_keys = [db_assembly_readfiles_id])
    seq_request = relationship("SeqRequest", back_populates="assembly")

    relationships = {"assembly_readfiles", "seq_request"}

    def __repr__(self):
        return f"Assembly(assembly_id={self.assembly_id}, " \
               f"mads_proevenr={self.mads_proevenr}, " \
               f"mads_isolatnr={self.mads_isolatnr}, " \
               f"db_assembly_readfiles_id={self.db_assembly_readfiles_id}, " \
               f"assembly_type={self.assembly_type}, " \
               f"assembler={self.assembler}, " \
               f"pipeline_version={self.pipeline_version}, " \
               f"assembly_file={self.assembly_file}, " \
               f"assembly_dir={self.assembly_dir}, " \
               f"n50={self.n50}, " \
               f"num_contigs={self.num_contigs}, " \
               f"genome_size={self.genome_size}, " \
               f"coverage={self.coverage}, " \
               f"release={self.release}, " \
               f"analysis_user={self.analysis_user}, " \
               f"release_user={self.release_user}, " \
               f"timestamp={self.timestamp}, " \
               f"pipeline_organism_name={self.pipeline_organism_name}, " \
               f"institution={self.institution}, " \
               f"request_id={self.request_id})"

    def to_dict(self) -> Dict[str, Any]:
        """Convert object to dictionary (without interfering with SQLAlchemy's dict operations)."""
        return {"assembly_id": self.assembly_id,
                "mads_proevenr": self.mads_proevenr,
                "mads_isolatnr": self.mads_isolatnr,
                "db_assembly_readfiles_id": self.db_assembly_readfiles_id,
                "assembly_type": self.assembly_type,
                "assembler": self.assembler,
                "pipeline_version": self.pipeline_version,
                "assembly_file": self.assembly_file,
                "assembly_dir": self.assembly_dir,
                "n50": self.n50,
                "num_contigs": self.num_contigs,
                "genome_size": self.genome_size,
                "coverage": self.coverage,
                "release": self.release,
                "analysis_user": self.analysis_user,
                "release_user": self.release_user,
                "timestamp": self.timestamp,
                "pipeline_organism_name": self.pipeline_organism_name,
                "institution": self.institution,
                "request_id": self.request_id,
                "assembly_readfiles": self.assembly_readfiles,
                "seq_request": self.seq_request
                }

    def to_property_dict(self) -> Dict[str, Any]:
        """Convert object to dictionary, removing relationships."""
        return {"assembly_id": self.assembly_id,
                "mads_proevenr": self.mads_proevenr,
                "mads_isolatnr": self.mads_isolatnr,
                "db_assembly_readfiles_id": self.db_assembly_readfiles_id,
                "assembly_type": self.assembly_type,
                "assembler": self.assembler,
                "pipeline_version": self.pipeline_version,
                "assembly_file": self.assembly_file,
                "assembly_dir": self.assembly_dir,
                "n50": self.n50,
                "num_contigs": self.num_contigs,
                "genome_size": self.genome_size,
                "coverage": self.coverage,
                "release": self.release,
                "analysis_user": self.analysis_user,
                "release_user": self.release_user,
                "timestamp": self.timestamp,
                "pipeline_organism_name": self.pipeline_organism_name,
                "institution": self.institution,
                "request_id": self.request_id
                }
