# KMA Snaketopia: bacterial isolate analysis

This is a pipeline for analysis of bacterial isolates sequenced with Illumina (Nanopore and hybrid
sequence support is currently very experimental). The pipeline started as a Snakemake implementation
of [Bactopia](https://bactopia.github.io/v2.1.1/), but is being continuously developed to adapt it 
for use in a (Danish) clinical context. \
The current maintainer is Kat Steinke at KMA Odense.

## Prerequisites
The pipeline currently only runs as a Nomad job in a predefined container. Conda environments for 
rules in the Snakefile as well as for starting the workflow itself exist in the base container.

The pipeline requires mamba (recommended) or conda to manage environments.

## Setting up the pipeline
The pipeline can be obtained by simply cloning the repository:
```shell
git clone https://gitlab.com/KatSteinke/rsyd-basic.git
```
To update, run `git pull`.

The pipeline can additionally be configured to match local parameters (storage locations, sample number formats,
...), as described below.

### Installing the base environment
The base environment for launching the pipeline is defined as `base_env.yml`. This file can be used
to create the base environment using mamba or conda. Navigate to the directory containing this 
repository for the next step. 

#### Using mamba
```
mamba env create --file envs/base_env.yml
```

#### Using conda
```
conda env create --file envs/base_env.yml
```
### Configuring the pipeline
A default config file with placeholders is provided as `pipeline_routine.yaml`; replace 
the placeholders and you're ready to go. For testing etc., it is recommended to create separate 
config files. 

#### Language settings
The pipeline supports both Danish and English output. Output language is specified in the 
pipeline config file under the `language` parameter. Specify `da` for Danish output and `en` for 
English output. 

#### Input file settings
Expected column names for the input files (see below) can be configured flexibly. 
A template for the column names with explanations can be found under 
`config/input_settings/input_template.yaml`, and Danish and English examples are given
as `input_da.yaml` and `input_en.yaml` respectively.

The path to the input settings file can be specified in the config file as `input_names`.

#### Laboratory information system settings
Some of the features of this pipeline depend on external reports from a laboratory information system. Specifically, 
this is the case for:
* adding expected species information (name and size) to the summary file
* running species-specific analyses

If the LIS system uses a SQL database, the LIS report can be fetched directly. This requires setting
several parameters under the `lab_info_system` section of the config file:
* `dialect`: the SQL dialect to use to connect to the database
* `database`: the path to the database
* `db_table`: the table or view to access
* `schema`: the owner of the table or view to access, if this isn't the user used to access the database
* `filter_column`: a column to filter results by
* `filter_value`: the filter column's expected value for the samples which should be extracted from the LIS report


The pipeline will save (and thus overwrite) the results in the file given under `lis_report`.
If username and password are required for the database, they must be specified as environment 
variables `LIS_DB_USER` and `LIS_DB_PASS`.

If `dialect` and `database` fields are left empty, LIS data is read from the file given under
`lis_report`.

The pipeline was designed with OUH's MADS setup in mind. Report files therefore are 
assumed to
* be semicolon-separated
* be encoded using `latin-1` encoding
* contain fields for:
  * sample number (year and sample number as YYXXXXXX) - 
  the column name can be specified under `sample_number` in the `lis_columns` section of the input config
  * sample type (to be used as prefix to sample number) - to be specified under `sample_type` 
  in the `lis_columns` section of the input config
  * isolate number (between 0 and 9, as a suffix to sample number; one sample can have
  multiple isolates) - to be specified under `isolate_number` in the `lis_columns` section
  of the input config
  lab identification of bacteria encoded as numbers (for species-specific workflows) - 
  to be specified under `bacteria_code` in the `lis_columns` section of the input config
  * lab identification of bacteria as text - to be specified under `bacteria_text` in the `lis_columns` section of the input config

An example of a LIS report file can be found [here](docs/sample_mads.csv).  

For running species-specific workflows and providing an estimated genome size for the expected 
species, a "translation" from bacteria codes to taxonomic names is required. This file is assumed to
* be semicolon-separated
* be encoded using `latin-1` encoding
* contain the following fields:
  *  lab identification of bacteria encoded as numbers as in
  the main report - to be specified under `bacteria_code` in the `bact_translation` section of the input config
  * the corresponding internal text - to be specified under `internal_bacteria_text` in the 
  `bact_translation` section of the input config
  *  the encoded organisms' taxonomic name (genus or species) - to be specified under 
  `bacteria_category` in the `bact_translations` section of the input config

An example of a "translation" file can be found [here](docs/sample_categories.csv). 

Multiple bacteria codes may map to the same species (if e.g. code 1 stands for "E. coli" and code 2 
stands for "E. coli VTEC", both will be "Escherichia coli" in the `bacteria_category` field.)

If your laboratory information system differs, get in touch with this pipeline's maintainer(s) for help with
implementation - the idea is to make things as flexible as needed over time.

## Running the pipeline
The pipeline can be run in two modes: "classic" mode, primarily meant for users not 
comfortable with running commands on the commandline, and "commandline" mode, which 
offers greater control over the pipeline.
### Activating the environment
Before running the pipeline, the base environment needs to be activated. This is done by running
```shell
conda activate rsyd_basic
```

### Classic mode
Classic mode can be started by running 
```shell
python3 run_pipeline.py
```
without any options in the virtual environment containing all prerequisites. \
Classic mode guides the user through the process in a "questionnaire" style. \
As it is meant for routine use by non-expert users, some options cannot be changed in classic mode:
* **config file used**: the pipeline will use the default config file.
* **sequencing mode**: sequencing mode is locked to the one specified in the default config file
* **test vs routine mode**: in classic mode, only routine runs can be started. This means fasta and other files will 
automatically be copied to the respective files and directories if specified, and error messages are output in a less 
detailed format.
* **continuing a stopped run**: a stopped run cannot be continued in classic mode, only in commandline mode
### Commandline mode
Commandline mode inputs depend on the sequencing mode specified (either in the config file or in the
command itself). For runs only consisting of Illumina or Nanopore sequences, the minimal input is
```shell
python3 run_pipeline.py --{illumina, nanopore}_dir path/to/rundir --{illumina, nanopore}_sheet path/to/runsheet
```
(For runs with hybrid sequences, both Illumina and Nanopore runsheets and output directories will need to 
be specified. However, hybrid runs are not supported yet.) \
This runs the pipeline with the same default settings as in classic mode. Additionally, the 
following options can be specified:
* `--outdir`: output directory (also in classic mode)
* `--mode`: sequencing technology used (`illumina`, `nanopore`)
* `--continue_pipeline`: flag to specify continuing a previously stopped run
* `--test_run`: flag to specify a test run; files generated are not copied to directories containing data from all 
(routine) runs, and more detailed error messages are output.
* `--workflow_config_file`: configuration file with settings to use for current run

### Input formats
The pipeline requires the following inputs:
* Illumina or nanopore sequencing data in .fastq format (optionally compressed). For Illumina, 
one can either specify the directory containing the reads, or a "base" directory with the default
output structure of the MiSeq (`*/Alignment_*/*/Fastq`) or NextSeq (`*/Analysis/*/Data/fastq`)
depending on which device is given in the config. \
For Nanopore, one can either specify the `fastq_pass` directory containing all barcode directories, 
or a "base" directory with the default Nanopore output structure (`rawdata/*/fastq_pass/barcode*`).
* a "runsheet" with information about each sample in .xlsx format.
* and optionally:
  * a report from your laboratory information system, containing at least sample numbers, isolate 
  numbers and numeric codes for bacteria (see above under "Laboratory information system settings")

#### Minimal runsheet
As the pipeline was developed with the runsheets in use at KMA Odense in mind, the runsheet 
is expected to be an Excel sheet with at least three tabs (one of which depends on the sequencing 
mode). The names of the tabs and columns can be configured in the input config file.

* Information on the sample (tab name to be specified under `sample_info_sheet` in the 
`metadata_sheet` section of the input config):
  * sample number (column name specified under `sample_number` in the `metadata_sheet` section)
  * extraction well number (column name specified under `extraction_well_number`
  in the `metadata_sheet`section)
  * extraction run number (column name specified under `extraction_run_number`
  in the `metadata_sheet`section)
  * measured DNA concentration (column name specified under `dna_concentration` in the
  `metadata_sheet` section)
  * the person who requested the sequencing (column name specified under `requested_by`
  in the `metadata_sheet` section)
  * comments on the sequencing request (column name specified under `comment` in the
  `metadata_sheet` section)
  * sequencing run number (column name specified under `sequencing_run_number`
  in the `metadata_sheet` section)
  * as well as a list of indications for sequencing (with a boolean value for each in each sample to
  indicate whether this was the indication for sequencing; multiple indications are possible). 
  The list of available indications can be given under `indications`  in the `metadata_sheet`
  section of the input config. If toxin gene identification is to be performed, the corresponding 
  indication needs to contain the string "toxin".
* sheet used for calculations of sample dilution; name can be given under `dna_normalization_sheet` 
in the `metadata_sheet` section of the input config; required columns are
  *  sample number (column name to be given under `dna_normalization_sample_nr` in the
  `metadata_sheet` section as it may vary between sheets) 
  *  Illumina well position (column name to be given under `ilm_well_number` in the
  `metadata_sheet` section of the input config)
*  Illumina plate layout with the run name given in the third line - the sheet name is given under
`plate_layout` in the `metadata_sheet` section of the input config
* a "runsheet" for the sequencing method used, named in the format `[RUNSHEET_BASE]_[METHOD]`
  (where `[RUNSHEET_BASE]` is the prefix specified under `runsheet_base` in the `metadata_sheet`
section of the input config and `[METHOD]` is the sequencing method - "nanopore" for Nanopore and 
sequencer-specific names for Illumina): 
  * For Illumina, an optional template for an Illumina runsheet (using version 2 of the format),
    used to pass the wetlab protocol version into the pipeline
  * For Nanopore, a **required** table linking sample numbers to barcodes. Required columns:
    * experiment name. Needs to be in one of the first three columns of the
     second row of the sheet. Column name specified under `experiment_name`
    in the `metadata_sheet` section.
    * sample number: column A, starting in row 4; column name specified under 
    `nanopore_sheet_sample_number` in the `metadata_sheet` section
    * barcode used for the sample: column B, starting in row 4; column name specified under 
    `barcode` in the `metadata_sheet` section 

An example for an Illumina runsheet can be found [here](docs/minimal_runsheet.xlsx).

## Output
The pipeline outputs a directory with results for each sample that is analyzed, as well as
directories with combined results and two summary files. The structure is as follows:
```
+run_directory
+-sample_1
|   +-abritamr
|   +-annotation
|   +-assembly
|   |   +-quast
|   +-for_mads
|   +-kraken
|   +-logs
|   +-minmers
|   +-mlst
|   +-(nanopore_assembly)
|   |  +-(00-assembly)
|   |  +-(10-consensus)
|   |  +-(20-repeat)
|   |  +-(30-contigger)
|   |  +-(40-polishing)
|   +-(nanopore_reads)
|   +-plasmids
|   +-(quality-control)    # Illumina only
|   |   +-(summary)
|   +-(typing)             # only for selected species
|       +-(SeqSero)        # Salmonella
|       +-(serotypefinder) # E. coli
+-sample_2
|   +-abritamr
|   +-...
+-checkm_all
+-kraken_all
+-logs
+-quast_all
+-raw_results
+-read_stats
+ RUN_NAME_all_results.xlsx
+ RUN_NAME_mads_wide.xlsx
```
### Sample-level directories
Sample-level directories contain results for the individual sample. This includes:
* **abritamr**: Results from `abritamr` as well as the raw AMRFinder+ output before parsing
* **annotation**: A genbank file of the assembly, annotated with prokka
* **assembly**: For Illumina reads, a shovill assembly of the reads. Subdirectories are
  * **quast**: Sample-level assembly QC results with QUAST, including coverage metrics. A report 
  with visualizations can be accessed under `report.html`.
* **for_mads**: Analysis results for the sample, summarized for entry in a LIS system (if used)
* **kraken**: A summarized Kraken report for the sample (as obtained with kraken2's `--report` flag)
* **logs**: Log files for sample-level analysis steps
* **minmers**: mash and sourmash matches to RefSeq and Genbank databases for "quick and dirty" 
species identification and/or identification of contaminants
* **mlst**: Results of MLST typing using mlst
* **nanopore_assembly**: Nanopore only. Flye assembly results (including a .gfa file for graph 
visualization) and medaka consensus file. Subdirectories are those generated by flye.
* **nanopore_reads**: Nanopore only. Filtered, compressed reads. If any of the filtering steps 
should fail, the intermediate file will stay in this directory; otherwise, they are deleted
to save space.
* **quality-control**: Illumina only. Filtered, compressed reads. If any of the filtering steps should fail, the 
intermediate file will stay in this directory; otherwise, they are deleted to save space.
  * **summary**: FastQC and fastqscan reports on read quality before any cleaning steps and after 
  the last step
* **typing**: an optional directory, currently only output for *E. coli* and *Salmonella* samples.
This directory only contains subdirectories for species-specific analyses. Depending on species, 
this can be:
  * **SeqSero**: Serotyping results for *Salmonella*, using SeqSero2
  * **serotypefinder**: Serotyping results for *E. coli*, using serotypefinder
### Run level directories
* **checkm_all**: A summary of CheckM completeness/contamination results for all samples
* **kraken_all**: A summary of the "best" species and genus for each sample in Kraken2, determined
by percentage of reads matching. Percentage of reads matching is given as well.
* **logs**: Run-level logfiles:
  * **checkm.log**: CheckM's logfile
  * **snakemake.log**: Snakemake's logfile
  * **start_pipeline.log**: A detailed log for the pipeline's start script
* **quast_all**: QUAST QC results for the entire run, allowing comparison of all samples. However,
coverage statistics are not given in this report.
* **raw_results**: Backups of the summary files in .tsv format
* **read_stats**: Average coverage depth for each sample
### Summary files
* **RUN_NAME_all_results.xlsx**: A quality control-focused summary, containing
  * metadata on why sequencing was ordered (indication) and by whom
  * identification and comparison to laboratory-based information (if available), including expected
  genome sizes; for *E. coli* and *Salmonella*, serotype is given as well
  * assembly statistics: amount of contigs, GC content, N50, NG50, predicted genes
  * details on Kraken species and genus call (including % reads matching)
  * CheckM marker lineage, contamination and completeness calls
  * laboratory-related metadata: DNA concentration and preprocessing and sequencing well numbers
  * pipeline version
* **RUN_NAME_mads_wide.xlsx**: a summary of analysis results for all samples. Currently available 
results are:
  * identification: Kraken species call based on highest proportion of reads
  * MLST: MLST type using mlst
  * plasmids: plasmid Inc typing using PlasmidFinder
  * resistance genes in specified categories (defaults given under `data/amr/reduced_classes.csv`). 
  The default in use at KMA OUH is reporting
    * carbapenemases
    * other beta-lactamases
    * colistin resistance
    * vancomycin resistance
  * serotype or serovar (currently for *E. coli* and *Salmonella*)
  * supplementary type: currently subspecies for *Salmonella*
  * toxin genes: if requested, toxin genes from the list of toxin genes in `data/toxins/toxins_for_report.csv`


## Test dataset and expected results
A test dataset and expected results can be found [on Zenodo](https://zenodo.org/records/13881353).

## Tutorial
A tutorial showing how to run RSYD-BASIC in classic and commandline mode with the test dataset can be found [here](docs/tutorial.md). 

## Planned features
* additional modules specific to indications given on the runsheet
* local version with reduced resources


# License
The RSYD-BASIC pipeline is released under version 3 of the GNU General Public License;
a copy of the license's text can be found [here](licenses/RSYD_BASIC_GPL).
The Illumina filtering and assembly portion of the workflow as well as genome size estimation are originally
based on code from the Bactopia pipeline, which is licensed under an MIT license included[ here](licenses/BACTOPIA_LICENSE).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.