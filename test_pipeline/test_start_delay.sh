#!/usr/bin/env bash

mkdir -p run_test/rawdata/testdir/fastq_pass/barcode01
mkdir -p run_test/rawdata/testdir/fastq_pass/barcode42

python3 /snake_qc/run_pipeline.py --dry_run \
        --nanopore_sheet /snake_qc/test_pipeline/test_nanopore_runsheet.xlsx \
        --nanopore_dir run_test \
        --workflow_config_file /snake_qc/test_pipeline/pipeline_testjob_no_lis.yaml &> delayed_start_log
sleep 30
touch run_test/rawdata/testdir/final_summary_test.txt
sleep 30
cat delayed_start_log
grep -e "staging-snaketopia" delayed_start_log
