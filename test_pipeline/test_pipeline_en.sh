#!/usr/bin/env bash

SNAKETOPIA_BASENAME=$(basename "$1")

ALLOC_RESPONSE=$(nomad job dispatch \
                  -meta bactopia_names="/data/test_data/WGS_ISOLAT_UDVIKLING/test_data/nanopore/NANO_Run9999_Y20240301_XYZ_kort.tsv" \
                  -meta outdir="$1" \
                  -meta outdir_name="$SNAKETOPIA_BASENAME"\
                  -meta runsheet="/data/test_data/WGS_ISOLAT_UDVIKLING/test_data/nanopore/NANO_Run9999_Y20240301_XYZ_kort_en.xlsm" \
                  -meta node_dir="/alloc/data" \
                  -meta sequencing_dir="/share/instrumentdata/GridION_MT189944/NANO_isolat_Y20240223_RUN0105_MWH/rawdata/20240223_1251_X1_FAY19560_4d272d7a/fastq_pass" \
                  -meta use_lis="1" \
                  -meta result_lang="_en" \
                  staging-snaketopia "config/pipeline_testjob_en.yaml")
echo "$ALLOC_RESPONSE"
if [[ "$ALLOC_RESPONSE" == *"failed to place"* ]] ;
  then new_eval=$(echo "${ALLOC_RESPONSE}" | grep "waiting for additional capacity" | awk {'print $3'} | tr -d '"')
       ALLOC_ID=$(nomad eval status -monitor $new_eval | grep 'Allocation ' | awk {'print $3'} | tr -d '"')
  else
    ALLOC_ID=$(echo "${ALLOC_RESPONSE}" | grep 'Allocation ' | awk {'print $3'} | tr -d '"')
fi
echo "$ALLOC_ID"

# check for poststop test status
until $(nomad alloc status $ALLOC_ID | egrep -q 'Task "poststop_test" \(poststop\) is "dead"'); do
   sleep 30
   echo "Waiting for pipeline to finish ......................."

   alloc_replaced=$(nomad alloc status "$ALLOC_ID" | grep -E 'Replacement Alloc ID')
   if [ "$alloc_replaced" ] ;
      then ALLOC_ID=$(echo "$alloc_replaced" | awk {'print $5'});
   fi
done

# get poststop test status - output the section after poststop_test
EXIT_CODE=$(nomad alloc status $ALLOC_ID | grep -E --after-context=20 'Task "poststop_test" \(poststop\) is "dead"' | grep -E --max-count=1 'Terminated\s+Exit Code:' | awk {'print $5'} | tr -d ",")

exit "$EXIT_CODE"
