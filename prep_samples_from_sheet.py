"""Prepare file of filenames from Illumina and Nanopore sample sheets with additional checks"""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
import pathlib
import re

from typing import Any, Dict, Optional

import pandas as pd

import helpers
import input_names
import localization_helpers
import pipeline_config
from helpers import check_sample_sheet, check_nanopore_runsheet


# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging
logger = logging.getLogger("prepare_input_files")
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
console_log.setLevel(logging.WARNING)
logger.addHandler(console_log)

sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(workflow_config)


def get_samples_from_illumina_sheet(run_data: pathlib.Path, seq_dir: pathlib.Path,
                                    lis_report: Optional[pd.DataFrame] = None,
                                    species_coding: Optional[pd.DataFrame] = None,
                                    active_config: Dict[str, Any] = workflow_config,
                                    runsheet_names: input_names.RunsheetNames = sheet_names,
                                    bacteria_names: input_names.BacteriaMappingNames = bact_names,
                                    lis_data_names: input_names.LISDataNames = lis_names
                                    ) \
        -> pd.DataFrame:
    """Find read files corresponding to samples from Illumina sample sheet and set them up in
    the format required by Bactopia files-of-filenames
    (https://bactopia.github.io/usage-basic/#the-fofn-format).

    Arguments:
        run_data:           An Excel Illumina runsheet
        seq_dir:            the directory containing reads for the samples;
                            reads must be gathered in a single directory
        lis_report:         optional: LIS report with an additional column giving the isolate number
                            in a format matching the one in the runsheet
        species_coding:     optional: a translation of bacteria codes to taxonomic species names
                            (not just internal names, which may be on strain/serotype level)
        active_config:      the configuration to use
        runsheet_names:     Column names in the runsheet
        bacteria_names:     Column names in the code -> species name mapping
        lis_data_names:     Column names in LIS data

    Returns:
        The sample name, sample type, and read location for each sample in the sample sheet.
    """
    check_sample_sheet(run_data, lis_report, species_coding, active_config = active_config,
                       runsheet_names = runsheet_names, lis_data_names = lis_data_names,
                       bacteria_names = bacteria_names)
    # TODO: avoid repeat?
    data_sheet = pd.read_excel(run_data, sheet_name = runsheet_names.sample_info_sheet,
                               dtype = {runsheet_names.sample_number: str})
    data_sheet = data_sheet.dropna(subset = [runsheet_names.sample_number])
    # find files in seq dir
    names_with_files = pd.DataFrame()
    names_with_files["sample"] = data_sheet[runsheet_names.sample_number]
    # sequencing type will always be paired-end for Illumina runs
    names_with_files["runtype"] = "paired-end"
    # file names should be derived from sample number + read identifier + some Illumina fluff
    # TODO: improve finding illumina fluff
    # check if they exist - this will complain if they don't
    helpers.check_for_samples(seq_dir, data_sheet, runsheet_names = runsheet_names)
    names_with_files["r1"] = names_with_files['sample'].apply(lambda sample_name:
                                                              list(seq_dir.glob(f"{sample_name}_*"
                                                                                f"_R1_*.fastq.gz"))[0]
                                                              if list(seq_dir.glob(f"{sample_name}_*"
                                                                              f"_R1_*.fastq.gz"))
                                                              else pd.NA)
    names_with_files["r2"] = names_with_files['sample'].apply(lambda sample_name:
                                                              list(seq_dir.glob(f"{sample_name}_*"
                                                                                f"_R2_*.fastq.gz"))[0]
                                                              if list(seq_dir.glob(f"{sample_name}_*"
                                                                              f"_R2_*.fastq.gz"))
                                                              else pd.NA)
    # now we've found everything, we can translate the sample number
    from_format = re.compile(active_config["sample_number_settings"]["format_in_sheet"])
    to_format = re.compile(active_config["sample_number_settings"]["format_output"])
    translate_from = active_config["sample_number_settings"]["sample_numbers_in"]
    translate_to = active_config["sample_number_settings"]["sample_numbers_report"]
    mapping = active_config["sample_number_settings"]["number_to_letter"]
    names_with_files["sample"] = names_with_files['sample'].apply(lambda sample_name:
                                                                  helpers.rearrange_sample_number(
                                                                      sample_name,
                                                                      from_format,
                                                                      to_format.groupindex)
                                                                  )
    # translate start if needed
    names_with_files["sample"] = names_with_files["sample"].apply(lambda sample_nr:
                                                            helpers.translate_sample_category(
                                                                sample_nr,
                                                                to_format,
                                                                translate_from,
                                                                translate_to,
                                                                mapping))

    return names_with_files


def get_samples_from_nanopore_sheet(nanopore_sheet: pathlib.Path, seq_dir: pathlib.Path,
                                    lis_data: Optional[pd.DataFrame] = None,
                                    species_coding: Optional[pd.DataFrame] = None,
                                    active_config: Dict[str, Any] = workflow_config,
                                    runsheet_names: input_names.RunsheetNames = sheet_names,
                                    bacteria_names: input_names.BacteriaMappingNames = bact_names,
                                    lis_data_names: input_names.LISDataNames = lis_names) \
        -> pd.DataFrame:
    """Find read files corresponding to samples from Nanopore sample sheet and set them up in
        the format required by Bactopia files-of-filenames
        (https://bactopia.github.io/usage-basic/#the-fofn-format).

        Arguments:
            nanopore_sheet:     A Nanopore runsheet
            seq_dir:            the run's base directory
            lis_data:           optional: LIS report with an additional column giving the
                                isolate number
                                in a format matching the one in the runsheet
            species_coding:     optional: a translation of bacteria codes to taxonomic species names
                                (not just internal names, which may be on strain/serotype level)
            active_config:      the configuration to use
            runsheet_names:     Column names in the runsheet
            bacteria_names:     Column names in the code -> species name mapping
            lis_data_names:     Column names in LIS data



        Returns:
            The sample name, sample type, and location of the base directory for each sample in
            the sample sheet.
    """
    # see if we even have data where we expect it to be
    if not seq_dir.exists():
        raise FileNotFoundError("Run directory not found.")
    # check if we have everything
    check_sample_sheet(nanopore_sheet, lis_data, species_coding, active_config = active_config,
                       runsheet_names = runsheet_names, lis_data_names = lis_data_names,
                       bacteria_names = bacteria_names)
    # run nanopore-specific checks
    check_nanopore_runsheet(nanopore_sheet, runsheet_names = runsheet_names)
    nanopore_data = pd.read_excel(nanopore_sheet, usecols = "A:C", skiprows = 3,
                                  dtype = {runsheet_names.nanopore_sheet_sample_number: str,
                                           runsheet_names.barcode: str},
                                  sheet_name = f"{runsheet_names.runsheet_base}_Nanopore")
    nanopore_data = nanopore_data.dropna(subset=[runsheet_names.nanopore_sheet_sample_number])
    nanopore_data = nanopore_data.rename(columns={runsheet_names.nanopore_sheet_sample_number:
                                                      runsheet_names.sample_number})
    # check if the data is there
    passed_fastqs = helpers.get_fastq_pass_parent(seq_dir, nanopore_data,
                                               runsheet_names = runsheet_names) / "fastq_pass"
    # we can't tag the sample with barcodes as we may need to merge it with untagged Illumina
    # samples later
    nanopore_data["sample"] = nanopore_data[runsheet_names.sample_number]
    # we don't need to cross-check with file names here, so just rename everything
    from_format = re.compile(active_config["sample_number_settings"]["format_in_sheet"])
    to_format = re.compile(active_config["sample_number_settings"]["format_output"])
    translate_from = active_config["sample_number_settings"]["sample_numbers_in"]
    translate_to = active_config["sample_number_settings"]["sample_numbers_report"]
    mapping = active_config["sample_number_settings"]["number_to_letter"]
    nanopore_data["sample"] = nanopore_data['sample'].apply(
        lambda sample_name: helpers.rearrange_sample_number(sample_name,
                                                            from_format,
                                                            to_format.groupindex))
    # translate start if needed
    nanopore_data["sample"] = nanopore_data["sample"].apply(lambda sample_nr:
                                                            helpers.translate_sample_category(
                                                                              sample_nr,
                                                                              to_format,
                                                                              translate_from,
                                                                              translate_to,
                                                                              mapping))
    # nanopore names its folders after barcodes alone, so we'll get the barcode number from the
    # runsheet
    # TODO: smarter handling of barcode extraction? It'll *likely* always end in two digits
    nanopore_data["tmp_barcode"] = nanopore_data[runsheet_names.barcode].str.extract(r"(\d{2})$",
                                                                                     expand = False)
    nanopore_data["runtype"] = "ont"
    nanopore_data["extra"] = nanopore_data["tmp_barcode"].apply(lambda barcode:
                                                                passed_fastqs / f"barcode{barcode}")
    return nanopore_data[["sample", "runtype", "extra"]]


def merge_illumina_nanopore_samples(illumina_samples: pd.DataFrame, nanopore_samples: pd.DataFrame)\
        -> pd.DataFrame:
    """Merge Illumina and Nanopore files-of-filenames for hybrid runs, identify all hybrid samples
    as such and leave any remaining ones as Illumina or Nanopore only.

    Arguments:
        illumina_samples:   Sample name, sample type, and read location for all Illumina samples
        nanopore_samples:   Sample name, sample type, and read location for all Nanopore samples

    Returns:
        Sample name, sample type and read location(s) for all samples, with hybrid samples being
        marked as such and having read location information for both Illumina and Nanopore reads
    """
    all_reads = illumina_samples.merge(nanopore_samples, how="outer", on="sample",
                                       suffixes=("_illumina", "_nanopore"))
    # identify all samples that have reads for both Illumina and Nanopore -> hybrid reads
    all_reads["runtype"] = pd.NA
    all_reads.loc[all_reads["r1"].notna() & all_reads["extra"].notna(), ["runtype"]] = "hybrid"
    # fill run types for the rest - should be either in Illumina or Nanopore run types
    all_reads["runtype"] = all_reads["runtype"].fillna(all_reads["runtype_illumina"])
    all_reads["runtype"] = all_reads["runtype"].fillna(all_reads["runtype_nanopore"])
    illumina_only = list(all_reads.query("runtype == 'paired-end'")["sample"])
    nanopore_only = list(all_reads.query("runtype == 'ont'")["sample"])
    if illumina_only:
        print(illumina_only)
        logger.warning(f"Hybrid run specified but some samples only have Illumina reads: "
                       f"{', '.join(illumina_only)}")
    if nanopore_only:
        print(nanopore_only)
        logger.warning(f"Hybrid run specified but some samples only have Nanopore reads: "
                       f"{', '.join(nanopore_only)}")
    return all_reads[["sample", "runtype", "r1", "r2", "extra"]]
