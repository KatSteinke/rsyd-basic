"""Monitor the output directory of a specified sequencing run and
 start isolate analysis pipeline."""

__author__ = "Kat Steinke"

import logging
import pathlib
import re
import subprocess
import time
from datetime import timedelta

from typing import Any, Dict, Optional, List

import pandas as pd

import get_lis_report
import helpers
import localization_helpers
import pipeline_config
import version

from prep_samples_from_sheet import get_samples_from_illumina_sheet, get_samples_from_nanopore_sheet

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging
logger = logging.getLogger("launch_run")


# TODO: what happens when setting new runsheet? Should that be allowed?
class IsolateRun:
    """Parameters for an isolate sequencing run.

    Attributes:
        rundir_path:        the directory containing sequencing data
        runsheet_path:      the path to the runsheet (Illumina or Nanopore)
        sample_sheet:       the contents of the file of filenames for the pipeline
        file_of_filenames:  the path to the file of filenames for the pipeline
        configfile:         the file containing the configuration for the pipeline
        sequencing_time:    the expected duration of the sequencing run
        active_config:      the configuration to use for the pipeline
        outdir:             the directory to write results to
                            (defaults to base path + experiment name)
        test_run:           whether to run the pipeline in test mode (overrides config setting)
        seq_mode:           the sequencing mode (Nanopore or Illumina)
    """
    def __init__(self, rundir_path: pathlib.Path, runsheet_path: pathlib.Path,
                 sample_sheet: pd.DataFrame, configfile: pathlib.Path,
                 active_config: Dict[str, Any], outdir: Optional[pathlib.Path] = None,
                 sequencing_time: Optional[float] = None, test_run: Optional[bool] = None,
                 sequencing_mode: Optional[str] = None):
        self.rundir_path = rundir_path
        self.runsheet_path = runsheet_path
        # catch runsheet issues early since we need it for the sequencing dir
        if not runsheet_path.exists():
            raise FileNotFoundError(f"Could not find runsheet at {runsheet_path}")
        self.sample_sheet = sample_sheet
        self.configfile = configfile
        self.active_config = active_config
        seq_time = float(active_config['seq_run_duration_hours'])
        if sequencing_time:
            seq_time = float(sequencing_time)
        if seq_time < 0:
            raise ValueError("Expected sequencing time must be greater than 0 hours.")
        self.sequencing_time = timedelta(hours = seq_time)
        if test_run is not None:
            self.test_run = test_run
        else:
            self.test_run = active_config['debug']
        # TODO: lots of repetition
        self.seq_mode = active_config["sequencing_mode"]
        if sequencing_mode:
            self.seq_mode = sequencing_mode
        # get runsheet names for experiment name
        (sheet_names,
         lis_names,
         bact_names) = localization_helpers.load_input_from_config(active_config)
        if self.seq_mode == "illumina":
            experiment_name = helpers.construct_experiment_name(illumina_sheet = runsheet_path,
                                                                runsheet_names = sheet_names)
        elif self.seq_mode == "nanopore":
            experiment_name = helpers.construct_experiment_name(nanopore_sheet = runsheet_path,
                                                                runsheet_names = sheet_names)
        elif self.seq_mode == "hybrid":
            raise NotImplementedError("Hybrid sequencing is not yet implemented.")
        else:
            raise ValueError(f"Invalid sequencing mode {self.seq_mode}.")
        if not outdir:
            base_out_path = pathlib.Path(active_config["paths"]["output_base_path"])
            self.outdir = base_out_path / experiment_name
        else:
            self.outdir = outdir
        self.file_of_filenames = self.outdir / f"{experiment_name}.tsv"

    def __repr__(self):
        return (f"IsolateRun(rundir_path={self.rundir_path}, runsheet_path={self.runsheet_path},"
                f" file_of_filenames={self.file_of_filenames}, configfile={self.configfile},"
                f" active_config={self.active_config}, test_run={self.test_run}, "
                f"sequencing_time={self.sequencing_time}, outdir={self.outdir},"
                f" seq_mode={self.seq_mode})\n"
                f"Sample sheet:\n{self.sample_sheet.to_string()}")

    def __eq__(self, other):
        if isinstance(other, IsolateRun):
            return (self.rundir_path == other.rundir_path
                    and self.runsheet_path == other.runsheet_path
                    and self.sample_sheet.equals(other.sample_sheet)
                    and self.file_of_filenames == other.file_of_filenames
                    and self.configfile == other.configfile
                    and self.active_config == other.active_config
                    and self.sequencing_time == other.sequencing_time
                    and self.outdir == other.outdir
                    and self.test_run == other.test_run
                    and self.seq_mode == other.seq_mode)
        return False

    def __ne__(self, other):
        if isinstance(other, IsolateRun):
            return (~(self.rundir_path == other.rundir_path
                      and self.runsheet_path == other.runsheet_path
                      and self.sample_sheet.equals(other.sample_sheet)
                      and self.file_of_filenames == other.file_of_filenames
                      and self.configfile == other.configfile
                      and self.active_config == other.active_config
                      and self.sequencing_time == other.sequencing_time
                      and self.outdir == other.outdir
                      and self.test_run == other.test_run
                      and self.seq_mode == other.seq_mode))
        return True

    def set_outdir(self, outdir: pathlib.Path) -> None:
        """Set a new output directory and adjust the file of filenames accordingly

        Arguments:
            outdir: the new output directory

        """
        self.outdir = outdir
        fofn_name = self.file_of_filenames.name
        self.file_of_filenames = outdir / fofn_name


# TODO: feed all this to that function or move it out to a setup function?
# TODO use language settings
def get_run_input_by_seq_mode(sequence_dir: pathlib.Path, outdir: Optional[pathlib.Path] = None,
                              illumina_runsheet: Optional[pathlib.Path] = None,
                              nanopore_runsheet: Optional[pathlib.Path] = None,
                              sequencing_time: Optional[float] = None,
                              active_config: Dict[str, Any] = workflow_config,
                              config_path: pathlib.Path = default_config_file,
                              test_run: Optional[bool] = None) -> IsolateRun:
    """Get all parameters required to start the pipeline, handling Nanopore and Illumina data
     as required.

    Arguments:
        sequence_dir:       the directory containing sequencing data
        outdir:             the directory to write results to
        illumina_runsheet:  the path to the Illumina runsheet (if given)
        nanopore_runsheet:  the path to the Nanopore runsheet (if given)
        sequencing_time:    the expected duration of the sequencing run
        active_config:      the configuration to use
        config_path:        the path to the config file to use
        test_run:           whether to run the pipeline in test mode (overrides config setting)

    Returns:
        Samples structured for a file of filenames, as well as paths to sequencing directory
        and runsheet for the run.

    Raises:
        NotImplementedError:    when both Illumina and Nanopore data are supplied -
                                hybrid runs are not supported yet
        ValueError:             when neither Illumina nor Nanopore data are supplied
                                or an invalid sequencing time is supplied
    """
    # we can't handle hybrid yet
    if illumina_runsheet and nanopore_runsheet:
        raise NotImplementedError("Hybrid sequencing analysis is currently not implemented.")
    # load language settings from config
    sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(active_config)
    # check if sequencing time was given - todo: check valid value here when it gets checked later anyway?
    seq_time = float(active_config['seq_run_duration_hours'])
    if sequencing_time:
        seq_time = float(sequencing_time)
    bacteria_codes = None
    data_from_lis = None
    # read LIS data if we need it
    if active_config["lab_info_system"]["use_lis_features"]:
        # access bacteria code translations
        bacteria_codes = pd.read_csv(active_config['lab_info_system']['bacteria_codes'],
                                     sep = ";",
                                     encoding = "latin-1",
                                     dtype = {bact_names.bacteria_code: str})
        lis_report_path = active_config["lab_info_system"]["lis_report"]
        data_from_lis = get_lis_report.get_report_with_isolate_number(lis_report_path,
                                                               active_config[
                                                                   "sample_number_settings"][
                                                                   "number_to_letter"],
                                                               translate_from =
                                                               active_config[
                                                                   "sample_number_settings"][
                                                                   "sample_numbers_out"],
                                                               translate_to =
                                                               active_config[
                                                                   "sample_number_settings"][
                                                                   "sample_numbers_in"],
                                                               from_format = re.compile(
                                                                   active_config[
                                                                       "sample_number_settings"][
                                                                       "format_in_lis"]),
                                                               to_format = re.compile(
                                                                   active_config[
                                                                       "sample_number_settings"][
                                                                       "format_in_sheet"]),
                                                               lis_data_names = lis_names
                                                               )
    if illumina_runsheet:
        run_samples = get_samples_from_illumina_sheet(illumina_runsheet,
                                                      sequence_dir,
                                                      data_from_lis,
                                                      bacteria_codes,
                                                      active_config = active_config,
                                                      runsheet_names = sheet_names,
                                                      lis_data_names = lis_names,
                                                      bacteria_names = bact_names)
        # fill the missing columns

        runsheet_for_run = illumina_runsheet
        # if we've been given an Illumina sheet we can expect the sequencing mode to be Illumina
        seq_mode = "illumina"
    elif nanopore_runsheet:
        run_samples = get_samples_from_nanopore_sheet(nanopore_runsheet,
                                                      sequence_dir,
                                                      data_from_lis,
                                                      bacteria_codes,
                                                      active_config = active_config,
                                                      runsheet_names = sheet_names,
                                                      lis_data_names = lis_names,
                                                      bacteria_names = bact_names)

        runsheet_for_run = nanopore_runsheet
        # if we've been given a Nanopore sheet we can expect the sequencing mode to be Nanopore
        seq_mode = "nanopore"
    # if we find neither, complain
    else:
        raise ValueError("One of illumina_runsheet or nanopore_runsheet needs to be supplied "
                         "to start analysis.")
    # sanity check sequencing mode against what's in the config
    if seq_mode != active_config["sequencing_mode"]:
        logger.warning(f"{seq_mode.capitalize()} runsheet given "
                       f"but config expects {active_config['sequencing_mode']}. "
                       "Check that you are using the correct config file.")

    # fill the missing columns
    run_samples = run_samples.reindex(columns=["sample", "runtype", "r1", "r2", "extra"],
                                      fill_value = "")
    return IsolateRun(rundir_path = sequence_dir, runsheet_path = runsheet_for_run,
                      sample_sheet = run_samples, configfile = config_path,
                      active_config = active_config, outdir = outdir, sequencing_mode = seq_mode,
                      sequencing_time = seq_time,
                      test_run = test_run)


# TODO: match get_pipeline_command from 16S
def get_nomad_command(params_for_run: IsolateRun) -> List[str]:
    """Generate a Nomad command to start a job with the parameters supplied by the user.

    Arguments:
        params_for_run:     input parameters for the run: samples and paths to run dir and runsheet

    Returns:
        A nomad command to start a job with the requested parameters
    """
    outdir = params_for_run.outdir
    runsheet = params_for_run.runsheet_path
    sequence_dir = params_for_run.rundir_path
    pipeline_inputs = params_for_run.file_of_filenames
    active_config = params_for_run.active_config
    config_file_path = params_for_run.configfile
    debug_mode = params_for_run.test_run
    # sanity check paths - TODO: do we need runsheet?
    names_to_paths = {"file of filenames": pipeline_inputs, "runsheet": runsheet,
                      "run directory": sequence_dir}
    for file_name, file_path in names_to_paths.items():
        if not file_path.exists():
            raise FileNotFoundError(f"Could not find {file_name} at {file_path}")
    # override debug if needed
    if debug_mode is None:
        run_as_debug = active_config['debug']
    else:
        run_as_debug = debug_mode
    job_name = "staging-snaketopia" if run_as_debug else "prod-snaketopia"
    # TODO: ensure that config specified by file path is the same as active config?
    start_nomad = ["nomad", "job", "dispatch",
                   "-meta", f"bactopia_names={pipeline_inputs}",
                   "-meta", f'outdir={str(outdir)}',
                   "-meta", f"outdir_name={outdir.name}",
                   "-meta", f'node_dir={active_config["temp_data_dir"]}',
                   "-meta",
                   f"use_lis={int(active_config['lab_info_system']['use_lis_features'])}",
                   "-meta", f"runsheet={runsheet}",
                   "-meta", f"sequencing_dir={sequence_dir}",
                   job_name,
                   str(config_file_path)]
    return start_nomad


def get_docker_command(params_for_run: IsolateRun) -> List[str]:
    """Generate a Docker command to start a job with the parameters supplied by the user.

    Arguments:
        params_for_run:     input parameters for the run: samples and paths to run dir and runsheet

    Returns:
        A Docker command to start the pipeline with the requested parameters
    """
    outdir = params_for_run.outdir
    runsheet = params_for_run.runsheet_path
    sequence_dir = params_for_run.rundir_path
    pipeline_inputs = params_for_run.file_of_filenames
    active_config = params_for_run.active_config
    config_file_path = params_for_run.configfile
    # sanity check paths
    names_to_paths = {"file of filenames": pipeline_inputs, "runsheet": runsheet,
                      "run directory": sequence_dir}
    for file_name, file_path in names_to_paths.items():
        if not file_path.exists():
            raise FileNotFoundError(f"Could not find {file_name} at {file_path}")

    docker_cmd = ["docker", "run",
                  # mount all relevant files and dirs
                  "-v", f"{str(runsheet)}:{str(runsheet)}",
                  "-v", f"{str(outdir)}:{str(outdir)}",
                  "-v", f"{config_file_path}:{config_file_path}",
                  "-v", f"{pipeline_inputs}:{pipeline_inputs}",
                  "-v", f"{sequence_dir}:{sequence_dir}",
                  "-v", f"{active_config['input_names']}:{active_config['input_names']}"]
    # get paths in bulk
    database_mounts = []
    for local_path in active_config["paths"]:
        database_mounts.extend(["-v",
                                f"{active_config['paths'][local_path]}:"
                                f"{active_config['paths'][local_path]}"])

    docker_cmd.extend(database_mounts)  # TODO: can we do this in a reduced setup?

    # add LIS report if used
    if active_config["lab_info_system"]["use_lis_features"]:
        docker_cmd.extend(["-v",
                           f"{active_config['lab_info_system']['lis_report']}:"
                           f"{active_config['lab_info_system']['lis_report']}",
                           "-v",
                           f"{active_config['lab_info_system']['bacteria_codes']}:"
                           f"{active_config['lab_info_system']['bacteria_codes']}"
                           ])
    # add image
    docker_cmd.append("rsyd-basic-docker")
    # add config options
    config_opts = ["--cores", f'{active_config["cores"]}',
                   "--config",
                   f'runsheet="{str(runsheet)}"',
                   f'rundir="{str(sequence_dir)}"',
                   f'outdir="{str(outdir)}"',
                   f'config_path="{config_file_path}"',
                   f'input_samples="{pipeline_inputs}"',
                   "--configfile", str(config_file_path),
                   "-r",
                   "--rerun-incomplete",
                   "--rerun-triggers", "input",
                   "--shadow-prefix", "/bactopia_datasets",
                   ]
    docker_cmd.extend(config_opts)
    # TODO: get resources?
    return docker_cmd


def start_run_by_mode(isolate_run: IsolateRun,
                      dry_run: bool = False) -> subprocess.CompletedProcess:
    """Start the analysis pipeline for a given isolate run,
    on Nomad or locally as specified in the run's config.

    Arguments:
        isolate_run:    the run for which the analysis pipeline should be started
        dry_run:        whether or not to only print the pipeline command

    Returns:
        The process that launches the pipeline

    Raises:
        ValueError: if the run mode is invalid (not local or nomad)
    """
    pipeline_mode = isolate_run.active_config["run_on"]
    if pipeline_mode == "nomad":
        pipeline_command = get_nomad_command(isolate_run)
    elif pipeline_mode == "local":
        pipeline_command = get_docker_command(isolate_run)
    else:
        raise ValueError(f"Invalid run mode {pipeline_mode}")
    logger.info("Running analysis pipeline")
    logger.info(f"Starting pipeline in {pipeline_mode} mode.")
    # return only string if in test mode
    if dry_run:
        pipeline_command_text = " ".join(pipeline_command)
        return subprocess.run(["echo", f'"{pipeline_command_text}"'])
    return subprocess.run(pipeline_command)


def start_on_file_found(run_to_watch: IsolateRun, pattern_to_watch: str, dry_run: bool = False,
                        watch_interval: int = 300, watch_timeout: int = 600,
                        log_interval: int = 300) -> subprocess.CompletedProcess:
    """Start the analysis pipeline when a given file is found in the specified Nanopore run's
     sequencing dir, on Nomad or locally as specified in the run's config.

    Arguments:
        run_to_watch:       the run for which the analysis pipeline should be started
        pattern_to_watch:   the file pattern to watch for
        dry_run:            whether or not to only print the pipeline command
        watch_interval:     the interval (in seconds) in which the script should check for
                            the presence of the file
        watch_timeout:      the timespan (in seconds) to wait for the file
        log_interval:       the interval (in seconds) in which the script should log activity


    Returns:
        The process that launches the pipeline
    Raises:
        FileNotFoundError:      if the file is not found before the timeout
        NotImplementedError:    if the run is not a Nanopore run
    """
    seconds_per_hour = 3600
    # TODO: what to wait for for Illumina?
    if run_to_watch.active_config["sequencing_mode"] != "nanopore":
        raise NotImplementedError("Delayed start is currently only implemented for Nanopore runs.")
    file_found = 0
    time_watching = 0
    time_since_log = 0
    if not run_to_watch.rundir_path.exists():
        raise FileNotFoundError(f"Parent directory {run_to_watch.rundir_path} does not exist.")
    # read language settings
    (sheet_names,
     lis_names,
     bact_names) = localization_helpers.load_input_from_config(run_to_watch.active_config)

    nanopore_sheet_data = pd.read_excel(run_to_watch.runsheet_path, usecols = "A:C", skiprows = 3,
                                        dtype = {sheet_names.sample_number: str,
                                                 sheet_names.barcode: str},
                                        sheet_name = f"{sheet_names.runsheet_base}_Nanopore")
    # TODO: this breaks when we started by specifying a fastq dir in run_pipeline - get the parent from the sheet?
    fastq_parent_dir = helpers.get_fastq_pass_parent(run_to_watch.rundir_path, nanopore_sheet_data,
                                                     runsheet_names = sheet_names)
    logger.info("Pipeline is now waiting for sequencing to finish...")
    # watch for presence of file
    while not (file_found or time_watching >= watch_timeout):
        # keep a log so we know it's alive
        time_since_log += watch_interval
        if time_since_log >= log_interval:
            logger.info("Waiting for sequencing to finish...")
            time_since_log = 0
        file_found = len(list(fastq_parent_dir.glob(pattern_to_watch)))
        if not file_found:  # TODO: we can definitely make this flow more nicely
            time_watching += watch_interval
            time.sleep(watch_interval)
    if not file_found:
        timeout_hours = watch_timeout / seconds_per_hour
        logger.error(f"No file matching pattern {pattern_to_watch} found in {fastq_parent_dir} "
                     f"after {timeout_hours:.1f} hours.")
        raise FileNotFoundError(f"No file matching pattern {pattern_to_watch} "
                                f"found in {fastq_parent_dir}.")
    logger.info(f"Found {pattern_to_watch} in {fastq_parent_dir} after {time_watching} seconds.")
    # get command
    return start_run_by_mode(run_to_watch, dry_run = dry_run)
