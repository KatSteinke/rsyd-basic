"""Start Snaketopia pipeline from Illumina or Nanopore sample sheet"""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import argparse
import logging
import os
import pathlib
import re
import readline
import subprocess
import sys
import warnings

from argparse import ArgumentParser
from datetime import timedelta
from typing import Any, Dict, Optional, List

import pandas as pd
import yaml

import get_lis_report
import helpers
import input_names
import localization_helpers
import monitor_run
import pipeline_config
import set_log

from helpers import BadPathError

# import parameters
DEFAULT_CONFIG_FILE = pipeline_config.default_config_file
WORKFLOW_CONFIG = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging - TODO: extend?
logging.captureWarnings(True)
logger = logging.getLogger("run_pipeline")

SHEET_NAMES, LIS_NAMES, BACT_NAMES = localization_helpers.load_input_from_config(WORKFLOW_CONFIG)


def get_existing_path(path_to_check: pathlib.Path) -> pathlib.Path:
    """Recursively check if path exists, else go down one level until an existing path is found.
    Adapted from https://stackoverflow.com/a/39489505

    Arguments:
        path_to_check:  Path whose components should be checked

    Returns:
        The existing parts of the path
    """
    if path_to_check.exists():
        return path_to_check
    return get_existing_path(path_to_check.parent)


def get_clean_outdir(outdir_path: pathlib.Path) -> pathlib.Path:
    """Check which parts of a path already exist and sanitize the new ones by removing spaces
    and special characters if needed.

    Arguments:
        outdir_path:    Path to check for spaces and special characters

    Returns:
         The sanitized version of the path
    """
    illegal_in_windows = r'[<>:"|?*]'
    # find out until which point path exists
    existing_path = get_existing_path(outdir_path)
    # for anything up to that, if it contains a "bad" character or a space, fail immediately
    if " " in str(existing_path):
        raise BadPathError("The path you are trying to save results to contains a space "
                           "in an existing folder's name. This can break the pipeline. "
                           "\nAborting....")
    if re.search(illegal_in_windows, str(existing_path)):
        raise BadPathError("The path you are trying to save results to contains a character that "
                           "can't be used in Windows in an existing folder's name. "
                           "This can break the pipeline. "
                           "\nAborting....")
    if existing_path == outdir_path:
        return existing_path
    # get rest of the path: relative to existing path
    new_path = outdir_path.relative_to(existing_path)
    # for the new part of the filename:
    # otherwise remove the "illegal" characters and substitute spaces with underscores
    # TODO: any way to use pathlib for this?
    plain_path = str(new_path)
    plain_path = plain_path.replace(" ", "_")
    plain_path = re.sub(illegal_in_windows, "", plain_path)
    cleaned_path = existing_path / plain_path
    # if the path contains a device name, stop and complain
    # if device_names.intersection(set(new_path.parts)):
    if pathlib.PureWindowsPath(cleaned_path).is_reserved():
        raise BadPathError("The path you are trying to save results to contains a name that is "
                           "reserved in Windows. Cannot create this path. \n"
                           "Aborting....")
    return cleaned_path


# find Illumina path:
def find_illumina_path(illumina_path: pathlib.Path, illumina_run_sheet: pd.DataFrame,
                       sequencer: str = WORKFLOW_CONFIG["platform"],
                       runsheet_names: input_names.RunsheetNames = SHEET_NAMES) \
        -> pathlib.Path:
    """Check if the supplied path contains all Illumina samples; if not, try to find the directory
    containing the samples based on the specified (Illumina) sequencer.

    Arguments:
        illumina_path:      Path to check for samples
        illumina_run_sheet: Sample information from an Excel Illumina runsheet
        sequencer:          Sequencing platform used to generate reads
        runsheet_names:     Column names in the runsheet

    Returns:
        The directory containing all samples, if this exists

    Raises:
        MissingReadfilesError:  if a required directory and/or reads are missing
        ValueError:             if an invalid platform is given
                                or the directory with all samples cannot be found unambiguously
    """
    # check if the platform is Illumina
    if sequencer not in {"MiSeq", "NextSeq"}:
        raise ValueError(f"Cannot look for Illumina reads on sequencing platform {sequencer}.")

    logger.info("Checking for samples in specified Illumina path.")
    # are all samples we'd expect from the runsheet there?
    try:
        helpers.check_for_samples(illumina_path, illumina_run_sheet,
                                  runsheet_names = runsheet_names)
        # if yes, we're already where we need to be
        return illumina_path
    # if not:
    except helpers.MissingReadfilesError as missing_sample_err:
        # if some are present (not all samples are in the list of missing samples)
        # note what is missing and complain
        if (set(illumina_run_sheet[runsheet_names.sample_number])
                - set(missing_sample_err.missing_r1)
                or set(illumina_run_sheet[runsheet_names.sample_number])
                - set(missing_sample_err.missing_r2)):
            raise missing_sample_err
        # if none are present (all samples also in missing samples)
        # then try and follow the Illumina directory structure
        # if this is MiSeq data is in */Alignment_*/*/Fastq
        # if it's NextSeq it's */Analysis/*/Data/fastq
        if sequencer == "MiSeq":
            analysis_base = "*/Alignment_*"
            fastq_path = "*/Fastq"
            required_dir_name = "Alignment"
        else:
            analysis_base = "*/Analysis/*/Data"
            fastq_path = "fastq"
            required_dir_name = "Analysis"
        missing_r1 = missing_sample_err.missing_r1
        missing_r2 = missing_sample_err.missing_r2
        logger.warning(f"No samples found in {illumina_path}. "
                       f"Treating this as a base directory: "
                       f"looking for samples in {analysis_base}/{fastq_path}.")

    # one subdir in, do we have exactly one dir named Alignment_n or Analysis/n?
    result_base_dirs = list(illumina_path.glob(analysis_base))
    # if we have none, complain with everything that is missing
    if not result_base_dirs:
        error_message = f"No {required_dir_name} directory found in {illumina_path}. \n"
        if missing_r1:
            error_message += f"R1 not found in {illumina_path} for samples {missing_r1}. \n"
        if missing_r2:
            error_message += f"R2 not found in {illumina_path} for samples {missing_r2}. \n"
        raise helpers.MissingReadfilesError(error_message, missing_r1 = missing_r1,
                                            missing_r2 = missing_r2)
    # if we have too many, complain and tell the user to specify the entire path down to fastq
    if len(result_base_dirs) > 1:
        raise ValueError(f"The directory you have specified contains "
                         f"multiple {required_dir_name} directories."
                         "Please choose the one containing the fastq files you want to analyze and"
                         "specify the entire path to the directory with the fastq files.")
    #   TODO: or only to the alignment dir?
    # if we have one, follow that one through */Fastq
    result_dir = result_base_dirs[0]
    fastq_dirs = list(result_dir.glob(fastq_path))
    if not fastq_dirs:
        error_message = f"No fastq directory found in {result_dir}. \n"
        if missing_r1:
            error_message += f"R1 not found in {illumina_path} for samples {missing_r1}. \n"
        if missing_r2:
            error_message += f"R2 not found in {illumina_path} for samples {missing_r2}. \n"
        raise helpers.MissingReadfilesError(error_message, missing_r1 = missing_r1,
                                            missing_r2 = missing_r2)
    fastq_dir = fastq_dirs[0]
    # check if we have the samples there
    helpers.check_for_samples(fastq_dir, illumina_run_sheet)
    # if we don't, it'll have complained - if it hasn't, we have samples in the fastq directory
    return fastq_dir


def ask_output_dir(output_dir_path: pathlib.Path) -> pathlib.Path:
    """Ask the user to confirm the default output directory or define a new one.

    Arguments:
        output_dir_path:    the suggested output directory

    Returns:
        The output directory to use for the run

    Raises:
        BadPathError:   if the path that was entered would break on a Windows file system
        ValueError:     if an invalid response is entered
        """
    try:
        # check if this would break anything in Windows or contains spaces
        output_dir_path = get_clean_outdir(output_dir_path)
        # if it's good, ask user for confirmation
        target_accept = input(f"Do you accept {str(output_dir_path)} as target folder [y/n]")
        if target_accept == "n":
            output_dir_path = pathlib.Path(
                input("Type full path or name of target folder and press "
                      "enter: ").strip().strip("'"))
        elif target_accept == "y":  # TODO: can we handle this more nicely?
            pass
        else:
            raise ValueError("Output folder not entered. Aborting")
    except BadPathError:
        logger.warning("The default target folder contains characters that can break the pipeline.")
        output_dir_path = pathlib.Path(input("Type full path to new target folder "
                                             "and press enter: ").strip().strip("'"))
    # if something still is broken now, we yell at the user and fail
    output_dir_path = get_clean_outdir(output_dir_path)
    logger.info(f"Saving results to {output_dir_path}")
    return output_dir_path


def set_up_outputs(current_run: monitor_run.IsolateRun,
                   continue_run: Optional[bool] = False) -> None:
    """Set up output directories for the pipeline

    Arguments:
        current_run:    The run for which to set up output dirs
        continue_run:   whether to continue the pipeline from a stopped run

    Raises:
        FileExistsError:   if a file of filenames already exists in the output directory
                           and the run is not a continuation
    """
    outdir = current_run.outdir
    if not outdir.exists():
        outdir.mkdir(parents=True)
    # create dir for snakemake logs
    if not (outdir / "logs").exists():
        (outdir / "logs").mkdir()

    # complain if we haven't said to continue the pipeline
    if current_run.file_of_filenames.exists():
        if not continue_run:
            raise FileExistsError("File of filenames already found.")
        logger.info("Output directory already exists. Continuing run...")
        # TODO: skip rewriting in this case?

    current_run.sample_sheet.to_csv(path_or_buf=current_run.file_of_filenames, sep= "\t",
                                    index=False)


# TODO use language settings - this one can load from the config
def initialize_illumina_run(active_config: Dict[str, Any] = WORKFLOW_CONFIG,
                            config_path: pathlib.Path = DEFAULT_CONFIG_FILE) \
        -> monitor_run.IsolateRun:
    """Initialize an Illumina IsolateRun from user input.

    Arguments:
        active_config:  the config to use
        config_path:    the path to the configfile in use

    Returns:
        The Illumina run, optionally with user-supplied settings for output.
    """
    ilm_dir = input("Type full path or name of Illumina sequencing folder and press enter: ")
    ilm_dir = pathlib.Path((ilm_dir.strip().strip("'"))).resolve()
    ilm_sheet = input("Output directory will be based on experiment name."
                      "\n"
                      "Enter path to Illumina runsheet: ")
    ilm_sheet = pathlib.Path((ilm_sheet.strip().strip("'"))).resolve()
    # figure out if we've been given the whole path or just the start
    # load language settings
    (runsheet_names,
     lis_data_names,
     bacteria_names) = localization_helpers.load_input_from_config(active_config)

    ilm_sheet_data = pd.read_excel(ilm_sheet, sheet_name = runsheet_names.sample_info_sheet)
    ilm_dir = find_illumina_path(ilm_dir, ilm_sheet_data,
                                 sequencer = active_config["platform"],
                                 runsheet_names = runsheet_names)
    lab_version = helpers.get_illumina_lab_protocol_version(ilm_sheet,
                                                            runsheet_names = runsheet_names)
    logger.info(f"Wetlab version is {lab_version}.")
    # TODO: can we handle setting output dir more nicely?
    illumina_run = monitor_run.get_run_input_by_seq_mode(ilm_dir, illumina_runsheet = ilm_sheet,
                                                         nanopore_runsheet = None,
                                                         active_config = active_config,
                                                         config_path = config_path)
    return illumina_run


def initialize_nanopore_run(active_config: Dict[str, Any] = WORKFLOW_CONFIG,
                            config_path: pathlib.Path = DEFAULT_CONFIG_FILE) \
        -> monitor_run.IsolateRun:
    """Initialize a Nanopore IsolateRun from user input.

    Arguments:
        active_config:  the config to use
        config_path:    the path to the configfile in use

    Returns:
        The Nanopore run, optionally with user-supplied settings for output.

    Raises:
        ValueError: if sequencing time wasn't entered
    """
    ont_dir = input("Type full path or name of Nanopore sequencing folder ")
    ont_dir = pathlib.Path(ont_dir.strip().strip("'")).resolve()
    accept_seq_time = input("Expecting sequencing to be finished after"
                            f" {active_config['seq_run_duration_hours']} hours. "
                            "Is this correct? [y/n]")
    if accept_seq_time == "n":
        sequencing_time = float(input("Type how many hours the sequencing run is expected to last"
                                      " (e.g. 2 if you set it to 2 hours) and press enter: "))
    elif accept_seq_time == "y":
        sequencing_time = float(active_config["seq_run_duration_hours"])
    else:
        raise ValueError("Sequencing time not entered. Aborting")
    ont_sheet = pathlib.Path(
        input("Enter path to Nanopore runsheet: ").strip().strip("'")).resolve()
    ont_run = monitor_run.get_run_input_by_seq_mode(ont_dir, illumina_runsheet = None,
                                                    nanopore_runsheet = ont_sheet,
                                                    sequencing_time = sequencing_time,
                                                    active_config = active_config,
                                                    config_path = config_path)
    return ont_run


def initialize_manual_run(active_config: Dict[str, Any] = WORKFLOW_CONFIG,
                          config_path: pathlib.Path = DEFAULT_CONFIG_FILE) \
        -> monitor_run.IsolateRun:
    """Initialize an IsolateRun from user input.

    Arguments:
        active_config:  the config to use
        config_path:    the path to the configfile in use

    Returns:
        The run, optionally with user-supplied settings for output.

    Raises:
        NotImplementedError:    when a hybrid run is requested
        ValueError:             when an invalid sequencing mode is entered
    """
    seq_mode = active_config["sequencing_mode"]
    if seq_mode not in {"illumina", "hybrid", "nanopore"}:
        raise ValueError(f"Invalid sequencing mode {seq_mode}")
    if seq_mode == "hybrid":
        raise NotImplementedError("Hybrid sequencing analysis is currently not implemented.")
    # give license information
    logger.info("RSYD-BASIC Copyright 2023-2024 Kat Steinke \n"
                "This program comes with ABSOLUTELY NO WARRANTY; "
                "for details type run_pipeline.py --show_warranty. \n"
                "This is free software, and you are welcome to redistribute it "
                "under certain conditions; type run_pipeline.py --license for details.")
    # greet the user nicely now we've established we've got a valid run
    logger.info("### Bacterial isolate genome assembly\n"
                f"# Setup {seq_mode.capitalize()} analysis -------------------------------")
    if seq_mode == "illumina":
        current_run = initialize_illumina_run(active_config = active_config,
                                              config_path = config_path)
    else:
        current_run = initialize_nanopore_run(active_config = active_config,
                                              config_path = config_path)
    outdir = ask_output_dir(current_run.outdir)
    current_run.set_outdir(outdir)
    return current_run


# TODO: do we need to specify runsheet names,... here or can we restructure?
def initialize_commandline_run(start_args: argparse.Namespace,
                               active_config: Dict[str, Any] = WORKFLOW_CONFIG,
                               runsheet_names: input_names.RunsheetNames = SHEET_NAMES) \
        -> monitor_run.IsolateRun:
    """Initialize a run from commandline arguments.

    Arguments:
        start_args:     the arguments the pipeline was started with
        active_config:  the config to use
        runsheet_names: column names in the runsheet

    Returns:
        The run, optionally with user-supplied settings for output.

    Raises:
        NotImplementedError:    if an unsupported run type is requested (hybrid for now)
        ValueError:             if no runsheet or data directory for the specified run type has
                                been given, or an invalid sequencing mode is requested
    """
    ilm_sheet = None
    ont_sheet = None
    workflow_config_file = DEFAULT_CONFIG_FILE
    # TODO: check config vs config file
    if start_args.workflow_config_file:
        workflow_config_file = start_args.workflow_config_file
    # initialize defaults here
    test_run = active_config["debug"]
    run_time = float(active_config["seq_run_duration_hours"])
    seq_mode = active_config['sequencing_mode']
    if start_args.test_run:
        test_run = True
    if start_args.mode:
        seq_mode = start_args.mode
    valid_modes = {"illumina", "nanopore", "hybrid"}  # technically, one day...
    if seq_mode not in valid_modes:
        raise ValueError(f"Invalid sequencing mode {seq_mode}.")
    if seq_mode == "illumina":
        if not start_args.illumina_sheet:
            raise ValueError("Illumina runsheet not specified.")
        if not start_args.illumina_dir:
            raise ValueError("Illumina run directory not specified.")
        ilm_sheet = pathlib.Path(start_args.illumina_sheet).resolve()
        ilm_dir = pathlib.Path(start_args.illumina_dir).resolve()
        # figure out if we've been given the whole path or just the start
        ilm_sheet_data = pd.read_excel(ilm_sheet,
                                       sheet_name = runsheet_names.sample_info_sheet)
        seq_dir_for_run = find_illumina_path(ilm_dir, ilm_sheet_data,
                                             sequencer = active_config["platform"],
                                             runsheet_names = runsheet_names)
        # report lab version
        lab_version = helpers.get_illumina_lab_protocol_version(ilm_sheet,
                                                                runsheet_names = runsheet_names)
        logger.info(f"Wetlab version is {lab_version}.")
    elif seq_mode == "nanopore":
        if not start_args.nanopore_sheet:
            raise ValueError("Nanopore runsheet not specified.")
        if not start_args.nanopore_dir:
            raise ValueError("Nanopore run directory not specified.")
        ont_sheet = pathlib.Path(start_args.nanopore_sheet).resolve()
        seq_dir_for_run = pathlib.Path(start_args.nanopore_dir).resolve()  # we'll find the parent dir later
    else:
        raise NotImplementedError("Hybrid sequencing analysis is currently not implemented.")
    # get output dir from experiment name(s)
    run_name = helpers.construct_experiment_name(ilm_sheet, ont_sheet,
                                                 runsheet_names = runsheet_names)
    # set output dir
    if start_args.outdir:  # can only be given in commandline mode
        outdir = pathlib.Path(start_args.outdir)
    else:
        outdir = (pathlib.Path(active_config["paths"]["output_base_path"])
                      / run_name)
    if start_args.run_time:
        run_time = float(start_args.run_time)
    current_params = monitor_run.get_run_input_by_seq_mode(seq_dir_for_run, outdir,
                                                           illumina_runsheet = ilm_sheet,
                                                           nanopore_runsheet = ont_sheet,
                                                           sequencing_time = run_time,
                                                           active_config = active_config,
                                                           config_path = workflow_config_file,
                                                           test_run = test_run)
    return current_params


def check_called_without_args(start_args: argparse.Namespace,
                              parent_parser: ArgumentParser) -> bool:
    """Check if a set of arguments is derived from its parent parser being called without arguments.

    Arguments:
        start_args:     the arguments to check
        parent_parser:  the parser the arguments were derived from

    Returns:
        True if the parser was called without arguments (all values None or default),
         False otherwise.

    Raises:
        KeyError:   if the start arguments contain arguments that aren't found in the parent parser
    """
    args_with_vals = vars(start_args)
    parser_actions = parent_parser._actions
    parser_args = {arg.dest for arg in parser_actions}
    excess_args = set(args_with_vals.keys()) - parser_args
    if excess_args:
        error_msg = helpers.PrettyKeyErrorMessage(f"Argument(s) {sorted(list(excess_args))}"
                                                  " are not valid arguments for the "
                                                  "specified parser.")
        raise KeyError(error_msg)
    # start with the easiest case: everything is None/False
    # - we can only have this if no default is True
    if not any(arg.default is True for arg in parser_actions):
        if not any(vars(start_args).values()):
            return True
    # however, even if there are values, these might be defaults
    # https://stackoverflow.com/a/44543594/15704972 for getting parser defaults
    parent_defaults = {key: parent_parser.get_default(key) for key in args_with_vals}
    all_default = args_with_vals == parent_defaults
    return all_default


def run_pipeline(start_args: List[str]) -> subprocess.CompletedProcess:
    """Run the pipeline.

    Arguments:
        start_args: the arguments to start the pipeline with

    Returns:
        The process that starts the pipeline
    """
    arg_parser = ArgumentParser(description = "Start bacterial isolate analysis pipeline. "
                                              "The RSYD-BASIC pipeline is released under GPL v3.")
    arg_parser.add_argument("--illumina_sheet", help = "Path to Illumina sample sheet")
    arg_parser.add_argument("--nanopore_sheet", help = "Path to Nanopore sample sheet")
    arg_parser.add_argument("--mode", help = f"Analysis mode (nanopore, illumina or hybrid;"
                                             f"default: {WORKFLOW_CONFIG['sequencing_mode']})",
                            default = None)
    arg_parser.add_argument("--illumina_dir",
                            help = "Path to directory containing Illumina sequences")
    arg_parser.add_argument("--nanopore_dir",
                            help = "Path to directory containing Nanopore sequences")
    arg_parser.add_argument("--outdir", help = f"Path to output directory"
                                               f"(default: "
                                               f"{WORKFLOW_CONFIG['paths']['output_base_path']}"
                                               f"/[experiment_name])")
    arg_parser.add_argument("--workflow_config_file", help = "Config file for run "
                                                             "(overrides default config given in "
                                                             "script, can be overridden by "
                                                             "commandline options)")
    arg_parser.add_argument("--test_run", action = "store_true",
                            help = "Start a test run (don't add results to "
                                   "all_results and all_fastas; give more detailed"
                                   " error messages)")  # TODO: will we gather runs?
    arg_parser.add_argument("--continue_pipeline", help = "Continue pipeline",
                            action = "store_true")
    arg_parser.add_argument("--show_warranty", help="Show warranty information",
                            action="store_true")
    arg_parser.add_argument("--license", help="Print licensing information. "
                                              "Do yourself a favor and pipe this into less.",
                            action="store_true")
    arg_parser.add_argument("--dry_run", action = "store_true",
                            help = "Determine the pipeline start command,"
                                   " set up required dirs and exit")
    arg_parser.add_argument("--run_time", action = "store", type = float,
                            help = "Expected sequencing time in hours "
                                   "(will wait for the sequencing run for another hour after this;"
                                   f" default: {WORKFLOW_CONFIG['seq_run_duration_hours']})")
    arg_parser.add_argument("--logfile", action = "store",
                            help = "Logfile to store analysis start commands (default: "
                                   f"{WORKFLOW_CONFIG['paths']['output_base_path']}/"
                                   "[experiment_name]/logs/start_pipeline.log",
                            default = None)
    args = arg_parser.parse_args(start_args)
    # initialize root logger
    pipeline_logger = set_log.get_stream_log()
    # we'll save to file later, but some things will be logged before we know where to save them to
    # -> store them in the meantime
    log_store = set_log.RecordsListHandler()
    pipeline_logger.addHandler(log_store)
    # we assume this is commandline mode unless started without arguments
    manual_mode = False
    default_config_file = DEFAULT_CONFIG_FILE
    workflow_config = WORKFLOW_CONFIG
    sheet_names = SHEET_NAMES
    # https://stackoverflow.com/a/57152325/15704972 in the full awareness that this is situational
    if check_called_without_args(args, arg_parser):
        manual_mode = True
        # lots of typing, so  allow tab completion of paths
        readline.set_completer_delims('\t\n=')
        readline.parse_and_bind("tab: complete")
    # otherwise set up terminal mode
    else:
        # exit early if we should print license or warranty information
        if args.show_warranty:
            print(
                "There is no warranty for the program, to the extent permitted by applicable law."
                " Except when otherwise stated in writing the copyright holders "
                "and/or other parties provide the program “as is” without warranty of any kind, "
                "either expressed or implied, including, but not limited to, the implied warranties"
                " of merchantability and fitness for a particular purpose. The entire risk as to "
                "the quality and performance of the program is with you. "
                "Should the program prove defective, you assume the cost of all necessary "
                "servicing, repair or correction.")
            exit(0)
        if args.license:
            with open(pathlib.Path(__file__).parent / "licenses" / "RSYD_BASIC_GPL", mode = "r",
                      encoding = "utf-8") as license_file:
                license_text = license_file.read()
                print(license_text)
            exit(0)
        # load config if present - we need to do this early since it contains mode information
        if args.workflow_config_file:
            default_config_file = pathlib.Path(args.workflow_config_file).resolve()
            with open(default_config_file, "r", encoding = "utf-8") as config_file:
                workflow_config = yaml.safe_load(config_file)
            (sheet_names,
             lis_names,
             bact_names) = localization_helpers.load_input_from_config(workflow_config)
    # set up use of LIS features if enabled - do this early so we can fetch newest LIS DB
    if workflow_config["lab_info_system"]["use_lis_features"]:
        # set up the report here, save to file if connected directly to DB
        # -> pipeline reads the fetched file in all other places
        get_lis_report.setup_lis_data_from_config(workflow_config)

    if manual_mode:
        run_params = initialize_manual_run(active_config = workflow_config,
                                           config_path = default_config_file)
    else:
        run_params = initialize_commandline_run(args, active_config = workflow_config,
                                                runsheet_names = sheet_names)
    # we check the output dir - this is the first check in commandline mode, second in manual mode
    # if something still is broken, or the commandline version has been given a wrong path,
    # we yell at the user and fail
    output_dir = get_clean_outdir(run_params.outdir)
    run_params.set_outdir(output_dir)

    # we can check for whether this is a continued pipeline here - default False in manual mode
    continue_pipeline = args.continue_pipeline

    # we'll have to handle creating our folders ourselves
    set_up_outputs(run_params, continue_pipeline)

    # now we have a clean outdir we can save our logfile
    if args.logfile:
        log_file = pathlib.Path(args.logfile)
    else:
        log_file = run_params.outdir / "logs" / "start_pipeline.log"
    file_handle = logging.FileHandler(log_file)
    # file logs ought to be a bit more detailed
    file_format = logging.Formatter(fmt = '%(asctime)s - %(levelname)s: %(module)s: %(message)s')
    file_handle.setFormatter(file_format)
    # properly attach the handler
    pipeline_logger.addHandler(file_handle)
    # add all of our old messages to the file
    set_log.handle_bulk_logs(file_handle, log_store.record_log)
    # ... and remove our temporary one
    pipeline_logger.removeHandler(log_store)
    log_store.close()

    # TODO: combine reads if mode is hybrid
    if run_params.seq_mode == "illumina":
        # start pipeline (in Docker container)
        pipeline_logger.info("Running analysis pipeline")
        start_command = monitor_run.start_run_by_mode(run_params, dry_run = args.dry_run)
        # TODO function?
        for handler in pipeline_logger.handlers[:]:
            pipeline_logger.removeHandler(handler)
            handler.close()
        return start_command
    pipeline_logger.info("Pipeline is now waiting for sequencing to finish...")
    # calculate waiting time and check interval
    seq_run_fudge_factor = timedelta(hours = 1)
    total_time = run_params.sequencing_time + seq_run_fudge_factor
    check_interval = workflow_config["check_interval_seconds"]
    if os.fork():
        sys.exit()

    start_pipeline = monitor_run.start_on_file_found(run_params, "final_summary*.txt",
                                                     dry_run = args.dry_run,
                                                     watch_timeout = int(total_time.total_seconds()),
                                                     watch_interval = check_interval,
                                                     log_interval = 3600)
    logger.info(f"Started pipeline with command {' '.join(start_pipeline.args)}")

    for handler in pipeline_logger.handlers[:]:
        pipeline_logger.removeHandler(handler)
        handler.close()
    return start_pipeline


if __name__ == "__main__":
    # deactivate traceback in classic mode
    def quiet_except_hook(exc_type, exc_value, traceback):
        """
        Hide traceback unless a debug flag is specified.
        Adapted from https://stackoverflow.com/a/27674608/15704972
        and
        https://stackoverflow.com/questions/6598053/python-global-exception-handling/6598286#6598286
        """
        # if the debug flag is set, give the original exception
        if debug:
            sys.__excepthook__(exc_type, exc_value, traceback)
        else:
            print(f"\n{exc_type.__name__}: {exc_value}")


    sys.excepthook = quiet_except_hook
    # traceback is on from the start since if things fail so early we'll want to know why
    debug = True
    # we also want to disable the warning about DataValidation as it won't impact the data
    # (compare https://stackoverflow.com/a/66571471/15704972)
    warnings.filterwarnings('ignore',
                            message="Data Validation extension is not supported and will be removed",
                            module="openpyxl")
    run_pipeline(sys.argv[1:])