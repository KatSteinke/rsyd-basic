FROM registry.gitlab.com/regionsyd/container-management/namespaces/ouh-kma/bactopia-base:latest

WORKDIR /snake_data

COPY . /snake_data

WORKDIR /bactopia_datasets

COPY config/input_settings/ /bactopia_datasets/config/input_settings/