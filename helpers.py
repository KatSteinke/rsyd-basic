"""Assorted helper functions"""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.


import io
import logging
import pathlib
import re

from typing import Any, Dict, List, Optional, Union

import pandas as pd
from pandas._libs.missing import NAType

import input_names
import localization_helpers as loc_helpers
import pipeline_config

default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

sheet_names, lis_names, bact_names = loc_helpers.load_input_from_config(workflow_config)


# start logging - TODO: extend?
logger = logging.getLogger("helpers")


class BadPathError(Exception):
    """Exception raised when a path to be created contains illegal characters or reserved
    filenames. Workaround to be able to distinguish from incorrect user input in classic mode.
    """
    pass


class MissingReadfilesError(FileNotFoundError):
    """Exception raised when one or more read files for samples in a run are missing."""
    def __init__(self, message: str, missing_r1: Optional[List[str]] = None,
                 missing_r2: Optional[List[str]] = None,
                 missing_long: Optional[List[str]] = None):
        # use base class message
        super().__init__(message)
        # add information on samples missing R1 or R2 (or long read)
        self.missing_r1 = missing_r1
        self.missing_r2 = missing_r2
        self.missing_long = missing_long


class PrettyKeyErrorMessage(str):
    """Workaround to allow formatted explanatory messages when raising a KeyError.
    Taken from https://stackoverflow.com/a/70114007/15704972
    """
    def __repr__(self):
        return str(self)


# TODO: should the data sheet be a path here too (to avoid hvaing to read it multiple times)
def check_for_samples(seq_dir: pathlib.Path, data_sheet: pd.DataFrame,
                      runsheet_names: input_names.RunsheetNames = sheet_names) -> None:
    """Check if forward and reverse reads for all samples in a run are present in the specified
    directory.

    Arguments:
        seq_dir:        the directory containing reads for the samples; reads must be gathered in
                        a single directory
        data_sheet:     Sample information from an Excel Illumina runsheet
        runsheet_names: Column names in the runsheet

    Raises:
        MissingReadfilesError:    if one or more read files are missing
    """
    sample_reads = pd.DataFrame()
    # find missing reads for all samples - check if *not* found
    sample_reads["missing_in_r1"] = data_sheet[runsheet_names.sample_number].apply(lambda sample_name:
                                                                not list(seq_dir.glob(f"{sample_name}_*"
                                                                                      f"_R1_*.fastq.gz")))
    sample_reads["missing_in_r2"] = data_sheet[runsheet_names.sample_number].apply(lambda sample_name:
                                                                not list(seq_dir.glob(f"{sample_name}_*"
                                                                                      f"_R2_*.fastq.gz")))
    # if we found any missing samples, complain
    if sample_reads["missing_in_r1"].any() or sample_reads["missing_in_r2"].any():
        missing_r1 = data_sheet[runsheet_names.sample_number].loc[
            sample_reads["missing_in_r1"]].tolist()
        missing_r2 = data_sheet[runsheet_names.sample_number].loc[
            sample_reads["missing_in_r2"]].tolist()
        error_message = ""
        if missing_r1:
            error_message += f"R1 not found in {str(seq_dir)} for samples {sorted(missing_r1)}. \n"
        if missing_r2:
            error_message += f"R2 not found in {str(seq_dir)} for samples {sorted(missing_r2)}. \n"
        raise MissingReadfilesError(error_message, missing_r1 = missing_r1, missing_r2=missing_r2)


def check_nanopore_dirs(fastq_pass_dir: pathlib.Path, nanopore_sheet: pd.DataFrame,
                        runsheet_names: input_names.RunsheetNames = sheet_names) -> None:
    """Check if barcode directories for all samples in a run are present in the specified directory.

    Arguments:
        runsheet_names:
        fastq_pass_dir: the fastq_pass directory for the run
        nanopore_sheet: Sample information from an Excel Nanopore runsheet
        runsheet_names: Column names in the runsheet


    Raises:
        MissingReadfilesError:    if one or more barcode dirs are missing
    """
    # standard format may have blank rows due to formatting setup
    nanopore_sheet = nanopore_sheet.dropna()
    barcode_hits = pd.DataFrame()
    barcode_hits["barcode_dirs"] = "barcode" + nanopore_sheet[
        runsheet_names.barcode].str.extract(r"(\d{2})")
    barcode_hits["barcode_found"] = barcode_hits["barcode_dirs"].apply(lambda barcode:
                                                                       (fastq_pass_dir
                                                                        / barcode).exists())
    if not barcode_hits["barcode_found"].all():
        missing_barcodes = sorted(barcode_hits[~barcode_hits["barcode_found"]]["barcode_dirs"].tolist())
        error_msg = f"Barcode directories {missing_barcodes} are missing from {fastq_pass_dir}."
        raise MissingReadfilesError(message = error_msg, missing_long = missing_barcodes)
    logger.debug(f"Found all barcodes in {fastq_pass_dir}.")


def get_fastq_pass_parent(nanopore_basedir: pathlib.Path,
                          nanopore_sheet: pd.DataFrame,
                          runsheet_names: input_names.RunsheetNames = sheet_names) -> pathlib.Path:
    """"Find the parent directory of the fastq_pass directory for the given run directory.

    Arguments:
        nanopore_basedir:   the base directory for the run or the fastq_pass directory
                            (or any directory between these two)
        nanopore_sheet:     Sample information from an Excel Nanopore runsheet
        runsheet_names:     Column names in the runsheet


    Returns:
        The fastq_pass directory containing all barcodes, if this exists.

    Raises:
        FileNotFoundError:      if the fastq_pass directory is not in the expected location
        ValueError:             if there are multiple fastq_pass directories
                                and the one to use is not specified

    """
    ## we may need to give the fastq_pass directory directly
    # or a group of dirs in the fastq_pass dir
    existing_path = nanopore_basedir.parts
    try:
        # if the fastq_pass directory already is somewhere in the dirs given, use this
        fastq_pass_parts = existing_path[:existing_path.index("fastq_pass") + 1]
        # parts contains the initial "/" - resolve the path to clean this up
        fastq_pass_dir = pathlib.Path("/".join(fastq_pass_parts)).resolve()
    except ValueError:
        logger.info(f"Searching for fastq_pass folder in {nanopore_basedir}...")
        check_fastq_pass = list(nanopore_basedir.glob("**/fastq_pass"))
        if not check_fastq_pass:
            raise FileNotFoundError(f"fastq_pass folder not found in {nanopore_basedir} "
                                    "or any subfolders. \n"
                                    "Ensure correct directory and/or directory structure is used.\n"
                                    "Aborting isolate pipeline...")
        if len(check_fastq_pass) > 1:
            raise ValueError(f"The directory {nanopore_basedir} contains "
                             "multiple fastq_pass directories."
                             "Please choose the one containing the fastq files you want to analyze"
                             " and specify the entire path to the fastq_pass directory.")
        fastq_pass_dir = check_fastq_pass[0]

    # we're using this at multiple points, at some of which barcode dirs not being present might not
    # be an issue
    try:
        check_nanopore_dirs(fastq_pass_dir, nanopore_sheet, runsheet_names = runsheet_names)
    except MissingReadfilesError as missing_readfiles:
        logger.warning(f"Barcode directories {missing_readfiles.missing_long} are missing"
                       f" from {fastq_pass_dir}.\n"
                       "This may be due to a delay in copying files from the sequencer, but could"
                       " also mean you have given the wrong path. \n"
                       "Only continue if you are sure. ")
    fastq_pass_parent = fastq_pass_dir.parent
    logger.info(f"Data is retrieved from the following folder:\n"
                f"{fastq_pass_parent}")
    return fastq_pass_parent


# TODO: restructure this? We're doing sort of similar things all over the place (esp. the merge)
def find_missing_species(data_sheet: pd.DataFrame, lis_report: pd.DataFrame,
                         species_coding: pd.DataFrame,
                         runsheet_names: input_names.RunsheetNames = sheet_names,
                         bacteria_names: input_names.BacteriaMappingNames = bact_names,
                         lis_data_names: input_names.LISDataNames = lis_names) -> pd.DataFrame:
    """Find all isolates missing a translation for their species code.

    Arguments:
        data_sheet:     the data section of the Illumina sample sheet
        lis_report:     LIS report with an additional column giving the isolate number in a
                        format matching the one in the runsheet
        species_coding: a translation of bacteria codes to taxonomic species names (not just
                        internal names, which may be on strain/serotype level)
        runsheet_names: Column names in the runsheet
        bacteria_names: Column names in the code -> species name mapping
        lis_data_names: Column names in LIS data

    Returns:
        Isolate numbers and species codes for isolates with no translation for their species code.
    """
    samples_in_lis = data_sheet.merge(lis_report, how = "left",
                                      left_on = runsheet_names.sample_number,
                                      right_on = "proevenr")  # TODO: can we give this a less confusing placeholder?
    samples_in_lis['code_found'] = samples_in_lis[
        lis_data_names.bacteria_code].isin(species_coding[bacteria_names.bacteria_code])
    samples_no_code = samples_in_lis[~samples_in_lis['code_found']][[runsheet_names.sample_number,
                                                                     lis_data_names.bacteria_code]]
    samples_no_code = samples_no_code.reset_index(drop = True)
    return samples_no_code


# TODO: break into smaller chunks?
def check_sample_sheet(run_data: pathlib.Path, lis_report: Optional[pd.DataFrame] = None,
                       species_coding: Optional[pd.DataFrame] = None,
                       active_config: Dict[str, Any] = workflow_config,
                       runsheet_names: input_names.RunsheetNames = sheet_names,
                       bacteria_names: input_names.BacteriaMappingNames = bact_names,
                       lis_data_names: input_names.LISDataNames = lis_names) \
        -> None:
    """Check if the sample sheet is correct (sample numbers are in the correct format as supplied
     in the config file, sample numbers match between runsheet and sample data sheet,
      barcodes are present for a nanopore sheet).
    If a lab information system report is given, also check if the sample numbers are present
    in this report, and check if all samples have translations for species codes.

    Arguments:
        run_data:       the sample sheet
        lis_report:     optional: LIS report with an additional column giving the isolate number
                        in a format matching the one in the runsheet
        species_coding: optional: a translation of bacteria codes to taxonomic species names
                        (not just internal names, which may be on strain/serotype level)
        active_config:  the configuration to use
        runsheet_names: Column names in the runsheet
        bacteria_names: Column names in the code -> species name mapping
        lis_data_names: Column names in LIS data


    Raises:
        ValueError: if sample IDs do not conform to required sample format
        KeyError:   if samples not found in LIS report

    """
    # recognize runsheet type!
    with pd.ExcelFile(run_data) as data_file:
        data_sheet = pd.read_excel(data_file, sheet_name=runsheet_names.sample_info_sheet,
                                   dtype = {runsheet_names.sample_number: str})
        data_sheet = data_sheet.dropna(subset=[runsheet_names.sample_number])
    if data_sheet.empty:
        raise ValueError("No sample numbers in sample information sheet"
                         f" ('{runsheet_names.sample_info_sheet}').")
    id_pattern = active_config["sample_number_settings"]["sample_number_format"]
    fail_numbers = data_sheet[runsheet_names.sample_number][
        ~data_sheet[runsheet_names.sample_number].apply(str).str.match(id_pattern,
                                                                       na = False)].dropna().tolist()
    # TODO: complain specifically for missing isolate numbers, missing year...?
    if fail_numbers:
        # adapt error message to sample number format somewhat
        if active_config['sample_number_settings']['sample_numbers_in'] == "number":
            allowed_start = active_config['sample_number_settings']['number_to_letter'].keys()
        else:
            allowed_start = active_config['sample_number_settings']['number_to_letter'].values()
        raise ValueError(f"Sample IDs {fail_numbers} are not valid. \n"
                         f"Sample IDs must start with {' or '.join(sorted(allowed_start))}"
                         f" followed by eight numbers and a single digit isolate number. "
                         f"Please correct sample IDs in runsheet.")

    # check if samples are in LIS if we're using it
    if lis_report is not None:
        samples_in_lis = data_sheet.merge(lis_report, how="left",
                                          left_on = runsheet_names.sample_number,
                                          right_on = "proevenr")
        missing_numbers = samples_in_lis.loc[samples_in_lis["proevenr"].isna(),
                                             runsheet_names.sample_number].tolist()
        if missing_numbers:
            pretty_print_missing = "\n".join(missing_numbers)
            error_msg = PrettyKeyErrorMessage(f"The following numbers were not found "
                                              f"in the MADS report:\n"
                                              f"{pretty_print_missing}")
            raise KeyError(error_msg)
        if species_coding is not None:
            # TODO: should we complain otherwise?
            #  We do when merging results but this might be different?
            isolates_no_code = find_missing_species(data_sheet, lis_report, species_coding,
                                                    runsheet_names = runsheet_names,
                                                    lis_data_names = lis_data_names,
                                                    bacteria_names = bacteria_names)
            if not isolates_no_code.empty:
                warn_msg = f"No species names found for MADS bacteria codes " \
                           f"for the following samples: \n" \
                           f"{isolates_no_code.to_string()}\n" \
                           f"The pipeline will still run, but no species-specific analyses " \
                           f"will be performed for these samples. \n" \
                           f"Contact the responsible bioinformatician."
                logger.warning(warn_msg)
    # if we haven't raised until now everything is fine
    logger.info("Data sheet is OK")


def check_nanopore_runsheet(nanopore_sheet: pathlib.Path,
                            runsheet_names: input_names.RunsheetNames = sheet_names) -> None:
    """Check runsheet for issues specific to a Nanopore runsheet.

    Arguments:
        nanopore_sheet:     Path to the runsheet to check. Needs a Runsheet_Nanopore tab.
        runsheet_names:     Column names in the runsheet

    Raises:
        ValueError: if some or all of the sample numbers or barcodes are missing
    """
    with pd.ExcelFile(nanopore_sheet) as data_file:
        data_sheet = pd.read_excel(data_file, sheet_name = runsheet_names.sample_info_sheet,
                                   dtype = {runsheet_names.sample_number: str})
        data_sheet = data_sheet.dropna(subset = [runsheet_names.sample_number])
        nanopore_runsheet = pd.read_excel(data_file,
                                          sheet_name = f"{runsheet_names.runsheet_base}"
                                                       "_Nanopore",
                                          usecols = "A:C", skiprows = 3,
                                          dtype = {
                                              runsheet_names.nanopore_sheet_sample_number: str,
                                              runsheet_names.barcode: str})
    nanopore_runsheet = nanopore_runsheet.dropna(subset = [runsheet_names.nanopore_sheet_sample_number])
    nanopore_runsheet = nanopore_runsheet.rename(columns =
                                                 {runsheet_names.nanopore_sheet_sample_number:
                                                      runsheet_names.sample_number})
    if nanopore_runsheet.empty:
        raise ValueError("No sample numbers in runsheet.")
    missing_barcodes = nanopore_runsheet[
        nanopore_runsheet[runsheet_names.barcode].isna()][runsheet_names.sample_number].tolist()
    if missing_barcodes:
        raise ValueError(f"No barcodes given for sample(s) {missing_barcodes}.")
    not_in_runsheet = (set(data_sheet[runsheet_names.sample_number])
                       - set(nanopore_runsheet[runsheet_names.sample_number]))
    if not_in_runsheet:
        raise ValueError(f"Samples {sorted(list(not_in_runsheet))} are "
                         "present in sample information sheet "
                         f"('{runsheet_names.sample_info_sheet}') "
                         "but not runsheet.")
    not_in_sample_data = (set(nanopore_runsheet[runsheet_names.sample_number])
                          - set(data_sheet[runsheet_names.sample_number]))
    if not_in_sample_data:
        raise ValueError(f"Samples {sorted(list(not_in_sample_data))} are "
                         "present in runsheet "
                         "but not sample information sheet"
                         f" ('{runsheet_names.sample_info_sheet}').")
    # if we haven't raised until now everything is fine
    logger.info("Nanopore runsheet is OK")


# TODO: move repetitive filename check to separate function
def check_experiment_name_problems(experiment_name: str) -> None:
    """Check whether an experiment name contains any parts that may cause issues
     when used as filenames.

    Arguments:
        experiment_name:    the experiment name to check

    Raises:
        ValueError: if the experiment name contains chars that can break something
                    (reserved chars, whitespace, slashes)
    """
    illegal_in_windows = r'[<>:"|?*]'
    if re.search(illegal_in_windows, experiment_name):
        raise ValueError("The run name contains a character that cannot be used in "
                         "Windows filenames.")
    # ...or straight up being a reserved filename
    if pathlib.PureWindowsPath(experiment_name).is_reserved():
        raise ValueError("The run name contains a character that cannot be used in "
                         "Windows filenames.")
    # or it could break something from containing whitespace
    if re.search(r"\s", experiment_name):
        raise ValueError("The run name contains spaces or line breaks.")
    # sometimes people enter multiple initials separated with slashes - this'll create subdirs
    if "/" in experiment_name:
        raise ValueError("The run name contains a forward slash (/). "
                         "This will break the result directory."
                         " Replace forward slashes with underscores (_).")
    logger.debug(f"No issues found with experiment name {experiment_name}.")


def extract_illumina_run_name(runsheet: pathlib.Path,
                              runsheet_names: input_names.RunsheetNames = sheet_names) -> str:
    """Extract the name of an Illumina sequencing run from its Excel runsheet.

    Arguments:
         runsheet:          Path to an Excel runsheet containing the plate layout
                            (as tab specified under plate_layout in the config)
         runsheet_names: Column names in the runsheet
     Returns:
         The run's name as specified under "run name".

    Raises:
        ValueError: if the experiment name contains chars that can break something
                    (reserved chars, whitespace, slashes)
    """
    # take experiment name from plate layout: rows 3:4, columns A:B

    plate_layout = pd.read_excel(runsheet, sheet_name = runsheet_names.plate_layout,
                                 usecols = "A:B",
                                 skiprows = 2, nrows = 2, index_col=0)
    # reshape plate layout
    plate_layout = plate_layout.transpose().reset_index()
    plate_layout = plate_layout.rename(columns={"index": "Run name"})
    plate_layout.columns.names = [None]
    # get experiment name: first and only entry under run name
    experiment_name = plate_layout.at[0, "Run name"]
    # the experiment name is used as file names for a lot of things, so catch if it breaks something
    # could break something from containing characters that aren't allowed in Windows
    check_experiment_name_problems(experiment_name)
    return experiment_name


def extract_nanopore_run_name(runsheet: pathlib.Path,
                              runsheet_names: input_names.RunsheetNames = sheet_names) -> str:
    """Extract the name of a Nanopore sequencing run from its Excel runsheet.

    Arguments:
         runsheet:          Path to an Excel runsheet containing the Nanopore runsheet
         runsheet_names:    Column names in the runsheet

     Returns:
         The run's name as specified under "RUNxxxx-INI".

    Raises:
        ValueError: if the experiment name contains chars that can break something
                    (reserved chars, whitespace, slashes)
    """
    experiment_sheet = pd.read_excel(runsheet,
                                     sheet_name = f"{runsheet_names.runsheet_base}"
                                                  "_Nanopore",
                                   usecols = "A:C", skiprows = 1, nrows=2)
    experiment_name = experiment_sheet.at[0, runsheet_names.experiment_name]
    # the experiment name is used as file names for a lot of things, so catch if it breaks something
    # could break something from containing characters that aren't allowed in Windows
    check_experiment_name_problems(experiment_name)
    return experiment_name


# TODO: copy these changes to the metadata database?
def split_illumina_sheet(raw_illumina_sheet: str) -> Dict[str, str]:
    """Split an Illumina runsheet into its individual sections.

    Arguments:
        raw_illumina_sheet: the raw Illumina sheet as extracted from the full Excel file

    Returns:
        The individual sections of the runsheet under their respective headings.

    Raises:
        KeyError:   if a section is missing from the runsheet
        ValueError: if a section of the runsheet is badly formed
    """
    with io.StringIO(raw_illumina_sheet) as read_raw_sheet:
        sheet_sections = {}
        current_header = False
        row_count = 0
        # read every line in the file
        for line in read_raw_sheet:
            # count rows in order to give informative error messages
            row_count += 1
            # having to go pandas -> string means we have a bunch of whitespace we don't need
            line = line.lstrip()
            # header lines are marked with square brackets
            if line.startswith("["):
                header_hit = re.search(r"\[(?P<section_name>\w+)]", line)
                if not header_hit:
                    raise ValueError(f"The Illumina runsheet's format is invalid: "
                                     f"broken section header in row {row_count}. \n"
                                     f"Section headers need to be in square brackets "
                                     f"(like this: [Et_eller_andet_eksempel])")
                current_header = header_hit.group("section_name")
            else:
                # only start recording when a header has been recorded
                if current_header:
                    if current_header in sheet_sections:
                        sheet_sections[current_header] += line
                    else:
                        sheet_sections[current_header] = line
        # check that all relevant sections are there - different this time
    required_headers = {"Header"}
    if not required_headers.issubset(set(sheet_sections.keys())):
        missing_sections = required_headers - set(sheet_sections.keys())
        error_msg = PrettyKeyErrorMessage(f"Section(s) {missing_sections} were not found "
                                          f"in the runsheet.")
        raise KeyError(error_msg)
    return sheet_sections


# get wetlab protocol
def get_illumina_lab_protocol_version(full_runsheet: pathlib.Path,
                                      runsheet_names: input_names.RunsheetNames = sheet_names) \
        -> str:
    """Get the lab protocol version if given.
    Lab protocol version is given in the Illumina sample sheet, under the Sample_Project column
    under the BCL_Data section.

    Arguments:
        full_runsheet:  the run's full runsheet (containing consolidated data from the
                        "master" sheet)
        runsheet_names: Column names in the runsheet
    Returns:
        The lab protocol version if given, otherwise an empty string.

    """
    # TODO: handle branching!
    # find any tab starting with Runsheet - complain if there are multiple ones
    full_sheet_data = pd.ExcelFile(full_runsheet)
    runsheets = [runsheet for runsheet in full_sheet_data.sheet_names
                 if runsheet.startswith(runsheet_names.runsheet_base)]
    # TODO: warn or fail?
    if not runsheets:
        logger.warning("No runsheet tab found in Excel sheet. "
                       "Wetlab protocol version will not be recorded in results.")
        return ""
    if len(runsheets) > 1:
        logger.warning("Too many runsheet tabs found in Excel sheet. "
                       "Cannot find the correct runsheet. "
                       "Wetlab protocol version will not be recorded in results.")
        return ""
    ilm_runsheet = pd.read_excel(full_sheet_data, sheet_name = runsheets[0])
    # convert the contents to a string so we can handle the chunks separately
    raw_runsheet = ilm_runsheet.to_csv(index = False, sep = "\t")
    # parse the string - extract the sections
    sheet_sections = split_illumina_sheet(raw_runsheet)
    # header part can be parsed to a dict
    sheet_header = sheet_sections["Header"].strip()
    header_values = {}
    all_header_info = sheet_header.split("\n")
    # header rows are comma-separated; heading comes first, followed by value -
    # TODO: we only need one piece of information, just read until instead?
    for header_row in all_header_info:
        header_row = header_row.split("\t")
        header_values[header_row[0]] = header_row[1]
    # check if the file version is correct - alert and return empty string otherwise
    format_version = header_values.get("FileFormatVersion", "not found")
    if format_version != "2":
        logger.warning(f"Runsheet format version is {format_version}. "
                       f"Wetlab protocol version can only be extracted from "
                       f"runsheets with format version 2. \n"
                       f"Wetlab protocol version will not be recorded in results.")
        return ""
    # find BCLConvert_Data - here we have something that's nice to parse with pandas
    if "BCLConvert_Data" not in sheet_sections:
        logger.warning("Cannot find section BCLConvert_Data. "
                       "Wetlab protocol version will not be recorded in results.")
        return ""
    # get Sample_Project column if given
    bcl_convert_data = pd.read_csv(io.StringIO(sheet_sections["BCLConvert_Data"]), sep = "\t")
    if 'Sample_Project' not in bcl_convert_data.columns:
        logger.warning("Wetlab protocol version should be given under the column Sample_Project.\n"
                       "As the column is missing, the wetlab protocol version will not be recorded"
                       " in results.")
        return ""
    protocol_versions = [str(version_name)
                         for version_name in bcl_convert_data['Sample_Project'].unique()]
    # TODO: what to do if there are no sample IDs here?
    return ";".join(protocol_versions)


def get_nanopore_lab_protocol_version(full_runsheet: pathlib.Path,
                                      runsheet_names: input_names.RunsheetNames = sheet_names) \
        -> str:
    """Get the lab protocol version if given. Protocol version is given in the header of the
    Nanopore runsheet.

    Arguments:
        full_runsheet:  the run's full runsheet (containing consolidated data from the
                        "master" sheet)
        runsheet_names: Column names in the runsheet

    Returns:
        The protocol version if given; blank string if the version column is missing.
    Raises:
        KeyError:   if no Nanopore runsheet is given.
    """
    nanopore_sheet_name = f"{runsheet_names.runsheet_base}_Nanopore"
    version_col = runsheet_names.protocol_version
    full_sheet_data = pd.ExcelFile(full_runsheet)
    if nanopore_sheet_name not in full_sheet_data.sheet_names:
        raise KeyError("No Nanopore runsheet tab found in sheet.")
    nanopore_sheet = pd.read_excel(full_sheet_data, sheet_name = nanopore_sheet_name, skiprows = 1,
                                   nrows = 2)
    if version_col not in nanopore_sheet.columns:
        logger.warning("No version column found in runsheet. "
                       "Wetlab protocol version will not be recorded in results.")
        return ""
    protocol_version = nanopore_sheet.at[0, version_col]
    return protocol_version


def get_number_letter_combination(number_to_letter: Dict[str, str], samples_in: str,
                                  samples_out: str) -> Dict[str, str]:
    """Generate correct mapping for a sample number setup from a number to letter mapping.

    Arguments:
        number_to_letter:   The original number to letter mapping
        samples_in:         The format sample numbers have in the runsheet
        samples_out:        The format sample numbers have in the laboratory information system

    Returns:
        A mapping of sample number starts in samples_in format to sample numbers in samples_out
        format.
    """
    # TODO: include some kind of test to ensure this is number to letter?
    if samples_in not in {"number", "letter"}:
        raise ValueError(f"Invalid initial sample format {samples_in}. "
                         f"Sample format can only be number or letter")
    if samples_out not in {"number", "letter"}:
        raise ValueError(f"Invalid desired sample format {samples_out}. "
                         f"Sample format can only be number or letter")
    if samples_in == "number":
        if samples_out == "letter":
            return number_to_letter
        # otherwise, samples_out must be number, so we return a self-to-self mapping
        return {key: key for key in number_to_letter}
    else:  # samples_in is letter
        if samples_out == "number":
            # reverse the dictionary
            return {value: key for (key, value) in number_to_letter.items()}
        # otherwise, samples_out is letter
        return {value: value for value in number_to_letter.values()}


def translate_sample_category(sample_number: str, sample_number_format: re.Pattern, samples_in: str,
                              samples_out: str,
                              mapping: Dict[str, str] = workflow_config["sample_number_settings"][
                                  "number_to_letter"]) -> str:
    """Translate sample category to desired format.

    Arguments:
        sample_number:          the sample number to translate
        sample_number_format:   the sample number format as a regex with named groups for components
        samples_in:             the format (number or letter) used in the input
        samples_out:            the format (number or letter) used in the output
        mapping:                mapping of category codes in number format to letter format

    Returns:
        The sample number with the sample category translated to the desired format.

    Raises:
        KeyError:   if the sample number format does not contain a match group for sample type
        ValueError: if the sample number does not match the specified format
    """
    # check if the format is valid
    if "sample_type" not in sample_number_format.groupindex:
        error_msg = PrettyKeyErrorMessage("No sample type specified in sample number format"
                     f" {sample_number_format.pattern}. "
                     "Please specify sample type as a match group named sample_type.")
        raise KeyError(error_msg)
    # check if the sample number matches the format
    match_sample_number = re.search(sample_number_format, sample_number)
    if not match_sample_number:
        raise ValueError(f"Sample number {sample_number} does not match the specified format.")
    # do we even need to translate?
    if samples_in == samples_out:
        return sample_number
    # if we do:
    # convert the number to letter mapping to the format we need
    format_mapping = get_number_letter_combination(mapping, samples_in, samples_out)
    # get all match groups
    matches = match_sample_number.groupdict()
    # replace the sample type with the corresponding item from the mapping
    matches["sample_type"] = format_mapping[matches["sample_type"]]
    # put it all back together
    sample_number_translate = "".join(matches.values())
    return sample_number_translate



def rearrange_sample_number(old_sample_number: str, pattern_in: re.Pattern,
                            order_out: Dict[str, int]) -> str:
    """
    Split up a sample number in its components (as given by a regex) and reorder them
    in the order given.

    Arguments:
        old_sample_number:  the sample number in its original order
        pattern_in:         a regex representing the components of the original sample number
        order_out:          the desired order of the components: component to position

    Returns:
        The sample number with components reordered in the desired order.

    Raises:
        KeyError:   if the desired order contains a component not found in the input pattern

    """
    # sanity check if we have everything
    extra_components = set(order_out.keys()) - set(pattern_in.groupindex.keys())
    if extra_components:
        error_msg = PrettyKeyErrorMessage(f"Not all desired sample number components could be "
                                          f"found in the original format. "
                                          f"Desired sample number format contains additional "
                                          f"components {extra_components}.")
        raise KeyError(error_msg)
    # find components of the sample number
    sample_components = re.search(pattern_in, old_sample_number)
    # check if any are missing
    if not sample_components:
        raise ValueError(f"No match in sample number {old_sample_number}.")
    # add components in the order given in the input
    sample_reordered = []
    # we're getting this as a key, value tuple
    component_order = dict(sorted(order_out.items(),
                                  key = lambda component_position: component_position[1]))
    for component in component_order:
        sample_reordered.append(sample_components.group(component))

    # combine components
    new_sample_number = "".join(sample_reordered)
    return new_sample_number



# TODO: do we need to rework this to get species by sample? Make a separate one for that?
#  Maybe the second - we need this to run in bulk but we may also just want a single species
def get_species_by_code(species_code: str, species_by_code: pd.DataFrame,
                        bacteria_names: input_names.BacteriaMappingNames = bact_names)\
        -> Union[str, NAType]:
    """Translate a bacteria code to a taxonomic species name.

    Arguments:
        species_code:       the bacteria code in the LIS (LOKAL_BAKT_KODE)
        species_by_code:    a translation of bacteria codes to taxonomic species names (not just
                            internal names, which may be on strain/serotype level)
        bacteria_names:     Column names in the code -> species name mapping


    Returns:
        The taxonomic name of the species if found, else NA.
    """
    # since species code only shows up in the query string it gets wrongly flagged as unused
    # pylint: disable=unused-argument
    species_coding = species_by_code.copy()
    species_names = species_coding.query(f"{bacteria_names.bacteria_code}"
                                         " == @species_code")[bacteria_names.bacteria_category]
    species_names = species_names.reset_index(drop=True)
    # if we don't find a match we can't return anything
    if species_names.empty:
        return pd.NA
    # otherwise there only should be one entry
    species_name = species_names.iat[0]
    return species_name


def get_species_by_number(sample_number: str, lis_report: pd.DataFrame,
                          species_coding: pd.DataFrame,
                          lis_data_names: input_names.LISDataNames = lis_names,
                          bacteria_names: input_names.BacteriaMappingNames = bact_names)\
        -> Union[str, NAType]:
    """
    Look a sample up in the LIS report and identify its species through the species code given.
    Arguments:
        sample_number:      the sample's sample number in the format in the format used for output
        lis_report:         the LIS report, "translated" so sample number format matches.
                            Must contain bacteria code
        species_coding:     a translation of bacteria codes to taxonomic species names (not just
                            internal names, which may be on strain/serotype level)
        lis_data_names:     Column names in the LIS
        bacteria_names:     Column names in the code -> species name mapping

    Returns:
        The taxonomic name of the species if found, else NA.

    Raises:
        KeyError:   if the sample number isn't found in the LIS report
    """
    if sample_number not in lis_report["proevenr"].tolist():
        error_msg = PrettyKeyErrorMessage(f"Sample number {sample_number} not found in LIS report.")
        raise KeyError(error_msg)
    sample_code = lis_report.query("proevenr "
                                   "== @sample_number")[lis_data_names.bacteria_code].iloc[0]
    species_name = get_species_by_code(sample_code, species_coding, bacteria_names = bacteria_names)
    return species_name


def pivot_wide_to_long(wide_df: pd.DataFrame) -> pd.DataFrame:
    """Change a wide dataframe (read from a file saved with indexes) into a long one.

    Arguments:
        wide_df:    The dataframe that should be changed into long format

    Returns:
        The dataframe in long format, with the former index as a column.
    """
    long_df = wide_df.transpose(copy = True).reset_index()
    # rename column names and drop the duplicated row
    long_df.columns = long_df.iloc[0]
    long_df = long_df.drop(long_df.index[0])
    # reset index again for consistency
    long_df = long_df.reset_index(drop = True)
    # and reset column naming
    long_df.columns.names = [None]
    return long_df


def construct_experiment_name(illumina_sheet: Optional[pathlib.Path] = None,
                              nanopore_sheet: Optional[pathlib.Path] = None,
                              runsheet_names: input_names.RunsheetNames = sheet_names) -> str:
    """Extract experiment name(s) from Illumina/Nanopore runsheet(s) and combine if needed.

    Arguments:
        illumina_sheet: the path to the Illumina runsheet, if given
        nanopore_sheet: the path to the Nanopore runsheet, if given
        runsheet_names: column and sheet names in the runsheet

    Returns:
        The experiment name, as extracted from the runsheet(s),
         combined with the Illumina run going first in case of hybrid runs.
    Raises:
        ValueError: if neither Illumina nor Nanopore sheet were specified
    """
    if not (illumina_sheet or nanopore_sheet):
        raise ValueError("No Illumina or Nanopore sheet given."
                         " At least one runsheet must be provided.")
    experiment_names = []
    if illumina_sheet:
        experiment_names.append(extract_illumina_run_name(illumina_sheet,
                                                          runsheet_names = runsheet_names))
    if nanopore_sheet:
        experiment_names.append(extract_nanopore_run_name(nanopore_sheet,
                                                          runsheet_names = runsheet_names))
    run_name = "_".join(experiment_names)
    return run_name


def clean_gtdb_name(species_name: str) -> str:
    """Remove placeholders from GTDB species names.

    Arguments:
        species_name:   the species name to clean

    Returns:
        The species name without any placeholders.
    """
    if pd.isna(species_name) or not species_name:
        logger.warning("No GTDB species result given.")
        # all the possible ways we might get a missing name should still result in a string output
        return ""
    cleaned_name = re.sub(r"_[A-Z]+", "", species_name)
    return cleaned_name
