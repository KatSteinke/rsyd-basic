"""Parse typing results for different species-specific analyses."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pathlib
from textwrap import wrap

from typing import List

import pandas as pd

import localization_helpers
import pipeline_config
import version


default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF
__version__ = version.__version__


# TODO: can still change depending on what we decide on for input
def parse_seqsero(seqsero_result: pd.DataFrame) -> pd.DataFrame:
    """Parse SeqSero2 output and extract the identification and serotype.

    Arguments:
        seqsero_result: The output of SeqSero2 analysis in kmer mode

    Returns:
        Identification as supplementary ID, and serotype as serotype, in the format required for
        entry in MADS.
    """
    serotyping_for_report = pd.DataFrame()
    serotyping_for_report["proevenr"] = seqsero_result["Sample name"]
    serotyping_for_report["O_WGS supplerende type"] = seqsero_result["Predicted identification"]
    serotyping_for_report["O_WGS Serotype/gruppe"] = seqsero_result["Predicted serotype"]
    return serotyping_for_report


def parse_mlst(mlst_result: pathlib.Path) -> pd.DataFrame:
    """Parse the output of mlst and return the MLST type for the sample.

    Arguments:
        mlst_result:    Path to file containing mlst results in tab-separated format

    Returns:
        MLST as MLST field in the format required for entry in MADS.
    """
    # the output is a single tab-delimited line
    mlst_results = pd.read_csv(mlst_result, sep="\t", usecols = [0, 2],
                               names = ["proevenr", "O_WGS MLST"], dtype = str)
    # if the file is empty, it's easier to make a dummy
    if mlst_results.empty:
        mlst_results = pd.DataFrame(data={"proevenr": [""], "O_WGS MLST": ["-"]})
    mlst_results["proevenr"] = mlst_results["proevenr"].apply(lambda path_to_sample:
                                                              pathlib.Path(path_to_sample).stem)
    mlst_results["O_WGS MLST"] = mlst_results["O_WGS MLST"].fillna(value="-")
    return mlst_results


def parse_serotype_finder(serotype_report: pd.DataFrame, sample_format: str) -> pd.DataFrame:
    """Extract the predicted E. coli serotype from a SeroTypeFinder report.

    Arguments:
        serotype_report:    the serotype report as extracted from the tab-separated output of
                            SeroTypeFinder
        sample_format:      sample number format as regex

    Returns:
        The sample number and predicted serotype in a format required for entry in MADS.
    """
    serotype_for_mads = pd.DataFrame()
    # if the dataframe is entirely blank, return early
    if serotype_report.isna().all(axis=None):
        # we cannot infer sample number - it has to stay blank
        serotype_for_mads["proevenr"] = pd.NA
        serotype_for_mads["O_WGS Serotype/gruppe"] = pd.NA
        return serotype_for_mads
    # sample number is encoded in contig name - the format may already contain a group, so we
    # extract everything and get the sample number by name
    sample_number = serotype_report["Contig"].str.extract(f"^(?P<proevenr>{sample_format})_")
    # there are entries for each DB hit (H and O genes) - collapse this down to one
    serotype_for_mads["proevenr"] = sample_number["proevenr"].unique()

    # sanity check:
    # there should be only one H type - if not, we have an issue
    # TODO: do we need to filter for quality?
    h_types = list(serotype_report.query("Database == 'H_type'")["Serotype"].unique())
    # if we have too many or no results, something's wrong
    if len(h_types) > 1 or not h_types:
        h_type = "H?"
    else:
        h_type = h_types[0]
    # O units depend on two genes, either wzx/wzy or wzm/wzt - both should result in the same
    # serotype
    o_types = list(serotype_report.query("Database == 'O_type'")["Serotype"].unique())
    if len(o_types) > 1 or not o_types:
        o_type = "O?"
    else:
        o_type = o_types[0]

    serotype_for_mads["O_WGS Serotype/gruppe"] = f"{o_type}:{h_type}"

    return serotype_for_mads


def trim_to_max_length(to_trim: List[str], max_length: int, delimiter: str = "|") \
        -> List[List[str]]:
    """Trim a list so that the string resulting from joining by the specified delimiter doesn't
    exceed the given maximum length.

    Arguments:
        to_trim:    the list of results to trim
        max_length: the maximum length the resulting string may have
        delimiter:  the delimiter with which the list will be joined

    Returns:
        The list trimmed to the maximum permitted length, with the remainder in another list.
    """
    if len(delimiter) > max_length:
        raise ValueError("The desired delimiter is already longer than the maximum text length.")
    if any(len(result) > max_length for result in to_trim):
        raise ValueError("The list contains items that are longer than the maximum text length.")
    trimmed = to_trim.copy()
    text_to_trim = delimiter.join(trimmed)
    too_long_entries = []
    # check if the resulting string would be longer than allowed if joined
    while len(text_to_trim) > max_length:
        #   if it is, keep moving these to a new list  (insert each entry before the previous) until
        #   it fits
        too_long_entries.insert(0, trimmed.pop())
        text_to_trim = delimiter.join(trimmed)
    if not too_long_entries:
        return [trimmed]
    return [trimmed, *trim_to_max_length(too_long_entries, max_length, delimiter)]


def parse_plasmidfinder(plasmidfinder_report: pathlib.Path, sample_format: str) -> pd.DataFrame:
    """Extract predicted plasmids (if any) from PlasmidFinder report and report them
    for entry in LIS, with line breaks if required.

    Arguments:
        plasmidfinder_report:   the tab-separated output of plasmidfinder
        sample_format:          sample number format as regex

    Returns:
        The sample number and predicted plasmid(s) for the sample.
    """
    max_field_length = 50
    delimiter = ";"
    plasmids_for_mads = pd.DataFrame()
    try:
        plasmidfinder_data = pd.read_csv(plasmidfinder_report, sep="\t")
    # if we get an empty file, return early
    except pd.errors.EmptyDataError:
        plasmids_for_mads["proevenr"] = pd.NA
        plasmids_for_mads["O_WGS Plasmid-ID_0"] = pd.NA
        return plasmids_for_mads
    if plasmidfinder_data.isna().all(axis=None):
        # we cannot infer sample number - it has to stay blank
        plasmids_for_mads["proevenr"] = pd.NA
        plasmids_for_mads["O_WGS Plasmid-ID_0"] = pd.NA
        return plasmids_for_mads
    sample_number = plasmidfinder_data["Contig"].str.extract(f"^(?P<proevenr>{sample_format})_")
    plasmids_for_mads["proevenr"] = sample_number["proevenr"].unique()
    plasmid_hits = sorted(plasmidfinder_data["Plasmid"].tolist())
    # try to split columns nicely
    try:
        plasmid_columns = trim_to_max_length(plasmid_hits, max_field_length, delimiter)
        plasmid_columns = [delimiter.join(plasmids) for plasmids in plasmid_columns]
    # we can assume it only breaks if a single column is longer than the maximum - in this case
    # we break every 50th character regardless of what that breaks
    except ValueError:
        plasmid_columns = wrap(delimiter.join(plasmid_hits), max_field_length)
    # for each column, add a name (resistance_n)
    plasmid_data = pd.DataFrame(data = {f"O_WGS Plasmid-ID_{n}": [plasmid_columns[n]]
                                        for n in range(len(plasmid_columns))})
    plasmid_data["proevenr"] = plasmids_for_mads["proevenr"]
    plasmid_col_order = plasmid_data.columns.tolist()
    plasmid_col_order.remove("proevenr")
    plasmid_col_order.insert(0, "proevenr")
    return plasmid_data[plasmid_col_order]


# TODO: where to supply a custom file? Should we even?
def parse_abritamr(abritamr_report: pd.DataFrame,
                   resistance_overview: pathlib.Path = (pathlib.Path(__file__).parent / "data"
                                                        / "amr" / "reduced_classes.csv")) \
        -> pd.DataFrame:
    """Parse abritAMR report and report names of selected resistance genes.

    Arguments:
        abritamr_report:        abritAMR's abritamr.txt report, containing full, partial and
                                imperfect matches for categories of antibiotics
        resistance_overview:    mapping of abritAMR categories to internal categories

    Returns:
        The isolate number and selected resistance genes found in the isolate's sequence (currently
        carbapenemase, other beta-lactamase, colistin and vancomycin resistance).
    """
    # initialize resistance data df
    resistance_data = pd.DataFrame(data={"proevenr": abritamr_report["Isolate"]})
    resistance_data["proevenr"] = resistance_data["proevenr"].apply(lambda sample_path:
                                                                    pathlib.Path(sample_path).stem)
    # get resistances from overview
    resistance_with_categories = pd.read_csv(resistance_overview, sep=";")
    resistance_with_categories = resistance_with_categories.dropna(subset=["KMA_subclass"])
    resistance_categories = resistance_with_categories["KMA_subclass"].unique().tolist()
    # for each set of antibiotic resistances:
    for category in resistance_categories:
        resistances_in_category = resistance_with_categories.query("`KMA_subclass` == @category")[
            "enhanced_subclass"].tolist()
        # filter report to only contain columns with the respective genes
        columns_matching_category = [column_name for column_name in abritamr_report.columns
                                     if column_name in resistances_in_category]
        if columns_matching_category:
            # ensure consistent sort order - one column can contain multiple genes
            genes_in_category = abritamr_report.loc[0,
                                                    columns_matching_category].apply(lambda res_genes:
                                                                                     sorted(res_genes.split(","))).tolist()
            genes_in_category = [gene for gene_list in genes_in_category for gene in gene_list]
            # concatenate all genes if found
            resistance_genes = ";".join(sorted(genes_in_category))
        else:
            resistance_genes = pd.NA
        resistance_data[category] = resistance_genes
    return resistance_data


def parse_toxins(abritamr_virulence: pd.DataFrame,
                 toxins_overview: pathlib.Path = (pathlib.Path(__file__).parent / "data"
                                                  / "toxins" / "toxins_for_report.csv"),
                 target_language: str = workflow_config["language"]) \
        -> pd.DataFrame:
    """Parse abritAMR virulence gene report and report selected toxin genes.

    Arguments:
        abritamr_virulence: abritAMR's report of virulence genes found in the sample with AMRfinder+
        toxins_overview:    mapping of virulence gene names to names for internal reports
        target_language:    the language in which to give the virulence gene report

    Returns:
        The isolate number and long names of virulence genes
    """
    # column name has to be translated when passing it to generate_reports
    _ = localization_helpers.set_language(target_language)
    # initialize toxin data df
    toxin_data = pd.DataFrame(data = {"proevenr": abritamr_virulence["Isolate"]})
    toxin_data["proevenr"] = toxin_data["proevenr"].apply(lambda sample_path:
                                                          pathlib.Path(sample_path).stem)
    # return early if no virulence genes found
    if "Virulence" not in abritamr_virulence.columns:
        toxin_data[f"{_('O_WGS Toxinpåvisning')}_0"] = pd.NA
        return toxin_data
    # extract list of virulence genes
    toxins_in_sample = abritamr_virulence.at[0, "Virulence"].split(",")
    toxin_genes = pd.DataFrame(data={"toxin_gene": toxins_in_sample})
    # only keep those we should report
    # get the toxins we want to report and deduplicate
    toxins_to_report = pd.read_csv(toxins_overview, sep=";")
    toxins_to_report = toxins_to_report.drop_duplicates()
    # keep only the genes that are in the list we want
    toxins_in_sample = toxins_to_report.merge(toxin_genes, left_on="gene", right_on="toxin_gene")
    # get KMA names for all of them and report those - one name may cover multiple genes
    toxin_kma_names = set(toxins_in_sample["MADS_NAVN"].tolist())
    # we may have virulence hits but nothing we want
    if not toxin_kma_names:
        toxin_data[f"{_('O_WGS Toxinpåvisning')}_0"] = pd.NA
        return toxin_data
    toxin_kma_names = sorted(list(toxin_kma_names))
    for gene_count, toxin_gene in enumerate(toxin_kma_names):
        toxin_data[f"{_('O_WGS Toxinpåvisning')}_{gene_count}"] = toxin_gene
    return toxin_data


def parse_gtdbtk(gtdb_report: pathlib.Path) -> pd.DataFrame:
    """Extract GTDB-Tk genus and species call (if present) for all samples that could be analyzed.

    Arguments:
        gtdb_report:   path to the tab-separated overview produced by GTDB-Tk

    Returns:
        GTDB-Tk genus and species call; species can be "s__" if no species was found.
    """
    try:
        taxo_summary = pd.read_csv(gtdb_report, sep = "\t")
    except pd.errors.EmptyDataError:
        sample_and_classification = pd.DataFrame(data={"proevenr": [pd.NA],
                                                       "WGS_genus_GTDB": [pd.NA],
                                                       "WGS_species_GTDB": [pd.NA],
                                                       "ANI_til_ref": [pd.NA]})
        return sample_and_classification
    sample_and_classification = taxo_summary[['user_genome', 'classification',
                                              'fastani_ani']].copy()
    # just extract genus and species (for now)
    taxo_string = r'g__(?P<genus>\w+);s__(?P<species>[-A-Za-z _]*)'
    sample_and_classification[['WGS_genus_GTDB',
                               'WGS_species_GTDB']] = sample_and_classification[
        'classification'].str.extract(taxo_string)
    sample_and_classification = sample_and_classification.rename(columns = {'user_genome':
                                                                            'proevenr',
                                                                            'fastani_ani':
                                                                                'ANI_til_ref'})
    sample_and_classification[["WGS_genus_GTDB",
                               "WGS_species_GTDB"]] = sample_and_classification[["WGS_genus_GTDB",
                                                                                 "WGS_species_GTDB"]].fillna("Unclassified")
    return sample_and_classification[['proevenr', 'WGS_genus_GTDB', 'WGS_species_GTDB',
                                      'ANI_til_ref']]
