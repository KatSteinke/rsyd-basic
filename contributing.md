# How to contribute
Contributions to this pipeline - features you want, bugfixes, options for your own lab's systems,... - are very welcome!
To make sure everyone's contributions work together well and can be maintained, there's a few guidelines to keep in 
mind.
## Getting started
* **Create a Github account** if you don't have one yet
* **Submit an issue** for the change you want to make - this helps avoid multiple people working on the same issue without 
knowing. You may also want to share it on the BinfNet Slack in the appropriate channel.
## Working on the code
* **Fork the repo** on Github
* **Make a new branch** based on the `master` branch - don't work on `master` directly
* Try and make commits in **logical units** so it's easy to see what changed from commit to commit. This means a commit
should only contain changes related to *one* task - it could be a one-liner in one file or a bunch of related changes in
all of them.
* **Make sure things work** before you commit. Use **unit tests** to check that your code does what you want it to do, 
and structure your code in a way that makes it easy for you to test wherever possible.
* **Don't** add anything that could contain patient data to your commits. This includes real-life sample numbers. There 
are ways of filtering these from your commit history, but they tend to make an unholy mess for everyone involved.
* Ideally, describe your commits **briefly**. Include the issue number, note the component, and give a brief summary (in 
"imperative form" e.g. "helpers: fix issue 123"). This again makes it easy to see what's going on.
* Read the project's style guidelines (see below) so your code blends well with the rest and is easy to
understand and maintain
## Style guidelines
To make code easily readable for everyone (including yourself when you get back from your summer holiday):
* use **descriptive** names for variables, functions, rules and classes wherever possible (and sensible): e.g.
  * `sequences = {"seq_1.fq", "seq_2.fq"}`; 
  * `primer_version = 3`; 
  * `get_amplicon_coverage(sequences, primer_version)`
* be careful with naming variables something that includes their type (e.g. `sequence_dict`) - if you later realize a
list would do the job you now need to rename the variable. 
* **comment** your code if needed. The "right amount" of comments is an individual thing, but as a general suggestion:
focus on commenting **why** you're doing something rather than what you're doing. However, there are some points (e.g.
more complicated regular expressions) where it makes sense to explain what you're doing as well.

**Note**: The goal is to have some code guidelines we all can agree on so it's easy to understand each other's code. 
This does mean there'll be a final version of this style guide, but until then it's not set in stone.
So if you feel something is missing, or something doesn't make sense, bring it up, 
either in an issue or on BinfNet. 

### Python style guidelines
Some details are Python-specific:
* try to keep close to [PEP8](https://www.python.org/dev/peps/pep-0008/)
* use **type hints** in function definitions. This helps understand what should go into a function, and what the result
should be. For example:
```python
def answer_everything(question: str) -> int:
    return 42
```
* **write a docstring** for your function, briefly describing what it does and what the input and output are. For 
example:
```python
def answer_everything(question: str) -> int:
    """Answer questions about life, the universe and everything.
    Arguments:
        question: a question about life, the universe and everything
    Returns:
        the answer to the question
    """
    return 42
```
When in doubt, look at what's already in place, or ask around. 

## Submitting the change
* Once everything works and you've cleaned up the code, push the changes to the branch you created in your fork of the 
repo 
* Submit a pull request to the main repo 
* Link the pull request in the issue you created before so it's easy to see what belongs to what
* The maintainer(s) will try and get back to you as soon as possible
