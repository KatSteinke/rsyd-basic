__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pathlib
import re

import pandas as pd

import get_analysis_subsets
import get_lis_report
import helpers
import localization_helpers
import parse_nanopore_report
import pipeline_config
import snake_helpers
import version


__version__ = version.__version__


# set default config if none is provided - will be overridden in most setups
configfile: pipeline_config.default_config_file
# path to config file needs to be specified for other scripts
CONFIG_PATH = config["config_path"] if "config_path" in config \
              else pipeline_config.default_config_file

NEXTFLOW_CONFIG = config["nextflow_config"]

# initialize input language settings
sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(config)


# define samples - get from Bactopia FOFN - TODO: split into different downstream processing later?
OUTDIR = config['outdir']
if config["debug"]:
    print(f"Outputting to {OUTDIR}")
print(list(pathlib.Path("/bactopia_datasets/minmer").iterdir()))
sample_overview = pathlib.Path(config["input_samples"])
samples_for_bactopia = pd.read_csv(sample_overview, sep="\t", dtype = {'sample': str})
ALL_SAMPLES = samples_for_bactopia["sample"].tolist()

# set up Nanopore-specific stuff - empty if not needed
nanopore_seq_report = []
run_params = None
basecall_mode = "illumina"


# experiment name can be extracted at least if we're using an Illumina style runsheet
# TODO: hybrid handling
if config["sequencing_mode"] == "illumina":
    EXPERIMENT_NAME = helpers.construct_experiment_name(illumina_sheet =
                                                        pathlib.Path(config["runsheet"]),
                                                        runsheet_names = sheet_names)
else:  # assume it's nanopore
    EXPERIMENT_NAME = helpers.construct_experiment_name(nanopore_sheet =
                                                        pathlib.Path(config["runsheet"]),
                                                        runsheet_names = sheet_names)
    # given it's nanopore, we want run params
    nanopore_seq_report = snake_helpers.get_nanopore_run_report(samples_for_bactopia)
    run_params = parse_nanopore_report.get_current_run_params(nanopore_seq_report)
    basecall_mode = run_params.basecalling_mode


# check samples: does this have illumina reads? Does it have nanopore reads?
samples_by_sequencer = get_analysis_subsets.get_samples_by_sequencer(samples_for_bactopia)
ILLUMINA_ONLY = samples_by_sequencer.illumina
HYBRID_SAMPLES = samples_by_sequencer.hybrid
NANOPORE_ONLY = samples_by_sequencer.nanopore
# Nanopore reads: "extra" has to contain the path to the barcode directory
ALL_NANOPORE = samples_by_sequencer.all_nanopore
ALL_ILLUMINA = samples_by_sequencer.all_illumina

print(ALL_ILLUMINA)

# when using Nomad use of LIS features needs to be fed into the pipeline as an int
# since it gets turned into
use_lis = bool(int(config["lab_info_system"]["use_lis_features"]))
# TODO: the config value gets read as a string -> always True. bool(int(value))?
if use_lis:
    print(f'Looking at lab info system results '
          f'because use_lis_features is {use_lis}')
    # get samples by species
    mads_report = get_lis_report.get_report_with_isolate_number(lis_report = config["lab_info_system"]["lis_report"],
        translate_to = config["sample_number_settings"]["sample_numbers_report"],
        translate_from = config["sample_number_settings"]["sample_numbers_out"],
        from_format = re.compile(config["sample_number_settings"]["format_in_lis"]),
        to_format = re.compile(config["sample_number_settings"]["format_output"]),
        lis_data_names = lis_names
    )
    # get "broad" species coding (all subspecies, serovars etc. collapsed into one)
    # we're going to use bacteria category/code a LOT, shorten them for readability
    bact_category = bact_names.bacteria_category
    bact_code = bact_names.bacteria_code
    species_coding = pd.read_csv(config["lab_info_system"]["bacteria_codes"],sep = ";",
        encoding = "latin-1",
        dtype = {bact_code: str})

    # get samples with broad species for genome size estimation
    # TODO: handle this in the script?
    samples_with_species = mads_report.merge(samples_for_bactopia, left_on="proevenr",
                                             right_on="sample", how="inner")
    samples_with_species["species_kategori"] = samples_with_species[lis_names.bacteria_code].apply(lambda
                                                                                         bacteria_code:
                                                                                         helpers.get_species_by_code(
                                                                                             bacteria_code,
                                                                                             species_coding,
                                                                                         bacteria_names = bact_names)
                                                                                         )

    # get species codes for analyses that need to be applied to multiple organisms

    ECOLI_CODES = species_coding[species_coding[bact_category] == "Escherichia coli"][bact_code]
    KLEBSIELLA_CODES = species_coding[species_coding[bact_category].str.startswith("Klebsiella")][
        bact_code]
    PSEUDOMONAS_CODES = species_coding[species_coding[bact_category].str.startswith("Pseudomonas")][
        bact_code]
    ACINETOBACTER_CODES = \
    species_coding[species_coding[bact_category].str.startswith("Acinetobacter")][bact_code]

    SALMONELLA_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,
        mads_report,
        species_coding[species_coding[bact_category] == "Salmonella enterica"][bact_code],
        lis_names)
    ECOLI_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        ECOLI_CODES,
        lis_names)
    LISTERIA_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        species_coding[species_coding[bact_category] == "Listeria monocytogenes"][bact_code],
        lis_names)
    LEGIONELLA_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        species_coding[species_coding[bact_category].str.startswith("Legionella")][bact_code],
        lis_names)
    VIBRIO_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        species_coding[species_coding[bact_category] == "Vibrio cholerae"][bact_code],
        lis_names)
    KLEBSIELLA_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        KLEBSIELLA_CODES,
        lis_names)
    STAPH_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia,mads_report,
        species_coding[species_coding[bact_category] == "Staphylococcus aureus"][bact_code],
        lis_names)
    CPO_SAMPLES = get_analysis_subsets.get_samples_by_bact_id(samples_for_bactopia, mads_report,
                                            [*ECOLI_CODES, *KLEBSIELLA_CODES, *PSEUDOMONAS_CODES,
                                             *ACINETOBACTER_CODES],
        lis_names)
else:
    mads_report = None
    species_coding = None
    samples_with_species = None
    SALMONELLA_SAMPLES = []
    ECOLI_SAMPLES = []
    LISTERIA_SAMPLES = []
    LEGIONELLA_SAMPLES = []
    VIBRIO_SAMPLES = []
    KLEBSIELLA_SAMPLES = []
    STAPH_SAMPLES = []
    CPO_SAMPLES = []

# TODO: analyses by request (e.g. everything that gets marked as resistance?)
sheet_data = pd.read_excel(config["runsheet"], sheet_name = sheet_names.sample_info_sheet)
# remove any blank sample numbers
sheet_data = sheet_data.dropna(subset=[sheet_names.sample_number])
sheet_data[sheet_names.sample_number] = sheet_data[sheet_names.sample_number].apply(lambda sample_number:
                                                    helpers.rearrange_sample_number(sample_number,
                                                                                    re.compile(config["sample_number_settings"]["format_in_sheet"]),
                                                                                    re.compile(config["sample_number_settings"]["format_output"]).groupindex))

TOXIN_SAMPLES = snake_helpers.get_toxin_samples(sheet_data, runsheet_names = sheet_names)

SAMPLE_NUMBER_FORMAT = config["sample_number_settings"]["sample_number_format"]

# constrain sample number sample format to avoid reading experiment name
wildcard_constraints:
    sample = SAMPLE_NUMBER_FORMAT


workdir: OUTDIR

# TODO: what if there are no assemblies of one type? conditional run_all variants?
# for now: generate lists with mini-functions - can definitely be prettier...


rule run_all:
    input:
        all_assemblies = snake_helpers.get_all_assemblies(ILLUMINA_ONLY, NANOPORE_ONLY,
            HYBRID_SAMPLES, experiment_name = EXPERIMENT_NAME),
        qc_all = expand("quast_all/transposed_report_{platform}.tsv",
                        platform= snake_helpers.get_qc_platforms(ILLUMINA_ONLY, NANOPORE_ONLY,
                            HYBRID_SAMPLES)),
        checkm_for_all= "checkm_all_samples/checkm-results.txt",
        sequence_type_all = expand("{sample}/mlst/{sample}_sequence_type.tsv",
                                   sample=ALL_SAMPLES),
        #compress_all_nanopore = expand("{sample}/nanopore_reads/{sample}.downsample.fastq.gz",
        #                               sample=ALL_NANOPORE),
        # TODO: how many QC things are dependencies of something else?
        # TODO: is there a way to drop them if empty?
        all_fastqscan_orginal = expand("{sample}/quality-control/summary/{sample}_R1_original.json",
            sample=ALL_ILLUMINA),
        all_fastqscan_final = expand("{sample}/quality-control/summary/{sample}_R1_final.json",
            sample=ALL_ILLUMINA),
        all_fastqc = expand("{sample}/quality-control/summary/{sample}_R1_fastqc.html",
            sample=ALL_ILLUMINA),
        all_single_quast = snake_helpers.get_all_single_quast(ILLUMINA_ONLY, NANOPORE_ONLY,
            HYBRID_SAMPLES),
        all_annotated = expand("{sample}/annotation/{sample}.gbk",
            sample=ALL_SAMPLES),
        all_abritamr_report= expand("{sample}/abritamr/abritamr.txt", sample=ALL_SAMPLES),
        # TODO: functions for mash depending on what exists
        mash_refseq_illumina = expand("{sample}/minmers/check_mash_query_illumina",
                                      sample=ALL_ILLUMINA),
        mash_refseq_nanopore = expand("{sample}/minmers/check_mash_query_nanopore",
                                      sample=ALL_NANOPORE),
        all_sourmash_illumina = expand("{sample}/minmers/{sample}-sourmash-genbank-k21_illumina.txt",
                                        sample=ALL_ILLUMINA),
        all_sourmash_nanopore = expand("{sample}/minmers/{sample}-sourmash-genbank-k21_nanopore.txt",
                                       sample = ALL_NANOPORE),
        all_kraken = expand("{sample}/kraken/{sample}_report.tsv", sample=ALL_SAMPLES),
        all_wide_reports = expand("{sample}/for_mads/{sample}_mads_wide.tsv", sample=ALL_SAMPLES)
                           if use_lis else [],
        summary_report = f"raw_results/{EXPERIMENT_NAME}_all_results.tsv",
        mads_wide = f"raw_results/{EXPERIMENT_NAME}_mads_wide.tsv" if use_lis else []

# estimate genome size: TODO: borrowed from bactopia right now:
# https://github.com/bactopia/bactopia/blob/5e627da50ae4f91ed4389087e687ff346c657c18/modules/local/bactopia/gather_samples/main.nf
# but there has to be a better way

# TODO: split into illumina or nanopore
rule estimate_genome_size_illumina:
    input:
        # extract read locations from FOFN
        reads_r1 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
                                                                           samples_for_bactopia,
                                                                           "r1"),
        reads_r2 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
                                                                           samples_for_bactopia,
                                                                           "r2")
    output:
        mash_genome_size = "{sample}/{sample}_genome_size.txt"
    params:
        min_kmer_count = 10 if config["platform"] == "NextSeq" else 3
    conda: "minmers_env"
    resources:
        mem_mb = 200  # rough estimate for now
    shell:
        """
        mash sketch -o test -k 31 -m {params.min_kmer_count} -r "{input.reads_r1}" "{input.reads_r2}" 2>&1 | \
        grep "Estimated genome size:" | \
        awk '{{if($4){{printf("%d\\n", $4)}}}} END {{if (!NR) print "0"}}' > "{output.mash_genome_size}" || echo "0" > "{output.mash_genome_size}"
        """

# as we may be dealing with individual sequences from multiple runs we may need to handle both
# zipped and unzipped reads

rule concatenate_fastqs:
    params:
        barcode_dir = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
                                                                           samples_for_bactopia,
                                                                           "extra"),
        file_format = lambda wildcards: "fastq.gz" if snake_helpers.is_gzipped(wildcards.sample,
                                                                                samples_for_bactopia)
                                                    else "fastq",
        concatenate = lambda wildcards: "zcat" if snake_helpers.is_gzipped(wildcards.sample,
                                                                           samples_for_bactopia)
                                               else "cat"
    output:
        concat_fasta = temp("{sample}/nanopore_reads/{sample}.reads.fastq")
    message: "# Transferring fastq files for sample {wildcards.sample}...."
    log:
        "logs/concat_fastq/{sample}_log.txt"
    resources:
        mem_mb = 200
    shell:
        """
         find \
         {params.barcode_dir} \
            -type f \
            -name "*{params.file_format}" \
            -exec {params.concatenate} {{}} \; \
            > {output.concat_fasta} || touch {output.concat_fasta}

         if [ ! -s {output.concat_fasta} ]; then
            echo "No reads were found for {wildcards.sample}. No consensus is generated."
            echo "No reads were found for {wildcards.sample}. No consensus is generated." > {log}
         fi
        """

rule clean_nanopore_reads:
    input:
        concat_fastq = "{sample}/nanopore_reads/{sample}.reads.fastq"
    output:
        filtered_fastq = temp("{sample}/nanopore_reads/{sample}.filtered.fastq")
    params:
        min_length = 1000
    resources:
        mem_mb = 200
    conda: "nanopore_qc_env"
    shell:
        """
        filtlong --min_length {params.min_length} --keep_percent 95 {input.concat_fastq} >  {output.filtered_fastq}
        """


# run on cleaned reads! Try to see if keeping the best is enough
rule estimate_size_nanopore:
    input:
        filtered_fastq = "{sample}/nanopore_reads/{sample}.filtered.fastq"
    output:
        mash_genome_size = "{sample}/{sample}_genome_size_nanopore.txt"
    conda: "minmers_env"
    resources:
        mem_mb = 200  # rough estimate for now
    shell:
        """
        mash sketch -o test -k 31 -m 10 -r "{input.filtered_fastq}" 2>&1 | \
        grep "Estimated genome size:" | \
        awk '{{if($4){{printf("%d\\n", $4)}}}} END {{if (!NR) print "0"}}' > "{output.mash_genome_size}" || echo "0" > "{output.mash_genome_size}"
        """

rule get_nanopore_size_lis:
    input:
        filtered_fastq = "{sample}/nanopore_reads/{sample}.filtered.fastq"
    output:
        lis_genome_size = "{sample}/{sample}_genome_size_nanopore_lis.txt"
    conda: "minmers_env"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        lis_species = lambda wildcards: samples_with_species[samples_with_species["proevenr"] == wildcards.sample]["species_kategori"].squeeze()
    resources:
        mem_mb = 200  # rough estimate for now
    shell:
        """
        python3 "{params.snakemake_path}/look_up_size.py" "{params.lis_species}" | xargs printf "%.0f\\n" > "{output.lis_genome_size}" || {{ mash sketch -o test -k 31 -m 10 -r "{input.filtered_fastq}" 2>&1 | \
        grep "Estimated genome size:" | \
        awk '{{if($4){{printf("%d\\n", $4)}}}} END {{if (!NR) print "0"}}' > "{output.lis_genome_size}" || echo "0" > "{output.lis_genome_size}" ; }}
        """

# second filtering pass - quality-based again
rule downsample_nanopore:
    input:
        filtered_fastq = "{sample}/nanopore_reads/{sample}.filtered.fastq",
        genome_size = "{sample}/{sample}_genome_size_nanopore_lis.txt" if use_lis else f"{{sample}}/{{sample}}_genome_size_nanopore.txt"
    output:
        downsampled_fastq = temp("{sample}/nanopore_reads/{sample}.downsample.fastq")
    params:
        target_coverage = 100
    resources:
        mem_mb = 200
    conda: "nanopore_qc_env"
    log: "{sample}/logs/quality-control/nanopore_downsample.log"
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.genome_size}")
         if [ $GENOME_SIZE -gt 0 ] ; then
                TOTAL_BP=$(( {params.target_coverage}*"${{GENOME_SIZE}}" ))
                echo "${{TOTAL_BP}}"
                filtlong --target_bases "${{TOTAL_BP}}" {input.filtered_fastq} >  {output.downsampled_fastq}
        else
                echo "Genome size for {wildcards.sample} predicted to be 0. Skipping downsampling." >> {log}
                cp "{input.filtered_fastq}" "{output.downsampled_fastq}" 
        fi
        """



# clean reads
rule clean_reads:
    input:
        # extract read locations from FOFN
        reads_r1 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r1"),
        reads_r2 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r2")
    output:
        cleaned_r1 = temp("{sample}/quality-control/{sample}_trim_adapter_R1.fastq"),
        cleaned_r2 = temp("{sample}/quality-control/{sample}_trim_adapter_R2.fastq")
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        read_check = "{sample}/logs/quality-control/check_cleaned_reads.log"
    log:
        reads_log = "{sample}/logs/quality-control/clean_reads.log"
    threads: workflow.cores / 4 if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 800
    conda: "polish_ilm"
    # the job deliberately fails if the created files are malformed - retry within the workflow
    retries: 3
    # first check if the files actually contain anything
    # check for the correct file type after running: if it's not a text file something's gone wrong
    # check that read counts match
    # TODO: more flexible handling of cat vs zcat? -make compression detection more flexible?
    shell: # TODO: make configurable?
        """
        read_1_len=$(zcat {input.reads_r1} | wc -l)
        read_2_len=$(zcat {input.reads_r2} | wc -l)
        if [  $read_1_len -gt 0 -a $read_2_len -gt 0 ]; then
            bbduk.sh in="{input.reads_r1}" in2="{input.reads_r2}" \
            out="{output.cleaned_r1}" out2="{output.cleaned_r2}" \
            ref=adapters \
            k=23 \
            ktrim=r \
            mink=11 \
            hdist=1 \
            tpe=t \
            tbo=t \
            threads={threads} \
            ftm=5 \
            qin=auto ordered=t \
             -Xmx{resources.mem_mb}m >> "{log.reads_log}" 2>&1
            python3 "{params.snakemake_path}/check_reads.py" "{output.cleaned_r1}" "{output.cleaned_r2}" >> "{params.read_check}" 2>&1
        else
            echo "No reads found for {wildcards.sample}." >> {log.reads_log}
            touch "{output.cleaned_r1}" "{output.cleaned_r2}"
        fi
        """

rule clean_phix:
    input:
        cleaned_r1 = "{sample}/quality-control/{sample}_trim_adapter_R1.fastq",
        cleaned_r2 = "{sample}/quality-control/{sample}_trim_adapter_R2.fastq"
    output:
        no_phix_r1 = temp("{sample}/quality-control/{sample}_trim_phix_R1.fastq"),
        no_phix_r2 = temp("{sample}/quality-control/{sample}_trim_phix_R2.fastq")
    conda: "polish_ilm"
    threads: workflow.cores / 4 if (workflow.cores / 4) < 16 else 16
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        read_check= "{sample}/logs/quality-control/check_clean_phix.log"
    resources:
        mem_mb = 800
    # the job deliberately fails if the created files are malformed - retry within the workflow
    retries: 3
    log:
        reads_log = "{sample}/logs/quality-control/clean_phix.log"
    shell:  # TODO: make configurable?
        """
        read_1_len=$(cat {input.cleaned_r1} | wc -l)
        read_2_len=$(cat {input.cleaned_r2} | wc -l)
        if [  $read_1_len -gt 0 -a $read_2_len -gt 0 ]; then
            bbduk.sh in="{input.cleaned_r1}" in2="{input.cleaned_r2}" \
            out="{output.no_phix_r1}" out2="{output.no_phix_r2}" \
            ref=phix \
            k=31 \
            hdist=1 \
            tpe=t \
            tbo=t \
            qtrim=rl \
            trimq=6 \
            minlength=35 \
            threads={threads} \
            minavgquality=10 \
            tossjunk=t \
            qin=auto qout=33 ordered=t \
             -Xmx{resources.mem_mb}m >> "{log.reads_log}" 2>&1
             python3 "{params.snakemake_path}/check_reads.py" "{output.no_phix_r1}" "{output.no_phix_r2}" >> "{params.read_check}" 2>&1 
         else
            echo "No reads found for {wildcards.sample}." >> {log.reads_log}
            touch "{output.no_phix_r1}" "{output.no_phix_r2}"
        fi
        """

rule correct_errors:
    input:
        no_phix_r1 = "{sample}/quality-control/{sample}_trim_phix_R1.fastq",
        no_phix_r2 = "{sample}/quality-control/{sample}_trim_phix_R2.fastq",
        mash_genome_size= "{sample}/{sample}_genome_size.txt"
    params:
        output_dir = "{sample}/quality-control/"
    output:
        corrected_r1 = temp("{sample}/quality-control/{sample}_trim_phix_R1.cor.fq"),
        corrected_r2 = temp("{sample}/quality-control/{sample}_trim_phix_R2.cor.fq")
    threads: 6  # diminishing returns after 6 threads, https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0509-9
    resources:
        mem_mb = 50
    log: "{sample}/logs/quality-control/lighter.log"
    conda: "assembly_env" # piggyback off shovill's lighter
    shell:
        """
        read_1_len=$(cat {input.no_phix_r1} | wc -l)
        read_2_len=$(cat {input.no_phix_r2} | wc -l)
        if [ $read_1_len -gt 0 -a  $read_2_len -gt 0 ]; then
            GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
            lighter -od "{params.output_dir}" -r "{input.no_phix_r1}" -r "{input.no_phix_r2}" -K 31 "${{GENOME_SIZE}}" \
            -maxcor 1 -zlib 0 -t {threads} &> "{log}" || touch "{output.corrected_r1}" "{output.corrected_r2}"
        else
            echo "No reads found for {wildcards.sample}." >> {log}
            touch "{output.corrected_r1}" "{output.corrected_r2}"
        fi
        """

# downsample reads
rule downsample:
    input:
        mash_genome_size = "{sample}/{sample}_genome_size.txt",
        corrected_r1 = "{sample}/quality-control/{sample}_trim_phix_R1.cor.fq",
        corrected_r2 = "{sample}/quality-control/{sample}_trim_phix_R2.cor.fq"
    output:
        downsampled_r1 = temp("{sample}/quality-control/{sample}_downsampled_R1.fastq"),
        downsampled_r2 = temp("{sample}/quality-control/{sample}_downsampled_R2.fastq")
    params:
        coverage = 100,
        sample_seed = 42,
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        read_check= "{sample}/logs/quality-control/check_downsample.log"
    log:
        read_log = "{sample}/logs/quality-control/downsample.log"
    resources:
        mem_mb = 800
    conda: "polish_ilm"
    # the job deliberately fails if the created files are malformed - retry within the workflow
    retries: 3
    # somewhat adapted from
    # https://github.com/bactopia/bactopia/blob/dbc5755fb6790d183a051533afdc09cf73c21c92/modules/local/bactopia/qc_reads/main.nf
    shell:
        """
        read_1_len=$(cat {input.corrected_r1} | wc -l)
        read_2_len=$(cat {input.corrected_r2} | wc -l)
        if [  $read_1_len -gt 0 -a $read_2_len -gt 0 ]; then
            GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
            if [ $GENOME_SIZE -gt 0 ] ; then
                TOTAL_BP=$(( {params.coverage}*"${{GENOME_SIZE}}" ))
                echo "${{TOTAL_BP}}"
                reformat.sh in="{input.corrected_r1}" in2="{input.corrected_r2}" \
                    out="{output.downsampled_r1}" out2="{output.downsampled_r2}" \
                    samplebasestarget="${{TOTAL_BP}}" \
                    sampleseed={params.sample_seed} \
                    overwrite=t \
                    -Xmx{resources.mem_mb}m >> "{log.read_log}" 2>&1
                python3 "{params.snakemake_path}/check_reads.py" "{output.downsampled_r1}" "{output.downsampled_r2}" >> "{params.read_check}" 2>&1
            else
                echo "Genome size for {wildcards.sample} predicted to be 0. Skipping downsampling." >> {log.read_log}
                cp "{input.corrected_r1}" "{output.downsampled_r1}" 
                cp "{input.corrected_r2}" "{output.downsampled_r2}"
            fi
        else
            echo "No reads found for {wildcards.sample}." >> {log.read_log}
            touch "{output.downsampled_r1}" "{output.downsampled_r2}"
        fi
        """

# compress reads
rule compress_reads:
    input:
        downsampled_r1 = "{sample}/quality-control/{sample}_downsampled_R1.fastq",
        downsampled_r2 = "{sample}/quality-control/{sample}_downsampled_R2.fastq"
    output:
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz"
    conda: "annotate_env"  # borrow annotation env's pigz install
    threads: 2
    resources:
        mem_mb = 100
    shell:
        """
        pigz -p {threads} -c -n "{input.downsampled_r1}" > "{output.compressed_r1}"
        pigz -p {threads} -c -n "{input.downsampled_r2}" > "{output.compressed_r2}"
        """

rule compress_nanopore_reads:
    input:
        downsampled_nanopore = "{sample}/nanopore_reads/{sample}.downsample.fastq"
    output:
        compressed_nanopore = "{sample}/nanopore_reads/{sample}.downsample.fastq.gz"
    conda: "annotate_env"  # borrow annotation env's pigz install
    threads: 2
    resources:
        mem_mb = 100
    shell:
        """
        pigz -p {threads} -c -n "{input.downsampled_nanopore}" > "{output.compressed_nanopore}"
        """

rule fastq_scan_originals:
    input:
        reads_r1 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r1"),
        reads_r2 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r2"),
        mash_genome_size= "{sample}/{sample}_genome_size.txt",
    output:
        scan_original_r1 = "{sample}/quality-control/summary/{sample}_R1_original.json",
        scan_original_r2 = "{sample}/quality-control/summary/{sample}_R2_original.json"
    conda: "ilm_read_qc"
    resources:
        mem_mb = 200  # guessing for now
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
        gzip -cd "{input.reads_r1}" | fastq-scan -g "${{GENOME_SIZE}}" > "{output.scan_original_r1}"
        gzip -cd "{input.reads_r2}" | fastq-scan -g "${{GENOME_SIZE}}" > "{output.scan_original_r2}"
        """

rule fastq_scan_final:
    input:
        downsampled_r1 = "{sample}/quality-control/{sample}_downsampled_R1.fastq",
        downsampled_r2 = "{sample}/quality-control/{sample}_downsampled_R2.fastq",
        mash_genome_size = "{sample}/{sample}_genome_size.txt"
    output:
        scan_final_r1 = "{sample}/quality-control/summary/{sample}_R1_final.json",
        scan_final_r2 = "{sample}/quality-control/summary/{sample}_R2_final.json"
    conda: "ilm_read_qc"
    resources:
        mem_mb = 200  # guessing for now
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
        cat "{input.downsampled_r1}" | fastq-scan -g "${{GENOME_SIZE}}" > "{output.scan_final_r1}"
        cat "{input.downsampled_r2}" | fastq-scan -g "${{GENOME_SIZE}}" > "{output.scan_final_r2}"
        """

# run fastQC on original and cleaned reads
rule fastqc:
    input:
        reads_r1 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r1"),
        reads_r2 = lambda wildcards: snake_helpers.get_reads_from_overview(wildcards.sample,
            samples_for_bactopia,
            "r2"),
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz"
    params:
        outdir = "{sample}/quality-control/summary"
    output: # TODO how to handle Illumina sample raw names
        fastqc_corrected_1 = "{sample}/quality-control/summary/{sample}_R1_fastqc.html",
        fastqc_corrected_2 = "{sample}/quality-control/summary/{sample}_R2_fastqc.html"
    # use -o to put all output in desired output dir!
    conda: "ilm_read_qc"
    threads: 1
    resources:
        mem_mb = 512  # 512 * threads - adjust accordingly
    shell:
        """
        fastqc --noextract -f fastq -t {threads} -o "{params.outdir}" \
         "{input.reads_r1}" "{input.reads_r2}" "{input.compressed_r1}" "{input.compressed_r2}"
        """

rule assemble_illumina:
    input:
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz",
        mash_genome_size= "{sample}/{sample}_genome_size.txt" # TODO: can we let it guess instead?
    params:
        outdir = "{sample}/assembly",
        contig_name_format = "{sample}_%05d",
        min_contig_length = 500,
        min_coverage = 2,
        assembler = "skesa"
    output:
        assembly = f"{{sample}}/assembly/{EXPERIMENT_NAME}_{{sample}}.fna",
        log = "{sample}/logs/assembly/shovill.log" # ensure this is kept with shadow rule
    #log: "{sample}/logs/assembly/shovill.log" # TODO: how to handle logging when logfile target can't be changed
    threads: workflow.cores / 2 if (workflow.cores / 2 ) < 16 else 16 # shovill can't handle more
    conda: "assembly_env"
    shadow: "full"
    resources:
        mem_mb = 18000  # default maximum setting for shovill is 16GB - it'll *aim* for this but may miss
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
        shovill --outdir "{params.outdir}" --R1 "{input.compressed_r1}" --R2 "{input.compressed_r2}" \
         --gsize "${{GENOME_SIZE}}" \
         --minlen {params.min_contig_length} \
         --mincov {params.min_coverage} \
         --namefmt "{params.contig_name_format}" \
         --keepfiles \
         --cpus {threads} \
         --assembler {params.assembler} \
         --force || touch "{params.outdir}/contigs.fa"
         mv "{params.outdir}/contigs.fa" "{output.assembly}"
         mv "{params.outdir}/shovill.log" "{output.log}"
        """

rule compress_assembly_illumina:
    input:
        assembly = f"{{sample}}/assembly/{EXPERIMENT_NAME}_{{sample}}.fna"
    output:
        assembly_compressed =  "{sample}/assembly/{sample}.fna.gz"
    threads: 1
    resources:
        mem_mb = 10
    conda: "annotate_env"
    shell:
        """
        pigz -p {threads} -c -n "{input.assembly}" > "{output.assembly_compressed}"
        """

# TODO: adapt genome size estimate?
rule assemble_nanopore_only:
    input:
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq",
        genome_size = "{sample}/{sample}_genome_size_nanopore_lis.txt" if use_lis else f"{{sample}}/{{sample}}_genome_size_nanopore.txt"
    output:
        nanopore_assembly = temp("{sample}/nanopore_assembly/assembly.fasta"),
        assembly_info = "{sample}/nanopore_assembly/{sample}_assembly_info.txt"
    params:
        out_dir = "{sample}/nanopore_assembly/",
        # use high quality mode for super accurate ONT data
        read_qual = "hq" if basecall_mode == "sup" else "raw"
    threads: workflow.cores / 2 if (workflow.cores / 2 ) < 16 else 16
    log: "{sample}/logs/assembly/flye.log"
    conda: "nanopore_only"
    resources:
        mem_mb = 20000
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.genome_size}")
        flye --nano-{params.read_qual} "{input.downsampled_fastq}" --out-dir "{params.out_dir}" \
            --threads {threads} --genome-size "${{GENOME_SIZE}}" &> "{log}" || {{ touch "{output.nanopore_assembly}" ; touch "{params.out_dir}/assembly_info.txt" ; }}
        mv "{params.out_dir}/assembly_info.txt" "{output.assembly_info}"
        """


# TODO: improve parallelization by running steps individually?
rule polish_medaka:
    input:
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq",
        flye_asm = "{sample}/nanopore_assembly/assembly.fasta",
        run_report = lambda wildcards: snake_helpers.get_nanopore_run_report(samples_for_bactopia)
    output:
        cleaned_asm = "{sample}/nanopore_assembly/consensus.fasta"  # TODO: maybe cleanup the larger ones anyway?
    resources:
        mem_mb = 18000
    threads: 16  # TODO: does medaka consensus actually claim four extra threads
    params:
        output_basedir = lambda wildcards, output: str(pathlib.Path(output.cleaned_asm).parent),
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    log: "{sample}/logs/medaka_log"
    conda: "medaka_env"
    shadow: "full"
    # only run this if we in fact have a sequence
    shell:
        """
        if [ ! -s {input.flye_asm} ]; then
            touch "{output.cleaned_asm}"
        else
            medaka_consensus -i "{input.downsampled_fastq}" \
            -d "{input.flye_asm}" \
            -o "{params.output_basedir}" \
            -t {threads} -f > {log} || {{ echo ${{?}}; echo "Medaka failed. Trying our own model detection." >> {log}; \
              MEDAKA_MODELNAME=$(python3 "{params.snakemake_path}/parse_nanopore_report.py" "{input.run_report}" 2>> "{log}"); \
              medaka_consensus -i "{input.downsampled_fastq}" \
              -d "{input.flye_asm}" \
              -o "{params.output_basedir}" \
              -t {threads} \
              -m "${{MEDAKA_MODELNAME}}" -f >> {log} ; }}
        fi
         """

rule rename_nanopore:
    input:
        cleaned_asm = lambda wildcards: "{sample}/nanopore_assembly/consensus.fasta" if basecall_mode != "sup" else "{sample}/nanopore_assembly/assembly.fasta"
    output:
        renamed_asm = f"{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta"
    resources:
        mem_mb = 10
    shell:
        """sed 's/contig/{wildcards.sample}_contig/' "{input.cleaned_asm}" > "{output.renamed_asm}" """


rule assemble_hybrid:
    input:
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz",
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq",
    output:
        hybrid_assembly = f"{{sample}}/hybrid_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
        log = "{sample}/logs/assembly/unicycler.log"
    threads: workflow.cores / 2 if (workflow.cores / 2 ) < 16 else 16
    params:
        outdir = "{sample}/hybrid_assembly"
    shadow: "full"
    conda: "unicycler_env"
    shell:
        """
        unicycler --short1 {input.compressed_r1} --short2 {input.compressed_r2} \
        --long {input.downsampled_fastq} --threads {threads} --out {params.outdir}
        mv "{params.outdir}/assembly.fasta" "{output.hybrid_assembly}"
        mv "{params.outdir}/unicycler.log" "{output.log}"
        """


# TODO: can we get everything in one rule or is this too cluttered?
# TODO: handle QUAST's handling of non ASCII paths
rule quast_single_sample_illumina:
    input:
        mash_genome_size = "{sample}/{sample}_genome_size.txt",
        assembly_compressed =  "{sample}/assembly/{sample}.fna.gz",
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz"
    params:
        output_dir = "{sample}/assembly/quast/",
        assembly_abspath = f"{OUTDIR}/{{sample}}/assembly/{EXPERIMENT_NAME}_{{sample}}.fna",
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    output:
        quast_report = f"{{sample}}/assembly/quast/illumina_report.tsv",
        #read_dir= directory("{sample}/assembly/quast/reads_stats_illumina"),
        quast_reads = "{sample}/assembly/quast/reads_stats/illumina_reads_report.tsv"
    threads: workflow.cores / 4  # bottleneck due to memory issues
    resources:
        mem_mb = 64000
    conda: "qc_env"
    shadow: "shallow"
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
        quast "{params.assembly_abspath}" --est-ref-size "${{GENOME_SIZE}}" \
        --labels "{wildcards.sample}" \
         -o "{params.output_dir}" \
         --threads {threads} \
         --glimmer \
         --pe1 "{input.compressed_r1}" --pe2 "{input.compressed_r2}" --no-sv || \
         {{ echo ${{?}}; python3 "{params.snakemake_path}/make_fallbacks.py" --sample {wildcards.sample} --outdir "{wildcards.sample}/assembly/quast" --mode quast; }}
        mv {wildcards.sample}/assembly/quast/transposed_report.tsv {output.quast_report}
        mv {wildcards.sample}/assembly/quast/reads_stats/reads_report.tsv {output.quast_reads}
        """

rule quast_single_sample_nanopore:
    input:
        genome_size = "{sample}/{sample}_genome_size_nanopore_lis.txt" if use_lis else f"{{sample}}/{{sample}}_genome_size_nanopore.txt",
        nanopore_assembly = f"{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq"
    params:
        output_dir = "{sample}/assembly/quast/",
        assembly_abspath = f"{OUTDIR}/{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
        snakemake_path= str(pathlib.Path(workflow.basedir).resolve())
    output:
        quast_report = "{sample}/assembly/quast/nanopore_report.tsv",
        read_report = "{sample}/assembly/quast/reads_stats/nanopore_reads_report.tsv"
    threads: workflow.cores / 4
    conda: "qc_env"
    shadow: "shallow"
    resources:
        mem_mb = 64000
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.genome_size}")
        quast "{params.assembly_abspath}" --est-ref-size "${{GENOME_SIZE}}" \
         -o "{params.output_dir}" \
         --labels "{wildcards.sample}" \
         --threads {threads} \
         --glimmer \
         --nanopore "{input.downsampled_fastq}" || \
         {{ echo ${{?}}; python3 "{params.snakemake_path}/make_fallbacks.py" --sample {wildcards.sample} --outdir "{wildcards.sample}/assembly/quast" --mode quast; }}
         mv {params.output_dir}/transposed_report.tsv {output.quast_report}
         mv {params.output_dir}/reads_stats/reads_report.tsv {output.read_report}
        """

rule quast_single_sample_hybrid:
    input:
        mash_genome_size = "{sample}/{sample}_genome_size.txt",
        hybrid_assembly = f"{{sample}}/hybrid_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq",
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz"
    params:
        output_dir = "{sample}/assembly/quast/",
        assembly_abspath = f"{OUTDIR}/{{sample}}/hybrid_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta"
    output:
        quast_report = "{sample}/assembly/quast/hybrid_report.tsv"
    threads: workflow.cores / 4
    resources:
        mem_mb = 64000
    conda: "qc_env"
    shadow: "shallow"
    shell:
        """
        GENOME_SIZE=$(head -n 1 "{input.mash_genome_size}")
        quast "{params.assembly_abspath}" --est-ref-size "${{GENOME_SIZE}}" \
         -o "{params.output_dir}" \
         --labels "{wildcards.sample}" \
         --threads {threads} \
         --glimmer \
         --nanopore "{input.downsampled_fastq}" \
         --pe1 "{input.compressed_r1}" --pe2 "{input.compressed_r2}"
         mv {params.output_dir}/report.tsv {output.quast_report}
        """

# TODO: expand this for other sequencers!
rule copy_for_checkm:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample, ILLUMINA_ONLY, NANOPORE_ONLY, HYBRID_SAMPLES, EXPERIMENT_NAME)
    output:
        assembly_copied = temp("all_fasta/{sample}.fna")
    resources:
        mem_mb = 10
    shell:
        """
        cp {input.assembly} {output.assembly_copied}
        """

# TODO: handle extension!
rule checkm_all_illumina:
    input:
        all_illumina_assemblies = expand("all_fasta/{sample}.fna", sample=ALL_SAMPLES)
    output:
        checkm_results = "checkm_all_samples/checkm-results.txt"
    params:
        assembly_dir = "all_fasta",
        output_dir = lambda wildcards, output: pathlib.Path(output.checkm_results).parent,
        unique = 10,
        multi = 10,
        aai_strain = 0.9,
        length = 0.7,
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        checkm_alignment = f"checkm_all_samples/checkm-genes.aln", # we need to specify this but don't use it later
        fallback_header = str(pathlib.Path(workflow.basedir) / "data" / "dummy_data" / "checkm_header.tsv"),
        samples = ALL_SAMPLES
    log : "logs/checkm.log"
    conda: "qc_env"
    threads: (workflow.cores / 4) if (workflow.cores / 4) < 64 else 64
    resources:
        mem_mb = 64000
    shell:
        """
        checkm lineage_wf "{params.assembly_dir}" "{params.output_dir}" \
            --alignment_file "{params.checkm_alignment}" \
            --tab_table \
            --file "{output.checkm_results}" \
            --threads {threads} \
            --pplacer_threads {threads} \
            --unique {params.unique} \
            --multi {params.multi} \
            --aai_strain {params.aai_strain} \
            --length {params.length} >> "{log}" || \
            {{ echo ${{?}} >> "{log}"; cat {params.fallback_header} > {output.checkm_results}; echo {params.samples} >> {output.checkm_results} ; }}
            echo ${{?}}
        """

rule annotate:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample, 
                                                                     ILLUMINA_ONLY, NANOPORE_ONLY, 
                                                                     HYBRID_SAMPLES,
                                                                     experiment_name = EXPERIMENT_NAME)
    params:
        output_directory = "{sample}/annotation",
        evalue = "1e-09",
        coverage = 80,
        min_contig_length = 500
    output:
        annotated_genbank = "{sample}/annotation/{sample}.gbk"
    log: "{sample}/logs/annotation/prokka.log"
    threads: 8  # no precise values for speedup after 8 threads but no longer linear increase (https://academic.oup.com/bioinformatics/article/30/14/2068/2390517)
    resources:
        mem_mb = 250  # as per https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7789693/
    conda: "annotate_env"
    shell:
        """
        prokka --outdir "{params.output_directory}" \
         --prefix "{wildcards.sample}" \
         --genus Genus \
         --species species \
         --evalue {params.evalue} \
         --coverage {params.coverage} \
         --mincontiglen {params.min_contig_length} \
         --locustag "{wildcards.sample}" \
         --cpus {threads} \
         --addgenes \
         --force \
        "{input.assembly}" && \
         mv "{params.output_directory}/{wildcards.sample}.log" "{log}" || \
         {{ touch {output.annotated_genbank}; \
          echo "Sample {wildcards.sample} could not be annotated" | tee "{log}"; }}
        """

rule unpack_ariba_db:
    input:
        ariba_db = "/bactopia_datasets/ariba/{database}.tar.gz"
    params:
        database_path = "/bactopia_datasets/ariba"
    output:
        ariba_target = directory("/bactopia_datasets/ariba/{database}db"),
        unpack_check = touch("/bactopia_datasets/ariba/check_{database}")
    shell:
        """
        tar -C "{params.database_path}" -xzvf "{input.ariba_db}"
        echo "Local files"
        ls
        echo "Files in database dir"
        ls "{params.database_path}"
        mv "{params.database_path}/{wildcards.database}" "{output.ariba_target}"
        """


rule ariba:
    input:
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz",
        unpack_check = "/bactopia_datasets/ariba/check_{database}"
    params:
        database_path = "/bactopia_datasets/ariba",
        output_dir = "{sample}/ariba/{database}",
        nucmer_min_id = 90,
        nucmer_min_length = 20,
        nucmer_break_length = 200,
        assembly_cov = 50,
        min_depth = 10,
        assembled_threshold = 0.95,
        gene_nt_extend = 30,
        unique_threshold = 0.03
    output:
        ariba_analysis = f"{{sample}}/ariba/{{database}}/report.tsv"
    log: "{sample}/logs/ariba/{database}/ariba_run.log.gz"
    threads: workflow.cores / 2
    conda: "ariba_env"
    shell:
        """
        ariba run "{params.database_path}/{wildcards.database}db" \
          "{input.compressed_r1}" "{input.compressed_r2}" \
          "{params.output_dir}" \
          --nucmer_min_id {params.nucmer_min_id} \
          --nucmer_min_len {params.nucmer_min_length} \
          --nucmer_breaklen {params.nucmer_break_length} \
          --assembly_cov {params.assembly_cov} \
          --min_scaff_depth {params.min_depth} \
          --assembled_threshold {params.assembled_threshold} \
          --gene_nt_extend {params.gene_nt_extend} \
          --unique_threshold {params.unique_threshold} \
          --threads {threads} \
          --force
        mv "{params.output_dir}/log.clusters.gz" "{log}"
        """

rule summarize_ariba:
    input:
        ariba_analysis = "{sample}/ariba/{database}/report.tsv"
    params:
        output_dir = "{sample}/ariba/{database}"
    output:
        ariba_summary = "{sample}/ariba/{database}/summary.csv"
    conda: "ariba_env"
    shell:
        """
        ariba summary "{params.output_dir}/summary" "{input.ariba_analysis}" \
         --cluster_cols assembled,match,known_var,pct_id,ctg_cov,novel_var \
         --col_filter n --row_filter n
        """

# TODO: convert comma separated to tab or semicolon separated

rule minmer_sketch_illumina:
    input:
        downsampled_r1 = "{sample}/quality-control/{sample}_downsampled_R1.fastq",
        downsampled_r2 = "{sample}/quality-control/{sample}_downsampled_R2.fastq"
    params:
        sketch_size = 10000
    output:
        mash_sketch = "{sample}/minmers/{sample}-k{kmersize}_illumina.msh"
    threads: (workflow.cores / 4) if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 200
    conda: "minmers_env"
    shell:
        """
        cat "{input.downsampled_r1}" "{input.downsampled_r2}" | mash sketch -o "{output.mash_sketch}" \
        -k {wildcards.kmersize} -s {params.sketch_size} -r -I "{wildcards.sample}" -
        """

rule minmer_sketch_nanopore:
    input:
        nanopore_reads = "{sample}/nanopore_reads/{sample}.filtered.fastq"
    params:
        sketch_size = 10000
    output:
        mash_sketch = "{sample}/minmers/{sample}-k{kmersize}_illumina.msh" # TODO should be nano
    threads: (workflow.cores / 4) if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 200
    conda: "minmers_env"
    shell:
        """
        cat "{input.nanopore_reads}" | mash sketch -o "{output.mash_sketch}" \
        -k {wildcards.kmersize} -s {params.sketch_size} -r -I "{wildcards.sample}" -
        """

rule sourmash_sketch_illumina:
    input:
        compressed_r1 = "{sample}/quality-control/{sample}_R1.fastq.gz",
        compressed_r2 = "{sample}/quality-control/{sample}_R2.fastq.gz"
    params:
        scale_rate = 10000
    output:
        sourmash_sketch = "{sample}/minmers/{sample}_illumina.sig"
    conda: "minmers_env"
    resources:
        mem_mb = 1000
    shell:
        """
        sourmash sketch dna -p k=21,k=31,k=51,abund,scaled={params.scale_rate} \
        --merge "{wildcards.sample}" -o "{output.sourmash_sketch}" "{input.compressed_r1}" "{input.compressed_r2}"
        """

rule sourmash_sketch_nanopore:
    input:
        nanopore_reads = "{sample}/nanopore_reads/{sample}.filtered.fastq"
    params:
        scale_rate = 10000
    output:
        sourmash_sketch = "{sample}/minmers/{sample}_nanopore.sig"
    conda: "minmers_env"
    resources:
        mem_mb = 1000
    shell:
        """
        sourmash sketch dna -p k=21,k=31,k=51,abund,scaled={params.scale_rate} \
        --merge "{wildcards.sample}" -o "{output.sourmash_sketch}" "{input.nanopore_reads}"
        """

rule make_mash_header:
    output:
        mash_refseq = "{sample}/minmers/{sample}-mash-refseq-k21_{platform}.msh"
    shell:
        """
        printf "identity\\tshared-hashes\\tmedian-multiplicity\\tp-value\\tquery-ID\\tquery-comment\\n" > {output.mash_refseq}

        """

rule unpack_mash:
    input:
        mash_compressed = config["paths"]["mash_data"]
    output:
        mash_refseq_db = str(pathlib.Path(config["paths"]["mash_outdir"]) / "mash-refseq-k21.msh")
    conda: "minmers_env"
    resources:
        mem_mb = 100  # should be higher than worst case
    threads: 2
    shell:
        """
        xzcat --memory={resources.mem_mb}MB --threads={threads} "{input.mash_compressed}" > "{output.mash_refseq_db}"
        """

rule mash_query_illumina:
    input:
        downsampled_r1 = "{sample}/quality-control/{sample}_downsampled_R1.fastq",
        downsampled_r2 = "{sample}/quality-control/{sample}_downsampled_R2.fastq",
        mash_refseq= "{sample}/minmers/{sample}-mash-refseq-k21_illumina.msh",
        mash_refseq_db = rules.unpack_mash.output.mash_refseq_db

    output: # TODO: vary database?
        mash_refseq_check = touch("{sample}/minmers/check_mash_query_illumina")
    params:
        identity = 0.8
    log: "{sample}/logs/minmers/illumina_sort_log"
    threads: (workflow.cores / 4) if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 200
    conda: "minmers_env"
    shell: # TODO: better solution for the header?
        """
        cat "{input.downsampled_r1}" "{input.downsampled_r2}" | mash screen -w -i "{params.identity}" \
         -p {threads} "{input.mash_refseq_db}" - | sort -gr --debug >> "{input.mash_refseq}" 2> {log}
         
        """

rule mash_query_nanopore:
    input:
        nanopore_reads = "{sample}/nanopore_reads/{sample}.filtered.fastq",
        mash_refseq = "{sample}/minmers/{sample}-mash-refseq-k21_nanopore.msh",
        mash_refseq_db= rules.unpack_mash.output.mash_refseq_db
    output:  # TODO: vary database?
        mash_refseq_check = touch("{sample}/minmers/check_mash_query_nanopore")
    params:
        identity = 0.8
    log: "{sample}/logs/minmers/nanopore_sort_log"
    threads: (workflow.cores / 4) if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 200
    conda: "minmers_env"
    shell:  # TODO: better solution for the header?
        """
        cat "{input.nanopore_reads}" | mash screen -w -i "{params.identity}" \
         -p {threads} "{input.mash_refseq_db}" - | sort -gr --debug >> "{input.mash_refseq}" 2> {log}

        """

rule get_sourmash_db:
    output:
        sourmash_db = temp("minmer/sourmash-genbank-k21.json.gz")
    shell:
        """
        wget -O "{output.sourmash_db}" https://osf.io/d7rv8/download
        """


rule sourmash_query:
    input:
        sourmash_sketch = "{sample}/minmers/{sample}_{platform}.sig",
        sourmash_db= "minmer/sourmash-genbank-k21.json.gz" if config["run_on"] == "local"
                      else "/bactopia_datasets/minmer/sourmash-genbank-k21.json.gz"
    output:
        sourmash_query = "{sample}/minmers/{sample}-sourmash-genbank-k21_{platform}.txt"
    conda: "minmers_env"
    resources:
        mem_mb = 1000
    shell:
        """
        sourmash lca classify --query "{input.sourmash_sketch}" --db "{input.sourmash_db}" > "{output.sourmash_query}"
        """


# TODO: cleanup of temp files?

# MLST typing: for now, use mlst with autodetect
# TODO: better to get json because that has separate headers?
rule get_sequence_type:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample, 
                                                                     ILLUMINA_ONLY, NANOPORE_ONLY, 
                                                                     HYBRID_SAMPLES,
        experiment_name = EXPERIMENT_NAME)
    output:
        sequence_type = "{sample}/mlst/{sample}_sequence_type.tsv"
    conda: "mlst_env"
    resources:
        mem_mb = 200  # currently a guess
    log:
        "{sample}/logs/mlst/mlst.log"
    shell:
        """
        mlst "{input.assembly}" --label "{wildcards.sample}" 1> "{output.sequence_type}" 2> "{log}"
        """

# get quast output for all samples - TODO: multiple ones for nanopore, illumina and hybrid?
rule all_qc_illumina:
    input:
        bactopia_assemblies = expand(f"{{sample}}/assembly/{{sample}}.fna.gz",
                                    sample = ILLUMINA_ONLY),
        cleaned_reads = expand(f"{{sample}}/quality-control/{{sample}}_R1.fastq.gz",
                                sample=ILLUMINA_ONLY)
    params:
        output_dir = "quast_all",
        read_output = "quast_all/reads_stats_illumina",
        assemblies_abspaths = expand(f"{OUTDIR}/{{sample}}/assembly/{EXPERIMENT_NAME}_{{sample}}.fna",
            sample=ILLUMINA_ONLY),
        samples = ",".join(ILLUMINA_ONLY),
        fallback_header = str(pathlib.Path(workflow.basedir) / "data" / "dummy_data" / "quast_header.tsv")
    output:
        qc_all = f"quast_all/transposed_report_illumina.tsv"
    conda: "qc_env"
    threads:
        workflow.cores / 3 # TODO: can likely be adjusted once we've got more things going on
    resources:
        mem_mb = 64000
    shadow: "shallow"
    shell:
        """
        quast {params.assemblies_abspaths} -o "{params.output_dir}" --threads {threads} \
        --labels {params.samples} \
        --glimmer \
        || {{ cat {params.fallback_header} > quast_all/transposed_report.tsv ; echo {params.samples} | tr ',' '\n' >> quast_all/transposed_report.tsv ; }}
        mv quast_all/transposed_report.tsv {output.qc_all}
        """

rule all_qc_nanopore:
    input:
        nanopore_assemblies = expand(f"{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
                                      sample = NANOPORE_ONLY),
        downsampled_reads = expand("{sample}/nanopore_reads/{sample}.downsample.fastq",
                                sample=NANOPORE_ONLY)
    params:
        output_dir = "quast_all",
        read_output = "quast_all/reads_stats_nanopore",
        assemblies_abspaths = expand(f"{OUTDIR}/{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
                                    sample=NANOPORE_ONLY),
        labels = ",".join(NANOPORE_ONLY)
    output:
        qc_all = f"quast_all/transposed_report_nanopore.tsv"
    conda: "qc_env"
    threads:
        workflow.cores / 3 # TODO: can likely be adjusted once we've got more things going on
    resources:
        mem_mb = 64000
    shadow: "shallow"
    shell:
        """
        quast {params.assemblies_abspaths} -o "{params.output_dir}" --threads {threads} \
        --glimmer --labels {params.labels}
        mv quast_all/transposed_report.tsv {output.qc_all}
        """

rule all_qc_hybrid:
    input:
        hybrid_assembly = expand(f"{{sample}}/hybrid_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta", sample=HYBRID_SAMPLES),
        nanopore_reads = expand("{sample}/nanopore_reads/{sample}.downsample.fastq",
            sample=HYBRID_SAMPLES),
        compressed_r1 = expand("{sample}/quality-control/{sample}_R1.fastq.gz",
            sample=HYBRID_SAMPLES),
        compressed_r2 = expand("{sample}/quality-control/{sample}_R2.fastq.gz",
            sample=HYBRID_SAMPLES)
    params:
        output_dir = "quast_all",
        read_output = "quast_all/reads_stats_hybrid",
        assemblies_abspaths = expand(f"{OUTDIR}/{{sample}}/hybrid_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta",
                                    sample=HYBRID_SAMPLES),
    output:
        qc_all = f"quast_all/transposed_report_hybrid.tsv"
    conda: "qc_env"
    threads:
        workflow.cores / 3 # TODO: can likely be adjusted once we've got more things going on
    resources:
        mem_mb = 64000
    shadow: "shallow"
    shell:
        """
        quast {params.assemblies_abspaths} -o "{params.output_dir}" --threads {threads} \
        --glimmer
        mv quast_all/transposed_report.tsv {output.qc_all}
        """

rule align_nanopore_reads:
    input:
        downsampled_fastq = "{sample}/nanopore_reads/{sample}.downsample.fastq",
        nanopore_assembly = f"{{sample}}/nanopore_assembly/{EXPERIMENT_NAME}_{{sample}}.fasta"
    output:
        nanopore_aligned = temp("{sample}/read_alignments/{sample}_reads_aligned.sam")
    conda: "assembly_env"
    shell:
        """
        minimap2 -a "{input.nanopore_assembly}" "{input.downsampled_fastq}" > "{output.nanopore_aligned}"
        """

rule sort_nanopore_reads:
    input:
        nanopore_aligned = "{sample}/read_alignments/{sample}_reads_aligned.sam"
    output:
        nanopore_sorted = temp("{sample}/read_alignments/{sample}_reads_sorted.bam")
    conda: "ilm_read_qc"
    resources:
        mem_mb = 800
    shell:
        """
        samtools sort -o "{output.nanopore_sorted}" -m {resources.mem_mb}M "{input.nanopore_aligned}"
        """

rule get_nanopore_coverage:
    input:
        nanopore_sorted = "{sample}/read_alignments/{sample}_reads_sorted.bam"
    output:
        nanopore_coverage = "{sample}/read_alignments/{sample}_coverage.tsv"
    conda: "ilm_read_qc"
    shell:
        """
        samtools coverage -o "{output.nanopore_coverage}" {input.nanopore_sorted}
        """

rule get_own_nanopore_coverage:
    input:
        nanopore_sorted = expand("{sample}/nanopore_assembly/{sample}_assembly_info.txt",
                                 sample = NANOPORE_ONLY)
    output:
        nanopore_coverage = "read_stats/own_nanopore_coverage.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_file = CONFIG_PATH
    resources:
        mem_mb = 500
    shell:
        """
         python3 {params.snakemake_path}/get_nanopore_coverage.py {input.nanopore_sorted} \
         --outfile "{output.nanopore_coverage}" --workflow_config_file "{params.config_file}"
        """

# concatenate all CheckM results
# rule all_checkm:
#     input:
#         all_checkm = expand(f"{{sample}}/assembly/checkm/checkm-results.txt",
#                                     sample = ALL_SAMPLES)
#     params:
#         snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
#     output:
#         checkm_for_all = f"checkm_all/{EXPERIMENT_NAME}_checkm.tsv"
#     threads: 1
#     shell:
#         """
#         python3 "{params.snakemake_path}/concat_reports.py" {input.all_checkm} \
#         --report_type "generic" \
#          --outfile "{output.checkm_for_all}"
#         """


rule all_coverage_quast:
    input:
        quast_reads = snake_helpers.get_all_read_reports(ILLUMINA_ONLY, NANOPORE_ONLY)
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    output:
        quast_coverage = "read_stats/all_quast_coverage.tsv"
    resources:
        mem_mb = 100  # rough measurements + fudge factor
    shell:
        """
        python3 "{params.snakemake_path}/concat_reports.py" {input.quast_reads} \
         --report_type "coverage" \
         --outfile "{output.quast_coverage}"
        """

rule all_ng50_quast:
    input:
        quast_reports = expand("{sample}/assembly/quast/illumina_report.tsv", sample=ILLUMINA_ONLY)
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve())
    output:
        illumina_ng50 = "quast_all/ng50_illumina.tsv"
    resources:
        mem_mb = 100  # rough measurements + fudge factor
    shell:
        """
        python3 "{params.snakemake_path}/concat_reports.py" {input.quast_reports} \
         --report_type "ng50" \
         --outfile "{output.illumina_ng50}"
        """

rule kraken_prepare_db:
    input:
        kraken_compressed = config['paths']['kraken_db']
    output:
        kraken_taxo = pathlib.Path(config['paths']['kraken_outdir']) / "taxo.k2d"
    params:
        kraken_parent = pathlib.Path(config['paths']['kraken_outdir'])
    resources:
        mem_mb = 50  # tar shouldn't use all that much
    shell:
        """
        tar -C "{params.kraken_parent}" -zxf "{input.kraken_compressed}"
        echo "Contents of {params.kraken_parent}"
        ls "{params.kraken_parent}"
        """

# TODO: is there a good Nanopore alternative?
rule kraken_classify:
    input:
        reads = lambda wildcards: snake_helpers.get_reads(wildcards.sample,ALL_ILLUMINA,
            ALL_NANOPORE,HYBRID_SAMPLES),
        kraken_taxo = pathlib.Path(config['paths']['kraken_outdir']) / "taxo.k2d"
    params:
        kraken_db = pathlib.Path(config['paths']['kraken_outdir']),
        # if we can, use Illumina reads
        read_type = lambda wildcards: "" if wildcards.sample in NANOPORE_ONLY else "--paired"
    output:
        kraken_report = "{sample}/kraken/{sample}_report.tsv"
    conda: "kraken_env"
    resources:
        mem_mb = 10000  # database + a bit extra
    threads: workflow.cores
    shell:
        """
        kraken2 --db "{params.kraken_db}" --report "{output.kraken_report}" \
        --output "-" --threads {threads} {params.read_type} \
        {input.reads}
        """

rule merge_kraken_results:
    input:
        kraken_reports = expand("{sample}/kraken/{sample}_report.tsv", sample=ALL_SAMPLES)
    output:
        kraken_result = f"kraken_all/{EXPERIMENT_NAME}_report.tsv"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_path = CONFIG_PATH
    resources:
        mem_mb = 150
    shell:
        """
        python3 "{params.snakemake_path}/parse_kraken.py" {input.kraken_reports} \
        --outfile "{output.kraken_result}" --workflow_config_file "{params.config_path}"
        """

rule gtdb_prepare_db:
    input:
        gtdb_compressed = config['paths']['gtdb_data']
    output:
        gtdb_metadata = pathlib.Path(config['paths']['gtdb_outdir']) / "release214" / "metadata" / "metadata.txt"
    params:
        gtdb_dir = pathlib.Path(config['paths']['gtdb_outdir'])
    shell:
        """
        tar -C "{params.gtdb_dir}" -zxf "{input.gtdb_compressed}"

        echo "Contents of {params.gtdb_dir}"
        ls "{params.gtdb_dir}"
        """


# find non empty files using find and write to check file
rule gather_non_empty:
    input:
        all_illumina_assemblies = snake_helpers.get_all_assemblies(ILLUMINA_ONLY,
                                                                   NANOPORE_ONLY,
                                                                   HYBRID_SAMPLES,
                                                                   experiment_name = EXPERIMENT_NAME)
    output:
        non_empty_fastas = "all_fasta_nonempty.txt",  # TODO: temp?
        files_only = "non_empty.tmp"  # TODO: make temp() once it runs
    params:
        find_fastas_indirs = "*/*assembly/",  # TODO: can we do this a bit more cleanly?
        find_fastas_expression = f"\( -name {EXPERIMENT_NAME}_*.fna -o -name {EXPERIMENT_NAME}_*.fasta \)",
        sample_number_expression = lambda wildcards: f"{SAMPLE_NUMBER_FORMAT}(?=/(nanopore_)?assembly)"
    shell:  # TODO: prettier solution with awk?
        """
        find {params.find_fastas_indirs} {params.find_fastas_expression} -not -empty -print | sort > "{output.files_only}"
        grep -P "{params.sample_number_expression}" "{output.files_only}" --only-matching | paste "{output.files_only}" - > "{output.non_empty_fastas}" 
        """


rule gtdb_classify:
    input:
        all_assemblies = rules.gather_non_empty.output.non_empty_fastas,
        gtdb_metadata = pathlib.Path(config['paths']['gtdb_outdir']) / "release214" / "metadata" / "metadata.txt"
    output:
        gtdb_summary = f"gtdb/classify/{EXPERIMENT_NAME}.bac120.summary.tsv"
    params:
        gtdb_basedir = "gtdb",
        gtdb_base_path = pathlib.Path(config['paths']['gtdb_outdir']) / "release214",
        scratch_dir = pathlib.Path(config['paths']['gtdb_outdir']) / "scratch",
        gtdb_mash_db = pathlib.Path(config['paths']['gtdb_outdir']) / "mash"
    # threads is for everything except pplacer which is a memory hungry monster
    threads: workflow.cores / 4 if (workflow.cores / 4) < 64 else 64
    # TODO: needs to be some sort of shadow
    resources:
        mem_mb = 150000
    conda: "gtdb_env"
    shell:
        """
        export GTDBTK_DATA_PATH="{params.gtdb_base_path}"
        gtdbtk classify_wf --batchfile "{input.all_assemblies}" \
        --out_dir "{params.gtdb_basedir}" \
        --cpus {threads} --pplacer_cpus 1 --prefix {EXPERIMENT_NAME} \
        --scratch_dir "{params.scratch_dir}"  --mash_db "{params.gtdb_mash_db}"
        """




rule merge_all_results:
    input:
        qc_all = lambda wildcards: [f"quast_all/transposed_report_{platform}.tsv"
                                    for platform in snake_helpers.get_qc_platforms(ILLUMINA_ONLY,
                NANOPORE_ONLY,
                HYBRID_SAMPLES)],
        read_stats = "read_stats/all_quast_coverage.tsv",
        checkm_for_all = "checkm_all_samples/checkm-results.txt",
        kraken_for_all = f"kraken_all/{EXPERIMENT_NAME}_report.tsv",
        gtdb_summary = rules.gtdb_classify.output.gtdb_summary,
        quast_ng50 = "quast_all/ng50_illumina.tsv" if ALL_ILLUMINA else [],
        nanopore_read_stats = "read_stats/own_nanopore_coverage.tsv" if ALL_NANOPORE else [],
        runsheet = config["runsheet"],
        data_for_lis = f"raw_results/{EXPERIMENT_NAME}_mads_wide.tsv" if use_lis else []
    output:
        summary_report = f"raw_results/{EXPERIMENT_NAME}_all_results.tsv",
        summary_sheet = f"{EXPERIMENT_NAME}_all_results.xlsx"
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        ng50_input = '--ng50 "quast_all/ng50_illumina.tsv"' if ALL_ILLUMINA else "",
        nanopore_read_input = '--nanopore_reads "read_stats/own_nanopore_coverage.tsv"'
                               if ALL_NANOPORE else "",
        config_file = CONFIG_PATH,
        results_for_lis = f"--results_for_lis raw_results/{EXPERIMENT_NAME}_mads_wide.tsv"
                          if use_lis else "",
        lis_report = f'--mads_report {config["lab_info_system"]["lis_report"]}'
                    if use_lis else "",
        bacteria_coding = f'--species_coding {config["lab_info_system"]["bacteria_codes"]}'
                          if use_lis else ""
    threads: 1
    resources:
        mem_mb = 200  # educated guess with heavy fudging
    shell:
        """
        python3 "{params.snakemake_path}/merge_all_results.py" --quast "{input.qc_all}" \
        --checkm "{input.checkm_for_all}" --kraken "{input.kraken_for_all}" \
        --gtdb "{input.gtdb_summary}" \
        --runsheet "{input.runsheet}" \
        --quast_reads "{input.read_stats}" \
         {params.nanopore_read_input} {params.ng50_input} \
        {params.results_for_lis} \
        --outfile_raw "{output.summary_report}" --outfile "{output.summary_sheet}" \
        {params.lis_report} {params.bacteria_coding} \
         --workflow_config_file {params.config_file}
        """


rule seqsero_salmonella:
    input:
        # TODO: get at reads
        seqsero_input = lambda wildcards: snake_helpers.get_reads(wildcards.sample, ALL_ILLUMINA,
                                                                  ALL_NANOPORE, HYBRID_SAMPLES)
                                         if not wildcards.sample in ALL_NANOPORE
                                         else snake_helpers.get_assembly_name(wildcards.sample,
                                                                             ALL_ILLUMINA,
                                                                             ALL_NANOPORE,
                                                                             HYBRID_SAMPLES,
                                                                             EXPERIMENT_NAME)
    output:
        seqsero_report = "{sample}/typing/SeqSero/SeqSero_result.tsv"
    shadow: "full"
    conda: "typing_env"
    params:
        # input type: 2 is paired-end reads, 4 is assembly;
        # we want to use illumina reads - higher quality - if we can
        input_type = lambda wildcards: "4" if wildcards.sample in NANOPORE_ONLY else "2",
        workflow = lambda wildcards: "k" if wildcards.sample in NANOPORE_ONLY else "a",
        output_dir = "{sample}/typing/SeqSero",
        placeholder_data = r"Sample name\tPredicted identification\tPredicted serotype\n" \
                           "{sample}" \
                           r"\t-\t-"
    threads: workflow.cores / 4 if (workflow.cores / 4) < 16 else 16
    resources:
        mem_mb = 500  # todo: better estimate
    shell:
        """
        SeqSero2_package.py -t {params.input_type} -m {params.workflow} -p {threads} \
        -i {input.seqsero_input} -d {params.output_dir} || printf "{params.placeholder_data}" > "{output.seqsero_report}"
        """

rule serotypefinder_ecoli:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample, 
                                                                     ILLUMINA_ONLY, NANOPORE_ONLY, 
                                                                     HYBRID_SAMPLES,
            experiment_name = EXPERIMENT_NAME)
    output:
        serotypefinder_results = "{sample}/typing/serotypefinder/results_tab.tsv"
    params:
        serotypefinder_db = "/bactopia_datasets/serotypefinder",
        serotypefinder_outdir = "{sample}/typing/serotypefinder"
    conda: "typing_env"
    resources:
        mem_mb = 500  # todo: profile
    shell:
        """
        serotypefinder -i {input.assembly} -o {params.serotypefinder_outdir} -x \
         -p {params.serotypefinder_db} || touch {output.serotypefinder_results}
        """

rule plasmidfinder:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample,
            ILLUMINA_ONLY,NANOPORE_ONLY,
            HYBRID_SAMPLES, experiment_name = EXPERIMENT_NAME)
    output:
        plasmid_hits = "{sample}/plasmids/results_tab.tsv"
    params:
        plasmid_db = "/bactopia_datasets/plasmidfinder",
        plasmid_outdir = lambda wildcards, output: pathlib.Path(output.plasmid_hits).parent
    log: "{sample}/logs/plasmids/plasmidfinder.log"
    conda: "plasmid_env"
    resources:
        mem_mb = 500  # todo: profile
    shell:
        """
        plasmidfinder.py -i "{input.assembly}" -o "{params.plasmid_outdir}" -x \
         -p "{params.plasmid_db}" | tee "{log}" || touch "{output.plasmid_hits}"
        """

rule abritamr_by_org:
    input:
        assembly = lambda wildcards: snake_helpers.get_assembly_name(wildcards.sample, 
                                                                     ILLUMINA_ONLY, NANOPORE_ONLY, 
                                                                     HYBRID_SAMPLES,
        experiment_name = EXPERIMENT_NAME)
    output:
        abritamr_report = "{sample}/abritamr/abritamr.txt",
        virulence_report = "{sample}/abritamr/summary_virulence.txt"
    params:
        output_dir = "{sample}/abritamr",
        organism_for_pointmuts = lambda wildcards:
                                    snake_helpers.get_abritamr_organism(wildcards.sample,
                                        mads_report,
                                        species_coding,
                                    lis_data_names = lis_names,
                                    bacteria_names = bact_names)
    conda:
        "abritamr_env"
    threads: workflow.cores / 4 if (workflow.cores / 4) < 16 else 16
    shadow: "shallow"
    log: "{sample}/logs/abritamr/abritamr.log"
    resources:
        mem_mb = 1000  # todo: benchmark
    shell:
        """
        abritamr run --contigs "{input.assembly}" --prefix "{params.output_dir}" --jobs {threads} \
        {params.organism_for_pointmuts}
        mv abritamr.log {log}
        if [ ! -f {output.abritamr_report} ]; then
            echo "No AMR genes found for {wildcards.sample}" | tee -a "{log}"
            touch "{output.abritamr_report}"
         fi
        if [ ! -f {output.virulence_report} ]; then
            echo "No virulence genes found for {wildcards.sample}" | tee -a "{log}"
            touch "{output.virulence_report}"
         fi
        """


rule make_mads_report:
    input:
        all_gtdb = rules.gtdb_classify.output.gtdb_summary,
        abritamr_report = "{sample}/abritamr/abritamr.txt",
        sequence_type = "{sample}/mlst/{sample}_sequence_type.tsv",
        plasmid_report = "{sample}/plasmids/results_tab.tsv",
        serotyping_report = lambda wildcards: snake_helpers.get_serotyping_report(wildcards.sample,
                                                                                  ECOLI_SAMPLES,
                                                                                  SALMONELLA_SAMPLES),
        toxin_report = lambda wildcards: f"{wildcards.sample}/abritamr/summary_virulence.txt"
                                         if wildcards.sample in TOXIN_SAMPLES
                                         else []
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_path = CONFIG_PATH,
        toxin_flag = lambda wildcards, input: f'--toxin_report "{input.toxin_report}"'
                                              if input.toxin_report else "",
        serotype_flag = lambda wildcards, input: f'--serotyping "{input.serotyping_report}"'
                                                 if input.serotyping_report else "",
        lis_report = config["lab_info_system"]["lis_report"],
        bacteria_coding = config["lab_info_system"]["bacteria_codes"]
    output:
        report_for_mads = "{sample}/for_mads/{sample}_report.tsv",
        wide_report = "{sample}/for_mads/{sample}_mads_wide.tsv"
    resources:
        mem_mb = 200  # todo: benchmark
    shell:
        """
        python3 "{params.snakemake_path}/generate_reports.py" "{wildcards.sample}" \
         "{input.all_gtdb}" \
         "{input.abritamr_report}" \
         "{input.sequence_type}" \
         "{input.plasmid_report}" \
         {params.toxin_flag} \
         {params.serotype_flag} \
         --mads_report "{params.lis_report}" \
         --species_coding "{params.bacteria_coding}" \
         --outfile "{output.report_for_mads}" \
         --outfile_wide "{output.wide_report}" \
         --workflow_config_file "{params.config_path}"
        """


rule make_wide_mads_overview: # TODO: gather all these?
    input:
        mads_reports = expand("{sample}/for_mads/{sample}_report.tsv", sample=ALL_SAMPLES)
    params:
        snakemake_path = str(pathlib.Path(workflow.basedir).resolve()),
        config_path= CONFIG_PATH
    output:
        wide_report_raw = f"raw_results/{EXPERIMENT_NAME}_mads_wide.tsv",
        wide_report_sheet = f"{EXPERIMENT_NAME}_mads_wide.xlsx"
    resources:
        mem_mb = 200  # todo: benchmark
    shell:
        """
        python3 "{params.snakemake_path}/concat_reports.py" {input.mads_reports} \
         --report_type "mads" \
         --outfile "{output.wide_report_raw}" --out_sheet "{output.wide_report_sheet}" \
         --workflow_config_file "{params.config_path}"
        """


onsuccess:
    shell('mkdir -p logs; cat "{log}" >> "logs/snakemake.log"')

onerror:
    shell('mkdir -p logs; cat "{log}" >> "logs/snakemake.log"')
