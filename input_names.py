"""Data structure for setting up input names from config."""

__author__ = "Kat Steinke"


from typing import Dict, List, NamedTuple, Union

import pipeline_config
import version

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF


class RunsheetNames(NamedTuple):
    """Column names in the metadata sheet.

    Attributes:
        sample_info_sheet:              name of sheet with sample information
        sample_number:                  column for sample number in sheet with sample information
        extraction_well_number:         column for extraction well number in sheet with sample
                                        information
        extraction_run_number:          column for number of extraction run
        dna_concentration:              column for DNA concentration in sheet with
                                        sample information
        requested_by:                   column for initials of person requesting sequencing
        comment:                        column for comments on the request
        sequencing_run_number:          column for number of the sequencing run
        indications:                    list of possible indication columns
        dna_normalization_sheet:        name of sheet used for DNA normalization calculations
        dna_normalization_sample_nr:    column for sample number in DNA normalization sheet
        ilm_well_number:                column for Illumina library prep plate well number
        plate_layout:                   plate layout sheet name
        runsheet_base:                  base name of sheet with runsheet for methods
                                        - suffixed with _nanopore, ...
        experiment_name:                column for experiment name in Nanopore runsheet
        nanopore_sheet_sample_number:   column for sample number in Nanopore runsheet
        barcode:                        column for barcode in Nanopore runsheet
        protocol_version:               column for protocol version in Nanopore runsheet

    """
    sample_info_sheet: str
    sample_number: str
    extraction_well_number: str
    extraction_run_number: str
    dna_concentration: str
    requested_by: str
    comment: str
    sequencing_run_number: str
    indications: List[str]
    dna_normalization_sheet: str
    dna_normalization_sample_nr: str
    ilm_well_number: str
    plate_layout: str
    runsheet_base: str
    experiment_name: str
    nanopore_sheet_sample_number: str
    barcode: str
    protocol_version: str


class LISDataNames(NamedTuple):
    """Column names in LIS data.

    Attributes:
      sample_number:    column for sample number in LIS data
      sample_type:      column for sample type (as part of unique sample identifier)
      isolate_number:   column for isolate number (as part of unique isolate identifier)
      bacteria_code:    column for numeric code for bacteria identified in the sample
      bacteria_text:    column for bacteria identified in the sample in text form
    """
    sample_number: str
    sample_type: str
    # isolate number (as part of unique isolate identifier)
    isolate_number: str
    # numeric code for bacteria identified in the sample
    bacteria_code: str
    # bacteria identified in the sample in text form
    bacteria_text: str


class BacteriaMappingNames(NamedTuple):
    """Column names in bacteria number to species mapping.
    Attributes:
        bacteria_code:          column for numeric code for bacteria identified in the sample
        internal_bacteria_text: column for LIS' internal name for bacteria
        bacteria_category:      column for bacterial species/genus for use in the pipeline
    """
    bacteria_code: str
    internal_bacteria_text: str
    bacteria_category: str


# TODO: should this be configurable (using Nanopore vs Illumina?)
def set_up_runsheet_names(runsheet_config: Dict[str, Union[str, List[str]]]) -> RunsheetNames:
    """Initialize runsheet column names from the runsheet name section of the config.

    Arguments:
        runsheet_config:    the runsheet name section of the config

    Returns:
        Runsheet column and sheet names as a RunsheetNames namedtuple

    Raises:
        KeyError:   if a column name is not filled out
    """
    if not all(runsheet_config.values()):
        missing_names = sorted([column for column, colname in runsheet_config.items()
                                if not colname])
        raise KeyError(f"Column/sheet names cannot be blank. Please add name(s) for {missing_names}"
                       " in the metadata_sheet section of your input settings.")
    # we don't want any ints later on but we do have to handle the list of indications
    for column in runsheet_config:
        if isinstance(runsheet_config[column], int):
            runsheet_config[column] = str(runsheet_config[column])

    current_names = RunsheetNames(**runsheet_config)
    return current_names


# TODO: can we allow for the absence of columns here or is this too much?
def set_up_lis_names(lis_name_config: Dict[str, str]) -> LISDataNames:
    """Initialize LIS column names from the LIS data section of the config.

    Arguments:
        lis_name_config:    the LIS data section of the config

    Returns:
        LIS column names as a LISDataNames namedtuple
    Raises:
        KeyError:   if a column name is not filled out
    """
    if not all(lis_name_config.values()):
        missing_names = sorted([column for column, colname in lis_name_config.items()
                                if not colname])
        raise KeyError(f"Column names cannot be blank. Please add name(s) for {missing_names}"
                       " in the lis_columns section of your input settings.")

    # we can just convert everything into a string here, no lists to watch out for
    lis_names_plain = {column: str(colname) for column, colname in lis_name_config.items()}
    current_names = LISDataNames(**lis_names_plain)
    return current_names


def set_up_bact_names(bact_name_config: Dict[str, str]) -> BacteriaMappingNames:
    """Initialize column names from the bacteria number mapping section of the config.

    Arguments:
        bact_name_config:   the bacteria number mapping section of the config

    Returns:
        Bacteria mapping column names as a BacteriaMappingNames namedtuple

    Raises:
        KeyError:   if a column name is not filled out
    """
    if not all(bact_name_config.values()):
        missing_names = sorted([column for column, colname in bact_name_config.items()
                                if not colname])
        raise KeyError(f"Column names cannot be blank. Please add name(s) for {missing_names}"
                       " in the bact_translation section of your input settings.")

    # we can just convert everything into a string here, no lists to watch out for
    bact_names_plain = {column: str(colname) for column, colname in bact_name_config.items()}
    current_names = BacteriaMappingNames(**bact_names_plain)
    return current_names


