"""A collection of input functions for the Snaketopia workflow."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
import pathlib
import re
from typing import List, Literal, Optional, Union

import numpy as np
import pandas as pd

import helpers

from input_names import RunsheetNames, BacteriaMappingNames, LISDataNames

logger = logging.getLogger("snake_helpers")


def get_reads_from_overview(sample: str, sample_overview: pd.DataFrame,
                            read_type: Literal["r1", "r2", "extra"]) -> str:
    """Get the path to reads of the specified type for the given sample.

    Arguments:
        sample:             the isolate number for which reads should be retrieved
        sample_overview:    the file of filenames mapping sample number to read files
        read_type:          the read type (r1, r2 or extra - the last for long reads)

    Returns:
        The path to the requested read file for the isolate.

    Raises:
        KeyError:   if the requested isolate is not present in the file or does not have the
                    requested kind of reads (e.g. forward read for ONT data)
        ValueError: if an invalid read type is requested
    """
    valid_types = ['r1', 'r2', 'extra']
    if read_type not in valid_types:
        raise ValueError(f"{read_type} is not a valid read type. "
                         f"Valid read types are {valid_types}.")
    if sample not in sample_overview["sample"].values:
        missing_sample_error = helpers.PrettyKeyErrorMessage(f"Isolate {sample} not found "
                                                             f"in read overview.")
        raise KeyError(missing_sample_error)
    sample_read = sample_overview.loc[sample_overview["sample"] == sample, read_type].squeeze()
    if pd.isna(sample_read):
        missing_read_error = helpers.PrettyKeyErrorMessage(f"No {read_type} reads found"
                                                           f" for {sample}.")
        raise KeyError(missing_read_error)

    return sample_read


# TODO: check error types?
def get_nanopore_run_report(sample_overview: pd.DataFrame) -> str:
    """Get the path to the run report for the Nanopore run from which samples originate.
    Arguments:
        sample_overview:    the file of filenames mapping sample number to read files

    Returns:
        The path to the JSON-formatted run report.
    Raises:
        FileNotFoundError:  if no Nanopore report is found
        KeyError:           if no Nanopore samples are present
        ValueError:         if Nanopore samples are from multiple runs or there are multiple reports
    """
    if pd.isna(sample_overview["extra"]).all():
        raise KeyError("No Nanopore samples found.")
    # the report is in the subdir below fastq_pass
    run_dirs = sample_overview["extra"].apply(lambda nanopore_read:
                                              pathlib.Path(nanopore_read).parents[1]).unique()
    if len(run_dirs) > 1:
        raise ValueError("Nanopore samples from multiple runs are not supported.")
    run_dir = run_dirs[0]
    run_reports = list(run_dir.glob("report*.json"))
    if not run_reports:
        raise FileNotFoundError("No run report found.")
    if len(run_reports) > 1:
        raise ValueError("Multiple run reports found.")
    run_report = str(run_reports[0])
    return run_report


def get_all_assemblies(illumina_samples: List[str], nanopore_samples: List[str],
                       hybrid_samples: List[str], experiment_name: Optional[str] = None) \
        -> List[str]:
    """Generate paths to assembly files for all sample types found

    Arguments:
        illumina_samples:   Overview of samples exclusively sequenced with Illumina
        nanopore_samples:   Overview of samples exclusively sequenced with Nanopore
        hybrid_samples:     Overview of samples sequenced with both Illumina and Nanopore (for
                            hybrid assembly)
        experiment_name:    the name of the sequencing run, if given
    Returns:
        Paths to assembly files for all samples; paths are specific for samples' sequencing methods

    """
    # only add experiment name *and underscore* if needed
    experiment_prefix = f"{experiment_name}_" if experiment_name else ""
    # initialize our list - if there are no Illumina samples we'll just have an empty list
    all_assemblies = [f"{sample}/assembly/{experiment_prefix}{sample}.fna"
                      for sample in illumina_samples]
    # extend the initialized list with any nanopore/hybrid samples we might have
    all_assemblies.extend([f"{sample}/nanopore_assembly/{experiment_prefix}{sample}.fasta"
                           for sample in nanopore_samples])
    all_assemblies.extend([f"{sample}/hybrid_assembly/{experiment_prefix}{sample}.fasta"
                           for sample in hybrid_samples])
    # we should now have *something* in our list
    if not all_assemblies:
        raise ValueError("No Illumina, Nanopore or hybrid samples given.")
    return all_assemblies


def get_all_single_quast(illumina_samples: List[str], nanopore_samples: List[str],
                         hybrid_samples: List[str]) -> List[str]:
    """Generate paths to individual QUAST output for all sample types found

    Arguments:
        illumina_samples:   Overview of samples exclusively sequenced with Illumina
        nanopore_samples:   Overview of samples exclusively sequenced with Nanopore
        hybrid_samples:     Overview of samples sequenced with both Illumina and Nanopore (for
                            hybrid assembly)
    Returns:
        Paths to individual QUAST output for all samples; paths are specific for samples' sequencing
        methods
    """
    # initialize our list - if there are no Illumina samples we'll just have an empty list
    all_single_quast = [f"{sample}/assembly/quast/illumina_report.tsv"
                        for sample in illumina_samples]
    # extend the initialized list with any nanopore/hybrid samples we might have
    all_single_quast.extend([f"{sample}/assembly/quast/nanopore_report.tsv"
                           for sample in nanopore_samples])
    all_single_quast.extend([f"{sample}/assembly/quast/hybrid_report.tsv"
                             for sample in hybrid_samples])
    # we should now have *something* in our list
    if not all_single_quast:
        raise ValueError("No Illumina, Nanopore or hybrid samples given.")
    return all_single_quast


def get_all_read_reports(illumina_samples: List[str], nanopore_samples: List[str]) -> List[str]:
    """Get all Quast read reports for Illumina and Nanopore samples
     (hybrid runs are not supported for now).

    Arguments:
        illumina_samples:   Overview of samples exclusively sequenced with Illumina
        nanopore_samples:   Overview of samples exclusively sequenced with Nanopore

    Returns:
        Paths to quast read reports for Illumina and Nanopore saples

    Raises:
        ValueError: if no Illumina or Nanopore samples are given
    """
    # initialize our list - if there are no Illumina samples we'll just have an empty list
    all_read_reports = [f"{sample}/assembly/quast/reads_stats/illumina_reads_report.tsv"
                        for sample in illumina_samples]
    # extend the initialized list with any nanopore/hybrid samples we might have
    all_read_reports.extend([f"{sample}/assembly/quast/reads_stats/nanopore_reads_report.tsv"
                           for sample in nanopore_samples])
    # we should now have *something* in our list
    if not all_read_reports:
        raise ValueError("No Illumina or Nanopore samples given.")
    return all_read_reports



def get_qc_platforms(illumina_samples: List[str], nanopore_samples: List[str],
                     hybrid_samples: List[str]) -> List[str]:
    """Get all sequencing approaches used in the current batch. Hybrid sequencing counts as its own
        approach.

    Arguments:
        illumina_samples:   Overview of samples exclusively sequenced with Illumina
        nanopore_samples:   Overview of samples exclusively sequenced with Nanopore
        hybrid_samples:     Overview of samples sequenced with both Illumina and Nanopore (for
                            hybrid assembly)
    Returns:
        Sequencing approaches used in the current batch (illumina, nanopore and/or hybrid)
    """
    qc_platforms = []
    if illumina_samples:
        qc_platforms.append("illumina")
    if nanopore_samples:
        qc_platforms.append("nanopore")
    if hybrid_samples:
        qc_platforms.append("hybrid")
    if not qc_platforms:
        raise ValueError("No Illumina, Nanopore or hybrid samples given.")
    return qc_platforms


def is_gzipped(sample: str, sample_overview: pd.DataFrame) -> bool:
    """Identify compression status for nanopore fastq files.

    Arguments:
        sample:          Name of the current sample
        sample_overview: Overview of samples containing paths to barcode directories in "extra"

    Returns:
        True if all files in the barcode directory are gzipped, False if not.

    """
    barcode_dir = pathlib.Path(get_reads_from_overview(sample, sample_overview,
                                                       read_type = "extra"))

    extensions = [read_file.suffix for read_file in barcode_dir.iterdir() if read_file.is_file()]
    if all(extension == ".gz" for extension in extensions):
        return True
    if all(extension == ".fastq" for extension in extensions):
        return False
    # if we haven't returned by now something is wrong
    raise ValueError(f"Extensions {sorted(extensions)} not supported.")


# TODO: make these more efficient now we're not tied to the snakefile

def get_assembly_name(sample: str, illumina_samples: List[str], nanopore_samples: List[str],
                      hybrid_samples: List[str], experiment_name: Optional[str] = None) -> str:
    """Return illumina, nanopore or hybrid assembly file name depending on sample type.

    Arguments:
        sample:             Name of the current sample
        illumina_samples:   All samples only sequenced with Illumina in the current batch
        nanopore_samples:   All samples only sequenced with Nanopore in the current batch
        hybrid_samples:     All samples sequenced with Illumina and Nanopore in the current batch
        experiment_name:    the name of the sequencing run, if given


    Returns:
        The path to the assembly file for the current sample.

    """
    # only add experiment name *and underscore* if needed
    experiment_prefix = f"{experiment_name}_" if experiment_name else ""
    if sample in illumina_samples:
        return f"{sample}/assembly/{experiment_prefix}{sample}.fna"
    if sample in nanopore_samples:
        return f"{sample}/nanopore_assembly/{experiment_prefix}{sample}.fasta"
    if sample in hybrid_samples:
        return f"{sample}/hybrid_assembly/{experiment_prefix}{sample}.fasta"
    raise KeyError(f"Sample {sample} not found.")


def get_reads(sample: str, illumina_samples: List[str], nanopore_samples: List[str],
              hybrid_samples: List[str]) -> List[str]:
    """Return Illumina paired end reads or Nanopore reads depending on sample type. For hybrid
    samples, Illumina reads will be returned as they have higher quality.

    Arguments:
        sample:             Name of the current sample
        illumina_samples:   All samples only sequenced with Illumina in the current batch
        nanopore_samples:   All samples only sequenced with Nanopore in the current batch
        hybrid_samples:     All samples sequenced with Illumina and Nanopore in the current batch

    Returns:
        The read file(s) for the current sample, with Illumina reads being favored if present.

    """
    if sample in illumina_samples or sample in hybrid_samples:
        return [f"{sample}/quality-control/{sample}_R1.fastq.gz",
                f"{sample}/quality-control/{sample}_R2.fastq.gz"]
    if sample in nanopore_samples:
        return [f"{sample}/nanopore_reads/{sample}.downsample.fastq"]
    raise KeyError(f"Sample {sample} not found.")


# TODO: pass this information in a better way? Structured by species somehow?
def get_abritamr_organism(sample: str, lis_report: pd.DataFrame = None,
                          species_coding: pd.DataFrame = None,
                          lis_data_names: Optional[LISDataNames] = None,
                          bacteria_names: Optional[BacteriaMappingNames] = None) -> str:
    """Supply additional information on organism to abritAMR depending on lab identification of
    the sample.

    Arguments:
        sample:             the current sample's sample number
        lis_report:         the LIS report, "translated" so sample number format matches.
                            Must contain bacteria code
        species_coding:     a translation of bacteria codes to taxonomic species names (not just
                            internal names, which may be on strain/serotype level)
        lis_data_names:     Column names in the LIS. Required when using LIS report
        bacteria_names:     Column names in the code -> species name mapping.
                            Required when using LIS report

    Returns:
        The "--species" flag to be passed to abritAMR if the species is being examined separately
        and abritAMR has point mutations for it; a blank string if not.
    """
    # get abritamr species and genus level lists
    abritamr_at_species_level = {"Clostridium difficile": "Clostridioides_difficile",
                                 "Acinetobacter baumannii": "Acinetobacter_baumannii",
                                 "Enterococcus faecalis": "Enterococcus_faecalis",
                                 "Enterococcus faecium": "Enterococcus_faecium",
                                 "Staphylococcus aureus": "Staphylococcus_aureus",
                                 "Staphylococcus pseudintermedius":
                                     "Staphylococcus_pseudintermedius",
                                 "Streptococcus agalactiae": "Streptococcus_agalactiae",
                                 "Streptococcus pneumoniae": "Streptococcus_pneumoniae",
                                 "Streptococcus pyogenes": "Streptococcus_pyogenes"
                                 }
    abritamr_at_genus_level = {"Neisseria", "Campylobacter", "Escherichia", "Klebsiella",
                               "Salmonella"}
    # does it match any of the explicitly specified species?
    # Get the name if it's a hit, otherwise initialize to blank
    abritamr_species = ""
    if lis_report is not None:
        # we need the column names - in snake_helpers it's more logical not to have defaults here
        if lis_data_names is not None and bacteria_names is not None:
            if species_coding is not None:
                species = helpers.get_species_by_number(sample,
                                                        lis_report,
                                                        species_coding,
                                                        lis_data_names = lis_data_names,
                                                        bacteria_names = bacteria_names)
                abritamr_species = abritamr_at_species_level.get(species, "")
                # if it doesn't, is this because there's no species or does it match a genus?
                if not abritamr_species and pd.notna(species):
                    abritamr_genus_pattern = "|".join(abritamr_at_genus_level)
                    abritamr_genus_match = re.search(f"({abritamr_genus_pattern})", species)
                    if abritamr_genus_match:
                        abritamr_species = abritamr_genus_match.group(0)
                # if we have a match by now, slap the flag on
                if abritamr_species:
                    abritamr_species = f"--species {abritamr_species}"
            else:
                raise ValueError("When using a LIS report, a translation of the local bacteria codes "
                                 "to species names is required.")
        else:
            raise ValueError("LIS and bacteria mapping column names have to be specified"
                             " when using LIS "
                     "data.")
    return abritamr_species


def get_serotyping_report(sample: str, ecoli_samples: List[str], salmonella_samples: List[str])\
        -> Union[List, str]:
    """Get the correct serotyping report for E. coli or Salmonella.

    Arguments:
        sample:             the name of the current sample
        ecoli_samples:      all samples identified as E. coli by lab methods
        salmonella_samples: all samples identified as Salmonella by lab methods

    Returns:
        SerotypeFinder reports for E. coli; SeqSero2 report for Salmonella; empty list for the rest
        (as Snakemake needs input to be a string or list thereof).

    """
    if sample in ecoli_samples:
        return f"{sample}/typing/serotypefinder/results_tab.tsv"
    if sample in salmonella_samples:
        return f"{sample}/typing/SeqSero/SeqSero_result.tsv"
    # if it's not E. coli or Salmonella, it doesn't need a serotype report
    return []


def infer_toxin_column(indications: List[str]) -> List[str]:
    """Try to find toxin gene identification in a list of arbitrary indications.

    Arguments:
        indications:    the various sequencing indications

    Returns:
        Any indication(s) containing the word "toxin" (regardless of case), if present, else None.
    """
    toxin_indications = [indication for indication in indications if re.search("toxin", indication,
                                                                               re.IGNORECASE)]
    if not toxin_indications:
        logger.info("No indication for toxin gene identification found.")
    else:
        logger.info(f"Samples with indication(s) {sorted(toxin_indications)} will be screened"
                    " for toxin genes.")
    return toxin_indications


def get_toxin_samples(runsheet_data: pd.DataFrame, runsheet_names: RunsheetNames) -> List[str]:
    """Get all samples which have any indication relating to toxin genes from a runsheet.

    Arguments:
        runsheet_data:  the runsheet for the current run
        runsheet_names: the column names for the runsheet in question

    Returns:
        Any samples which have any indication relating to toxin genes in the runsheet, if any.
    """
    toxin_indications = infer_toxin_column(runsheet_names.indications)
    # we don't need to try and slice the df if we aren't looking for toxins to begin with
    if not toxin_indications:
        return []
    # get all rows in which *any* of the toxin columns are positive
    any_with_toxins = runsheet_data[toxin_indications].apply(np.logical_or.reduce, axis=1)
    toxin_samples = runsheet_data.loc[any_with_toxins, runsheet_names.sample_number].tolist()
    return toxin_samples
