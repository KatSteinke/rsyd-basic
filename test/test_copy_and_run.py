#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import filecmp
import logging
import pathlib
import re
import shutil
import unittest

from subprocess import SubprocessError

import pandas as pd
import pytest

import copy_and_run

from helpers import MissingReadfilesError
from input_names import RunsheetNames


class TestRsyncWithRetries(unittest.TestCase):
    @classmethod
    def tearDownClass(cls) -> None:
        # empty out test_dirs
        (pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_test" / "test_file").unlink()
        (pathlib.Path(__file__).parent / "data" / "copy_test" / "file_test" / "test_file").unlink()

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_copy_dir_success(self):
        source_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "source_test"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_test"
        log_msg = f"Successfully rsynced {source_dir} to {dest_dir} (tries: 1)"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.rsync_with_check(source_dir, dest_dir)
            common_files = [source_file.name for source_file in source_dir.iterdir()]
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(source_dir, dest_dir, common_files, shallow = False)
            assert not compare_mismatches
            assert not compare_errors
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_copy_file_success(self):
        source_file = pathlib.Path(__file__).parent / "data" / "copy_test" / "source_test" \
                     / "test_file"
        dest_file = pathlib.Path(__file__).parent / "data" / "copy_test" / "file_test" / "test_file"
        log_msg = f"Successfully rsynced {source_file} to {dest_file} " \
                  f"(tries: 1)"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.rsync_with_check(source_file, dest_file)
            assert filecmp.cmp(source_file, dest_file, shallow = False)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_copy_failure(self):
        source_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "no_such_source"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "no_such_dest"
        error_msg = f"Rsyncing of {source_dir} to {dest_dir} failed with error code 23 " \
                    f"after 3 tries"
        with pytest.raises(SubprocessError, match=re.escape(error_msg)):
            copy_and_run.rsync_with_check(source_dir, dest_dir)


class TestCopyInput(unittest.TestCase):
    @classmethod
    def tearDownClass(cls) -> None:
        # clean up data after running
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_for_all")
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_ilm_translate")
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_nanopore")
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_with_mads")
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "copy_test"
                      / "dest_with_mads_string")

    # expected read data is generated semi-dynamically per test but needs to be cleaned up afterwards
    def tearDown(self) -> None:
        (pathlib.Path(__file__).parent
         / "data"
         / "copy_test"
         / "expected_results"
         / "current_outdir"
         / "reads_moved.tsv").write_text(data="", encoding = "utf-8")
        (pathlib.Path(__file__).parent
         / "data"
         / "copy_test"
         / "expected_results_mads"
         / "current_outdir"
         / "reads_moved.tsv").write_text(data = "", encoding = "utf-8")

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_copy_success(self):
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "sequencing_test"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "copy_test" / "runsheet_test.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_for_all"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "illumina",
                       "platform": "MiSeq",
                       "sample_number_settings": {"format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                       r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'number',
                                                  'sample_numbers_out': 'letter',
                                                   "sample_numbers_report": "number",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"}}
                       }
        expected_results = pathlib.Path(__file__).parent / "data" / "copy_test" / "expected_results"
        # semi-dynamically create expected read overview (parent path may vary)
        expected_reads = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                              "runtype": ["paired-end"],
                                              "r1": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R1_001.fastq.gz"],
                                              "r2": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R2_001.fastq.gz"],
                                              "extra": [""]})
        expected_reads.to_csv((expected_results / "current_outdir" / "reads_moved.tsv"), sep="\t",
                              index=False)
        log_msg = f"Successfully copied all inputs to {str(dest_dir)}.\n" \
                  f"Inputs:\n" \
                  f"- outdir: {str(outdir)}\n" \
                  f"- reads: {str(sequencing_dir)}\n" \
                  f"- runsheet: {str(runsheet)}"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples
            common_files = [source_path.name for source_path in expected_results.iterdir()
                            if not source_path.is_dir()]
            common_files.extend([f"{source_path.name}/{subdir_path.name}"
                                 for source_path in expected_results.iterdir()
                                 if source_path.is_dir()
                                 for subdir_path in source_path.iterdir()])
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(expected_results, dest_dir, common_files,
                                                shallow = False)
            assert not compare_mismatches
            assert not compare_errors

    def test_copy_translated_illumina(self):
        """Copy data with a different Illumina sample/metadata sheet."""
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "sequencing_test"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = (pathlib.Path(__file__).parent / "data" / "copy_test"
                    / "runsheet_ilm_en.xlsx")
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_ilm_translate"
        sheet_names_en = RunsheetNames(sample_info_sheet = "Sample_information",
                                       sample_number = "Sample_number",
                                       extraction_well_number = "Extraction_well_number",
                                       extraction_run_number = "Extraction_run_number",
                                       dna_concentration = "DNA_concentration",
                                       requested_by = "Requested_by",
                                       comment = "Comment",
                                       sequencing_run_number = "sequencing_run_number",
                                       indications = ["AMR", "Identification", "Relapse", "HAI",
                                                      "Toxin gene identification",
                                                      "Environmental sample isolate",
                                                      "Outbreak sample isolate",
                                                      "Surveillance sample isolate", "Research",
                                                      "Other"],
                                       dna_normalization_sheet = "DNA normalization",
                                       dna_normalization_sample_nr = "Sample_number",
                                       ilm_well_number = "ILM_well_position",
                                       plate_layout = "Plate layout",
                                       runsheet_base = "Runsheet",
                                       experiment_name = "RUNxxxx-INI",
                                       nanopore_sheet_sample_number = "Sample_number",
                                       barcode = "Barcode",
                                       protocol_version = "Protokol (versionsnummer)")
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "illumina",
                       "platform": "MiSeq",
                       "sample_number_settings": {"format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'number',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "number",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"}}
                       }
        expected_results = pathlib.Path(__file__).parent / "data" / "copy_test" / "expected_results_en"
        # semi-dynamically create expected read overview (parent path may vary)
        expected_reads = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                              "runtype": ["paired-end"],
                                              "r1": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R1_001.fastq.gz"],
                                              "r2": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R2_001.fastq.gz"],
                                              "extra": [""]})
        expected_reads.to_csv((expected_results / "current_outdir" / "reads_moved.tsv"), sep = "\t",
                              index = False)
        log_msg = f"Successfully copied all inputs to {str(dest_dir)}.\n" \
                  f"Inputs:\n" \
                  f"- outdir: {str(outdir)}\n" \
                  f"- reads: {str(sequencing_dir)}\n" \
                  f"- runsheet: {str(runsheet)}"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config, runsheet_names = sheet_names_en)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples
            common_files = [source_path.name for source_path in expected_results.iterdir()
                            if not source_path.is_dir()]
            common_files.extend([f"{source_path.name}/{subdir_path.name}"
                                 for source_path in expected_results.iterdir()
                                 if source_path.is_dir()
                                 for subdir_path in source_path.iterdir()])
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(expected_results, dest_dir, common_files,
                                                shallow = False)
            assert not compare_mismatches
            assert not compare_errors

    def test_copy_nanopore_success(self):
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "nanopore_test"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_nanopore"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "sample_number_settings": {"format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'number',
                                                  'sample_numbers_out': 'letter',
                                                  'sample_numbers_report': 'number',
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"}}
                       }
        expected_results = pathlib.Path(__file__).parent / "data" / "copy_test" / "expected_nanopore"
        # semi-dynamically create expected read overview (parent path may vary)
        expected_reads = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                              "runtype": ["ont", "ont"],
                                              "r1": ["", ""],
                                              "r2": ["", ""],
                                              "extra": [dest_dir / "reads" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode01",
                                                        dest_dir / "reads" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode42"
                                                        ]})
        expected_reads.to_csv((expected_results / "current_outdir" / "reads_moved.tsv"), sep="\t",
                              index=False)
        log_msg = f"Successfully copied all inputs to {str(dest_dir)}.\n" \
                  f"Inputs:\n" \
                  f"- nanopore_report: " \
                   f"{str(sequencing_dir / 'rawdata' / 'subdir' / 'report_testrun.json')}\n" \
                  f"- outdir: {str(outdir)}\n" \
                  f"- reads: {str(sequencing_dir / 'rawdata' / 'subdir' / 'fastq_pass')}\n" \
                  f"- runsheet: {str(runsheet)}"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples

            common_files = [source_path.name for source_path in expected_results.iterdir()
                            if not source_path.is_dir()]
            #common_files.extend([f"{source_path.name}/{subdir_path.name}"
            #                     for source_path in expected_results.iterdir()
            #                     if source_path.is_dir()
            #                     for subdir_path in source_path.iterdir()])
            # hardcode for now
            outdir_files = [ "current_outdir/reads_moved.tsv"]
            barcode_files = [( "reads/rawdata/subdir/fastq_pass"
                              "/barcode01/blank_read.fastq.gz"),
                             ("reads/rawdata/subdir/fastq_pass"
                              "/barcode42/blank_read.fastq.gz"),
                             ("reads/rawdata/subdir/fastq_pass"
                              "/barcode42/blank_read.fastq")
                             ]
            report_files = [("reads/rawdata/subdir"
                             "/report_testrun.json")]
            common_files.extend([*outdir_files, *barcode_files, *report_files])
            print(common_files)
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(expected_results, dest_dir, common_files,
                                                shallow = False)
            print(compare_errors)
            assert not compare_mismatches
            assert not compare_errors

    def test_copy_success_mads(self):
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "sequencing_test"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "copy_test" / "runsheet_test.xlsx"
        lis_data = pathlib.Path(__file__).parent / "data" / "copy_test" / "test_mads.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "copy_test" / "bacteria_codes.csv"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_with_mads"
        test_config = {"lab_info_system": {"use_lis_features": True,
                                           "lis_report": lis_data,
                                           "bacteria_codes": bacteria_codes},
                       "sequencing_mode": "illumina",
                       "platform": "MiSeq",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT]|[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)'
                                                  }}
        expected_results = pathlib.Path(__file__).parent / "data" / "copy_test" \
                           / "expected_results_mads"
        # semi-dynamically create expected read overview (parent path may vary)
        expected_reads = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                              "runtype": ["paired-end"],
                                              "r1": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R1_001.fastq.gz"],
                                              "r2": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R2_001.fastq.gz"],
                                              "extra": [""]})
        expected_reads.to_csv((expected_results / "current_outdir" / "reads_moved.tsv"), sep = "\t",
                              index = False)
        log_msg = f"Successfully copied all inputs to {str(dest_dir)}.\n" \
                  f"Inputs:\n" \
                  f"- bacteria_codes: {str(bacteria_codes)}\n" \
                  f"- lis_report: {str(lis_data)}\n" \
                  f"- outdir: {str(outdir)}\n" \
                  f"- reads: {str(sequencing_dir)}\n" \
                  f"- runsheet: {str(runsheet)}"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples
            common_files = [source_path.name for source_path in expected_results.iterdir()
                            if not source_path.is_dir()]
            common_files.extend([f"{source_path.name}/{subdir_path.name}"
                                 for source_path in expected_results.iterdir()
                                if source_path.is_dir()
                                 for subdir_path in source_path.iterdir()])
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(expected_results, dest_dir, common_files,
                                                shallow = False)
            assert not compare_mismatches
            assert not compare_errors

    def test_copy_success_mads_string(self):
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "sequencing_test"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "copy_test" / "runsheet_test.xlsx"
        lis_data = pathlib.Path(__file__).parent / "data" / "copy_test" / "test_mads.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "copy_test" / "bacteria_codes.csv"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_with_mads_string"
        test_config = {"lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(lis_data),
                                           "bacteria_codes": str(bacteria_codes)},
                       "sequencing_mode": "illumina",
                       "platform": "MiSeq",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT]|[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  }}
        expected_results = pathlib.Path(__file__).parent / "data" / "copy_test" \
                           / "expected_results_mads"
        # semi-dynamically create expected read overview (parent path may vary)
        expected_reads = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                              "runtype": ["paired-end"],
                                              "r1": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R1_001.fastq.gz"],
                                              "r2": [dest_dir / "reads"
                                                     / "B99123456-1_L001_R2_001.fastq.gz"],
                                              "extra": [""]})
        expected_reads.to_csv((expected_results / "current_outdir" / "reads_moved.tsv"), sep = "\t",
                              index = False)
        log_msg = f"Successfully copied all inputs to {str(dest_dir)}.\n" \
                  f"Inputs:\n" \
                  f"- bacteria_codes: {str(bacteria_codes)}\n" \
                  f"- lis_report: {str(lis_data)}\n" \
                  f"- outdir: {str(outdir)}\n" \
                  f"- reads: {str(sequencing_dir)}\n" \
                  f"- runsheet: {str(runsheet)}"
        with self._caplog.at_level(level="INFO", logger="run_snaketopia"):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)
            assert ("run_snaketopia", logging.INFO, log_msg) in self._caplog.record_tuples
            common_files = [source_path.name for source_path in expected_results.iterdir()
                            if not source_path.is_dir()]
            common_files.extend([f"{source_path.name}/{subdir_path.name}"
                                 for source_path in expected_results.iterdir()
                                if source_path.is_dir()
                                 for subdir_path in source_path.iterdir()])
            (compare_matches,
             compare_mismatches,
             compare_errors) = filecmp.cmpfiles(expected_results, dest_dir, common_files,
                                                shallow = False)
            assert not compare_mismatches
            assert not compare_errors

    def test_fail_missing_original_reads(self):
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_missing"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "copy_test" / "runsheet_test.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_missing_reads"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "illumina",
                       "platform": "MiSeq",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)'
                                                  }}
        error_msg = f"R1 not found in {str(rawdata_dir)} for samples ['B99123456-1']. \n"
        with pytest.raises(MissingReadfilesError, match=re.escape(error_msg)):
            copy_and_run.set_up_inputs(rawdata_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)

    def test_fail_unsupported_format(self):
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_missing"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(__file__).parent / "data" / "copy_test" / "runsheet_test.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_missing_reads"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "pacbio",
                       "platform": "MiSeq",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  }}
        error_msg = "Sequencing mode pacbio is not supported."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            copy_and_run.set_up_inputs(rawdata_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)

    def test_fail_no_nanopore(self):
        """Fail if there is no Nanopore report to be copied up."""
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "nanopore_test_no_report"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_nanopore"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  }}

        error_msg = "No nanopore report found"
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)

    def test_fail_too_many_nanopore(self):
        """Fail if there are too many Nanopore reports."""
        sequencing_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "nanopore_test_multireport"
        outdir = pathlib.Path(__file__).parent / "data" / "copy_test" / "outdir_test"
        runsheet = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
        dest_dir = pathlib.Path(__file__).parent / "data" / "copy_test" / "dest_nanopore"
        test_config = {"lab_info_system": {"use_lis_features": False,
                                           "lis_report": None,
                                           "bacteria_codes": None},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"},
                                                  "sample_numbers_in": "letter",
                                                  "sample_numbers_out": "number",
                                                  "sample_numbers_report": "letter",
                                                  'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  }}


        error_msg = "Too many nanopore reports found"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            copy_and_run.set_up_inputs(sequencing_dir, outdir, runsheet, dest_dir,
                                       active_config = test_config)