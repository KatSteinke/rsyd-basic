import logging
import pathlib
import re
import unittest
import warnings

from datetime import date, datetime
from unittest import mock

import numpy as np
import pandas as pd
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

import database_tables
import helpers
import update_db
from database_tables import Sample, Isolate, SequencingRun, SeqRequest, ExtractRun, Eluate, \
    ReadFile, AssemblyReadFiles, Assembly

session = None


class TestReadResults(unittest.TestCase):
    def test_good_sheet(self):
        test_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                         "godkendt": [1, 0]})
        sheet_okay = update_db.check_result_sheet(test_data)
        assert sheet_okay

    def test_fail_no_data(self):
        test_data = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199123456-2"],
                                       "godkendt": [1, pd.NA]})
        error_msg = r"Missing approval status for sample(s) ['1199123456-2']." \
                    r" Please note approval status as 1 (approved) or 0 (failed)."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            update_db.check_result_sheet(test_data)

    def test_fail_wrong_params(self):
        test_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                         "godkendt": [1, "½"]})
        error_msg = r"Incorrect approval status for sample(s) ['1199123456-2']." \
                    r" Approval status can only be 1 (approved) or 0 (failed)."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            update_db.check_result_sheet(test_data)

    def test_fail_duplicated_samples(self):
        test_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-1"],
                                         "godkendt": [1, 0]})
        error_msg = "Result sheet contains multiple lines with isolate number(s) ['1199123456-1']. " \
                    "Please remove the duplicated lines so only the one you want to use remains."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            update_db.check_result_sheet(test_data)


class TestCheckFiles(unittest.TestCase):
    experiment_name = "ILM_Run0000_Y20230508_XYZ"

    def test_all_good(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        dir_okay = update_db.check_files_present(test_dir, self.experiment_name)
        assert dir_okay

    def test_old_names(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir_old_names"
        dir_okay = update_db.check_files_present(test_dir, self.experiment_name)
        assert dir_okay

    def test_fail_is_a_file(self):
        path_to_file = pathlib.Path(__file__).parent / "data" / "update_db" / "test_file"
        error_msg = "The path you have given is not a folder." \
                    " Please try again and enter the path to the folder with " \
                    "all analysis results."
        with pytest.raises(NotADirectoryError, match = re.escape(error_msg)):
            update_db.check_files_present(path_to_file, self.experiment_name)

    def test_fail_no_fofn(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_fofn"
        error_msg = f"No file with paths to reads found for run ILM_Run0000_Y20230508_XYZ" \
                    f" in folder {test_dir}."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_no_pairedend(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_pairedend"
        error_msg = "The following issues were found with the file with paths to reads:\n" \
                    "Missing forward reads for sample(s) ['B99123456-1'].\n" \
                    "Missing reverse reads for sample(s) ['B99123456-1']."
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_no_long(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_long"
        error_msg = "The following issues were found with the file with paths to reads:\n" \
                    "Missing long reads for sample(s) ['B99123456-1']."
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_no_illumina_asm(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_illumina_asm"
        error_msg = "Missing assemblies for sample(s) ['B99123456-1']."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_no_nanopore_asm(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_nanopore_asm"
        error_msg = "Missing assemblies for sample(s) ['B99123456-1']."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_no_hybrid_asm(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_no_hybrid_asm"
        error_msg = "Missing assemblies for sample(s) ['B99123456-1']."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_one_missing(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "one_found_one_missing"
        error_msg = "Missing assemblies for sample(s) ['B99123456-2']."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)

    def test_fail_both_missing(self):
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "fail_missing_multi_asm"
        error_msg = "Missing assemblies for sample(s) ['B99123456-1', 'B99123456-2']."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            update_db.check_files_present(test_dir, self.experiment_name)


class TestGetPipelineID(unittest.TestCase):
    def test_get_final_id(self):
        isolate_number = "1199123456-7"
        results = pd.DataFrame(data={"proevenr": ["1199123456-7"],
                                     "final_id": ["Placeholderia fakeorum"]})
        expected_call = "Placeholderia fakeorum"
        test_call = update_db.get_pipeline_species_call(isolate_number, results)
        assert expected_call == test_call

    def test_get_gtdb_id_final_blank(self):
        isolate_number = "1199123456-7"
        results = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                       "final_id": [""],
                                       "WGS_species_GTDB_basics": ["Placeholderia fakeorum"]})
        expected_call = "Placeholderia fakeorum"
        test_call = update_db.get_pipeline_species_call(isolate_number, results)
        assert expected_call == test_call

    def test_get_gtdb_id_no_final(self):
        isolate_number = "1199123456-7"
        results = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                       "WGS_species_GTDB_basics": ["Placeholderia fakeorum"]})
        expected_call = "Placeholderia fakeorum"
        test_call = update_db.get_pipeline_species_call(isolate_number, results)
        assert expected_call == test_call

    def test_get_kraken_id(self):
        isolate_number = "1199123456-7"
        results = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                       "WGS_species_kraken": ["Placeholderia fakeorum"]})
        expected_call = "Placeholderia fakeorum"
        test_call = update_db.get_pipeline_species_call(isolate_number, results)
        assert expected_call == test_call

    def test_handle_nan(self):
        isolate_number = "1199123456-7"
        results = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                       "WGS_species_kraken": [np.nan]})
        test_call = update_db.get_pipeline_species_call(isolate_number, results)
        assert test_call is None
        isolate_number_2 = "1199123456-7"
        results_2 = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                         "WGS_species_kraken": [pd.NA]})
        test_call_2 = update_db.get_pipeline_species_call(isolate_number_2, results_2)
        assert test_call_2 is None

    def test_fail_isolate_not_found(self):
        isolate_number = "1199123456-8"
        results = pd.DataFrame(data = {"proevenr": ["1199123456-7"],
                                       "final_id": [""],
                                       "WGS_species_GTDB_basics": ["Placeholderia fakeorum"]})
        error_msg = "Isolate number 1199123456-8 not found in results."
        with pytest.raises(KeyError, match=re.escape(error_msg)):
            update_db.get_pipeline_species_call(isolate_number, results)


class TestGetSequencingRun(unittest.TestCase):
    config_to_use = {"project_owner": "KMA",
                     "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                     "10": "D",
                                                                     "11": "F",
                                                                     "50": "T"},
                                                "sample_number_format":
                                                    '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                   r'(?P<sample_year>\d{2})'
                                                                   r'(?P<sample_number>\d{6})'
                                                                   r'(?P<bact_number>-\d{1})',
                                                "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                 r'(?P<sample_year>\d{2})'
                                                                 r'(?P<sample_number>\d{6})'
                                                                 r'(?P<bact_number>-\d{1})',
                                                "sample_numbers_in": "letter",
                                                "sample_numbers_out": "letter"},
                     "sequencing_mode": "illumina"}

    def test_success(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["MiSeq"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'utilities_test' \
                   / 'runsheet_experiment_name.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "MiSeq",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'utilities_test'
                                                                      / 'runsheet_experiment_name.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = self.config_to_use)
        # TODO: nicer equals handling in the class? but obs implications in other situations
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    def test_success_nanopore(self):
        """Handle a nanopore run"""
        config_to_use = {"project_owner": "KMA",
                         "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                         "10": "D",
                                                                         "11": "F",
                                                                         "50": "T"},
                                                    "sample_number_format":
                                                        '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                    "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                       r'(?P<sample_year>\d{2})'
                                                                       r'(?P<sample_number>\d{6})'
                                                                       r'(?P<bact_number>-\d{1})',
                                                    "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "sample_numbers_in": "letter",
                                                    "sample_numbers_out": "letter"},
                         "platform": "GridION",
                         "sequencing_mode": "nanopore"}
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["GridION"]})
        runsheet = pathlib.Path(__file__).parent / "data"/"update_db"/"test_nanopore_runsheet.xlsx"
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        expected_result = database_tables.SequencingRun(seq_run_name = "ONT_RUN0001_Y20990101_XYZ",
                                                        platform = "GridION",
                                                        sequencing_type = "nanopore",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'update_db'
                                                                      / 'test_nanopore_runsheet.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "miniondir"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2099-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = config_to_use)
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    def test_success_nanopore_short_initials(self):
        """Handle a nanopore run where the user's initials are under three characters"""
        config_to_use = {"project_owner": "KMA",
                         "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                         "10": "D",
                                                                         "11": "F",
                                                                         "50": "T"},
                                                    "sample_number_format":
                                                        '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                    "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                       r'(?P<sample_year>\d{2})'
                                                                       r'(?P<sample_number>\d{6})'
                                                                       r'(?P<bact_number>-\d{1})',
                                                    "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "sample_numbers_in": "letter",
                                                    "sample_numbers_out": "letter"},
                         "platform": "GridION",
                         "sequencing_mode": "nanopore"}
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["GridION"]})
        runsheet = (pathlib.Path(__file__).parent / "data"/"update_db"
                    /"test_nanopore_runsheet_short_ini.xlsx")
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        expected_result = database_tables.SequencingRun(seq_run_name = "ONT_RUN0001_Y20990101_XZ",
                                                        platform = "GridION",
                                                        sequencing_type = "nanopore",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'update_db'
                                                                      / 'test_nanopore_runsheet_short_ini.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "miniondir"),
                                                        run_user_id = "XZ",
                                                        run_date = date.fromisoformat("2099-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = config_to_use)
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    def test_success_nanopore_no_initials(self):
        """Handle a nanopore run where the user's initials are missing"""
        config_to_use = {"project_owner": "KMA",
                         "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                         "10": "D",
                                                                         "11": "F",
                                                                         "50": "T"},
                                                    "sample_number_format":
                                                        '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                    "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                       r'(?P<sample_year>\d{2})'
                                                                       r'(?P<sample_number>\d{6})'
                                                                       r'(?P<bact_number>-\d{1})',
                                                    "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "sample_numbers_in": "letter",
                                                    "sample_numbers_out": "letter"},
                         "platform": "GridION",
                         "sequencing_mode": "nanopore"}
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["GridION"]})
        runsheet = (pathlib.Path(__file__).parent / "data"/"update_db"
                    /"test_nanopore_runsheet_no_ini.xlsx")
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        expected_result = database_tables.SequencingRun(seq_run_name = "ONT_RUN0001_Y20990101",
                                                        platform = "GridION",
                                                        sequencing_type = "nanopore",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'update_db'
                                                                      / 'test_nanopore_runsheet_no_ini.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "miniondir"),
                                                        run_user_id = "Unknown",
                                                        run_date = date.fromisoformat("2099-01-01"),
                                                        institution = "KMA")
        log_msg = ("WARNING:update_database:No initials found in run name ONT_RUN0001_Y20990101."
                   " Run user will have to be added manually.")
        with self.assertLogs("update_database") as logged:
            test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                                   active_config = config_to_use)
            assert log_msg in logged.output
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    def test_fix_caps(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["MiSeq"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
                   / 'runsheet_lowercase_ini.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "MiSeq",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'update_db'
                                                                      / 'runsheet_lowercase_ini.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = self.config_to_use)
        # TODO: nicer equals handling in the class? but obs implications in other situations
        assert test_result.run_user_id == expected_result.run_user_id

    def test_success_no_platform(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'utilities_test' \
                   / 'runsheet_experiment_name.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'utilities_test'
                                                                      / 'runsheet_experiment_name.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = self.config_to_use)
        # TODO: nicer equals handling in the class? but obs implications in other situations
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    def test_success_handle_nan(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["MiSeq"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
                   / 'runsheet_no_user.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "MiSeq",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'update_db'
                                                                      / 'runsheet_no_user.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = None,
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")
        test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                               active_config = self.config_to_use)
        assert test_result.seq_run_name == expected_result.seq_run_name
        assert test_result.platform == expected_result.platform
        assert test_result.sequencing_type == expected_result.sequencing_type
        assert test_result.flowcell_id == expected_result.flowcell_id
        assert test_result.runsheet_path == expected_result.runsheet_path
        assert test_result.data_path == expected_result.data_path
        assert test_result.run_user_id == expected_result.run_user_id
        assert test_result.run_date == expected_result.run_date
        assert test_result.institution == expected_result.institution

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_log_bad_date(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["MiSeq"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
                   / 'runsheet_experiment_name_badyear.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        warn_msg = "Run date could not be inferred for run ILM_Run0000_Y2022-01-01_XYZ. " \
                   "Date will have to be entered manually."
        with self._caplog.at_level(level="WARNING", logger="update_database"):
            update_db.get_run_for_db(qc_results, runsheet, rundir,
                                     active_config = self.config_to_use)
            assert ("update_database", logging.WARNING, warn_msg) in self._caplog.record_tuples

    def test_log_multi_platform(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1", "B99123456-2"],
                                          "godkendt": [1, 1],
                                          "Bestilt af": ["XYZ", "XYZ"],
                                          "Indikation": ["Identifikation", "Identifikation"],
                                          "Avg. coverage depth_basics": [100, 100],
                                          "Grampos/neg": ["Positiv", "Positiv"],
                                          "MADS species": ["Placeholderia", "Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA, pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum", "Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype", "FakeSerotype"],
                                          "platform": ["MiSeq", 'NextSeq']})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'utilities_test' \
                   / 'runsheet_experiment_name.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "MiSeq;NextSeq",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'utilities_test'
                                                                      / 'runsheet_experiment_name.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")
        warn_msg = "Multiple sequencing platforms found for the run."
        with self._caplog.at_level(level="INFO", logger="update_database"):
            test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                                   active_config = self.config_to_use)
            assert ("update_database", logging.INFO, warn_msg) in self._caplog.record_tuples
            assert test_result.seq_run_name == expected_result.seq_run_name
            assert test_result.platform == expected_result.platform
            assert test_result.sequencing_type == expected_result.sequencing_type
            assert test_result.flowcell_id == expected_result.flowcell_id
            assert test_result.runsheet_path == expected_result.runsheet_path
            assert test_result.data_path == expected_result.data_path
            assert test_result.run_user_id == expected_result.run_user_id
            assert test_result.run_date == expected_result.run_date
            assert test_result.institution == expected_result.institution

    def test_log_no_platform(self):
        qc_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["Identifikation"],
                                          "Avg. coverage depth_basics": [100],
                                          "Grampos/neg": ["Positiv"],
                                          "MADS species": ["Placeholderia"],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Placeholderia fakeorum"],
                                          "Serotypning": ["FakeSerotype"],
                                          "platform": ["Sanger"]})
        runsheet = pathlib.Path(__file__).parent / 'data' / 'utilities_test' \
                   / 'runsheet_experiment_name.xlsx'
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"

        expected_result = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                                        platform = "Sanger",
                                                        sequencing_type = "illumina",
                                                        flowcell_id = None,
                                                        runsheet_path = str(pathlib.Path(__file__).parent
                                                                      / 'data'
                                                                      / 'utilities_test'
                                                                      / 'runsheet_experiment_name.xlsx'),
                                                        data_path = str(pathlib.Path(__file__).parent
                                                                  / "data"
                                                                  / "utilities_test"
                                                                  / "fastq_nextseq"),
                                                        run_user_id = "XYZ",
                                                        run_date = date.fromisoformat("2022-01-01"),
                                                        institution = "KMA")

        platform_to_technique = {'illumina_platforms': {'MiSeq', 'NextSeq'},
                                 'nanopore_platforms': {'MinION', 'GridION'}}
        warn_msg = 'No sequencing technique could be inferred for platform Sanger. ' \
                   f'Valid Illumina platforms are {platform_to_technique["illumina_platforms"]}. ' \
                   f'Valid Nanopore platforms are {platform_to_technique["nanopore_platforms"]}. ' \
                   'Sequencing technique will be inferred from the config file.'
        with self._caplog.at_level(level="WARNING", logger="update_database"):
            test_result = update_db.get_run_for_db(qc_results, runsheet, rundir,
                                                   active_config = self.config_to_use)
            assert ("update_database", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert test_result.seq_run_name == expected_result.seq_run_name
            assert test_result.platform == expected_result.platform
            assert test_result.sequencing_type == expected_result.sequencing_type
            assert test_result.flowcell_id == expected_result.flowcell_id
            assert test_result.runsheet_path == expected_result.runsheet_path
            assert test_result.data_path == expected_result.data_path
            assert test_result.run_user_id == expected_result.run_user_id
            assert test_result.run_date == expected_result.run_date
            assert test_result.institution == expected_result.institution


class TestGetEluateRuns(unittest.TestCase):
    config_to_use = {"project_owner": "KMA",
                     "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                     "10": "D",
                                                                     "11": "F",
                                                                     "50": "T"},
                                                "sample_number_format":
                                                    '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                   r'(?P<sample_year>\d{2})'
                                                                   r'(?P<sample_number>\d{6})'
                                                                   r'(?P<bact_number>-\d{1})',
                                                "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                 r'(?P<sample_year>\d{2})'
                                                                 r'(?P<sample_number>\d{6})'
                                                                 r'(?P<bact_number>-\d{1})',
                                                "sample_numbers_in": "letter",
                                                "sample_numbers_out": "letter"}}

    def test_single_run(self):
        runsheet = pd.DataFrame(data = {"Oprensnings_kørselsnr": ['0001', '0001']})
        expected_result = database_tables.ExtractRun(extract_run_id = 1, extract_run_name = '0001',
                                                     institution = "KMA")
        test_results = update_db.get_extract_runs(runsheet, self.config_to_use)
        assert len(test_results) == 1
        test_extract_run = test_results[0]
        assert test_extract_run.extract_run_id == expected_result.extract_run_id
        assert test_extract_run.extract_run_name == expected_result.extract_run_name
        assert test_extract_run.institution == expected_result.institution

    def test_multi_runs(self):
        runsheet = pd.DataFrame(data = {"Oprensnings_kørselsnr": ['0001', '0002']})
        test_results = update_db.get_extract_runs(runsheet, self.config_to_use)
        assert len(test_results) == 2

    def test_bad_name(self):
        runsheet = pd.DataFrame(data = {"Oprensnings_kørselsnr": ['0001', 'run0002']})
        error_msg = 'Cannot extract run number from extraction run name run0002'
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_extract_runs(runsheet, self.config_to_use)


class TestGetEluate(unittest.TestCase):
    def test_eluate_success(self):
        runsheet = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99123456-2"],
                                        "Oprensnings_kørselsnr": ['0001', '0001']})
        test_eluate = update_db.get_eluate("F99123456-1", runsheet)
        expected_result = database_tables.Eluate(mads_isolatnr = "F99123456-1", extract_run_id = 1)
        assert test_eluate.mads_isolatnr == expected_result.mads_isolatnr
        assert test_eluate.extract_run_id == expected_result.extract_run_id

    def test_fail_wrong_number(self):
        runsheet = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99123456-2"],
                                        "Oprensnings_kørselsnr": ['0001', '0001']})
        error_msg = "Isolate F99123456-7 was not found in results given."
        with pytest.raises(KeyError, match=error_msg):
            update_db.get_eluate("F99123456-7", runsheet)

    def test_fail_bad_run_name(self):
        runsheet = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99123456-2"],
                                        "Oprensnings_kørselsnr": ['run0001', '0001']})
        error_msg = 'Cannot extract run number from extraction run name run0001'
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_eluate("F99123456-1", runsheet)


class TestGetReadfiles(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """Set up database connection."""
        # TODO: how can we test mysql specific things
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        global session
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)

    @classmethod
    def tearDownClass(cls) -> None:
        """Disconnect from database."""
        session.close()

    def setUp(self) -> None:
        """Truncate tables."""
        session.query(database_tables.ReadFile).delete()
        session.query(database_tables.AssemblyReadFiles).delete()

    seq_run = database_tables.SequencingRun(seq_run_name = "ILM_Run0000_Y20220101_XYZ",
                                            platform = "MiSeq",
                                            sequencing_type = "illumina",
                                            flowcell_id = None,
                                            runsheet_path = str(pathlib.Path(__file__).parent
                                                          / 'data'
                                                          / 'utilities_test'
                                                          / 'runsheet_experiment_name.xlsx'),
                                            data_path = str(pathlib.Path(__file__).parent
                                                      / "data"
                                                      / "utilities_test"
                                                      / "fastq_nextseq"),
                                            run_user_id = "XYZ",
                                            run_date = date.fromisoformat("2022-01-01"),
                                            institution = "KMA")
    eluate = database_tables.Eluate(mads_isolatnr = "F99123456-1", extract_run_id = 1)

    run_config = {"project_owner": "KMA"}

    def test_add_pairedend(self):
        path_to_reads = pd.DataFrame(data={"sample": ["F99123456-1"], "runtype": ["paired-end"],
                                           "r1": ["path/to/r1"], "r2": ["path/to/r2"],
                                           "extra": [None]})
        assembly_reads = update_db.get_assembly_readfiles("F99123456-1", path_to_reads,
                                                          self.seq_run, self.eluate,
                                                          self.run_config)
        session.add(assembly_reads)
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        # check that they have been created correctly
        fwd_read = session.query(
            database_tables.ReadFile).filter_by(mads_isolatnummer = "F99123456-1",
                                                read_direction = "forward").first()
        rev_read = session.query(
            database_tables.ReadFile).filter_by(mads_isolatnummer = "F99123456-1",
                                                read_direction = "reverse").first()
        assert fwd_read.read_file == "path/to/r1"
        assert fwd_read.read_type == "Illumina"
        assert rev_read.read_file == "path/to/r2"
        assert rev_read.read_type == "Illumina"
        asm_readfiles = session.query(database_tables.AssemblyReadFiles).first()
        assert asm_readfiles.forward_readfile_id == fwd_read.readfile_id
        assert asm_readfiles.reverse_readfile_id == rev_read.readfile_id
        assert not asm_readfiles.long_readfile_id

    def test_add_nanopore(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["F99123456-1"], "runtype": ["ont"],
                                             "r1": [None], "r2": [None],
                                             "extra": ["path/to/long_reads"]})
        assembly_reads = update_db.get_assembly_readfiles("F99123456-1", path_to_reads,
                                                          self.seq_run, self.eluate,
                                                          self.run_config)
        session.add(assembly_reads)
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 1
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        # check that they have been created correctly
        long_read = session.query(
            database_tables.ReadFile).filter_by(mads_isolatnummer = "F99123456-1").first()
        assert long_read.read_file == "path/to/long_reads"
        assert long_read.read_type == "Nanopore"
        asm_readfiles = session.query(database_tables.AssemblyReadFiles).first()
        assert asm_readfiles.long_readfile_id == long_read.readfile_id
        assert not asm_readfiles.forward_readfile_id
        assert not asm_readfiles.reverse_readfile_id

    def test_hybrid(self):
        # TODO: how do we supply information for hybrid runs?
        pass

    def test_fail_not_in_fofn(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["F99123456-1"], "runtype": ["ont"],
                                             "r1": [None], "r2": [None],
                                             "extra": ["path/to/long_reads"]})
        error_msg = "Isolate F99123456-2 was not found in read overview."
        with pytest.raises(KeyError, match=error_msg):
            update_db.get_assembly_readfiles("F99123456-2", path_to_reads,
                                             self.seq_run, self.eluate,
                                             self.run_config)

    def test_fail_no_r1(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["F99123456-1"], "runtype": ["paired-end"],
                                             "r1": [None], "r2": ["path/to/r2"],
                                             "extra": [None]})
        error_msg = "No forward reads given for isolate F99123456-1. "
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_assembly_readfiles("F99123456-1", path_to_reads,
                                             self.seq_run, self.eluate,
                                             self.run_config)

    def test_fail_no_r2(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["F99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["path/to/r1"], "r2": [None],
                                             "extra": [None]})
        error_msg = "No reverse reads given for isolate F99123456-1. "
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_assembly_readfiles("F99123456-1", path_to_reads,
                                             self.seq_run, self.eluate,
                                             self.run_config)

    def test_fail_no_long(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["F99123456-1"], "runtype": ["ont"],
                                             "r1": [None], "r2": [None],
                                             "extra": [None]})
        error_msg = "No long reads given for isolate F99123456-1. "
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_assembly_readfiles("F99123456-1", path_to_reads,
                                             self.seq_run, self.eluate,
                                             self.run_config)


class TestHandleSeqRequest(unittest.TestCase):
    sample_sheet = pd.DataFrame(data={"Prøvenr": ["F99123456-1"],
                                      "MADS_Prøvenummer": ["F99123456"],
                                      "Bestilt af": ["XYZ"],
                                      "Resistens": [False],
                                      "Identifikation": [True],
                                      "Relaps": [False],
                                      "Toxingen_ID": [False],
                                      "HAI": [False],
                                      "Miljøprøveisolat": [False],
                                      "Udbrudsisolat": [False],
                                      "Overvågningsisolat": [False],
                                      "Forskning": [False],
                                      "Andet": [False],
                                      "Registrering_udført_dato": [
                                          datetime.fromisoformat("2099-01-01 00:00:00")],
                                      "Registreret_af": ["ABC"],
                                      "Kommentar": [pd.NA],
                                      "Gram_Positiv": [True],
                                      "Gram_negativ": [False],
                                      "ID2": [pd.NA]})
    run_config = {"project_owner": "KMA"}

    def test_all_good(self):
        expected_result = database_tables.SeqRequest(mads_isolatnr = "F99123456-1",
                                                     mads_proevenr = "F99123456",
                                                     requested_by = "XYZ",
                                                     resistens = False,
                                                     identifikation = True,
                                                     relaps = False,
                                                     toxingen_identifikation = False,
                                                     hai = False,
                                                     miljoeproeve = False,
                                                     udbrud = False,
                                                     overvaagning = False,
                                                     forskning = False,
                                                     andet = False,
                                                     request_date = datetime.fromisoformat(
                                                         "2099-01-01 00:00:00"),
                                                     institution = "KMA",
                                                     gram_positiv = True,
                                                     gram_negativ = False,
                                                     request_comment = None,
                                                     secondary_id = None,
                                                     registered_by = "ABC")
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                    self.sample_sheet,
                                                                    active_config = self.run_config)
        assert expected_result.mads_isolatnr == test_request.mads_isolatnr
        assert expected_result.mads_proevenr == test_request.mads_proevenr
        assert expected_result.requested_by == test_request.requested_by
        assert expected_result.resistens == test_request.resistens
        assert expected_result.identifikation == test_request.identifikation
        assert expected_result.relaps == test_request.relaps
        assert expected_result.toxingen_identifikation == test_request.toxingen_identifikation
        assert expected_result.hai == test_request.hai
        assert expected_result.miljoeproeve == test_request.miljoeproeve
        assert expected_result.udbrud == test_request.udbrud
        assert expected_result.overvaagning == test_request.overvaagning
        assert expected_result.forskning == test_request.forskning
        assert expected_result.andet == test_request.andet
        assert expected_result.request_date == test_request.request_date
        assert expected_result.institution == test_request.institution
        assert expected_result.gram_positiv == test_request.gram_positiv
        assert expected_result.gram_negativ == test_request.gram_negativ
        assert expected_result.request_comment == test_request.request_comment
        assert expected_result.secondary_id == test_request.secondary_id
        assert expected_result.registered_by == test_request.registered_by

    def test_fix_lowercase_initials(self):
        sample_sheet = pd.DataFrame(data = {"Prøvenr": ["F99123456-1"],
                                            "MADS_Prøvenummer": ["F99123456"],
                                            "Bestilt af": ["xyz"],
                                            "Resistens": [False],
                                            "Identifikation": [True],
                                            "Relaps": [False],
                                            "Toxingen_ID": [False],
                                            "HAI": [False],
                                            "Miljøprøveisolat": [False],
                                            "Udbrudsisolat": [False],
                                            "Overvågningsisolat": [False],
                                            "Forskning": [False],
                                            "Andet": [False],
                                            "Registrering_udført_dato": [
                                                datetime.fromisoformat("2099-01-01 00:00:00")],
                                            "Registreret_af": ["aBC"],
                                            "Kommentar": [pd.NA],
                                            "Gram_Positiv": [True],
                                            "Gram_negativ": [False],
                                            "ID2": [pd.NA]})
        expected_result = database_tables.SeqRequest(mads_isolatnr = "F99123456-1",
                                                     mads_proevenr = "F99123456",
                                                     requested_by = "XYZ",
                                                     resistens = False,
                                                     identifikation = True,
                                                     relaps = False,
                                                     toxingen_identifikation = False,
                                                     hai = False,
                                                     miljoeproeve = False,
                                                     udbrud = False,
                                                     overvaagning = False,
                                                     forskning = False,
                                                     andet = False,
                                                     request_date = datetime.fromisoformat(
                                                         "2099-01-01 00:00:00"),
                                                     institution = "KMA",
                                                     gram_positiv = True,
                                                     gram_negativ = False,
                                                     request_comment = None,
                                                     secondary_id = None,
                                                     registered_by = "ABC")
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                    sample_sheet,
                                                                    active_config = self.run_config)
        assert expected_result.requested_by == test_request.requested_by
        assert expected_result.registered_by == test_request.registered_by

    def test_missing_id2(self):
        sample_sheet = pd.DataFrame(data = {"Prøvenr": ["F99123456-1"],
                                            "MADS_Prøvenummer": ["F99123456"],
                                            "Bestilt af": ["XYZ"],
                                            "Resistens": [False],
                                            "Identifikation": [True],
                                            "Relaps": [False],
                                            "Toxingen_ID": [False],
                                            "HAI": [False],
                                            "Miljøprøveisolat": [False],
                                            "Udbrudsisolat": [False],
                                            "Overvågningsisolat": [False],
                                            "Forskning": [False],
                                            "Andet": [False],
                                            "Registrering_udført_dato": [
                                                datetime.fromisoformat("2099-01-01 00:00:00")],
                                            "Registreret_af": ["ABC"],
                                            "Kommentar": [pd.NA],
                                            "Gram_Positiv": [True],
                                            "Gram_negativ": [False]})
        expected_result = database_tables.SeqRequest(mads_isolatnr = "F99123456-1",
                                                     mads_proevenr = "F99123456",
                                                     requested_by = "XYZ",
                                                     resistens = False,
                                                     identifikation = True,
                                                     relaps = False,
                                                     toxingen_identifikation = False,
                                                     hai = False,
                                                     miljoeproeve = False,
                                                     udbrud = False,
                                                     overvaagning = False,
                                                     forskning = False,
                                                     andet = False,
                                                     request_date = datetime.fromisoformat(
                                                         "2099-01-01 00:00:00"),
                                                     institution = "KMA",
                                                     gram_positiv = True,
                                                     gram_negativ = False,
                                                     request_comment = None,
                                                     secondary_id = None,
                                                     registered_by = "ABC")
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                    sample_sheet,
                                                                    active_config = self.run_config)
        assert expected_result.mads_isolatnr == test_request.mads_isolatnr
        assert expected_result.mads_proevenr == test_request.mads_proevenr
        assert expected_result.requested_by == test_request.requested_by
        assert expected_result.resistens == test_request.resistens
        assert expected_result.identifikation == test_request.identifikation
        assert expected_result.relaps == test_request.relaps
        assert expected_result.toxingen_identifikation == test_request.toxingen_identifikation
        assert expected_result.hai == test_request.hai
        assert expected_result.miljoeproeve == test_request.miljoeproeve
        assert expected_result.udbrud == test_request.udbrud
        assert expected_result.overvaagning == test_request.overvaagning
        assert expected_result.forskning == test_request.forskning
        assert expected_result.andet == test_request.andet
        assert expected_result.request_date == test_request.request_date
        assert expected_result.institution == test_request.institution
        assert expected_result.gram_positiv == test_request.gram_positiv
        assert expected_result.gram_negativ == test_request.gram_negativ
        assert expected_result.request_comment == test_request.request_comment
        assert expected_result.secondary_id == test_request.secondary_id
        assert expected_result.registered_by == test_request.registered_by

    def test_handle_nans(self):
        sample_sheet = pd.DataFrame(data = {"Prøvenr": ["F99123456-1"],
                                            "MADS_Prøvenummer": ["F99123456"],
                                            "Bestilt af": [pd.NA],
                                            "Resistens": [False],
                                            "Identifikation": [True],
                                            "Relaps": [False],
                                            "Toxingen_ID": [False],
                                            "HAI": [False],
                                            "Miljøprøveisolat": [False],
                                            "Udbrudsisolat": [False],
                                            "Overvågningsisolat": [False],
                                            "Forskning": [False],
                                            "Andet": [False],
                                            "Registrering_udført_dato": [
                                                datetime.fromisoformat("2099-01-01 00:00:00")],
                                            "Registreret_af": [np.nan],
                                            "Kommentar": [pd.NA],
                                            "Gram_Positiv": [True],
                                            "Gram_negativ": [False],
                                            "ID2": [pd.NA]})
        expected_result = database_tables.SeqRequest(mads_isolatnr = "F99123456-1",
                                                     mads_proevenr = "F99123456",
                                                     requested_by = None,
                                                     resistens = False,
                                                     identifikation = True,
                                                     relaps = False,
                                                     toxingen_identifikation = False,
                                                     hai = False,
                                                     miljoeproeve = False,
                                                     udbrud = False,
                                                     overvaagning = False,
                                                     forskning = False,
                                                     andet = False,
                                                     request_date = datetime.fromisoformat(
                                                         "2099-01-01 00:00:00"),
                                                     institution = "KMA",
                                                     gram_positiv = True,
                                                     gram_negativ = False,
                                                     request_comment = None,
                                                     secondary_id = None,
                                                     registered_by = None)
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                    sample_sheet,
                                                                    active_config = self.run_config)
        assert expected_result.mads_isolatnr == test_request.mads_isolatnr
        assert expected_result.mads_proevenr == test_request.mads_proevenr
        assert expected_result.requested_by == test_request.requested_by
        assert expected_result.resistens == test_request.resistens
        assert expected_result.identifikation == test_request.identifikation
        assert expected_result.relaps == test_request.relaps
        assert expected_result.toxingen_identifikation == test_request.toxingen_identifikation
        assert expected_result.hai == test_request.hai
        assert expected_result.miljoeproeve == test_request.miljoeproeve
        assert expected_result.udbrud == test_request.udbrud
        assert expected_result.overvaagning == test_request.overvaagning
        assert expected_result.forskning == test_request.forskning
        assert expected_result.andet == test_request.andet
        assert expected_result.request_date == test_request.request_date
        assert expected_result.institution == test_request.institution
        assert expected_result.gram_positiv == test_request.gram_positiv
        assert expected_result.gram_negativ == test_request.gram_negativ
        assert expected_result.request_comment == test_request.request_comment
        assert expected_result.secondary_id == test_request.secondary_id
        assert expected_result.registered_by == test_request.registered_by

    def test_enforce_date(self):
        sheet_with_stringdate = self.sample_sheet.copy()
        sheet_with_stringdate["Registrering_udført_dato"] = "2099-01-01"
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                    sheet_with_stringdate,
                                                                    active_config = self.run_config)
        assert test_request.request_date == datetime.fromisoformat("2099-01-01 00:00:00")

        sheet_with_date = self.sample_sheet.copy()
        sheet_with_date["Registrering_udført_dato"] = date.fromisoformat("2099-01-01")
        test_isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456")
        test_date_request = update_db.get_sequencing_request_for_isolate(test_isolate,
                                                                         sheet_with_date,
                                                                         active_config = self.run_config)
        assert test_date_request.request_date == datetime.fromisoformat("2099-01-01 00:00:00")


    def test_fail_not_in_sheet(self):
        error_msg = "Isolate F99123456-7 not found in runsheet"
        test_isolate = Isolate(mads_isolatnr = "F99123456-7", mads_proevenr = "F99123456")
        with pytest.raises(KeyError, match=error_msg):
            update_db.get_sequencing_request_for_isolate(test_isolate,
                                                         self.sample_sheet,
                                                         active_config = self.run_config)


class TestHandleSampleAndIsolate(unittest.TestCase):
    config_to_use = {"project_owner": "KMA",
                     "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                     "10": "D",
                                                                     "11": "F",
                                                                     "50": "T"},
                                                "sample_number_format":
                                                    '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                   r'(?P<sample_year>\d{2})'
                                                                   r'(?P<sample_number>\d{6})'
                                                                   r'(?P<bact_number>-\d{1})',
                                                "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                 r'(?P<sample_year>\d{2})'
                                                                 r'(?P<sample_number>\d{6})'
                                                                 r'(?P<bact_number>-\d{1})',
                                                "format_output": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                   r'(?P<sample_year>\d{2})'
                                                                   r'(?P<sample_number>\d{6})'
                                                                   r'(?P<bact_number>-\d{1})',
                                                "sample_numbers_in": "letter",
                                                "sample_numbers_out": "number",
                                                "sample_numbers_report": "letter"}}
    success_results = pd.DataFrame(data = {"proevenr": ["F99123456-1"], "godkendt": [1],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["Identifikation"],
                                           "final_id": [pd.NA],
                                           "Avg. coverage depth_basics": [100],
                                           "Grampos/neg": ["Positiv"],
                                           "MADS species": ["Placeholderia"],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Placeholderia fakeorum"],
                                           "Serotypning": ["FakeSerotype"],
                                           "platform": ["MiSeq"]})

    def test_get_sample_success(self):
        sample_number = "F99123456-1"
        expected_sample = database_tables.Sample(mads_proevenr = "F99123456", institution = "KMA",
                                                 lab_section = "F")
        expected_isolate = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                                   mads_proevenr = "F99123456",
                                                   preliminary_organism_name = "Placeholderia",
                                                   final_organism_name = "Placeholderia fakeorum",
                                                   wgs_serotyping = "FakeSerotype",
                                                   institution = "KMA")

        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     self.success_results,
                                                                     self.config_to_use)
        # check sample data
        assert test_sample.mads_proevenr == expected_sample.mads_proevenr
        assert test_sample.institution == expected_sample.institution
        assert test_sample.lab_section == expected_sample.lab_section
        # check isolate data
        assert test_isolate.mads_isolatnr == expected_isolate.mads_isolatnr
        assert test_isolate.mads_proevenr == expected_isolate.mads_proevenr
        assert test_isolate.preliminary_organism_name == expected_isolate.preliminary_organism_name
        assert test_isolate.final_organism_name == expected_isolate.final_organism_name
        assert test_isolate.wgs_serotyping == expected_isolate.wgs_serotyping
        assert test_isolate.institution == expected_isolate.institution

    def test_use_final_isolate(self):
        sample_number = "F99123456-1"
        final_isolate_result = self.success_results.copy()
        final_isolate_result['final_id'] = "Placeholderia bielefeldensis"
        expected_sample = database_tables.Sample(mads_proevenr = "F99123456", institution = "KMA",
                                                 lab_section = "F")
        expected_isolate = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                                   mads_proevenr = "F99123456",
                                                   preliminary_organism_name = "Placeholderia",
                                                   final_organism_name = "Placeholderia "
                                                                         "bielefeldensis",
                                                   wgs_serotyping = "FakeSerotype",
                                                   institution = "KMA")

        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     final_isolate_result,
                                                                     self.config_to_use)
        # check sample data
        assert test_sample.mads_proevenr == expected_sample.mads_proevenr
        assert test_sample.institution == expected_sample.institution
        assert test_sample.lab_section == expected_sample.lab_section
        # check isolate data
        assert test_isolate.mads_isolatnr == expected_isolate.mads_isolatnr
        assert test_isolate.mads_proevenr == expected_isolate.mads_proevenr
        assert test_isolate.preliminary_organism_name == expected_isolate.preliminary_organism_name
        assert test_isolate.final_organism_name == expected_isolate.final_organism_name
        assert test_isolate.wgs_serotyping == expected_isolate.wgs_serotyping
        assert test_isolate.institution == expected_isolate.institution

    def test_kraken_fallback(self):
        sample_number = "F99123456-1"
        kraken_result = pd.DataFrame(data = {"proevenr": ["F99123456-1"], "godkendt": [1],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["Identifikation"],
                                           "final_id": [pd.NA],
                                           "Avg. coverage depth_basics": [100],
                                           "Grampos/neg": ["Positiv"],
                                           "MADS species": ["Placeholderia"],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_kraken":
                                               ["Placeholderia fakeorum"],
                                           "Serotypning": ["FakeSerotype"],
                                           "platform": ["MiSeq"]})
        expected_sample = database_tables.Sample(mads_proevenr = "F99123456", institution = "KMA",
                                                 lab_section = "F")
        expected_isolate = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                                   mads_proevenr = "F99123456",
                                                   preliminary_organism_name = "Placeholderia",
                                                   final_organism_name = "Placeholderia "
                                                                         "fakeorum",
                                                   wgs_serotyping = "FakeSerotype",
                                                   institution = "KMA")

        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     kraken_result,
                                                                     self.config_to_use)
        # check sample data
        assert test_sample.mads_proevenr == expected_sample.mads_proevenr
        assert test_sample.institution == expected_sample.institution
        assert test_sample.lab_section == expected_sample.lab_section
        # check isolate data
        assert test_isolate.mads_isolatnr == expected_isolate.mads_isolatnr
        assert test_isolate.mads_proevenr == expected_isolate.mads_proevenr
        assert test_isolate.preliminary_organism_name == expected_isolate.preliminary_organism_name
        assert test_isolate.final_organism_name == expected_isolate.final_organism_name
        assert test_isolate.wgs_serotyping == expected_isolate.wgs_serotyping
        assert test_isolate.institution == expected_isolate.institution

    def test_no_serotyping(self):
        sample_number = "F99123456-1"
        kraken_result = pd.DataFrame(data = {"proevenr": ["F99123456-1"], "godkendt": [1],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "final_id": [pd.NA],
                                             "Avg. coverage depth_basics": [100],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_kraken":
                                                 ["Placeholderia fakeorum"],
                                             "platform": ["MiSeq"]})

        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     kraken_result,
                                                                     self.config_to_use)
        assert test_isolate.wgs_serotyping is None

    def test_handle_kraken_nan(self):
        sample_number = "F99123456-1"
        kraken_result = pd.DataFrame(data = {"proevenr": ["F99123456-1"], "godkendt": [1],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["Identifikation"],
                                           "final_id": [pd.NA],
                                           "Avg. coverage depth_basics": [100],
                                           "Grampos/neg": ["Positiv"],
                                           "MADS species": ["Placeholderia"],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_kraken":
                                               [np.nan],
                                           "Serotypning": [pd.NA],
                                           "platform": ["MiSeq"]})
        expected_sample = database_tables.Sample(mads_proevenr = "F99123456", institution = "KMA",
                                                 lab_section = "F")
        expected_isolate = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                                   mads_proevenr = "F99123456",
                                                   preliminary_organism_name = "Placeholderia",
                                                   final_organism_name = None,
                                                   wgs_serotyping = None,
                                                   institution = "KMA")

        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     kraken_result,
                                                                     self.config_to_use)
        # check sample data
        assert test_sample.mads_proevenr == expected_sample.mads_proevenr
        assert test_sample.institution == expected_sample.institution
        assert test_sample.lab_section == expected_sample.lab_section
        # check isolate data
        assert test_isolate.mads_isolatnr == expected_isolate.mads_isolatnr
        assert test_isolate.mads_proevenr == expected_isolate.mads_proevenr
        assert test_isolate.preliminary_organism_name == expected_isolate.preliminary_organism_name
        assert test_isolate.final_organism_name == expected_isolate.final_organism_name
        assert test_isolate.wgs_serotyping == expected_isolate.wgs_serotyping
        assert test_isolate.institution == expected_isolate.institution

    def test_isolate_not_approved(self):
        sample_number = "F99123456-1"
        expected_isolate = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                                   mads_proevenr = "F99123456",
                                                   preliminary_organism_name = "Placeholderia",
                                                   final_organism_name = None,
                                                   wgs_serotyping = None,
                                                   institution = "KMA")
        fail_results = pd.DataFrame(data = {"proevenr": ["F99123456-1"], "godkendt": [0],
                                            "Bestilt af": ["XYZ"],
                                            "Indikation": ["Identifikation"],
                                            "Avg. coverage depth_basics": [20],
                                            "Grampos/neg": ["Positiv"],
                                            "MADS species": ["Placeholderia"],
                                            "MADS: kendt længde i bp": [pd.NA],
                                            "WGS_species_GTDB_basics":
                                                ["Placeholderia fakeorum"],
                                            "Serotypning": ["FakeSerotype"],
                                            "platform": ["MiSeq"]})
        test_sample, test_isolate = update_db.get_sample_and_isolate(sample_number,
                                                                     fail_results,
                                                                     self.config_to_use)
        # check isolate data
        assert test_isolate.mads_isolatnr == expected_isolate.mads_isolatnr
        assert test_isolate.mads_proevenr == expected_isolate.mads_proevenr
        assert test_isolate.preliminary_organism_name == expected_isolate.preliminary_organism_name
        assert test_isolate.final_organism_name == expected_isolate.final_organism_name
        assert test_isolate.wgs_serotyping == expected_isolate.wgs_serotyping
        assert test_isolate.institution == expected_isolate.institution

    # TODO: handle sample with multiple isolates!

    def test_fail_no_match(self):
        sample_number = "F2099123456-1"
        success_results = pd.DataFrame(data = {"proevenr": ["F2099123456-1"], "godkendt": [1],
                                               "Bestilt af": ["XYZ"],
                                               "Indikation": ["Identifikation"],
                                               "Avg. coverage depth_basics": [100],
                                               "Grampos/neg": ["Positiv"],
                                               "MADS species": ["Placeholderia"],
                                               "MADS: kendt længde i bp": [pd.NA],
                                               "WGS_species_GTDB_basics":
                                                   ["Placeholderia fakeorum"],
                                               "Serotypning": ["FakeSerotype"],
                                               "platform": ["MiSeq"]})
        err_msg = "Isolate number F2099123456-1 does not match " \
                  "the isolate number format in the config."
        with pytest.raises(ValueError, match = err_msg):
            update_db.get_sample_and_isolate(sample_number,
                                             success_results,
                                             self.config_to_use)
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_warn_not_in_translation(self):
        config_to_use = {"project_owner": "KMA",
                         "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                         "10": "D",
                                                                         "50": "T"},
                                                    "sample_number_format":
                                                        '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                    "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                       r'(?P<sample_year>\d{2})'
                                                                       r'(?P<sample_number>\d{6})'
                                                                       r'(?P<bact_number>-\d{1})',
                                                    "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "sample_numbers_in": "letter",
                                                    "sample_numbers_out": "number"}}
        sample_number = "F99123456-1"
        log_msg = 'Lab section could not be inferred from sample type F. ' \
                  'Lab section will have to be entered manually.'
        with self._caplog.at_level(level="WARNING", logger="update_database"):
            update_db.get_sample_and_isolate(sample_number,
                                             self.success_results,
                                             config_to_use)
            assert ("update_database", logging.WARNING, log_msg) in self._caplog.record_tuples


class TestReadResultSheet(unittest.TestCase):
    def test_good_result(self):
        test_sheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
               / 'result_sheet.xlsx'
        expected_outcome = pd.DataFrame(data={"godkendt": [True], "køres om": [pd.NA],
                                              "noter": [pd.NA],
                                              "pipeline_noter": [pd.NA],
                                              "final_id": [pd.NA],
                                              "proevenr": ["1199123456-1"],
                                              "Bestilt af": ["XYZ"],
                                              "Indikation": ["Identifikation"],
                                              "N50_basics": [12000],
                                              "Avg. coverage depth_basics": [40],
                                              "Grampos/neg": ["Positiv"],
                                              "MADS species": ["Placeholderia"],
                                              "MADS: kendt længde i bp": [pd.NA],
                                              "WGS_species_GTDB_basics":
                                                  ["Placeholderia fakeorum"],
                                              "Serotypning": ["FakeSerotype"],
                                              "ANI_til_ref_basics": [97],
                                              "GTDB: kendt længde i bp": [pd.NA],
                                              "Total length_basics": [100000],
                                              "proevenr_quast": ["1199123456-1"],
                                              "# contigs": [50],
                                              "GC (%)": [50.0],
                                              "NG50": [11000],
                                              "# predicted genes (unique)": [500],
                                              "proevenr_kraken": ["1199123456-1"],
                                              "WGS_genus_kraken": ["Placeholderia"],
                                              "WGS_genus_kraken_%": [90],
                                              "WGS_species_kraken": ["Placeholderia fakeorum"],
                                              "WGS_species_kraken_%": [70],
                                              "proevenr_gtdb": ["1199123456-1"],
                                              "WGS_genus_GTDB": ["Placeholderia"],
                                              "WGS_species_GTDB": ["Placeholderia fakeorum"],
                                              "ANI_til_ref": [97],
                                              "proevenr_checkm": ["1199123456-1"],
                                              "Marker lineage": ["Bacteria"],
                                              "# genomes": [100],
                                              "# markers": [50],
                                              "Completeness": [100],
                                              "Contamination": [0.3],
                                              "proevenr_troubleshooting": ["1199123456-1"],
                                              "DNA_Konc": [10.1],
                                              "Oprensnings_kørselsnr": ["0001"],
                                              "Brønd_nr": ["A1"],
                                              "ILM_Brønd": ["A1"],
                                              "SEK_Run_nr": ["0001"],
                                              "pipeline_version": ["1.2.3"],
                                              "platform": ['MiSeq'],
                                              "lab_protokol": ['1.2.3']
                                              })
        expected_outcome = expected_outcome.astype({"køres om": "object", "noter": "object",
                                                    "pipeline_noter": "object",
                                                    "final_id": "object",
                                                    "MADS: kendt længde i bp": "Float64",
                                                    "GTDB: kendt længde i bp": "Float64"})
        test_data = update_db.read_result_sheet(test_sheet)
        print(test_data.columns)
        print(expected_outcome.columns)
        pd.testing.assert_frame_equal(test_data, expected_outcome)


class TestGetAssembly(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """Set up database connection."""
        # TODO: how can we test mysql specific things
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        global session
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)

    @classmethod
    def tearDownClass(cls) -> None:
        """Disconnect from database."""
        session.close()

    def setUp(self) -> None:
        """Truncate tables."""
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()

    run_config = {"project_owner": "KMA"}
    qc_sheet = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                    "Bestilt af": ["XYZ"],
                                    "Indikation": ["Identifikation"],
                                    "Avg. coverage depth_basics": [100],
                                    "final_id": [pd.NA],
                                    "N50_basics": [12000],
                                    "# contigs": [20],
                                    "Total length_basics": [2000000],
                                    "Grampos/neg": ["Positiv"],
                                    "MADS species": ["Placeholderia"],
                                    "MADS: kendt længde i bp": [pd.NA],
                                    "WGS_species_GTDB_basics":
                                        ["Placeholderia fakeorum"],
                                    "Serotypning": ["FakeSerotype"],
                                    "platform": ["MiSeq"],
                                    "Oprensnings_kørselsnr": ["0001"],
                                    "pipeline_version": ["1.2.3"]})

    seq_run = {"seq_run_name": "ILM_Run0000_Y20230508_XYZ",
               "platform": "MiSeq",
               "sequencing_type": "illumina",
               "flowcell_id": None,
               "runsheet_path": str(pathlib.Path(__file__).parent
                                    / 'data'
                                    / 'utilities_test'
                                    / 'runsheet_experiment_name.xlsx'),
               "data_path": str(pathlib.Path(__file__).parent
                                / "data"
                                / "update_db"
                                / "rawdata_correct"),
               "run_user_id": "XYZ",
               "run_date": date.fromisoformat("2022-01-01"),
               "institution": "KMA"}
    eluate = {"mads_isolatnr" : "B99123456-1", "extract_run_id" : 1}
    seq_request = {"mads_isolatnr": "B99123456-1",
                   "mads_proevenr": "B99123456",
                   "requested_by": "XYZ",
                   "resistens": False,
                   "identifikation": True,
                   "relaps": False,
                   "toxingen_identifikation": False,
                   "hai": False,
                   "miljoeproeve": False,
                   "udbrud": False,
                   "overvaagning": False,
                   "forskning": False,
                   "andet": False,
                   "request_date": date.fromisoformat("2099-01-01"),
                   "institution": "KMA",
                   "gram_positiv": True,
                   "gram_negativ": False,
                   "request_comment": None,
                   "secondary_id": None,
                   "registered_by": "ABC"}

    def test_add_ilm_success(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, self.qc_sheet, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "illumina"
        assert assembly_from_db.assembler == "shovill/skesa"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "success_dir"
                                                     / "B99123456-1"
                                                     / "assembly"
                                                     / "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna")
        assert assembly_from_db.pipeline_organism_name == "Placeholderia fakeorum"

    def test_add_old_ilm_success(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir_old_names"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, self.qc_sheet, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "illumina"
        assert assembly_from_db.assembler == "shovill/skesa"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "success_dir_old_names"
                                                     / "B99123456-1"
                                                     / "assembly"
                                                     / "B99123456-1.fna")
        assert assembly_from_db.pipeline_organism_name == "Placeholderia fakeorum"

    def test_handle_nans(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        qc_sheet = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                        "Bestilt af": ["XYZ"],
                                        "Indikation": ["Identifikation"],
                                        "Avg. coverage depth_basics": [100],
                                        "final_id": [pd.NA],
                                        "N50_basics": [12000],
                                        "# contigs": [20],
                                        "Total length_basics": [2000000],
                                        "Grampos/neg": ["Positiv"],
                                        "MADS species": ["Placeholderia"],
                                        "MADS: kendt længde i bp": [pd.NA],
                                        "WGS_species_GTDB_basics":
                                            [pd.NA],
                                        "Serotypning": [np.nan],
                                        "platform": ["MiSeq"],
                                        "Oprensnings_kørselsnr": ["0001"],
                                        "pipeline_version": ["1.2.3"]})
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, qc_sheet, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "illumina"
        assert assembly_from_db.assembler == "shovill/skesa"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "success_dir"
                                                     / "B99123456-1"
                                                     / "assembly"
                                                     / "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna")
        assert assembly_from_db.pipeline_organism_name is None

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_handle_missing_version(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        qc_sheet = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                    "Bestilt af": ["XYZ"],
                                    "Indikation": ["Identifikation"],
                                    "Avg. coverage depth_basics": [100],
                                    "final_id": [pd.NA],
                                    "N50_basics": [12000],
                                    "# contigs": [20],
                                    "Total length_basics": [2000000],
                                    "Grampos/neg": ["Positiv"],
                                    "MADS species": ["Placeholderia"],
                                    "MADS: kendt længde i bp": [pd.NA],
                                    "WGS_species_GTDB_basics":
                                        ["Placeholderia fakeorum"],
                                    "Serotypning": ["FakeSerotype"],
                                    "platform": ["MiSeq"],
                                    "Oprensnings_kørselsnr": ["0001"]})
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        log_msg = "No pipeline version found. " \
                   "Pipeline version will have to be entered manually."
        with self._caplog.at_level(level="WARNING", logger="update_database"):
            assembly = update_db.get_assembly_for_isolate(isolate, qc_sheet, result_dir, readfiles,
                                                          database_tables.SeqRequest(**self.seq_request),
                                                          active_config = self.run_config)
            assert ("update_database", logging.WARNING, log_msg) in self._caplog.record_tuples
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1

        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.pipeline_version is None

    def test_add_ilm_use_final(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        qc_with_final = self.qc_sheet.copy()
        qc_with_final['final_id'] = "Placeholderia bielefeldensis"
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, qc_with_final, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "illumina"
        assert assembly_from_db.assembler == "shovill/skesa"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "success_dir"
                                                     / "B99123456-1"
                                                     / "assembly"
                                                     / "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna")
        assert assembly_from_db.pipeline_organism_name == "Placeholderia bielefeldensis"

    def test_add_ilm_use_kraken(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["paired-end"],
                                             "r1": [
                                                 "data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": [
                                                 "data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": [None]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1",
                                          mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        qc_with_kraken = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                             "Bestilt af": ["XYZ"],
                             "Indikation": ["Identifikation"],
                             "Avg. coverage depth_basics": [100],
                             "final_id": [pd.NA],
                             "N50_basics": [12000],
                             "# contigs": [20],
                             "Total length_basics": [2000000],
                             "Grampos/neg": ["Positiv"],
                             "MADS species": ["Placeholderia"],
                             "MADS: kendt længde i bp": [pd.NA],
                             "WGS_species_kraken":
                                 ["Placeholderia fakeorum"],
                             "Serotypning": ["FakeSerotype"],
                             "platform": ["MiSeq"],
                             "Oprensnings_kørselsnr": ["0001"],
                             "pipeline_version": ["1.2.3"]})
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, qc_with_kraken, result_dir, readfiles,
                                                      database_tables.SeqRequest(
                                                          **self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 2
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "illumina"
        assert assembly_from_db.assembler == "shovill/skesa"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "success_dir"
                                                     / "B99123456-1"
                                                     / "assembly"
                                                     / "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna")
        assert assembly_from_db.pipeline_organism_name == "Placeholderia fakeorum"

    def test_add_nanopore_success(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["ont"],
                                             "r1": [None],
                                             "r2": [None],
                                             "extra": ["data/update_db/nanopore_data/barcode01"]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "nanopore_success"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, self.qc_sheet, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 1
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "nanopore"
        assert assembly_from_db.assembler == "flye+medaka"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "nanopore_success"
                                                     / "B99123456-1"
                                                     / "nanopore_assembly"
                                                     / "Run0001_B99123456-1.fasta")

    def test_add_hybrid_success(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["hybrid"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": ["data/update_db/nanopore_data/barcode01"]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "hybrid_success"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        assembly_found = session.query(database_tables.Assembly).all()
        assert not assembly_found
        assembly = update_db.get_assembly_for_isolate(isolate, self.qc_sheet, result_dir, readfiles,
                                                      database_tables.SeqRequest(**self.seq_request),
                                                      active_config = self.run_config)
        session.add(assembly)
        assembly_found = session.query(database_tables.Assembly).all()
        assert len(assembly_found) == 1
        readfiles_found = session.query(database_tables.ReadFile).all()
        assert len(readfiles_found) == 3
        assembly_readfiles_found = session.query(database_tables.AssemblyReadFiles).all()
        assert len(assembly_readfiles_found) == 1
        assembly_from_db = assembly_found[0]  # type: database_tables.Assembly
        assert assembly_from_db.assembly_id
        assert assembly_from_db.assembly_type == "hybrid"
        assert assembly_from_db.assembler == "unicycler"
        assert assembly_from_db.assembly_file == str(pathlib.Path(__file__).parent
                                                     / "data"
                                                     / "update_db"
                                                     / "hybrid_success"
                                                     / "B99123456-1"
                                                     / "hybrid_assembly"
                                                     / "B99123456-1.fasta")

    def test_fail_not_in_qc(self):
        path_to_reads = pd.DataFrame(data = {"sample": ["B99123456-1"], "runtype": ["hybrid"],
                                             "r1": ["data/update_db/rawdata_correct/B99123456-1_L001_R1_001.fastq.gz"],
                                             "r2": ["data/update_db/rawdata_correct/B99123456-1_L001_R2_001.fastq.gz"],
                                             "extra": ["data/update_db/nanopore_data/barcode01"]})
        readfiles = update_db.get_assembly_readfiles("B99123456-1", path_to_reads,
                                                     database_tables.SequencingRun(**self.seq_run),
                                                     database_tables.Eluate(**self.eluate),
                                                     active_config = self.run_config)
        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "hybrid_success"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        result_sheet = pd.DataFrame(data = {"proevenr": ["B99123456-2"], "godkendt": [1],
                                            "Bestilt af": ["XYZ"],
                                            "Indikation": ["Identifikation"],
                                            "Avg. coverage depth_basics": [100],
                                            "N50_basics": [12000],
                                            "# contigs": [20],
                                            "Total length_basics": [2000000],
                                            "Grampos/neg": ["Positiv"],
                                            "MADS species": ["Placeholderia"],
                                            "MADS: kendt længde i bp": [pd.NA],
                                            "WGS_species_GTDB_basics":
                                                ["Placeholderia fakeorum"],
                                            "Serotypning": ["FakeSerotype"],
                                            "platform": ["MiSeq"],
                                            "Oprensnings_kørselsnr": ["0001"],
                                            "pipeline_version": ["1.2.3"]})
        error_msg = "Isolate B99123456-1 not found in QC result sheet."
        with pytest.raises(KeyError, match = error_msg):
            update_db.get_assembly_for_isolate(isolate, result_sheet, result_dir, readfiles,
                                               database_tables.SeqRequest(**self.seq_request),
                                               active_config = self.run_config)

    def test_fail_no_reads(self):
        readfiles = database_tables.AssemblyReadFiles()

        result_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        isolate = database_tables.Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                          preliminary_organism_name = "Placeholderia",
                                          institution = "KMA")
        error_msg = "No reads recorded for isolate B99123456-1."
        with pytest.raises(ValueError, match = error_msg):
            update_db.get_assembly_for_isolate(isolate, self.qc_sheet, result_dir, readfiles,
                                               database_tables.SeqRequest(**self.seq_request),
                                               active_config = self.run_config)


class TestCheckForNewerAssemblies(unittest.TestCase):
    runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
               / 'runsheet_experiment_name.xlsx'
    @classmethod
    def setUpClass(cls) -> None:
        """Set up database connection."""
        # TODO: how can we test mysql specific things
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        global session
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)

    @classmethod
    def tearDownClass(cls) -> None:
        """Disconnect from database."""
        session.close()

    def setUp(self) -> None:
        """Truncate tables."""
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_find_newer_short_reads(self):
        """Ensure that newer short read data are found"""
        original_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        original_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2023-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        original_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [original_request])
        original_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = original_extract_run.extract_run_id,
                                 extract_run = original_extract_run, isolate = original_isolate)
        original_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        original_forward = ReadFile(readfile_id = 1, read_type = "Illumina",
                                    read_direction = "forward", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R1_001.fastq.gz",
                                    isolate = original_isolate,
                                    eluate = original_eluate, institution = "KMA",
                                    assembly_readfiles = [original_asm_readfiles],
                                    eluat_id = original_eluate.eluat_id)
        original_reverse = ReadFile(readfile_id = 2, read_type = "Illumina",
                                    read_direction = "reverse", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R2_001.fastq.gz",
                                    isolate = original_isolate,
                                    eluate = original_eluate, institution = "KMA",
                                    assembly_readfiles = [original_asm_readfiles],
                                    eluat_id = original_eluate.eluat_id)
        original_asm_readfiles.forward_read = original_forward
        original_asm_readfiles.reverse_read = original_reverse
        original_asm_readfiles.forward_readfile_id = original_forward.readfile_id
        original_asm_readfiles.reverse_readfile_id = original_reverse.readfile_id
        original_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [original_forward,
                                                                        original_reverse])
        original_forward.sequencing_run = original_run
        original_forward.batch_id = original_run.seq_run_id
        original_reverse.sequencing_run = original_run
        original_reverse.batch_id = original_run.seq_run_id
        original_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [original_isolate])
        original_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = original_asm_readfiles,
                                     seq_request = original_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = original_asm_readfiles.assembly_readfiles_id,
                                     request_id = original_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        session.add(original_assembly)
        session.add(original_sample)
        session.add(original_eluate)
        session.add(original_run)
        session.add(original_isolate)
        session.add(original_request)
        session.add(original_asm_readfiles)
        session.add(original_forward)
        session.add(original_reverse)
        session.add(original_extract_run)
        old_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia bielefeldensis",
                                   wgs_serotyping = "FakeSerotype")
        old_seq_run = SequencingRun(seq_run_name = "ONT_Run0000_Y20220508_XYZ",
                                     platform = "GridION", sequencing_type = "nanopore",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2022-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA")
        old_long_read = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                 sequencing_run = old_seq_run, isolate = old_isolate)
        old_asm_readfiles = AssemblyReadFiles(long_read = old_long_read, 
                                              long_readfile_id = old_long_read.readfile_id)
        old_seq_run.read_files = [old_long_read]
        log_msg = "There is a result for isolate B99123456-1" \
                  " from a newer sequencing run in the database. Final ID will not be updated."
        with self._caplog.at_level(level="INFO", logger="update_database"):
            test_find_newer = update_db.find_newer_assemblies(old_isolate, old_seq_run, session)
        assert ("update_database", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_find_newer

    def test_find_newer_long_reads(self):
        """Ensure that newer long read data are found"""
        original_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        original_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2023-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        original_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [original_request])
        original_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = original_extract_run.extract_run_id,
                                 extract_run = original_extract_run, isolate = original_isolate)
        original_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        original_long = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                    isolate = original_isolate,
                                    eluate = original_eluate, institution = "KMA",
                                    assembly_readfiles = [original_asm_readfiles],
                                    eluat_id = original_eluate.eluat_id)
        original_asm_readfiles.long_read = original_long
        original_asm_readfiles.long_readfile_id = original_long.readfile_id
        original_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [original_long])
        original_long.sequencing_run = original_run
        original_long.batch_id = original_run.seq_run_id
        original_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [original_isolate])
        original_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = original_asm_readfiles,
                                     seq_request = original_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = original_asm_readfiles.assembly_readfiles_id,
                                     request_id = original_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        session.add(original_assembly)
        session.add(original_sample)
        session.add(original_eluate)
        session.add(original_run)
        session.add(original_isolate)
        session.add(original_request)
        session.add(original_asm_readfiles)
        session.add(original_long)
        session.add(original_extract_run)
        old_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia bielefeldensis",
                                   wgs_serotyping = "FakeSerotype")
        old_seq_run = SequencingRun(seq_run_name = "ONT_Run0000_Y20220508_XYZ",
                                    platform = "GridION", sequencing_type = "nanopore",
                                    run_user_id = "XYZ",
                                    run_date = date.fromisoformat("2022-05-08"),
                                    runsheet_path = str(self.runsheet),
                                    data_path = "test/data/update_db/rawdata_correct",
                                    institution = "KMA")
        old_long_read = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                 sequencing_run = old_seq_run)
        old_asm_readfiles = AssemblyReadFiles(long_read = old_long_read,
                                              long_readfile_id = old_long_read.readfile_id)
        old_seq_run.read_files = [old_long_read]
        log_msg = "There is a result for isolate B99123456-1" \
                  " from a newer sequencing run in the database. Final ID will not be updated."
        with self._caplog.at_level(level="INFO", logger="update_database"):
            test_find_newer = update_db.find_newer_assemblies(old_isolate, old_seq_run, session)
        assert ("update_database", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_find_newer

    def test_find_newer_hybrid_reads(self):
        """Ensure that newer hybrid read data are found"""
        original_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        original_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2023-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        original_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [original_request])
        original_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = original_extract_run.extract_run_id,
                                 extract_run = original_extract_run, isolate = original_isolate)
        original_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        original_forward = ReadFile(readfile_id = 1, read_type = "Illumina",
                                    read_direction = "forward", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R1_001.fastq.gz",
                                    isolate = original_isolate,
                                    eluate = original_eluate, institution = "KMA",
                                    assembly_readfiles = [original_asm_readfiles],
                                    eluat_id = original_eluate.eluat_id)
        original_reverse = ReadFile(readfile_id = 2, read_type = "Illumina",
                                    read_direction = "reverse", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R2_001.fastq.gz",
                                    isolate = original_isolate,
                                    eluate = original_eluate, institution = "KMA",
                                    assembly_readfiles = [original_asm_readfiles],
                                    eluat_id = original_eluate.eluat_id)
        original_long = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                 isolate = original_isolate,
                                 eluate = original_eluate, institution = "KMA",
                                 assembly_readfiles = [original_asm_readfiles],
                                 eluat_id = original_eluate.eluat_id)
        original_asm_readfiles.long_read = original_long
        original_asm_readfiles.long_readfile_id = original_long.readfile_id
        original_asm_readfiles.forward_read = original_forward
        original_asm_readfiles.reverse_read = original_reverse
        original_asm_readfiles.forward_readfile_id = original_forward.readfile_id
        original_asm_readfiles.reverse_readfile_id = original_reverse.readfile_id
        original_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [original_forward,
                                                                        original_reverse])
        original_ont = SequencingRun(seq_run_id = 2, seq_run_name = "ONT_Run0000_Y20230508_XYZ",
                                     platform = "GridION", sequencing_type = "nanopore",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2022-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [original_long])
        original_forward.sequencing_run = original_run
        original_forward.batch_id = original_run.seq_run_id
        original_reverse.sequencing_run = original_run
        original_reverse.batch_id = original_run.seq_run_id
        original_long.sequencing_run = original_ont
        original_long.batch_id = original_ont.seq_run_id
        original_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [original_isolate])
        original_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = original_asm_readfiles,
                                     seq_request = original_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = original_asm_readfiles.assembly_readfiles_id,
                                     request_id = original_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        session.add(original_assembly)
        session.add(original_sample)
        session.add(original_eluate)
        session.add(original_run)
        session.add(original_isolate)
        session.add(original_request)
        session.add(original_asm_readfiles)
        session.add(original_forward)
        session.add(original_reverse)
        session.add(original_extract_run)
        old_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia bielefeldensis",
                                   wgs_serotyping = "FakeSerotype")
        old_seq_run = SequencingRun(seq_run_name = "ONT_Run00001_Y20220509_XYZ",
                                    platform = "GridION", sequencing_type = "nanopore",
                                    run_user_id = "XYZ",
                                    run_date = date.fromisoformat("2022-05-09"),
                                    runsheet_path = str(self.runsheet),
                                    data_path = "test/data/update_db/rawdata_correct",
                                    institution = "KMA")
        old_long_read = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                 sequencing_run = old_seq_run)
        old_asm_readfiles = AssemblyReadFiles(long_read = old_long_read,
                                              long_readfile_id = old_long_read.readfile_id)
        old_seq_run.read_files = [old_long_read]
        log_msg = "There is a result for isolate B99123456-1" \
                  " from a newer sequencing run in the database. Final ID will not be updated."
        with self._caplog.at_level(level="INFO", logger="update_database"):
            test_find_newer = update_db.find_newer_assemblies(old_isolate, old_seq_run, session)
        assert ("update_database", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_find_newer

    def test_no_readfiles(self):
        """Handle assemblies without read files."""
        original_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        original_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2023-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        original_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [original_request])
        original_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = original_extract_run.extract_run_id,
                                 extract_run = original_extract_run, isolate = original_isolate)
        original_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA")
        original_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [original_isolate])
        original_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)

        original_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     seq_request = original_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     request_id = original_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23"),
                                     assembly_readfiles = original_asm_readfiles
                                     )
        session.add(original_assembly)
        session.add(original_asm_readfiles)
        session.add(original_sample)
        session.add(original_eluate)
        session.add(original_run)
        session.add(original_isolate)
        session.add(original_request)
        session.add(original_extract_run)
        new_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                              institution = "KMA", preliminary_organism_name = "Placeholderia",
                              final_organism_name = "Placeholderia bielefeldensis",
                              wgs_serotyping = "FakeSerotype")
        new_seq_run = SequencingRun(seq_run_name = "ONT_Run0000_Y20220508_XYZ",
                                    platform = "GridION", sequencing_type = "nanopore",
                                    run_user_id = "XYZ",
                                    run_date = date.fromisoformat("2022-05-08"),
                                    runsheet_path = str(self.runsheet),
                                    data_path = "test/data/update_db/rawdata_correct",
                                    institution = "KMA")
        new_long_read = ReadFile(mads_isolatnummer = "B99123456-1", read_type = "nanopore",
                                 sequencing_run = new_seq_run)
        new_asm_readfiles = AssemblyReadFiles(long_read = new_long_read,
                                              long_readfile_id = new_long_read.readfile_id)
        new_seq_run.read_files = [new_long_read]

        log_msg = f"No run date found for assembly" \
                  f" {original_assembly.assembly_file}"
        with self._caplog.at_level(level="WARNING", logger="update_database"):
            test_find_newer = update_db.find_newer_assemblies(new_isolate, new_seq_run, session)
        assert ("update_database", logging.WARNING, log_msg) in self._caplog.record_tuples
        assert not test_find_newer


class TestAddToDatabase(unittest.TestCase):
    runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
               / 'runsheet_experiment_name.xlsx'
    config_to_use = {"project_owner": "KMA",
                     "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                     "10": "D",
                                                                     "11": "F",
                                                                     "50": "T"},
                                                "sample_number_format":
                                                    '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                "format_in_sheet":  r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                    r'(?P<sample_year>\d{2})'
                                                                    r'(?P<sample_number>\d{6})'
                                                                    r'(?P<bact_number>-\d{1})',
                                                "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                 r'(?P<sample_year>\d{2})'
                                                                 r'(?P<sample_number>\d{6})'
                                                                 r'(?P<bact_number>-\d{1})',
                                                "format_output": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                   r'(?P<sample_year>\d{2})'
                                                                   r'(?P<sample_number>\d{6})'
                                                                   r'(?P<bact_number>-\d{1})',
                                                "sample_numbers_in": "letter",
                                                "sample_numbers_out": "letter",
                                                "sample_numbers_report": "letter"},
                     "sequencing_mode": "illumina"}

    @classmethod
    def setUpClass(cls) -> None:
        """Set up database connection."""
        # TODO: how can we test mysql specific things
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = False, future = True)
        global session
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)

    @classmethod
    def tearDownClass(cls) -> None:
        """Disconnect from database."""
        session.close()

    def setUp(self) -> None:
        """Truncate tables."""
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()

    def tearDown(self):
        session.rollback()

    # mock date (https://stackoverflow.com/a/55187924/15704972)
    @mock.patch(f'{update_db.__name__}.datetime', wraps=datetime)
    def test_add_approved_result(self, mock_datetime):
        mock_datetime.now.return_value = datetime.fromisoformat("2099-01-01 00:01:23")
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-1"], "godkendt": [1],
                                             "final_id": [pd.NA],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        expected_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        expected_request = SeqRequest(request_id=1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2022-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution="KMA", request_comment = None,
                                      secondary_id = None)
        expected_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [expected_request])
        expected_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = expected_extract_run.extract_run_id,
                                 extract_run = expected_extract_run, isolate = expected_isolate)
        expected_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        expected_forward = ReadFile(readfile_id = 1, read_type = "Illumina",
                                    read_direction = "forward", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R1_001.fastq.gz",
                                    isolate = expected_isolate,
                                    eluate = expected_eluate, institution="KMA",
                                    assembly_readfiles = [expected_asm_readfiles],
                                    eluat_id = expected_eluate.eluat_id)
        expected_reverse = ReadFile(readfile_id = 2,read_type = "Illumina",
                                    read_direction = "reverse", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R2_001.fastq.gz",
                                    isolate = expected_isolate,
                                    eluate = expected_eluate, institution="KMA",
                                    assembly_readfiles = [expected_asm_readfiles],
                                    eluat_id = expected_eluate.eluat_id)
        expected_asm_readfiles.forward_read = expected_forward
        expected_asm_readfiles.reverse_read = expected_reverse
        expected_asm_readfiles.forward_readfile_id = expected_forward.readfile_id
        expected_asm_readfiles.reverse_readfile_id = expected_reverse.readfile_id
        expected_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-05-08"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [expected_forward,
                                                                        expected_reverse])
        expected_forward.sequencing_run = expected_run
        expected_forward.batch_id = expected_run.seq_run_id
        expected_reverse.sequencing_run = expected_run
        expected_reverse.batch_id = expected_run.seq_run_id
        expected_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [expected_isolate])
        expected_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = expected_asm_readfiles,
                                     seq_request = expected_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = expected_asm_readfiles.assembly_readfiles_id,
                                     request_id = expected_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        expected_asm_readfiles.assembly = [expected_assembly]
        all_db_results_sample = session.query(Sample).all()
        assert len(all_db_results_sample) == 1
        assert expected_sample.check_matches(all_db_results_sample[0],
                                             check_relationships = "properties")
        all_db_results_seq_request = session.query(SeqRequest).all()
        assert len(all_db_results_seq_request) == 1
        assert expected_request.check_matches(all_db_results_seq_request[0],
                                              check_relationships = "properties")
        all_db_results_isolate = session.query(Isolate).all()
        assert len(all_db_results_isolate) == 1
        assert expected_isolate.check_matches(all_db_results_isolate[0],
                                              check_relationships = "properties")

        all_db_results_seq_run = session.query(SequencingRun).all()
        assert len(all_db_results_seq_run) == 1
        assert expected_run.check_matches(all_db_results_seq_run[0],
                                          check_relationships = "properties")
        all_db_results_extract_run = session.query(ExtractRun).all()
        assert len(all_db_results_extract_run) == 1
        assert expected_extract_run.check_matches(all_db_results_extract_run[0],
                                                  check_relationships = "properties")
        all_db_results_eluates = session.query(Eluate).all()
        assert len(all_db_results_eluates) == 1
        assert expected_eluate.check_matches(all_db_results_eluates[0],
                                             check_relationships = "properties")
        found_eluate = all_db_results_eluates[0]  # type: database_tables.Eluate
        assert found_eluate.extract_run == all_db_results_extract_run[0]
        all_db_results_reads = session.query(ReadFile).all()
        assert len(all_db_results_reads) == 2
        db_forward_read = session.query(ReadFile).filter_by(read_direction = "forward").one()
        assert expected_forward.check_matches(db_forward_read, check_relationships = "properties")
        db_reverse_read = session.query(ReadFile).filter_by(read_direction = "reverse").one()
        assert expected_reverse.check_matches(db_reverse_read, check_relationships = "properties")
        all_db_results_asm_readfiles = session.query(AssemblyReadFiles).all()

        assert len(all_db_results_asm_readfiles) == 1
        assert expected_asm_readfiles.check_matches(all_db_results_asm_readfiles[0],
                                                    check_relationships = "properties")
        all_db_results_assembly = session.query(Assembly).all()
        assert len(all_db_results_assembly) == 1
        assert expected_assembly.check_matches(all_db_results_assembly[0],
                                               check_relationships = "properties")

    @mock.patch(f'{update_db.__name__}.datetime', wraps = datetime)
    def test_add_nanopore_result(self, mock_datetime):
        mock_datetime.now.return_value = datetime.fromisoformat("2099-01-01 00:01:23")
        runsheet = pathlib.Path(__file__).parent / 'data' / 'update_db' \
                   / 'test_nanopore_runsheet.xlsx'
        runsheet_data = pd.read_excel(runsheet, sheet_name = "Prøveoplysninger")
        config_to_use = {"project_owner": "KMA",
                         "sample_number_settings": {"number_to_letter": {"30": "B",
                                                                         "10": "D",
                                                                         "11": "F",
                                                                         "50": "T"},
                                                    "sample_number_format":
                                                        '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                    "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                       r'(?P<sample_year>\d{2})'
                                                                       r'(?P<sample_number>\d{6})'
                                                                       r'(?P<bact_number>-\d{1})',
                                                    "format_in_lis": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "format_output": r'(?P<sample_type>[135]0|11|[BDFT])'
                                                                     r'(?P<sample_year>\d{2})'
                                                                     r'(?P<sample_number>\d{6})'
                                                                     r'(?P<bact_number>-\d{1})',
                                                    "sample_numbers_in": "letter",
                                                    "sample_numbers_out": "letter",
                                                    "sample_numbers_report": "letter"},
                         "sequencing_mode": "nanopore"}
        success_results = pd.DataFrame(data = {"proevenr": ["B99123456-1"], "godkendt": [1],
                                               "final_id": [pd.NA],
                                               "Bestilt af": ["XYZ"],
                                               "Indikation": ["Identifikation"],
                                               "Avg. coverage depth_basics": [100],
                                               "N50_basics": [12000],
                                               "# contigs": [50],
                                               "Total length_basics": [2000000],
                                               "Grampos/neg": ["Positiv"],
                                               "MADS species": ["Placeholderia"],
                                               "MADS: kendt længde i bp": [pd.NA],
                                               "WGS_species_GTDB_basics":
                                                   ["Placeholderia fakeorum"],
                                               "Serotypning": ["FakeSerotype"],
                                               "platform": ["GridION"],
                                               "Oprensnings_kørselsnr": ["0001"],
                                               "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "nanopore_success"
        update_db.load_run_into_db(success_results, test_dir, runsheet, session,
                                   active_config = config_to_use)
        expected_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        expected_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2022-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        expected_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [expected_request])
        expected_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = expected_extract_run.extract_run_id,
                                 extract_run = expected_extract_run, isolate = expected_isolate)
        expected_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        expected_read = ReadFile(readfile_id = 1, read_type = "Nanopore",
                                    read_direction = None, mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/nanopore_data/"
                                                "barcode01",
                                    isolate = expected_isolate,
                                    eluate = expected_eluate, institution = "KMA",
                                    assembly_readfiles = [expected_asm_readfiles],
                                    eluat_id = expected_eluate.eluat_id)
        expected_asm_readfiles.long_read = expected_read
        expected_asm_readfiles.long_readfile_id = expected_read.readfile_id
        expected_run = SequencingRun(seq_run_id = 1, seq_run_name = "ONT_RUN0001_Y20990101_XYZ",
                                     platform = "GridION", sequencing_type = "nanopore",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2099-01-01"),
                                     runsheet_path = str(runsheet),
                                     data_path = "test/data/update_db/nanopore_data",
                                     institution = "KMA", read_files = [expected_read])
        expected_read.sequencing_run = expected_run
        expected_read.batch_id = expected_run.seq_run_id
        expected_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [expected_isolate])
        expected_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "nanopore", assembler = "flye+medaka",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/nanopore_success/B99123456-1"
                                                            "/nanopore_assembly/"
                                                            "Run0001_B99123456-1.fasta"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/nanopore_success/B99123456-1"
                                                            "/nanopore_assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = expected_asm_readfiles,
                                     seq_request = expected_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = expected_asm_readfiles.assembly_readfiles_id,
                                     request_id = expected_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        expected_asm_readfiles.assembly = [expected_assembly]
        all_db_results_sample = session.query(Sample).all()
        assert len(all_db_results_sample) == 1
        assert expected_sample.check_matches(all_db_results_sample[0],
                                             check_relationships = "properties")
        all_db_results_seq_request = session.query(SeqRequest).all()
        assert len(all_db_results_seq_request) == 1
        assert expected_request.check_matches(all_db_results_seq_request[0],
                                              check_relationships = "properties")
        all_db_results_isolate = session.query(Isolate).all()
        assert len(all_db_results_isolate) == 1
        assert expected_isolate.check_matches(all_db_results_isolate[0],
                                              check_relationships = "properties")

        all_db_results_seq_run = session.query(SequencingRun).all()
        assert len(all_db_results_seq_run) == 1
        assert expected_run.check_matches(all_db_results_seq_run[0],
                                          check_relationships = "properties")
        all_db_results_extract_run = session.query(ExtractRun).all()
        assert len(all_db_results_extract_run) == 1
        assert expected_extract_run.check_matches(all_db_results_extract_run[0],
                                                  check_relationships = "properties")
        all_db_results_eluates = session.query(Eluate).all()
        assert len(all_db_results_eluates) == 1
        assert expected_eluate.check_matches(all_db_results_eluates[0],
                                             check_relationships = "properties")
        found_eluate = all_db_results_eluates[0]  # type: database_tables.Eluate
        assert found_eluate.extract_run == all_db_results_extract_run[0]
        all_db_results_reads = session.query(ReadFile).all()
        assert len(all_db_results_reads) == 1
        db_read = session.query(ReadFile).one()
        assert expected_read.check_matches(db_read, check_relationships = "properties")
        all_db_results_asm_readfiles = session.query(AssemblyReadFiles).all()

        assert len(all_db_results_asm_readfiles) == 1
        assert expected_asm_readfiles.check_matches(all_db_results_asm_readfiles[0],
                                                    check_relationships = "properties")
        all_db_results_assembly = session.query(Assembly).all()
        assert len(all_db_results_assembly) == 1
        assert expected_assembly.check_matches(all_db_results_assembly[0],
                                               check_relationships = "properties")

    def test_handle_nan_samples(self):
        qc_sheet = pd.DataFrame(data = {"proevenr": ["B99123456-1", pd.NA], "godkendt": [1, pd.NA],
                                        "Bestilt af": ["XYZ", pd.NA],
                                        "noter": [pd.NA,
                                                  "A note that applies to the entire sheet"],
                                        "Indikation": ["Identifikation", pd.NA],
                                        "Avg. coverage depth_basics": [100, pd.NA],
                                        "final_id": [pd.NA, pd.NA],
                                        "N50_basics": [12000, pd.NA],
                                        "# contigs": [20, pd.NA],
                                        "Total length_basics": [2000000, pd.NA],
                                        "Grampos/neg": ["Positiv", pd.NA],
                                        "MADS species": ["Placeholderia", pd.NA],
                                        "MADS: kendt længde i bp": [pd.NA, pd.NA],
                                        "WGS_species_GTDB_basics":
                                            ["Placeholderia fakeorum", pd.NA],
                                        "Serotypning": ["FakeSerotype", pd.NA],
                                        "platform": ["MiSeq", pd.NA],
                                        "Oprensnings_kørselsnr": ["0001", pd.NA],
                                        "pipeline_version": ["1.2.3", pd.NA]})
        original_sample = database_tables.Sample(mads_proevenr = "B99123456", institution = "KMA",
                                                 lab_section = "B")
        session.add(original_sample)
        all_db_results = session.query(Sample).all()
        assert len(all_db_results) == 1
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(qc_sheet, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        all_db_results = session.query(Sample).all()
        assert len(all_db_results) == 1

    def test_no_overwrite_existing_sample(self):
        original_sample = database_tables.Sample(mads_proevenr = "B99123456", institution = "KMA",
                                                 lab_section = "B")
        session.add(original_sample)
        all_db_results = session.query(Sample).all()
        assert len(all_db_results) == 1
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2"], "godkendt": [1],
                                             "Bestilt af": ["XYZ"],
                                             "final_id": [pd.NA],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                       active_config = self.config_to_use)
        all_db_results = session.query(Sample).all()
        assert len(all_db_results) == 1

    def test_one_sample_multi_isolate(self):
        original_sample = database_tables.Sample(mads_proevenr = "B99654321", institution = "KMA",
                                                 lab_section = "B")
        session.add(original_sample)
        all_db_results = session.query(Sample).all()
        assert len(all_db_results) == 1
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2", "B99123456-1"],
                                             "godkendt": [1, 1],
                                             "Bestilt af": ["XYZ", "XYZ"],
                                             "final_id": [pd.NA, pd.NA],
                                             "Indikation": ["Identifikation", "HAI"],
                                             "Avg. coverage depth_basics": [100, 100],
                                             "N50_basics": [12000, 12000],
                                             "# contigs": [50, 50],
                                             "Total length_basics": [2000000, 2000000],
                                             "Grampos/neg": ["Positiv", "Positiv"],
                                             "MADS species": ["Placeholderia", "Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA, pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum",
                                                  "Placeholderia bielefeldensis"],
                                             "Serotypning": ["FakeSerotype", "FakeSerotype2"],
                                             "platform": ["MiSeq", "MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001", "0001"],
                                             "pipeline_version": ["1.2.3", "1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                       active_config = self.config_to_use)
            all_db_results = session.query(Sample).all()
            assert len(all_db_results) == 2

    def test_no_overwrite_existing_extraction(self):
        original_run = database_tables.ExtractRun(extract_run_id = 1, extract_run_name = '0001',
                                                  institution = "KMA")
        session.add(original_run)
        all_db_results = session.query(ExtractRun).all()
        assert len(all_db_results) == 1
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2"], "godkendt": [1],
                                             "Bestilt af": ["XYZ"],
                                             "final_id": [pd.NA],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        all_db_results = session.query(ExtractRun).all()
        print(all_db_results)
        assert len(all_db_results) == 1

    def test_update_isolate_if_approved(self):
        original_isolate = Isolate(mads_isolatnr = "B99123456-2", mads_proevenr = "B99123456",
                                   preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "",
                                   institution = "KMA")
        session.add(original_isolate)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == ""
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2"], "godkendt": [1],
                                             "Bestilt af": ["XYZ"],
                                             "final_id": [pd.NA],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == "FakeSerotype"

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_dont_update_if_older(self):
        expected_extract_run = database_tables.ExtractRun(extract_run_id = 1,
                                                          extract_run_name = '0001',
                                                          institution = "KMA")
        # we'll need to pretend we know all the IDs
        expected_request = SeqRequest(request_id = 1, mads_proevenr = "B99123456",
                                      mads_isolatnr = "B99123456-1",
                                      requested_by = "XYZ", resistens = False,
                                      identifikation = True, relaps = False,
                                      toxingen_identifikation = False, hai = False,
                                      miljoeproeve = False, udbrud = False, overvaagning = False,
                                      forskning = False, andet = False, registered_by = "ABC",
                                      request_date = datetime.fromisoformat("2023-10-05 00:00:00"),
                                      gram_negativ = False, gram_positiv = True,
                                      institution = "KMA", request_comment = None,
                                      secondary_id = None)
        expected_isolate = Isolate(mads_isolatnr = "B99123456-1", mads_proevenr = "B99123456",
                                   institution = "KMA", preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "FakeSerotype", requests = [expected_request])
        expected_eluate = Eluate(eluat_id = 1, mads_isolatnr = "B99123456-1",
                                 extract_run_id = expected_extract_run.extract_run_id,
                                 extract_run = expected_extract_run, isolate = expected_isolate)
        expected_asm_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1)
        expected_forward = ReadFile(readfile_id = 1, read_type = "Illumina",
                                    read_direction = "forward", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R1_001.fastq.gz",
                                    isolate = expected_isolate,
                                    eluate = expected_eluate, institution = "KMA",
                                    assembly_readfiles = [expected_asm_readfiles],
                                    eluat_id = expected_eluate.eluat_id)
        expected_reverse = ReadFile(readfile_id = 2, read_type = "Illumina",
                                    read_direction = "reverse", mads_isolatnummer = "B99123456-1",
                                    read_file = "test/data/update_db/rawdata_correct/"
                                                "B99123456-1_L001_R2_001.fastq.gz",
                                    isolate = expected_isolate,
                                    eluate = expected_eluate, institution = "KMA",
                                    assembly_readfiles = [expected_asm_readfiles],
                                    eluat_id = expected_eluate.eluat_id)
        expected_asm_readfiles.forward_read = expected_forward
        expected_asm_readfiles.reverse_read = expected_reverse
        expected_asm_readfiles.forward_readfile_id = expected_forward.readfile_id
        expected_asm_readfiles.reverse_readfile_id = expected_reverse.readfile_id
        expected_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_Run0000_Y20230508_XYZ",
                                     platform = "MiSeq", sequencing_type = "illumina",
                                     run_user_id = "XYZ",
                                     run_date = date.fromisoformat("2023-10-10"),
                                     runsheet_path = str(self.runsheet),
                                     data_path = "test/data/update_db/rawdata_correct",
                                     institution = "KMA", read_files = [expected_forward,
                                                                        expected_reverse])
        expected_forward.sequencing_run = expected_run
        expected_forward.batch_id = expected_run.seq_run_id
        expected_reverse.sequencing_run = expected_run
        expected_reverse.batch_id = expected_run.seq_run_id
        expected_sample = Sample(mads_proevenr = "B99123456", institution = "KMA",
                                 lab_section = "B", isolates = [expected_isolate])
        expected_assembly = Assembly(assembly_id = 1,
                                     mads_proevenr = "B99123456", mads_isolatnr = "B99123456-1",
                                     assembly_type = "illumina", assembler = "shovill/skesa",
                                     pipeline_version = "1.2.3",
                                     assembly_file = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"
                                                            "ILM_Run0000_Y20230508_XYZ_B99123456-1.fna"),
                                     assembly_dir = str(pathlib.Path(
                                         __file__).parent / "data/update_db/success_dir/B99123456-1"
                                                            "/assembly/"),
                                     n50 = 12000, num_contigs = 50, coverage = 100,
                                     genome_size = 2000000, release = True,
                                     institution = "KMA",
                                     assembly_readfiles = expected_asm_readfiles,
                                     seq_request = expected_request,
                                     pipeline_organism_name = "Placeholderia fakeorum",
                                     db_assembly_readfiles_id = expected_asm_readfiles.assembly_readfiles_id,
                                     request_id = expected_request.request_id,
                                     timestamp = datetime.fromisoformat("2099-01-01 00:01:23")
                                     )
        session.add(expected_assembly)
        session.add(expected_sample)
        session.add(expected_eluate)
        session.add(expected_run)
        session.add(expected_isolate)
        session.add(expected_request)
        session.add(expected_asm_readfiles)
        session.add(expected_forward)
        session.add(expected_reverse)
        session.add(expected_extract_run)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == "FakeSerotype"
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-1"], "godkendt": [1],
                                             "Bestilt af": ["XYZ"],
                                             "final_id": [pd.NA],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia bielefeldensis"],
                                             "Serotypning": ["FakeSerotype2"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        log_msg = "There is a result for isolate B99123456-1" \
                  " from a newer sequencing run in the database. Final ID will not be updated."
        with self._caplog.at_level(level="INFO", logger="update_database"):
            update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                       active_config = self.config_to_use)
        assert ("update_database", logging.INFO, log_msg) in self._caplog.record_tuples
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == "FakeSerotype"
        assert isolate_hit.final_organism_name == "Placeholderia fakeorum"

    def test_update_with_final_id(self):
        original_isolate = Isolate(mads_isolatnr = "B99123456-2", mads_proevenr = "B99123456",
                                   preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "",
                                   institution = "KMA")
        session.add(original_isolate)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == ""
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2"], "godkendt": [1],
                                             "final_id": ["Placeholderia bielefeldensis"],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [12000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == "FakeSerotype"
        assert isolate_hit.final_organism_name == "Placeholderia bielefeldensis"

    def test_no_update_if_fail(self):
        original_isolate = Isolate(mads_isolatnr = "B99123456-2", mads_proevenr = "B99123456",
                                   preliminary_organism_name = "Placeholderia",
                                   final_organism_name = "Placeholderia fakeorum",
                                   wgs_serotyping = "",
                                   institution = "KMA")
        session.add(original_isolate)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == ""
        success_results = pd.DataFrame(data={"proevenr": ["B99123456-2"], "godkendt": [0],
                                             "final_id": [pd.NA],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Avg. coverage depth_basics": [100],
                                             "N50_basics": [9000],
                                             "# contigs": [50],
                                             "Total length_basics": [2000000],
                                             "Grampos/neg": ["Positiv"],
                                             "MADS species": ["Placeholderia"],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Placeholderia fakeorum"],
                                             "Serotypning": ["FakeSerotype"],
                                             "platform": ["MiSeq"],
                                             "Oprensnings_kørselsnr": ["0001"],
                                             "pipeline_version": ["1.2.3"]})
        test_dir = pathlib.Path(__file__).parent / "data" / "update_db" / "success_dir"
        update_db.load_run_into_db(success_results, test_dir, self.runsheet, session,
                                   active_config = self.config_to_use)
        all_db_results = session.query(Isolate).all()
        assert len(all_db_results) == 1
        isolate_hit = all_db_results[0]
        assert isolate_hit.wgs_serotyping == ""
