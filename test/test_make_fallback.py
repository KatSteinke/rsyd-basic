#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest

import pandas as pd
import pytest

import make_fallbacks


class TestFakeReports(unittest.TestCase):
    def test_quast_reads(self):
        true_quast_reads = pd.DataFrame(data={"Assembly": ["# total reads", "# left", "# right",
                                                           "# mapped", "Mapped (%)", 
                                                           "# properly paired",
                                                           "Properly paired (%)",
                                                           "# singletons",
                                                           "# misjoint mates",
                                                           "Misjoint mates (%)",
                                                           "Avg. coverage depth",
                                                           "Coverage >= 1x (%)",
                                                           "Coverage >= 5x (%)",
                                                           "Coverage >= 10x (%)"],
                                              "F99123456": [pd.NA, pd.NA, pd.NA,
                                                            pd.NA, pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA,
                                                            pd.NA]})
        test_quast_reads = make_fallbacks.make_blank_quast_reads(sample_number = "F99123456")
        print(test_quast_reads)
        pd.testing.assert_frame_equal(true_quast_reads, test_quast_reads)

    def test_quast_report(self):
        true_quast_report = pd.DataFrame(data={"Assembly": ["F99123456"],
                                               "# contigs (>= 0 bp)": [pd.NA],
                                               "# contigs (>= 1000 bp)": [pd.NA],
                                               "# contigs (>= 5000 bp)": [pd.NA],
                                               "# contigs (>= 10000 bp)": [pd.NA],
                                               "# contigs (>= 25000 bp)": [pd.NA],
                                               "# contigs (>= 50000 bp)": [pd.NA],
                                               "Total length (>= 0 bp)": [pd.NA],
                                               "Total length (>= 1000 bp)": [pd.NA],
                                               "Total length (>= 5000 bp)": [pd.NA],
                                               "Total length (>= 10000 bp)": [pd.NA],
                                               "Total length (>= 25000 bp)": [pd.NA],
                                               "Total length (>= 50000 bp)": [pd.NA],
                                               "# contigs": [pd.NA],
                                               "Largest contig": [pd.NA],
                                               "Total length": [pd.NA],
                                               "Estimated reference length": [pd.NA],
                                               "GC (%)": [pd.NA],
                                               "N50": [pd.NA],
                                               "NG50": [pd.NA],
                                               "N90": [pd.NA],
                                               "auN": [pd.NA],
                                               "auNG": [pd.NA],
                                               "L50": [pd.NA],
                                               "LG50": [pd.NA],
                                               "L90": [pd.NA],
                                               "# total reads": [pd.NA],
                                               "# left": [pd.NA],
                                               "# right": [pd.NA],
                                               "Mapped (%)": [pd.NA],
                                               "Properly paired (%)": [pd.NA],
                                               "Avg. coverage depth": [pd.NA],
                                               "Coverage >= 1x (%)": [pd.NA],
                                               "# N's per 100 kbp": [pd.NA],
                                               "# predicted genes (unique)": [pd.NA],
                                               "# predicted genes (>= 0 bp)": [pd.NA],
                                               "# predicted genes (>= 300 bp)": [pd.NA],
                                               "# predicted genes (>= 1500 bp)": [pd.NA],
                                               "# predicted genes (>= 3000 bp)": [pd.NA]})
        test_quast_report = make_fallbacks.make_blank_quast_report(sample_number = "F99123456")
        print(test_quast_report)
        pd.testing.assert_frame_equal(true_quast_report, test_quast_report)


class TestCheckmFallbacks(unittest.TestCase):
    def test_checkm_sample(self):
        true_blank = pd.DataFrame(data = {"Bin Id": ["F99123456"],
                                          "Marker lineage": [pd.NA],
                                          "# genomes": [pd.NA],
                                          "# markers": [pd.NA],
                                          "# marker sets": [pd.NA],
                                          "0": [pd.NA],
                                          "1": [pd.NA],
                                          "2": [pd.NA],
                                          "3": [pd.NA],
                                          "4": [pd.NA],
                                          "5+": [pd.NA],
                                          "Completeness": [0.00],
                                          "Contamination": [pd.NA],
                                          "Strain heterogeneity": [pd.NA]})
        test_blank = make_fallbacks.make_blank_checkm("F99123456")
        pd.testing.assert_frame_equal(test_blank, true_blank)
