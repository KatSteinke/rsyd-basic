#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import pandas as pd
import pytest

import get_lis_report
import helpers
from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames

# set up some translations we're going to be using over and over
SHEET_NAMES_EN = RunsheetNames(sample_info_sheet = "Sample_information",
                               sample_number = "Sample_number",
                               extraction_well_number = "Extraction_well_number",
                               extraction_run_number = "Extraction_run_number",
                               dna_concentration = "DNA_concentration",
                               requested_by = "Requested_by",
                               comment = "Comment",
                                       sequencing_run_number = "sequencing_run_number",
                               indications = ["AMR", "Identification", "Relapse", "HAI",
                                              "Toxin gene identification",
                                              "Environmental sample isolate",
                                              "Outbreak sample isolate",
                                              "Surveillance sample isolate", "Research",
                                              "Other"],
                               dna_normalization_sheet = "DNA normalization",
                               dna_normalization_sample_nr = "Sample_number",
                               ilm_well_number = "ILM_well_position",
                               plate_layout = "Plate_layout",
                               runsheet_base = "Runsheet",
                               experiment_name = "RUNxxxx-INI",
                               nanopore_sheet_sample_number = "Sample_number",
                               barcode = "Barcode",
                               protocol_version = "Lab protocol version")
LIS_NAMES_EN = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                            isolate_number = "BACT_NR",
                            bacteria_code = "LOCAL_BACT_CODE",
                            bacteria_text = "LOCAL_BACT_TEXT")
BACT_NAMES_EN = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                     internal_bacteria_text = "bacteria_text",
                                     bacteria_category = "bacteria_category")


class TestKeyErrorMessageOverride(unittest.TestCase):
    def test_message_repr(self):
        to_print = helpers.PrettyKeyErrorMessage("This is a message with a\nnewline")
        true_message = """This is a message with a
newline"""
        test_message = repr(to_print)
        assert test_message == true_message


class TestCheckSampleNames(unittest.TestCase):
    error_message = "Sample IDs must start with 10 or 11 or 30 or 50 followed by " \
                    "eight numbers and a single digit isolate number. " \
                    "Please correct sample IDs in runsheet."
    workflow_config = {"sample_number_settings":
                           {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                            "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "format_output": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "sample_numbers_in": "number",
                            "sample_numbers_out": "letter",
                            "sample_numbers_report": "letter",
                            "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                            },
                       "sequencing_mode": "illumina"}

    def test_catch_wrong_prefix(self):
        fail_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "wrong_prefix.xlsx"
        sample_error = "Sample IDs ['3199123456-1'] are not valid. \n" + self.error_message
        with pytest.raises(ValueError, match = re.escape(sample_error)):
            helpers.check_sample_sheet(fail_sheet, active_config = self.workflow_config)

    def test_catch_missing_year(self):
        fail_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "missing_year.xlsx"
        sample_error = "Sample IDs ['30123456-1'] are not valid. \n" + self.error_message
        with pytest.raises(ValueError, match = re.escape(sample_error)):
            helpers.check_sample_sheet(fail_sheet)

    def test_catch_missing_isolate(self):
        fail_sheet = pathlib.Path(__file__).parent  / "data" / "utilities_test" / "missing_isolate.xlsx"
        sample_error = "Sample IDs ['3099123456'] are not valid. \n" + self.error_message
        with pytest.raises(ValueError, match = re.escape(sample_error)):
            helpers.check_sample_sheet(fail_sheet, active_config = self.workflow_config)

    def test_check_not_in_mads(self):
        fail_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "not_in_mads.xlsx"
        fake_mads = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199654321-1"]})
        error_message = "The following numbers were not found in the MADS report:\n1199654321-2"
        with pytest.raises(KeyError, match = re.escape(error_message)):
            helpers.check_sample_sheet(fail_sheet, lis_report = fake_mads)

    def test_check_multiple_not_in_mads(self):
        fail_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "not_in_mads.xlsx"

        fake_mads = pd.DataFrame(data = {"proevenr": ["1199123456-2", "1199654321-1"]})
        error_message = "The following numbers were not found in the MADS report:\n" \
                        "1199123456-1\n" \
                        "1199654321-2"
        with pytest.raises(KeyError, match = re.escape(error_message)):
            helpers.check_sample_sheet(fail_sheet, lis_report = fake_mads)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_check_warn_missing_codes(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "success_sheet.xlsx"
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOKAL_BAKT_KODE": ["1", "3"]})
        translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                            "bakterie_kategori": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        expected_missing = pd.DataFrame(data = {"Prøvenr": ["1199123456-2"],
                                                "LOKAL_BAKT_KODE": ["3"]})
        warn_msg = f"No species names found for MADS bacteria codes " \
                   f"for the following samples: \n" \
                   f"{expected_missing.to_string()}\n" \
                   f"The pipeline will still run, but no species-specific analyses " \
                   f"will be performed for these samples. \n" \
                   f"Contact the responsible bioinformatician."
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            helpers.check_sample_sheet(test_sheet, lis_data, translations)
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples

    def test_check_warn_missing_translate(self):
        """Warn for missing codes with non-default column names."""
        test_sheet = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "success_sheet_en.xlsx"
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOCAL_BACT_CODE": ["1", "3"]})
        translations = pd.DataFrame(data = {"bacteria_code": ["1", "2"],
                                            "bacteria_category": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        expected_missing = pd.DataFrame(data = {"Sample_number": ["1199123456-2"],
                                                "LOCAL_BACT_CODE": ["3"]})
        warn_msg = "No species names found for MADS bacteria codes " \
                   "for the following samples: \n" \
                   f"{expected_missing.to_string()}\n" \
                   "The pipeline will still run, but no species-specific analyses " \
                   "will be performed for these samples. \n" \
                   "Contact the responsible bioinformatician."
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            helpers.check_sample_sheet(test_sheet, lis_data, translations,
                                       runsheet_names = SHEET_NAMES_EN,
                                       lis_data_names = LIS_NAMES_EN,
                                       bacteria_names = BACT_NAMES_EN)
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples

    def test_different_config(self):
        test_config = {"sample_number_settings": {'sample_number_format':
                                                      '([BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"}},
                       "sequencing_mode": "illumina"}
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" / "success_sheet.xlsx"
        sample_error = "Sample IDs ['1199123456-1', '1199123456-2'] are not valid. \n" \
                       "Sample IDs must start with B or D or F or T followed by " \
                       "eight numbers and a single digit isolate number. " \
                       "Please correct sample IDs in runsheet."
        with pytest.raises(ValueError, match = re.escape(sample_error)):
            helpers.check_sample_sheet(test_sheet, active_config = test_config)

    def test_check_success(self):
        """Successfully check an Illumina runsheet without issues."""
        test_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "success_sheet.xlsx")
        success_msg = "Data sheet is OK"
        with self._caplog.at_level(level="INFO", logger="helpers"):
            helpers.check_sample_sheet(test_sheet)
        assert ("helpers", logging.INFO, success_msg) in self._caplog.record_tuples

    def test_check_success_translate(self):
        """Successfully check an Illumina runsheet with non-default column names."""
        test_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "success_sheet_en.xlsx")
        success_msg = "Data sheet is OK"
        with self._caplog.at_level(level="INFO", logger="helpers"):
            helpers.check_sample_sheet(test_sheet, runsheet_names = SHEET_NAMES_EN,
                                       lis_data_names = LIS_NAMES_EN,
                                       bacteria_names = BACT_NAMES_EN)
        assert ("helpers", logging.INFO, success_msg) in self._caplog.record_tuples


class TestCheckNanoporeSheet(unittest.TestCase):
    def test_check_barcodes(self):
        """Complain if no barcodes are given in a Nanopore runsheet."""
        fail_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_nanopore_runsheet_no_barcodes.xlsx")
        error_msg = "No barcodes given for sample(s) ['B99123456-1', 'B99123456-2']."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_nanopore_runsheet(fail_sheet)

    def test_fail_blank_runsheet(self):
        """Complain if there are no sample numbers in the runsheet."""
        fail_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "nanopore_blank_runsheet.xlsx")
        error_msg = "No sample numbers in runsheet."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_nanopore_runsheet(fail_sheet)

    def test_check_sample_numbers_in_data(self):
        """Complain if sample numbers in Nanopore runsheet are not present
        in sample information sheet."""
        fail_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "nanopore_not_in_data_sheet.xlsx")
        error_msg = ("Samples ['F99123456-1', 'F99123456-2'] are "
                     "present in runsheet but not sample information sheet ('Prøveoplysninger').")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_nanopore_runsheet(fail_sheet)

    def test_check_sample_numbers_in_runsheet(self):
        """Complain if sample numbers in Nanopore sample information sheet are not present
        in runsheet."""
        fail_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "nanopore_not_in_runsheet.xlsx")
        error_msg = ("Samples ['F99123456-1', 'F99123456-2'] are "
                     "present in sample information sheet ('Prøveoplysninger') but not runsheet.")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_nanopore_runsheet(fail_sheet)

    def test_sample_numbers_runsheet_translate(self):
        """Complain if sample numbers in a Nanopore sample information sheet with a non-default name
         are not present in runsheet."""
        fail_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "nanopore_not_in_runsheet_en.xlsx")
        error_msg = ("Samples ['F99123456-1', 'F99123456-2'] are "
                     "present in sample information sheet ('Sample_information') but not runsheet.")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_nanopore_runsheet(fail_sheet,
                                            runsheet_names = SHEET_NAMES_EN)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_check_success_nanopore(self):
        """Successfully check a Nanopore runsheet without issues."""
        test_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_nanopore_runsheet.xlsx")
        success_msg = "Nanopore runsheet is OK"
        with self._caplog.at_level(level="INFO", logger="helpers"):
            helpers.check_nanopore_runsheet(test_sheet)
        assert ("helpers", logging.INFO, success_msg) in self._caplog.record_tuples

    def test_success_check_nanopore_translate(self):
        """Successfully check a Nanopore runsheet with non-default column names."""
        test_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_nanopore_runsheet_en.xlsx")
        success_msg = "Nanopore runsheet is OK"
        with self._caplog.at_level(level="INFO", logger="helpers"):
            helpers.check_nanopore_runsheet(test_sheet,
                                            runsheet_names = SHEET_NAMES_EN)
        assert ("helpers", logging.INFO, success_msg) in self._caplog.record_tuples


class TestSplitRunsheet(unittest.TestCase):
    def test_split_simple_sheet(self):
        test_sheet = pd.read_csv(pathlib.Path(__file__).parent / "data" / "utilities_test" \
                                 / "simple_runsheet.csv").to_csv(index = False, sep = "\t")
        true_result = {"Header": "IEMFileVersion\t4\t\t\t\n"
                                 "Investigator Name\tXYZ\t\t\t\n"
                                 "Project Name\ttestproject\t\t\t\n"
                                 "Experiment Name\ttest_experiment\t\t\t\n"
                                 "Date\t20990101\t\t\t\n"
                                 "Workflow\tGenerate FASTQ\t\t\t\n"
                                 "Application\t\t\t\t\n"
                                 "Assay\tNextera XT\t\t\t\n"
                                 "Description\t\t\t\t\n"
                                 "Chemistry\tAmplicon\t\t\t\n",
                       "Data": "Sample_ID\tI7_Index_ID\tindex\tI5_Index_ID\tindex2\n"
                               "B99123456-1\tN701\tTAAGGCGA\tS501\tTAGATCGC\n"}
        test_result = helpers.split_illumina_sheet(test_sheet)
        print(true_result)
        print(test_result)
        assert test_result == true_result

    def test_split_missing_section(self):
        test_sheet = pd.read_csv(pathlib.Path(__file__).parent / "data" / "utilities_test" \
                                 / "runsheet_no_header.csv").to_csv(index = False, sep = "\t")
        with pytest.raises(KeyError,
                           match = re.escape("Section(s) {'Header'} were not found "
                                             "in the runsheet.")):
            helpers.split_illumina_sheet(test_sheet)

    def test_catch_bad_header(self):
        test_sheet = """    [Header Unnamed
        key value"""
        error_msg = f"The Illumina runsheet's format is invalid: " \
                    f"broken section header in row 1. \n" \
                    f"Section headers need to be in square brackets " \
                    f"(like this: [Et_eller_andet_eksempel])"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.split_illumina_sheet(test_sheet)


class TestGetLabProtocolVersion(unittest.TestCase):
    def test_success_get_protocol(self):
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_template_correct.xlsx"
        expected_version = "Version_1.2.3"
        test_version = helpers.get_illumina_lab_protocol_version(runsheet)
        assert test_version == expected_version

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_warn_no_runsheet(self):
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                       / "runsheet_template_no_sheets.xlsx"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet)
            warn_msg = "No runsheet tab found in Excel sheet. " \
                       "Wetlab protocol version will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version

    def test_warn_too_many_sheets(self):
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                       / "runsheet_template_too_many_sheets.xlsx"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet)
            warn_msg = "Too many runsheet tabs found in Excel sheet. " \
                       "Cannot find the correct runsheet. " \
                       "Wetlab protocol version will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version

    def test_handle_non_default(self):
        """Handle non-default runsheet base name."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_template_too_many_sheets.xlsx"
        test_config = RunsheetNames(sample_info_sheet="Prøveoplysninger",
                                    sample_number = "Prøvenr",
                                    extraction_well_number = "Brønd_nr",
                                    extraction_run_number = "Oprensnings_kørselsnr",
                                    dna_concentration = "DNA_konc",
                                    requested_by = "Bestilt af",
                                    comment = "Kommentar",
                                    sequencing_run_number = "SEK_Run_nr",
                                    indications = ["Resistens", "Identifikation",
                                                   "Relaps", "Toxingen_ID",
                                                   "HAI", "Miljøprøveisolat",
                                                   "Udbrudsisolat",
                                                   "Overvågningsisolat",
                                                   "Forskning", "Andet"],
                                    dna_normalization_sheet = "DNA normalisering",
                                    dna_normalization_sample_nr = "Prøvenummer",
                                    ilm_well_number = "ILM Brønd position",
                                    plate_layout = "Pladelayout",
                                    runsheet_base = "Run_sheet",
                                    experiment_name = "RUNxxxx-INI",
                                    nanopore_sheet_sample_number = "KMA_nr",
                                    barcode = "Barkode",
                                    protocol_version = "Protokol (versionsnummer)")
        expected_version = "Version_1.2.3"
        test_version = helpers.get_illumina_lab_protocol_version(runsheet,
                                                                 runsheet_names = test_config)
        assert test_version == expected_version


    # check both version not found and wrong version
    def test_warn_wrong_version(self):
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                       / "ILM_Run_Skabelon.xlsm"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet)
            warn_msg = "Runsheet format version is not found. " \
                       f"Wetlab protocol version can only be extracted from " \
                       f"runsheets with format version 2. \n" \
                       f"Wetlab protocol version will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version
            runsheet_wrong_version = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                                     / "runsheet_template_wrong_version.xlsx"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet_wrong_version)
            warn_msg = "Runsheet format version is 1. " \
                       f"Wetlab protocol version can only be extracted from " \
                       f"runsheets with format version 2. \n" \
                       f"Wetlab protocol version will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version

    def test_missing_bcl_section(self):
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                       / "runsheet_template_no_bcl.xlsx"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet)
            warn_msg = "Cannot find section BCLConvert_Data. " \
                       "Wetlab protocol version will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version

    def test_missing_project_col(self):
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                       / "runsheet_template_no_project.xlsx"
            test_version = helpers.get_illumina_lab_protocol_version(runsheet)
            warn_msg = "Wetlab protocol version should be given " \
                       "under the column Sample_Project.\n" \
                       "As the column is missing, the wetlab protocol version" \
                       " will not be recorded in results."
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples
            assert not test_version

    def test_handle_blank_samples(self):
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_template_blank_samples.xlsx"
        expected_version = "nan"
        test_version = helpers.get_illumina_lab_protocol_version(runsheet)
        assert test_version == expected_version


class TestGetNanoporeProtocolVersion(unittest.TestCase):
    def test_get_version_success(self):
        """Ensure the version can be extracted from a correctly formatted runsheet."""
        runsheet = pathlib.Path(
            __file__).parent / "data" / "merge_results" / "test_nanopore_runsheet.xlsx"
        expected_version = "Version_1.2.3"
        test_version = helpers.get_nanopore_lab_protocol_version(runsheet)
        assert expected_version == test_version

    def test_get_version_success_translate(self):
        """Ensure the version can be extracted from a correctly formatted runsheet with
        non-default column names."""
        runsheet = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet_en.xlsx"
        expected_version = "Version_1.2.3"
        test_version = helpers.get_nanopore_lab_protocol_version(runsheet,
                                                                 runsheet_names = SHEET_NAMES_EN)
        assert expected_version == test_version

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_handle_missing_version_col(self):
        """Ensure missing version column is handled."""
        runsheet = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
        expected_version = ""
        log_msg = "No version column found in runsheet. " \
                  "Wetlab protocol version will not be recorded in results."
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            test_version = helpers.get_nanopore_lab_protocol_version(runsheet)
            assert ("helpers", logging.WARNING, log_msg) in self._caplog.record_tuples
        assert expected_version == test_version

        runsheet_different_cols = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet_en.xlsx"
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            self._caplog.clear()
            test_version_en = helpers.get_nanopore_lab_protocol_version(runsheet_different_cols)
            assert ("helpers", logging.WARNING, log_msg) in self._caplog.record_tuples
        assert expected_version == test_version_en

    def test_fail_no_nanopore_sheet(self):
        """Ensure the script stops informatively if the Nanopore runsheet has not been given."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "ILM_Run_Skabelon.xlsm"
        error_msg = "No Nanopore runsheet tab found in sheet."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            helpers.get_nanopore_lab_protocol_version(runsheet)


class TestCheckExperimentName(unittest.TestCase):
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_no_issues(self):
        """Ensure a name without problematic components doesn't raise an exception."""
        test_name = "samplerun"
        log_msg = "No issues found with experiment name samplerun."
        with self._caplog.at_level(level="DEBUG", logger="helpers"):
            helpers.check_experiment_name_problems(test_name)
            assert ("helpers", logging.DEBUG, log_msg) in self._caplog.record_tuples

    def test_check_whitespace_breaks(self):
        """Ensure that the script complains on names containing whitespace"""
        test_name = "sample run"
        with pytest.raises(ValueError, match = "The run name contains spaces or line breaks."):
            helpers.check_experiment_name_problems(test_name)

    def test_check_illegal_in_windows(self):
        """Ensure that the script complains for names that are illegal in Windows"""
        test_control_char = "NUL"
        test_bad_char = "sample:run"
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used "
                                   "in Windows filenames."):
            helpers.check_experiment_name_problems(test_control_char)
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used"
                                   " in Windows filenames."):
            helpers.check_experiment_name_problems(test_bad_char)

    def test_check_slash_breaks(self):
        """Ensure the script complains for names containing forward slashes"""
        test_name = "sample/run"
        error_msg = "The run name contains a forward slash (/). " \
                    "This will break the result directory." \
                    " Replace forward slashes with underscores (_)."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.check_experiment_name_problems(test_name)


class TestExtractIlluminaRun(unittest.TestCase):
    def test_get_run_name(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "runsheet_experiment_name.xlsx"
        true_run_name = "ILM_Run0000_Y20220101_XYZ"
        test_run_name = helpers.extract_illumina_run_name(test_sheet)
        assert test_run_name == true_run_name

    def test_get_run_name_translate(self):
        """Get the run name in a sheet with non-default tab names."""
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "success_sheet_en.xlsx"
        true_run_name = "ILM_Run0000_Y20220101_XYZ"
        test_run_name = helpers.extract_illumina_run_name(test_sheet,
                                                          runsheet_names = SHEET_NAMES_EN)
        assert test_run_name == true_run_name

    def test_check_whitespace_breaks(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "runsheet_bad_name.xlsx"
        with pytest.raises(ValueError, match = "The run name contains spaces or line breaks."):
            helpers.extract_illumina_run_name(test_sheet)

    def test_check_illegal_in_windows(self):
        test_control_char = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                            / "runsheet_controlchar.xlsx"
        test_bad_char = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "runsheet_badchar.xlsx"
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used "
                                   "in Windows filenames."):
            helpers.extract_illumina_run_name(test_control_char)
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used"
                                   " in Windows filenames."):
            helpers.extract_illumina_run_name(test_bad_char)

    def test_slash_breaks(self):
        test_with_slash = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "runsheet_slash.xlsx"
        error_msg = "The run name contains a forward slash (/). " \
                    "This will break the result directory." \
                    " Replace forward slashes with underscores (_)."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.extract_illumina_run_name(test_with_slash)


class TestExtractNanoporeRun(unittest.TestCase):
    def test_get_run_name(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "test_nanopore_runsheet.xlsx"
        true_run_name = "ONT_RUN0000_Y20990101_XYZ"
        test_run_name = helpers.extract_nanopore_run_name(test_sheet)
        assert test_run_name == true_run_name

    def test_get_run_name_translate(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "test_nanopore_runsheet_change_more.xlsx"
        sheet_names_new = RunsheetNames(sample_info_sheet = "Sample_information",
                                        sample_number = "Sample_number",
                                        extraction_well_number = "Extraction_well_number",
                                        extraction_run_number = "Extraction_run_number",
                                        dna_concentration = "DNA_concentration",
                                        requested_by = "Requested_by",
                                        comment = "Comment",
                                        sequencing_run_number = "sequencing_run_number",
                                        indications = ["AMR", "Identification", "Relapse", "HAI",
                                                       "Toxin gene identification",
                                                       "Environmental sample isolate",
                                                       "Outbreak sample isolate",
                                                       "Surveillance sample isolate", "Research",
                                                       "Other"],
                                        dna_normalization_sheet = "DNA normalization",
                                        dna_normalization_sample_nr = "Sample_number",
                                        ilm_well_number = "ILM_well_position",
                                        plate_layout = "Plate_layout",
                                        runsheet_base = "Run_sheet",
                                        experiment_name = "RUNxxxx-initials",
                                        nanopore_sheet_sample_number = "Sample_number",
                                        barcode = "Barcode",
                                        protocol_version = "Lab protocol version")
        true_run_name = "ONT_RUN0000_Y20990101_XYZ"
        test_run_name = helpers.extract_nanopore_run_name(test_sheet,
                                                          runsheet_names = sheet_names_new)
        assert test_run_name == true_run_name

    def test_check_whitespace_breaks(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "nanopore_bad_name.xlsx"
        with pytest.raises(ValueError, match = "The run name contains spaces or line breaks."):
            helpers.extract_nanopore_run_name(test_sheet)

    def test_check_illegal_in_windows(self):
        test_control_char = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                            / "nanopore_controlchar.xlsx"
        test_bad_char = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "nanopore_badchar.xlsx"
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used "
                                   "in Windows filenames."):
            helpers.extract_nanopore_run_name(test_control_char)
        with pytest.raises(ValueError,
                           match = "The run name contains a character that cannot be used"
                                   " in Windows filenames."):
            helpers.extract_nanopore_run_name(test_bad_char)

    def test_slash_breaks(self):
        test_with_slash = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "nanopore_slash.xlsx"
        error_msg = "The run name contains a forward slash (/). " \
                    "This will break the result directory." \
                    " Replace forward slashes with underscores (_)."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.extract_nanopore_run_name(test_with_slash)


class TestCheckForSamples(unittest.TestCase):
    def test_check_one_missing(self):
        samples_missing = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "rawdata_missing"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        expected_error = f"R1 not found in {str(samples_missing)} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(expected_error)):
            helpers.check_for_samples(samples_missing, sample_numbers)

    def test_multiple_missing(self):
        samples_missing = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "rawdata_missing"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1", "B99654321-1"]})
        expected_error = f"R1 not found in {str(samples_missing)} " \
                         f"for samples ['B99123456-1', 'B99654321-1']. \n" \
                         f"R2 not found in {str(samples_missing)} for samples ['B99654321-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(expected_error)):
            helpers.check_for_samples(samples_missing, sample_numbers)

    def test_handle_translation(self):
        """Use non-default column names when checking sample numbers."""
        samples_missing = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "rawdata_missing"
        sample_numbers = pd.DataFrame(data = {"Sample_number": ["B99123456-1"]})

        expected_error = f"R1 not found in {str(samples_missing)} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(expected_error)):
            helpers.check_for_samples(samples_missing, sample_numbers,
                                      runsheet_names = SHEET_NAMES_EN)


class TestFindBarcodeDirs(unittest.TestCase):
    test_runsheet = pathlib.Path(
        __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
    test_data = pd.read_excel(test_runsheet, usecols = "A:C", skiprows = 3,
                              dtype = {"KMA nr": str, "Barkode": str},
                              sheet_name = "Runsheet_Nanopore")
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog
    def test_success_find_barcodes(self):
        test_path = pathlib.Path(__file__).parent / "data" / "snake_helpers" \
                    / "rawdata" / "subdir" / "fastq_pass"
        with self._caplog.at_level(level="DEBUG", logger="helpers"):
            log_msg = f"Found all barcodes in {test_path}."
            helpers.check_nanopore_dirs(test_path, self.test_data)
        assert ("helpers", logging.DEBUG, log_msg) in self._caplog.record_tuples

    def test_catch_fail(self):
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / \
                    "test_dir_multi_pass" / "rawdata" / "missing_barcodes" / "fastq_pass"
        error_msg = "Barcode directories ['barcode01', 'barcode42'] " \
                    f"are missing from {test_path}."
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_msg)):
            helpers.check_nanopore_dirs(test_path, self.test_data)

    def test_success_translate_columns(self):
        """Handle non-default column names."""
        test_data = self.test_data.rename(columns = {"KMA nr": "Sample_number",
                                                     "Barkode": "Barcode"})

        test_path = pathlib.Path(__file__).parent / "data" / "snake_helpers" \
                    / "rawdata" / "subdir" / "fastq_pass"
        with self._caplog.at_level(level="DEBUG", logger="helpers"):
            log_msg = f"Found all barcodes in {test_path}."
            helpers.check_nanopore_dirs(test_path, test_data, runsheet_names = SHEET_NAMES_EN)
        assert ("helpers", logging.DEBUG, log_msg) in self._caplog.record_tuples


class TestFindNanoporePath(unittest.TestCase):
    test_runsheet = pathlib.Path(
        __file__).parent / "data" / "utilities_test" / "test_nanopore_runsheet.xlsx"
    test_data = pd.read_excel(test_runsheet, usecols = "A:C", skiprows = 3,
                              dtype = {"KMA nr": str, "Barkode": str},
                              sheet_name = "Runsheet_Nanopore")
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_find_absolute_path_success(self):
        """Find the parent directory of a specified fastq pass dir."""
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                    / "test_dir_multi_pass" / "rawdata" / "subdir" / "fastq_pass"
        true_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                    / "test_dir_multi_pass" / "rawdata" / "subdir"
        with self._caplog.at_level(level="INFO", logger="helpers"):
            log_msg = f"Data is retrieved from the following folder:\n" \
                      f"{true_path}"
            test_fastq = helpers.get_fastq_pass_parent(test_path, self.test_data)
            assert ("helpers", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_fastq == true_path

    def test_return_fastq_when_given(self):
        """Find the parent directory of a fastq pass directory when the base dir is given."""
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        true_path = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir" /
                     "test1" / "rawdata" / "test_subdir")
        with self._caplog.at_level(level="INFO", logger="helpers"):
            log_msg = f"Data is retrieved from the following folder:\n" \
                      f"{true_path}"
            test_fastq = helpers.get_fastq_pass_parent(test_path, self.test_data)
            assert ("helpers", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_fastq == true_path

    def test_fail_path(self):
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "subdir"
        error_msg =(f"fastq_pass folder not found in {test_path} "
                    "or any subfolders. \n"
                    "Ensure correct directory and/or directory structure is used.\n"
                    "Aborting isolate pipeline...")
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)), \
                 self._caplog.at_level(level="INFO", logger="helpers"):
            log_msg = f"Searching for fastq_pass folder in {test_path}..."
            helpers.get_fastq_pass_parent(test_path, self.test_data)
        assert ("helpers", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_fastq_fail_path(self):
        test_path = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "subdir_3" / "fastq_fail"
        error_msg = (f"fastq_pass folder not found in {test_path} "
                     "or any subfolders. \n"
                     "Ensure correct directory and/or directory structure is used.\n"
                     "Aborting isolate pipeline...")
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)), \
                 self._caplog.at_level(level="INFO", logger="helpers"):
            log_msg = f"Searching for fastq_pass folder in {test_path}..."
            helpers.get_fastq_pass_parent(test_path, self.test_data)
        assert ("helpers", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_fail_multiple_fastq_dirs(self):
        test_path = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
        error_msg = f"The directory {test_path} contains " \
                    f"multiple fastq_pass directories." \
                    "Please choose the one containing the fastq files you want to analyze and " \
                    "specify the entire path to the fastq_pass directory."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.get_fastq_pass_parent(test_path, self.test_data)

    def test_warn_missing_barcode_dirs(self):
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / \
                    "test_dir_multi_pass" / "rawdata" / "subdir_3" / "fastq_pass"
        warn_msg = ("Barcode directories ['barcode01', 'barcode42'] "
                    f"are missing from {test_path}.\n"
                    "This may be due to a delay in copying files from the sequencer, but could"
                    " also mean you have given the wrong path. \n"
                    "Only continue if you are sure. ")
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            helpers.get_fastq_pass_parent(test_path, self.test_data)
            assert ("helpers", logging.WARNING, warn_msg) in self._caplog.record_tuples


class TestTranslateSampleNumbers(unittest.TestCase):
    number_to_letter = {"40": "H", "70": "P"}

    def test_wrong_sample_in(self):
        with pytest.raises(ValueError,
                           match = "Invalid initial sample format int. Sample format can only be number or letter"):
            helpers.get_number_letter_combination(self.number_to_letter, "int", "letter")

    def test_wrong_sample_out(self):
        with pytest.raises(ValueError,
                           match = "Invalid desired sample format str. Sample format can only be number or letter"):
            helpers.get_number_letter_combination(self.number_to_letter, "number", "str")

    def test_correct_results(self):
        number_to_number = helpers.get_number_letter_combination(self.number_to_letter, "number",
                                                                 "number")
        number_to_letter = helpers.get_number_letter_combination(self.number_to_letter, "number",
                                                                 "letter")
        letter_to_letter = helpers.get_number_letter_combination(self.number_to_letter, "letter",
                                                                 "letter")
        letter_to_number = helpers.get_number_letter_combination(self.number_to_letter, "letter",
                                                                 "number")
        self.assertEqual(number_to_number, {"40": "40", "70": "70"})
        self.assertEqual(number_to_letter, self.number_to_letter)
        self.assertEqual(letter_to_letter, {"H": "H", "P": "P"})
        self.assertEqual(letter_to_number, {"H": "40", "P": "70"})


class TestTranslateSampleCategory(unittest.TestCase):
    sample_format = re.compile(
        "(?P<sample_type>[135]0|11|[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)")
    category_mapping = {"30": "B", "10": "D", "11": "F", "50": "T"}

    def test_fail_no_type(self):
        """Fail if sample type is not given in the format regex."""
        input_number = "F99123456-1"
        sample_format = re.compile(
            "(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)")
        error_msg = ("No sample type specified in sample number format"
                     " (?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d). "
                     "Please specify sample type as a match group named sample_type.")
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            helpers.translate_sample_category(input_number, sample_format, "letter", "letter",
                                              self.category_mapping)

    def test_fail_no_hit_in_number(self):
        """Fail if the sample number does not match the format regex."""
        input_number = "X99123456-1"
        error_msg = "Sample number X99123456-1 does not match the specified format."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            helpers.translate_sample_category(input_number, self.sample_format, "letter", "letter",
                                              self.category_mapping)

    def test_no_change(self):
        """Leave the sample number unchanged if no change is required."""
        input_number = "F99123456-1"

        test_number = helpers.translate_sample_category(input_number, self.sample_format, "letter",
                                                        "letter", self.category_mapping)
        assert test_number == input_number

    def test_letter_to_number(self):
        """Translate sample category letter to number."""
        input_number = "F99123456-1"
        true_number = "1199123456-1"
        test_number = helpers.translate_sample_category(input_number, self.sample_format, "letter",
                                                        "number", self.category_mapping)
        assert test_number == true_number

    def test_number_to_letter(self):
        """Translate sample category number to letter."""
        input_number = "1199123456-1"
        true_number = "F99123456-1"
        test_number = helpers.translate_sample_category(input_number, self.sample_format, "number",
                                                        "letter", self.category_mapping)
        assert test_number == true_number


class TestRearrangeSampleNumber(unittest.TestCase):
    def test_no_change(self):
        input_number = "F99123456-1"
        true_number = "F99123456-1"
        pattern_in = re.compile(r"(?P<sample_type>[BDFT])"
                                r"(?P<sample_year>\d{2})"
                                r"(?P<sample_number>\d{6})"
                                r"(?P<bact_number>-\d)")
        order_out = {"sample_type": 1, "sample_year": 2, "sample_number": 3, "bact_number": 4}
        test_number = helpers.rearrange_sample_number(input_number, pattern_in, order_out)
        assert test_number == true_number

    def test_rearrange_success(self):
        input_number = "F99123456-1"
        true_number = "F12345699-1"
        pattern_in = re.compile(r"(?P<sample_type>[BDFT])"
                                r"(?P<sample_year>\d{2})"
                                r"(?P<sample_number>\d{6})"
                                r"(?P<bact_number>-\d)")
        order_out = {"sample_type": 1, "sample_year": 3, "sample_number": 2, "bact_number": 4}
        test_number = helpers.rearrange_sample_number(input_number, pattern_in, order_out)
        assert test_number == true_number

    def test_fail_too_many_components(self):
        input_number = "F99123456-1"
        pattern_in = re.compile(r"(?P<sample_type>[BDFT])"
                                r"(?P<sample_number>\d{6})"
                                r"(?P<bact_number>-\d)")
        order_out = {"sample_type": 1, "sample_year": 2, "sample_number": 3, "bact_number": 4}
        error_msg = "Not all desired sample number components could be " \
                    "found in the original format. " \
                    "Desired sample number format contains additional " \
                    "components {'sample_year'}."
        with pytest.raises(KeyError, match = error_msg):
            helpers.rearrange_sample_number(input_number, pattern_in, order_out)

    def test_fail_no_hits(self):
        input_number = "1199123456-1"
        pattern_in = re.compile(r"(?P<sample_type>[BDFT])"
                                r"(?P<sample_year>\d{2})"
                                r"(?P<sample_number>\d{6})"
                                r"(?P<bact_number>-\d)")
        order_out = {"sample_type": 1, "sample_year": 2, "sample_number": 3, "bact_number": 4}
        error_msg = "No match in sample number 1199123456-1."
        with pytest.raises(ValueError, match = error_msg):
            helpers.rearrange_sample_number(input_number, pattern_in, order_out)

    def test_success_remove_component(self):
        input_number = "F99123456-1"
        true_number = "F123456-1"
        pattern_in = re.compile(r"(?P<sample_type>[BDFT])"
                                r"(?P<sample_year>\d{2})"
                                r"(?P<sample_number>\d{6})"
                                r"(?P<bact_number>-\d)")
        order_out = {"sample_type": 1, "sample_number": 2, "bact_number": 3}
        test_number = helpers.rearrange_sample_number(input_number, pattern_in, order_out)
        assert test_number == true_number


class TestTranslateBacteriaCodes(unittest.TestCase):
    def test_translate_code_success(self):
        translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                            "bakterie_kategori": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        true_species = "Fakeococcus fakei"
        test_species = helpers.get_species_by_code("1", translations)
        assert test_species == true_species

    def test_translate_code_success_en(self):
        """Translate species with non-default column names"""
        translations = pd.DataFrame(data = {"bacteria_code": ["1", "2"],
                                            "bacteria_category": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        true_species = "Fakeococcus fakei"
        test_species = helpers.get_species_by_code("1", translations,
                                                   bacteria_names = BACT_NAMES_EN)
        assert test_species == true_species

    def test_no_hit(self):
        translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                            "bakterie_kategori": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        test_species = helpers.get_species_by_code("3", translations)
        assert pd.isna(test_species)


class TestGetSpeciesFromLIS(unittest.TestCase):
    lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                    "LOKAL_BAKT_KODE": ["1", "3"]})
    translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                        "bakterie_kategori": ["Fakeococcus fakei",
                                                              "Placeholderia fakeorum"]})

    def test_translate_success(self):
        true_species = "Fakeococcus fakei"
        test_species = helpers.get_species_by_number("1199123456-1", self.lis_data,
                                                     self.translations)
        assert test_species == true_species

    def test_translate_success_en(self):
        """Translate sample number to species using LIS and bacteria mapping with
        non-default column names."""
        true_species = "Fakeococcus fakei"
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOCAL_BACT_CODE": ["1", "3"]})
        translations = pd.DataFrame(data = {"bacteria_code": ["1", "2"],
                                            "bacteria_category": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        test_species = helpers.get_species_by_number("1199123456-1", lis_data,
                                                     translations, lis_data_names = LIS_NAMES_EN,
                                                     bacteria_names = BACT_NAMES_EN)
        assert test_species == true_species

    def test_translate_no_such_code(self):
        test_species = helpers.get_species_by_number("1199123456-2", self.lis_data,
                                                     self.translations)
        assert pd.isna(test_species)

    def test_fail_wrong_number(self):
        error_msg = "Sample number 1199123456-3 not found in LIS report."
        with pytest.raises(KeyError, match = error_msg):
            helpers.get_species_by_number("1199123456-3", self.lis_data,
                                          self.translations)


class TestCheckMissingCodes(unittest.TestCase):
    def test_no_missing_codes(self):
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOKAL_BAKT_KODE": ["1", "2"]})
        translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                            "bakterie_kategori": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        samples = pd.DataFrame(data = {'Prøvenr': ["1199123456-1", "1199123456-2"]})
        test_result = helpers.find_missing_species(samples, lis_data, translations)
        assert test_result.empty

    def test_find_missing_codes(self):
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOKAL_BAKT_KODE": ["1", "3"]})
        translations = pd.DataFrame(data = {"BAKTERIE": ["1", "2"],
                                            "bakterie_kategori": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        samples = pd.DataFrame(data = {'Prøvenr': ["1199123456-1", "1199123456-2"]})
        test_result = helpers.find_missing_species(samples, lis_data, translations)
        expected_result = pd.DataFrame(data = {"Prøvenr": ["1199123456-2"],
                                               "LOKAL_BAKT_KODE": ["3"]})
        pd.testing.assert_frame_equal(expected_result, test_result)

    def test_find_missing_translated(self):
        """Use non-default column names."""
        lis_data = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                        "LOCAL_BACT_CODE": ["1", "3"]})
        translations = pd.DataFrame(data = {"bacteria_code": ["1", "2"],
                                            "bacteria_category": ["Fakeococcus fakei",
                                                                  "Placeholderia fakeorum"]})
        samples = pd.DataFrame(data = {'Sample_number': ["1199123456-1", "1199123456-2"]})
        test_result = helpers.find_missing_species(samples, lis_data, translations,
                                                   runsheet_names = SHEET_NAMES_EN,
                                                   lis_data_names = LIS_NAMES_EN,
                                                   bacteria_names = BACT_NAMES_EN)
        expected_result = pd.DataFrame(data = {"Sample_number": ["1199123456-2"],
                                               "LOCAL_BACT_CODE": ["3"]})
        pd.testing.assert_frame_equal(expected_result, test_result)



class TestPivotDataframe(unittest.TestCase):
    def test_pivot_success(self):
        input_df = pd.DataFrame(data = {"proevenr": ["O_WID", "O_WGS Serotype/gruppe"],
                                        "1199123456-1": ["Fakeococcus fakei", "FakeSerotype"],
                                        "1199654321-1": ["Placeholderia fakeorum", ""]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199654321-1"],
                                           "O_WID": ["Fakeococcus fakei", "Placeholderia fakeorum"],
                                           "O_WGS Serotype/gruppe": ["FakeSerotype", ""]})
        test_result = helpers.pivot_wide_to_long(input_df)
        pd.testing.assert_frame_equal(true_result, test_result)


class TestConstructExperimentName(unittest.TestCase):
    def test_fail_no_sheets(self):
        """Complain if neither runsheet is given."""
        error_msg = "No Illumina or Nanopore sheet given. At least one runsheet must be provided."
        with pytest.raises(ValueError, match=error_msg):
            helpers.construct_experiment_name(illumina_sheet = None, nanopore_sheet = None)

    def test_success_illumina_sheet(self):
        """Successfully extract experiment name from Illumina runsheet."""
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "runsheet_experiment_name.xlsx"
        true_run_name = "ILM_Run0000_Y20220101_XYZ"
        test_run_name = helpers.construct_experiment_name(illumina_sheet = test_sheet)
        assert test_run_name == true_run_name

    def test_success_nanopore_sheet(self):
        """Successfully extract experiment name from Nanopore runsheet."""
        test_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "test_nanopore_runsheet.xlsx"
        true_run_name = "ONT_RUN0000_Y20990101_XYZ"
        test_run_name = helpers.construct_experiment_name(nanopore_sheet = test_sheet)
        assert test_run_name == true_run_name

    def test_success_hybrid_name(self):
        """Successfully construct experiment name for hybrid run."""
        illumina_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "runsheet_experiment_name.xlsx"
        nanopore_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                     / "test_nanopore_runsheet.xlsx"
        true_run_name = "ILM_Run0000_Y20220101_XYZ_ONT_RUN0000_Y20990101_XYZ"
        test_run_name = helpers.construct_experiment_name(illumina_sheet = illumina_sheet,
                                                          nanopore_sheet = nanopore_sheet)
        assert test_run_name == true_run_name

    def test_success_hybrid_translate(self):
        """Construct experiment name for a hybrid run where runsheets have non-default column
        and sheet names."""
        illumina_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                         / "success_sheet_en.xlsx"
        nanopore_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                         / "test_nanopore_runsheet_en.xlsx"
        true_run_name = "ILM_Run0000_Y20220101_XYZ_ONT_RUN0000_Y20990101_XYZ"
        test_run_name = helpers.construct_experiment_name(illumina_sheet = illumina_sheet,
                                                          nanopore_sheet = nanopore_sheet,
                                                          runsheet_names = SHEET_NAMES_EN)
        assert test_run_name == true_run_name


class TestCleanGTDBName(unittest.TestCase):
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_warn_no_name(self):
        """Warn if no GTDB name was given."""
        no_name_log = "No GTDB species result given."
        empty_name = ""
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            test_empty_name = helpers.clean_gtdb_name(empty_name)
            assert ("helpers", logging.WARNING, no_name_log) in self._caplog.record_tuples
            assert test_empty_name == empty_name
        none_name = None
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            self._caplog.clear()
            test_none_name = helpers.clean_gtdb_name(none_name)
            assert ("helpers", logging.WARNING, no_name_log) in self._caplog.record_tuples
            assert test_none_name == empty_name

        na_name = pd.NA
        with self._caplog.at_level(level="WARNING", logger="helpers"):
            self._caplog.clear()
            test_na_name = helpers.clean_gtdb_name(na_name)
            assert ("helpers", logging.WARNING, no_name_log) in self._caplog.record_tuples
            assert test_na_name == empty_name

    def test_success_no_change(self):
        """Don't change the species name if there is no placeholder."""
        expected_name = "Fakeococcus fakei"
        test_name = helpers.clean_gtdb_name(expected_name)
        assert test_name == expected_name

    def test_success_remove_final_placeholder(self):
        """Remove placeholders at the end of the name."""
        expected_name = "Fakeococcus fakei"
        short_final = "Fakeococcus fakei_A"
        test_short_final = helpers.clean_gtdb_name(short_final)
        assert test_short_final == expected_name
        long_final = "Fakeococcus fakei_ABC"
        test_long_final = helpers.clean_gtdb_name(long_final)
        assert test_long_final == expected_name

    def test_success_remove_mid_placeholder(self):
        """Remove placeholders anywhere else in the name."""
        expected_name = "Fakeococcus fakei"
        short_mid = "Fakeococcus_A fakei"
        test_short_mid = helpers.clean_gtdb_name(short_mid)
        assert test_short_mid == expected_name
        long_mid = "Fakeococcus_ABC fakei"
        test_long_mid = helpers.clean_gtdb_name(long_mid)
        assert test_long_mid == expected_name

