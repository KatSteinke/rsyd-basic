#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import shutil
import time
import unittest
from datetime import timedelta

from argparse import ArgumentParser, Namespace
from unittest import mock

import pytest
import pandas as pd

import helpers
import monitor_run
import run_pipeline

from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames

# set up some translations we're going to be using over and over
SHEET_NAMES_EN = RunsheetNames(sample_info_sheet = "Sample_information",
                               sample_number = "Sample_number",
                               extraction_well_number = "Extraction_well_number",
                               extraction_run_number = "Extraction_run_number",
                               dna_concentration = "DNA_concentration",
                               requested_by = "Requested_by",
                               comment = "Comment",
                               sequencing_run_number = "sequencing_run_number",
                               indications = ["AMR", "Identification", "Relapse", "HAI",
                                              "Toxin gene identification",
                                              "Environmental sample isolate",
                                              "Outbreak sample isolate",
                                              "Surveillance sample isolate", "Research",
                                              "Other"],
                               dna_normalization_sheet = "DNA normalization",
                               dna_normalization_sample_nr = "Sample_number",
                               ilm_well_number = "ILM_well_position",
                               plate_layout = "Plate_layout",
                               runsheet_base = "Runsheet",
                               experiment_name = "RUNxxxx-INI",
                               nanopore_sheet_sample_number = "Sample_number",
                               barcode = "Barcode",
                               protocol_version = "Lab protocol version")
LIS_NAMES_EN = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                            isolate_number = "BACT_NR",
                            bacteria_code = "LOCAL_BACT_CODE",
                            bacteria_text = "LOCAL_BACT_TEXT")
BACT_NAMES_EN = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                     internal_bacteria_text = "bacteria_text",
                                     bacteria_category = "bacteria_category")


class TestFindExistingPath(unittest.TestCase):
    def test_find_all_exists(self):
        true_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2"\
                        / "rawdata"
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2"\
                        / "rawdata"
        existing_path = run_pipeline.get_existing_path(test_path)
        assert existing_path == true_path

    def test_find_partial(self):
        true_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_4"
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_4" \
                    / "rawdata"
        existing_path = run_pipeline.get_existing_path(test_path)
        assert existing_path == true_path


class TestSanitizePath(unittest.TestCase):
    def test_entire_path_exists_success(self):
        true_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2" \
                    / "rawdata"
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2" \
                    / "rawdata"
        existing_path = run_pipeline.get_clean_outdir(test_path)
        assert existing_path == true_path

    def test_space_in_existing(self):
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test dir spaces"
        with pytest.raises(helpers.BadPathError,
                           match="The path you are trying to save results to contains"
                                 " a space in an existing folder's name. "
                                 "This can break the pipeline. "
                                 "\nAborting...."):
            run_pipeline.get_clean_outdir(test_path)

    @mock.patch(f'{run_pipeline.__name__}.get_existing_path')
    def test_illegal_char_in_existing(self, mock_get_existing):
        mock_get_existing.return_value = "does_this_fail?"
        error_msg = "The path you are trying to save results to contains a character that " \
                    "can't be used in Windows in an existing folder's name. " \
                    "This can break the pipeline. " \
                    "\nAborting...."
        test_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2" \
                    / "rawdata"
        with pytest.raises(helpers.BadPathError, match = error_msg):
            run_pipeline.get_clean_outdir(test_path)

    def test_reserved_name(self):
        plain_reserved = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_2" \
                         / "rawdata" / "NUL"
        reserved_after_cleaning = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                                  / "test_dir_2" / "rawdata" / "N*UL"
        with pytest.raises(helpers.BadPathError,
                           match = "The path you are trying to save results to contains "
                                   "a name that is reserved in Windows. "
                                   "Cannot create this path. \n"
                                   "Aborting...."):
            run_pipeline.get_clean_outdir(plain_reserved)
        with pytest.raises(helpers.BadPathError,
                           match = "The path you are trying to save results to contains "
                                   "a name that is reserved in Windows. "
                                   "Cannot create this path. \n"
                                   "Aborting...."):
            run_pipeline.get_clean_outdir(reserved_after_cleaning)

    def test_strip_illegal_chars(self):
        messy_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir*5"
        cleaned_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir5"
        test_path = run_pipeline.get_clean_outdir(messy_path)
        assert test_path == cleaned_path

    def test_strip_spaces(self):
        messy_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir 5"
        cleaned_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_5"
        test_path = run_pipeline.get_clean_outdir(messy_path)
        assert test_path == cleaned_path

    def test_new_path_success(self):
        clean_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_5"
        test_path = run_pipeline.get_clean_outdir(clean_path)
        assert test_path == clean_path


class TestGetIlluminaPath(unittest.TestCase):
    def test_good_path(self):
        finished_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "rawdata_correct"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        test_path = run_pipeline.find_illumina_path(finished_path, sample_numbers,
                                                    sequencer = "MiSeq")
        assert test_path == finished_path

    def test_good_path_translate(self):
        """Find an existing path when the sheet data has non-default column names."""
        finished_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "rawdata_correct"
        sample_numbers = pd.DataFrame(data = {"Sample_number": ["B99123456-1"]})
        test_path = run_pipeline.find_illumina_path(finished_path, sample_numbers,
                                                    sequencer = "MiSeq",
                                                    runsheet_names = SHEET_NAMES_EN)
        assert test_path == finished_path

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_find_fastq_dir_miseq(self):
        initial_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_hit"
        finished_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "fastq_hit" / "test" / "Alignment_1" / "moretest" / "Fastq"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        with self._caplog.at_level(level="WARNING", logger="run_pipeline"):
            test_path = run_pipeline.find_illumina_path(initial_path, sample_numbers,
                                                        sequencer = "MiSeq")
            warn_msg = f"No samples found in {initial_path}. " \
                       "Treating this as a base directory: " \
                       "looking for samples in */Alignment_*/*/Fastq."
            assert ("run_pipeline", logging.WARNING, warn_msg) in self._caplog.record_tuples
        assert test_path == finished_path

    def test_find_fastq_dir_nextseq(self):
        initial_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"
        finished_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "fastq_nextseq" / "test" / "Analysis" / "1" / "Data" / "fastq"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        with self._caplog.at_level(level="WARNING", logger="run_pipeline"):
            test_path = run_pipeline.find_illumina_path(initial_path, sample_numbers,
                                                        sequencer = "NextSeq")
            warn_msg = f"No samples found in {initial_path}. " \
                       "Treating this as a base directory: " \
                       "looking for samples in */Analysis/*/Data/fastq."
            assert ("run_pipeline", logging.WARNING, warn_msg) in self._caplog.record_tuples
        assert test_path == finished_path

    def test_invalid_sequencer(self):
        finished_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "rawdata_correct"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_msg = "Cannot look for Illumina reads on sequencing platform GridION."
        with pytest.raises(ValueError, match = error_msg):
            run_pipeline.find_illumina_path(finished_path, sample_numbers,
                                            sequencer = "GridION")

    def test_empty_dir(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                        / "test_dir_4"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = f"No Alignment directory found in {bad_path}. \n" \
                        f"R1 not found in {bad_path} for samples ['B99123456-1']. \n" \
                        f"R2 not found in {bad_path} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match=re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "MiSeq")

    def test_empty_dir_nextseq(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_dir_4"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = f"No Analysis directory found in {bad_path}. \n" \
                        f"R1 not found in {bad_path} for samples ['B99123456-1']. \n" \
                        f"R2 not found in {bad_path} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "NextSeq")

    def test_multiple_aligns(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "multiple_aligns"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = "The directory you have specified contains multiple Alignment directories." \
                        "Please choose the one containing the fastq files you want to analyze and" \
                        "specify the entire path to the directory with the fastq files."
        with pytest.raises(ValueError, match=re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "MiSeq")

    def test_multiple_analysis_dirs(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "multiple_analyses"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = "The directory you have specified contains multiple Analysis directories." \
                        "Please choose the one containing the fastq files you want to analyze and" \
                        "specify the entire path to the directory with the fastq files."
        with pytest.raises(ValueError, match=re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "NextSeq")

    def test_no_fastq_miseq(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "no_fasta"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = f"No fastq directory found in {bad_path / 'data' / 'Alignment_1'}. \n" \
                        f"R1 not found in {bad_path} for samples ['B99123456-1']. \n" \
                        f"R2 not found in {bad_path} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "MiSeq")

    def test_partial_hit(self):
        samples_missing = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "rawdata_missing"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        expected_error = f"R1 not found in {samples_missing} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match=re.escape(expected_error)):
            run_pipeline.find_illumina_path(samples_missing, sample_numbers, sequencer = "MiSeq")

    def test_partial_hit_translate(self):
        """Handle partially missing data with non-default column names in the sample sheet."""
        samples_missing = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                          / "rawdata_missing"
        sample_numbers = pd.DataFrame(data = {"Sample_number": ["B99123456-1"]})
        expected_error = f"R1 not found in {samples_missing} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match=re.escape(expected_error)):
            run_pipeline.find_illumina_path(samples_missing, sample_numbers, sequencer = "MiSeq",
                                            runsheet_names = SHEET_NAMES_EN)

    def test_no_fastq_nextseq(self):
        bad_path = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "no_fastq_nextseq"
        sample_numbers = pd.DataFrame(data = {"Prøvenr": ["B99123456-1"]})
        error_message = f"No fastq directory found in {bad_path / 'test' / 'Analysis' / '1' / 'Data'}. \n" \
                        f"R1 not found in {bad_path} for samples ['B99123456-1']. \n" \
                        f"R2 not found in {bad_path} for samples ['B99123456-1']. \n"
        with pytest.raises(helpers.MissingReadfilesError, match = re.escape(error_message)):
            run_pipeline.find_illumina_path(bad_path, sample_numbers, sequencer = "NextSeq")


class TestAskOutputPath(unittest.TestCase):
    default_path = pathlib.Path("data/test_run")

    @mock.patch("builtins.input")
    def test_fail_invalid_accept(self, mock_input):
        """Fail on an invalid response for whether to accept the default output path."""
        mock_input.return_value = "nope"
        error_msg = "Output folder not entered. Aborting"
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            run_pipeline.ask_output_dir(self.default_path)

    # to check issues in the existing path, we need to pretend we've got a broken path
    # decorators are applied bottom up: https://stackoverflow.com/a/15922422/15704972
    @mock.patch(f'{run_pipeline.__name__}.get_existing_path')
    @mock.patch("builtins.input", side_effect=["n", # user rejects path
                                               "/data/test:run"])  # user suggests new
    def test_fail_bad_user_path(self, mock_input, mock_path):
        """Fail when the user suggests a new path and it's invalid."""
        mock_path.return_value = "/data/test:run"
        error_msg = ("The path you are trying to save results to contains a character that "
                           "can't be used in Windows in an existing folder's name. "
                           "This can break the pipeline. "
                           "\nAborting....")
        with pytest.raises(run_pipeline.BadPathError, match=re.escape(error_msg)):
            run_pipeline.ask_output_dir(self.default_path)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    @mock.patch(f'{run_pipeline.__name__}.get_existing_path')
    @mock.patch("builtins.input", side_effect=["/data/test:run/run2"])  # user needs to give a new path because the old one is broken
    def test_fail_bad_corrected_path(self, mock_input, mock_path):
        """Fail when the original path is invalid and the user's correction is as well."""
        mock_path.return_value = "/data/test:run"
        default_path = pathlib.Path("/data/test:run")
        error_msg = ("The path you are trying to save results to contains a character that "
                           "can't be used in Windows in an existing folder's name. "
                           "This can break the pipeline. "
                           "\nAborting....")
        log_msg = ("The default target folder contains characters "
                   "that can break the pipeline.")
        with (pytest.raises(run_pipeline.BadPathError,
                            match = re.escape(error_msg)),
              self._caplog.at_level(level = "WARNING", logger = "run_pipeline")):
            run_pipeline.ask_output_dir(default_path)
        assert ("run_pipeline", logging.WARNING, log_msg) in self._caplog.record_tuples

    @mock.patch("builtins.input")
    def test_success_valid_default(self, mock_input):
        """Return the default output path when the user accepts it."""
        mock_input.return_value = "y"
        expected_path = self.default_path
        log_msg = f"Saving results to {self.default_path}"
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_path = run_pipeline.ask_output_dir(self.default_path)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_path == test_path

    @mock.patch("builtins.input", side_effect = ["n",  # user rejects the path
                                                 "/data/test_run2"])  # ...and gives a new one
    def test_success_valid_user_path(self, mock_input):
        """Return the user's new path if it's valid."""
        expected_path = pathlib.Path("/data/test_run2")
        log_msg = f"Saving results to {expected_path}"
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_path = run_pipeline.ask_output_dir(self.default_path)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_path == test_path

    @mock.patch("builtins.input", side_effect = ["/data/test_run2"])  # user needs to give a new path because the old one is broken
    def test_success_valid_correction(self, mock_input):
        """Ask the user for a new path and return it if the original path is invalid but the
        user's correction fixes it."""
        default_path = pathlib.Path(__file__).parent / "data" / "utilities_test" / "test dir spaces"
        expected_path = pathlib.Path("/data/test_run2")
        log_msg = ("The default target folder contains characters "
                   "that can break the pipeline.")
        success_msg = f"Saving results to {expected_path}"
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_path = run_pipeline.ask_output_dir(default_path)
            assert ("run_pipeline", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert ("run_pipeline", logging.INFO, success_msg) in self._caplog.record_tuples
        assert expected_path == test_path

    @mock.patch("builtins.input")
    def test_success_correct_fixable_default(self, mock_input):
        """Correct a fixable bad path from default input."""
        mock_input.return_value = "y"
        default_path = pathlib.Path("data/test run")
        expected_path = self.default_path
        log_msg = f"Saving results to {expected_path}"
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_path = run_pipeline.ask_output_dir(default_path)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_path == test_path

    @mock.patch("builtins.input", side_effect = ["n",  # user rejects the path
                                                 "/data/test run2"])  # ...and gives a new one
    def test_success_correct_fixable_user(self, mock_input):
        """Correct a fixable bad path from user input."""
        expected_path = pathlib.Path("/data/test_run2")
        log_msg = f"Saving results to {expected_path}"
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_path = run_pipeline.ask_output_dir(self.default_path)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_path == test_path


class TestInitializeIlluminaRun(unittest.TestCase):
    test_configfile = pathlib.Path("/path/to/config.yaml")
    test_config = {"debug": True,
                   "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'letter',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "/path/to/output"},
                   "sequencing_mode": "illumina",
                   "platform": "NextSeq",
                   "input_names": (pathlib.Path(__file__).parent / "data"
                                   / "localization" / "input_da.yaml")
                   }

    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "runsheet_singleindication_multisample.xlsx"
    expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                            "runtype": ["paired-end", "paired-end"],
                                            "r1": [(pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-1_L001_R1_001.fastq.gz"),
                                                   (pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-2_L001_R1_001.fastq.gz")
                                                   ],
                                            "r2": [(pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-1_L001_R2_001.fastq.gz"),
                                                   (pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-2_L001_R2_001.fastq.gz")
                                                   ],
                                            "extra": ["", ""]
                                            })
    expected_fofn = (pathlib.Path(test_config["paths"]["output_base_path"])
                     / "ILM_Run0000_Y20220101_XYZ" / "ILM_Run0000_Y20220101_XYZ.tsv")

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    @mock.patch("builtins.input")
    def test_success_exact_indir(self, mock_input):
        """Successfully initialize an IsolateRun from user input
        when the input directory is given explicitly."""
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq" \
                 / "test" / "Analysis" / "1" / "Data" / "fastq"
        mock_input.side_effect = [str(rundir), str(self.runsheet)]

        expected_run = monitor_run.IsolateRun(rundir, self.runsheet, self.expected_samples,
                                              self.test_configfile, self.test_config)
        log_msg = "Wetlab version is Version_1.2.3."
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_illumina_run(self.test_config, self.test_configfile)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_success_exact_indir_en(self, mock_input):
        """Successfully initialize an IsolateRun from user input
        when the input directory is given explicitly, using non-default column names."""
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq" \
                 / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample_en.xlsx"
        mock_input.side_effect = [str(rundir), str(runsheet)]
        test_config = {"debug": False,
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "lab_info_system": {"use_lis_features": False,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep_en.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep_en.csv")},
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "platform": "NextSeq",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_en.yaml")
                       }

        expected_run = monitor_run.IsolateRun(rundir, runsheet, self.expected_samples,
                                              self.test_configfile, test_config)
        log_msg = "Wetlab version is Version_1.2.3."
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_illumina_run(test_config, self.test_configfile)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_success_infer_indir(self, mock_input):
        """Successfully initialize an IsolateRun from user input
         when input directory needs to be found."""
        input_rundir =  pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq"
        expected_rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq" \
                 / "test" / "Analysis" / "1" / "Data" / "fastq"
        mock_input.side_effect = [str(input_rundir), str(self.runsheet)]

        expected_run = monitor_run.IsolateRun(expected_rundir, self.runsheet, self.expected_samples,
                                              self.test_configfile, self.test_config)

        test_run = run_pipeline.initialize_illumina_run(self.test_config, self.test_configfile)
        assert test_run == expected_run


class TestInitializeNanoporeRun(unittest.TestCase):
    test_configfile = pathlib.Path("/path/to/config.yaml")
    test_config = {"debug": True,
                   "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'letter',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "/path/to/output"},
                   "sequencing_mode": "nanopore",
                   "platform": "GridION",
                   "input_names": (pathlib.Path(__file__).parent / "data"
                                   / "localization" / "input_da.yaml")
                   }

    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "test_nanopore_runsheet.xlsx"

    expected_fofn = (pathlib.Path(test_config["paths"]["output_base_path"])
                     / "ONT_RUN0000_Y20990101_XYZ" / "ONT_RUN0000_Y20990101_XYZ.tsv")

    @mock.patch("builtins.input")
    def test_success_exact_indir(self, mock_input):
        """Successfully initialize an IsolateRun from user input
        when the input directory is given explicitly."""
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [rundir / "fastq_pass" / "barcode01",
                                                          rundir / "fastq_pass" / "barcode42"]})
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(self.runsheet),  # enter runsheet
                                  ]

        expected_run = monitor_run.IsolateRun(rundir, self.runsheet, expected_samples,
                                              self.test_configfile, self.test_config)
        test_run = run_pipeline.initialize_nanopore_run(self.test_config, self.test_configfile)
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_success_infer_indir(self, mock_input):
        """Successfully initialize an IsolateRun from user input
         when input directory needs to be found."""
        input_rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        expected_rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
                           / "test1" / "rawdata" / "test_subdir")
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [expected_rundir / "fastq_pass" / "barcode01",
                                                          expected_rundir / "fastq_pass" / "barcode42"]})
        mock_input.side_effect = [str(input_rundir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(self.runsheet),  # enter runsheet
                                  ]
        expected_run = monitor_run.IsolateRun(input_rundir, self.runsheet, expected_samples,
                                              self.test_configfile, self.test_config)

        test_run = run_pipeline.initialize_nanopore_run(self.test_config, self.test_configfile)
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_get_nanopore_data_translate(self, mock_input):
        """Initialize Nanopore run with non-default column names"""
        seq_dir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
                           / "test1" / "rawdata" / "test_subdir")
        expected_rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
                           / "test1" / "rawdata" / "test_subdir")
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        mock_input.side_effect = [str(seq_dir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(runsheet),  # enter runsheet
                                  ]
        workflow_config = {"lab_info_system": {"use_lis_features": False},
                           "sample_number_settings": {'sample_number_format':
                                                          '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                      'sample_numbers_in': 'letter',
                                                      'sample_numbers_out': 'number',
                                                      'sample_numbers_report': 'letter',
                                                      'number_to_letter': {"30": "B", "10": "D",
                                                                           "11": "F", "50": "T"},
                                                      "format_in_sheet":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_output":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)'
                                                      },
                           "paths": {"output_base_path": "/path/to/output"},
                           "sequencing_mode": "nanopore",
                           "platform": "GridION",
                           "seq_run_duration_hours": 1,
                           "input_names": (pathlib.Path(__file__).parent / "data"
                                           / "localization" / "input_en.yaml"),
                           "debug": False
                           }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [
                                                    expected_rundir / "fastq_pass" / "barcode01",
                                                    expected_rundir / "fastq_pass" / "barcode42"]})
        test_run_params = run_pipeline.initialize_nanopore_run(workflow_config,
                                                               self.test_configfile)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path

    @mock.patch("builtins.input")
    def test_complain_bad_seq_time(self, mock_input):
        """Fail if an invalid response to sequencing time was entered."""
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "nope",  # reject sequencing time
                                  str(self.runsheet),  # enter runsheet
                                  "y"]  # accept output directory
        error_msg = "Sequencing time not entered. Aborting"
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            run_pipeline.initialize_nanopore_run(self.test_config, self.test_configfile)

    @mock.patch("builtins.input")
    def test_set_seq_time(self, mock_input):
        """Successfully set a different sequencing time."""
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        sequencing_time = 1.5
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "n",  # reject sequencing time
                                  sequencing_time,  # enter new sequencing time
                                  str(self.runsheet),  # enter runsheet
                                  "y"]  # accept output directory
        test_run = run_pipeline.initialize_nanopore_run(self.test_config, self.test_configfile)
        assert timedelta(hours=sequencing_time) == test_run.sequencing_time


class TestInitializeManualRun(unittest.TestCase):
    test_configfile = pathlib.Path("/path/to/config.yaml")

    def test_fail_invalid_mode(self):
        """Complain if an invalid sequencing mode is given."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "both",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }

        error_msg = "Invalid sequencing mode both"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_manual_run(active_config = test_config,
                                               config_path = self.test_configfile)

    def test_complain_hybrid(self):
        """Complain when starting a hybrid run (not implemented)."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "hybrid",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }
        error_msg = "Hybrid sequencing analysis is currently not implemented."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            run_pipeline.initialize_manual_run(active_config = test_config,
                                               config_path = self.test_configfile)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    @mock.patch("builtins.input")
    def test_initialize_illumina_default(self, mock_input):
        """Successfully initialize an Illumina run with default settings."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "platform": "NextSeq",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq" \
                 / "test" / "Analysis" / "1" / "Data" / "fastq"
        mock_input.side_effect = [str(rundir),  # enter Illumina dir
                                  str(runsheet),  # enter Illumina sheet
                                  "y"]  # accept suggested output dir

        expected_run = monitor_run.IsolateRun(rundir, runsheet, expected_samples,
                                              self.test_configfile, test_config)
        license_msg = ("RSYD-BASIC Copyright 2023-2024 Kat Steinke \n"
                       "This program comes with ABSOLUTELY NO WARRANTY; "
                       "for details type run_pipeline.py --show_warranty. \n"
                       "This is free software, and you are welcome to redistribute it "
                       "under certain conditions; type run_pipeline.py --license for details.")
        greet_illumina = ("### Bacterial isolate genome assembly\n"
                          "# Setup Illumina analysis -------------------------------")
        log_msg = "Wetlab version is Version_1.2.3."
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_manual_run(active_config = test_config,
                                                          config_path = self.test_configfile)
            assert ("run_pipeline", logging.INFO, license_msg) in self._caplog.record_tuples
            assert ("run_pipeline", logging.INFO, greet_illumina) in self._caplog.record_tuples
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_success_set_outdir_illumina(self, mock_input):
        """Set a different output directory than the default output directory."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "platform": "NextSeq",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                 / "multi_fastq_nextseq" \
                 / "test" / "Analysis" / "1" / "Data" / "fastq"
        expected_outdir = pathlib.Path("/path/to/new_outdir")
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  str(runsheet),  # enter runsheet
                                  "n",  # reject suggested outdir
                                  str(expected_outdir)]  # enter new outdir

        expected_run = monitor_run.IsolateRun(rundir, runsheet, expected_samples,
                                              self.test_configfile, test_config,
                                              outdir = expected_outdir)
        test_run = run_pipeline.initialize_manual_run(active_config = test_config,
                                                      config_path = self.test_configfile)
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_initialize_nanopore(self, mock_input):
        """Successfully initialize a Nanopore run with default parameters."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }

        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [rundir / "fastq_pass" / "barcode01",
                                                          rundir / "fastq_pass" / "barcode42"]})
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(runsheet),  # enter runsheet
                                  "y"]  # accept output directory

        expected_run = monitor_run.IsolateRun(rundir, runsheet, expected_samples,
                                              self.test_configfile, test_config)
        license_msg = ("RSYD-BASIC Copyright 2023-2024 Kat Steinke \n"
                       "This program comes with ABSOLUTELY NO WARRANTY; "
                       "for details type run_pipeline.py --show_warranty. \n"
                       "This is free software, and you are welcome to redistribute it "
                       "under certain conditions; type run_pipeline.py --license for details.")
        greet_nanopore = ("### Bacterial isolate genome assembly\n"
                          "# Setup Nanopore analysis -------------------------------")
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_manual_run(active_config = test_config,
                                                          config_path = self.test_configfile)
            assert ("run_pipeline", logging.INFO, license_msg) in self._caplog.record_tuples

            assert ("run_pipeline", logging.INFO, greet_nanopore) in self._caplog.record_tuples
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_success_set_outdir_nanopore(self, mock_input):
        """Set a different output directory than the default output directory for a nanopore run."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }

        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        expected_outdir = pathlib.Path("/path/to/new_outdir")
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [rundir / "fastq_pass" / "barcode01",
                                                          rundir / "fastq_pass" / "barcode42"]})
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(runsheet),  # enter runsheet
                                  "n",  # reject suggested outdir
                                  str(expected_outdir)]  # set new outdir

        expected_run = monitor_run.IsolateRun(rundir, runsheet, expected_samples,
                                              self.test_configfile, test_config,
                                              outdir = expected_outdir)
        test_run = run_pipeline.initialize_manual_run(active_config = test_config,
                                                      config_path = self.test_configfile)
        assert test_run == expected_run

    @mock.patch("builtins.input")
    def test_complain_bad_seq_time(self, mock_input):
        """Fail if an invalid response to sequencing time was entered."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }

        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "nope",  # reject sequencing time
                                  str(runsheet),  # enter runsheet
                                  "y"]  # accept output directory
        error_msg = "Sequencing time not entered. Aborting"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_manual_run(active_config = test_config,
                                               config_path = self.test_configfile)

    @mock.patch("builtins.input")
    def test_set_seq_time(self, mock_input):
        """Successfully set a different sequencing time."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "platform": "GridION",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }

        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
                  / "rawdata" / "subdir")
        sequencing_time = 1.5
        mock_input.side_effect = [str(rundir),  # enter run directory
                                  "n",  # reject sequencing time
                                  sequencing_time,  # enter new sequencing time
                                  str(runsheet),  # enter runsheet
                                  "y"]  # accept output directory
        test_run = run_pipeline.initialize_manual_run(active_config = test_config,
                                                      config_path = self.test_configfile)
        assert timedelta(hours = sequencing_time) == test_run.sequencing_time


class TestInitializeCommandlineRun(unittest.TestCase):
    sheet_names = RunsheetNames(sample_info_sheet = "Prøveoplysninger",
                                sample_number = "Prøvenr",
                                extraction_well_number = "Brønd_nr",
                                extraction_run_number = "Oprensnings_kørselsnr",
                                dna_concentration = "DNA_konc",
                                requested_by = "Bestilt af",
                                comment = "Kommentar",
                                sequencing_run_number = "SEK_Run_nr",
                                indications = ["Resistens", "Identifikation",
                                               "Relaps", "Toxingen_ID",
                                               "HAI", "Miljøprøveisolat",
                                               "Udbrudsisolat",
                                               "Overvågningsisolat",
                                               "Forskning", "Andet"],
                                dna_normalization_sheet = "DNA normalisering",
                                dna_normalization_sample_nr = "Prøvenummer",
                                ilm_well_number = "ILM Brønd position",
                                plate_layout = "Pladelayout",
                                runsheet_base = "Runsheet",
                                experiment_name = "RUNxxxx-INI",
                                nanopore_sheet_sample_number = "KMA_nr",
                                barcode = "Barkode",
                                protocol_version = "Protokol (versionsnummer)")
    test_config_illumina = {"debug": False,
                   "lab_info_system": {"use_lis_features": False},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "/path/to/output"},
                   "sequencing_mode": "illumina",
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml"),
                            "platform": "NextSeq"
                   }
    test_config_nanopore = {"debug": False,
                            "lab_info_system": {"use_lis_features": False},
                            "sample_number_settings": {'sample_number_format':
                                                           '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                       'sample_numbers_in': 'letter',
                                                       'sample_numbers_out': 'number',
                                                       "sample_numbers_report": "letter",
                                                       'number_to_letter': {"30": "B", "10": "D",
                                                                            "11": "F", "50": "T"},
                                                       "format_in_sheet":
                                                           r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                       "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                       "format_output":
                                                           r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                       },
                            "seq_run_duration_hours": 1,
                            "paths": {"output_base_path": "/path/to/output"},
                            "sequencing_mode": "nanopore",
                            "input_names": (pathlib.Path(__file__).parent / "data"
                                            / "localization" / "input_da.yaml"),
                            "platform": "GridION"
                            }
    test_outdir = pathlib.Path("/path/to/outdir")
    test_configfile = pathlib.Path("/path/to/config.yaml")
    ilm_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
              / "multi_fastq_nextseq" \
              / "test" / "Analysis" / "1" / "Data" / "fastq"
    ilm_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "runsheet_singleindication_multisample.xlsx"
    expected_samples_ilm = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })

    ont_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
    ont_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                / "test_nanopore_runsheet.xlsx"
    expected_samples_ont = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                           / "data"
                                                           / "prep_sheets"
                                                           / "test_dir"
                                                           / "rawdata"
                                                           / "subdir"
                                                           / "fastq_pass"
                                                           / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                           / "data"
                                                           / "prep_sheets"
                                                           / "test_dir"
                                                           / "rawdata"
                                                           / "subdir"
                                                           / "fastq_pass"
                                                           / "barcode42")]
                                                })

    def test_fail_unsupported_mode(self):
        """Fail when an unsupported sequencing mode is requested."""
        args = Namespace(mode="hybrid", illumina_sheet = self.ilm_sheet,
                         illumina_dir = self.ilm_dir, workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Hybrid sequencing analysis is currently not implemented."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args, active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    def test_fail_invalid_mode(self):
        """Fail when an invalid sequencing mode is requested."""
        args = Namespace(mode = "sanger", illumina_sheet = self.ilm_sheet,
                         illumina_dir = self.ilm_dir, workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Invalid sequencing mode sanger."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args, active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    def test_fail_ilm_missing_sheet(self):
        """Fail when Illumina analysis has been requested but no Illumina runsheet was given."""
        args = Namespace(mode="illumina", illumina_dir = self.ilm_dir, illumina_sheet = None,
                         workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Illumina runsheet not specified."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args,
                                                    active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    def test_fail_ilm_missing_dir(self):
        """Fail when Illumina analysis has been requested but no result directory was given."""
        args = Namespace(mode = "illumina", illumina_sheet = self.ilm_sheet, illumina_dir = None,
                         workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Illumina run directory not specified."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args,
                                                    active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    def test_fail_ont_missing_sheet(self):
        """Fail when Nanopore analysis has been requested but no Nanopore runsheet was given."""
        args = Namespace(mode = "nanopore", nanopore_dir = self.ont_dir, nanopore_sheet = None,
                         workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Nanopore runsheet not specified."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args,
                                                    active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    def test_fail_ont_missing_dir(self):
        """Fail when Nanopore analysis has been requested but no result directory was given."""
        args = Namespace(mode = "nanopore", nanopore_sheet = self.ont_sheet, nanopore_dir = None,
                         workflow_config_file=self.test_configfile,
                         test_run=None)
        error_msg = "Nanopore run directory not specified."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            run_pipeline.initialize_commandline_run(args,
                                                    active_config = self.test_config_illumina,
                                                    runsheet_names = self.sheet_names)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_default_illumina(self):
        """Successfully set up an Illumina run with default parameters."""
        args = Namespace(illumina_dir = self.ilm_dir,
                         illumina_sheet = self.ilm_sheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ilm_dir,
                                              runsheet_path = self.ilm_sheet,
                                              sample_sheet = self.expected_samples_ilm,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_illumina)
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_commandline_run(args,
                                                               active_config = self.test_config_illumina,
                                                               runsheet_names = self.sheet_names)
            log_msg = 'Wetlab version is Version_1.2.3.'
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_run == test_run

    def test_success_illumina_en(self):
        """Successfully set up an Illumina run with non-default column names in the runsheet."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample_en.xlsx"
        test_config = {"debug": False,
                   "lab_info_system": {"use_lis_features": False},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "/path/to/output"},
                   "sequencing_mode": "illumina",
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml"),
                            "platform": "NextSeq"
                   }
        args = Namespace(illumina_dir = self.ilm_dir,
                         illumina_sheet = runsheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time = None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ilm_dir,
                                              runsheet_path = runsheet,
                                              sample_sheet = self.expected_samples_ilm,
                                              configfile = self.test_configfile,
                                              active_config = test_config)
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_commandline_run(args,
                                                               active_config = test_config,
                                                               runsheet_names = SHEET_NAMES_EN)
            log_msg = "Wetlab version is Version_1.2.3."
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_run == test_run

    def test_success_default_config(self):
        """Use default config path and file if none is given."""
        args = Namespace(illumina_dir = self.ilm_dir,
                         illumina_sheet = self.ilm_sheet,
                         workflow_config_file = None,
                         mode=None, outdir=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ilm_dir,
                                              runsheet_path = self.ilm_sheet,
                                              sample_sheet = self.expected_samples_ilm,
                                              configfile = run_pipeline.DEFAULT_CONFIG_FILE,
                                              active_config = self.test_config_illumina)
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_run = run_pipeline.initialize_commandline_run(args,
                                                               active_config = self.test_config_illumina,
                                                               runsheet_names = self.sheet_names)
            log_msg = 'Wetlab version is Version_1.2.3.'
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_run == test_run

    def test_success_illumina_custom_outdir(self):
        """Successfully set up an Illumina run with a user-defined output directory."""
        args = Namespace(illumina_dir = self.ilm_dir,
                         illumina_sheet = self.ilm_sheet,
                         outdir = pathlib.Path(__file__).parent / "data" / "outdir",
                         workflow_config_file = self.test_configfile, mode=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ilm_dir,
                                              runsheet_path = self.ilm_sheet,
                                              sample_sheet = self.expected_samples_ilm,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_illumina,
                                              outdir = pathlib.Path(__file__).parent / "data"
                                                       / "outdir")
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_illumina,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_default_nanopore(self):
        """Successfully set up a Nanopore run with default parameters."""
        args = Namespace(nanopore_dir = self.ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_default_nanopore_explicit_dir(self):
        """Successfully set up a Nanopore run with default parameters,
         with the fastq dir explicitly given."""
        ont_dir = (pathlib.Path(__file__).parent
                   / "data"
                   / "prep_sheets"
                   / "test_dir"
                   / "rawdata"
                   / "subdir"
                   / "fastq_pass")
        args = Namespace(nanopore_dir = ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None, test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_default_nanopore_infer_dir(self):
        """Successfully set up a Nanopore run with default parameters,
         with the directory only partially given."""
        ont_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
        expected_rundir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir"
                           / "test1" / "rawdata" / "test_subdir")
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [
                                                    expected_rundir / "fastq_pass" / "barcode01",
                                                    expected_rundir / "fastq_pass" / "barcode42"]})
        args = Namespace(nanopore_dir = ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None, test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = expected_samples,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_nanopore_en(self):
        """Successfully set up a Nanopore run with non-default column names in the runsheet."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        test_config =  {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml"),
                        "platform": "GridION"
                       }
        args = Namespace(nanopore_dir = self.ont_dir,
                         nanopore_sheet = runsheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = runsheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = test_config)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = test_config,
                                                           runsheet_names = SHEET_NAMES_EN)
        assert expected_run == test_run

    def test_success_test_run(self):
        """Successfully set up a test run when the config specifies routine."""
        args = Namespace(nanopore_dir = self.ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         workflow_config_file = self.test_configfile,
                         mode=None, outdir=None, run_time=None, test_run=True)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore,
                                              test_run = True)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_override_defaults(self):
        """Override sequencing mode settings given in the config."""
        args = Namespace(mode = "nanopore", nanopore_dir = self.ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         workflow_config_file = self.test_configfile,
                         outdir=None, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_illumina,
                                              sequencing_mode = "nanopore")
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_illumina,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_nanopore_custom_outdir(self):
        """Successfully set up a Nanopore run with a user-defined output directory."""
        args = Namespace(mode = "nanopore", nanopore_dir = self.ont_dir,
                         nanopore_sheet = self.ont_sheet,
                         outdir = pathlib.Path(__file__).parent / "data" / "outdir",
                         workflow_config_file = self.test_configfile, run_time=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore,
                                              outdir = pathlib.Path(__file__).parent / "data"
                                                       / "outdir")
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run

    def test_success_nanopore_custom_seq_time(self):
        """Successfully set up a Nanopore run with user-defined sequencing time."""
        args = Namespace(mode = "nanopore", nanopore_dir = self.ont_dir,
                         nanopore_sheet = self.ont_sheet, run_time=3.14,
                         workflow_config_file = self.test_configfile,
                         outdir=None,
                         test_run=None)
        expected_run = monitor_run.IsolateRun(rundir_path = self.ont_dir,
                                              runsheet_path = self.ont_sheet,
                                              sample_sheet = self.expected_samples_ont,
                                              configfile = self.test_configfile,
                                              active_config = self.test_config_nanopore,
                                              sequencing_time = 3.14)
        test_run = run_pipeline.initialize_commandline_run(args,
                                                           active_config = self.test_config_nanopore,
                                                           runsheet_names = self.sheet_names)
        assert expected_run == test_run


class TestCreateOutputDirs(unittest.TestCase):
    input_dir = pathlib.Path("path/to/input")
    configfile = pathlib.Path("path/to/configfile")
    test_config = {"debug": False,
                   "lab_info_system": {"use_lis_features": False},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B",
                                                                   "10": "D",
                                                                   "11": "F",
                                                                   "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },

                   "sequencing_mode": "illumina",
                   "paths": {"output_base_path": "/path/to/output"},
                   "seq_run_duration_hours": 1,
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")}
    runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
               / "runsheet_experiment_name.xlsx")
    sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                        "runtype": ["paired-end"],
                                        "r1": ["F99123456_R1.fastq"],
                                        "r2": ["F99123456_R2.fastq"],
                                        "extra": [pd.NA]})

    @classmethod
    def tearDownClass(cls) -> None:
        # clean up data after running
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_output")
        shutil.rmtree(pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_existing_output" / "logs")

    def test_fail_existing_dir(self):
        """Fail if the output directory exists already."""
        error_msg = "File of filenames already found."
        outdir = pathlib.Path(__file__).parent / "data"/"monitor_run"/"test_illumina_outdir"
        test_run = monitor_run.IsolateRun(self.input_dir, self.runsheet, self.sample_sheet,
                                          self.configfile, self.test_config, outdir = outdir)
        with pytest.raises(FileExistsError, match = re.escape(error_msg)):
            run_pipeline.set_up_outputs(test_run)

    def test_success_create_new(self):
        """Create a new output directory and log directory."""
        output_dir = pathlib.Path(__file__).parent / "data" / "utilities_test"/ "test_output"
        assert not output_dir.exists()
        assert not (output_dir / "logs").exists()
        test_run = monitor_run.IsolateRun(self.input_dir, self.runsheet, self.sample_sheet,
                                          self.configfile, self.test_config, outdir = output_dir)
        run_pipeline.set_up_outputs(test_run)
        assert output_dir.exists()
        assert (output_dir / "logs").exists()
        test_sample_sheet = pd.read_csv(test_run.file_of_filenames, sep="\t")
        pd.testing.assert_frame_equal(test_sample_sheet, test_run.sample_sheet, check_dtype = False)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_continue_run(self):
        """Don't complain for a continued run, and log that it's continued."""
        log_msg = "Output directory already exists. Continuing run..."
        output_dir = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_existing_output")
        assert output_dir.exists()
        assert not (output_dir / "logs").exists()
        test_run = monitor_run.IsolateRun(self.input_dir, self.runsheet, self.sample_sheet,
                                          self.configfile, self.test_config, outdir = output_dir)
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            run_pipeline.set_up_outputs(test_run, continue_run = True)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert output_dir.exists()
        assert (output_dir / "logs").exists()
        test_sample_sheet = pd.read_csv(test_run.file_of_filenames, sep = "\t")
        pd.testing.assert_frame_equal(test_sample_sheet, test_run.sample_sheet, check_dtype = False)


class TestCalledWithoutArgs(unittest.TestCase):
    def test_fail_mismatched_args(self):
        """Fail if the arguments to check aren't found in the parser."""
        fail_args = Namespace(foo=None, bar=None)
        parser = ArgumentParser(add_help = False)
        parser.add_argument("--baz")
        error_msg = "Argument(s) ['bar', 'foo'] are not valid arguments for the specified parser."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            run_pipeline.check_called_without_args(fail_args, parser)

    def test_success_all_args_none(self):
        """Report parser called without args when all Namespace values are False or None."""
        test_args = Namespace(foo=None, bar=None)
        parser = ArgumentParser(add_help = False)
        parser.add_argument("--foo")
        parser.add_argument("--bar")
        assert run_pipeline.check_called_without_args(test_args, parser) is True

    def test_success_all_args_default(self):
        """Report parser called without args when all Namespace values match defaults."""
        test_args = Namespace(foo = 42, bar = False)
        parser = ArgumentParser(add_help = False)
        parser.add_argument("--foo", default=42)
        parser.add_argument("--bar", action = "store_true")
        assert run_pipeline.check_called_without_args(test_args, parser) is True

    def test_success_store_false(self):
        """Report parser called without args when the parser has an argument with store_false
        ( = called without the flag = True)."""
        test_args = Namespace(foo = None, bar = True)
        parser = ArgumentParser(add_help = False)
        parser.add_argument("--foo")
        parser.add_argument("--bar", action = "store_false")
        assert run_pipeline.check_called_without_args(test_args, parser) is True

    def test_success_called_with_args(self):
        """Report that the parser was called with arguments."""
        test_args = Namespace(foo = 42, bar = None)
        parser = ArgumentParser(add_help = False)
        parser.add_argument("--foo")
        parser.add_argument("--bar")
        assert run_pipeline.check_called_without_args(test_args, parser) is False


class TestRunPipeline(unittest.TestCase):
    config_illumina = {"debug": True,
                   "lab_info_system": {"use_lis_features": True,
                                       "lis_report": str(pathlib.Path(__file__).parent
                                                         / "data"
                                                         / "utilities_test"
                                                         / "mads_for_sheet_prep.txt"),
                                       "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv"),
                                       "database": "",
                                       "dialect": ""},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'letter',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": str(pathlib.Path(__file__).parent / "data"
                                                     / "utilities_test" / "test_outdir")},
                   "sequencing_mode": "illumina",
                   "platform": "NextSeq",
                   "input_names": (pathlib.Path(__file__).parent / "data"
                                   / "localization" / "input_da.yaml")
                   }
    ilm_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                / "runsheet_singleindication_multisample.xlsx"
    ilm_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
              / "multi_fastq_nextseq" \
              / "test" / "Analysis" / "1" / "Data" / "fastq"
    ilm_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                            "runtype": ["paired-end", "paired-end"],
                                            "r1": [(pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-1_L001_R1_001.fastq.gz"),
                                                   (pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-2_L001_R1_001.fastq.gz")
                                                   ],
                                       "r2": [(pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-1_L001_R2_001.fastq.gz"),
                                                   (pathlib.Path(__file__).parent / "data"
                                                    / "utilities_test"
                                                    / "multi_fastq_nextseq"
                                                    / "test" / "Analysis" / "1" / "Data"
                                                    / "fastq"
                                                    / "B99123456-2_L001_R2_001.fastq.gz")
                                                   ],
                                       "extra": ["", ""]
                                       })
    config_ont = {"debug": True,
                   "lab_info_system": {"use_lis_features": True,
                                       "lis_report": str(pathlib.Path(__file__).parent
                                                         / "data"
                                                         / "utilities_test"
                                                         / "mads_for_sheet_prep.txt"),
                                       "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv"),
                                       "database": "",
                                       "dialect": ""},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'letter',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 0.01,
                   "check_interval_seconds": 1,
                   "paths": {"output_base_path": str(pathlib.Path(__file__).parent / "data"
                                                     / "utilities_test" / "test_outdir")},
                   "sequencing_mode": "nanopore",
                   "platform": "GridION",
                  "input_names": (pathlib.Path(__file__).parent / "data"
                                  / "localization" / "input_da.yaml")
                  }
    prod_config = config_ont.copy()
    prod_config["debug"] = False

    ont_sheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "test_nanopore_runsheet.xlsx"
    ont_dir = (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_dir_multi_pass"
              / "rawdata" / "subdir")
    ont_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                            "runtype": ["ont", "ont"],
                                            "r1": ["", ""],
                                            "r2": ["", ""],
                                            "extra": [ont_dir / "fastq_pass" / "barcode01",
                                                      ont_dir / "fastq_pass" / "barcode42"]})
    blank_args = ["--dry_run"]  # we mock check_called_without_args -> still "looks manual"

    # reset logfile
    # reset output dirs

    def tearDown(self):
        # clean up any existing paths
        output_paths = [(pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "ILM_Run0000_Y20220101_XYZ"),
                        (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "test_manual_outdir_ilm"),
                        (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "ONT_RUN0000_Y20990101_XYZ"),
                        (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "test_manual_outdir_ont"),
                        (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "test_start_illumina_en"),
                        (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
                         / "test_start_ont_en")
                        ]
        for path in output_paths:
            if path.exists():
                shutil.rmtree(path)
        if (pathlib.Path(__file__).parent / "data" / "utilities_test"
            / "test_existing_output" / "logs").exists():
            shutil.rmtree((pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_existing_output" / "logs"))
        (pathlib.Path(__file__).parent / "data" / "utilities_test"
         / "test_separate_log.log").unlink(missing_ok = True)

    # @classmethod
    # def tearDownClass(cls) -> None:
    #     # clean up data after running
    #     output_paths = [(pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
    #                      / "ILM_Run0000_Y20220101_XYZ"),
    #                     (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
    #                      / "test_manual_outdir_ilm"),
    #                     (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
    #                      / "ONT_RUN0000_Y20990101_XYZ"),
    #                     (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
    #                      / "test_manual_outdir_ont"),
    #                     (pathlib.Path(__file__).parent / "data" / "utilities_test" / "test_outdir"
    #                      / "test_start_illumina_en")]
    #     for path in output_paths:
    #         if path.exists():
    #             (path / "logs" / "start_pipeline.log").unlink(missing_ok = True)
    #             shutil.rmtree(path / "logs", ignore_errors = True)
    #             shutil.rmtree(path, ignore_errors = True)
    #     if (pathlib.Path(__file__).parent / "data" / "utilities_test"
    #         / "test_existing_output" / "logs").exists():
    #         (pathlib.Path(__file__).parent / "data" / "utilities_test"
    #          / "test_existing_output" / "logs" / "pipeline_start.log").unlink(missing_ok = True)
    #         shutil.rmtree((pathlib.Path(__file__).parent / "data" / "utilities_test"
    #                        / "test_existing_output" / "logs"), ignore_errors = True)
    #     (pathlib.Path(__file__).parent / "data" / "utilities_test"
    #      / "test_separate_log.log").unlink(missing_ok = True)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog
    # TODO: when we get to the filtered log handler replace it with one that logs to stringio

    # we need to override the configfile here so output dir is a test directory
    # most of this needs to be in dry run mode - for the manual parts,
    # we pass dry_run through anyway but make it look like it's called with defaults
    @mock.patch(f"{run_pipeline.__name__}.check_called_without_args")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_illumina)
    @mock.patch("builtins.input")
    def test_run_illumina_manual_defaults(self, mock_input, mock_check):
        """Start an Illumina run in manual mode without altering any inputs."""
        mock_check.return_value = True
        mock_input.side_effect = [str(self.ilm_dir),  # enter Illumina dir
                                  str(self.ilm_sheet),  # enter Illumina sheet
                                  "y"]  # accept suggested output dir
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                     / "ILM_Run0000_Y20220101_XYZ")
        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ilm_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(self.blank_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(# Setup Illumina analysis -------------------------------)"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            print(line)
            assert re.match(log_format, line.strip())  # TODO readlines and compare line for line so the file can be closed

    @mock.patch(f"{run_pipeline.__name__}.check_called_without_args")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_illumina)
    @mock.patch("builtins.input")
    def test_run_illumina_manual_set_outdir(self, mock_input, mock_check):
        """Start an Illumina run in manual mode while setting the output dir."""
        mock_check.return_value = True
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                     / "test_manual_outdir_ilm")
        mock_input.side_effect = [str(self.ilm_dir),  # enter run directory
                                  str(self.ilm_sheet),  # enter runsheet
                                  "n",  # reject suggested outdir
                                  str(outdir)]  # enter new outdir

        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                            "-meta", f"bactopia_names={str(file_of_filenames)}",
                            "-meta", f'outdir={str(outdir)}',
                            "-meta", f"outdir_name={outdir.name}",
                            "-meta", 'node_dir=None',
                            "-meta", "use_lis=1",
                            "-meta", f"runsheet={str(self.ilm_sheet)}",
                            "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                            "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(self.blank_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(# Setup Illumina analysis -------------------------------)"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch(f"{run_pipeline.__name__}.check_called_without_args")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    @mock.patch("builtins.input")
    def test_run_nanopore_manual_defaults(self, mock_input, mock_check, mock_fork):
        """Start a Nanopore run in manual mode without altering any inputs."""
        mock_fork.return_value = False
        mock_check.return_value = True
        mock_input.side_effect = [str(self.ont_dir),  # enter Nanopore dir
                                  "y",  # accept sequencing time
                                  str(self.ont_sheet),  # enter Nanopore sheet
                                  "y"]  # accept suggested output dir
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "ONT_RUN0000_Y20990101_XYZ")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(self.blank_args)
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(# Setup Nanopore analysis -------------------------------)"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        assert expected_command == test_command.args

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch(f"{run_pipeline.__name__}.check_called_without_args")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    @mock.patch("builtins.input")
    def test_run_nanopore_manual_custom(self, mock_input, mock_check, mock_fork):
        """Start a Nanopore run in manual mode while setting the output dir."""
        mock_fork.return_value = False
        mock_check.return_value = True
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                     / "test_manual_outdir_ont")
        mock_input.side_effect = [str(self.ont_dir),  # enter run directory
                                  "y",  # accept sequencing time
                                  str(self.ont_sheet),  # enter runsheet
                                  "n",  # reject suggested outdir
                                  str(outdir)]  # set new outdir
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nomad_command =["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(self.blank_args)
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(# Setup Nanopore analysis -------------------------------)"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        assert expected_command == test_command.args

    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_illumina)
    def test_run_illumina_commandline_defaults(self):
        """Start an Illumina run in commandline mode without altering any inputs."""
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                  / "ILM_Run0000_Y20220101_XYZ")
        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ilm_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ilm_args = ["--illumina_dir", str(self.ilm_dir),
                    "--illumina_sheet", str(self.ilm_sheet),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ilm_args)
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        assert expected_command == test_command.args

    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_illumina)
    def test_run_illumina_commandline_set_outdir(self):
        """Start an Illumina run in commandline mode while setting the output dir."""
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                  / "test_manual_outdir_ilm")
        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ilm_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ilm_args = ["--illumina_dir", str(self.ilm_dir),
                    "--illumina_sheet", str(self.ilm_sheet),
                    "--outdir", str(outdir),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ilm_args)
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        assert expected_command == test_command.args

    def test_run_illumina_commandline_set_config(self):
        """Start an Illumina run in commandline mode with a different config file."""
        outdir = (pathlib.Path(self.config_illumina["paths"]["output_base_path"])
                  / "test_start_illumina_en")
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample_en.xlsx"
        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        configfile = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_config_ilm_en.yaml")
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(runsheet)}",
                         "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                         "staging-snaketopia", str(configfile)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ilm_args =["--illumina_dir", str(self.ilm_dir),
                    "--illumina_sheet", str(runsheet),
                   "--outdir", str(outdir),
                   "--workflow_config_file", str(configfile),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ilm_args)
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        assert expected_command == test_command.args

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    def test_run_nanopore_commandline_defaults(self, mock_fork):
        """Start a Nanopore run in commandline mode without altering any inputs."""
        mock_fork.return_value = False
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "ONT_RUN0000_Y20990101_XYZ")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())
        # check that we've logged the part after waiting
        monitor_logs = [log_line for log_line in log_data if "monitor_run" in log_line]
        assert monitor_logs

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    def test_run_nanopore_commandline_set_outdir(self, mock_fork):
        """Start a Nanopore run in commandline mode while setting the output dir."""
        mock_fork.return_value = False
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "test_manual_outdir_ont")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet),
                    "--outdir", str(outdir),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    def test_run_nanopore_commandline_set_config(self, mock_fork):
        """Start a Nanopore run in commandline mode with a different config file."""
        mock_fork.return_value = False
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "test_start_ont_en")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        configfile = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                      / "test_config_ont_en.yaml")
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(runsheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(configfile)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(runsheet),
                    "--outdir", str(outdir),
                    "--workflow_config_file", str(configfile),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())

# TODO: mock monitor_run - ensure that the correct time is given (assert called with ...?)
    @mock.patch(f"{run_pipeline.__name__}.monitor_run.start_on_file_found")
    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", prod_config)
    def test_run_nanopore_long_run(self, mock_fork, mock_start):
        """Properly pass waiting time when waiting for over an hour."""
        mock_fork.return_value = False
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "ONT_RUN0000_Y20990101_XYZ")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        watch_hours = 24
        fudge_hours = 1
        watch_seconds = (watch_hours + fudge_hours) * 3600
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet),
                    "--run_time", str(watch_hours),
                    "--dry_run"]
        test_command = run_pipeline.run_pipeline(ont_args)
        mock_start.assert_called_with(mock.ANY, # run params - TODO: should we double-check we're calling with the right ones?
                                      "final_summary*.txt",
                                      dry_run=True,
                                      watch_timeout = watch_seconds,
                                      watch_interval = mock.ANY,
                                      log_interval = 3600)


    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", prod_config)
    def test_run_override_test_mode(self, mock_fork):
        """Override the config and run pipeline in test mode from the commandline."""
        mock_fork.return_value = False
        outdir = (pathlib.Path(self.prod_config["paths"]["output_base_path"])
                  / "test_manual_outdir_ont")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet),
                    "--outdir", str(outdir),
                    "--test_run",
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args

    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_illumina)
    def test_run_continue_pipeline(self):
        """Continue the pipeline from the commandline when an output dir already exists."""
        log_msg = "Output directory already exists. Continuing run..."
        outdir = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                  / "test_existing_output")
        file_of_filenames = outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ilm_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ilm_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--illumina_dir", str(self.ilm_dir),
                    "--illumina_sheet", str(self.ilm_sheet),
                   "--outdir", str(outdir),
                   "--continue_pipeline",
                    "--dry_run"]
        # make sure we don't have deleted the expected dirs before
        assert outdir.exists()
        with self._caplog.at_level(level="INFO", logger="run_pipeline"):
            test_command = run_pipeline.run_pipeline(ont_args)
            assert ("run_pipeline", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert (outdir / "logs").exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ilm_dir)})")
        with open((outdir / "logs" / "start_pipeline.log"), "r", encoding = "utf-8") as read_log:
            for line in read_log:
                assert re.match(log_format, line.strip())

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    def test_run_set_logfile(self, mock_fork):
        """Set a different log file from the commandline."""
        mock_fork.return_value = False
        outdir = (pathlib.Path(self.config_ont["paths"]["output_base_path"])
                  / "test_manual_outdir_ont")
        file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        logfile = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                  / "test_separate_log.log")
        nomad_command = ["nomad", "job", "dispatch",
                         "-meta", f"bactopia_names={str(file_of_filenames)}",
                         "-meta", f'outdir={str(outdir)}',
                         "-meta", f"outdir_name={outdir.name}",
                         "-meta", 'node_dir=None',
                         "-meta", "use_lis=1",
                         "-meta", f"runsheet={str(self.ont_sheet)}",
                         "-meta", f"sequencing_dir={str(self.ont_dir)}",
                         "staging-snaketopia", str(run_pipeline.DEFAULT_CONFIG_FILE)]
        # dry run so what we're looking for is the command being echoed
        expected_command = ["echo", f'"{" ".join(nomad_command)}"']
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet),
                    "--outdir", str(outdir),
                    "--logfile", str(logfile),
                    "--dry_run"]
        # make sure we don't have the expected dirs before
        assert not outdir.exists()
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args
        # we also want to know that the dirs exist now
        assert outdir.exists()
        assert logfile.exists()
        # and that we've written the log to file in the format we want
        # TODO: when we have filtering this needs to exclude "base" as the source
        log_format = re.compile(r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}"
                                r" - (INFO|WARNING|ERROR): \w+: [-:a-zA-Z0-9_./#]+"
                                r"|(This program comes with ABSOLUTELY NO WARRANTY; for details"
                                r" type run_pipeline.py --show_warranty.)"
                                r"|(This is free software, and you are welcome to redistribute it"
                                r" under certain conditions; type run_pipeline.py --license "
                                r"for details.)"
                                f"|({str(self.ont_dir)})")
        with open(logfile, "r", encoding = "utf-8") as read_log:
            log_data = read_log.readlines()
        for line in log_data:
            assert re.match(log_format, line.strip())

    @mock.patch(f"{run_pipeline.__name__}.os.fork")
    @mock.patch.dict(f"{run_pipeline.__name__}.WORKFLOW_CONFIG", config_ont)
    @mock.patch(f'{monitor_run.__name__}.get_nomad_command',
                wraps = monitor_run.get_nomad_command)
    def test_non_dry(self, mock_command, mock_fork):
        """Run a non-dry run."""
        mock_fork.return_value = False
        mock_command.return_value = ["echo", "hello"]
        expected_command = ["echo", "hello"]
        ont_args = ["--nanopore_dir", str(self.ont_dir),
                    "--nanopore_sheet", str(self.ont_sheet)]
        test_command = run_pipeline.run_pipeline(ont_args)
        assert expected_command == test_command.args
