#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest

import pandas as pd

import get_analysis_subsets
from input_names import LISDataNames

LIS_NAMES_EN = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                            isolate_number = "BACT_NR",
                            bacteria_code = "LOCAL_BACT_CODE",
                            bacteria_text = "LOCAL_BACT_TEXT")

class TestGetSequencers(unittest.TestCase):
    def test_get_illumina_only(self):
        run_sheet = pd.DataFrame(data={"sample": ["1199123456-1", "1199234567-2"],
                                       "runtype": ["paired-end", "paired-end"],
                                       "r1": ["1199123456-1_R1.fq.gz", "1199234567-2_R1.fq.gz"],
                                       "r2": ["1199123456-1_R2.fq.gz", "1199234567-2_R2.fq.gz"],
                                       "extra": [pd.NA, pd.NA]})
        expected_result = get_analysis_subsets.SamplesBySequencer(illumina = ["1199123456-1",
                                                                              "1199234567-2"],
                                                                  nanopore = [],
                                                                  hybrid = [],
                                                                  all_illumina = ["1199123456-1",
                                                                                  "1199234567-2"],
                                                                  all_nanopore = [])
        test_result = get_analysis_subsets.get_samples_by_sequencer(run_sheet)
        assert test_result == expected_result

    def test_get_hybrid(self):
        run_sheet = pd.DataFrame(data = {"sample": ["1199123456-1", "1199234567-2"],
                                         "runtype": ["paired-end", "hybrid"],
                                         "r1": ["1199123456-1_R1.fq.gz", "1199234567-2_R1.fq.gz"],
                                         "r2": ["1199123456-1_R2.fq.gz", "1199234567-2_R2.fq.gz"],
                                         "extra": [pd.NA, "barcode01"]})
        expected_result = get_analysis_subsets.SamplesBySequencer(illumina = ["1199123456-1"],
                                                                  nanopore = [],
                                                                  hybrid = ["1199234567-2"],
                                                                  all_illumina = ["1199123456-1",
                                                                                  "1199234567-2"],
                                                                  all_nanopore = ["1199234567-2"])
        test_result = get_analysis_subsets.get_samples_by_sequencer(run_sheet)
        assert test_result == expected_result


class TestSubsetBySpecies(unittest.TestCase):
    def setUp(self) -> None:
        self.mads_report = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199234567-2",
                                                           "1099654321-9", "1099654321-8"],
                                              "LOKAL_BAKT_KODE": ["1", "888", "1", "3"]})
        self.lis_names_da =  LISDataNames(sample_number = "PRV_NR", sample_type = "P_TYPE",
                            isolate_number = "BAKT_NR",
                            bacteria_code = "LOKAL_BAKT_KODE",
                            bacteria_text = "LOKAL_BAKT_TEKST")

    def test_find_by_single_code(self):
        run_sheet = pd.DataFrame(data = {"sample": ["1199123456-1", "1199234567-2"]})
        true_samples = ["1199123456-1"]
        test_samples = get_analysis_subsets.get_samples_by_bact_id(run_sheet, self.mads_report,
                                                                   organism_ids = ["1"],
                                                                   lis_data_names = self.lis_names_da)
        assert true_samples == test_samples

    def test_find_different_code(self):
        """Find subset using LIS report with nonstandard column names."""
        mads_report_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199234567-2",
                                                             "1099654321-9", "1099654321-8"],
                                                "LOCAL_BACT_CODE": ["1", "888", "1", "3"]})
        run_sheet = pd.DataFrame(data = {"sample": ["1199123456-1", "1199234567-2"]})
        true_samples = ["1199123456-1"]
        test_samples = get_analysis_subsets.get_samples_by_bact_id(run_sheet, mads_report_en,
                                                                   organism_ids = ["1"],
                                                                   lis_data_names = LIS_NAMES_EN)
        assert true_samples == test_samples

    def test_find_multi_code(self):
        run_sheet = pd.DataFrame(data = {"sample": ["1199123456-1", "1099654321-8",
                                                    "1199234567-2"]})
        true_samples = ["1199123456-1", "1099654321-8"]
        test_samples = get_analysis_subsets.get_samples_by_bact_id(run_sheet, self.mads_report,
                                                                   organism_ids = ["1", "3"],
                                                                   lis_data_names = self.lis_names_da)
        assert true_samples == test_samples

    def test_no_result(self):
        run_sheet = pd.DataFrame(data = {"sample": ["1199123456-1", "1199234567-2"]})
        test_samples = get_analysis_subsets.get_samples_by_bact_id(run_sheet, self.mads_report,
                                                                   organism_ids = ["10"],
                                                                   lis_data_names = self.lis_names_da)
        assert not test_samples
