import unittest

import pandas as pd
import pytest

import look_up_size


class TestGetExpectedSize(unittest.TestCase):
    def test_get_size_one_hit(self):
        # TODO: are there even ones with only one hit?
        pass

    def test_get_size_multi_hits(self):
        true_size = 4349809.0
        test_size = look_up_size.get_expected_size_by_name("Fictibacillus barbaricus")
        assert test_size == true_size

        nocardia_true_size = 6194645.0
        nocardia_test_size = look_up_size.get_expected_size_by_name("Nocardia cyriacigeorgica")
        assert nocardia_test_size == nocardia_true_size

    def test_no_hit(self):
        """Fail if the organism wasn't found in the database."""
        error_msg = "No expected size found for organism Testobacter fakei"
        with pytest.raises(KeyError, match=error_msg):
            look_up_size.get_expected_size_by_name("Testobacter fakei")

    def test_no_species(self):
        """Fail if no organism was given."""
        error_msg = "No species given."
        with pytest.raises(ValueError, match=error_msg):
            look_up_size.get_expected_size_by_name(pd.NA)
