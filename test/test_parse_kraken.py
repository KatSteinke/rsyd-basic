#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib
import re
import unittest

import pandas as pd
import pytest

import parse_kraken


class TestParseKrakenReport(unittest.TestCase):
    def test_parse_report(self):
        kraken_report = pathlib.Path(__file__).parent / "data" / "parse_kraken" / "kraken_report.tsv"
        true_report = pd.DataFrame(data={"percent_reads_covered": [20.00, 80.00, 80.00],
                                         "number_reads_covered": [20, 80, 80],
                                         "number_reads_exactly_in_tax": [20, 10, 9],
                                         "taxonomic_level": ["U", "R", "R1"],
                                         "NCBI_rank_ID": ["0", "1", "131567"],
                                         "taxon_name": ["unclassified",
                                                        "root",
                                                        "cellular organisms"]})
        test_report = parse_kraken.parse_kraken_report(kraken_report)
        pd.testing.assert_frame_equal(test_report, true_report)


class TestFindMaxForTaxon(unittest.TestCase):
    def test_find_best_match(self):
        kraken_report = pd.DataFrame(data={"percent_reads_covered": [20.00,
                                                                     80.00,
                                                                     80.00,
                                                                     49.00,
                                                                     50.00],
                                           "number_reads_covered": [20,
                                                                    80,
                                                                    80,
                                                                    49,
                                                                    50],
                                           "number_reads_exactly_in_tax": [20,
                                                                           10,
                                                                           9,
                                                                           49,
                                                                           50],
                                           "taxonomic_level": ["U",
                                                               "R",
                                                               "R1",
                                                               "G",
                                                               "G"],
                                           "NCBI_rank_ID": ["0",
                                                            "1",
                                                            "131567",
                                                            "X",
                                                            "Y"],
                                           "taxon_name": ["unclassified",
                                                          "root",
                                                          "cellular organisms",
                                                          "Testobacter",
                                                          "Testococcus"]})
        true_result = pd.DataFrame(data={"percent_reads_covered": [50.0],
                                         "number_reads_covered": [50],
                                         "number_reads_exactly_in_tax": [50],
                                         "taxonomic_level": ["G"],
                                         "NCBI_rank_ID": ["Y"],
                                         "taxon_name": ["Testococcus"]})
        test_result = parse_kraken.find_best_match_in_kraken(kraken_report, "G")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_invalid_level(self):
        report = pd.DataFrame(data = {"percent_reads_covered": [20.00, 80.00, 80.00],
                                      "number_reads_covered": [20, 80, 80],
                                      "number_reads_exactly_in_tax": [20, 10, 9],
                                      "taxonomic_level": ["U", "R", "R1"],
                                      "NCBI_rank_ID": ["0", "1", "131567"],
                                      "taxon_name": ["unclassified",
                                                     "root",
                                                     "cellular organisms"]})
        error_message = "Taxonomic level A is not a valid level. " \
                        "Levels must be specified as U, R, D, K, P, C, O, F, G or S, optionally" \
                        "followed by a number for a sublevel."
        with pytest.raises(ValueError, match=re.escape(error_message)):
            parse_kraken.find_best_match_in_kraken(report, taxo_level = "A")

    def test_one_blank(self):
        # make sure we get a blank dataframe back if we don't find anything
        true_result = pd.DataFrame(data = {"percent_reads_covered": [pd.NA],
                                           "number_reads_covered": [pd.NA],
                                           "number_reads_exactly_in_tax": [pd.NA],
                                           "taxonomic_level": [pd.NA],
                                           "NCBI_rank_ID": [pd.NA],
                                           "taxon_name": [pd.NA]})
        kraken_report = pd.DataFrame(data = {"percent_reads_covered": [20.00,
                                                                       80.00,
                                                                       80.00,
                                                                       49.00,
                                                                       50.00],
                                             "number_reads_covered": [20,
                                                                      80,
                                                                      80,
                                                                      49,
                                                                      50],
                                             "number_reads_exactly_in_tax": [20,
                                                                             10,
                                                                             9,
                                                                             49,
                                                                             50],
                                             "taxonomic_level": ["U",
                                                                 "R",
                                                                 "R1",
                                                                 "G",
                                                                 "G"],
                                             "NCBI_rank_ID": ["0",
                                                              "1",
                                                              "131567",
                                                              "X",
                                                              "Y"],
                                             "taxon_name": ["unclassified",
                                                            "root",
                                                            "cellular organisms",
                                                            "Testobacter",
                                                            "Testococcus"]})
        test_result = parse_kraken.find_best_match_in_kraken(kraken_report, "S")
        pd.testing.assert_frame_equal(true_result, test_result)

class TestCombineAll(unittest.TestCase):
    def setUp(self) -> None:
        self.kraken_hit_fakeococcus = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                                 / "F99123456-1_report.tsv"
        self.kraken_hit_fakeobacter = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                                 / "F99654321-1_report.tsv"
        self.kraken_hit_blank = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                                 / "F99111111-1_report.tsv"
        self.sample_number_format = "([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}"

    def test_make_overview_all_hits(self):
        true_result = pd.DataFrame(data={"WGS_species_kraken": ["Fakeococcus fakei",
                                                                "Fakeobacter testensis"],
                                         "WGS_species_kraken_%": [80.0, 70.0],
                                         "WGS_genus_kraken": ["Fakeococcus", "Fakeobacter"],
                                         "WGS_genus_kraken_%": [90.0, 80.0],
                                         "proevenr": ["F99123456-1", "F99654321-1"]})
        samples = [self.kraken_hit_fakeococcus, self.kraken_hit_fakeobacter]
        test_result = parse_kraken.make_kraken_overview(samples, self.sample_number_format)
        pd.testing.assert_frame_equal(test_result, true_result, check_like=True)

    def test_make_overview_one_blank(self):
        true_result = pd.DataFrame(data = {"WGS_species_kraken": ["Fakeococcus fakei",
                                                                  pd.NA,
                                                                  "Fakeobacter testensis"],
                                           "WGS_species_kraken_%": [80.0, pd.NA, 70.0],
                                           "WGS_genus_kraken": ["Fakeococcus", "Fakeomyces",
                                                                "Fakeobacter"],
                                           "WGS_genus_kraken_%": [90.0, 20.0, 80.0],
                                           "proevenr": ["F99123456-1",
                                                        "F99111111-1",
                                                        "F99654321-1"]})
        samples = [self.kraken_hit_fakeococcus, self.kraken_hit_blank, self.kraken_hit_fakeobacter]
        test_result = parse_kraken.make_kraken_overview(samples, self.sample_number_format)
        print(test_result)
        print(true_result)
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_handle_missing_samplenumber(self):
        kraken_report = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                        / "kraken_report.tsv"
        samples = [self.kraken_hit_fakeococcus, kraken_report]
        error_msg = f"Sample number could not be inferred from name for {str(kraken_report)}"
        with pytest.raises(ValueError, match = error_msg):
            parse_kraken.make_kraken_overview(samples, self.sample_number_format)
