#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import pandas as pd
import pytest

import generate_reports
from get_analysis_subsets import SampleByIndication


class TestSortNumberedCols(unittest.TestCase):
    def test_sort_numbered_cols(self):
        toxin_cols = ["O_WGS Toxinpåvisning_0", "O_WGS Toxinpåvisning_1",
                      "O_WGS Toxinpåvisning_15", "O_WGS Toxinpåvisning_3"]
        sorted_cols = ["O_WGS Toxinpåvisning_0", "O_WGS Toxinpåvisning_1", "O_WGS Toxinpåvisning_3",
                       "O_WGS Toxinpåvisning_15"]
        sorted_test = generate_reports.sort_numbered_cols(toxin_cols, "O_WGS Toxinpåvisning_")
        assert sorted_test == sorted_cols


class TestSortForMads(unittest.TestCase):
    def test_catch_one_missing(self):
        input_cols = pd.Index(["proevenr", "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                               "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                               "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                               "O_WGS MLST", "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS Toxinpåvisning_0", "O_WGS Plasmid-ID_0"])
        error_msg = "Columns ['O_WGS supplerende type'] are missing from sample report."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            generate_reports.sort_for_mads(input_cols)

    def test_catch_multiple_missing(self):
        input_cols = pd.Index(["O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                               "O_WGS MLST",
                               "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                               "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                               "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS Toxinpåvisning_0", "O_WGS Plasmid-ID_0"])
        error_msg = "Columns ['O_WGS supplerende type', 'proevenr'] are missing from sample report."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            generate_reports.sort_for_mads(input_cols)

    def test_catch_no_toxins(self):
        input_cols = pd.Index(["proevenr", "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                               "O_WGS MLST",
                               "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                               "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                               "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS supplerende type", "O_WGS Plasmid-ID_0"])
        error_msg = "Report has been initialized without toxin gene columns."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            generate_reports.sort_for_mads(input_cols)

    def test_catch_no_plasmids(self):
        input_cols = pd.Index(["proevenr", "O_WGS Toxinpåvisning_0", "O_WGS Toxinpåvisning_1",
                               "O_WGS Toxinpåvisning_15", "O_WGS Toxinpåvisning_3",
                               "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                               "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                               "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                               "O_WGS MLST", "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS supplerende type"])
        error_msg = "Report has been initialized without plasmid columns."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            generate_reports.sort_for_mads(input_cols)

    def test_sort_success(self):
        input_cols = pd.Index(["proevenr", "O_WGS Toxinpåvisning_0", "O_WGS Toxinpåvisning_1",
                               "O_WGS Toxinpåvisning_15", "O_WGS Toxinpåvisning_3",
                               "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                               "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                               "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                               "O_WGS MLST", "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS supplerende type", "O_WGS Plasmid-ID_0"])
        sorted_cols = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identifikation",
                       "O_WGS MLST", "O_WGS Plasmid-ID_0",
                       "O_WGS res.gen anden beta-lactamase", "O_WGS res.gen carbapenemase",
                       "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                       "O_WGS Serotype/gruppe", "O_WGS Spatype", "O_WGS ST",
                       "O_WGS supplerende type", "O_WGS Toxinpåvisning_0",
                       "O_WGS Toxinpåvisning_1",
                       "O_WGS Toxinpåvisning_3", "O_WGS Toxinpåvisning_15"]
        test_sort = generate_reports.sort_for_mads(input_cols)
        assert test_sort == sorted_cols

    def test_sort_success_translated(self):
        """Sort row names in English."""
        input_cols = pd.Index(["sample_number", "O_WGS toxin identification_0", "O_WGS toxin identification_1",
                               "O_WGS toxin identification_15", "O_WGS toxin identification_3",
                               "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identification",
                               "O_WGS res.gene other beta-lactamase", "O_WGS res.gene carbapenemase",
                               "O_WGS res.gene colistin", "O_WGS res.gene vancomycin",
                               "O_WGS MLST", "O_WGS Serotype/group", "O_WGS Spatype", "O_WGS ST",
                               "O_WGS supplementary type", "O_WGS Plasmid-ID_0"])
        sorted_cols = ["sample_number", "O_WGS CC-type", "O_WGS cgMLST", "O_WGS identification",
                       "O_WGS MLST", "O_WGS Plasmid-ID_0",
                       "O_WGS res.gene other beta-lactamase", "O_WGS res.gene carbapenemase",
                       "O_WGS res.gene colistin", "O_WGS res.gene vancomycin",
                       "O_WGS Serotype/group", "O_WGS Spatype", "O_WGS ST",
                       "O_WGS supplementary type", "O_WGS toxin identification_0",
                       "O_WGS toxin identification_1",
                       "O_WGS toxin identification_3", "O_WGS toxin identification_15"]
        test_sort = generate_reports.sort_for_mads(input_cols, report_language = "EN")
        assert test_sort == sorted_cols


class TestGenerateGenericReport(unittest.TestCase):
    def test_report_success(self):
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        sample_number = "1199123456-1"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_success_en(self):
        """Translate the column names if needed."""
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        sample_number = "1199123456-1"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene carbapenemase": [pd.NA],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report,
                                                        target_language = "EN")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_handle_blank_gtdb(self):
        """Handle blank GTDB identification."""
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb_na.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        sample_number = "1199123456-1"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Unclassified"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_clean_placeholder_names(self):
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb_placeholder.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"

        sample_number = "1199123456-1"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_clean_placeholder_long(self):
        """Ensure that longer placeholders (>1 character) are handled properly."""
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb_placeholder_long.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"

        sample_number = "1199123456-1"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_handle_missing_sample(self):
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        sample_number = "1199123456-9"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-9"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Unclassified"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.get_basic_report(sample_number, gtdb_report, mlst_report)
        pd.testing.assert_frame_equal(true_result, test_result)


class TestMakeModularReport(unittest.TestCase):
    def test_report_salmonella(self):
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Salmonella enterica")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_salmo.tsv"
        seqsero_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                         / "seqsero_report.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Salmonella enterica"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": ["I 4,[5],12:i:-"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": ["Salmonella enterica "
                                                                      "subspecies enterica "
                                                                      "(subspecies I)"],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           serotype_report = seqsero_report)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_report_salmonella_en(self):
        """Generate a translated report for Salmonella."""
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Salmonella enterica")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_salmo.tsv"
        seqsero_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                         / "seqsero_report.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Salmonella enterica"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": [pd.NA],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": ["I 4,[5],12:i:-"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": ["Salmonella enterica "
                                                                        "subspecies enterica "
                                                                        "(subspecies I)"],
                                           "O_WGS toxin identification_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           serotype_report = seqsero_report,
                                                           target_language = "EN")
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_report_ecoli(self):
        # TODO: test actual hits!
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Escherichia coli",
                                    resistance = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_ecoli.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success-abritamr.txt"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        serotypefinder_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                                / "serotypefinder_tab.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Escherichia coli"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": ["O1:H1"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report,
                                                           serotype_report = serotypefinder_report)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_report_ecoli_en(self):
        """Handle a report for E. coli in English."""
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Escherichia coli",
                                    resistance = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_ecoli.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success-abritamr.txt"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        serotypefinder_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                                / "serotypefinder_tab.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Escherichia coli"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": [pd.NA],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": ["O1:H1"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report,
                                                           serotype_report = serotypefinder_report,
                                                           target_language = "EN")
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_handle_blank_serotypefinder(self):
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Escherichia coli",
                                    resistance = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_ecoli.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success-abritamr.txt"
        serotypefinder_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                                / "serotypefinder_blank.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Escherichia coli"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report,
                                                           serotype_report = serotypefinder_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_handle_empty_serotypefinder(self):
        sample = SampleByIndication(sample_number = "1199123456-1", species = "Escherichia coli")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_ecoli.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        serotypefinder_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                                / "serotypefinder_empty.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Escherichia coli"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report,
                                                               serotype_report = serotypefinder_report)
            log_msg = "Serotype report for sample 1199123456-1 is empty."
            assert ("generate_reports", logging.WARNING, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_staph(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Staphylococcus aureus",
                                    resistance = True)
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "fake_gtdb_staph.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success-abritamr.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Staphylococcus aureus"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_staph_realhit(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Staphylococcus aureus",
                                    resistance = True)
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb_staph.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success_abritamr_realhit.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Staphylococcus aureus"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_staph_realhit_en(self):
        """Handle a translated report with antimicrobial resistance genes."""
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Staphylococcus aureus",
                                    resistance = True)
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb_staph.tsv"
        abritamr_report = pathlib.Path(__file__).parent / "data" / "generate_report" / \
                          "success_abritamr_realhit.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Staphylococcus aureus"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           resistance_report = abritamr_report,
                                                           target_language = "EN")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_generic(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_generic_translate(self):
        """Generate a translated report."""
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": [pd.NA],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report, target_language = "EN")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_sort_plasmids(self):
        """Ensure plasmids are output in sorted order."""
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "plasmids_to_sort.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncFIB(K);IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_handle_blank_resistance(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei",
                                    resistance = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        resistance_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                            / "blank_abritamr.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report,
                                                               resistance_report = resistance_report)
            log_msg = "No resistance genes reported for sample 1199123456-1."
            assert ("generate_reports", logging.WARNING, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_fail_no_resistance(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Staphylococcus aureus",
                                    resistance = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb_staph.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        with pytest.raises(FileNotFoundError,
                           match = "Resistance should be determined for 1199123456-1"
                                   "but no report was supplied."):
            generate_reports.make_modular_report(sample, gtdb_report, mlst_report, plasmid_report)

    def test_report_toxin(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei", toxin = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        toxin_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "success_toxin.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": ["eae"]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           virulence_report = toxin_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_report_toxin_en(self):
        """Report toxins in a non-default language."""
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei", toxin = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        toxin_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "success_toxin.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": [pd.NA],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": ["eae"]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report,
                                                           virulence_report = toxin_report,
                                                           target_language = "en")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_handle_blank_virulence(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei",
                                    toxin = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        toxin_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                       / "blank_toxin.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report,
                                                               virulence_report = toxin_report)
            log_msg = "No toxin genes reported for sample 1199123456-1."
            assert ("generate_reports", logging.WARNING, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_catch_empty_toxins(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei",
                                    toxin = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        toxin_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                       / "empty_toxin.txt"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report,
                                                               virulence_report = toxin_report)
            log_msg = "No toxin genes reported for sample 1199123456-1."
            assert ("generate_reports", logging.WARNING, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_handle_empty_plasmids(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "empty_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report)
        pd.testing.assert_frame_equal(true_result, test_result, check_dtype = False,
                                      check_index_type = False)

    def test_handle_blank_plasmids(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "blank_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                           plasmid_report)
        pd.testing.assert_frame_equal(true_result, test_result, check_dtype = False,
                                      check_index_type = False)

    def test_handle_all_blank(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                        / "F99123456-1_report_empty.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "blank_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "blank_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Unclassified"],
                                           "O_WGS MLST": ["-"],
                                           "O_WGS Plasmid-ID_0": [pd.NA],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        warn_msg = "No species found for 1199123456-1"
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report)
            assert ("generate_reports", logging.WARNING, warn_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False)

    def test_handle_blank_species(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "parse_kraken" \
                        / "F99123456-1_report_empty.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Unclassified"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        warn_msg = "No species found for 1199123456-1"
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            test_result = generate_reports.make_modular_report(sample, gtdb_report, mlst_report,
                                                               plasmid_report)
            assert ("generate_reports", logging.WARNING, warn_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_fail_no_toxin_file(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Staphylococcus aureus",
                                    toxin = True)
        gtdb_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb_staph.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        with pytest.raises(FileNotFoundError,
                           match = "Toxins should be determined for 1199123456-1"
                                   "but no report was supplied."):
            generate_reports.make_modular_report(sample, gtdb_report, mlst_report, plasmid_report)

    def test_fail_unsupported_serotyping(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Fakeococcus fakei")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        serotypefinder_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                                / "serotypefinder_blank.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        error_msg = "Serotyping report given for sample 1199123456-1 " \
                    "(species: Fakeococcus fakei)." \
                    f"Serotyping reports are only supported for E. coli " \
                    f"and Salmonella."
        with pytest.raises(NotImplementedError, match = re.escape(error_msg)):
            generate_reports.make_modular_report(sample, gtdb_report, mlst_report, plasmid_report,
                                                 serotype_report = serotypefinder_report)

    def test_warn_wrong_species(self):
        sample = SampleByIndication(sample_number = "1199123456-1",
                                    species = "Salmonella enterica")
        gtdb_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb.tsv"
        mlst_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mlst.tsv"
        seqsero_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                         / "seqsero_report.tsv"
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            warn_msg = "Species should be Salmonella enterica, is Fakeococcus fakei."
            generate_reports.make_modular_report(sample, gtdb_report, mlst_report, plasmid_report,
                                                 serotype_report = seqsero_report)
            assert ("generate_reports", logging.WARNING, warn_msg) in self._caplog.record_tuples


class TestSetupAndReport(unittest.TestCase):
    def setUp(self) -> None:
        self.mads_data = pd.DataFrame(data = {"proevenr": ["1199123456-3",
                                                           "1199123456-2",
                                                           "1199123456-1"],
                                              # TODO: use our own codes or just fakes?
                                              "LOKAL_BAKT_KODE": ["1",
                                                                  "2",
                                                                  "999"]})
        self.species_encoding = pd.DataFrame(data = {"BAKTERIE": ["1", "2", "3", "999"],
                                                     "bakterie_kategori": ["Escherichia coli",
                                                                           "Salmonella enterica",
                                                                           "Staphylococcus aureus",
                                                                           "Fakeococcus fakei"]})

    def test_setup_generic(self):
        sample = "1199123456-1"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.setup_sample_and_report(sample, gtdb_report, abritamr_report,
                                                               mlst_report, plasmid_report,
                                                               self.mads_data,
                                                               self.species_encoding)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_translate_generic(self):
        """Set up a translated version of the generic report."""
        sample = "1199123456-1"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                          / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        config_en = {"language": "en", "input_names": pathlib.Path(__file__).parent
                                                        / "data" / "localization" / "input_da.yaml"}
        test_result = generate_reports.setup_sample_and_report(sample, gtdb_report, abritamr_report,
                                                               mlst_report, plasmid_report,
                                                               self.mads_data,
                                                               self.species_encoding,
                                                               active_config = config_en)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_setup_ecoli(self):
        sample = "1199123456-3"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                          / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        serotypefinder_report = str(pathlib.Path(__file__).parent / "data" / "generate_report"
                                    / "serotypefinder_tab.tsv")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-3"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Escherichia coli"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": ["O1:H1"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.setup_sample_and_report(sample, gtdb_report, abritamr_report,
                                                               mlst_report, plasmid_report,
                                                               self.mads_data,
                                                               self.species_encoding,
                                                               serotyping_report = serotypefinder_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_setup_salmo(self):
        sample = "1199123456-2"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                          / "fake_gtdb.tsv")
        seqsero_report = str(pathlib.Path(__file__).parent / "data" / "generate_report"
                             / "seqsero_report.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-2"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Salmonella enterica"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": ["I 4,[5],12:i:-"],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": ["Salmonella enterica "
                                                                      "subspecies enterica "
                                                                      "(subspecies I)"],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = generate_reports.setup_sample_and_report(sample, gtdb_report, abritamr_report,
                                                               mlst_report, plasmid_report,
                                                               self.mads_data,
                                                               self.species_encoding,
                                                               serotyping_report = seqsero_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_setup_toxin_genes(self):
        sample = "1199123456-1"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                      / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        virulence_report = str(pathlib.Path(__file__).parent / "data" / "generate_report"
                               / "success_toxin.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": ["eae"]})
        test_result = generate_reports.setup_sample_and_report(sample, gtdb_report, abritamr_report,
                                                               mlst_report, plasmid_report,
                                                               self.mads_data,
                                                               self.species_encoding,
                                                               toxin_report = virulence_report)
        pd.testing.assert_frame_equal(true_result, test_result)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_fallback_invalid_code(self):
        mads_data = pd.DataFrame(data = {"proevenr": ["1199123456-3",
                                                      "1199123456-2",
                                                      "1199123456-1"],
                                         # TODO: use our own codes or just fakes?
                                         "LOKAL_BAKT_KODE": ["1",
                                                             "2",
                                                             "1000"]})
        sample = "1199123456-1"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                          / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identifikation": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gen anden beta-lactamase": [pd.NA],
                                           "O_WGS res.gen carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA],
                                           "O_WGS Serotype/gruppe": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplerende type": [pd.NA],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            warn_msg = "Bacteria code 1000 not in translation table. " \
                       "Proceeding with generic species for sample 1199123456-1."
            test_result = generate_reports.setup_sample_and_report(sample, gtdb_report,
                                                                   abritamr_report, mlst_report,
                                                                   plasmid_report, mads_data,
                                                                   self.species_encoding)
            assert ("generate_reports", logging.WARNING, warn_msg) in self._caplog.record_tuples
            pd.testing.assert_frame_equal(true_result, test_result)

    def test_fallback_invalid_code_translate(self):
        """Handle invalid bacteria codes when the input contains non-default column names."""
        mads_data = pd.DataFrame(data = {"proevenr": ["1199123456-3",
                                                      "1199123456-2",
                                                      "1199123456-1"],
                                         # TODO: use our own codes or just fakes?
                                         "LOCAL_BACT_CODE": ["1",
                                                             "2",
                                                             "1000"]})
        species_encoding = pd.DataFrame(data = {"bacteria_code": ["1", "2", "3", "999"],
                                                "bacteria_category": ["Escherichia coli",
                                                                      "Salmonella enterica",
                                                                      "Staphylococcus aureus",
                                                                      "Fakeococcus fakei"]})
        test_config = {"language": "en", "input_names": pathlib.Path(__file__).parent
                                                        / "data" / "localization" / "input_en.yaml"}
        sample = "1199123456-1"
        gtdb_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" \
                          / "fake_gtdb.tsv")
        mlst_report = str(pathlib.Path(__file__).parent / "data" / "merge_results"
                          / "fake_mlst.tsv")
        abritamr_report = str(pathlib.Path(__file__).parent / "data" / "generate_report" /
                              "success_abritamr_realhit.txt")
        plasmid_report = str(pathlib.Path(__file__).parent / "data" / "merge_results" /
                             "fake_plasmids.tsv")
        true_result = pd.DataFrame(data = {"sample_number": ["1199123456-1"],
                                           "O_WGS CC-type": [pd.NA],
                                           "O_WGS cgMLST": [pd.NA],
                                           "O_WGS identification": ["Fakeococcus fakei"],
                                           "O_WGS MLST": ["1"],
                                           "O_WGS Plasmid-ID_0": ["IncX3"],
                                           "O_WGS res.gene carbapenemase": ["xyz(A)"],
                                           "O_WGS res.gene colistin": [pd.NA],
                                           "O_WGS res.gene other beta-lactamase": [pd.NA],
                                           "O_WGS res.gene vancomycin": [pd.NA],
                                           "O_WGS Serotype/group": [pd.NA],
                                           "O_WGS Spatype": [pd.NA],
                                           "O_WGS ST": [pd.NA],
                                           "O_WGS supplementary type": [pd.NA],
                                           "O_WGS toxin identification_0": [pd.NA]})
        with self._caplog.at_level(level="WARNING", logger="generate_reports"):
            warn_msg = "Bacteria code 1000 not in translation table. " \
                       "Proceeding with generic species for sample 1199123456-1."
            test_result = generate_reports.setup_sample_and_report(sample, gtdb_report,
                                                                   abritamr_report, mlst_report,
                                                                   plasmid_report, mads_data,
                                                                   species_encoding,
                                                                   active_config = test_config)
            assert ("generate_reports", logging.WARNING, warn_msg) in self._caplog.record_tuples
            pd.testing.assert_frame_equal(true_result, test_result, check_column_type = False)
