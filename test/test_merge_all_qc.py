#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import numpy as np
import pandas as pd
import pytest

import merge_all_results

from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames

# set up some translations we're going to be using over and over
SHEET_NAMES_EN = RunsheetNames(sample_info_sheet = "Sample_information",
                               sample_number = "Sample_number",
                               extraction_well_number = "Extraction_well_number",
                               extraction_run_number = "Extraction_run_number",
                               dna_concentration = "DNA_concentration",
                               requested_by = "Requested_by",
                               comment = "Comment",
                               sequencing_run_number = "Sequencing_run_number",
                               indications = ["AMR", "Identification", "Relapse", "HAI",
                                              "Toxin gene identification",
                                              "Environmental sample isolate",
                                              "Outbreak sample isolate",
                                              "Surveillance sample isolate", "Research",
                                              "Other"],
                               dna_normalization_sheet = "DNA normalization",
                               ilm_well_number = "ILM_well_position",
                               dna_normalization_sample_nr = "Sample_number",
                               plate_layout = "Plate_layout",
                               runsheet_base = "Runsheet",
                               experiment_name = "RUNxxxx-INI",
                               nanopore_sheet_sample_number = "Sample_number",
                               barcode = "Barcode",
                               protocol_version = "Lab protocol version")
LIS_NAMES_EN = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                            isolate_number = "BACT_NR",
                            bacteria_code = "LOCAL_BACT_CODE",
                            bacteria_text = "LOCAL_BACT_TEXT")
BACT_NAMES_EN = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                     internal_bacteria_text = "bacteria_text",
                                     bacteria_category = "bacteria_category")


class TestFilterQuast(unittest.TestCase):
    def test_filter_quast(self):
        fake_quast_input = pd.DataFrame(data = {"Assembly": ["1199123456-1"],
                                                "# contigs (>= 0 bp)": [100],
                                                "# contigs (>= 1000 bp)": [100],
                                                "# contigs (>= 5000 bp)": [100],
                                                "# contigs (>= 10000 bp)": [20],
                                                "# contigs (>= 25000 bp)": [10],
                                                "# contigs (>= 50000 bp)": [5],
                                                "Total length (>= 0 bp)": [2000000],
                                                "Total length (>= 1000 bp)": [2000000],
                                                "Total length (>= 5000 bp)": [2000000],
                                                "Total length (>= 10000 bp)": [200000],
                                                "Total length (>= 25000 bp)": [200000],
                                                "Total length (>= 50000 bp)": [200000],
                                                "# contigs": [100],
                                                "Largest contig": [50000],
                                                "Total length": [2000000],
                                                "GC (%)": [50],
                                                "N50": [20000],
                                                "N75": [10000],
                                                "L50": [19],
                                                "L75": [25],
                                                "# total reads": [266666],
                                                "# left": [133333],
                                                "# right": [133333],
                                                "Mapped (%)": [75],
                                                "Properly paired (%)": [70],
                                                "Coverage >= 1x (%)": [100],
                                                "# N's per 100 kbp": [0],
                                                "# predicted genes (unique)": [2000],
                                                "# predicted genes (>= 0 bp)": [2000],
                                                "# predicted genes (>= 300 bp)": [1500],
                                                "# predicted genes (>= 1500 bp)": [20],
                                                "# predicted genes (>= 3000 bp)": [15]})
        true_output = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "# contigs": [100],
                                           "Total length": [2000000],
                                           "GC (%)": [50],
                                           "N50": [20000],
                                           "# predicted genes (unique)": [2000]})
        test_filtered_quast = merge_all_results.get_quast_data(fake_quast_input)
        pd.testing.assert_frame_equal(true_output, test_filtered_quast)


class TestGetDepth(unittest.TestCase):
    def test_illumina_coverage(self):
        # TODO: minimal or full example?
        sample_number = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        fake_illumina_coverage = pd.DataFrame(data = {"#ID": ["1199123456-1_contig1 cov=100",
                                                              "1199123456-1_contig2 cov=300"],
                                                      "Avg_fold": [100, 300],
                                                      "Length": [2000000,
                                                                 10000]})
        coverage_stats = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                              "Avg. coverage depth": [200.0]})
        test_coverage_stats = merge_all_results.get_illumina_read_stats(fake_illumina_coverage,
                                                                        sample_number)
        pd.testing.assert_frame_equal(test_coverage_stats, coverage_stats)


class TestFilterCheckm(unittest.TestCase):
    def test_filter_checkm(self):
        fake_checkm_input = pd.DataFrame(data = {"Bin Id": ["1199123456-1"],
                                                 "Marker lineage": ["g_Fakeococcus"],
                                                 "# genomes": [100],
                                                 "# markers": [1000],
                                                 "# marker sets": [300],
                                                 "0": [10], "1": [980], "2": [10],
                                                 "3": [0], "4": [0], "5+": [0],
                                                 "Completeness": [99], "Contamination": [1],
                                                 "Strain heterogeneity": [0]})
        true_output = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99], "Contamination": [1]})
        test_filtered_checkm = merge_all_results.get_checkm_data(fake_checkm_input)
        pd.testing.assert_frame_equal(true_output, test_filtered_checkm)


class TestParseRunsheet(unittest.TestCase):
    def test_single_indication(self):
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" / "runsheet_singleindication.xlsx"
        true_run_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                               "Oprensnings_kørselsnr": ["0000"],
                                               "Brønd_nr": ["A1"],
                                               "DNA_Konc": [4.0],
                                               "Kommentar": ["Omkørsel"],
                                               "Bestilt af": ["XYZ"],
                                               "Resistens": [False],
                                               "Identifikation": [True],
                                               "Relaps": [False],
                                               "Toxingen_ID": [False],
                                               "HAI": [False],
                                               "Miljøprøveisolat": [False],
                                               "Udbrudsisolat": [False],
                                               "Overvågningsisolat": [False],
                                               "Forskning": [False],
                                               "Andet": [False],
                                               "Grampos/neg": ["Negativ"],
                                               "SEK_Run_nr": ["0000"],
                                               "Indikation": ["Identifikation"]})
        test_result = merge_all_results.get_sheet_data(runsheet)
        pd.testing.assert_frame_equal(true_run_result, test_result)

    def test_multiple_indications(self):
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" / "runsheet.xlsx"
        true_run_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                               "Brønd_nr": ["A1"],
                                               "Bestilt af": ["XYZ"],
                                               "Kommentar": [pd.NA],
                                               "Resistens": [False],
                                               "Identifikation": [True],
                                               "Relaps": [False],
                                               "Toxingen_ID": [False],
                                               "HAI": [True],
                                               "Miljøprøveisolat": [False],
                                               "Udbrudsisolat": [False],
                                               "Overvågningsisolat": [False],
                                               "Forskning": [False],
                                               "Andet": [False],
                                               "SEK_Run_nr": ["0000"],
                                               "Indikation": ["Identifikation; HAI"]})
        test_result = merge_all_results.get_sheet_data(runsheet)
        pd.testing.assert_frame_equal(true_run_result, test_result, check_dtype = False)

    def test_multiple_indications_translate(self):
        """Extract multiple non-default indications from a
        runsheet with non-default column names."""
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" / "runsheet_en.xlsx"
        true_run_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                               "Extraction_well_number": ["A1"],
                                               "Requested_by": ["XYZ"],
                                               "Comment": [pd.NA],
                                               "AMR": [False],
                                               "Identification": [True],
                                               "Relapse": [False],
                                               "Toxin gene identification": [False],
                                               "HAI": [True],
                                               "Environmental sample isolate": [False],
                                               "Outbreak sample isolate": [False],
                                               "Surveillance sample isolate": [False],
                                               "Research": [False],
                                               "Other": [False],
                                               "Sequencing_run_number": ["0000"],
                                               "Indikation": ["Identification; HAI"]})
        test_result = merge_all_results.get_sheet_data(runsheet, runsheet_names = SHEET_NAMES_EN)
        pd.testing.assert_frame_equal(true_run_result, test_result, check_dtype = False)


class TestGetIlluminaLayout(unittest.TestCase):
    def test_get_layout(self):
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" / "runsheet.xlsx"
        true_result = pd.DataFrame(data={"proevenr": ["1199123456-1"], "ILM_Brønd": ["A1"]})
        test_result = merge_all_results.get_illumina_layout(runsheet)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_get_layout_en(self):
        """Handle non-default column names for the Illumina well position."""
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "runsheet_singleindication_en.xlsx"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"], "ILM_Brønd": ["A1"]})
        test_result = merge_all_results.get_illumina_layout(runsheet,
                                                            runsheet_names = SHEET_NAMES_EN)
        pd.testing.assert_frame_equal(true_result, test_result)


class TestMergeResults(unittest.TestCase):
    def test_merge_two(self):
        quast_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                             "# contigs": [100],
                                             "Total length": [2000000],
                                             "GC (%)": [50],
                                             "# predicted genes (unique)": [2000]})
        checkm_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                              "Marker lineage": ["g_Fakeococcus"],
                                              "# genomes": [100],
                                              "# markers": [1000],
                                              "Completeness": [99], "Contamination": [1]})
        true_merged = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "# contigs": [100],
                                           "Total length": [2000000],
                                           "GC (%)": [50],
                                           "# predicted genes (unique)": [2000],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99], "Contamination": [1]})
        test_merged = merge_all_results.merge_reports([quast_trimmed, checkm_trimmed])
        pd.testing.assert_frame_equal(true_merged, test_merged)

    def test_merge_all(self):
        quast_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                             "# contigs": [100],
                                             "Total length": [2000000],
                                             "GC (%)": [50],
                                             "# predicted genes (unique)": [2000]})
        checkm_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                              "Marker lineage": ["g_Fakeococcus"],
                                              "# genomes": [100],
                                              "# markers": [1000],
                                              "Completeness": [99], "Contamination": [1]})
        reads_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                             "Avg. coverage depth": [50]})
        true_merged = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "# contigs": [100],
                                           "Total length": [2000000],
                                           "GC (%)": [50],
                                           "# predicted genes (unique)": [2000],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99], "Contamination": [1],
                                           "Avg. coverage depth": [50.0]})
        test_merged = merge_all_results.merge_reports([quast_trimmed, checkm_trimmed, reads_trimmed])
        pd.testing.assert_frame_equal(true_merged, test_merged, check_dtype = False)

    def test_merge_some_missing(self):
        quast_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                             "# contigs": [100, 50],
                                             "Total length": [2000000, 2000000],
                                             "GC (%)": [50, 55],
                                             "# predicted genes (unique)": [2000, 1500]})
        checkm_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                              "Marker lineage": ["g_Fakeococcus", "g_Fakeobacter"],
                                              "# genomes": [100, 120],
                                              "# markers": [1000, 1200],
                                              "Completeness": [99, 100], "Contamination": [1, 0]})
        reads_trimmed = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                             "Avg. coverage depth": [50]})
        true_merged = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "# contigs": [100, 50],
                                           "Total length": [2000000, 2000000],
                                           "GC (%)": [50, 55],
                                           "# predicted genes (unique)": [2000, 1500],
                                           "Marker lineage": ["g_Fakeococcus", "g_Fakeobacter"],
                                           "# genomes": [100, 120],
                                           "# markers": [1000, 1200],
                                           "Completeness": [99, 100], "Contamination": [1, 0],
                                           "Avg. coverage depth": [50.0, pd.NA]})
        test_merged = merge_all_results.merge_reports([quast_trimmed, checkm_trimmed, reads_trimmed])
        pd.testing.assert_frame_equal(true_merged, test_merged, check_dtype = False)


class TestGetSerotype(unittest.TestCase):
    def test_get_serotype(self):
        mads_long = pathlib.Path(__file__).parent / "data" / "merge_results"\
                    / "fake_mads_results.tsv"
        true_result = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199123456-2"],
                                         "Serotypning": ["FakeSerotype", pd.NA]})
        test_result = merge_all_results.get_serotype_from_lis(mads_long)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_get_serotype_en(self):
        """Handle translated LIS reports."""
        mads_long = pathlib.Path(__file__).parent / "data" / "merge_results"\
                    / "fake_mads_results_en.tsv"
        true_result = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199123456-2"],
                                         "Serotypning": ["FakeSerotype", pd.NA]})
        test_result = merge_all_results.get_serotype_from_lis(mads_long, report_language = "EN")
        pd.testing.assert_frame_equal(true_result, test_result)


class TestRearrangeData(unittest.TestCase):
    test_config = {"platform": "MiSeq", "lab_info_system": {"use_lis_features": True}}

    def test_rename_from_mads(self):
        true_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                           "noter": [pd.NA],
                                           "pipeline_noter": [pd.NA],
                                           "final_id": [pd.NA],
                                           "proevenr": ["1199123456-1"],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["HAI"],
                                           "Kommentar": ["Omkørsel"],
                                           "N50_basics": [20000],
                                           "Avg. coverage depth_basics": [200.0],
                                           "Grampos/neg": ["Negativ"],
                                           "MADS species": ["Fakeococcus sp."],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Fakeococcus fakei"],
                                           "Serotypning": [pd.NA],
                                           "ANI_til_ref_basics": [97.0],
                                           "GTDB: kendt længde i bp": [pd.NA],
                                           "Total length_basics": [2000000],
                                           "Contamination": [1],
                                           "proevenr_quast": ["1199123456-1"],
                                           "# contigs": [100],
                                           "GC (%)": [50],
                                           "NG50": [20000],
                                           "Weighted coverage depth": [pd.NA],
                                           "longest_contig_coverage": [pd.NA],
                                           "# predicted genes (unique)": [2000],
                                           "proevenr_kraken": ["1199123456-1"],
                                           "WGS_genus_kraken": ["Fakeococcus"],
                                           "WGS_genus_kraken_%": [90.0],
                                           "WGS_species_kraken": ["Fakeococcus fakei"],
                                           "WGS_species_kraken_%": [80.0],
                                           "proevenr_gtdb": ["1199123456-1"],
                                           "WGS_genus_GTDB": ["Fakeococcus"],
                                           "WGS_species_GTDB": ["Fakeococcus fakei"],
                                           "ANI_til_ref": [97.0],
                                           "proevenr_checkm": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99],
                                           "proevenr_troubleshooting": ["1199123456-1"],
                                           "DNA_Konc": [4.0],
                                           "Oprensnings_kørselsnr": ["0000"],
                                           "Brønd_nr": ["A1"],
                                           "ILM_Brønd": [pd.NA],
                                           "SEK_Run_nr": ["0000"],
                                           "proevenr_runsheet": ["1112345699-1"],
                                           "pipeline_version": [merge_all_results.__version__],
                                           "platform": ["MiSeq"],
                                           "Lab_protocol": ["Version_1.2.3"]
                                           })
        test_input = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                          "proevenr_runsheet": ["1112345699-1"],
                                          "# contigs": [100],
                                          "Total length": [2000000],
                                          "GC (%)": [50],
                                          "N50": [20000],
                                          "NG50": [20000],
                                          "# predicted genes (unique)": [2000],
                                          "Avg. coverage depth": [200.0],
                                          "WGS_genus_kraken": ["Fakeococcus"],
                                          "WGS_genus_kraken_%": [90.0],
                                          "WGS_species_kraken": ["Fakeococcus fakei"],
                                          "WGS_species_kraken_%": [80.0],
                                          "Marker lineage": ["g_Fakeococcus"],
                                          "# genomes": [100],
                                          "# markers": [1000],
                                          "Completeness": [99], "Contamination": [1],
                                          "WGS_genus_GTDB": ["Fakeococcus"],
                                          "WGS_species_GTDB": ["Fakeococcus fakei"],
                                          "ANI_til_ref": [97.0],
                                          "DNA_Konc": [4.0],
                                          "Oprensnings_kørselsnr": ["0000"],
                                          "Brønd_nr": ["A1"],
                                          "run_navn": ["ILM_Run0000_Y20220101_XYZ"],
                                          "run_dato": ["20220101"],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["HAI"],
                                          "Grampos/neg": ["Negativ"],
                                          "LOKAL_BAKT_NAVN": ["Fakeococcus sp."],
                                          "Serotypning": [pd.NA],
                                          "SEK_Run_nr": ["0000"],
                                          "Lab_protocol": ["Version_1.2.3"],
                                          "Kommentar": ["Omkørsel"],
                                          })
        test_result = merge_all_results.rearrange_data(test_input, active_config = self.test_config)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_rename_translated(self):
        """Handle rearranging non-default column names where relevant."""
        true_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                           "noter": [pd.NA],
                                           "pipeline_noter": [pd.NA],
                                           "final_id": [pd.NA],
                                           "proevenr": ["1199123456-1"],
                                           "Requested_by": ["XYZ"],
                                           "Indikation": ["HAI"],
                                           "Comment": ["Rerun"],
                                           "N50_basics": [20000],
                                           "Avg. coverage depth_basics": [200.0],
                                           "Grampos/neg": ["Negativ"],
                                           "MADS species": ["Fakeococcus sp."],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Fakeococcus fakei"],
                                           "Serotypning": [pd.NA],
                                           "ANI_til_ref_basics": [97.0],
                                           "GTDB: kendt længde i bp": [pd.NA],
                                           "Total length_basics": [2000000],
                                           "Contamination": [1],
                                           "proevenr_quast": ["1199123456-1"],
                                           "# contigs": [100],
                                           "GC (%)": [50],
                                           "NG50": [20000],
                                           "Weighted coverage depth": [pd.NA],
                                           "longest_contig_coverage": [pd.NA],
                                           "# predicted genes (unique)": [2000],
                                           "proevenr_kraken": ["1199123456-1"],
                                           "WGS_genus_kraken": ["Fakeococcus"],
                                           "WGS_genus_kraken_%": [90.0],
                                           "WGS_species_kraken": ["Fakeococcus fakei"],
                                           "WGS_species_kraken_%": [80.0],
                                           "proevenr_gtdb": ["1199123456-1"],
                                           "WGS_genus_GTDB": ["Fakeococcus"],
                                           "WGS_species_GTDB": ["Fakeococcus fakei"],
                                           "ANI_til_ref": [97.0],
                                           "proevenr_checkm": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99],
                                           "proevenr_troubleshooting": ["1199123456-1"],
                                           "DNA_concentration": [4.0],
                                           "Extraction_run_number": ["0000"],
                                           "Extraction_well_number": ["A1"],
                                           "ILM_Brønd": [pd.NA],
                                           "Sequencing_run_number": ["0000"],
                                           "proevenr_runsheet": ["1112345699-1"],
                                           "pipeline_version": [merge_all_results.__version__],
                                           "platform": ["MiSeq"],
                                           "Lab_protocol": ["Version_1.2.3"]
                                           })
        test_input = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                          "proevenr_runsheet": ["1112345699-1"],
                                          "# contigs": [100],
                                          "Total length": [2000000],
                                          "GC (%)": [50],
                                          "N50": [20000],
                                          "NG50": [20000],
                                          "# predicted genes (unique)": [2000],
                                          "Avg. coverage depth": [200.0],
                                          "WGS_genus_kraken": ["Fakeococcus"],
                                          "WGS_genus_kraken_%": [90.0],
                                          "WGS_species_kraken": ["Fakeococcus fakei"],
                                          "WGS_species_kraken_%": [80.0],
                                          "Marker lineage": ["g_Fakeococcus"],
                                          "# genomes": [100],
                                          "# markers": [1000],
                                          "Completeness": [99], "Contamination": [1],
                                          "WGS_genus_GTDB": ["Fakeococcus"],
                                          "WGS_species_GTDB": ["Fakeococcus fakei"],
                                          "ANI_til_ref": [97.0],
                                          "DNA_concentration": [4.0],
                                          "Extraction_run_number": ["0000"],
                                          "Extraction_well_number": ["A1"],
                                          "run_navn": ["ILM_Run0000_Y20220101_XYZ"],
                                          "run_dato": ["20220101"],
                                          "Requested_by": ["XYZ"],
                                          "Indikation": ["HAI"],
                                          "Grampos/neg": ["Negativ"],
                                          "LOCAL_BACT_TEXT": ["Fakeococcus sp."],
                                          "Serotypning": [pd.NA],
                                          "Sequencing_run_number": ["0000"],
                                          "Lab_protocol": ["Version_1.2.3"],
                                          "Comment": ["Rerun"],
                                          })
        test_result = merge_all_results.rearrange_data(test_input, active_config = self.test_config,
                                                       runsheet_names = SHEET_NAMES_EN,
                                                       lis_data_names = LIS_NAMES_EN)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_handle_missing(self):
        true_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                           "noter": [pd.NA],
                                           "pipeline_noter": [pd.NA],
                                           "final_id": [pd.NA],
                                           "proevenr": ["1199123456-1"],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["HAI"],
                                           "Kommentar": ["Omkørsel"],
                                           "N50_basics": [20000],
                                           "Avg. coverage depth_basics": [200.0],
                                           "Grampos/neg": ["Negativ"],
                                           "MADS species": ["Fakeococcus sp."],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Fakeococcus fakei"],
                                           "Serotypning": [pd.NA],
                                           "ANI_til_ref_basics": [97.0],
                                           "GTDB: kendt længde i bp": [pd.NA],
                                           "Total length_basics": [2000000],
                                           "Contamination": [1],
                                           "proevenr_quast": ["1199123456-1"],
                                           "# contigs": [100],
                                           "GC (%)": [50],
                                           "NG50": [pd.NA],
                                           "Weighted coverage depth": [pd.NA],
                                           "longest_contig_coverage": [pd.NA],
                                           "# predicted genes (unique)": [2000],
                                           "proevenr_kraken": ["1199123456-1"],
                                           "WGS_genus_kraken": ["Fakeococcus"],
                                           "WGS_genus_kraken_%": [90.0],
                                           "WGS_species_kraken": ["Fakeococcus fakei"],
                                           "WGS_species_kraken_%": [80.0],
                                           "proevenr_gtdb": ["1199123456-1"],
                                           "WGS_genus_GTDB": ["Fakeococcus"],
                                           "WGS_species_GTDB": ["Fakeococcus fakei"],
                                           "ANI_til_ref": [97.0],
                                           "proevenr_checkm": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99],
                                           "proevenr_troubleshooting": ["1199123456-1"],
                                           "DNA_Konc": [4.0],
                                           "Oprensnings_kørselsnr": ["0000"],
                                           "Brønd_nr": ["A1"],
                                           "ILM_Brønd": [pd.NA],
                                           "SEK_Run_nr": [pd.NA],
                                           "proevenr_runsheet": ["1112345699-1"],
                                           "pipeline_version": [merge_all_results.__version__],
                                           "platform": ["MiSeq"],
                                           "Lab_protocol": [pd.NA]
                                           })
        test_input = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                          "proevenr_runsheet": ["1112345699-1"],
                                          "# contigs": [100],
                                          "Total length": [2000000],
                                          "GC (%)": [50],
                                          "N50": [20000],
                                          "Kommentar": ["Omkørsel"],
                                          "# predicted genes (unique)": [2000],
                                          "Avg. coverage depth": [200.0],
                                          "WGS_genus_kraken": ["Fakeococcus"],
                                          "WGS_genus_kraken_%": [90.0],
                                          "WGS_species_kraken": ["Fakeococcus fakei"],
                                          "WGS_species_kraken_%": [80.0],
                                          "Marker lineage": ["g_Fakeococcus"],
                                          "# genomes": [100],
                                          "# markers": [1000],
                                          "Completeness": [99], "Contamination": [1],
                                          "WGS_genus_GTDB": ["Fakeococcus"],
                                          "WGS_species_GTDB": ["Fakeococcus fakei"],
                                          "ANI_til_ref": [97.0],
                                          "DNA_Konc": [4.0],
                                          "Oprensnings_kørselsnr": ["0000"],
                                          "Brønd_nr": ["A1"],
                                          "run_navn": ["ILM_Run0000_Y20220101_XYZ"],
                                          "run_dato": ["20220101"],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["HAI"],
                                          "Grampos/neg": ["Negativ"],
                                          "LOKAL_BAKT_NAVN": ["Fakeococcus sp."]
                                          })
        test_result = merge_all_results.rearrange_data(test_input, active_config = self.test_config)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_no_mads(self):
        config_no_mads = {"platform": "MiSeq", "lab_info_system": {"use_lis_features": False}}
        true_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                           "noter": [pd.NA],
                                           "pipeline_noter": [pd.NA],
                                           "final_id": [pd.NA],
                                           "proevenr": ["1199123456-1"],
                                           "Bestilt af": ["XYZ"],
                                           "Indikation": ["HAI"],
                                           "Kommentar": ["Omkørsel"],
                                           "N50_basics": [20000],
                                           "Avg. coverage depth_basics": [200.0],
                                           "Grampos/neg": ["Negativ"],
                                           "MADS species": [pd.NA],
                                           "MADS: kendt længde i bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Fakeococcus fakei"],
                                           "Serotypning": [pd.NA],
                                           "ANI_til_ref_basics": [97.0],
                                           "GTDB: kendt længde i bp": [pd.NA],
                                           "Total length_basics": [2000000],
                                           "Contamination": [1],
                                           "proevenr_quast": ["1199123456-1"],
                                           "# contigs": [100],
                                           "GC (%)": [50],
                                           "NG50": [20000],
                                           "Weighted coverage depth": [pd.NA],
                                           "longest_contig_coverage": [pd.NA],
                                           "# predicted genes (unique)": [2000],
                                           "proevenr_kraken": ["1199123456-1"],
                                           "WGS_genus_kraken": ["Fakeococcus"],
                                           "WGS_genus_kraken_%": [90.0],
                                           "WGS_species_kraken": ["Fakeococcus fakei"],
                                           "WGS_species_kraken_%": [80.0],
                                           "proevenr_gtdb": ["1199123456-1"],
                                           "WGS_genus_GTDB": ["Fakeococcus"],
                                           "WGS_species_GTDB": ["Fakeococcus fakei"],
                                           "ANI_til_ref": [97.0],
                                           "proevenr_checkm": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99],
                                           "proevenr_troubleshooting": ["1199123456-1"],
                                           "DNA_Konc": [4.0],
                                           "Oprensnings_kørselsnr": ["0000"],
                                           "Brønd_nr": ["A1"],
                                           "ILM_Brønd": [pd.NA],
                                           "SEK_Run_nr": ["0000"],
                                           "proevenr_runsheet": ["1112345699-1"],
                                           "pipeline_version": [merge_all_results.__version__],
                                           "platform": ["MiSeq"],
                                           "Lab_protocol": [pd.NA]
                                           })
        test_input = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                          "proevenr_runsheet": ["1112345699-1"],
                                          "Kommentar": ["Omkørsel"],
                                          "# contigs": [100],
                                          "Total length": [2000000],
                                          "GC (%)": [50],
                                          "N50": [20000],
                                          "NG50": [20000],
                                          "# predicted genes (unique)": [2000],
                                          "Avg. coverage depth": [200.0],
                                          "WGS_genus_kraken": ["Fakeococcus"],
                                          "WGS_genus_kraken_%": [90.0],
                                          "WGS_species_kraken": ["Fakeococcus fakei"],
                                          "WGS_species_kraken_%": [80.0],
                                          "Marker lineage": ["g_Fakeococcus"],
                                          "# genomes": [100],
                                          "# markers": [1000],
                                          "Completeness": [99], "Contamination": [1],
                                          "WGS_genus_GTDB": ["Fakeococcus"],
                                          "WGS_species_GTDB": ["Fakeococcus fakei"],
                                          "ANI_til_ref": [97.0],
                                          "DNA_Konc": [4.0],
                                          "Oprensnings_kørselsnr": ["0000"],
                                          "Brønd_nr": ["A1"],
                                          "run_navn": ["ILM_Run0000_Y20220101_XYZ"],
                                          "run_dato": ["20220101"],
                                          "Bestilt af": ["XYZ"],
                                          "Indikation": ["HAI"],
                                          "Grampos/neg": ["Negativ"],
                                          "Serotypning": [pd.NA],
                                          "SEK_Run_nr": ["0000"]
                                          })
        test_result = merge_all_results.rearrange_data(test_input, active_config = config_no_mads)
        pd.testing.assert_frame_equal(test_result, true_result)


class TestTranslateHeaders(unittest.TestCase):
    test_input = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                       "noter": [pd.NA],
                                       "pipeline_noter": [pd.NA],
                                       "final_id": [pd.NA],
                                       "proevenr": ["1199123456-1"],
                                       "Bestilt af": ["XYZ"],
                                       "Indikation": ["HAI"],
                                       "Kommentar": ["Omkørsel"],
                                       "N50_basics": [20000],
                                       "Avg. coverage depth_basics": [200.0],
                                       "Grampos/neg": ["Negativ"],
                                       "MADS species": ["Fakeococcus sp."],
                                       "MADS: kendt længde i bp": [pd.NA],
                                       "WGS_species_GTDB_basics":
                                           ["Fakeococcus fakei"],
                                       "Serotypning": [pd.NA],
                                       "ANI_til_ref_basics": [97.0],
                                       "GTDB: kendt længde i bp": [pd.NA],
                                       "Total length_basics": [2000000],
                                       "Contamination": [1],
                                       "proevenr_quast": ["1199123456-1"],
                                       "# contigs": [100],
                                       "GC (%)": [50],
                                       "NG50": [20000],
                                       "Weighted coverage depth": [pd.NA],
                                       "longest_contig_coverage": [pd.NA],
                                       "# predicted genes (unique)": [2000],
                                       "proevenr_kraken": ["1199123456-1"],
                                       "WGS_genus_kraken": ["Fakeococcus"],
                                       "WGS_genus_kraken_%": [90.0],
                                       "WGS_species_kraken": ["Fakeococcus fakei"],
                                       "WGS_species_kraken_%": [80.0],
                                       "proevenr_gtdb": ["1199123456-1"],
                                       "WGS_genus_GTDB": ["Fakeococcus"],
                                       "WGS_species_GTDB": ["Fakeococcus fakei"],
                                       "ANI_til_ref": [97.0],
                                       "proevenr_checkm": ["1199123456-1"],
                                       "Marker lineage": ["g_Fakeococcus"],
                                       "# genomes": [100],
                                       "# markers": [1000],
                                       "Completeness": [99],
                                       "proevenr_troubleshooting": ["1199123456-1"],
                                       "DNA_Konc": [4.0],
                                       "Oprensnings_kørselsnr": ["0000"],
                                       "Brønd_nr": ["A1"],
                                       "ILM_Brønd": [pd.NA],
                                       "SEK_Run_nr": ["0000"],
                                       "proevenr_runsheet": ["1112345699-1"],
                                       "pipeline_version": [merge_all_results.__version__],
                                       "platform": ["MiSeq"],
                                       "Lab_protocol": ["Version_1.2.3"]
                                       })

    def test_fail_invalid_language(self):
        """Fail if an invalid language is specified."""
        error_msg = "tlh is not a supported language."
        with pytest.raises(NotImplementedError, match=error_msg):
            merge_all_results.rename_headers(self.test_input, target_language = "tlh")

    def test_success_no_translation(self):
        """Don't translate the original (Danish) strings."""
        test_from_default = merge_all_results.rename_headers(self.test_input)
        pd.testing.assert_frame_equal(test_from_default, self.test_input)
        test_translate = merge_all_results.rename_headers(self.test_input, target_language="da")
        pd.testing.assert_frame_equal(test_translate, self.test_input)

    def test_success_translate(self):
        """Translate to English."""
        true_results = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                            "should sequencing be redone": [pd.NA],
                                            "notes": [pd.NA],
                                            "pipeline_notes": [pd.NA],
                                            "final_id": [pd.NA],
                                            "sample_number": ["1199123456-1"],
                                            "Requested_by": ["XYZ"],
                                            "Indication": ["HAI"],
                                            "Comment": ["Omkørsel"],
                                            "N50_basics": [20000],
                                            "Avg. coverage depth_basics": [200.0],
                                            "Grampos/neg": ["Negativ"],
                                            "LIS species": ["Fakeococcus sp."],
                                            "LIS: known length in bp": [pd.NA],
                                            "WGS_species_GTDB_basics":
                                                ["Fakeococcus fakei"],
                                            "Serotyping": [pd.NA],
                                            "ANI_to_ref_basics": [97.0],
                                            "GTDB: known length in bp": [pd.NA],
                                            "Total length_basics": [2000000],
                                            "Contamination": [1],
                                            "sample_number_quast": ["1199123456-1"],
                                            "# contigs": [100],
                                            "GC (%)": [50],
                                            "NG50": [20000],
                                            "Weighted coverage depth": [pd.NA],
                                            "longest_contig_coverage": [pd.NA],
                                            "# predicted genes (unique)": [2000],
                                            "sample_number_kraken": ["1199123456-1"],
                                            "WGS_genus_kraken": ["Fakeococcus"],
                                            "WGS_genus_kraken_%": [90.0],
                                            "WGS_species_kraken": ["Fakeococcus fakei"],
                                            "WGS_species_kraken_%": [80.0],
                                            "sample_number_gtdb": ["1199123456-1"],
                                            "WGS_genus_GTDB": ["Fakeococcus"],
                                            "WGS_species_GTDB": ["Fakeococcus fakei"],
                                            "ANI_to_ref": [97.0],
                                            "sample_number_checkm": ["1199123456-1"],
                                            "Marker lineage": ["g_Fakeococcus"],
                                            "# genomes": [100],
                                            "# markers": [1000],
                                            "Completeness": [99],
                                            "sample_number_troubleshooting": ["1199123456-1"],
                                            "DNA_concentration": [4.0],
                                            "Extraction_run_number": ["0000"],
                                            "Extraction_well_number": ["A1"],
                                            "ILM_well_number": [pd.NA],
                                            "Sequencing_run_number": ["0000"],
                                            "sample_number_runsheet": ["1112345699-1"],
                                            "pipeline_version": [merge_all_results.__version__],
                                            "platform": ["MiSeq"],
                                            "Lab_protocol": ["Version_1.2.3"]
                                            })
        test_translate = merge_all_results.rename_headers(self.test_input, target_language = "en")
        pd.testing.assert_frame_equal(test_translate, true_results, check_dtype = False)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_skip_nonstandard(self):
        """Handle renaming columns when some of them already are non-default."""
        test_input = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                          "noter": [pd.NA],
                                          "pipeline_noter": [pd.NA],
                                          "final_id": [pd.NA],
                                          "proevenr": ["1199123456-1"],
                                          "Requested_by": ["XYZ"],
                                          "Indikation": ["HAI"],
                                          "Comment": ["Rerun"],
                                          "N50_basics": [20000],
                                          "Avg. coverage depth_basics": [200.0],
                                          "Grampos/neg": ["Negativ"],
                                          "MADS species": ["Fakeococcus sp."],
                                          "MADS: kendt længde i bp": [pd.NA],
                                          "WGS_species_GTDB_basics":
                                              ["Fakeococcus fakei"],
                                          "Serotypning": [pd.NA],
                                          "ANI_til_ref_basics": [97.0],
                                          "GTDB: kendt længde i bp": [pd.NA],
                                          "Total length_basics": [2000000],
                                          "Contamination": [1],
                                          "proevenr_quast": ["1199123456-1"],
                                          "# contigs": [100],
                                          "GC (%)": [50],
                                          "NG50": [20000],
                                          "Weighted coverage depth": [pd.NA],
                                          "longest_contig_coverage": [pd.NA],
                                          "# predicted genes (unique)": [2000],
                                          "proevenr_kraken": ["1199123456-1"],
                                          "WGS_genus_kraken": ["Fakeococcus"],
                                          "WGS_genus_kraken_%": [90.0],
                                          "WGS_species_kraken": ["Fakeococcus fakei"],
                                          "WGS_species_kraken_%": [80.0],
                                          "proevenr_gtdb": ["1199123456-1"],
                                          "WGS_genus_GTDB": ["Fakeococcus"],
                                          "WGS_species_GTDB": ["Fakeococcus fakei"],
                                          "ANI_til_ref": [97.0],
                                          "proevenr_checkm": ["1199123456-1"],
                                          "Marker lineage": ["g_Fakeococcus"],
                                          "# genomes": [100],
                                          "# markers": [1000],
                                          "Completeness": [99],
                                          "proevenr_troubleshooting": ["1199123456-1"],
                                          "DNA_concentration": [4.0],
                                          "Extraction_run_number": ["0000"],
                                          "Extraction_well_number": ["A1"],
                                          "ILM_Brønd": [pd.NA],
                                          "Sequencing_run_number": ["0000"],
                                          "proevenr_runsheet": ["1112345699-1"],
                                          "pipeline_version": [merge_all_results.__version__],
                                          "platform": ["MiSeq"],
                                          "Lab_protocol": ["Version_1.2.3"]
                                          })
        log_msg = ("Could not find columns ['Bestilt af', 'Brønd_nr',"
                   " 'DNA_Konc', 'Kommentar', 'Oprensnings_kørselsnr', "
                   "'SEK_Run_nr']; any equivalents were not translated.")
        true_results = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                            "should sequencing be redone": [pd.NA],
                                            "notes": [pd.NA],
                                            "pipeline_notes": [pd.NA],
                                            "final_id": [pd.NA],
                                            "sample_number": ["1199123456-1"],
                                            "Requested_by": ["XYZ"],
                                            "Indication": ["HAI"],
                                            "Comment": ["Rerun"],
                                            "N50_basics": [20000],
                                            "Avg. coverage depth_basics": [200.0],
                                            "Grampos/neg": ["Negativ"],
                                            "LIS species": ["Fakeococcus sp."],
                                            "LIS: known length in bp": [pd.NA],
                                            "WGS_species_GTDB_basics":
                                                ["Fakeococcus fakei"],
                                            "Serotyping": [pd.NA],
                                            "ANI_to_ref_basics": [97.0],
                                            "GTDB: known length in bp": [pd.NA],
                                            "Total length_basics": [2000000],
                                            "Contamination": [1],
                                            "sample_number_quast": ["1199123456-1"],
                                            "# contigs": [100],
                                            "GC (%)": [50],
                                            "NG50": [20000],
                                            "Weighted coverage depth": [pd.NA],
                                            "longest_contig_coverage": [pd.NA],
                                            "# predicted genes (unique)": [2000],
                                            "sample_number_kraken": ["1199123456-1"],
                                            "WGS_genus_kraken": ["Fakeococcus"],
                                            "WGS_genus_kraken_%": [90.0],
                                            "WGS_species_kraken": ["Fakeococcus fakei"],
                                            "WGS_species_kraken_%": [80.0],
                                            "sample_number_gtdb": ["1199123456-1"],
                                            "WGS_genus_GTDB": ["Fakeococcus"],
                                            "WGS_species_GTDB": ["Fakeococcus fakei"],
                                            "ANI_to_ref": [97.0],
                                            "sample_number_checkm": ["1199123456-1"],
                                            "Marker lineage": ["g_Fakeococcus"],
                                            "# genomes": [100],
                                            "# markers": [1000],
                                            "Completeness": [99],
                                            "sample_number_troubleshooting": ["1199123456-1"],
                                            "DNA_concentration": [4.0],
                                            "Extraction_run_number": ["0000"],
                                            "Extraction_well_number": ["A1"],
                                            "ILM_well_number": [pd.NA],
                                            "Sequencing_run_number": ["0000"],
                                            "sample_number_runsheet": ["1112345699-1"],
                                            "pipeline_version": [merge_all_results.__version__],
                                            "platform": ["MiSeq"],
                                            "Lab_protocol": ["Version_1.2.3"]
                                            })
        with self._caplog.at_level(level="DEBUG", logger="merge_results"):
            test_translate = merge_all_results.rename_headers(test_input,
                                                              target_language = "en")
            assert ("merge_results", logging.DEBUG, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(test_translate, true_results, check_dtype = False)


class TestParseAndMergeAll(unittest.TestCase):
    def setUp(self) -> None:
        self.fake_quast_input = [(pathlib.Path(__file__).parent / "data" / "merge_results"
                                  / "fake_quast.tsv")]
        self.fake_generic_coverage = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                     / "fake_illumina_cov.tsv"
        self.fake_nanopore_coverage = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                      / "fake_nanopore_cov.tsv"
        self.fake_checkm_input = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                 / "fake_checkm.tsv"
        self.fake_kraken = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_kraken.tsv"
        self.fake_gtdb = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_gtdb.tsv"
        self.runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "runsheet_singleindication.xlsx"

        self.standard_output = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                                    "noter": [pd.NA],
                                                    "pipeline_noter": [""],
                                                    "final_id": [pd.NA],
                                                    "proevenr": ["1199123456-1"],
                                                    "Bestilt af": ["XYZ"],
                                                    "Indikation": ["Identifikation"],
                                                    "Kommentar": ["Omkørsel"],
                                                    "N50_basics": [20000],
                                                    "Avg. coverage depth_basics": [200.0],
                                                    "Grampos/neg": ["Negativ"],
                                                    "MADS species": [pd.NA],
                                                    "MADS: kendt længde i bp": [pd.NA],
                                                    "WGS_species_GTDB_basics":
                                                        ["Fakeococcus fakei"],
                                                    "Serotypning": [pd.NA],
                                                    "ANI_til_ref_basics": [97.0],
                                                    "GTDB: kendt længde i bp": [pd.NA],
                                                    "Total length_basics": [2000000],
                                                    "Contamination": [1.0],
                                                    "proevenr_quast": ["1199123456-1"],
                                                    "# contigs": [100],
                                                    "GC (%)": [50.0],
                                                    "NG50": [pd.NA],
                                                    "Weighted coverage depth": [pd.NA],
                                                    "longest_contig_coverage": [pd.NA],
                                                    "# predicted genes (unique)": [2000],
                                                    "proevenr_kraken": ["1199123456-1"],
                                                    "WGS_genus_kraken": ["Fakeococcus"],
                                                    "WGS_genus_kraken_%": [90.0],
                                                    "WGS_species_kraken": ["Fakeococcus fakei"],
                                                    "WGS_species_kraken_%": [80.0],
                                                    "proevenr_gtdb": ["1199123456-1"],
                                                    "WGS_genus_GTDB": ["Fakeococcus"],
                                                    "WGS_species_GTDB": ["Fakeococcus fakei"],
                                                    "ANI_til_ref": [97.0],
                                                    "proevenr_checkm": ["1199123456-1"],
                                                    "Marker lineage": ["g_Fakeococcus"],
                                                    "# genomes": [100],
                                                    "# markers": [1000],
                                                    "Completeness": [99.0],
                                                    "proevenr_troubleshooting": ["1199123456-1"],
                                                    "DNA_Konc": [4.0],
                                                    "Oprensnings_kørselsnr": ["0000"],
                                                    "Brønd_nr": ["A1"],
                                                    "ILM_Brønd": ["A1"],
                                                    "SEK_Run_nr": ["0000"],
                                                    "proevenr_runsheet": ["1199123456-1"],
                                                    "pipeline_version":
                                                        [merge_all_results.__version__],
                                                    "platform": ["MiSeq"],
                                                    "Lab_protocol": ["Version_1.2.3"]
                                                    })
        self.standard_output = self.standard_output.astype({"# genomes": "Int64",
                                                            "# markers": "Int64",
                                                            "# contigs": "Int64",
                                                            "Total length_basics": "Int64",
                                                            "N50_basics": "Int64",
                                                            "# predicted genes (unique)": "Int64"
                                                            })
        self.experiment_name = "ILM_Run0000_Y20220101_XYZ"
        run_header = [self.experiment_name] * len(self.standard_output.columns)
        self.standard_output.columns = pd.MultiIndex.from_arrays([run_header,
                                                                  self.standard_output.columns])
        self.test_config = {"platform": "MiSeq",
                            "language": "da",
                            "sequencing_mode": "illumina",
                            "lab_info_system": {"use_lis_features": True},
                            "sample_number_settings": {
                                "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "sample_numbers_in": "number",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "number",
                                "number_to_letter": {"30": "B",
                                                     "10": "D",
                                                     "11": "F",
                                                     "50": "T"}}}

    def test_success_illumina_reads(self):
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_translate_names(self):
        """Give a translated dataframe if needed."""
        true_en = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                       "should sequencing be redone": [pd.NA],
                                       "notes": [pd.NA],
                                       "pipeline_notes": [""],
                                       "final_id": [pd.NA],
                                       "sample_number": ["1199123456-1"],
                                       "Requested_by": ["XYZ"],
                                       "Indication": ["Identification"],
                                       "Comment": ["Omkørsel"],
                                       "N50_basics": [20000],
                                       "Avg. coverage depth_basics": [200.0],
                                       "Grampos/neg": ["Negativ"],
                                       "LIS species": [pd.NA],
                                       "LIS: known length in bp": [pd.NA],
                                       "WGS_species_GTDB_basics":
                                           ["Fakeococcus fakei"],
                                       "Serotyping": [pd.NA],
                                       "ANI_to_ref_basics": [97.0],
                                       "GTDB: known length in bp": [pd.NA],
                                       "Total length_basics": [2000000],
                                       "Contamination": [1.0],
                                       "sample_number_quast": ["1199123456-1"],
                                       "# contigs": [100],
                                       "GC (%)": [50.0],
                                       "NG50": [pd.NA],
                                       "Weighted coverage depth": [pd.NA],
                                       "longest_contig_coverage": [pd.NA],
                                       "# predicted genes (unique)": [2000],
                                       "sample_number_kraken": ["1199123456-1"],
                                       "WGS_genus_kraken": ["Fakeococcus"],
                                       "WGS_genus_kraken_%": [90.0],
                                       "WGS_species_kraken": ["Fakeococcus fakei"],
                                       "WGS_species_kraken_%": [80.0],
                                       "sample_number_gtdb": ["1199123456-1"],
                                       "WGS_genus_GTDB": ["Fakeococcus"],
                                       "WGS_species_GTDB": ["Fakeococcus fakei"],
                                       "ANI_to_ref": [97.0],
                                       "sample_number_checkm": ["1199123456-1"],
                                       "Marker lineage": ["g_Fakeococcus"],
                                       "# genomes": [100],
                                       "# markers": [1000],
                                       "Completeness": [99.0],
                                       "sample_number_troubleshooting":
                                           ["1199123456-1"],
                                       "DNA_concentration": [4.0],
                                       "Extraction_run_number": ["0000"],
                                       "Extraction_well_number": ["A1"],
                                       "ILM_well_number": ["A1"],
                                       "Sequencing_run_number": ["0000"],
                                       "sample_number_runsheet": ["1199123456-1"],
                                       "pipeline_version":
                                           [merge_all_results.__version__],
                                       "platform": ["MiSeq"],
                                       "Lab_protocol": ["Version_1.2.3"]
                                       })
        config_en = {"platform": "MiSeq",
                            "language": "en",
                            "sequencing_mode": "illumina",
                            "lab_info_system": {"use_lis_features": True},
                            "sample_number_settings": {
                                "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "sample_numbers_in": "number",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "number",
                                "number_to_letter": {"30": "B",
                                                     "10": "D",
                                                     "11": "F",
                                                     "50": "T"}}}
        run_header = [self.experiment_name] * len(true_en.columns)
        true_en.columns = pd.MultiIndex.from_arrays([run_header, true_en.columns])
        read_results = self.fake_generic_coverage
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "runsheet_singleindication_en.xlsx"

        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = config_en,
                                                          runsheet_names = SHEET_NAMES_EN,
                                                          bacteria_names = BACT_NAMES_EN,
                                                          lis_data_names = LIS_NAMES_EN)
        pd.testing.assert_frame_equal(test_result, true_en, check_dtype = False)

    def test_success_illumina_translate_runsheet(self):
        """Translate Illumina runsheet format if needed."""
        test_config = {"platform": "MiSeq",
                       "sequencing_mode": "illumina",
                       "language": "da",
                       "lab_info_system": {"use_lis_features": True},
                       "sample_number_settings": {
                           "format_in_sheet": r'(?P<sample_type>[135]0|11)(?P<sample_number>\d{6})(?P<sample_year>\d{2})(?P<bact_number>-\d)',
                           "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           "number_to_letter": {"30": "B",
                                                "10": "D",
                                                "11": "F",
                                                "50": "T"}}}
        self.runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "runsheet_rearrange_year.xlsx"
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "proevenr_runsheet")] = "1112345699-1"
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_simplify_gtdb(self):
        fake_gtdb = pathlib.Path(__file__).parent / "data" / "merge_results" \
                    / "fake_gtdb_placeholder.tsv"
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "WGS_species_GTDB_basics")] = "Fakeococcus_A fakei"
        true_result.loc[0, (self.experiment_name, "WGS_species_GTDB")] = "Fakeococcus_A fakei"
        true_result.loc[0, (self.experiment_name, "WGS_genus_GTDB")] = "Fakeococcus_A"
        true_result.loc[0, (self.experiment_name, "final_id")] = "Fakeococcus fakei"

        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_simplify_long_placeholder(self):
        """Ensure that longer placeholders (>1 character) are handled properly."""
        fake_gtdb = pathlib.Path(__file__).parent / "data" / "merge_results" \
                    / "fake_gtdb_placeholder_long.tsv"
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[
            0, (self.experiment_name, "WGS_species_GTDB_basics")] = "Fakeococcus_AB fakei"
        true_result.loc[0, (self.experiment_name, "WGS_species_GTDB")] = "Fakeococcus_AB fakei"
        true_result.loc[0, (self.experiment_name, "WGS_genus_GTDB")] = "Fakeococcus_AB"
        true_result.loc[0, (self.experiment_name, "final_id")] = "Fakeococcus fakei"
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_simplify_gtdb_long(self):
        fake_gtdb = pathlib.Path(__file__).parent / "data" / "merge_results" \
                    / "fake_gtdb_long_placeholder.tsv"
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "WGS_species_GTDB_basics")] = "Fakeococcus_AB fakei"
        true_result.loc[0, (self.experiment_name, "WGS_species_GTDB")] = "Fakeococcus_AB fakei"
        true_result.loc[0, (self.experiment_name, "WGS_genus_GTDB")] = "Fakeococcus_AB"
        true_result.loc[0, (self.experiment_name, "final_id")] = "Fakeococcus fakei"

        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_blank_all_quast(self):
        read_results = self.fake_generic_coverage
        quast_results = pathlib.Path(__file__).parent / "data" / "merge_results" / "blank_quast.tsv"
        test_result = merge_all_results.merge_all_results([quast_results], read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "N50_basics")] = pd.NA
        true_result.loc[0, (self.experiment_name, "Total length_basics")] = pd.NA
        true_result.loc[0, (self.experiment_name, "# contigs")] = pd.NA
        true_result.loc[0, (self.experiment_name, "GC (%)")] = pd.NA
        true_result.loc[0, (self.experiment_name, "NG50")] = pd.NA
        true_result.loc[0, (self.experiment_name, "# predicted genes (unique)")] = pd.NA
        true_result.loc[0, (self.experiment_name, "godkendt")] = 0
        true_result.loc[0, (self.experiment_name, "pipeline_noter")] = "Ingen data for " \
                                                                       "Total length" \
                                                                       " og N50 - assembly'et er " \
                                                                       "sandsynligvis failet"
        print(true_result.loc[0, (self.experiment_name, "GC (%)")])
        print(test_result.loc[0, (self.experiment_name, "GC (%)")])
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_success_illumina_ng50(self):
        illumina_ng50 = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "illumina_ng50.tsv"
        read_results = self.fake_generic_coverage
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          ng50_quast_results = illumina_ng50,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "NG50")] = 20000
        print(true_result)
        true_result[self.experiment_name] = true_result[self.experiment_name].astype({"NG50":
                                                                                          "Int64"})
        print(self.standard_output)
        print(true_result)
        print(test_result)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_success_nanopore_reads(self):
        read_results = self.fake_generic_coverage
        nanopore_reads = self.fake_nanopore_coverage
        nanopore_runsheet = pathlib.Path(__file__).parent / "data" / \
                            "merge_results" / "test_nanopore_runsheet.xlsx"
        nanopore_experiment = "ONT_RUN0000_Y20990101_XYZ"
        test_config = {"platform": "GridION",
                       "sequencing_mode": "nanopore",
                       "language": "da",
                       "lab_info_system": {"use_lis_features": True},
                       "sample_number_settings": {"sample_number_format":
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "sample_numbers_in": "number",
                                                  "sample_numbers_out": "letter",
                                                  "sample_numbers_report": "number",
                                                  "number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"}}}
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, nanopore_runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = test_config,
                                                          nanopore_coverage = nanopore_reads)
        true_result = self.standard_output
        true_result = true_result.rename(columns={self.experiment_name: nanopore_experiment})
        true_result.loc[0, (nanopore_experiment, "ILM_Brønd")] = pd.NA
        true_result.loc[0, (nanopore_experiment, "SEK_Run_nr")] = pd.NA
        true_result.loc[0, (nanopore_experiment, "platform")] = "GridION"
        true_result.loc[0, (nanopore_experiment, "Weighted coverage depth")] = 200.0
        true_result.loc[0, (nanopore_experiment, "longest_contig_coverage")] = 200.0
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_success_nanopore_reads_translate(self):
        """Handle Nanopore results with non-default column names and translate."""
        read_results = self.fake_generic_coverage
        nanopore_reads = self.fake_nanopore_coverage
        nanopore_runsheet = pathlib.Path(__file__).parent / "data" / \
                            "merge_results" / "test_nanopore_runsheet_en.xlsx"
        nanopore_experiment = "ONT_RUN0000_Y20990101_XYZ"
        test_config = {"platform": "GridION",
                       "sequencing_mode": "nanopore",
                       "language": "en",
                       "lab_info_system": {"use_lis_features": True},
                       "sample_number_settings": {"sample_number_format":
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  "format_in_sheet": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "sample_numbers_in": "number",
                                                  "sample_numbers_out": "letter",
                                                  "sample_numbers_report": "number",
                                                  "number_to_letter": {"30": "B",
                                                                       "10": "D",
                                                                       "11": "F",
                                                                       "50": "T"}}}
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, nanopore_runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = test_config,
                                                          nanopore_coverage = nanopore_reads,
                                                          runsheet_names = SHEET_NAMES_EN,
                                                          bacteria_names = BACT_NAMES_EN,
                                                          lis_data_names = LIS_NAMES_EN)
        true_result = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                           "should sequencing be redone": [pd.NA],
                                           "notes": [pd.NA],
                                           "pipeline_notes": [""],
                                           "final_id": [pd.NA],
                                           "sample_number": ["1199123456-1"],
                                           "Requested_by": ["XYZ"],
                                           "Indication": ["Identification"],
                                           "Comment": ["Omkørsel"],
                                           "N50_basics": [20000],
                                           "Avg. coverage depth_basics": [200.0],
                                           "Grampos/neg": ["Negativ"],
                                           "LIS species": [pd.NA],
                                           "LIS: known length in bp": [pd.NA],
                                           "WGS_species_GTDB_basics":
                                               ["Fakeococcus fakei"],
                                           "Serotyping": [pd.NA],
                                           "ANI_to_ref_basics": [97.0],
                                           "GTDB: known length in bp": [pd.NA],
                                           "Total length_basics": [2000000],
                                           "Contamination": [1.0],
                                           "sample_number_quast": ["1199123456-1"],
                                           "# contigs": [100],
                                           "GC (%)": [50.0],
                                           "NG50": [pd.NA],
                                           "Weighted coverage depth": [200.0],
                                           "longest_contig_coverage": [200.0],
                                           "# predicted genes (unique)": [2000],
                                           "sample_number_kraken": ["1199123456-1"],
                                           "WGS_genus_kraken": ["Fakeococcus"],
                                           "WGS_genus_kraken_%": [90.0],
                                           "WGS_species_kraken": ["Fakeococcus fakei"],
                                           "WGS_species_kraken_%": [80.0],
                                           "sample_number_gtdb": ["1199123456-1"],
                                           "WGS_genus_GTDB": ["Fakeococcus"],
                                           "WGS_species_GTDB": ["Fakeococcus fakei"],
                                           "ANI_to_ref": [97.0],
                                           "sample_number_checkm": ["1199123456-1"],
                                           "Marker lineage": ["g_Fakeococcus"],
                                           "# genomes": [100],
                                           "# markers": [1000],
                                           "Completeness": [99.0],
                                           "sample_number_troubleshooting":
                                               ["1199123456-1"],
                                           "DNA_concentration": [4.0],
                                           "Extraction_run_number": ["0000"],
                                           "Extraction_well_number": ["A1"],
                                           "ILM_well_number": [pd.NA],
                                           "Sequencing_run_number": [pd.NA],
                                           "sample_number_runsheet": ["1199123456-1"],
                                           "pipeline_version":
                                               [merge_all_results.__version__],
                                           "platform": ["GridION"],
                                           "Lab_protocol": ["Version_1.2.3"]
                                       })
        run_header = [nanopore_experiment] * len(true_result.columns)
        true_result.columns = pd.MultiIndex.from_arrays([run_header, true_result.columns])
        print(test_result.to_string())
        print(true_result.to_string())
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_success_illumina_and_nanopore(self):  # TODO: use multiple QUAST reports for this
        nanopore_coverage = pathlib.Path(__file__).parent / "data" / "merge_results" \
                            / "fake_nanopore_sample2.tsv"
        read_results =  pathlib.Path(__file__).parent / "data" / "merge_results" \
                            / "fake_cov_two_samples.tsv"
        fake_quast_inputs = self.fake_quast_input.copy()
        fake_quast_second_sample = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                   / "fake_quast_sample2.tsv"
        fake_quast_inputs.append(fake_quast_second_sample)

        fake_checkm_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_checkm_two_samples.tsv"
        runsheet_multi_samples = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                 / "runsheet_singleindication_multisample.xlsx"
        fake_kraken_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_kraken_two_samples.tsv"
        fake_gtdb_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_gtdb_two_samples.tsv"
        test_result = merge_all_results.merge_all_results(fake_quast_inputs, read_results,
                                                          fake_checkm_both, runsheet_multi_samples,
                                                          fake_kraken_both, fake_gtdb_both,
                                                          nanopore_coverage = nanopore_coverage)
        second_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                             "noter": [pd.NA],
                                             "pipeline_noter": [""],
                                             "final_id": [pd.NA],
                                             "proevenr": ["1199123456-2"],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Kommentar": [pd.NA],
                                             "N50_basics": [20000],
                                             "Avg. coverage depth_basics": [200.0],
                                             "Grampos/neg": ["Negativ"],
                                             "MADS species": [pd.NA],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Fakeococcus fakei"],
                                             "Serotypning": [pd.NA],
                                             "ANI_til_ref_basics": [97.0],
                                             "GTDB: kendt længde i bp": [pd.NA],
                                             "Total length_basics": [2000000],
                                             "Contamination": [1],
                                             "proevenr_quast": ["1199123456-2"],
                                             "# contigs": [100],
                                             "GC (%)": [50],
                                             "NG50": [pd.NA],
                                             "Weighted coverage depth": [200.0],
                                             "longest_contig_coverage": [200.0],
                                             "# predicted genes (unique)": [2000],
                                             "proevenr_kraken": ["1199123456-2"],
                                             "WGS_genus_kraken": ["Fakeococcus"],
                                             "WGS_genus_kraken_%": [90.0],
                                             "WGS_species_kraken": ["Fakeococcus fakei"],
                                             "WGS_species_kraken_%": [80.0],
                                             "proevenr_gtdb": ["1199123456-2"],
                                             "WGS_genus_GTDB": ["Fakeococcus"],
                                             "WGS_species_GTDB": ["Fakeococcus fakei"],
                                             "ANI_til_ref": [97.0],
                                             "proevenr_checkm": ["1199123456-2"],
                                             "Marker lineage": ["g_Fakeococcus"],
                                             "# genomes": [100],
                                             "# markers": [1000],
                                             "Completeness": [99],
                                             "proevenr_troubleshooting": ["1199123456-2"],
                                             "DNA_Konc": [4.0],
                                             "Oprensnings_kørselsnr": ["0000"],
                                             "Brønd_nr": ["A1"],
                                             "ILM_Brønd": ["A2"],   # TODO: it shouldn't actually have a well...
                                             "SEK_Run_nr": ["0000"],
                                             "proevenr_runsheet": ["1199123456-2"],
                                             "pipeline_version": [merge_all_results.__version__],
                                             "platform": ["MiSeq"],
                                             "Lab_protocol": ["Version_1.2.3"]
                                             })
        experiment_name = "ILM_Run0000_Y20220101_XYZ"
        run_header = [experiment_name] * len(second_result.columns)
        second_result.columns = pd.MultiIndex.from_arrays([run_header,
                                                           second_result.columns])
        true_result = pd.concat([self.standard_output, second_result])
        true_result = true_result.reset_index(drop=True)
        pd.testing.assert_frame_equal(test_result, true_result, check_index_type = False)

    def test_success_no_second_gtdb(self):
        nanopore_coverage = pathlib.Path(__file__).parent / "data" / "merge_results" \
                            / "fake_nanopore_sample2.tsv"
        read_results = pathlib.Path(__file__).parent / "data" / "merge_results" \
                       / "fake_cov_two_samples.tsv"
        fake_quast_inputs = self.fake_quast_input.copy()
        fake_quast_second_sample = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                   / "fake_quast_sample2.tsv"
        fake_quast_inputs.append(fake_quast_second_sample)

        fake_checkm_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_checkm_two_samples.tsv"
        runsheet_multi_samples = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                 / "runsheet_singleindication_multisample.xlsx"
        fake_kraken_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_kraken_two_samples.tsv"

        test_result = merge_all_results.merge_all_results(fake_quast_inputs, read_results,
                                                          fake_checkm_both, runsheet_multi_samples,
                                                          fake_kraken_both, self.fake_gtdb,
                                                          nanopore_coverage = nanopore_coverage,
                                                          active_config = self.test_config)
        second_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                             "noter": [pd.NA],
                                             "pipeline_noter": [""],
                                             "final_id": [np.nan],
                                             "proevenr": ["1199123456-2"],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Kommentar": [pd.NA],
                                             "N50_basics": [20000],
                                             "Avg. coverage depth_basics": [200.0],
                                             "Grampos/neg": ["Negativ"],
                                             "MADS species": [pd.NA],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 [pd.NA],
                                             "Serotypning": [pd.NA],
                                             "ANI_til_ref_basics": [pd.NA],
                                             "GTDB: kendt længde i bp": [pd.NA],
                                             "Total length_basics": [2000000],
                                             "Contamination": [1],
                                             "proevenr_quast": ["1199123456-2"],
                                             "# contigs": [100],
                                             "GC (%)": [50],
                                             "NG50": [pd.NA],
                                             "Weighted coverage depth": [200.0],
                                             "longest_contig_coverage": [200.0],
                                             "# predicted genes (unique)": [2000],
                                             "proevenr_kraken": ["1199123456-2"],
                                             "WGS_genus_kraken": ["Fakeococcus"],
                                             "WGS_genus_kraken_%": [90.0],
                                             "WGS_species_kraken": ["Fakeococcus fakei"],
                                             "WGS_species_kraken_%": [80.0],
                                             "proevenr_gtdb": ["1199123456-2"],
                                             "WGS_genus_GTDB": [pd.NA],
                                             "WGS_species_GTDB": [pd.NA],
                                             "ANI_til_ref": [pd.NA],
                                             "proevenr_checkm": ["1199123456-2"],
                                             "Marker lineage": ["g_Fakeococcus"],
                                             "# genomes": [100],
                                             "# markers": [1000],
                                             "Completeness": [99],
                                             "proevenr_troubleshooting": ["1199123456-2"],
                                             "DNA_Konc": [4.0],
                                             "Oprensnings_kørselsnr": ["0000"],
                                             "Brønd_nr": ["A1"],
                                             "ILM_Brønd": ["A2"],
                                             "SEK_Run_nr": ["0000"],
                                             "proevenr_runsheet": ["1199123456-2"],
                                             "pipeline_version": [merge_all_results.__version__],
                                             "platform": ["MiSeq"],
                                             "Lab_protocol": ["Version_1.2.3"]
                                             })
        experiment_name = "ILM_Run0000_Y20220101_XYZ"
        run_header = [experiment_name] * len(second_result.columns)
        second_result.columns = pd.MultiIndex.from_arrays([run_header,
                                                           second_result.columns])
        true_result = pd.concat([self.standard_output, second_result])
        true_result = true_result.reset_index(drop=True)
        pd.testing.assert_frame_equal(test_result, true_result, check_index_type = False,
                                      check_dtype=False)

    def test_success_simplify_second_gtdb(self):
        nanopore_coverage = pathlib.Path(__file__).parent / "data" / "merge_results" \
                            / "fake_nanopore_sample2.tsv"
        read_results = pathlib.Path(__file__).parent / "data" / "merge_results" \
                       / "fake_cov_two_samples.tsv"
        fake_quast_inputs = self.fake_quast_input.copy()
        fake_quast_second_sample = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                   / "fake_quast_sample2.tsv"
        fake_quast_inputs.append(fake_quast_second_sample)

        fake_checkm_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_checkm_two_samples.tsv"
        runsheet_multi_samples = pathlib.Path(__file__).parent / "data" / "merge_results" \
                                 / "runsheet_singleindication_multisample.xlsx"
        fake_kraken_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                           / "fake_kraken_two_samples.tsv"
        fake_gtdb_both = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_gtdb_two_samples_placeholder.tsv"
        test_result = merge_all_results.merge_all_results(fake_quast_inputs, read_results,
                                                          fake_checkm_both,
                                                          runsheet_multi_samples,
                                                          fake_kraken_both, fake_gtdb_both,
                                                          nanopore_coverage = nanopore_coverage,
                                                          active_config = self.test_config)
        second_result = pd.DataFrame(data = {"godkendt": [pd.NA], "køres om": [pd.NA],
                                             "noter": [pd.NA],
                                             "pipeline_noter": [""],
                                             "final_id": ["Fakeococcus fakefacei"],
                                             "proevenr": ["1199123456-2"],
                                             "Bestilt af": ["XYZ"],
                                             "Indikation": ["Identifikation"],
                                             "Kommentar": [pd.NA],
                                             "N50_basics": [20000],
                                             "Avg. coverage depth_basics": [200.0],
                                             "Grampos/neg": ["Negativ"],
                                             "MADS species": [pd.NA],
                                             "MADS: kendt længde i bp": [pd.NA],
                                             "WGS_species_GTDB_basics":
                                                 ["Fakeococcus_A fakefacei"],
                                             "Serotypning": [pd.NA],
                                             "ANI_til_ref_basics": [97.0],
                                             "GTDB: kendt længde i bp": [pd.NA],
                                             "Total length_basics": [2000000],
                                             "Contamination": [1],
                                             "proevenr_quast": ["1199123456-2"],
                                             "# contigs": [100],
                                             "GC (%)": [50],
                                             "NG50": [pd.NA],
                                             "Weighted coverage depth": [200.0],
                                             "longest_contig_coverage": [200.0],
                                             "# predicted genes (unique)": [2000],
                                             "proevenr_kraken": ["1199123456-2"],
                                             "WGS_genus_kraken": ["Fakeococcus"],
                                             "WGS_genus_kraken_%": [90.0],
                                             "WGS_species_kraken": ["Fakeococcus fakei"],
                                             "WGS_species_kraken_%": [80.0],
                                             "proevenr_gtdb": ["1199123456-2"],
                                             "WGS_genus_GTDB": ["Fakeococcus_A"],
                                             "WGS_species_GTDB": ["Fakeococcus_A fakefacei"],
                                             "ANI_til_ref": [97.0],
                                             "proevenr_checkm": ["1199123456-2"],
                                             "Marker lineage": ["g_Fakeococcus"],
                                             "# genomes": [100],
                                             "# markers": [1000],
                                             "Completeness": [99],
                                             "proevenr_troubleshooting": ["1199123456-2"],
                                             "DNA_Konc": [4.0],
                                             "Oprensnings_kørselsnr": ["0000"],
                                             "Brønd_nr": ["A1"],
                                             "ILM_Brønd": ["A2"],
                                             "SEK_Run_nr": ["0000"],
                                             "proevenr_runsheet": ["1199123456-2"],
                                             "pipeline_version": [
                                                 merge_all_results.__version__],
                                             "platform": ["MiSeq"],
                                             "Lab_protocol": ["Version_1.2.3"]
                                             })
        experiment_name = "ILM_Run0000_Y20220101_XYZ"
        run_header = [experiment_name] * len(second_result.columns)
        second_result.columns = pd.MultiIndex.from_arrays([run_header,
                                                           second_result.columns])
        true_result = pd.concat([self.standard_output, second_result])
        true_result = true_result.reset_index(drop = True)
        print(test_result)
        print(true_result)

        pd.testing.assert_frame_equal(test_result, true_result, check_index_type = False,
                                      check_dtype = False)

    def test_use_lis_report(self):
        read_results =  self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" / "fake_mads.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "bacteria_codes.csv"

        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          raw_lis_report = mads_report,
                                                          species_codes = bacteria_codes,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "MADS species")] = "Fakeococcus sp."
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_use_lis_report_en(self):
        """Use LIS data with non-default column names"""
        read_results = self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" / "fake_mads_en.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "bacteria_codes_en.csv"
        true_en = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                       "should sequencing be redone": [pd.NA],
                                       "notes": [pd.NA],
                                       "pipeline_notes": [""],
                                       "final_id": [pd.NA],
                                       "sample_number": ["1199123456-1"],
                                       "Requested_by": ["XYZ"],
                                       "Indication": ["Identification"],
                                       "Comment": ["Omkørsel"],
                                       "N50_basics": [20000],
                                       "Avg. coverage depth_basics": [200.0],
                                       "Grampos/neg": ["Negativ"],
                                       "LIS species": ["Fakeococcus sp."],
                                       "LIS: known length in bp": [pd.NA],
                                       "WGS_species_GTDB_basics":
                                           ["Fakeococcus fakei"],
                                       "Serotyping": [pd.NA],
                                       "ANI_to_ref_basics": [97.0],
                                       "GTDB: known length in bp": [pd.NA],
                                       "Total length_basics": [2000000],
                                       "Contamination": [1.0],
                                       "sample_number_quast": ["1199123456-1"],
                                       "# contigs": [100],
                                       "GC (%)": [50.0],
                                       "NG50": [pd.NA],
                                       "Weighted coverage depth": [pd.NA],
                                       "longest_contig_coverage": [pd.NA],
                                       "# predicted genes (unique)": [2000],
                                       "sample_number_kraken": ["1199123456-1"],
                                       "WGS_genus_kraken": ["Fakeococcus"],
                                       "WGS_genus_kraken_%": [90.0],
                                       "WGS_species_kraken": ["Fakeococcus fakei"],
                                       "WGS_species_kraken_%": [80.0],
                                       "sample_number_gtdb": ["1199123456-1"],
                                       "WGS_genus_GTDB": ["Fakeococcus"],
                                       "WGS_species_GTDB": ["Fakeococcus fakei"],
                                       "ANI_to_ref": [97.0],
                                       "sample_number_checkm": ["1199123456-1"],
                                       "Marker lineage": ["g_Fakeococcus"],
                                       "# genomes": [100],
                                       "# markers": [1000],
                                       "Completeness": [99.0],
                                       "sample_number_troubleshooting":
                                           ["1199123456-1"],
                                       "DNA_concentration": [4.0],
                                       "Extraction_run_number": ["0000"],
                                       "Extraction_well_number": ["A1"],
                                       "ILM_well_number": ["A1"],
                                       "Sequencing_run_number": ["0000"],
                                       "sample_number_runsheet": ["1199123456-1"],
                                       "pipeline_version":
                                           [merge_all_results.__version__],
                                       "platform": ["MiSeq"],
                                       "Lab_protocol": ["Version_1.2.3"]
                                       })
        config_en = {"platform": "MiSeq",
                     "language": "en",
                     "sequencing_mode": "illumina",
                     "lab_info_system": {"use_lis_features": True},
                     "sample_number_settings": {
                         "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "format_output": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "sample_numbers_in": "number",
                         "sample_numbers_out": "letter",
                         "sample_numbers_report": "number",
                         "number_to_letter": {"30": "B",
                                              "10": "D",
                                              "11": "F",
                                              "50": "T"}}}
        run_header = [self.experiment_name] * len(true_en.columns)
        true_en.columns = pd.MultiIndex.from_arrays([run_header, true_en.columns])
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                   / "runsheet_singleindication_en.xlsx"
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          raw_lis_report = mads_report,
                                                          species_codes = bacteria_codes,
                                                          active_config = config_en,
                                                          runsheet_names = SHEET_NAMES_EN,
                                                          lis_data_names = LIS_NAMES_EN,
                                                          bacteria_names = BACT_NAMES_EN)
        pd.testing.assert_frame_equal(test_result, true_en, check_dtype = False,
                                      check_column_type = False)

    def test_add_serotype(self):
        read_results = self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" / "fake_mads.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "bacteria_codes.csv"
        mads_result = pathlib.Path(__file__).parent / "data" / "merge_results"\
                    / "fake_mads_results.tsv"
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          data_for_lis = mads_result,
                                                          raw_lis_report = mads_report,
                                                          species_codes = bacteria_codes,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "MADS species")] = "Fakeococcus sp."
        true_result.loc[0, (self.experiment_name, "Serotypning")] = "FakeSerotype"
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_add_serotype_en(self):
        read_results = self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" / "fake_mads_en.txt"
        bacteria_codes = pathlib.Path(__file__).parent / "data" / "bacteria_codes_en.csv"
        mads_result = pathlib.Path(__file__).parent / "data" / "merge_results"\
                    / "fake_mads_results_en.tsv"

        true_en = pd.DataFrame(data = {"sequence approved": [pd.NA],
                                       "should sequencing be redone": [pd.NA],
                                       "notes": [pd.NA],
                                       "pipeline_notes": [""],
                                       "final_id": [pd.NA],
                                       "sample_number": ["1199123456-1"],
                                       "Requested_by": ["XYZ"],
                                       "Indication": ["Identification"],
                                       "Comment": ["Omkørsel"],
                                       "N50_basics": [20000],
                                       "Avg. coverage depth_basics": [200.0],
                                       "Grampos/neg": ["Negativ"],
                                       "LIS species": ["Fakeococcus sp."],
                                       "LIS: known length in bp": [pd.NA],
                                       "WGS_species_GTDB_basics":
                                           ["Fakeococcus fakei"],
                                       "Serotyping": ["FakeSerotype"],
                                       "ANI_to_ref_basics": [97.0],
                                       "GTDB: known length in bp": [pd.NA],
                                       "Total length_basics": [2000000],
                                       "Contamination": [1.0],
                                       "sample_number_quast": ["1199123456-1"],
                                       "# contigs": [100],
                                       "GC (%)": [50.0],
                                       "NG50": [pd.NA],
                                       "Weighted coverage depth": [pd.NA],
                                       "longest_contig_coverage": [pd.NA],
                                       "# predicted genes (unique)": [2000],
                                       "sample_number_kraken": ["1199123456-1"],
                                       "WGS_genus_kraken": ["Fakeococcus"],
                                       "WGS_genus_kraken_%": [90.0],
                                       "WGS_species_kraken": ["Fakeococcus fakei"],
                                       "WGS_species_kraken_%": [80.0],
                                       "sample_number_gtdb": ["1199123456-1"],
                                       "WGS_genus_GTDB": ["Fakeococcus"],
                                       "WGS_species_GTDB": ["Fakeococcus fakei"],
                                       "ANI_to_ref": [97.0],
                                       "sample_number_checkm": ["1199123456-1"],
                                       "Marker lineage": ["g_Fakeococcus"],
                                       "# genomes": [100],
                                       "# markers": [1000],
                                       "Completeness": [99.0],
                                       "sample_number_troubleshooting":
                                           ["1199123456-1"],
                                       "DNA_concentration": [4.0],
                                       "Extraction_run_number": ["0000"],
                                       "Extraction_well_number": ["A1"],
                                       "ILM_well_number": ["A1"],
                                       "Sequencing_run_number": ["0000"],
                                       "sample_number_runsheet": ["1199123456-1"],
                                       "pipeline_version":
                                           [merge_all_results.__version__],
                                       "platform": ["MiSeq"],
                                       "Lab_protocol": ["Version_1.2.3"]
                                       })
        config_en = {"platform": "MiSeq",
                     "language": "en",
                     "sequencing_mode": "illumina",
                     "lab_info_system": {"use_lis_features": True},
                     "sample_number_settings": {
                         "format_in_sheet": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "format_output": r'(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                         "sample_numbers_in": "number",
                         "sample_numbers_out": "letter",
                         "sample_numbers_report": "number",
                         "number_to_letter": {"30": "B",
                                              "10": "D",
                                              "11": "F",
                                              "50": "T"}}}
        run_header = [self.experiment_name] * len(true_en.columns)
        true_en.columns = pd.MultiIndex.from_arrays([run_header, true_en.columns])
        runsheet = pathlib.Path(__file__).parent / "data" / "merge_results" \
                   / "runsheet_singleindication_en.xlsx"
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          data_for_lis = mads_result,
                                                          raw_lis_report = mads_report,
                                                          species_codes = bacteria_codes,
                                                          active_config = config_en,
                                                          runsheet_names = SHEET_NAMES_EN,
                                                          lis_data_names = LIS_NAMES_EN,
                                                          bacteria_names = BACT_NAMES_EN)
        pd.testing.assert_frame_equal(test_result, true_en, check_dtype = False,
                                      check_column_type = False)

    def test_use_lis_known_species(self):
        read_results = self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_mads_real_species.txt"
        bacteria_codes =  pathlib.Path(__file__).parent / "data" / "bacteria_codes.csv"
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          raw_lis_report = mads_report,
                                                          species_codes = bacteria_codes,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "MADS species")] = "Nocardia cyriacigeorgica"
        true_result.loc[0, (self.experiment_name, "MADS: kendt længde i bp")] = 6194645.0
        # since this doesn't match the length we found we'll get a QC fail
        true_result.loc[0, (self.experiment_name, "godkendt")] = 0
        true_result.loc[0, (self.experiment_name,
                            "pipeline_noter")] = "Over 20% forskel mellem " \
                                                 "forventet genomstørrelse fra MADS " \
                                                 "og fundet genomstørrelse. "
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_catch_qc_fail(self):
        read_results = (pathlib.Path(__file__).parent / "data"/ "merge_results"
                        / "low_illumina_cov.tsv")
        test_result = merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                          self.fake_checkm_input, self.runsheet,
                                                          self.fake_kraken, self.fake_gtdb,
                                                          active_config = self.test_config)
        true_result = self.standard_output.copy()
        true_result.loc[0, (self.experiment_name, "godkendt")] = 0
        true_result.loc[0, (self.experiment_name, "Avg. coverage depth_basics")] = 20.0
        true_result.loc[0, (self.experiment_name,
                            "pipeline_noter")] = "Coverage er lavere " \
                                                 "end grænsen for coverage (30). "
        pd.testing.assert_frame_equal(test_result, true_result, check_dtype = False)

    def test_fail_no_translations(self):
        read_results = self.fake_generic_coverage
        mads_report = pathlib.Path(__file__).parent / "data" / "merge_results" / "fake_mads.txt"
        error_msg = "When using a LIS report, a translation of the local bacteria codes " \
                    "to species names is required."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            merge_all_results.merge_all_results(self.fake_quast_input, read_results,
                                                self.fake_checkm_input, self.runsheet,
                                                self.fake_kraken, self.fake_gtdb,
                                                raw_lis_report = mads_report,
                                                active_config = self.test_config)


class TestMarkFailedQC(unittest.TestCase):
    config_en = {"sequencing_mode": "illumina", "language": "en"}

    def test_fail_unsupported_language(self):
        """Fail if notes should be given in an unsupported language."""
        fail_config = {"sequencing_mode": "illumina", "language": "tlh"}
        error_msg = "tlh is not a supported language."
        data_to_test = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                            "N50": [100000, 15000],
                                            "Avg. coverage depth": [100, 35],
                                            "MADS: kendt længde i bp": [2000000, 1500000],
                                            "Total length": [2200000, 1500000]})
        with pytest.raises(NotImplementedError, match = re.escape(error_msg)):
            merge_all_results.mark_qc_fails(data_to_test, fail_config)

    def test_no_failures(self):
        data_to_test = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199123456-2"],
                                          "N50": [100000, 15000],
                                          "Avg. coverage depth": [100, 35],
                                          "MADS: kendt længde i bp": [2000000, 1500000],
                                          "Total length": [2200000, 1500000]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 15000],
                                           "Avg. coverage depth": [100, 35],
                                           "MADS: kendt længde i bp": [2000000, 1500000],
                                           "Total length": [2200000, 1500000],
                                           "godkendt": [pd.NA, pd.NA],
                                           "pipeline_noter": ["", ""]})

        test_result = merge_all_results.mark_qc_fails(data_to_test)

        pd.testing.assert_frame_equal(true_result, test_result)

    def test_mark_failed_coverage(self):
        """Mark too low coverage (and check translation while we're at it)."""
        cov_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 15000],
                                           "Avg. coverage depth": [100, 25],
                                           "MADS: kendt længde i bp": [2000000, 1500000],
                                           "Total length": [2200000, 1500000],
                                           })
        true_cov_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                "N50": [100000, 15000],
                                                "Avg. coverage depth": [100, 25],
                                                "MADS: kendt længde i bp": [2000000, 1500000],
                                                "Total length": [2200000, 1500000],
                                                "godkendt": [pd.NA, 0],
                                                "pipeline_noter": ["",
                                                                   "Coverage er lavere end "
                                                                   "grænsen for coverage (30). "]})

        test_cov_too_low = merge_all_results.mark_qc_fails(cov_too_low)

        pd.testing.assert_frame_equal(true_cov_too_low, test_cov_too_low)

        true_cov_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                "N50": [100000, 15000],
                                                "Avg. coverage depth": [100, 25],
                                                "MADS: kendt længde i bp": [2000000, 1500000],
                                                "Total length": [2200000, 1500000],
                                                "godkendt": [pd.NA, 0],
                                                "pipeline_noter": ["",
                                                                   "Coverage is lower than the"
                                                                   " cutoff for coverage (30). "]})
        test_cov_en = merge_all_results.mark_qc_fails(cov_too_low, active_config = self.config_en)
        pd.testing.assert_frame_equal(true_cov_en, test_cov_en)
    def test_mark_failed_n50(self):
        """Mark too low N50 (and translation)"""
        n50_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 5000],
                                           "Avg. coverage depth": [100, 35],
                                           "MADS: kendt længde i bp": [2000000, 1500000],
                                           "Total length": [2200000, 1500000]})
        true_n50_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                "N50": [100000, 5000],
                                                "Avg. coverage depth": [100, 35],
                                                "MADS: kendt længde i bp": [2000000, 1500000],
                                                "Total length": [2200000, 1500000],
                                                "godkendt": [pd.NA, 0],
                                                "pipeline_noter": ["",
                                                                   "N50 er lavere end "
                                                                   "grænsen for N50 (10000). "]})

        test_n50_too_low = merge_all_results.mark_qc_fails(n50_too_low)

        pd.testing.assert_frame_equal(true_n50_too_low, test_n50_too_low)

        true_n50_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                "N50": [100000, 5000],
                                                "Avg. coverage depth": [100, 35],
                                                "MADS: kendt længde i bp": [2000000, 1500000],
                                                "Total length": [2200000, 1500000],
                                                "godkendt": [pd.NA, 0],
                                                "pipeline_noter": ["",
                                                                   "N50 is lower than the cutoff"
                                                                   " for N50 (10000). "]})
        test_n50_en = merge_all_results.mark_qc_fails(n50_too_low, active_config = self.config_en)
        pd.testing.assert_frame_equal(true_n50_en, test_n50_en)

    def test_catch_bad_size(self):
        """Catch size difference from LIS (and translation)"""
        size_too_high = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                             "N50": [100000, 15000],
                                             "Avg. coverage depth": [100, 35],
                                             "MADS: kendt længde i bp": [2000000, 1500000],
                                             "Total length": [3200000, 1500000]})
        true_size_too_high = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                  "N50": [100000, 15000],
                                                  "Avg. coverage depth": [100, 35],
                                                  "MADS: kendt længde i bp": [2000000, 1500000],
                                                  "Total length": [3200000, 1500000],
                                                  "godkendt": [0, pd.NA],
                                                  "pipeline_noter": ["Over 20% forskel mellem "
                                                                     "forventet genomstørrelse fra MADS "
                                                                     "og fundet genomstørrelse. ",
                                                                     ""]})

        test_size_too_high = merge_all_results.mark_qc_fails(size_too_high)

        pd.testing.assert_frame_equal(true_size_too_high, test_size_too_high)
        true_size_too_high_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                  "N50": [100000, 15000],
                                                  "Avg. coverage depth": [100, 35],
                                                  "MADS: kendt længde i bp": [2000000, 1500000],
                                                  "Total length": [3200000, 1500000],
                                                  "godkendt": [0, pd.NA],
                                                  "pipeline_noter": ["Over 20% difference between"
                                                                     " expected genome size from LIS"
                                                                     " and genome size found. ",
                                                                     ""]})

        test_size_too_high_en = merge_all_results.mark_qc_fails(size_too_high,
                                                                active_config = self.config_en)
        pd.testing.assert_frame_equal(true_size_too_high_en, test_size_too_high_en)

        size_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                            "N50": [100000, 15000],
                                            "Avg. coverage depth": [100, 35],
                                            "MADS: kendt længde i bp": [2000000, 1500000],
                                            "Total length": [2200000, 150000]})
        true_size_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                 "N50": [100000, 15000],
                                                 "Avg. coverage depth": [100, 35],
                                                 "MADS: kendt længde i bp": [2000000, 1500000],
                                                 "Total length": [2200000, 150000],
                                                 "godkendt": [pd.NA, 0],
                                                 "pipeline_noter": ["", "Over 20% forskel mellem "
                                                                        "forventet genomstørrelse fra MADS "
                                                                        "og fundet genomstørrelse. "
                                                                    ]
                                                 })

        test_size_too_low = merge_all_results.mark_qc_fails(size_too_low)

        pd.testing.assert_frame_equal(true_size_too_low, test_size_too_low)

    def test_multi_sample_fail(self):
        """Correctly report issues for multiple samples."""
        multi_fail = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                          "N50": [1000, 15000],
                                          "Avg. coverage depth": [100, 25],
                                          "MADS: kendt længde i bp": [2000000, 1500000],
                                          "Total length": [2200000, 1500000]})
        true_multi_fail = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                               "N50": [1000, 15000],
                                               "Avg. coverage depth": [100, 25],
                                               "MADS: kendt længde i bp": [2000000, 1500000],
                                               "Total length": [2200000, 1500000],
                                               "godkendt": [0, 0],
                                               "pipeline_noter": ["N50 er lavere end "
                                                                  "grænsen for N50 (10000). ",
                                                                  "Coverage er lavere end "
                                                                  "grænsen for coverage (30). "
                                                                  ]})
        true_multi_fail = true_multi_fail.astype({"godkendt": "object"})
        test_multi_fail = merge_all_results.mark_qc_fails(multi_fail)

        pd.testing.assert_frame_equal(true_multi_fail, test_multi_fail)

    def test_concat_fails(self):
        """Concatenate error messages."""
        all_wrong = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                         "N50": [100000, 1000],
                                         "Avg. coverage depth": [100, 25],
                                         "MADS: kendt længde i bp": [2000000, 1500000],
                                         "Total length": [2200000, 150000],
                                         })
        true_all_wrong = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                              "N50": [100000, 1000],
                                              "Avg. coverage depth": [100, 25],
                                              "MADS: kendt længde i bp": [2000000, 1500000],
                                              "Total length": [2200000, 150000],
                                              "godkendt": [pd.NA, 0],
                                              "pipeline_noter": ["",
                                                                 "Over 20% forskel mellem "
                                                                 "forventet genomstørrelse fra MADS "
                                                                 "og fundet genomstørrelse. "
                                                                 "Coverage er lavere end "
                                                                 "grænsen for coverage (30). "
                                                                 "N50 er lavere end "
                                                                 "grænsen for N50 (10000). "]})
        test_all_wrong = merge_all_results.mark_qc_fails(all_wrong)
        pd.testing.assert_frame_equal(true_all_wrong, test_all_wrong)

        true_all_wrong_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                              "N50": [100000, 1000],
                                              "Avg. coverage depth": [100, 25],
                                              "MADS: kendt længde i bp": [2000000, 1500000],
                                              "Total length": [2200000, 150000],
                                              "godkendt": [pd.NA, 0],
                                              "pipeline_noter": ["",
                                                                 "Over 20% difference between"
                                                                 " expected genome size from LIS"
                                                                 " and genome size found. "
                                                                 "Coverage is lower than the"
                                                                 " cutoff for coverage (30). "
                                                                 "N50 is lower than the cutoff"
                                                                 " for N50 (10000). "]})
        test_all_wrong_en = merge_all_results.mark_qc_fails(all_wrong,
                                                            active_config = self.config_en)
        pd.testing.assert_frame_equal(true_all_wrong_en, test_all_wrong_en)

    def test_mark_nanopore_failed(self):
        """Mark Nanopore results as failed using the Nanopore cutoff for coverage."""
        test_config = {"sequencing_mode": "nanopore", "language": "da"}
        cov_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 15000],
                                           "Avg. coverage depth": [100, 50],
                                           "MADS: kendt længde i bp": [2000000, 1500000],
                                           "Total length": [2200000, 1500000],
                                           })
        true_cov_too_low = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                "N50": [100000, 15000],
                                                "Avg. coverage depth": [100, 50],
                                                "MADS: kendt længde i bp": [2000000, 1500000],
                                                "Total length": [2200000, 1500000],
                                                "godkendt": [pd.NA, 0],
                                                "pipeline_noter": ["",
                                                                   "Coverage er lavere end "
                                                                   "grænsen for coverage (60). "]})

        test_cov_too_low = merge_all_results.mark_qc_fails(cov_too_low, active_config = test_config)
        pd.testing.assert_frame_equal(true_cov_too_low, test_cov_too_low)

        test_config_en = {"sequencing_mode": "nanopore", "language": "en"}
        true_cov_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 15000],
                                           "Avg. coverage depth": [100, 50],
                                           "MADS: kendt længde i bp": [2000000, 1500000],
                                           "Total length": [2200000, 1500000],
                                           "godkendt": [pd.NA, 0],
                                           "pipeline_noter": ["",
                                                              "Coverage is lower than the"
                                                              " cutoff for coverage (60). "]})
        test_cov_en = merge_all_results.mark_qc_fails(cov_too_low, active_config = test_config_en)
        pd.testing.assert_frame_equal(true_cov_en, test_cov_en)

    def test_handle_na(self):
        na_in_lis = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                         "N50": [100000, 15000],
                                         "Avg. coverage depth": [100, 35],
                                         "MADS: kendt længde i bp": [2000000, pd.NA],
                                         "Total length": [2200000, 1500000]})
        true_na_in_lis = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                              "N50": [100000, 15000],
                                              "Avg. coverage depth": [100, 35],
                                              "MADS: kendt længde i bp": [2000000, pd.NA],
                                              "Total length": [2200000, 1500000],
                                              "godkendt": [pd.NA, pd.NA],
                                              "pipeline_noter": ["", ""]})

        test_na_in_lis = merge_all_results.mark_qc_fails(na_in_lis)

        pd.testing.assert_frame_equal(true_na_in_lis, test_na_in_lis)

    def test_mark_na_length_failed(self):
        na_in_length = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                            "N50": [100000, 15000],
                                            "Avg. coverage depth": [100, 35],
                                            "MADS: kendt længde i bp": [2000000, 1500000],
                                            "Total length": [2200000, pd.NA]})
        true_na_in_length = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                 "N50": [100000, 15000],
                                                 "Avg. coverage depth": [100, 35],
                                                 "MADS: kendt længde i bp": [2000000, 1500000],
                                                 "Total length": [2200000, pd.NA],
                                                 "godkendt": [pd.NA, 0],
                                                 "pipeline_noter": ["",
                                                                    "Ingen data for "
                                                                    "Total length "
                                                                    "- assembly'et er sandsynligvis"
                                                                    " failet"]})

        test_na_in_length = merge_all_results.mark_qc_fails(na_in_length)

        pd.testing.assert_frame_equal(true_na_in_length, test_na_in_length)

        true_na_in_length_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                 "N50": [100000, 15000],
                                                 "Avg. coverage depth": [100, 35],
                                                 "MADS: kendt længde i bp": [2000000, 1500000],
                                                 "Total length": [2200000, pd.NA],
                                                 "godkendt": [pd.NA, 0],
                                                 "pipeline_noter": ["",
                                                                    "No data for "
                                                                    "Total length "
                                                                    "- assembly likely"
                                                                    " failed"]})
        test_na_in_length_en = merge_all_results.mark_qc_fails(na_in_length,
                                                               active_config = self.config_en)
        pd.testing.assert_frame_equal(true_na_in_length_en, test_na_in_length_en)

    def test_mark_multi_na_fails(self):
        """Mark a sample where multiple QC factors are NA as failed."""
        multi_na = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                            "N50": [100000, 15000],
                                            "Avg. coverage depth": [100, pd.NA],
                                            "MADS: kendt længde i bp": [2000000, 1500000],
                                            "Total length": [2200000, pd.NA]})
        true_multi_na = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                 "N50": [100000, 15000],
                                                 "Avg. coverage depth": [100, pd.NA],
                                                 "MADS: kendt længde i bp": [2000000, 1500000],
                                             "Total length": [2200000, pd.NA],
                                             "godkendt": [pd.NA, 0],
                                             "pipeline_noter": ["",
                                                                "Ingen data for "
                                                                "Total length "
                                                                "og Avg. coverage depth "
                                                                "- assembly'et er sandsynligvis"
                                                                " failet"]})

        test_multi_na = merge_all_results.mark_qc_fails(multi_na)

        pd.testing.assert_frame_equal(true_multi_na, test_multi_na)

        true_multi_na_en = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                 "N50": [100000, 15000],
                                                 "Avg. coverage depth": [100, pd.NA],
                                                 "MADS: kendt længde i bp": [2000000, 1500000],
                                             "Total length": [2200000, pd.NA],
                                             "godkendt": [pd.NA, 0],
                                             "pipeline_noter": ["",
                                                                "No data for "
                                                                "Total length and "
                                                                "Avg. coverage depth "
                                                                "- assembly likely"
                                                                " failed"]})
        test_multi_na_en = merge_all_results.mark_qc_fails(multi_na, active_config = self.config_en)
        pd.testing.assert_frame_equal(true_multi_na_en, test_multi_na_en)

    def test_different_nas(self):
        """Report NA values for different samples."""
        different_nas = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                             "N50": [pd.NA, 15000],
                                             "Avg. coverage depth": [100, 35],
                                             "MADS: kendt længde i bp": [2000000, 1500000],
                                             "Total length": [2200000, pd.NA]})
        true_different_nas = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                                  "N50": [pd.NA, 15000],
                                                  "Avg. coverage depth": [100, 35],
                                                  "MADS: kendt længde i bp": [2000000, 1500000],
                                                  "Total length": [2200000, pd.NA],
                                                  "godkendt": [0, 0],
                                                  "pipeline_noter": ["Ingen data for N50 - "
                                                                     "assembly'et er sandsynligvis"
                                                                     " failet",
                                                                     "Ingen data for "
                                                                     "Total length "
                                                                     "- assembly'et er"
                                                                     " sandsynligvis failet"]})
        true_different_nas = true_different_nas.astype({"godkendt": "object"})
        test_different_nas = merge_all_results.mark_qc_fails(different_nas)

        pd.testing.assert_frame_equal(true_different_nas, test_different_nas)

    def test_no_lis(self):
        data_to_test = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                            "N50": [100000, 15000],
                                            "Avg. coverage depth": [100, 35],
                                            "Total length": [2200000, 1500000]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "N50": [100000, 15000],
                                           "Avg. coverage depth": [100, 35],
                                           "Total length": [2200000, 1500000],
                                           "godkendt": [pd.NA, pd.NA],
                                           "pipeline_noter": ["", ""]})

        test_result = merge_all_results.mark_qc_fails(data_to_test)

        pd.testing.assert_frame_equal(true_result, test_result)


class TestGetExpectedSize(unittest.TestCase):
    def test_get_size_one_hit(self):
        # TODO: are there even ones with only one hit?
        pass

    def test_get_size_multi_hits(self):
        true_size = 4349809.0
        test_size = merge_all_results.get_size_by_species_safe("Fictibacillus barbaricus")
        assert test_size == true_size

        nocardia_true_size = 6194645.0
        nocardia_test_size = merge_all_results.get_size_by_species_safe("Nocardia cyriacigeorgica")
        assert nocardia_test_size == nocardia_true_size

    def test_no_hit(self):
        test_size = merge_all_results.get_size_by_species_safe("Testobacter fakei")
        assert pd.isnull(test_size)

    def test_no_species(self):
        test_size = merge_all_results.get_size_by_species_safe(pd.NA)
        assert pd.isnull(test_size)

