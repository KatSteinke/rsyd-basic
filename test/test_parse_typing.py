#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib
import unittest

import numpy as np
import pytest

import pandas as pd

import parse_analysis


class TestParseSeqSero(unittest.TestCase):
    def test_get_serotype(self):
        seqsero_report = pd.DataFrame(data = {"Sample name": ["1199123456-1"],
                                              "Predicted identification":
                                                  ["Salmonella enterica subspecies enterica "
                                                   "(subspecies I)"],
                                              "Potential inter-serotype contamination": ["no"],
                                              "Predicted serotype": ["I 4,[5],12:i:-"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS supplerende type":
                                               ["Salmonella enterica subspecies enterica "
                                                "(subspecies I)"],
                                           "O_WGS Serotype/gruppe": ["I 4,[5],12:i:-"]})
        test_result = parse_analysis.parse_seqsero(seqsero_report)
        pd.testing.assert_frame_equal(true_result, test_result)


class TestParseMLST(unittest.TestCase):
    def test_sequencetype_found(self):
        typing_report = pathlib.Path(__file__).parent / "data" / "parse_typing" / \
                        "typing_success.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS MLST": "1"})
        test_result = parse_analysis.parse_mlst(typing_report)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_no_st_found(self):
        typing_report = pathlib.Path(__file__).parent / "data" / "parse_typing" / \
                        "typing_failure.tsv"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS MLST": ["-"]})
        test_result = parse_analysis.parse_mlst(typing_report)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_blank_mlst(self):
        typing_report = pathlib.Path(__file__).parent / "data" / "generate_report" \
                        / "blank_mlst.tsv"
        true_result = pd.DataFrame(data = {"proevenr": [""],
                                           "O_WGS MLST": ["-"]})
        test_result = parse_analysis.parse_mlst(typing_report)
        pd.testing.assert_frame_equal(test_result, true_result)


class TestParseSeroTypeFinder(unittest.TestCase):
    def test_serotype_success(self):
        serotype_report = pd.DataFrame(data = {"Database": ["H_type", "O_type", "O_type"],
                                               "Serotype": ["H1", "O1", "O1"],
                                               "Contig": ["1199123456-1_00010 len=10000",
                                                          "1199123456-1_00011 len=9000",
                                                          "1199123456-1_00011 len=9000"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Serotype/gruppe": ["O1:H1"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_different_htypes(self):
        serotype_report = pd.DataFrame(data = {"Database": ["H_type", "H_type", "O_type", "O_type"],
                                               "Serotype": ["H1", "H3", "O1", "O1"],
                                               "Contig": ["1199123456-1_00010 len=10000",
                                                          "1199123456-1_00011 len=9000",
                                                          "1199123456-1_00011 len=9000",
                                                          "1199123456-1_00011 len=9000"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Serotype/gruppe": ["O1:H?"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_different_otypes(self):
        serotype_report = pd.DataFrame(data = {"Database": ["H_type", "O_type", "O_type"],
                                               "Serotype": ["H1", "O1", "O2"],
                                               "Contig": ["1199123456-1_00010 len=10000",
                                                          "1199123456-1_00011 len=9000",
                                                          "1199123456-1_00011 len=9000"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Serotype/gruppe": ["O?:H1"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_empty_df(self):
        serotype_report = pd.DataFrame(data = {"Database": [pd.NA],
                                               "Serotype": [pd.NA],
                                               "Contig": [pd.NA]})
        true_result = pd.DataFrame(data = {"proevenr": [],
                                           "O_WGS Serotype/gruppe": []})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        print(true_result)
        print(test_result)
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False,
                                      check_dtype = False)

    def test_missing_htype(self):
        serotype_report = pd.DataFrame(data = {"Database": ["O_type", "O_type"],
                                               "Serotype": ["O1", "O1"],
                                               "Contig": ["1199123456-1_00011 len=9000",
                                                          "1199123456-1_00011 len=9000"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Serotype/gruppe": ["O1:H?"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_missing_otype(self):
        serotype_report = pd.DataFrame(data = {"Database": ["H_type"],
                                               "Serotype": ["H1"],
                                               "Contig": ["1199123456-1_00010 len=10000"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Serotype/gruppe": ["O?:H1"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_result = parse_analysis.parse_serotype_finder(serotype_report, sample_format)
        pd.testing.assert_frame_equal(true_result, test_result)


class TestTrimList(unittest.TestCase):
    def test_too_long_delim(self):
        with pytest.raises(ValueError, match = "The desired delimiter is already longer than the "
                                               "maximum text length."):
            parse_analysis.trim_to_max_length(["foo", "bar", "baz"], max_length = 4,
                                              delimiter = "toolong")

    def test_too_long_chunks(self):
        with pytest.raises(ValueError, match = "The list contains items that are longer than the "
                                               "maximum text length."):
            parse_analysis.trim_to_max_length(["foobar", "baz"], max_length = 4)

    def test_size_fits(self):
        expected_result = [["foo", "bar"]]
        trimmed_result = parse_analysis.trim_to_max_length(["foo", "bar"], max_length = 8)
        assert expected_result == trimmed_result

    def test_too_long(self):
        expected_result = [["foo"], ["bar"]]
        trimmed_result = parse_analysis.trim_to_max_length(["foo", "bar"], max_length = 4)
        assert expected_result == trimmed_result

    def test_join(self):
        expected_result = [["foo", "bar"], ["baz"]]
        trimmed_result = parse_analysis.trim_to_max_length(["foo", "bar", "baz"], max_length = 8)
        assert expected_result == trimmed_result


class TestParseAbritAMR(unittest.TestCase):

    def test_default_resistance(self):
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(A)"],
                                               "ESBL (AmpC type)": ["AmpC"]
                                               })
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen anden beta-lactamase": ["AmpC"],
                                           "O_WGS res.gen carbapenemase": [pd.NA],
                                           "O_WGS res.gen colistin": [pd.NA],
                                           "O_WGS res.gen vancomycin": [pd.NA]
                                           })
        test_report = parse_analysis.parse_abritamr(abritamr_report)
        print(test_report)
        pd.testing.assert_frame_equal(test_report, true_report)

    def test_other_resistance(self):
        test_resistances = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                           / "reduced_classes.csv"
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(A)"],
                                               "ESBL (AmpC type)": ["AmpC"]
                                               })
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen_testocillin": ["xyz(A)"],
                                           "O_WGS res.gen_testomycin": [pd.NA],
                                           "O_WGS res.gen_testobactam": [pd.NA]
                                           })
        test_report = parse_analysis.parse_abritamr(abritamr_report, test_resistances)
        print(test_report)
        pd.testing.assert_frame_equal(test_report, true_report)

    def test_multiple_resistance_genes(self):
        test_resistances = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                           / "reduced_classes.csv"
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(A),xyz(B)"],
                                               "Testomycin": ["zxy(B)"]})
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen_testocillin": ["xyz(A);xyz(B)"],
                                           "O_WGS res.gen_testomycin": ["zxy(B)"],
                                           "O_WGS res.gen_testobactam": [pd.NA]
                                           })
        test_report = parse_analysis.parse_abritamr(abritamr_report, test_resistances)
        pd.testing.assert_frame_equal(test_report, true_report)

    def test_multiple_resistance_types(self):
        """Ensure multiple resistance types are reported in a consistent manner"""
        test_resistances = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                           / "reduced_classes.csv"
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(B)"],
                                               "Testocillin_res_2": ["xyz(A)"],
                                               "Testomycin": ["zxy(B)"]})
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen_testocillin": ["xyz(A);xyz(B)"],
                                           "O_WGS res.gen_testomycin": ["zxy(B)"],
                                           "O_WGS res.gen_testobactam": [pd.NA]
                                           })
        test_report = parse_analysis.parse_abritamr(abritamr_report, test_resistances)
        print(test_report)
        pd.testing.assert_frame_equal(test_report, true_report)

    def test_multiple_genes_multiple_resistance_types(self):
        """Ensure multiple resistance types containing multiple genes
         are reported in a consistent manner"""
        test_resistances = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                           / "reduced_classes.csv"
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(B)"],
                                               "Testocillin_res_2": ["xyz(A),xyz(C)"],
                                               "Testomycin": ["zxy(B)"]})
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen_testocillin": ["xyz(A);xyz(B);xyz(C)"],
                                           "O_WGS res.gen_testomycin": ["zxy(B)"],
                                           "O_WGS res.gen_testobactam": [pd.NA]
                                           })
        test_report = parse_analysis.parse_abritamr(abritamr_report, test_resistances)
        print(test_report)
        pd.testing.assert_frame_equal(test_report, true_report)

    def test_multiple_resistance_columns(self):
        test_resistances = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                           / "reduced_classes.csv"
        abritamr_report = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                               "Testocillin": ["xyz(A)"],
                                               "Testomycin": ["zxy(B)"],
                                               "Testobactam": ["yxz(C)"]})
        true_report = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS res.gen_testocillin": ["xyz(A)"],
                                           "O_WGS res.gen_testomycin": ["zxy(B)"],
                                           "O_WGS res.gen_testobactam": ["yxz(C)"]})
        test_report = parse_analysis.parse_abritamr(abritamr_report, test_resistances)
        print(test_report)
        pd.testing.assert_frame_equal(test_report, true_report)


class TestVirulenceAbritAMR(unittest.TestCase):
    def test_no_virulence(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Metal": ["xyz(A)"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_single_class(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["eae"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": ["eae"]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_filter_unrelated(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["eae,xyz(A)"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": ["eae"]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_blank_if_unrelated(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["xyz(A)"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": [pd.NA]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_multi_hit_single_class(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["cdtA,cdtB"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": ["binært toxin"]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_multi_class(self):
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["eae,ltcA"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS Toxinpåvisning_0": ["eae"],
                                           "O_WGS Toxinpåvisning_1": ["eltA"]})
        test_result = parse_analysis.parse_toxins(virulence_data)
        pd.testing.assert_frame_equal(test_result, true_result)

    def test_multi_class_en(self):
        """Report multiple virulence gene classes in a different language than the default."""
        virulence_data = pd.DataFrame(data = {"Isolate": ["test/1199123456-1"],
                                              "Virulence": ["eae,ltcA"]})
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "O_WGS toxin identification_0": ["eae"],
                                           "O_WGS toxin identification_1": ["eltA"]})
        test_result = parse_analysis.parse_toxins(virulence_data, target_language = "en")
        pd.testing.assert_frame_equal(test_result, true_result)


class TestParsePlasmidFinder(unittest.TestCase):
    def test_single_plasmid_success(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "fake_plasmids.tsv"
        true_data = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "O_WGS Plasmid-ID_0": ["IncX3"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data)

    def test_wrap_long_plasmid_list(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                         / "fake_plasmids_long.tsv"
        true_data = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "O_WGS Plasmid-ID_0": ["a_very_long_plasmid_name"],
                                         "O_WGS Plasmid-ID_1": ["an_even_longer_plasmid_name"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data)

    def test_force_wrap(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                         / "fake_plasmids_longer.tsv"
        true_data = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "O_WGS Plasmid-ID_0":
                                             ["a_veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeery_"],
                                         "O_WGS Plasmid-ID_1": ["long_plasmid_name"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data)

    def test_multi_plasmid(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "multi_plasmids.tsv"
        true_data = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "O_WGS Plasmid-ID_0": ["IncFIB(K);IncX3"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data)

    def test_emptyresult(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "empty_plasmids.tsv"
        true_data = pd.DataFrame(data = {"proevenr": [],
                                         "O_WGS Plasmid-ID_0": []})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data, check_index_type = False,
                                      check_dtype = False)

    def test_blank_result(self):
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "blank_plasmids.tsv"
        true_data = pd.DataFrame(data = {"proevenr": [],
                                         "O_WGS Plasmid-ID_0": []})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data, check_index_type = False,
                                      check_dtype = False)

    def test_force_sort(self):
        """Ensure plasmid names are sorted."""
        plasmid_report = pathlib.Path(__file__).parent / "data" / "merge_results" \
                         / "plasmids_to_sort.tsv"
        true_data = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "O_WGS Plasmid-ID_0": ["IncFIB(K);IncX3"]})
        sample_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        test_data = parse_analysis.parse_plasmidfinder(plasmid_report, sample_format)
        pd.testing.assert_frame_equal(test_data, true_data)


class TestParseGTDBTk(unittest.TestCase):
    def test_success_genus_species(self):
        input_data = pathlib.Path(__file__).parent / "data" / "parse_typing" / "test_gtdb.tsv"
        expected_output = pd.DataFrame(data={'proevenr': ['1199123456-1', '1199123456-2'],
                                             'WGS_genus_GTDB': ['Placeholderia',
                                                                'Placeholderia'],
                                             'WGS_species_GTDB': ['Placeholderia fakeorum',
                                                                  'Placeholderia'
                                                                  ' bielefeldensis'],
                                             "ANI_til_ref": [96.0, 98.0]
                                             })
        test_output = parse_analysis.parse_gtdbtk(input_data)
        pd.testing.assert_frame_equal(expected_output, test_output)

    def test_success_genus_only(self):
        input_data = pathlib.Path(__file__).parent / "data" / "parse_typing" / "gtdb_genus_only.tsv"
        expected_output = pd.DataFrame(data={'proevenr': ['1199123456-1', '1199123456-2'],
                                             'WGS_genus_GTDB': ['Placeholderia',
                                                                'Placeholderia'],
                                             'WGS_species_GTDB': ['Placeholderia fakeorum',
                                                                  ''],
                                             "ANI_til_ref": [96.0, np.nan]
                                             })
        test_output = parse_analysis.parse_gtdbtk(input_data)
        pd.testing.assert_frame_equal(expected_output, test_output)

    def test_handle_nas(self):
        """Handle NA values."""
        input_data = pathlib.Path(__file__).parent / "data" / "merge_results" \
                      / "fake_gtdb_na.tsv"
        expected_output = pd.DataFrame(data={'proevenr': ['1199123456-1'],
                                             'WGS_genus_GTDB': ['Unclassified'],
                                             'WGS_species_GTDB': ['Unclassified'],
                                             "ANI_til_ref": [np.nan]
                                             })
        test_output = parse_analysis.parse_gtdbtk(input_data)
        pd.testing.assert_frame_equal(expected_output, test_output)

    def test_handle_blank_report(self):
        input_data = pathlib.Path(__file__).parent / "data" / "parse_typing" \
                      / "blank_gtdb.tsv"
        expected_output = pd.DataFrame(data={'proevenr': [pd.NA],
                                             'WGS_genus_GTDB': [pd.NA],
                                             'WGS_species_GTDB': [pd.NA],
                                             "ANI_til_ref": [pd.NA]
                                             })
        test_output = parse_analysis.parse_gtdbtk(input_data)
        pd.testing.assert_frame_equal(expected_output, test_output)