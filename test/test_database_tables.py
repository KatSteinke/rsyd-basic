import unittest
from datetime import date, datetime

import numpy as np
import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

import database_tables
from database_tables import Sample, Isolate, SeqRequest, Eluate, ReadFile, SequencingRun, \
    Assembly, ExtractRun, AssemblyReadFiles


class TestComparisonMixin(unittest.TestCase):
    def test_complain_bad_args(self):
        error_msg = "nope is not a valid comparison type."
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia", institution = "KMA")
        isolate2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                           preliminary_organism_name = "Placeholderia", institution = "KMA")
        with pytest.raises(ValueError, match = error_msg):
            isolate.check_matches(isolate2, check_relationships = "nope")

    def test_success_nan_comparison(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia", final_organism_name = np.nan,
                          institution = "KMA")
        isolate2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                           final_organism_name = np.nan,
                           preliminary_organism_name = "Placeholderia", institution = "KMA")
        assert isolate.check_matches(isolate2)
        assert isolate2.check_matches(isolate)

    def test_fail_nan_comparison(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia", final_organism_name = np.nan,
                          institution = "KMA")
        isolate2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                           final_organism_name = "Placeholderia fakeorum",
                           preliminary_organism_name = "Placeholderia", institution = "KMA")
        assert not isolate.check_matches(isolate2)
        assert not isolate2.check_matches(isolate)


class TestSamples(unittest.TestCase):
    isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                      preliminary_organism_name = "Placeholderia", institution = "KMA")

    def test_print_sample(self):
        test_class = database_tables.Sample(mads_proevenr = "F99123456", institution = "KMA",
                                            lab_section = "F", isolates = [self.isolate])
        expected_repr = "Sample(mads_proevenr=F99123456, institution=KMA, lab_section=F, " \
                        "isolates=['Isolate(F99123456-1)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_sample_dict_no_isolates(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F")
        expected_result = {"mads_proevenr": "F99123456", "institution": "KMA", "lab_section": "F",
                           "isolates": []}
        assert sample.to_dict() == expected_result

    def test_sample_dict_isolates(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        expected_result = {"mads_proevenr": "F99123456", "institution": "KMA", "lab_section": "F",
                           "isolates": [self.isolate]}
        assert sample.to_dict() == expected_result

    def test_match_sample_no_isolates(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F")
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F")
        assert sample.check_matches(sample_2)
        assert sample_2.check_matches(sample)

    def test_match_sample_isolates_properties(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia", institution = "KMA")
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                          isolates = [isolate_2])
        assert sample.check_matches(sample_2, check_relationships = "properties")
        assert sample_2.check_matches(sample, check_relationships = "properties")

    def test_match_multi_isolates(self):
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia", institution = "KMA")
        isolate_3 = Isolate(mads_isolatnr = "F99123456-2", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia", institution = "KMA")
        isolate_4 = Isolate(mads_isolatnr = "F99123456-2", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia", institution = "KMA")
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate, isolate_3])
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                          isolates = [isolate_2, isolate_4])
        assert sample.check_matches(sample_2, check_relationships = "properties")
        assert sample_2.check_matches(sample, check_relationships = "properties")

    def test_no_match_isolate_different(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        sample2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F")
        assert not sample.check_matches(sample2, check_relationships = "properties")
        assert not sample2.check_matches(sample, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        session.add(sample)
        sample_in_db = session.query(Sample).first()
        assert sample.check_matches(sample_in_db, check_relationships = "strict")
        assert sample_in_db.check_matches(sample, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia", institution = "KMA")
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                          isolates = [isolate_2])
        assert not sample.check_matches(sample_2, check_relationships = "strict")
        assert not sample_2.check_matches(sample, check_relationships = "strict")

    def test_no_match_sample_diff_type(self):
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [self.isolate])
        assert not sample.check_matches(self.isolate)


class TestIsolate(unittest.TestCase):
    seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                             mads_proevenr = "F99123456", requested_by = "ABC",
                             resistens = False, identifikation = True,
                             relaps = False, toxingen_identifikation = False,
                             hai = False, miljoeproeve = False, udbrud = False,
                             overvaagning = False, forskning = False, andet = False,
                             request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                             institution = "KMA",
                             gram_positiv = True, gram_negativ = False,
                             request_comment = "", secondary_id = "",
                             registered_by = "XYZ")
    eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
    forward_read = ReadFile(readfile_id = 1, read_direction = "forward",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/forward_readfile",
                            batch_id = 1, eluat_id = 1, institution = "KMA")
    reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/reverse_readfile",
                            batch_id = 1, eluat_id = 1, institution = "KMA")

    def test_print_isolate(self):
        test_class = database_tables.Isolate(mads_isolatnr = "F99123456-1",
                                             mads_proevenr = "F99123456",
                                             preliminary_organism_name = "Placeholderia",
                                             final_organism_name = "Placeholderia fakeorum",
                                             wgs_serotyping = "",
                                             institution = "KMA")
        expected_repr = "Isolate(mads_isolatnr=F99123456-1, mads_proevenr=F99123456, " \
                        "preliminary_organism_name=Placeholderia, " \
                        "final_organism_name=Placeholderia fakeorum, " \
                        "wgs_serotyping=, institution=KMA, sample=Sample()," \
                        " requests=[], eluates=[], " \
                        "read_files=[])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_repr_isolate_with_connections(self):
        test_class = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                             preliminary_organism_name = "Placeholderia",
                             final_organism_name = "Placeholderia fakeorum",
                             wgs_serotyping = "",
                             institution = "KMA", requests = [self.seq_request],
                             eluates = [self.eluate],
                             read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [test_class])
        test_class.sample = sample
        expected_repr = "Isolate(mads_isolatnr=F99123456-1, mads_proevenr=F99123456, " \
                        "preliminary_organism_name=Placeholderia, " \
                        "final_organism_name=Placeholderia fakeorum, " \
                        "wgs_serotyping=, institution=KMA, sample=Sample(F99123456), " \
                        "requests=['SeqRequest(1)'], eluates=['Eluate(1)'], " \
                        "read_files=['ReadFile(path/to/forward_readfile)'," \
                        " 'ReadFile(path/to/reverse_readfile)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_isolate_nothing_attached_to_dict(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA")
        expected_result = {"mads_isolatnr": "F99123456-1", "mads_proevenr": "F99123456",
                           "preliminary_organism_name": "Placeholderia",
                           "final_organism_name": "Placeholderia fakeorum",
                           "wgs_serotyping": "",
                           "institution": "KMA",
                           "sample": None,
                           "requests": [],
                           "eluates": [],
                           "read_files": []}
        assert isolate.to_dict() == expected_result

    def test_isolate_with_connections_to_dict(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        expected_result = {"mads_isolatnr": "F99123456-1", "mads_proevenr": "F99123456",
                           "preliminary_organism_name": "Placeholderia",
                           "final_organism_name": "Placeholderia fakeorum",
                           "wgs_serotyping": "",
                           "institution": "KMA",
                           "sample": sample,
                           "requests": [self.seq_request],
                           "eluates": [self.eluate],
                           "read_files": [self.forward_read, self.reverse_read]}
        assert isolate.to_dict() == expected_result

    def test_match_isolate(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA")
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "",
                            institution = "KMA")
        assert isolate.check_matches(isolate_2)
        assert isolate_2.check_matches(isolate)

    def test_match_isolate_relationships(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        seq_request_2 = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                   mads_proevenr = "F99123456", requested_by = "ABC",
                                   resistens = False, identifikation = True,
                                   relaps = False, toxingen_identifikation = False,
                                   hai = False, miljoeproeve = False, udbrud = False,
                                   overvaagning = False, forskning = False, andet = False,
                                   request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                   institution = "KMA",
                                   gram_positiv = True, gram_negativ = False,
                                   request_comment = "", secondary_id = "",
                                   registered_by = "XYZ")
        eluate_2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/forward_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/reverse_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "",
                            institution = "KMA", requests = [seq_request_2],
                            eluates = [eluate_2],
                            read_files = [forward_read_2, reverse_read_2], sample = sample)
        assert isolate.check_matches(isolate_2, check_relationships = "properties")
        assert isolate_2.check_matches(isolate, check_relationships = "properties")

    def test_mismatch_isolate_relationships(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        # TODO: copy utility?
        eluate_2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/forward_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/reverse_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "",
                            institution = "KMA",
                            eluates = [eluate_2],
                            read_files = [forward_read_2, reverse_read_2], sample = sample)
        assert not isolate.check_matches(isolate_2, check_relationships = "properties")
        assert not isolate_2.check_matches(isolate, check_relationships = "properties")
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "OUH", lab_section = "F")
        seq_request_2 = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                   mads_proevenr = "F99123456", requested_by = "ABC",
                                   resistens = False, identifikation = True,
                                   relaps = False, toxingen_identifikation = False,
                                   hai = False, miljoeproeve = False, udbrud = False,
                                   overvaagning = False, forskning = False, andet = False,
                                   request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                   institution = "KMA",
                                   gram_positiv = True, gram_negativ = False,
                                   request_comment = "", secondary_id = "",
                                   registered_by = "XYZ")
        eluate_3 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        forward_read_3 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/forward_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_3 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/reverse_readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        isolate_3 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "",
                            institution = "KMA", requests = [seq_request_2],
                            eluates = [eluate_3],
                            read_files = [forward_read_3, reverse_read_3], sample = sample_2)
        assert not isolate.check_matches(isolate_3, check_relationships = "properties")
        assert not isolate_3.check_matches(isolate, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        session.add(isolate)
        isolate_in_db = session.query(Isolate).first()
        assert isolate.check_matches(isolate_in_db, check_relationships = "strict")
        assert isolate_in_db.check_matches(isolate, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        sample_2 = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                          isolates = [isolate])
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "",
                            institution = "KMA", requests = [self.seq_request],
                            eluates = [self.eluate],
                            read_files = [self.forward_read, self.reverse_read], sample = sample_2)
        assert not isolate.check_matches(isolate_2, check_relationships = "strict")
        assert not isolate_2.check_matches(isolate, check_relationships = "strict")

    def test_mismatch_isolate(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA", requests = [self.seq_request],
                          eluates = [self.eluate],
                          read_files = [self.forward_read, self.reverse_read])
        sample = Sample(mads_proevenr = "F99123456", institution = "KMA", lab_section = "F",
                        isolates = [isolate])
        isolate.sample = sample
        isolate_2 = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                            preliminary_organism_name = "Placeholderia",
                            final_organism_name = "Placeholderia fakeorum",
                            wgs_serotyping = "FakeSerotype",
                            institution = "KMA", requests = [self.seq_request],
                            eluates = [self.eluate],
                            read_files = [self.forward_read, self.reverse_read],
                            sample = sample)
        assert not isolate.check_matches(isolate_2)
        assert not isolate_2.check_matches(isolate)

    def test_mismatch_isolate_diff_type(self):
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia", institution = "KMA")
        assert not isolate.check_matches(self.eluate)


class TestSequencingRun(unittest.TestCase):
    forward_read = ReadFile(readfile_id = 1, read_direction = "forward",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile_1",
                            eluat_id = 1, institution = "KMA")
    reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile_2",
                            eluat_id = 1, institution = "KMA")

    def test_print_run(self):
        test_class = database_tables.SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                                   platform = "MiSeq", sequencing_type = "Illumina",
                                                   flowcell_id = "12345",
                                                   runsheet_path = "path/to/runsheet",
                                                   data_path = "path/to/data", run_user_id = "XYZ",
                                                   run_date = date.fromisoformat("2099-01-01"),
                                                   institution = "KMA",
                                                   read_files = [self.forward_read,
                                                                 self.reverse_read])
        expected_repr = "SequencingRun(seq_run_id=1, seq_run_name=ILM_RUN0001, " \
                        "run_date=2099-01-01, run_user_id=XYZ, " \
                        "platform=MiSeq, sequencing_type=Illumina, " \
                        "flowcell_id=12345, institution=KMA, " \
                        "runsheet_path=path/to/runsheet, " \
                        "data_path=path/to/data, read_files=['ReadFile(path/to/readfile_1)', " \
                        "'ReadFile(path/to/readfile_2)'])"

        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_seqrun_no_reads_to_dict(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345", runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA")
        expected_result = {"seq_run_id": 1, "seq_run_name": "ILM_RUN0001",
                           "platform": "MiSeq", "sequencing_type": "Illumina",
                           "flowcell_id": "12345", "runsheet_path": "path/to/runsheet",
                           "data_path": "path/to/data", "run_user_id": "XYZ",
                           "run_date": date.fromisoformat("2099-01-01"),
                           "institution": "KMA", "read_files": []}
        assert sequencing_run.to_dict() == expected_result

    def test_seqrun_with_reads_to_dict(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345", runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        expected_result = {"seq_run_id": 1, "seq_run_name": "ILM_RUN0001",
                           "platform": "MiSeq", "sequencing_type": "Illumina",
                           "flowcell_id": "12345", "runsheet_path": "path/to/runsheet",
                           "data_path": "path/to/data", "run_user_id": "XYZ",
                           "run_date": date.fromisoformat("2099-01-01"),
                           "institution": "KMA", "read_files": [self.forward_read,
                                                                self.reverse_read]}
        assert sequencing_run.to_dict() == expected_result

    def test_match_seqrun_with_reads(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345",
                                       runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_1",
                                  eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_2",
                                  eluat_id = 1, institution = "KMA")
        sequencing_run_2 = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                         platform = "MiSeq", sequencing_type = "Illumina",
                                         flowcell_id = "12345",
                                         runsheet_path = "path/to/runsheet",
                                         data_path = "path/to/data", run_user_id = "XYZ",
                                         run_date = date.fromisoformat("2099-01-01"),
                                         institution = "KMA", read_files = [forward_read_2,
                                                                            reverse_read_2])
        assert sequencing_run.check_matches(sequencing_run_2, check_relationships = "properties")
        assert sequencing_run_2.check_matches(sequencing_run, check_relationships = "properties")

    def test_mismatch_seqrun_with_reads(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345",
                                       runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile",
                                  eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile",
                                  eluat_id = 1, institution = "KMA")
        sequencing_run_2 = SequencingRun(seq_run_id = 2, seq_run_name = "ILM_RUN0001",
                                         platform = "MiSeq", sequencing_type = "Illumina",
                                         flowcell_id = "12345",
                                         runsheet_path = "path/to/runsheet",
                                         data_path = "path/to/data", run_user_id = "XYZ",
                                         run_date = date.fromisoformat("2099-01-01"),
                                         institution = "KMA", read_files = [forward_read_2,
                                                                            reverse_read_2])
        assert not sequencing_run.check_matches(sequencing_run_2,
                                                check_relationships = "properties")
        assert not sequencing_run_2.check_matches(sequencing_run,
                                                  check_relationships = "properties")

    def test_mismatch_seqrun_diff_type(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345",
                                       runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        assert not sequencing_run.check_matches(self.forward_read)

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345",
                                       runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        session.add(sequencing_run)
        seqrun_in_db = session.query(SequencingRun).first()
        assert sequencing_run.check_matches(seqrun_in_db, check_relationships = "strict")
        assert seqrun_in_db.check_matches(sequencing_run, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345",
                                       runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA", read_files = [self.forward_read,
                                                                          self.reverse_read])
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_1",
                                  eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_2",
                                  eluat_id = 1, institution = "KMA")
        sequencing_run_2 = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                         platform = "MiSeq", sequencing_type = "Illumina",
                                         flowcell_id = "12345",
                                         runsheet_path = "path/to/runsheet",
                                         data_path = "path/to/data", run_user_id = "XYZ",
                                         run_date = date.fromisoformat("2099-01-01"),
                                         institution = "KMA", read_files = [forward_read_2,
                                                                            reverse_read_2])
        assert not sequencing_run.check_matches(sequencing_run_2, check_relationships = "strict")
        assert not sequencing_run_2.check_matches(sequencing_run, check_relationships = "strict")


class TestSeqRequest(unittest.TestCase):
    isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                      preliminary_organism_name = "Placeholderia",
                      final_organism_name = "Placeholderia fakeorum",
                      wgs_serotyping = "",
                      institution = "KMA")
    assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                        mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                        assembly_type = "illumina", assembler = "shovill/skesa",
                        pipeline_version = "2.0.0",
                        assembly_file = "path/to/assembly/file.fna",
                        assembly_dir = "path/to/assembly",
                        n50 = 10000,
                        num_contigs = 500,
                        genome_size = 2000000,
                        coverage = 30.0,
                        release = False,
                        analysis_user = "XYZ",
                        release_user = "ABC",
                        timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                        pipeline_organism_name = "Placeholderia fakeorum",
                        institution = "KMA")

    def test_print_request_grampositive(self):
        test_class = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                mads_proevenr = "F99123456", requested_by = "ABC",
                                resistens = False, identifikation = True,
                                relaps = False, toxingen_identifikation = False,
                                hai = False, miljoeproeve = False, udbrud = False,
                                overvaagning = False, forskning = False,
                                andet = False,
                                request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                institution = "KMA",
                                gram_positiv = True, gram_negativ = False,
                                request_comment = "", secondary_id = "",
                                registered_by = "XYZ", isolate = self.isolate,
                                assembly = [self.assembly])
        expected_repr = "SeqRequest(request_id=1, mads_isolatnr=F99123456-1, " \
                        "mads_proevenr=F99123456, requested_by=ABC, " \
                        "request_date=2099-01-01 00:00:00, " \
                        "institution=KMA, " \
                        "gram_status=positive, " \
                        "request_comment=, secondary_id=, registered_by=XYZ, " \
                        "isolate=Isolate(F99123456-1), " \
                        "assembly=['Assembly(path/to/assembly/file.fna)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_print_request_gramnegative(self):
        test_class = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                mads_proevenr = "F99123456", requested_by = "ABC",
                                resistens = False, identifikation = True,
                                relaps = False, toxingen_identifikation = False,
                                hai = False, miljoeproeve = False, udbrud = False,
                                overvaagning = False, forskning = False,
                                andet = False,
                                request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                institution = "KMA",
                                gram_positiv = False, gram_negativ = True,
                                                request_comment = "", secondary_id = "",
                                                registered_by = "XYZ")
        expected_repr = "SeqRequest(request_id=1, mads_isolatnr=F99123456-1, " \
                        "mads_proevenr=F99123456, requested_by=ABC, " \
                        "request_date=2099-01-01 00:00:00, " \
                        "institution=KMA, " \
                        "gram_status=negative, " \
                        "request_comment=, secondary_id=, registered_by=XYZ, isolate=Isolate()," \
                        " assembly=)"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_print_request_ambiguous(self):
        test_class = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                mads_proevenr = "F99123456", requested_by = "ABC",
                                resistens = False, identifikation = True,
                                relaps = False, toxingen_identifikation = False,
                                hai = False, miljoeproeve = False, udbrud = False,
                                overvaagning = False, forskning = False,
                                andet = False,
                                request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                institution = "KMA",
                                gram_positiv = False, gram_negativ = False,
                                request_comment = "", secondary_id = "",
                                registered_by = "XYZ")
        expected_repr = "SeqRequest(request_id=1, mads_isolatnr=F99123456-1, " \
                        "mads_proevenr=F99123456, requested_by=ABC, " \
                        "request_date=2099-01-01 00:00:00, " \
                        "institution=KMA, " \
                        "gram_status=ambiguous, " \
                        "request_comment=, secondary_id=, registered_by=XYZ, isolate=Isolate()," \
                        " assembly=)"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_seq_request_to_dict(self):
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ")
        expected_result = {"request_id": 1, "mads_isolatnr": "F99123456-1",
                           "mads_proevenr": "F99123456", "requested_by": "ABC",
                           "resistens": False, "identifikation": True,
                           "relaps": False, "toxingen_identifikation": False,
                           "hai": False, "miljoeproeve": False, "udbrud": False,
                           "overvaagning": False, "forskning": False, "andet": False,
                           "request_date": datetime.fromisoformat("2099-01-01 00:00:00"),
                           "institution": "KMA",
                           "gram_positiv": False, "gram_negativ": False,
                           "request_comment": "", "secondary_id": "",
                           "registered_by": "XYZ", "isolate": None, "assembly": []}
        assert seq_request.to_dict() == expected_result

    def test_seq_request_with_data_to_dict(self):
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [self.assembly],
                                 isolate = self.isolate)
        expected_result = {"request_id": 1, "mads_isolatnr": "F99123456-1",
                           "mads_proevenr": "F99123456", "requested_by": "ABC",
                           "resistens": False, "identifikation": True,
                           "relaps": False, "toxingen_identifikation": False,
                           "hai": False, "miljoeproeve": False, "udbrud": False,
                           "overvaagning": False, "forskning": False, "andet": False,
                           "request_date": datetime.fromisoformat("2099-01-01 00:00:00"),
                           "institution": "KMA",
                           "gram_positiv": False, "gram_negativ": False,
                           "request_comment": "", "secondary_id": "",
                           "registered_by": "XYZ", "isolate": self.isolate,
                           "assembly": [self.assembly]}
        assert seq_request.to_dict() == expected_result

    def test_match_seq_request(self):
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [self.assembly],
                                 isolate = self.isolate)
        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA")
        seq_request_2 = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                   mads_proevenr = "F99123456", requested_by = "ABC",
                                   resistens = False, identifikation = True,
                                   relaps = False, toxingen_identifikation = False,
                                   hai = False, miljoeproeve = False, udbrud = False,
                                   overvaagning = False, forskning = False, andet = False,
                                   request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                   institution = "KMA",
                                   gram_positiv = False, gram_negativ = False,
                                   request_comment = "", secondary_id = "",
                                   registered_by = "XYZ", assembly = [assembly_2],
                                   isolate = self.isolate)
        assert seq_request.check_matches(seq_request_2, check_relationships = "properties")
        assert seq_request_2.check_matches(seq_request, check_relationships = "properties")

    def test_assert_no_match_seqrequest(self):
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [self.assembly],
                                 isolate = self.isolate)
        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA")
        seq_request_2 = SeqRequest(request_id = 2, mads_isolatnr = "F99123456-1",
                                   mads_proevenr = "F99123456", requested_by = "ABC",
                                   resistens = False, identifikation = True,
                                   relaps = False, toxingen_identifikation = False,
                                   hai = False, miljoeproeve = False, udbrud = False,
                                   overvaagning = False, forskning = False, andet = False,
                                   request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                   institution = "KMA",
                                   gram_positiv = False, gram_negativ = False,
                                   request_comment = "", secondary_id = "",
                                   registered_by = "XYZ", assembly = [assembly_2],
                                   isolate = self.isolate)
        assert not seq_request.check_matches(seq_request_2, check_relationships = "properties")
        assert not seq_request_2.check_matches(seq_request, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA")
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA")
        seq_request = SeqRequest(mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [assembly],
                                 isolate = isolate)
        session.add(seq_request)
        request_in_db = session.query(SeqRequest).first()
        assert seq_request.check_matches(request_in_db, check_relationships = "strict")
        assert request_in_db.check_matches(seq_request, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        seq_request = SeqRequest(mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [self.assembly],
                                 isolate = self.isolate)
        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA")
        seq_request_2 = SeqRequest(mads_isolatnr = "F99123456-1",
                                   mads_proevenr = "F99123456", requested_by = "ABC",
                                   resistens = False, identifikation = True,
                                   relaps = False, toxingen_identifikation = False,
                                   hai = False, miljoeproeve = False, udbrud = False,
                                   overvaagning = False, forskning = False, andet = False,
                                   request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                   institution = "KMA",
                                   gram_positiv = False, gram_negativ = False,
                                   request_comment = "", secondary_id = "",
                                   registered_by = "XYZ", assembly = [assembly_2],
                                   isolate = self.isolate)
        assert not seq_request.check_matches(seq_request_2, check_relationships = "strict")
        assert not seq_request_2.check_matches(seq_request, check_relationships = "strict")

    def test_mismatch_diff_type_seqrequest(self):
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ", assembly = [self.assembly],
                                 isolate = self.isolate)

        assert not seq_request.check_matches(self.isolate)


class TestExtractRun(unittest.TestCase):
    eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)

    def test_print_extractrun(self):
        test_class = database_tables.ExtractRun(extract_run_id = 1,
                                                extract_run_date = date.fromisoformat("2099-01-01"),
                                                extract_run_user = "XYZ",
                                                extract_run_name = "0001",
                                                method = "",
                                                method_version = "0.1.0",
                                                institution = "KMA", eluates=[self.eluate])
        expected_repr = "ExtractRun(extract_run_id=1, " \
                        "extract_run_date=2099-01-01, " \
                        "extract_run_user=XYZ, " \
                        "extract_run_name=0001, " \
                        "method=, " \
                        "method_version=0.1.0, " \
                        "institution=KMA, eluates=['Eluate(1)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_extract_run_to_dict(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA",
                                 eluates = [self.eluate])
        expected_result = {"extract_run_id": 1,
                           "extract_run_date": date.fromisoformat("2099-01-01"),
                           "extract_run_user": "XYZ",
                           "extract_run_name": "0001",
                           "method": "",
                           "method_version": "0.1.0",
                           "institution": "KMA", "eluates": [self.eluate]}
        assert extract_run.to_dict() == expected_result

    def test_match_extractrun(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA")
        extract_run_2 = ExtractRun(extract_run_id = 1,
                                   extract_run_date = date.fromisoformat("2099-01-01"),
                                   extract_run_user = "XYZ",
                                   extract_run_name = "0001",
                                   method = "",
                                   method_version = "0.1.0",
                                   institution = "KMA")
        assert extract_run.check_matches(extract_run_2)
        assert extract_run_2.check_matches(extract_run)

    def test_match_extractrun_eluate(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA",
                                 eluates = [self.eluate])
        eluate2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        extract_run_2 = ExtractRun(extract_run_id = 1,
                                   extract_run_date = date.fromisoformat("2099-01-01"),
                                   extract_run_user = "XYZ",
                                   extract_run_name = "0001",
                                   method = "",
                                   method_version = "0.1.0",
                                   institution = "KMA",
                                   eluates = [eluate2])
        assert extract_run.check_matches(extract_run_2, check_relationships = "properties")
        assert extract_run_2.check_matches(extract_run, check_relationships = "properties")

    def test_mismatch_extractrun(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA")
        eluate2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-2", extract_run_id = 1)
        extract_run_2 = ExtractRun(extract_run_id = 1,
                                   extract_run_date = date.fromisoformat("2099-01-01"),
                                   extract_run_user = "XYZ",
                                   extract_run_name = "0001",
                                   method = "",
                                   method_version = "0.1.0",
                                   institution = "KMA",
                                   eluates = [eluate2])
        assert not extract_run.check_matches(extract_run_2, check_relationships = "properties")
        assert not extract_run_2.check_matches(extract_run, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA",
                                 eluates = [self.eluate])
        session.add(extract_run)
        result_in_db = session.query(ExtractRun).first()
        assert extract_run.check_matches(result_in_db, check_relationships = "strict")
        assert result_in_db.check_matches(extract_run, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA",
                                 eluates = [self.eluate])
        eluate2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        extract_run_2 = ExtractRun(extract_run_id = 1,
                                   extract_run_date = date.fromisoformat("2099-01-01"),
                                   extract_run_user = "XYZ",
                                   extract_run_name = "0001",
                                   method = "",
                                   method_version = "0.1.0",
                                   institution = "KMA",
                                   eluates = [eluate2])
        assert not extract_run.check_matches(extract_run_2, check_relationships = "strict")
        assert not extract_run_2.check_matches(extract_run, check_relationships = "strict")

    def test_mismatch_diff_type_extractrun(self):
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA")
        extract_dict = {"extract_run_id": 1,
                        "extract_run_date": date.fromisoformat("2099-01-01"),
                        "extract_run_user": "XYZ",
                        "extract_run_name": "0001",
                        "method": "",
                        "method_version": "0.1.0",
                        "institution": "KMA"}
        assert not extract_run.check_matches(extract_dict)


class TestEluate(unittest.TestCase):
    forward_read = ReadFile(readfile_id = 1, read_direction = "forward",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile_1",
                            batch_id = 1, eluat_id = 1, institution = "KMA")
    reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile_2",
                            batch_id = 1, eluat_id = 1, institution = "KMA")
    isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                      preliminary_organism_name = "Placeholderia",
                      final_organism_name = "Placeholderia fakeorum",
                      wgs_serotyping = "",
                      institution = "KMA",
                      read_files = [forward_read, reverse_read])
    extract_run = ExtractRun(extract_run_id = 1,
                             extract_run_date = date.fromisoformat("2099-01-01"),
                             extract_run_user = "XYZ",
                             extract_run_name = "0001",
                             method = "",
                             method_version = "0.1.0",
                             institution = "KMA")

    def setUp(self) -> None:
        self.isolate.eluates = []

    def test_print_eluate(self):
        test_class = database_tables.Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1",
                                            extract_run_id = 1)
        expected_repr = "Eluate(eluat_id=1, mads_isolatnr=F99123456-1, extract_run_id=1, " \
                        "extract_run=ExtractRun(), isolate=Isolate(), read_files=)"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_print_eluate_relationships(self):
        test_class = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                            isolate = self.isolate,
                            read_files = [self.forward_read, self.reverse_read],
                            extract_run = self.extract_run)
        expected_repr = "Eluate(eluat_id=1, mads_isolatnr=F99123456-1, extract_run_id=1, " \
                        "extract_run=ExtractRun(1), isolate=Isolate(F99123456-1), " \
                        "read_files=['ReadFile(path/to/readfile_1)', " \
                        "'ReadFile(path/to/readfile_2)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_eluate_to_dict(self):
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = self.isolate, read_files = [self.forward_read, self.reverse_read],
                        extract_run = self.extract_run)

        expected_result = {"eluat_id": 1, "mads_isolatnr": "F99123456-1", "extract_run_id": 1,
                           "isolate": self.isolate, "extract_run": self.extract_run,
                           "read_files": [self.forward_read, self.reverse_read]}
        assert eluate.to_dict() == expected_result

    def test_match_eluate(self):
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = self.isolate, read_files = [self.forward_read, self.reverse_read],
                        extract_run = self.extract_run)
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_1",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_2",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        eluate_2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                          isolate = self.isolate,
                          read_files = [forward_read_2, reverse_read_2],
                          extract_run = self.extract_run)
        # TODO: handle repeated addition of eluates?
        assert eluate.check_matches(eluate_2, check_relationships = "properties")
        assert eluate_2.check_matches(eluate, check_relationships = "properties")

    def test_mismatch_eluate(self):
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = self.isolate, read_files = [self.forward_read, self.reverse_read],
                        extract_run = self.extract_run)
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile2",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile3",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        eluate_2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-2", extract_run_id = 1,
                          isolate = self.isolate,
                          read_files = [forward_read_2, reverse_read_2],
                          extract_run = self.extract_run)
        assert not eluate.check_matches(eluate_2, check_relationships = "properties")
        assert not eluate_2.check_matches(eluate, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile_1",
                                batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile_2",
                                batch_id = 1, eluat_id = 1, institution = "KMA")
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA",
                          read_files = [forward_read, reverse_read])
        extract_run = ExtractRun(extract_run_id = 1,
                                 extract_run_date = date.fromisoformat("2099-01-01"),
                                 extract_run_user = "XYZ",
                                 extract_run_name = "0001",
                                 method = "",
                                 method_version = "0.1.0",
                                 institution = "KMA")
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = isolate, read_files = [forward_read, reverse_read],
                        extract_run = extract_run)
        print(eluate.to_dict())
        session.add(eluate)
        result_in_db = session.query(Eluate).first()
        assert eluate.check_matches(result_in_db, check_relationships = "strict")
        assert result_in_db.check_matches(eluate, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = self.isolate, read_files = [self.forward_read, self.reverse_read],
                        extract_run = self.extract_run)
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_1",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        reverse_read_2 = ReadFile(readfile_id = 2, read_direction = "reverse",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile_2",
                                  batch_id = 1, eluat_id = 1, institution = "KMA")
        eluate_2 = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-2", extract_run_id = 1,
                          isolate = self.isolate,
                          read_files = [forward_read_2, reverse_read_2],
                          extract_run = self.extract_run)
        assert not eluate.check_matches(eluate_2, check_relationships = "strict")
        assert not eluate_2.check_matches(eluate, check_relationships = "strict")

    def test_mismatch_diff_type_eluate(self):
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1,
                        isolate = self.isolate, read_files = [self.forward_read, self.reverse_read],
                        extract_run = self.extract_run)
        assert not eluate.check_matches(self.isolate)


class TestReadFile(unittest.TestCase):
    isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                      preliminary_organism_name = "Placeholderia",
                      final_organism_name = "Placeholderia fakeorum",
                      wgs_serotyping = "",
                      institution = "KMA")
    eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
    sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                   platform = "MiSeq", sequencing_type = "Illumina",
                                   flowcell_id = "12345", runsheet_path = "path/to/runsheet",
                                   data_path = "path/to/data", run_user_id = "XYZ",
                                   run_date = date.fromisoformat("2099-01-01"),
                                   institution = "KMA")
    assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                           reverse_readfile_id = 2, long_readfile_id = "")

    def test_print_readfile(self):
        test_class = database_tables.ReadFile(readfile_id = 1, read_type = "Illumina",
                                              read_direction = "forward",
                                              mads_isolatnummer = "F99123456-1",
                                              read_file = "path/to/readfile",
                                              batch_id = 1, eluat_id = 1, institution = "KMA")
        expected_repr = "ReadFile(readfile_id=1, read_type=Illumina, read_direction=forward, " \
                        "mads_isolatnummer=F99123456-1, " \
                        "read_file=path/to/readfile, " \
                        "batch_id=1, eluat_id=1, institution=KMA, isolate=Isolate()," \
                        " sequencing_run=SequencingRun()," \
                        " eluate=Eluate(), assembly_readfiles=[])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_print_readfile_relationships(self):
        test_class = database_tables.ReadFile(readfile_id = 1, read_direction = "forward",
                                              read_type = "Illumina",
                                              mads_isolatnummer = "F99123456-1",
                                              read_file = "path/to/readfile",
                                              batch_id = 1, eluat_id = 1, institution = "KMA",
                                              sequencing_run = self.sequencing_run,
                                              isolate = self.isolate,
                                              eluate = self.eluate,
                                              assembly_readfiles = [self.assembly_readfiles])
        expected_repr = "ReadFile(readfile_id=1, read_type=Illumina, read_direction=forward, " \
                        "mads_isolatnummer=F99123456-1, " \
                        "read_file=path/to/readfile, " \
                        "batch_id=1, eluat_id=1, institution=KMA, isolate=Isolate(F99123456-1)," \
                        " sequencing_run=SequencingRun(1)," \
                        " eluate=Eluate(1), assembly_readfiles=['AssemblyReadFiles(1)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_read_file(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate,
                                assembly_readfiles = [self.assembly_readfiles])
        expected_result = {"readfile_id": 1, "read_type": "Illumina", "read_direction": "forward",
                           "mads_isolatnummer": "F99123456-1",
                           "read_file": "path/to/readfile",
                           "batch_id": 1, "eluat_id": 1, "institution": "KMA",
                           "sequencing_run": self.sequencing_run, "isolate": self.isolate,
                           "eluate": self.eluate,
                           "assembly_readfiles": [self.assembly_readfiles]}
        assert forward_read.to_dict() == expected_result

    def test_match_readfile(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate,
                                assembly_readfiles = [self.assembly_readfiles])
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "")
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",  read_type = "Illumina",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA",
                                  sequencing_run = self.sequencing_run, isolate = self.isolate,
                                  eluate = self.eluate,
                                  assembly_readfiles = [assembly_readfiles_2])
        assert forward_read.check_matches(forward_read_2)
        assert forward_read_2.check_matches(forward_read)

    def test_mismatch_readfile(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate)
        reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate)
        assert not forward_read.check_matches(reverse_read)
        assert not reverse_read.check_matches(forward_read)

    def test_mismatch_readfile_properties(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate,
                                assembly_readfiles = [self.assembly_readfiles])
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 2, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "")
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward",
                                  read_type = "Illumina",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA",
                                  sequencing_run = self.sequencing_run, isolate = self.isolate,
                                  eluate = self.eluate,
                                  assembly_readfiles = [assembly_readfiles_2])
        assert not self.assembly_readfiles.check_matches(assembly_readfiles_2)
        assert not forward_read.check_matches(forward_read_2, check_relationships = "properties")
        assert not forward_read_2.check_matches(forward_read, check_relationships = "properties")

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        isolate = Isolate(mads_isolatnr = "F99123456-1", mads_proevenr = "F99123456",
                          preliminary_organism_name = "Placeholderia",
                          final_organism_name = "Placeholderia fakeorum",
                          wgs_serotyping = "",
                          institution = "KMA")
        eluate = Eluate(eluat_id = 1, mads_isolatnr = "F99123456-1", extract_run_id = 1)
        sequencing_run = SequencingRun(seq_run_id = 1, seq_run_name = "ILM_RUN0001",
                                       platform = "MiSeq", sequencing_type = "Illumina",
                                       flowcell_id = "12345", runsheet_path = "path/to/runsheet",
                                       data_path = "path/to/data", run_user_id = "XYZ",
                                       run_date = date.fromisoformat("2099-01-01"),
                                       institution = "KMA")
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "")
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = sequencing_run, isolate = isolate,
                                eluate = eluate,
                                assembly_readfiles = [assembly_readfiles])
        session.add(forward_read)
        result_in_db = session.query(ReadFile).first()
        assert forward_read.check_matches(result_in_db, check_relationships = "strict")
        assert result_in_db.check_matches(forward_read, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate,
                                assembly_readfiles = [self.assembly_readfiles])
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "")
        forward_read_2 = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                  mads_isolatnummer = "F99123456-1",
                                  read_file = "path/to/readfile",
                                  batch_id = 1, eluat_id = 1, institution = "KMA",
                                  sequencing_run = self.sequencing_run, isolate = self.isolate,
                                  eluate = self.eluate,
                                  assembly_readfiles = [assembly_readfiles_2])
        assert not forward_read.check_matches(forward_read_2, check_relationships = "strict")
        assert not forward_read_2.check_matches(forward_read, check_relationships = "strict")

    def test_mismatch_diff_type_readfile(self):
        forward_read = ReadFile(readfile_id = 1, read_direction = "forward", read_type = "Illumina",
                                mads_isolatnummer = "F99123456-1",
                                read_file = "path/to/readfile",
                                batch_id = 1, eluat_id = 1, institution = "KMA",
                                sequencing_run = self.sequencing_run, isolate = self.isolate,
                                eluate = self.eluate,
                                assembly_readfiles = [self.assembly_readfiles])
        assert not forward_read.check_matches(self.isolate)


class TestAssemblyReadfiles(unittest.TestCase):
    forward_read = ReadFile(readfile_id = 1, read_direction = "forward",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile",
                            batch_id = 1, eluat_id = 1, institution = "KMA")
    reverse_read = ReadFile(readfile_id = 2, read_direction = "reverse",
                            mads_isolatnummer = "F99123456-1",
                            read_file = "path/to/readfile",
                            batch_id = 1, eluat_id = 1, institution = "KMA")
    assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                        mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                        assembly_type = "illumina", assembler = "shovill/skesa",
                        pipeline_version = "2.0.0",
                        assembly_file = "path/to/assembly/file.fna",
                        assembly_dir = "path/to/assembly",
                        n50 = 10000,
                        num_contigs = 500,
                        genome_size = 2000000,
                        coverage = 30.0,
                        release = False,
                        analysis_user = "XYZ",
                        release_user = "ABC",
                        timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                        pipeline_organism_name = "Placeholderia fakeorum",
                        institution = "KMA")

    def test_print_assembly_reads(self):
        test_class = database_tables.AssemblyReadFiles(assembly_readfiles_id = 1,
                                                       forward_readfile_id = 1,
                                                       reverse_readfile_id = 2,
                                                       long_readfile_id = "")
        expected_repr = "AssemblyReadFiles(assembly_readfiles_id=1, forward_readfile_id=1, " \
                        "reverse_readfile_id=2, long_readfile_id=, assembly=[])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_print_assembly_reads_with_asm(self):
        test_class = database_tables.AssemblyReadFiles(assembly_readfiles_id = 1,
                                                       forward_readfile_id = 1,
                                                       reverse_readfile_id = 2,
                                                       long_readfile_id = "",
                                                       assembly = [self.assembly])
        expected_repr = "AssemblyReadFiles(assembly_readfiles_id=1, forward_readfile_id=1, " \
                        "reverse_readfile_id=2, long_readfile_id=," \
                        " assembly=['Assembly(path/to/assembly/file.fna)'])"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_assembly_read_files(self):
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])
        expected_result = {"assembly_readfiles_id": 1, "forward_readfile_id": 1,
                           "reverse_readfile_id": 2, "long_readfile_id": "",
                           "forward_read": self.forward_read,
                           "reverse_read": self.reverse_read, "assembly": [self.assembly],
                           "long_read": None}
        assert assembly_readfiles.to_dict() == expected_result

    def test_match_assembly_readfiles(self):
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "",
                                                 forward_read = self.forward_read,
                                                 reverse_read = self.reverse_read,
                                                 assembly = [self.assembly])
        assert assembly_readfiles.check_matches(assembly_readfiles_2,
                                                check_relationships = "properties")
        assert assembly_readfiles_2.check_matches(assembly_readfiles,
                                                  check_relationships = "properties")

    def test_mismatch_assembly_readfiles(self):
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 2, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "",
                                                 forward_read = self.forward_read,
                                                 reverse_read = self.reverse_read,
                                                 assembly = [self.assembly])
        assert not assembly_readfiles.check_matches(assembly_readfiles_2)
        assert not assembly_readfiles_2.check_matches(assembly_readfiles)

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])
        session.add(assembly_readfiles)
        result_in_db = session.query(AssemblyReadFiles).first()
        assert assembly_readfiles.check_matches(result_in_db, check_relationships = "strict")
        assert result_in_db.check_matches(assembly_readfiles, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])
        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA")
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "",
                                                 forward_read = self.forward_read,
                                                 reverse_read = self.reverse_read,
                                                 assembly = [assembly_2])
        assert not assembly_readfiles.check_matches(assembly_readfiles_2,
                                                    check_relationships = "strict")
        assert not assembly_readfiles_2.check_matches(assembly_readfiles,
                                                      check_relationships = "strict")

    def test_mismatch_diff_type_asm_readfiles(self):
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "",
                                               forward_read = self.forward_read,
                                               reverse_read = self.reverse_read,
                                               assembly = [self.assembly])

        assert not assembly_readfiles.check_matches(self.assembly)


class TestAssembly(unittest.TestCase):
    assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                           reverse_readfile_id = 2, long_readfile_id = "")
    seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                             mads_proevenr = "F99123456", requested_by = "ABC",
                             resistens = False, identifikation = True,
                             relaps = False, toxingen_identifikation = False,
                             hai = False, miljoeproeve = False, udbrud = False,
                             overvaagning = False, forskning = False, andet = False,
                             request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                             institution = "KMA",
                             gram_positiv = False, gram_negativ = False,
                             request_comment = "", secondary_id = "",
                             registered_by = "XYZ")


    def test_print_assembly(self):
        test_class = database_tables.Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                                              mads_isolatnr = "F99123456-1",
                                              db_assembly_readfiles_id = 1,
                                              assembly_type = "illumina",
                                              assembler = "shovill/skesa",
                                              pipeline_version = "2.0.0",
                                              assembly_file = "path/to/assembly/file.fna",
                                              assembly_dir = "path/to/assembly",
                                              n50 = 10000,
                                              num_contigs = 500,
                                              genome_size = 2000000,
                                              coverage = 30.0,
                                              release = False,
                                              analysis_user = "XYZ",
                                              release_user = "ABC",
                                              timestamp = datetime.fromisoformat(
                                                  "2099-01-01 12:00:00"),
                                              pipeline_organism_name = "Placeholderia fakeorum",
                                              institution = "KMA",
                                              request_id = 1)
        expected_repr = "Assembly(assembly_id=1, mads_proevenr=F99123456, " \
                        "mads_isolatnr=F99123456-1, db_assembly_readfiles_id=1, " \
                        "assembly_type=illumina, assembler=shovill/skesa, " \
                        "pipeline_version=2.0.0, " \
                        "assembly_file=path/to/assembly/file.fna, " \
                        "assembly_dir=path/to/assembly, " \
                        "n50=10000, " \
                        "num_contigs=500, " \
                        "genome_size=2000000, " \
                        "coverage=30.0, " \
                        "release=False, " \
                        "analysis_user=XYZ, " \
                        "release_user=ABC, " \
                        "timestamp=2099-01-01 12:00:00, " \
                        "pipeline_organism_name=Placeholderia fakeorum, " \
                        "institution=KMA, " \
                        "request_id=1)"
        class_repr = test_class.__repr__()
        assert expected_repr == class_repr

    def test_assembly_to_dict(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)
        expected_result = {"assembly_id": 1, "mads_proevenr": "F99123456",
                           "mads_isolatnr": "F99123456-1", "db_assembly_readfiles_id": 1,
                           "assembly_type": "illumina", "assembler": "shovill/skesa",
                           "pipeline_version": "2.0.0",
                           "assembly_file": "path/to/assembly/file.fna",
                           "assembly_dir": "path/to/assembly",
                           "n50": 10000,
                           "num_contigs": 500,
                           "genome_size": 2000000,
                           "coverage": 30.0,
                           "release": False,
                           "analysis_user": "XYZ",
                           "release_user": "ABC",
                           "timestamp": datetime.fromisoformat("2099-01-01 12:00:00"),
                           "pipeline_organism_name": "Placeholderia fakeorum",
                           "institution": "KMA", "seq_request": self.seq_request,
                           "assembly_readfiles": self.assembly_readfiles,
                           "request_id": 1}
        assert assembly.to_dict() == expected_result

    def test_match_assembly(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)

        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA", seq_request = self.seq_request,
                              assembly_readfiles = self.assembly_readfiles,
                              request_id = 1)
        assert assembly.check_matches(assembly_2)
        assert assembly_2.check_matches(assembly)

    def test_match_assembly_properties(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "")

        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA", seq_request = self.seq_request,
                              assembly_readfiles = assembly_readfiles_2,
                              request_id = 1)
        assert assembly.check_matches(assembly_2, check_relationships = "properties")
        assert assembly_2.check_matches(assembly, check_relationships = "properties")

    def test_mismatch_assembly(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)

        assembly_2 = Assembly(assembly_id = 2, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA", seq_request = self.seq_request,
                              assembly_readfiles = self.assembly_readfiles,
                              request_id = 1)
        assert not assembly.check_matches(assembly_2)
        assert not assembly_2.check_matches(assembly)

    def test_match_strict(self):
        engine = create_engine("sqlite+pysqlite:///:memory:", echo = True, future = True)
        session = Session(engine)
        # create all the tables
        database_tables.Base.metadata.create_all(engine)
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        assert not len(session.query(AssemblyReadFiles).all())
        assembly_readfiles = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                               reverse_readfile_id = 2, long_readfile_id = "")
        # TODO: better way to clean up?
        seq_request = SeqRequest(request_id = 1, mads_isolatnr = "F99123456-1",
                                 mads_proevenr = "F99123456", requested_by = "ABC",
                                 resistens = False, identifikation = True,
                                 relaps = False, toxingen_identifikation = False,
                                 hai = False, miljoeproeve = False, udbrud = False,
                                 overvaagning = False, forskning = False, andet = False,
                                 request_date = datetime.fromisoformat("2099-01-01 00:00:00"),
                                 institution = "KMA",
                                 gram_positiv = False, gram_negativ = False,
                                 request_comment = "", secondary_id = "",
                                 registered_by = "XYZ")
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = seq_request,
                            assembly_readfiles = assembly_readfiles)
        session.add(assembly)
        result_in_db = session.query(Assembly).first()
        assert assembly.check_matches(result_in_db, check_relationships = "strict")
        assert result_in_db.check_matches(assembly, check_relationships = "strict")
        session.query(Assembly).delete()
        session.query(AssemblyReadFiles).delete()
        session.query(Eluate).delete()
        session.query(ExtractRun).delete()
        session.query(Isolate).delete()
        session.query(ReadFile).delete()
        session.query(Sample).delete()
        session.query(SeqRequest).delete()
        session.query(SequencingRun).delete()
        session.close()

    def test_mismatch_strict(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)
        assembly_readfiles_2 = AssemblyReadFiles(assembly_readfiles_id = 1, forward_readfile_id = 1,
                                                 reverse_readfile_id = 2, long_readfile_id = "")

        assembly_2 = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                              mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                              assembly_type = "illumina", assembler = "shovill/skesa",
                              pipeline_version = "2.0.0",
                              assembly_file = "path/to/assembly/file.fna",
                              assembly_dir = "path/to/assembly",
                              n50 = 10000,
                              num_contigs = 500,
                              genome_size = 2000000,
                              coverage = 30.0,
                              release = False,
                              analysis_user = "XYZ",
                              release_user = "ABC",
                              timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                              pipeline_organism_name = "Placeholderia fakeorum",
                              institution = "KMA", seq_request = self.seq_request,
                              assembly_readfiles = assembly_readfiles_2,
                              request_id = 1)
        assert not assembly.check_matches(assembly_2,
                                          check_relationships = "strict")
        assert not assembly_2.check_matches(assembly,
                                            check_relationships = "strict")

    def test_mismatch_diff_type_assembly(self):
        assembly = Assembly(assembly_id = 1, mads_proevenr = "F99123456",
                            mads_isolatnr = "F99123456-1", db_assembly_readfiles_id = 1,
                            assembly_type = "illumina", assembler = "shovill/skesa",
                            pipeline_version = "2.0.0",
                            assembly_file = "path/to/assembly/file.fna",
                            assembly_dir = "path/to/assembly",
                            n50 = 10000,
                            num_contigs = 500,
                            genome_size = 2000000,
                            coverage = 30.0,
                            release = False,
                            analysis_user = "XYZ",
                            release_user = "ABC",
                            timestamp = datetime.fromisoformat("2099-01-01 12:00:00"),
                            pipeline_organism_name = "Placeholderia fakeorum",
                            institution = "KMA", seq_request = self.seq_request,
                            assembly_readfiles = self.assembly_readfiles,
                            request_id = 1)

        assert not assembly.check_matches(self.assembly_readfiles)
