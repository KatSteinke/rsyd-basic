#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import unittest

import pandas as pd
import pytest


import add_names_to_db
import set_log

root_logger = set_log.get_stream_log(level="DEBUG")


class TestFindSingleName(unittest.TestCase):
    def test_find_name_success(self):
        true_name = "Fakeobacter fakei"
        name_strain = "Fakeobacter fakei str. 42"
        test_name = add_names_to_db.extract_species_name(name_strain)
        assert true_name == test_name

    def test_find_quoted_name(self):
        true_name = "Fakeobacter fakei"
        name_strain = "'Fakeobacter fakei' str. 42"
        test_name = add_names_to_db.extract_species_name(name_strain)
        assert true_name == test_name

    def test_strip_brackets(self):
        true_name = "Fakeobacter fakei"
        name_strain = "[Fakeobacter] fakei"
        test_name = add_names_to_db.extract_species_name(name_strain)
        assert true_name == test_name

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_warn_messy_name(self):
        true_name = "Fake-obacter fakei"
        name_strain = "Fake-obacter fakei"
        with self._caplog.at_level(logger="add_names_to_db", level=logging.WARNING):
            test_name = add_names_to_db.extract_species_name(name_strain)
            warn_msg = f"No species name found in Fake-obacter fakei. " \
                       f"Using NCBI organism name instead."
            assert self._caplog.record_tuples == [("add_names_to_db", logging.WARNING, warn_msg)]
            assert true_name == test_name


class TestRenameAll(unittest.TestCase):
    def test_rename_success(self):
        organism_db = pd.DataFrame(data = {"Organism Name": ["Fakeobacter fakei str. Fridge Goop",
                                                              "Testobacter madeupensis",
                                                              "[Placeholderia] testii"]})
        success_db = pd.DataFrame(data = {"Organism Name": ["Fakeobacter fakei str. Fridge Goop",
                                                             "Testobacter madeupensis",
                                                             "[Placeholderia] testii"],
                                          "species_name": ["Fakeobacter fakei",
                                                           "Testobacter madeupensis",
                                                           "Placeholderia testii"]})
        test_db = add_names_to_db.add_species_names(organism_db)
        pd.testing.assert_frame_equal(success_db, test_db)

    def test_rename_messy(self):
        organism_db = pd.DataFrame(data = {"Organism Name": ["Fakeobacter fakei str. Fridge Goop",
                                                              "(Testobacter) madeupensis",
                                                              "[Placeholderia] testii"]})
        success_db = pd.DataFrame(data = {"Organism Name": ["Fakeobacter fakei str. Fridge Goop",
                                                             "(Testobacter) madeupensis",
                                                             "[Placeholderia] testii"],
                                          "species_name": ["Fakeobacter fakei",
                                                           "(Testobacter) madeupensis",
                                                           "Placeholderia testii"]})
        test_db = add_names_to_db.add_species_names(organism_db)
        pd.testing.assert_frame_equal(success_db, test_db)


