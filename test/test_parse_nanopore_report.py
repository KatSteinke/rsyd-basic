import logging
import pathlib
import re
import unittest

import pandas as pd
import pytest

from packaging.version import Version

import parse_nanopore_report


class TestParseModelName(unittest.TestCase):
    def test_fail_missing_element(self):
        """Complain when one of the key elements is missing."""
        fail_model = "r941_min_g345"
        error_msg = "Model name r941_min_g345 is missing required elements."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.parse_medaka_version(fail_model)

    def test_fail_missing_version(self):
        """Complain when no version information is provided."""
        model_name = "r103_prom_high"
        error_msg = "Model name r103_prom_high does not contain a guppy or dorado version."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.parse_medaka_version(model_name)

    def test_success_get_model_info(self):
        """Report minimum data on a model."""
        success_data = pd.DataFrame(data={"model_name": ["r103_prom_high_g360"],
                                          "flowcell_version": ["r103"],
                                          "kit_chemistry": [pd.NA],
                                          "translocation": [pd.NA],
                                          "flowcell_type": ["prom"],
                                          "basecalling_mode": ["high"],
                                          "guppy_version": ["3.6.0"],
                                          "dorado_version": [pd.NA]})
        model_name = "r103_prom_high_g360"
        test_data = parse_nanopore_report.parse_medaka_version(model_name)
        print(test_data.to_string())
        pd.testing.assert_frame_equal(success_data, test_data)

    def test_success_handle_extra_info(self):
        """Handle a model name with additional information."""
        success_data = pd.DataFrame(data = {"model_name": ["r1041_e82_260bps_hac_g632"],
                                            "flowcell_version": ["r1041"],
                                            "kit_chemistry": ["e82"],
                                            "translocation": ["260bps"],
                                            "flowcell_type": [pd.NA],
                                            "basecalling_mode": ["hac"],
                                            "guppy_version": ["6.3.2"],
                                            "dorado_version": [pd.NA]})
        model_name = "r1041_e82_260bps_hac_g632"
        test_data = parse_nanopore_report.parse_medaka_version(model_name)
        pd.testing.assert_frame_equal(success_data, test_data)

    def test_success_handle_dorado_model(self):
        """Handle dorado version format."""
        success_data = pd.DataFrame(data = {"model_name": ["r1041_e82_260bps_sup_v4.1.0"],
                                            "flowcell_version": ["r1041"],
                                            "kit_chemistry": ["e82"],
                                            "translocation": ["260bps"],
                                            "flowcell_type": [pd.NA],
                                            "basecalling_mode": ["sup"],
                                            "guppy_version": [pd.NA],
                                            "dorado_version": ["v4.1.0"]})
        model_name = "r1041_e82_260bps_sup_v4.1.0"
        test_data = parse_nanopore_report.parse_medaka_version(model_name)
        pd.testing.assert_frame_equal(success_data, test_data)


class TestParseBasecallParams(unittest.TestCase):
    def test_catch_missing_guppy(self):
        """Complain when there is no guppy filename in the output."""
        error_msg = "Guppy filename not found in guppy arguments"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_no_guppy.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_current_run_params(fail_report)

    def test_catch_bad_guppy(self):
        """Complain when the guppy filename is malformed."""
        error_msg = "Malformed guppy filename guppy_r1.2.3_450bps_HAC - data could not be extracted"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_bad_guppy.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_current_run_params(fail_report)

    def test_catch_bad_guppy_version(self):
        """Complain when the guppy version is malformed."""
        error_msg = "Malformed guppy version 1.2+badbad - data could not be extracted"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_bad_guppy_version.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_current_run_params(fail_report)

    def test_catch_bad_flowcell_type(self):
        """Complain when there is no flowcell type in the output."""
        error_msg = "Flowcell type could not be extracted from flowcell code FLO-106"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_fail_no_flowcell.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_current_run_params(fail_report)

    def test_success_parse_minimal_guppy(self):
        """Parse a report with guppy as the basecaller and only basic information."""
        expected_data = parse_nanopore_report.BasecallModel(flowcell_version = "r941",
                                                            kit_chemistry = None,
                                                            translocation_speed = "450bps",
                                                            flowcell_type = "min",
                                                            basecalling_mode = "hac",
                                                            basecaller = "guppy",
                                                            basecaller_version = Version("5.0.7")
                                                            )
        success_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success_exact.json")
        test_data = parse_nanopore_report.get_current_run_params(success_report)
        assert test_data == expected_data

    def test_success_parse_minimal_dorado(self):
        """Parse a report with dorado as the basecaller and only basic information."""
        # TODO: Nanopore report is unreliable

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_additional_information(self):
        """Parse a report with guppy as the basecaller and additional information."""
        expected_data = parse_nanopore_report.BasecallModel(flowcell_version = "r1041",
                                                            kit_chemistry = "e82",
                                                            translocation_speed = "400bps",
                                                            flowcell_type = "min",
                                                            basecalling_mode = "hac",
                                                            basecaller = "guppy",
                                                            basecaller_version = Version("7.0.5")
                                                            )
        success_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success_newformat.json")
        log_msg = ("Software version not detected in old format,"
                   " trying new format.")
        with self._caplog.at_level(level="INFO", logger="parse_nanopore"):
            test_data = parse_nanopore_report.get_current_run_params(success_report)
            assert ("parse_nanopore", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_data == expected_data


class TestGetBestMedakaModel(unittest.TestCase):
    def test_catch_wrong_basecalling_type(self):
        """Complain when basecalling type is wrong."""
        error_msg = "Basecalling type hyper is not supported."
        fail_report = (pathlib.Path(__file__).parent / "data"/"parse_nanopore_reports"
                       /"report_fail_basecalling.json")
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_version_too_low(self):
        """Complain when the Guppy version is lower than any available models."""
        error_msg = "Guppy version 0.1.0 is too low to be supported."
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_fail_guppy.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_missing_guppy(self):
        """Complain when there is no guppy filename in the output."""
        error_msg = "Guppy filename not found in guppy arguments"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_no_guppy.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_bad_guppy(self):
        """Complain when the guppy filename is malformed."""
        error_msg = "Malformed guppy filename guppy_r1.2.3_450bps_HAC - data could not be extracted"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_bad_guppy.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_bad_guppy_version(self):
        """Complain when the guppy version is malformed."""
        error_msg = "Malformed guppy version 1.2+badbad - data could not be extracted"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_bad_guppy_version.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_bad_flowcell_type(self):
        """Complain when there is no flowcell type in the output."""
        error_msg = "Flowcell type could not be extracted from flowcell code FLO-106"
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_fail_no_flowcell.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_catch_no_match(self):
        """Complain if no models remain after filtering."""
        error_msg = ("No models with the desired combination of flowcell version, basecalling mode,"
                     " translocation speed and/or flowcell type found.")
        fail_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                       / "report_fail_no_match.json")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            parse_nanopore_report.get_best_medaka_model(fail_report)

    def test_get_model_success(self):
        """Ensure the correct model is output for clean input."""
        expected_model = "r941_min_hac_g507"
        success_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success_exact.json")
        test_model = parse_nanopore_report.get_best_medaka_model(success_report)
        assert test_model == expected_model

        prom_model = "r941_prom_hac_g507"
        prom_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success_promethion.json")
        test_prom = parse_nanopore_report.get_best_medaka_model(prom_report)
        assert test_prom == prom_model

    def test_infer_closest_success(self):
        """Ensure the closest model is selected for higher-than-supported guppy versions."""
        expected_model = "r941_min_hac_g507"
        success_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success.json")
        test_model = parse_nanopore_report.get_best_medaka_model(success_report)
        assert test_model == expected_model

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_handle_new_format(self):
        """Ensure the pipeline can handle multiple types of Nanopore report formats."""
        expected_model = "r1041_e82_400bps_hac_g632"
        success_report = (pathlib.Path(__file__).parent / "data" / "parse_nanopore_reports"
                          / "report_success_newformat.json")
        log_msg = ("Software version not detected in old format,"
                   " trying new format.")
        with self._caplog.at_level(level="INFO", logger="parse_nanopore"):
            test_model = parse_nanopore_report.get_best_medaka_model(success_report)
            assert ("parse_nanopore", logging.INFO, log_msg) in self._caplog.record_tuples
        assert test_model == expected_model