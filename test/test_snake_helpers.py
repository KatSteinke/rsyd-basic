#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import pandas as pd
import pytest

import input_names
import snake_helpers


class TestGetReadsFromOverview(unittest.TestCase):
    read_overview = pd.DataFrame(data={"sample": ["1199123456-1", "1199123456-2"],
                                       "r1": ["/path/to/r1", pd.NA],
                                       "r2": ["/path/to/r2", pd.NA],
                                       "extra": [pd.NA, "/path/to/long_reads"]})

    def test_success(self):
        expected_r1 = "/path/to/r1"
        test_r1 = snake_helpers.get_reads_from_overview("1199123456-1", self.read_overview, "r1")
        assert expected_r1 == test_r1

        expected_r2 = "/path/to/r2"
        test_r2 = snake_helpers.get_reads_from_overview("1199123456-1", self.read_overview, "r2")
        assert expected_r2 == test_r2

        expected_long = "/path/to/long_reads"
        test_long = snake_helpers.get_reads_from_overview("1199123456-2", self.read_overview,
                                                          "extra")
        assert expected_long == test_long

    def test_invalid_type(self):
        error_msg = "long is not a valid read type. Valid read types are ['r1', 'r2', 'extra']."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            snake_helpers.get_reads_from_overview("1199123456-2", self.read_overview,
                                                  "long")

    def test_no_reads(self):
        error_msg = "No extra reads found for 1199123456-1."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            snake_helpers.get_reads_from_overview("1199123456-1", self.read_overview,
                                                  "extra")

    def test_no_such_number(self):
        error_msg = "Isolate 1199123456-3 not found in read overview."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            snake_helpers.get_reads_from_overview("1199123456-3", self.read_overview,
                                                  "extra")


class TestGetAssemblies(unittest.TestCase):
    def test_illumina_only(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        illumina_assemblies = ["F99123456-1/assembly/F99123456-1.fna",
                               "F99654321-2/assembly/F99654321-2.fna"]
        test_assemblies = snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples,
                                                           hybrid_samples)
        assert illumina_assemblies == test_assemblies

    def test_nanopore_only(self):
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        hybrid_samples = []
        nanopore_assemblies = ["F99123456-1/nanopore_assembly/F99123456-1.fasta",
                               "F99654321-2/nanopore_assembly/F99654321-2.fasta"]
        test_assemblies = snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples,
                                                           hybrid_samples)
        assert nanopore_assemblies == test_assemblies

    def test_hybrid_samples(self):
        hybrid_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_samples = []
        hybrid_assemblies = ["F99123456-1/hybrid_assembly/F99123456-1.fasta",
                             "F99654321-2/hybrid_assembly/F99654321-2.fasta"]
        test_assemblies = snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples,
                                                           hybrid_samples)
        assert hybrid_assemblies == test_assemblies

    def test_multi_samples(self):
        illumina_samples = ["F99123456-1"]
        nanopore_samples = ["F99654321-2"]
        hybrid_samples = []
        all_assemblies = ["F99123456-1/assembly/F99123456-1.fna",
                          "F99654321-2/nanopore_assembly/F99654321-2.fasta"]
        test_assemblies = snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples,
                                                           hybrid_samples)
        assert all_assemblies == test_assemblies

    def test_add_experiment_name(self):
        experiment_name = "ILM_RUN0000_TEST"
        illumina_samples = ["F99123456-1"]
        nanopore_samples = ["F99654321-2"]
        hybrid_samples = []
        all_assemblies = ["F99123456-1/assembly/ILM_RUN0000_TEST_F99123456-1.fna",
                          "F99654321-2/nanopore_assembly/ILM_RUN0000_TEST_F99654321-2.fasta"]
        test_assemblies = snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples,
                                                           hybrid_samples,
                                                           experiment_name = experiment_name)
        assert all_assemblies == test_assemblies

    def test_fail_no_samples(self):
        illumina_samples = []
        nanopore_samples = []
        hybrid_samples = []
        with pytest.raises(ValueError,
                           match = re.escape("No Illumina, Nanopore or hybrid samples given.")):
            snake_helpers.get_all_assemblies(illumina_samples, nanopore_samples, hybrid_samples)


class TestGetNanoporeRunReport(unittest.TestCase):
    def test_fail_no_nanopore_samples(self):
        """Fail when no nanopore samples are found."""
        read_overview = pd.DataFrame(data = {"sample": ["1199123456-1", "1199123456-2"],
                                             "r1": ["/path/to/r1", pd.NA],
                                             "r2": ["/path/to/r2", pd.NA],
                                             "extra": [pd.NA, pd.NA]})
        error_msg = "No Nanopore samples found."
        with pytest.raises(KeyError, match=re.escape(error_msg)):
            snake_helpers.get_nanopore_run_report(read_overview)

    def test_fail_multiple_runs(self):
        """Fail when nanopore samples come from multiple runs."""
        read_overview = pd.DataFrame(data = {"sample": ["1199123456-1", "1199123456-2"],
                                             "r1": [pd.NA, pd.NA],
                                             "r2": [pd.NA, pd.NA],
                                             "extra": [(pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "run2" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode01"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01")]})
        error_msg = "Nanopore samples from multiple runs are not supported."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            snake_helpers.get_nanopore_run_report(read_overview)

    def test_fail_multiple_reports(self):
        """Fail when there are multiple reports."""
        read_overview = pd.DataFrame(data = {"sample": ["1199123456-1", "1199123456-2"],
                                             "r1": [pd.NA, pd.NA],
                                             "r2": [pd.NA, pd.NA],
                                             "extra": [(pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "run3" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode01"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "run3" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode42")]})
        error_msg = "Multiple run reports found."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            snake_helpers.get_nanopore_run_report(read_overview)

    def test_fail_no_report(self):
        """Complain if the directory doesn't contain a report."""
        read_overview = pd.DataFrame(data = {"sample": ["1199123456-1", "1199123456-2"],
                                             "r1": [pd.NA, pd.NA],
                                             "r2": [pd.NA, pd.NA],
                                             "extra": [(pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "run4" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode01"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "run4" / "rawdata"
                                                        / "subdir" / "fastq_pass" / "barcode42")]})
        error_msg = "No run report found."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            snake_helpers.get_nanopore_run_report(read_overview)

    def test_success(self):
        """Successfully retrieve the path to a Nanopore run report."""
        read_overview = pd.DataFrame(data = {"sample": ["1199123456-1", "1199123456-2"],
                                             "r1": [pd.NA, pd.NA],
                                             "r2": [pd.NA, pd.NA],
                                             "extra": [(pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "snake_helpers" / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode42")]})
        expected_report_path = str(pathlib.Path(__file__).parent / "data"
                                / "snake_helpers" / "rawdata" / "subdir" / "report_testrun.json")
        test_path = snake_helpers.get_nanopore_run_report(read_overview)
        assert test_path == expected_report_path


class TestGetQuast(unittest.TestCase):
    def test_illumina_only(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        illumina_reports = ["F99123456-1/assembly/quast/illumina_report.tsv",
                            "F99654321-2/assembly/quast/illumina_report.tsv"]
        test_reports = snake_helpers.get_all_single_quast(illumina_samples, nanopore_samples,
                                                          hybrid_samples)
        assert illumina_reports == test_reports

    def test_nanopore_only(self):
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        hybrid_samples = []
        nanopore_reports = ["F99123456-1/assembly/quast/nanopore_report.tsv",
                            "F99654321-2/assembly/quast/nanopore_report.tsv"]
        test_reports = snake_helpers.get_all_single_quast(illumina_samples, nanopore_samples,
                                                          hybrid_samples)
        assert nanopore_reports == test_reports

    def test_hybrid_samples(self):
        hybrid_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_samples = []
        hybrid_reports = ["F99123456-1/assembly/quast/hybrid_report.tsv",
                          "F99654321-2/assembly/quast/hybrid_report.tsv"]
        test_reports = snake_helpers.get_all_single_quast(illumina_samples, nanopore_samples,
                                                          hybrid_samples)
        assert hybrid_reports == test_reports

    def test_multi_samples(self):
        illumina_samples = ["F99123456-1"]
        nanopore_samples = ["F99654321-2"]
        hybrid_samples = []
        all_reports = ["F99123456-1/assembly/quast/illumina_report.tsv",
                       "F99654321-2/assembly/quast/nanopore_report.tsv"]
        test_reports = snake_helpers.get_all_single_quast(illumina_samples, nanopore_samples,
                                                          hybrid_samples)
        assert all_reports == test_reports

    def test_fail_no_samples(self):
        illumina_samples = []
        nanopore_samples = []
        hybrid_samples = []
        with pytest.raises(ValueError,
                           match = re.escape("No Illumina, Nanopore or hybrid samples given.")):
            snake_helpers.get_all_single_quast(illumina_samples, nanopore_samples, hybrid_samples)


class TestGetReadReports(unittest.TestCase):
    def test_illumina_only(self):
        """Get Illumina read reports."""
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_reports = ["F99123456-1/assembly/quast/reads_stats/illumina_reads_report.tsv",
                            "F99654321-2/assembly/quast/reads_stats/illumina_reads_report.tsv"]
        test_reports = snake_helpers.get_all_read_reports(illumina_samples, nanopore_samples)
        assert illumina_reports == test_reports

    def test_nanopore_only(self):
        """Get Nanopore read reports."""
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        nanopore_reports = ["F99123456-1/assembly/quast/reads_stats/nanopore_reads_report.tsv",
                            "F99654321-2/assembly/quast/reads_stats/nanopore_reads_report.tsv"]
        test_reports = snake_helpers.get_all_read_reports(illumina_samples, nanopore_samples)
        assert nanopore_reports == test_reports

    def test_multi_samples(self):
        """Get Nanopore and Illumina read reports."""
        illumina_samples = ["F99123456-1"]
        nanopore_samples = ["F99654321-2"]
        all_reports = ["F99123456-1/assembly/quast/reads_stats/illumina_reads_report.tsv",
                       "F99654321-2/assembly/quast/reads_stats/nanopore_reads_report.tsv"]
        test_reports = snake_helpers.get_all_read_reports(illumina_samples, nanopore_samples)
        assert all_reports == test_reports

    def test_fail_no_samples(self):
        """Fail if no samples are given."""
        illumina_samples = []
        nanopore_samples = []
        with pytest.raises(ValueError,
                           match = re.escape("No Illumina or Nanopore samples given.")):
            snake_helpers.get_all_read_reports(illumina_samples, nanopore_samples)


class TestGetQCPlatforms(unittest.TestCase):
    def test_add_illumina(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        qc_platforms = ["illumina"]
        test_platforms = snake_helpers.get_qc_platforms(illumina_samples, nanopore_samples,
                                                        hybrid_samples)
        assert qc_platforms == test_platforms

    def test_add_nanopore(self):
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        hybrid_samples = []
        qc_platforms = ["nanopore"]
        test_platforms = snake_helpers.get_qc_platforms(illumina_samples, nanopore_samples,
                                                        hybrid_samples)
        assert qc_platforms == test_platforms

    def test_add_hybrid(self):
        hybrid_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_samples = []
        qc_platforms = ["hybrid"]
        test_platforms = snake_helpers.get_qc_platforms(illumina_samples, nanopore_samples,
                                                        hybrid_samples)
        assert qc_platforms == test_platforms

    def test_add_multiple(self):
        illumina_samples = ["F99123456-1"]
        nanopore_samples = ["F99654321-2"]
        hybrid_samples = []
        qc_platforms = ["illumina", "nanopore"]
        test_platforms = snake_helpers.get_qc_platforms(illumina_samples, nanopore_samples,
                                                        hybrid_samples)
        assert qc_platforms == test_platforms

    def test_fail_no_samples(self):
        illumina_samples = []
        nanopore_samples = []
        hybrid_samples = []
        with pytest.raises(ValueError,
                           match = re.escape("No Illumina, Nanopore or hybrid samples given.")):
            snake_helpers.get_qc_platforms(illumina_samples, nanopore_samples, hybrid_samples)


class TestIsGzipped(unittest.TestCase):
    def test_get_fastq(self):
        barcode_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir" \
                      / "rawdata" / "subdir" / "fastq_pass" / "barcode01"
        sample_overview = pd.DataFrame(data={"sample": ["F99123456-1"],
                                             "extra": [barcode_dir]})
        sample = "F99123456-1"
        assert not snake_helpers.is_gzipped(sample, sample_overview)

    def test_get_gzipped(self):
        barcode_dir = pathlib.Path(__file__).parent / "data" / "snake_helpers" \
                      / "rawdata" / "subdir" / "fastq_pass" / "barcode01"
        sample_overview = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                               "extra": [barcode_dir]})
        sample = "F99123456-1"
        assert snake_helpers.is_gzipped(sample, sample_overview)

    def test_fail_mixed(self):
        barcode_dir = pathlib.Path(__file__).parent / "data" / "snake_helpers" \
                      / "rawdata" / "subdir" / "fastq_pass" / "barcode42"
        sample_overview = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                               "extra": [barcode_dir]})
        sample = "F99123456-1"
        error_msg = "Extensions ['.fastq', '.gz'] not supported."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            snake_helpers.is_gzipped(sample, sample_overview)


class TestGetAssemblyName(unittest.TestCase):
    def test_illumina_only(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        illumina_assembly = "F99123456-1/assembly/F99123456-1.fna"
        sample = "F99123456-1"
        test_assembly = snake_helpers.get_assembly_name(sample, illumina_samples,
                                                        nanopore_samples,
                                                        hybrid_samples)
        assert illumina_assembly == test_assembly

    def test_nanopore_only(self):
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        hybrid_samples = []
        nanopore_assembly = "F99123456-1/nanopore_assembly/F99123456-1.fasta"
        sample = "F99123456-1"
        test_assembly = snake_helpers.get_assembly_name(sample, illumina_samples,
                                                        nanopore_samples,
                                                        hybrid_samples)
        assert nanopore_assembly == test_assembly

    def test_hybrid_samples(self):
        hybrid_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_samples = []
        hybrid_assembly = "F99123456-1/hybrid_assembly/F99123456-1.fasta"
        sample = "F99123456-1"
        test_assembly = snake_helpers.get_assembly_name(sample, illumina_samples,
                                                        nanopore_samples,
                                                        hybrid_samples)

        assert hybrid_assembly == test_assembly

    def test_add_experiment_name(self):
        experiment_name = "ILM_RUN0000_TEST"
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        illumina_assembly = "F99123456-1/assembly/ILM_RUN0000_TEST_F99123456-1.fna"
        sample = "F99123456-1"
        test_assembly = snake_helpers.get_assembly_name(sample, illumina_samples,
                                                        nanopore_samples,
                                                        hybrid_samples,
                                                        experiment_name = experiment_name)
        assert illumina_assembly == test_assembly

    def test_fail_missing_sample(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        sample = "F99123456-2"
        error_msg = "Sample F99123456-2 not found."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            snake_helpers.get_assembly_name(sample, illumina_samples,
                                            nanopore_samples,
                                            hybrid_samples)


class TestReads(unittest.TestCase):
    def test_illumina_only(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        illumina_reads = ["F99123456-1/quality-control/F99123456-1_R1.fastq.gz",
                          "F99123456-1/quality-control/F99123456-1_R2.fastq.gz"]
        sample = "F99123456-1"
        test_reads = snake_helpers.get_reads(sample, illumina_samples,
                                             nanopore_samples,
                                             hybrid_samples)
        assert illumina_reads == test_reads

    def test_nanopore_only(self):
        nanopore_samples = ["F99123456-1", "F99654321-2"]
        illumina_samples = []
        hybrid_samples = []
        nanopore_reads = ["F99123456-1/nanopore_reads/F99123456-1.downsample.fastq"]
        sample = "F99123456-1"
        test_reads = snake_helpers.get_reads(sample, illumina_samples,
                                             nanopore_samples,
                                             hybrid_samples)
        print(test_reads)
        assert nanopore_reads == test_reads

    def test_hybrid_samples(self):
        hybrid_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        illumina_samples = []
        illumina_reads = ["F99123456-1/quality-control/F99123456-1_R1.fastq.gz",
                          "F99123456-1/quality-control/F99123456-1_R2.fastq.gz"]
        sample = "F99123456-1"
        test_reads = snake_helpers.get_reads(sample, illumina_samples,
                                             nanopore_samples,
                                             hybrid_samples)
        assert illumina_reads == test_reads

    def test_fail_missing_sample(self):
        illumina_samples = ["F99123456-1", "F99654321-2"]
        nanopore_samples = []
        hybrid_samples = []
        sample = "F99123456-2"
        error_msg = "Sample F99123456-2 not found."
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            snake_helpers.get_reads(sample, illumina_samples,
                                    nanopore_samples,
                                    hybrid_samples)


class TestGetPointmutSpecies(unittest.TestCase):
    lis_report = pd.DataFrame(data={'proevenr': ["F99123456-1",
                                                 "F99123456-2",
                                                 "F99123456-3",
                                                 "F99123456-4",
                                                 "F99123456-5",
                                                 "F99123456-6"],
                                    'LOKAL_BAKT_KODE': ["1", "2", "3", "4", "5", "6"]})
    species_coding = pd.DataFrame(data = {'bakterie_kategori': ["Escherichia coli",
                                                                "Salmonella enterica",
                                                                "Staphylococcus aureus",
                                                                "Klebsiella oxytoca",
                                                                "Placeholderia fakeorum"],
                                          'BAKTERIE': ["1", "2", "3", "4", "5"]})
    lis_names = input_names.LISDataNames(sample_number = "PRV_NR", sample_type = "P_TYPE",
                                                   isolate_number = "BAKT_NR",
                                                   bacteria_code = "LOKAL_BAKT_KODE",
                                                   bacteria_text = "LOKAL_BAKT_TEKST")
    bact_names =  input_names.BacteriaMappingNames(bacteria_code = "BAKTERIE",
                                                           internal_bacteria_text = "BAKTERIENAVN",
                                                           bacteria_category = "bakterie_kategori")

    def test_fail_no_names(self):
        """Fail if LIS and species coding have been given but column names haven't."""
        error_msg = ("LIS and bacteria mapping column names have to be specified when using LIS "
                     "data.")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            snake_helpers.get_abritamr_organism("F99123456-1", self.lis_report,
                                                            self.species_coding)

    def test_find_ecoli(self):
        expected_flag = "--species Escherichia"
        test_flag = snake_helpers.get_abritamr_organism("F99123456-1", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag

    def test_find_ecoli_translate(self):
        """Flag Escherichia sample correctly when using non-default column names."""
        lis_names_en = input_names.LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                                                isolate_number = "BACT_NR",
                                                bacteria_code = "LOCAL_BACT_CODE",
                                                bacteria_text = "LOCAL_BACT_TEXT")
        bact_names_en = input_names.BacteriaMappingNames(bacteria_code = "bacteria_code",
                                                         internal_bacteria_text = "bacteria_text",
                                                         bacteria_category = "bacteria_category")
        lis_report = pd.DataFrame(data = {'proevenr': ["F99123456-1",
                                                       "F99123456-2",
                                                       "F99123456-3",
                                                       "F99123456-4",
                                                       "F99123456-5",
                                                       "F99123456-6"],
                                          'LOCAL_BACT_CODE': ["1", "2", "3", "4", "5", "6"]})
        species_coding = pd.DataFrame(data = {'bacteria_category': ["Escherichia coli",
                                                                    "Salmonella enterica",
                                                                    "Staphylococcus aureus",
                                                                    "Klebsiella oxytoca",
                                                                    "Placeholderia fakeorum"],
                                              'bacteria_code': ["1", "2", "3", "4", "5"]})
        expected_flag = "--species Escherichia"
        test_flag = snake_helpers.get_abritamr_organism("F99123456-1", lis_report,
                                                        species_coding,
                                                        lis_data_names = lis_names_en,
                                                        bacteria_names = bact_names_en)
        assert expected_flag == test_flag

    def test_find_salmo(self):
        expected_flag = "--species Salmonella"
        test_flag = snake_helpers.get_abritamr_organism("F99123456-2", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag

    def test_find_staph(self):
        expected_flag = "--species Staphylococcus_aureus"
        test_flag = snake_helpers.get_abritamr_organism("F99123456-3", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag

    def test_find_klebsi(self):
        expected_flag = "--species Klebsiella"
        test_flag = snake_helpers.get_abritamr_organism("F99123456-4", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag

    def test_no_pointmut(self):
        expected_flag = ""
        test_flag = snake_helpers.get_abritamr_organism("F99123456-5", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag

    def test_no_lis_data(self):
        expected_flag = ""
        test_flag = snake_helpers.get_abritamr_organism("F99123456-5")
        assert expected_flag == test_flag

    def test_fail_no_categories(self):
        error_msg = "When using a LIS report, a translation of the local bacteria codes " \
                             "to species names is required."
        with pytest.raises(ValueError, match=error_msg):
            snake_helpers.get_abritamr_organism("F99123456-5", self.lis_report,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)

    def test_handle_blank_species(self):
        expected_flag = ""
        test_flag = snake_helpers.get_abritamr_organism("F99123456-6", self.lis_report,
                                                        self.species_coding,
                                                        lis_data_names = self.lis_names,
                                                        bacteria_names = self.bact_names)
        assert expected_flag == test_flag


class TestGetSerotyping(unittest.TestCase):
    def setUp(self) -> None:
        self.ecoli_samples = ["F99123456-1"]
        self.salmo_samples = ["F99654321-2"]

    def test_find_ecoli(self):
        sample = "F99123456-1"
        expected_report = "F99123456-1/typing/serotypefinder/results_tab.tsv"
        test_report = snake_helpers.get_serotyping_report(sample, self.ecoli_samples,
                                                          self.salmo_samples)
        assert test_report == expected_report

    def test_find_salmo(self):
        sample = "F99654321-2"
        expected_report = "F99654321-2/typing/SeqSero/SeqSero_result.tsv"
        test_report = snake_helpers.get_serotyping_report(sample, self.ecoli_samples,
                                                          self.salmo_samples)
        assert test_report == expected_report

    def test_no_report(self):
        sample = "F99654321-3"
        test_report = snake_helpers.get_serotyping_report(sample, self.ecoli_samples,
                                                          self.salmo_samples)
        assert not test_report


class TestGetToxinIndication(unittest.TestCase):
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_no_toxins(self):
        """Don't return anything if there's no indication that has anything to do with toxins."""
        log_msg = "No indication for toxin gene identification found."
        indications = ["AMR", "HAI"]
        with self._caplog.at_level(level = "INFO", logger = "snake_helpers"):
            toxin_cols = snake_helpers.infer_toxin_column(indications)
            assert ("snake_helpers", logging.INFO, log_msg) in self._caplog.record_tuples
        assert not toxin_cols

    def test_one_toxin(self):
        """Find a single column containing the word 'toxin'."""
        log_msg = ("Samples with indication(s) ['TOXINGENES'] will be screened"
                   " for toxin genes.")
        indications = ["AMR", "HAI", "TOXINGENES"]
        expected_toxins = ["TOXINGENES"]
        with self._caplog.at_level(level = "INFO", logger = "snake_helpers"):
            toxin_cols = snake_helpers.infer_toxin_column(indications)
            assert ("snake_helpers", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_toxins == toxin_cols

    def test_many_toxins(self):
        """Find multiple columns containing the word 'toxin'."""
        log_msg = ("Samples with indication(s) ['TOXINGENES', 'toxin_gene_id']"
                   " will be screened for toxin genes.")
        indications = ["AMR", "toxin_gene_id", "HAI", "TOXINGENES"]
        expected_toxins = ["toxin_gene_id", "TOXINGENES"]
        with self._caplog.at_level(level = "INFO", logger = "snake_helpers"):
            toxin_cols = snake_helpers.infer_toxin_column(indications)
            assert ("snake_helpers", logging.INFO, log_msg) in self._caplog.record_tuples
        assert expected_toxins == toxin_cols


class TestGetToxinSamples(unittest.TestCase):
    def test_no_toxin_indication(self):
        """Handle a setup with no indication for toxin gene identification."""
        sheet_names = input_names.RunsheetNames(sample_info_sheet = "Sample_information",
                                                sample_number = "Sample_number",
                                                extraction_well_number = "Extraction_well_number",
                                                extraction_run_number = "Extraction_run_number",
                                                dna_concentration = "DNA_concentration",
                                                requested_by = "Requested_by",
                                                comment = "Comment",
                                                sequencing_run_number = "sequencing_run_number",
                                                indications = ["AMR", "Identification", "Relapse",
                                                               "HAI",
                                                               "Environmental sample isolate",
                                                               "Outbreak sample isolate",
                                                               "Surveillance sample isolate",
                                                               "Research",
                                                               "Other"],
                                                dna_normalization_sheet = "DNA normalization",
                                                dna_normalization_sample_nr = "Sample_number",
                                                ilm_well_number = "ILM_well_position",
                                                plate_layout = "Plate_layout",
                                                runsheet_base = "Runsheet",
                                                experiment_name = "RUNxxxx-INI",
                                                nanopore_sheet_sample_number = "Sample_number",
                                                barcode = "Barcode",
                                                protocol_version = "Lab protocol version")
        runsheet = pd.DataFrame(data={"sample_number": ["1199123456-1", "1199123456-2",
                                                        "1199123456-3"],
                                      "AMR": [True, False, False],
                                      "HAI": [False, True, True]})
        expected_samples = []
        test_samples = snake_helpers.get_toxin_samples(runsheet, sheet_names)
        assert expected_samples == test_samples

    def test_no_toxin_samples(self):
        """Handle the case where no samples should be screened for toxin genes."""
        sheet_names = input_names.RunsheetNames(sample_info_sheet = "Sample_information",
                                                sample_number = "Sample_number",
                                                extraction_well_number = "Extraction_well_number",
                                                extraction_run_number = "Extraction_run_number",
                                                dna_concentration = "DNA_concentration",
                                                requested_by = "Requested_by",
                                                comment = "Comment",
                                                sequencing_run_number = "sequencing_run_number",
                                                indications = ["AMR", "Identification", "Relapse",
                                                               "HAI",
                                                               "Toxin gene identification",
                                                               "Environmental sample isolate",
                                                               "Outbreak sample isolate",
                                                               "Surveillance sample isolate",
                                                               "Research",
                                                               "Other"],
                                                dna_normalization_sheet = "DNA normalization",
                                                dna_normalization_sample_nr = "Sample_number",
                                                ilm_well_number = "ILM_well_position",
                                                plate_layout = "Plate_layout",
                                                runsheet_base = "Runsheet",
                                                experiment_name = "RUNxxxx-INI",
                                                nanopore_sheet_sample_number = "Sample_number",
                                                barcode = "Barcode",
                                                protocol_version = "Lab protocol version")
        runsheet = pd.DataFrame(data = {"Sample_number": ["1199123456-1", "1199123456-2",
                                                          "1199123456-3"],
                                        "AMR": [True, False, False],
                                        "HAI": [False, True, True],
                                        "Toxin gene identification": [False, False, False]})
        expected_samples = []
        test_samples = snake_helpers.get_toxin_samples(runsheet, sheet_names)
        assert expected_samples == test_samples

    def test_one_toxin_indication(self):
        """Extract samples which should be screened for toxin genes when
        only one indication for this exists."""
        sheet_names = input_names.RunsheetNames(sample_info_sheet = "Sample_information",
                                                sample_number = "Sample_number",
                                                extraction_well_number = "Extraction_well_number",
                                                extraction_run_number = "Extraction_run_number",
                                                dna_concentration = "DNA_concentration",
                                                requested_by = "Requested_by",
                                                comment = "Comment",
                                                sequencing_run_number = "sequencing_run_number",
                                                indications = ["AMR", "Identification", "Relapse",
                                                               "HAI",
                                                               "Toxin gene identification",
                                                               "Environmental sample isolate",
                                                               "Outbreak sample isolate",
                                                               "Surveillance sample isolate",
                                                               "Research",
                                                               "Other"],
                                                dna_normalization_sheet = "DNA normalization",
                                                dna_normalization_sample_nr = "Sample_number",
                                                ilm_well_number = "ILM_well_position",
                                                plate_layout = "Plate_layout",
                                                runsheet_base = "Runsheet",
                                                experiment_name = "RUNxxxx-INI",
                                                nanopore_sheet_sample_number = "Sample_number",
                                                barcode = "Barcode",
                                                protocol_version = "Lab protocol version")
        runsheet = pd.DataFrame(data = {"Sample_number": ["1199123456-1", "1199123456-2",
                                                          "1199123456-3"],
                                        "AMR": [True, False, False],
                                        "HAI": [False, True, True],
                                        "Toxin gene identification": [False, True, True]})
        expected_samples = ["1199123456-2", "1199123456-3"]
        test_samples = snake_helpers.get_toxin_samples(runsheet, sheet_names)
        assert expected_samples == test_samples

    def test_multiple_toxin_indications(self):
        """Extract samples which should be screened for toxins when
        multiple indications for this exist."""
        sheet_names = input_names.RunsheetNames(sample_info_sheet = "Sample_information",
                                                sample_number = "Sample_number",
                                                extraction_well_number = "Extraction_well_number",
                                                extraction_run_number = "Extraction_run_number",
                                                dna_concentration = "DNA_concentration",
                                                requested_by = "Requested_by",
                                                comment = "Comment",
                                                sequencing_run_number = "sequencing_run_number",
                                                indications = ["AMR", "Identification", "Relapse",
                                                               "HAI",
                                                               "Toxin gene identification",
                                                               "TOXINS",
                                                               "Environmental sample isolate",
                                                               "Outbreak sample isolate",
                                                               "Surveillance sample isolate",
                                                               "Research",
                                                               "Other"],
                                                dna_normalization_sheet = "DNA normalization",
                                                dna_normalization_sample_nr = "Sample_number",
                                                ilm_well_number = "ILM_well_position",
                                                plate_layout = "Plate_layout",
                                                runsheet_base = "Runsheet",
                                                experiment_name = "RUNxxxx-INI",
                                                nanopore_sheet_sample_number = "Sample_number",
                                                barcode = "Barcode",
                                                protocol_version = "Lab protocol version")
        runsheet = pd.DataFrame(data = {"Sample_number": ["1199123456-1", "1199123456-2",
                                                          "1199123456-3"],
                                        "AMR": [True, False, False],
                                        "HAI": [False, True, True],
                                        "Toxin gene identification": [False, True, False],
                                        "TOXINS": [False, False, True]})
        expected_samples = ["1199123456-2", "1199123456-3"]
        test_samples = snake_helpers.get_toxin_samples(runsheet, sheet_names)
        assert expected_samples == test_samples
