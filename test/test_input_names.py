import re
import unittest

import pytest

import input_names


class TestGetRunsheetNames(unittest.TestCase):
    def test_fail_missing_names(self):
        """Complain if there are blank column or sheet names."""
        error_msg = ("Column/sheet names cannot be blank. Please add name(s) for ['barcode',"
                     " 'sample_number'] in the metadata_sheet section of your input settings.")
        fail_config = {"sample_info_sheet": "Prøveoplysninger",
                       "sample_number": "",
                       "extraction_well_number": "Brønd_nr",
                       "extraction_run_number": "Oprensnings_kørselsnr",
                       "dna_concentration": "DNA_konc",
                       "requested_by": "Bestilt af",
                       "comment": "Kommentar",
                       "sequencing_run_number": "SEK_Run_nr",
                       "indications": ["Resistens", "Identifikation", "Relaps", "Toxingen_ID",
                                       "HAI",
                                       "Miljøprøveisolat", "Udbrudsisolat", "Overvågningsisolat",
                                       "Forskning",
                                       "Andet"],  # list of possible indication columns
                       # DNA normalization sheet name
                       "dna_normalization_sheet": "DNA normalisering",
                       "dna_normalization_sample_nr": "Prøvenr",
                       # sample number in DNA normalization sheet
                       "ilm_well_number": "ILM Brønd position",
                       # well number for Illumina library prep
                       # plate layout sheet name
                       "plate_layout": "Pladelayout",
                       # base name of sheet with runsheet for methods - suffixed with _nanopore, ...
                       "runsheet_base": "Runsheet",
                       # specific column/cell names in the Nanopore runsheet
                       "experiment_name": "RUNxxxx-INI",
                       "nanopore_sheet_sample_number": "KMA_nr",
                       "barcode": "",
                       "protocol_version": "Protokol (versionsnummer)"}
        with pytest.raises(KeyError, match=re.escape(error_msg)):
            input_names.set_up_runsheet_names(fail_config)

    def test_success(self):
        """Successfully read the settings"""
        test_config = {"sample_info_sheet": "Prøveoplysninger",
                       "sample_number": "Prøvenr",
                       "extraction_well_number": "Brønd_nr",
                       "extraction_run_number": "Oprensnings_kørselsnr",
                       "dna_concentration": "DNA_konc",
                       "requested_by": "Bestilt af",
                       "comment": "Kommentar",
                       "sequencing_run_number": "SEK_Run_nr",
                       "indications": ["Resistens", "Identifikation", "Relaps", "Toxingen_ID",
                                       "HAI",
                                       "Miljøprøveisolat", "Udbrudsisolat", "Overvågningsisolat",
                                       "Forskning",
                                       "Andet"],  # list of possible indication columns
                       # DNA normalization sheet name
                       "dna_normalization_sheet": "DNA normalisering",
                       "dna_normalization_sample_nr": "Prøvenr",
                       # sample number in DNA normalization sheet
                       "ilm_well_number": "ILM Brønd position",
                       # well number for Illumina library prep
                       # plate layout sheet name
                       "plate_layout": "Pladelayout",
                       # base name of sheet with runsheet for methods - suffixed with _nanopore, ...
                       "runsheet_base": "Runsheet",
                       # specific column/cell names in the Nanopore runsheet
                       "experiment_name": "RUNxxxx-INI",
                       "nanopore_sheet_sample_number": "KMA_nr",
                       "barcode": "Barkode",
                       "protocol_version": "Protokol (versionsnummer)"}
        expected_result = input_names.RunsheetNames(sample_info_sheet="Prøveoplysninger",
                                                    sample_number = "Prøvenr",
                                                    extraction_well_number = "Brønd_nr",
                                                    extraction_run_number = "Oprensnings_kørselsnr",
                                                    dna_concentration = "DNA_konc",
                                                    requested_by = "Bestilt af",
                                                    comment = "Kommentar",
                                                    sequencing_run_number = "SEK_Run_nr",
                                                    indications = ["Resistens", "Identifikation",
                                                                   "Relaps", "Toxingen_ID",
                                                                   "HAI", "Miljøprøveisolat",
                                                                   "Udbrudsisolat",
                                                                   "Overvågningsisolat",
                                                                   "Forskning", "Andet"],
                                                    dna_normalization_sheet = "DNA normalisering",
                                                    dna_normalization_sample_nr = "Prøvenr",
                                                    ilm_well_number = "ILM Brønd position",
                                                    plate_layout = "Pladelayout",
                                                    runsheet_base = "Runsheet",
                                                    experiment_name = "RUNxxxx-INI",
                                                    nanopore_sheet_sample_number = "KMA_nr",
                                                    barcode = "Barkode",
                                                    protocol_version = "Protokol (versionsnummer)")
        test_result = input_names.set_up_runsheet_names(test_config)
        assert test_result == expected_result

    def test_success_handle_numbers(self):
        """Handle numbers instead of names for columns - force everything to strings."""
        test_config = {"sample_info_sheet": "Prøveoplysninger",
                       "sample_number": 1,
                       "extraction_well_number": "Brønd_nr",
                       "extraction_run_number": "Oprensnings_kørselsnr",
                       "dna_concentration": "DNA_konc",
                       "requested_by": "Bestilt af",
                       "comment": "Kommentar",
                       "sequencing_run_number": "SEK_Run_nr",
                       "indications": ["Resistens", "Identifikation", "Relaps", "Toxingen_ID",
                                       "HAI",
                                       "Miljøprøveisolat", "Udbrudsisolat", "Overvågningsisolat",
                                       "Forskning",
                                       "Andet"],  # list of possible indication columns
                       # DNA normalization sheet name
                       "dna_normalization_sheet": "DNA normalisering",
                       "dna_normalization_sample_nr": "Prøvenr",
                       # sample number in DNA normalization sheet
                       "ilm_well_number": "ILM Brønd position",
                       # well number for Illumina library prep
                       # plate layout sheet name
                       "plate_layout": "Pladelayout",
                       # base name of sheet with runsheet for methods - suffixed with _nanopore, ...
                       "runsheet_base": "Runsheet",
                       # specific column/cell names in the Nanopore runsheet
                       "experiment_name": "RUNxxxx-INI",
                       "nanopore_sheet_sample_number": "KMA_nr",
                       "barcode": "Barkode",
                       "protocol_version": "Protokol (versionsnummer)"}
        expected_result = input_names.RunsheetNames(sample_info_sheet = "Prøveoplysninger",
                                                    sample_number = "1",
                                                    extraction_well_number = "Brønd_nr",
                                                    extraction_run_number = "Oprensnings_kørselsnr",
                                                    dna_concentration = "DNA_konc",
                                                    requested_by = "Bestilt af",
                                                    comment = "Kommentar",
                                                    sequencing_run_number = "SEK_Run_nr",
                                                    indications = ["Resistens", "Identifikation",
                                                                   "Relaps", "Toxingen_ID",
                                                                   "HAI", "Miljøprøveisolat",
                                                                   "Udbrudsisolat",
                                                                   "Overvågningsisolat",
                                                                   "Forskning", "Andet"],
                                                    dna_normalization_sheet = "DNA normalisering",
                                                    dna_normalization_sample_nr = "Prøvenr",
                                                    ilm_well_number = "ILM Brønd position",
                                                    plate_layout = "Pladelayout",
                                                    runsheet_base = "Runsheet",
                                                    experiment_name = "RUNxxxx-INI",
                                                    nanopore_sheet_sample_number = "KMA_nr",
                                                    barcode = "Barkode",
                                                    protocol_version = "Protokol (versionsnummer)")
        test_result = input_names.set_up_runsheet_names(test_config)
        assert test_result == expected_result


class TestSetupLISData(unittest.TestCase):
    def test_fail_missing_names(self):
        """Fail if column names are missing."""
        fail_msg = ("Column names cannot be blank. Please add name(s) for ['sample_number',"
                    " 'sample_type'] in the lis_columns section of your input settings.")
        fail_config = {"sample_number": "",
                       # sample type (as part of unique sample identifier)
                       "sample_type": "",
                       # isolate number (as part of unique isolate identifier)
                       "isolate_number": "BAKT_NR",
                       # numeric code for bacteria identified in the sample
                       "bacteria_code": "LOKAL_BAKT_KODE",
                       # bacteria identified in the sample in text form
                       "bacteria_text": "LOKAL_BAKT_TEKST"}
        with pytest.raises(KeyError, match=re.escape(fail_msg)):
            input_names.set_up_lis_names(fail_config)

    def test_success(self):
        """Successfully read the config."""
        test_config = {"sample_number": "PRV_NR",
                       # sample type (as part of unique sample identifier)
                       "sample_type": "P_TYPE",
                       # isolate number (as part of unique isolate identifier)
                       "isolate_number": "BAKT_NR",
                       # numeric code for bacteria identified in the sample
                       "bacteria_code": "LOKAL_BAKT_KODE",
                       # bacteria identified in the sample in text form
                       "bacteria_text": "LOKAL_BAKT_TEKST"}
        expected_result = input_names.LISDataNames(sample_number = "PRV_NR", sample_type = "P_TYPE",
                                                   isolate_number = "BAKT_NR",
                                                   bacteria_code = "LOKAL_BAKT_KODE",
                                                   bacteria_text = "LOKAL_BAKT_TEKST")
        test_result = input_names.set_up_lis_names(test_config)
        assert test_result == expected_result

    def test_success_numbers(self):
        """Handle columns having numeric names."""
        test_config = {"sample_number": 1,
                       # sample type (as part of unique sample identifier)
                       "sample_type": "P_TYPE",
                       # isolate number (as part of unique isolate identifier)
                       "isolate_number": "BAKT_NR",
                       # numeric code for bacteria identified in the sample
                       "bacteria_code": "LOKAL_BAKT_KODE",
                       # bacteria identified in the sample in text form
                       "bacteria_text": "LOKAL_BAKT_TEKST"}
        expected_result = input_names.LISDataNames(sample_number = "1", sample_type = "P_TYPE",
                                                   isolate_number = "BAKT_NR",
                                                   bacteria_code = "LOKAL_BAKT_KODE",
                                                   bacteria_text = "LOKAL_BAKT_TEKST")
        test_result = input_names.set_up_lis_names(test_config)
        assert test_result == expected_result


class TestSetupBactMapping(unittest.TestCase):
    def test_fail_missing_names(self):
        """Fail if column names are missing."""
        fail_msg = ("Column names cannot be blank. Please add name(s) for ['bacteria_code'] "
                    "in the bact_translation section of your input settings.")
        fail_config = {"bacteria_code": "", "bacteria_category": "bakterie_kategori"}
        with pytest.raises(KeyError, match=re.escape(fail_msg)):
            input_names.set_up_bact_names(fail_config)

    def test_success(self):
        """Successfully read the config and set up BacteriaMappingNames."""
        test_config = {"bacteria_code": "BAKTERIE",
                       "internal_bacteria_text": "BAKTERIENAVN",
                       "bacteria_category": "bakterie_kategori"}
        expected_result = input_names.BacteriaMappingNames(bacteria_code = "BAKTERIE",
                                                           internal_bacteria_text = "BAKTERIENAVN",
                                                           bacteria_category = "bakterie_kategori")
        test_result = input_names.set_up_bact_names(test_config)
        assert test_result == expected_result

    def test_success_numbers(self):
        """Handle columns having numeric names."""
        test_config = {"bacteria_code": 1,
                       "internal_bacteria_text": "BAKTERIENAVN",
                       "bacteria_category": "bakterie_kategori"}
        expected_result = input_names.BacteriaMappingNames(bacteria_code = "1",
                                                           internal_bacteria_text = "BAKTERIENAVN",
                                                           bacteria_category = "bakterie_kategori")
        test_result = input_names.set_up_bact_names(test_config)
        assert test_result == expected_result
