import logging
import pathlib
import re
import unittest

from pathlib import Path
from unittest import mock

import pytest

from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames
import localization_helpers
import set_log

root_logger = set_log.get_stream_log(level="DEBUG")


class TestGetSupportedLanguages(unittest.TestCase):
    @mock.patch(f'{localization_helpers.__name__}.pathlib.Path.glob')
    def test_fail_no_languages(self, mock_glob):
        """Fail if no languages are found in the locales directory."""
        mock_glob.return_value = []
        error_msg = "No languages found in locales directory."
        with pytest.raises(FileNotFoundError, match=re.escape(error_msg)):
            localization_helpers.get_supported_languages()

    def test_success_find_languages(self):
        """Find the languages for which translations exist."""
        true_supported = {'da', 'en'}
        test_supported = localization_helpers.get_supported_languages()
        assert true_supported == test_supported


class TestSetLanguage(unittest.TestCase):
    def test_fail_unsupported_language(self):
        """Fail if the target language is not supported."""
        error_msg = "tlh is not a supported language. Supported languages are ['da', 'en']."
        with pytest.raises(NotImplementedError, match = re.escape(error_msg)):
            localization_helpers.set_language('tlh')

    def test_success_set_language(self):
        """Successfully set a supported language."""
        _ = localization_helpers.set_language('en')
        expected_translation = ("Over 20% difference between expected genome size from "
                                "LIS and genome size found. ")
        test_translation = _("Over 20% forskel mellem forventet genomstørrelse fra MADS "
                             "og fundet genomstørrelse. ")
        assert expected_translation == test_translation


class TestLoadInputConfig(unittest.TestCase):
    def test_fail_missing_file(self):
        """Fail if the input config file is missing."""
        fail_file = Path(__file__).parent / 'data' / 'localization' / 'no_such_config.yaml'
        error_msg = f"Input config file {fail_file} does not exist."
        with pytest.raises(FileNotFoundError, match=re.escape(error_msg)):
            localization_helpers.load_input_settings(fail_file)

    def test_success_load_input_config(self):
        """Successfully load the different config sections."""
        input_file = Path(__file__).parent / 'data' / 'localization' / 'input_en.yaml'
        sheet_names_en = RunsheetNames(sample_info_sheet = "Sample_information",
                                       sample_number = "Sample_number",
                                       extraction_well_number = "Extraction_well_number",
                                       extraction_run_number = "Extraction_run_number",
                                       dna_concentration = "DNA_concentration",
                                       requested_by = "Requested_by",
                                       comment = "Comment",
                                       sequencing_run_number = "Sequencing_run_number",
                                       indications = ["AMR", "Identification", "Relapse", "HAI",
                                                      "Toxin gene identification",
                                                      "Environmental sample isolate",
                                                      "Outbreak sample isolate",
                                                      "Surveillance sample isolate", "Research",
                                                      "Other"],
                                       dna_normalization_sheet = "DNA normalization",
                                       dna_normalization_sample_nr = "Sample_number",
                                       ilm_well_number = "ILM_well_position",
                                       plate_layout = "Plate_layout",
                                       runsheet_base = "Runsheet",
                                       experiment_name = "RUNxxxx-INI",
                                       nanopore_sheet_sample_number = "Sample_number",
                                       barcode = "Barcode",
                                       protocol_version = "Lab protocol version")
        lis_names_en = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                                    isolate_number = "BACT_NR",
                                    bacteria_code = "LOCAL_BACT_CODE",
                                    bacteria_text = "LOCAL_BACT_TEXT")
        bact_names_en = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                             internal_bacteria_text = "bacteria_text",
                                             bacteria_category = "bacteria_category")
        (sheet_names_test,
         lis_names_test, bact_names_test) = localization_helpers.load_input_settings(input_file)
        assert sheet_names_en == sheet_names_test
        assert lis_names_en == lis_names_test
        assert bact_names_en == bact_names_test


class TestLoadFromConfig(unittest.TestCase):
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_load_from_abs_path(self):
        """Load input settings from an absolute path."""
        test_config = {"input_names": pathlib.Path(__file__).parent / "data"
                                         / "localization" / "input_en.yaml"}
        log_msg = f"Input settings loaded from {test_config['input_names']}"
        sheet_names_en = RunsheetNames(sample_info_sheet = "Sample_information",
                                       sample_number = "Sample_number",
                                       extraction_well_number = "Extraction_well_number",
                                       extraction_run_number = "Extraction_run_number",
                                       dna_concentration = "DNA_concentration",
                                       requested_by = "Requested_by",
                                       comment = "Comment",
                                       sequencing_run_number = "Sequencing_run_number",
                                       indications = ["AMR", "Identification", "Relapse", "HAI",
                                                      "Toxin gene identification",
                                                      "Environmental sample isolate",
                                                      "Outbreak sample isolate",
                                                      "Surveillance sample isolate", "Research",
                                                      "Other"],
                                       dna_normalization_sheet = "DNA normalization",
                                       dna_normalization_sample_nr = "Sample_number",
                                       ilm_well_number = "ILM_well_position",
                                       plate_layout = "Plate_layout",
                                       runsheet_base = "Runsheet",
                                       experiment_name = "RUNxxxx-INI",
                                       nanopore_sheet_sample_number = "Sample_number",
                                       barcode = "Barcode",
                                       protocol_version = "Lab protocol version")
        lis_names_en = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                                    isolate_number = "BACT_NR",
                                    bacteria_code = "LOCAL_BACT_CODE",
                                    bacteria_text = "LOCAL_BACT_TEXT")
        bact_names_en = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                             internal_bacteria_text = "bacteria_text",
                                             bacteria_category = "bacteria_category")
        with self._caplog.at_level(logger="localization_helpers", level=logging.DEBUG):
            (sheet_names_test,
             lis_names_test,
             bact_names_test) = localization_helpers.load_input_from_config(test_config)
            assert ("localization_helpers", logging.DEBUG, log_msg) in self._caplog.record_tuples
        assert sheet_names_en == sheet_names_test
        assert lis_names_en == lis_names_test
        assert bact_names_en == bact_names_test

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_load_from_relative_path(self):
        """Load input settings from a relative path."""
        test_config = {"input_names": "config/input_settings/input_en.yaml"}
        expected_path = (pathlib.Path(__file__).parent.parent / "config" / "input_settings"
                         / "input_en.yaml")
        log_msg = f"Input settings loaded from {expected_path}"
        sheet_names_en = RunsheetNames(sample_info_sheet = "Sample_information",
                                       sample_number = "Sample_number",
                                       extraction_well_number = "Extraction_well_number",
                                       extraction_run_number = "Extraction_run_number",
                                       dna_concentration = "DNA_concentration",
                                       requested_by = "Requested_by",
                                       comment = "Comment",
                                       sequencing_run_number = "Sequencing_run_number",
                                       indications = ["AMR", "Identification", "Relapse", "HAI",
                                                      "Toxin gene identification",
                                                      "Environmental sample isolate",
                                                      "Outbreak sample isolate",
                                                      "Surveillance sample isolate", "Research",
                                                      "Other"],
                                       dna_normalization_sheet = "DNA normalization",
                                       dna_normalization_sample_nr = "Sample_number",
                                       ilm_well_number = "ILM_well_position",
                                       plate_layout = "Plate_layout",
                                       runsheet_base = "Runsheet",
                                       experiment_name = "RUNxxxx-INI",
                                       nanopore_sheet_sample_number = "Sample_number",
                                       barcode = "Barcode",
                                       protocol_version = "Lab protocol version")
        lis_names_en = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLETYPE",
                                    isolate_number = "BACT_NR",
                                    bacteria_code = "LOCAL_BACT_CODE",
                                    bacteria_text = "LOCAL_BACT_TEXT")
        bact_names_en = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                             internal_bacteria_text = "bacteria_text",
                                             bacteria_category = "bacteria_category")
        with self._caplog.at_level(logger = "localization_helpers", level = logging.DEBUG):
            (sheet_names_test,
             lis_names_test,
             bact_names_test) = localization_helpers.load_input_from_config(test_config)
            assert ("localization_helpers", logging.DEBUG, log_msg) in self._caplog.record_tuples
        assert sheet_names_en == sheet_names_test
        assert lis_names_en == lis_names_test
        assert bact_names_en == bact_names_test

