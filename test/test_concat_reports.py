#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib
import re
import unittest

import numpy as np
import pandas as pd
import pytest

import concat_reports


class TestMergeAndPivot(unittest.TestCase):
    def test_merge_and_pivot(self):
        sample_reports = [(pathlib.Path(__file__).parent / "data" / "generate_report" / "mads_long_1.tsv"),
                          (pathlib.Path(__file__).parent / "data" / "generate_report" / "mads_long_2.tsv")]
        true_result = pd.DataFrame(data = {"0": ["1199123456-1", np.nan, np.nan,
                                                 "Fakeococcus fakei", "-", "IncX3",
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 "FakeSerotype", np.nan, np.nan, np.nan, np.nan],
                                           "1": ["1199654321-1", np.nan, np.nan,
                                                 "Placeholderia fakeorum", "-",
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan, np.nan, np.nan]},
                                   index = ["proevenr", 'O_WGS CC-type', 'O_WGS cgMLST',
                                            "O_WGS identifikation", 'O_WGS MLST',
                                            "O_WGS Plasmid-ID_0",
                                            "O_WGS res.gen anden beta-lactamase",
                                            "O_WGS res.gen carbapenemase",
                                            "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                                            'O_WGS Serotype/gruppe', 'O_WGS Spatype',
                                            'O_WGS ST', 'O_WGS supplerende type',
                                            "O_WGS Toxinpåvisning_0"])
        # column names need to be a RangeIndex but manually setting up the df doesn't give this
        true_result.columns = pd.RangeIndex(start = 0, stop = 2, step = 1)
        test_result = concat_reports.merge_and_make_wide(sample_reports, "da")
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False,
                                      check_column_type = False)

    def test_ensure_toxins_match(self):
        sample_reports = [(pathlib.Path(__file__).parent / "data" / "generate_report" / "mads_long_toxin_1.tsv"),
                          (pathlib.Path(__file__).parent / "data" / "generate_report" / "mads_long_toxin_2.tsv")]
        true_result = pd.DataFrame(data = {"0": ["1199123456-1", np.nan, np.nan,
                                                 "Fakeococcus fakei", "-", "IncX3", np.nan,
                                                 np.nan,
                                                 np.nan, np.nan, "FakeSerotype",
                                                 np.nan,
                                                 np.nan, np.nan,
                                                 "eae", "eltA"],
                                           "1": ["1199654321-1", np.nan, np.nan,
                                                 "Placeholderia fakeorum", "-",
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan]},
                                   index = ["proevenr", 'O_WGS CC-type', 'O_WGS cgMLST',
                                            "O_WGS identifikation", 'O_WGS MLST',
                                            "O_WGS Plasmid-ID_0",
                                            "O_WGS res.gen anden beta-lactamase",
                                            "O_WGS res.gen carbapenemase",
                                            "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                                            'O_WGS Serotype/gruppe', 'O_WGS Spatype',
                                            'O_WGS ST', 'O_WGS supplerende type',
                                            "O_WGS Toxinpåvisning_0", "O_WGS Toxinpåvisning_1"])
        # column names need to be a RangeIndex but manually setting up the df doesn't give this
        true_result.columns = pd.RangeIndex(start = 0, stop = 2, step = 1)
        test_result = concat_reports.merge_and_make_wide(sample_reports, "da")
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False,
                                      check_column_type = False)

    def test_ensure_toxins_match_en(self):
        """Ensure translated reports can be merged properly and toxin gene ID sorting is handled."""
        sample_reports = [(pathlib.Path(__file__).parent / "data" / "generate_report"
                           / "mads_long_toxin_1_en.tsv"),
                          (pathlib.Path(__file__).parent / "data" / "generate_report"
                           / "mads_long_toxin_2_en.tsv")]
        true_result = pd.DataFrame(data = {"0": ["1199123456-1", np.nan, np.nan,
                                                 "Fakeococcus fakei", "-", "IncX3", np.nan,
                                                 np.nan,
                                                 np.nan, np.nan, "FakeSerotype",
                                                 np.nan,
                                                 np.nan, np.nan,
                                                 "eae", "eltA"],
                                           "1": ["1199654321-1", np.nan, np.nan,
                                                 "Placeholderia fakeorum", "-",
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan]},
                                   index = ["sample_number", 'O_WGS CC-type', 'O_WGS cgMLST',
                                            "O_WGS identification", 'O_WGS MLST',
                                            "O_WGS Plasmid-ID_0",
                                            "O_WGS res.gene other beta-lactamase",
                                            "O_WGS res.gene carbapenemase",
                                            "O_WGS res.gene colistin",
                                            "O_WGS res.gene vancomycin",
                                            'O_WGS Serotype/group', 'O_WGS Spatype',
                                            'O_WGS ST', 'O_WGS supplementary type',
                                            "O_WGS toxin identification_0",
                                            "O_WGS toxin identification_1"])
        # column names need to be a RangeIndex but manually setting up the df doesn't give this
        true_result.columns = pd.RangeIndex(start = 0, stop = 2, step = 1)
        test_result = concat_reports.merge_and_make_wide(sample_reports, report_language = "EN")
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False,
                                      check_column_type = False)

    def test_ensure_mlst_format(self):
        sample_reports = [(pathlib.Path(__file__).parent / "data" / "concat_reports"
                           / "mads_mlst_1.tsv"),
                          (pathlib.Path(__file__).parent / "data" / "concat_reports"
                           / "mads_mlst_2.tsv")]
        true_result = pd.DataFrame(data = {"0": ["1199123456-1", np.nan, np.nan,
                                                 "Fakeococcus fakei", "42", "IncX3", np.nan,
                                                 np.nan,
                                                 np.nan, np.nan, "FakeSerotype",
                                                 np.nan,
                                                 np.nan, np.nan,
                                                 np.nan],
                                           "1": ["1199654321-1", np.nan, np.nan,
                                                 "Placeholderia fakeorum", "1",
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan]},
                                   index = ["proevenr", 'O_WGS CC-type', 'O_WGS cgMLST',
                                            "O_WGS identifikation", 'O_WGS MLST',
                                            "O_WGS Plasmid-ID_0",
                                            "O_WGS res.gen anden beta-lactamase",
                                            "O_WGS res.gen carbapenemase",
                                            "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                                            'O_WGS Serotype/gruppe', 'O_WGS Spatype',
                                            'O_WGS ST', 'O_WGS supplerende type',
                                            "O_WGS Toxinpåvisning_0"])
        # column names need to be a RangeIndex but manually setting up the df doesn't give this
        true_result.columns = pd.RangeIndex(start = 0, stop = 2, step = 1)
        test_result = concat_reports.merge_and_make_wide(sample_reports, "da")
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False)

    def test_mlst_with_na(self):
        sample_reports = [(pathlib.Path(__file__).parent / "data" / "concat_reports"
                           / "mads_mlst_1.tsv"),
                          (pathlib.Path(__file__).parent / "data" / "generate_report"
                           / "mads_long_2.tsv")]
        true_result = pd.DataFrame(data = {"0": ["1199123456-1", np.nan, np.nan,
                                                 "Fakeococcus fakei", "-", "IncX3", np.nan,
                                                 np.nan,
                                                 np.nan, np.nan, "FakeSerotype",
                                                 np.nan,
                                                 np.nan, np.nan,
                                                 np.nan],
                                           "1": ["1199654321-1", np.nan, np.nan,
                                                 "Placeholderia fakeorum", "1",
                                                 np.nan, np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan, np.nan,
                                                 np.nan]},
                                   index = ["proevenr", 'O_WGS CC-type', 'O_WGS cgMLST',
                                            "O_WGS identifikation", 'O_WGS MLST',
                                            "O_WGS Plasmid-ID_0",
                                            "O_WGS res.gen anden beta-lactamase",
                                            "O_WGS res.gen carbapenemase",
                                            "O_WGS res.gen colistin", "O_WGS res.gen vancomycin",
                                            'O_WGS Serotype/gruppe', 'O_WGS Spatype',
                                            'O_WGS ST', 'O_WGS supplerende type',
                                            "O_WGS Toxinpåvisning_0"])
        # column names need to be a RangeIndex but manually setting up the df doesn't give this
        true_result.columns = pd.RangeIndex(start = 0, stop = 2, step = 1)
        test_result = concat_reports.merge_and_make_wide(sample_reports, "da")
        pd.testing.assert_frame_equal(true_result, test_result, check_index_type = False)


class TestConcatGenericData(unittest.TestCase):
    def test_success_concat_generics(self):
        true_result = pd.DataFrame(data={"proevenr": ["1199123456-1",
                                                      "1199123456-2"],
                                         "completeness": [99.0, 99.9],
                                         "contamination": [0.0, 0.2]})
        test_frame_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" /\
                       "generic_frame_checkm_1.tsv"
        test_frame_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" /\
                       "generic_frame_checkm_2.tsv"
        test_result = concat_reports.concat_generic_reports([test_frame_1, test_frame_2], "\t")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_success_single_report(self):
        true_result = pd.DataFrame(data={"proevenr": ["1199123456-1"],
                                         "completeness": [99.0],
                                         "contamination": [0.0]})
        test_frame_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" /\
                       "generic_frame_checkm_1.tsv"
        test_result = concat_reports.concat_generic_reports([test_frame_1], "\t")
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_fail_different_colnames(self):
        test_frame_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_1.tsv"
        test_frame_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_coverage.tsv"
        test_frame_3 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_2.tsv"
        error_msg = "Column names aren't identical for all reports. This may indicate trying to" \
                    " concatenate different types of reports."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            concat_reports.concat_generic_reports([test_frame_1, test_frame_2, test_frame_3], "\t")

    def test_fail_missing_colnames(self):
        test_frame_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_1.tsv"
        test_frame_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_missing.tsv"
        test_frame_3 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_2.tsv"
        error_msg = "Column names aren't identical for all reports. This may indicate trying to" \
                    " concatenate different types of reports."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            concat_reports.concat_generic_reports([test_frame_1, test_frame_2, test_frame_3], "\t")

    def test_fail_extra_colnames(self):
        test_frame_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_1.tsv"
        test_frame_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_extra.tsv"
        test_frame_3 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                       "generic_frame_checkm_2.tsv"
        error_msg = "Column names aren't identical for all reports. This may indicate trying to" \
                    " concatenate different types of reports."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            concat_reports.concat_generic_reports([test_frame_1, test_frame_2, test_frame_3], "\t")


class TestConcatQuastReports(unittest.TestCase):
    def test_success_concat_quast(self):
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "Avg. coverage depth": [50.0, 60.0]})
        test_quast_read_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                            "quast_coverage_1.tsv"
        test_quast_read_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                            "quast_coverage_2.tsv"
        test_result = concat_reports.concat_quast_coverage([test_quast_read_1, test_quast_read_2])
        pd.testing.assert_frame_equal(true_result, test_result)


class TestConcatNG50(unittest.TestCase):
    def test_success_concat_NG50(self):
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                           "NG50": [20000, 500]})
        true_result = true_result.astype({"NG50": "Int64"})
        test_ng50_1 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                      "quast_ng50_1.tsv"
        test_ng50_2 = pathlib.Path(__file__).parent / "data" / "concat_reports" / \
                      "quast_ng50_2.tsv"
        test_result = concat_reports.concat_ng50([test_ng50_1, test_ng50_2])
        pd.testing.assert_frame_equal(true_result, test_result)


class TestTransposeQuastReads(unittest.TestCase):
    def test_transpose(self):
        fake_quast_reads = pd.DataFrame(data = {"Assembly": ["# total reads", "# left", "# right",
                                                             "# mapped", "Mapped (%)",
                                                             "# properly paired",
                                                             "Properly paired (%)",
                                                             "# singletons", "Singletons (%)",
                                                             "# misjoint mates",
                                                             "Misjoint mates (%)",
                                                             "Avg. coverage depth",
                                                             "Coverage >= 1x (%)",
                                                             "Coverage >= 5x (%)",
                                                             "Coverage >= 10x (%)"],
                                                "1199123456-1": [1000000, 500000, 500000, 750000,
                                                                75.0, 700000, 70.0, 300000, 30.0,
                                                                0, 0.0, 50.0, 100.0, 99.9, 98.9],
                                                "1199123456-2": [1000000, 500000, 500000, 750000,
                                                                75.0, 700000, 70.0, 300000, 30.0,
                                                                0, 0.0, 50.0, 100.0, 99.9, 98.9]
                                                })
        transposed_test = concat_reports.reshape_quast_reads(fake_quast_reads)
        true_transposed = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                               "Avg. coverage depth": [50.0, 50.0]})
        pd.testing.assert_frame_equal(transposed_test, true_transposed)


class TestGetNG50(unittest.TestCase):
    def test_success_ng50(self):
        success_input = pathlib.Path(__file__).parent / "data" / "merge_results" \
                        / "transposed_quast_ng50.txt"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "NG50": [20000]})
        true_result = true_result.astype({"NG50": "Int64"})

        test_result = concat_reports.get_quast_ng50(success_input)
        pd.testing.assert_frame_equal(true_result, test_result)

    def test_missing_ng50(self):
        missing_ng50_input = pathlib.Path(__file__).parent / "data" / "merge_results" \
                             / "transposed_quast_no_ng50.txt"
        true_result = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                           "NG50": [pd.NA]})

        test_result = concat_reports.get_quast_ng50(missing_ng50_input)
        pd.testing.assert_frame_equal(true_result, test_result)
