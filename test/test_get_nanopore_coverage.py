import pathlib
import re
import unittest

import pandas as pd
import pytest

import get_nanopore_coverage


class TestReadSampleReport(unittest.TestCase):
    def test_fail_bad_sample_number(self):
        """Complain when there are samples that don't match the sample number format."""
        test_file = (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456_assembly_info.txt")

        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        error_msg = ("Cannot extract sample number from 1199123456_assembly_info.txt"
                     " - invalid sample number format.")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_nanopore_coverage.get_report_with_sample_number(test_file,
                                                                  sample_number_format)

    def test_success_single_sample(self):
        """Get coverage for a single sample"""
        test_file = (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-1_assembly_info.txt")

        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"#seq_name": ["contig_1"],
                                         "length": [1000],
                                         "cov.": [10.0],
                                         "proevenr": ["1199123456-1"]})
        test_coverage = get_nanopore_coverage.get_report_with_sample_number(test_file,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)

    def test_success_empty_sample(self):
        """Handle a blank file"""
        test_file = (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-5_assembly_info.txt")

        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"#seq_name": [pd.NA],
                                         "length": [pd.NA],
                                         "cov.": [pd.NA],
                                         "proevenr": ["1199123456-5"]})
        test_coverage = get_nanopore_coverage.get_report_with_sample_number(test_file,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)


class TestGetAverageCoverage(unittest.TestCase):
    def test_fail_bad_sample_number(self):
        """Complain when there are samples that don't match the sample number format."""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       /"get_nanopore_coverage"/"1199123456-1_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        error_msg = ("Cannot extract sample number from ['1199123456_assembly_info.txt']"
                     " - invalid sample number format.")
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                  sample_number_format)

    def test_success_single_sample(self):
        """Get coverage for a single sample"""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-1_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"proevenr": ["1199123456-1"],
                                         "Weighted coverage depth": [10.0],
                                         "longest_contig_coverage": [10.0]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)

    def test_success_multi_sample(self):
        """Get coverage for multiple samples"""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-1_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-2_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2"],
                                         "Weighted coverage depth": [10.0, 10.0],
                                         "longest_contig_coverage": [10.0, 10.0]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)

    def test_success_multi_sample_one_blank(self):
        """Handle the presence of a blank result."""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-1_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-5_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-5"],
                                         "Weighted coverage depth": [10.0, pd.NA],
                                         "longest_contig_coverage": [10.0, pd.NA]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage, check_dtype=False)

    def test_success_multi_sample_barcodes(self):
        """Get coverage for multiple samples"""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "barcode01_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "barcode02_assembly_info.txt")
                      ]
        sample_number_format = r'(barcode)\d{2}'
        result_df = pd.DataFrame(data = {"proevenr": ["barcode01", "barcode02"],
                                         "Weighted coverage depth": [10.0, 10.0],
                                         "longest_contig_coverage": [10.0, 10.0]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)

    def test_success_multi_contigs(self):
        """Average coverage across multiple contigs"""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-2_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-3_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"proevenr": ["1199123456-2", "1199123456-3"],
                                         "Weighted coverage depth": [10.0, 20.0],
                                         "longest_contig_coverage": [10.0, 30.0]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage)


    def test_success_weight_by_length(self):
        """Get weighted average coverage by length"""
        test_files = [(pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-2_assembly_info.txt"),
                      (pathlib.Path(__file__).parent / "data"
                       / "get_nanopore_coverage" / "1199123456-4_assembly_info.txt")
                      ]
        sample_number_format = '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'
        result_df = pd.DataFrame(data = {"proevenr": ["1199123456-2", "1199123456-4"],
                                         "Weighted coverage depth": [10.0, 11.81818181818182],
                                         "longest_contig_coverage": [10.0, 10.0]})
        test_coverage = get_nanopore_coverage.get_average_coverage_per_sample(test_files,
                                                                              sample_number_format)
        pd.testing.assert_frame_equal(result_df, test_coverage, check_exact = False)

