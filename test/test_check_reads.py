#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import pytest

import check_reads


class TestCheckReadFormat(unittest.TestCase):
    def test_wrong_format(self):
        not_a_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "not_a_read.png"
        error_msg = (f"Read file {not_a_read} should be ASCII plain text,"
                     f" is image/png; charset=binary.")
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.check_read_file_type(not_a_read)

    def test_empty_file(self):
        empty_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "empty_read.fastq"
        error_msg = f"No reads found in {empty_read}."
        with pytest.raises(check_reads.NoReadsError, match = re.escape(error_msg)):
            check_reads.check_read_file_type(empty_read)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_message(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read.fastq"
        success_msg = f"File format for read file {forward} is correct."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.check_read_file_type(forward)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples


class TestCheckReadLengths(unittest.TestCase):
    def test_empty_file(self):
        empty_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "empty_read.fastq"
        error_msg = f"No reads found in {empty_read}."
        with pytest.raises(check_reads.NoReadsError, match = re.escape(error_msg)):
            check_reads.count_reads(empty_read)

    def test_wrong_linecount(self):
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_lines_read.fastq"
        error_msg = f"Wrong line count in {wrong_read} " \
                    f"- FASTQ files should have four lines per read."
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.count_reads(wrong_read)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_message(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read.fastq"
        success_msg = f"Line count for read file {forward} is correct."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.count_reads(forward)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples


class TestCompareReads(unittest.TestCase):
    def test_length_mismatch(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "two_forward_reads.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"Amount of forward and reverse reads don't match. " \
                    f"There are 2 in {forward} (forward) but " \
                    f"1 in {reverse} (reverse)."
        with pytest.raises(check_reads.ReadMismatchError, match = re.escape(error_msg)):
            check_reads.compare_read_counts(forward, reverse)

    def test_catch_empty(self):
        empty_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "empty_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"No reads found in {empty_read}."
        with pytest.raises(check_reads.NoReadsError, match = re.escape(error_msg)):
            check_reads.compare_read_counts(empty_read, reverse)

    def test_catch_lines(self):
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_lines_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"Wrong line count in {wrong_read} " \
                    f"- FASTQ files should have four lines per read."
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.compare_read_counts(wrong_read, reverse)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_message(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        success_msg = f"Read counts for {forward} and {reverse} match (1 and 1)."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.compare_read_counts(forward, reverse)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples


class TestParserFail(unittest.TestCase):
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_biopython_read_success(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read.fastq"
        success_msg = f"Successfully parsed {forward} in Biopython."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.check_if_parseable(forward)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples

    def test_biopython_fail_wrong_start(self):
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_start_read.fastq"
        error_msg = f"Biopython error parsing {wrong_read}:\n" \
                    f"Records in Fastq files should start with '@' character"
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.check_if_parseable(wrong_read)


    def test_success_quality_with_at(self):
        """Check if the parser is thrown off by quality starting with @."""
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read_qual_at.fastq"
        success_msg = f"Successfully parsed {forward} in Biopython."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.check_if_parseable(forward)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples

    def test_fail_quality_with_at(self):
        """Check if the parser catches a malformed fasta where the quality line starts with @."""
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_start_read_qual_at.fastq"
        error_msg = f"Biopython error parsing {wrong_read}:\n" \
                    f"Records in Fastq files should start with '@' character"
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.check_if_parseable(wrong_read)


class TestValidateReads(unittest.TestCase):
    def test_length_mismatch(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "two_forward_reads.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"Amount of forward and reverse reads don't match. " \
                    f"There are 2 in {forward} (forward) but " \
                    f"1 in {reverse} (reverse)."
        with pytest.raises(check_reads.ReadMismatchError, match = re.escape(error_msg)):
            check_reads.validate_paired_reads(forward, reverse)

    def test_catch_empty(self):
        empty_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "empty_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"No reads found in {empty_read}."
        with pytest.raises(check_reads.NoReadsError, match = re.escape(error_msg)):
            check_reads.validate_paired_reads(empty_read, reverse)

    def test_catch_lines(self):
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_lines_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"Biopython error parsing {wrong_read}:\n" \
                    f"End of file without quality information."
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.validate_paired_reads(wrong_read, reverse)

    def test_catch_parser_fail(self):
        wrong_read = pathlib.Path(__file__).parent / "data" / "check_reads" / "wrong_start_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        error_msg = f"Biopython error parsing {wrong_read}:\n" \
                    f"Records in Fastq files should start with '@' character"
        with pytest.raises(check_reads.BrokenReadFileError, match = re.escape(error_msg)):
            check_reads.validate_paired_reads(wrong_read, reverse)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success(self):
        forward = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_forward_read.fastq"
        reverse = pathlib.Path(__file__).parent / "data" / "check_reads" / "one_reverse_read.fastq"
        success_msg = f"No issues found for read files {forward} and {reverse}."
        with self._caplog.at_level(level="INFO", logger="check_reads"):
            check_reads.validate_paired_reads(forward, reverse)
            assert ("check_reads", logging.INFO, success_msg) in self._caplog.record_tuples
