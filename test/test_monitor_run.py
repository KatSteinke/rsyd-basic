import logging
import pathlib
import re
import unittest
from collections import namedtuple
from datetime import timedelta

from unittest import mock

import pandas as pd
import pytest

import monitor_run


class TestInitializeIsolateRun(unittest.TestCase):
    input_dir = pathlib.Path("path/to/input")
    configfile = pathlib.Path("path/to/configfile")
    illumina_default_config = {"debug": False,
                               "lab_info_system": {"use_lis_features": False},
                               "sample_number_settings": {'sample_number_format':
                                                              '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                          'sample_numbers_in': 'letter',
                                                          'sample_numbers_out': 'number',
                                                          "sample_numbers_report": "letter",
                                                          'number_to_letter': {"30": "B", "10": "D",
                                                                               "11": "F",
                                                                               "50": "T"},
                                                          "format_in_sheet":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_output":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          },

                               "sequencing_mode": "illumina",
                               "paths": {"output_base_path": "/path/to/output"},
                               "seq_run_duration_hours": 1,
                               "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")}

    def test_fail_hybrid_run(self):
        """Complain if trying to initialize a hybrid run."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "hybrid",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization"/ "input_da.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "test_nanopore_runsheet.xlsx")

        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        error_msg = "Hybrid sequencing is not yet implemented."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet, self.configfile,
                                   test_config)

    def test_fail_invalid_mode(self):
        """Complain if an invalid sequencing mode is given."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "both",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "test_nanopore_runsheet.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        error_msg = "Invalid sequencing mode both."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet, self.configfile,
                                   test_config)

    def test_fail_missing_runsheet(self):
        """Complain about a missing runsheet."""
        no_runsheet_message = "Could not find runsheet at no/such/path"
        runsheet = pathlib.Path("no/such/path")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        with pytest.raises(FileNotFoundError, match = re.escape(no_runsheet_message)):
            monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet, self.configfile,
                                   self.illumina_default_config)

    def test_initialize_isolate_run_illumina(self):
        """Successfully initialize an Illumina IsolateRun."""
        runsheet = (pathlib.Path(__file__).parent / "data"/"utilities_test"
                    /"runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data={"sample": ["F99123456-1"],
                                          "runtype": ["paired-end"],
                                          "r1": ["F99123456_R1.fastq"],
                                          "r2": ["F99123456_R2.fastq"],
                                          "extra": [pd.NA]})
        expected_outdir = (pathlib.Path(self.illumina_default_config["paths"]["output_base_path"])
                           / "ILM_Run0000_Y20220101_XYZ")
        expected_fofn = expected_outdir /"ILM_Run0000_Y20220101_XYZ.tsv"
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config)
        assert illumina_run.rundir_path == self.input_dir
        assert illumina_run.configfile == self.configfile
        assert illumina_run.file_of_filenames == expected_fofn
        pd.testing.assert_frame_equal(illumina_run.sample_sheet, sample_sheet)
        assert illumina_run.sequencing_time == timedelta(hours=self.illumina_default_config["seq_run_duration_hours"])
        assert illumina_run.outdir == expected_outdir
        assert not illumina_run.test_run
        assert illumina_run.seq_mode == "illumina"

    def test_initialize_isolate_run_illumina_en_from_config(self):
        """Successfully initialize an Illumina IsolateRun fron a runsheet with
        non-default column names from config."""
        illumina_config = {"debug": False,
                               "lab_info_system": {"use_lis_features": False},
                               "sample_number_settings": {'sample_number_format':
                                                              '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                          'sample_numbers_in': 'letter',
                                                          'sample_numbers_out': 'number',
                                                          "sample_numbers_report": "letter",
                                                          'number_to_letter': {"30": "B", "10": "D",
                                                                               "11": "F",
                                                                               "50": "T"},
                                                          "format_in_sheet":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_output":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          },

                               "sequencing_mode": "illumina",
                               "paths": {"output_base_path": "/path/to/output"},
                               "seq_run_duration_hours": 1,
                               "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml")}

        runsheet = (pathlib.Path(__file__).parent / "data"/"utilities_test"
                    /"runsheet_experiment_name_en.xlsx")
        sample_sheet = pd.DataFrame(data={"sample": ["1199123456-1"],
                                          "runtype": ["paired-end"],
                                          "r1": ["F99123456_R1.fastq"],
                                          "r2": ["F99123456_R2.fastq"],
                                          "extra": [pd.NA]})
        expected_outdir = (pathlib.Path(self.illumina_default_config["paths"]["output_base_path"])
                           / "ILM_Run0000_Y20220101_XYZ")
        expected_fofn = expected_outdir /"ILM_Run0000_Y20220101_XYZ.tsv"
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, illumina_config)
        assert illumina_run.rundir_path == self.input_dir
        assert illumina_run.configfile == self.configfile
        assert illumina_run.file_of_filenames == expected_fofn
        pd.testing.assert_frame_equal(illumina_run.sample_sheet, sample_sheet)
        assert illumina_run.sequencing_time == timedelta(hours=self.illumina_default_config["seq_run_duration_hours"])
        assert illumina_run.outdir == expected_outdir
        assert not illumina_run.test_run
        assert illumina_run.seq_mode == "illumina"


    def test_initialize_isolate_run_nanopore(self):
        """Successfully initialize a Nanopore IsolateRun."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "nanopore",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "test_nanopore_runsheet.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        expected_outdir = pathlib.Path(
            test_config["paths"]["output_base_path"]) / "ONT_RUN0000_Y20990101_XYZ"
        expected_fofn = expected_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nanopore_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, test_config)
        assert nanopore_run.rundir_path == self.input_dir
        assert nanopore_run.configfile == self.configfile
        assert nanopore_run.file_of_filenames == expected_fofn
        pd.testing.assert_frame_equal(nanopore_run.sample_sheet, sample_sheet)
        assert nanopore_run.outdir == expected_outdir
        assert nanopore_run.sequencing_time == timedelta(hours=test_config["seq_run_duration_hours"])
        assert not nanopore_run.test_run
        assert nanopore_run.seq_mode == "nanopore"


    def test_initialize_isolate_run_nanopore_en(self):
        """Successfully initialize a Nanopore IsolateRun with non-default column names
        from config."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "nanopore",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "test_nanopore_runsheet_en.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        expected_outdir = pathlib.Path(
            test_config["paths"]["output_base_path"]) / "ONT_RUN0000_Y20990101_XYZ"
        expected_fofn = expected_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nanopore_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, test_config)
        assert nanopore_run.rundir_path == self.input_dir
        assert nanopore_run.configfile == self.configfile
        assert nanopore_run.file_of_filenames == expected_fofn
        pd.testing.assert_frame_equal(nanopore_run.sample_sheet, sample_sheet)
        assert nanopore_run.outdir == expected_outdir
        assert nanopore_run.sequencing_time == timedelta(hours=test_config["seq_run_duration_hours"])
        assert not nanopore_run.test_run
        assert nanopore_run.seq_mode == "nanopore"


    def test_override_seq_mode(self):
        """Successfully override sequencing mode."""
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "illumina",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "test_nanopore_runsheet.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["ont"],
                                            "r1": [pd.NA],
                                            "r2": [pd.NA],
                                            "extra": ["/path/to/input/barcode01"]})
        expected_outdir = pathlib.Path(
            test_config["paths"]["output_base_path"]) / "ONT_RUN0000_Y20990101_XYZ"
        expected_fofn = expected_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
        nanopore_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, test_config,
                                              sequencing_mode = "nanopore")
        assert nanopore_run.rundir_path == self.input_dir
        assert nanopore_run.configfile == self.configfile
        assert nanopore_run.file_of_filenames == expected_fofn
        pd.testing.assert_frame_equal(nanopore_run.sample_sheet, sample_sheet)
        assert nanopore_run.outdir == expected_outdir
        assert nanopore_run.sequencing_time == timedelta(
            hours = test_config["seq_run_duration_hours"])
        assert not nanopore_run.test_run
        assert nanopore_run.seq_mode == "nanopore"


    def test_initialize_test_run(self):
        """Successfully initialize an IsolateRun as a test run,
        with test specified in the config."""
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "illumina",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, test_config)
        assert illumina_run.test_run

    def test_override_test_run(self):
        """Override test parameters specified in the config."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})

        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config,
                                              test_run = True)
        assert illumina_run.test_run
        test_config = {"debug": True,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },

                       "sequencing_mode": "illumina",
                       "paths": {"output_base_path": "/path/to/output"},
                       "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        no_test_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                             self.configfile, test_config, test_run = False)
        assert not no_test_run.test_run

    def test_initialize_with_new_outdir(self):
        """Override default outdir."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        expected_outdir = pathlib.Path(
            self.illumina_default_config["paths"]["output_base_path"]) / "ILM_Run0000_Y20220101_XYZ_ny"
        expected_fofn = expected_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config,
                                              outdir = expected_outdir)
        assert expected_outdir == illumina_run.outdir
        assert expected_fofn == illumina_run.file_of_filenames

    # TODO: handling of changes?
    def test_set_new_outdir(self):
        """Set new outdir after initialization."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        expected_outdir = pathlib.Path(
            self.illumina_default_config["paths"]["output_base_path"]) / "ILM_Run0000_Y20220101_XYZ"
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config)
        assert illumina_run.outdir == expected_outdir
        new_outdir = (pathlib.Path(self.illumina_default_config["paths"]["output_base_path"])
                      / "ILM_Run0000_Y20220101_XYZ_ny")
        expected_fofn = new_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        illumina_run.set_outdir(new_outdir)
        assert illumina_run.outdir == new_outdir
        assert illumina_run.file_of_filenames == expected_fofn

    def test_fail_invalid_sequencing_time(self):
        """Fail if the sequencing time is lower than 0 hours."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        bad_seq_time = -1.5
        error_msg = "Expected sequencing time must be greater than 0 hours."
        with pytest.raises(ValueError, match=error_msg):
            monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet, self.configfile,
                                   self.illumina_default_config, sequencing_time = bad_seq_time)

    def test_override_sequencing_time(self):
        """Override default sequencing time."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        expected_seq_time = 1.5
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config,
                                              sequencing_time = expected_seq_time)
        assert timedelta(hours=expected_seq_time) == illumina_run.sequencing_time

    def test_print_run(self):
        """Print run attributes in the correct format."""

        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        expected_outdir = pathlib.Path(
            self.illumina_default_config["paths"]["output_base_path"]) / "ILM_Run0000_Y20220101_XYZ"
        expected_fofn = expected_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config)
        expected_repr = (f"IsolateRun(rundir_path={self.input_dir}, runsheet_path={runsheet},"
                         f" file_of_filenames={expected_fofn}, configfile={self.configfile},"
                         f" active_config={self.illumina_default_config}, test_run=False, "
                         f"sequencing_time={timedelta(hours=1)}, "
                         f"outdir={expected_outdir}, "
                         f"seq_mode=illumina)\n"
                         f"Sample sheet:\n{sample_sheet.to_string()}")
        test_repr = illumina_run.__repr__()
        assert expected_repr == test_repr

    def test_compare_equal_runs(self):
        """Compare two identical runs."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        illumina_run_1 = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                                self.configfile, self.illumina_default_config)
        illumina_run_2 = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                                self.configfile, self.illumina_default_config,
                                                test_run = False)

        assert illumina_run_1 == illumina_run_2

    def test_compare_not_equal_runs(self):
        """Compare two different runs."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        new_outdir = (pathlib.Path(self.illumina_default_config["paths"]["output_base_path"])
                      / "ILM_Run0000_Y20220101_XYZ_ny")
        illumina_run_1 = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                                self.configfile, self.illumina_default_config)
        illumina_run_2 = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                                self.configfile, self.illumina_default_config,
                                                outdir = new_outdir)
        assert illumina_run_1 != illumina_run_2

    def test_compare_wrong_type(self):
        """Compare an IsolateRun against an object with a different type."""
        runsheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "runsheet_experiment_name.xlsx")
        sample_sheet = pd.DataFrame(data = {"sample": ["F99123456-1"],
                                            "runtype": ["paired-end"],
                                            "r1": ["F99123456_R1.fastq"],
                                            "r2": ["F99123456_R2.fastq"],
                                            "extra": [pd.NA]})
        illumina_run = monitor_run.IsolateRun(self.input_dir, runsheet, sample_sheet,
                                              self.configfile, self.illumina_default_config)
        OldIsolateRun = namedtuple("OldIsolateRun", ["rundir_path", "runsheet_path",
                                                     "sample_sheet", "file_of_filenames",
                                                     "configfile", "active_config", "outdir",
                                                     "sequencing_time",
                                                     "test_run"])
        expected_outdir = (pathlib.Path( self.illumina_default_config["paths"]["output_base_path"])
                           / "ILM_Run0000_Y20220101_XYZ")
        expected_fofn = expected_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        old_run = OldIsolateRun(self.input_dir, runsheet, sample_sheet, expected_fofn,
                                self.configfile, self.illumina_default_config, expected_outdir,
                                1, False)
        assert not illumina_run == old_run
        assert illumina_run != old_run


class TestGetRunInputs(unittest.TestCase):
    test_outdir = pathlib.Path("/path/to/outdir")
    test_configfile = pathlib.Path("/path/to/config.yaml")
    test_config = {"debug": False,
                   "lab_info_system": {"use_lis_features": False},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "/path/to/output"},
                   "sequencing_mode": "illumina",
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                   }

    def test_run_with_defaults(self):
        """Set up a run with all default parameters."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                  / "multi_fastq_nextseq" \
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"

        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        expected_outdir = (pathlib.Path(self.test_config["paths"]["output_base_path"])
                           / "ILM_Run0000_Y20220101_XYZ")
        expected_fofn = expected_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir,
                                                                illumina_runsheet = runsheet,
                                                                active_config = self.test_config,
                                                                config_path = self.test_configfile)
        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert expected_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_get_illumina_data(self):
        """Set up an Illumina isolate sequencing run."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                  / "multi_fastq_nextseq" \
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"

        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        expected_fofn = self.test_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                illumina_runsheet = runsheet,
                                                                active_config = self.test_config,
                                                                config_path = self.test_configfile)
        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    # use pytest's caplog fixture for flexibility in some cases
    # - https://stackoverflow.com/a/50375022/15704972
    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_get_illumina_data_mads(self):
        """Set up an Illumina isolate sequencing run using LIS data."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                  / "multi_fastq_nextseq" \
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"
        test_config = {"debug": False,
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        expected_fofn = self.test_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        # this should not complain about anything
        with self._caplog.at_level(logger="helpers", level=logging.WARNING):
            test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                    illumina_runsheet = runsheet,
                                                                    active_config = test_config,
                                                                    config_path = self.test_configfile)
            assert not self._caplog.records
        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_get_illumina_data_mads_en(self):
        """Set up an Illumina isolate sequencing run using LIS data with
        non-default column names."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                  / "multi_fastq_nextseq" \
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample_en.xlsx"
        test_config = {"debug": False,
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'letter',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep_en.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep_en.csv")},
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                           / "utilities_test"
                                                           / "multi_fastq_nextseq"
                                                           / "test" / "Analysis" / "1" / "Data"
                                                           / "fastq"
                                                           / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        expected_fofn = self.test_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                illumina_runsheet = runsheet,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_get_nanopore_data(self):
        """Set up a Nanopore isolate sequencing run."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode42")]
                                                })
        expected_fofn = self.test_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"

        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                nanopore_runsheet = runsheet,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_get_nanopore_data_subdir(self):
        """Set up a Nanopore isolate sequencing run where a fastq_pass directory
        has to be specified explicitly."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir_multi" / "rawdata" / "subdir" / "fastq_pass"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir_multi"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir_multi"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode42")]
                                                })
        expected_fofn = self.test_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"

        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                nanopore_runsheet = runsheet,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_get_nanopore_data_en(self):
        """Set up a Nanopore isolate sequencing run with non-default column names."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_en.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode42")]
                                                })
        expected_fofn = self.test_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"

        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                nanopore_runsheet = runsheet,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_override_config_illumina(self):
        """Initialize an Illumina run if an Illumina sheet is given even if runsheet says otherwise."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                  / "multi_fastq_nextseq" \
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_singleindication_multisample.xlsx"
        test_config = {"debug": False,
                               "lab_info_system": {"use_lis_features": False},
                               "sample_number_settings": {'sample_number_format':
                                                              '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                          'sample_numbers_in': 'letter',
                                                          'sample_numbers_out': 'number',
                                                          "sample_numbers_report": "letter",
                                                          'number_to_letter': {"30": "B", "10": "D",
                                                                               "11": "F",
                                                                               "50": "T"},
                                                          "format_in_sheet":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          "format_output":
                                                              r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                          },

                               "sequencing_mode": "nanopore",
                               "paths": {"output_base_path": "/path/to/output"},
                               "seq_run_duration_hours": 1,
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R1_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R1_001.fastq.gz")
                                                       ],
                                                "r2": [(pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-1_L001_R2_001.fastq.gz"),
                                                       (pathlib.Path(__file__).parent / "data"
                                                        / "utilities_test"
                                                        / "multi_fastq_nextseq"
                                                        / "test" / "Analysis" / "1" / "Data"
                                                        / "fastq"
                                                        / "B99123456-2_L001_R2_001.fastq.gz")
                                                       ],
                                                "extra": ["", ""]
                                                })
        expected_fofn = self.test_outdir / "ILM_Run0000_Y20220101_XYZ.tsv"
        warn_msg = ("Illumina runsheet given but config expects nanopore. "
                    "Check that you are using the correct config file.")
        with self._caplog.at_level(level="WARNING", logger="launch_run"):
            test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                    illumina_runsheet = runsheet,
                                                                    active_config = test_config,
                                                                    config_path = self.test_configfile)
            assert ("launch_run", logging.WARNING, warn_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run

    def test_override_config_nanopore(self):
        """Initialize a Nanopore run if a Nanopore sheet is given even if config says otherwise."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "illumina",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode42")]
                                                })
        expected_fofn = self.test_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"

        warn_msg = ("Nanopore runsheet given but config expects illumina. "
                    "Check that you are using the correct config file.")
        with self._caplog.at_level(level="WARNING", logger="launch_run"):
            test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                    nanopore_runsheet = runsheet,
                                                                    active_config = test_config,
                                                                    config_path = self.test_configfile)
            assert ("launch_run", logging.WARNING, warn_msg) in self._caplog.record_tuples

        # TODO: shorten?
        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert not test_run_params.test_run


    def test_set_seq_time(self):
        """Set sequencing time."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        seq_time_hours = 0.5
        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                nanopore_runsheet = runsheet,
                                                                sequencing_time = 0.5,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile)
        assert timedelta(hours=seq_time_hours) == test_run_params.sequencing_time


    def test_override_debug_mode(self):
        """Successfully activate or deactivate debug mode while overriding config."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet.xlsx"
        test_config = {"debug": False,
                       "lab_info_system": {"use_lis_features": False},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization"/ "input_da.yaml")
                       }
        expected_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "r1": ["", ""],
                                                "r2": ["", ""],
                                                "extra": [(pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode01"),
                                                          (pathlib.Path(__file__).parent
                                                              / "data"
                                                              / "prep_sheets"
                                                              / "test_dir"
                                                              / "rawdata"
                                                              / "subdir"
                                                              / "fastq_pass"
                                                              / "barcode42")]
                                                })
        expected_fofn = self.test_outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"

        test_run_params = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                nanopore_runsheet = runsheet,
                                                                active_config = test_config,
                                                                config_path = self.test_configfile,
                                                                test_run = True)

        pd.testing.assert_frame_equal(expected_samples, test_run_params.sample_sheet)
        assert seq_dir == test_run_params.rundir_path
        assert runsheet == test_run_params.runsheet_path
        assert expected_fofn == test_run_params.file_of_filenames
        assert self.test_configfile == test_run_params.configfile
        assert self.test_outdir == test_run_params.outdir
        assert test_run_params.test_run

        test_config = {"debug": True,
                           "lab_info_system": {"use_lis_features": False},
                           "sample_number_settings": {'sample_number_format':
                                                          '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                      'sample_numbers_in': 'letter',
                                                      'sample_numbers_out': 'number',
                                                      "sample_numbers_report": "letter",
                                                      'number_to_letter': {"30": "B", "10": "D",
                                                                           "11": "F", "50": "T"},
                                                      "format_in_sheet":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_output":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      },
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                       }
        test_run_no_debug = monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                                  nanopore_runsheet = runsheet,
                                                                  active_config = test_config,
                                                                  config_path = self.test_configfile,
                                                                  test_run = False)

        assert not test_run_no_debug.test_run

    def test_fail_nanopore_no_barcodes(self):
        """Fail when no barcodes are given for a Nanopore run."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_no_barcodes.xlsx"
        workflow_config = {"debug": False, "lab_info_system": {"use_lis_features": False},
                           "sample_number_settings": {'sample_number_format':
                                                          '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                      'sample_numbers_in': 'letter',
                                                      'sample_numbers_out': 'number',
                                                      'number_to_letter': {"30": "B", "10": "D",
                                                                           "11": "F", "50": "T"},
                                                      "format_in_sheet":
                                                          r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_in_lis": r'(?P<sample_type>[135]0|11)(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)'
                                                      },
                           "seq_run_duration_hours": 1,
                           "paths": {"output_base_path": "/path/to/output"},
                           "sequencing_mode": "nanopore",
                           "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization"/ "input_da.yaml")
                           }

        error_msg = f"No barcodes given for sample(s) ['B99123456-1', 'B99123456-2']."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                  nanopore_runsheet = runsheet,
                                                  active_config = workflow_config)

    def test_complain_hybrid_not_implemented(self):
        """Raise an error if hybrid sequencing input is given."""
        seq_dir = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        nanopore_runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                            / "test_nanopore_runsheet.xlsx"
        illumina_runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                            / "runsheet_singleindication_multisample.xlsx"
        workflow_config = {"lab_info_system": {"use_lis_features": False},
                           "debug": False,
                           "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization"/ "input_da.yaml")
                           }
        error_msg = "Hybrid sequencing analysis is currently not implemented."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                  illumina_runsheet = illumina_runsheet,
                                                  nanopore_runsheet = nanopore_runsheet,
                                                  active_config = workflow_config)

    def test_fail_no_data(self):
        workflow_config = {"debug": False,
                           "lab_info_system": {"use_lis_features": False},
                           "sample_number_settings": {'sample_number_format':
                                                          '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                      'sample_numbers_in': 'letter',
                                                      'sample_numbers_out': 'number',
                                                      'sample_numbers_report': 'letter',
                                                      'number_to_letter': {"30": "B", "10": "D",
                                                                           "11": "F", "50": "T"},
                                                      "format_in_sheet":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                      "format_output":
                                                          r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)'
                                                      },
                           "seq_run_duration_hours": 1,
                           "paths": {"output_base_path": "/path/to/output"
                                     },
                           "input_names": (pathlib.Path(__file__).parent / "data"
                                           / "localization" / "input_da.yaml")
                           }
        seq_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq"\
                  / "test" / "Analysis" / "1" / "Data" / "fastq"
        err_msg = "One of illumina_runsheet or nanopore_runsheet needs to be supplied " \
                  "to start analysis."
        with pytest.raises(ValueError, match=re.escape(err_msg)):
            monitor_run.get_run_input_by_seq_mode(seq_dir, self.test_outdir,
                                                  active_config = workflow_config)


class TestGetStartCommand(unittest.TestCase):
    outdir = pathlib.Path(__file__).parent /"data"/"monitor_run"/"test_illumina_outdir"
    test_configfile = pathlib.Path("/path/to/config.yaml")
    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "runsheet_experiment_name.xlsx"
    rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "fastq_nextseq" / "test"\
             / "Analysis" / "1" / "Data" / "fastq"
    test_config = {"debug": True,
                   "lab_info_system": {"use_lis_features": True},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": str(pathlib.Path(__file__).parent /
                                                     "data"/"monitor_run"/"test_illumina_outdir")},
                   "sequencing_mode": "illumina",
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml")
                   }
    run_params = monitor_run.IsolateRun(rundir_path = rundir, runsheet_path = runsheet,
                                        sample_sheet = pd.DataFrame(), configfile = test_configfile,
                                        active_config = test_config, outdir = outdir)

    def test_run_test_job(self):
        expected_command = ["nomad", "job", "dispatch",
                            "-meta", f"bactopia_names={str(self.run_params.file_of_filenames)}",
                            "-meta", f'outdir={str(self.outdir)}',
                            "-meta", f"outdir_name={self.outdir.name}",
                            "-meta", 'node_dir=None',
                            "-meta", "use_lis=1",
                            "-meta", f"runsheet={str(self.runsheet)}",
                            "-meta", f"sequencing_dir={str(self.rundir)}",
                            "staging-snaketopia", str(self.test_configfile)]
        test_command = monitor_run.get_nomad_command(self.run_params)
        assert expected_command == test_command

    def test_force_set_test_param(self):
        """Set the test_run parameter if it hasn't been set already"""
        test_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                             runsheet_path = self.runsheet,
                                             sample_sheet = pd.DataFrame(),
                                             configfile = self.test_configfile,
                                             active_config = self.test_config,
                                             outdir = self.outdir,
                                             test_run = None)
        expected_command = ["nomad", "job", "dispatch",
                            "-meta", f"bactopia_names={str(self.run_params.file_of_filenames)}",
                            "-meta", f'outdir={str(self.outdir)}',
                            "-meta", f"outdir_name={self.outdir.name}",
                            "-meta", 'node_dir=None',
                            "-meta", "use_lis=1",
                            "-meta", f"runsheet={str(self.runsheet)}",
                            "-meta", f"sequencing_dir={str(self.rundir)}",
                            "staging-snaketopia", str(self.test_configfile)]
        test_command = monitor_run.get_nomad_command(test_params)
        assert expected_command == test_command

    def test_override_test_param(self):
        test_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                             runsheet_path = self.runsheet,
                                             sample_sheet = pd.DataFrame(),
                                             configfile = self.test_configfile,
                                             active_config = self.test_config, outdir = self.outdir,
                                             test_run = False)
        expected_command = ["nomad", "job", "dispatch",
                            "-meta", f"bactopia_names={str(self.run_params.file_of_filenames)}",
                            "-meta", f'outdir={str(self.outdir)}',
                            "-meta", f"outdir_name={self.outdir.name}",
                            "-meta", 'node_dir=None',
                            "-meta", "use_lis=1",
                            "-meta", f"runsheet={str(self.runsheet)}",
                            "-meta", f"sequencing_dir={str(self.rundir)}",
                            "prod-snaketopia", str(self.test_configfile)]
        test_command = monitor_run.get_nomad_command(test_params)
        assert expected_command == test_command

    def test_catch_missing_files(self):
        fail_path = pathlib.Path("no/such/path")
        fail_path_existing = (pathlib.Path(__file__).parent / "data" / "monitor_run"
                              / "test_fail_dir")
        no_fofn_message = ("Could not find file of filenames at"
                           f" {fail_path_existing}/ILM_Run0000_Y20220101_XYZ.tsv")
        #no_runsheet_message = "Could not find runsheet at no/such/path"
        no_rundir_message = "Could not find run directory at no/such/path"

        fail_params_no_fofn = monitor_run.IsolateRun(rundir_path = self.rundir,
                                                     runsheet_path = self.runsheet,
                                                     sample_sheet = pd.DataFrame(),
                                                     configfile = self.test_configfile,
                                                     active_config = self.test_config,
                                                     outdir = fail_path_existing)

        with pytest.raises(FileNotFoundError, match=re.escape(no_fofn_message)):
            monitor_run.get_nomad_command(fail_params_no_fofn)
        #  IsolateRun does its own check now
        # fail_params_no_runsheet = monitor_run.IsolateRun(rundir_path = self.rundir,
        #                                                  runsheet_path = fail_path,
        #                                                  sample_sheet = pd.DataFrame(),
        #                                                  configfile = self.test_configfile,
        #                                                  active_config = self.test_config,
        #                                                  outdir = self.outdir)
        #
        # with pytest.raises(FileNotFoundError, match=re.escape(no_runsheet_message)):
        #     monitor_run.get_start_command(fail_params_no_runsheet)
        fail_params_no_rundir = monitor_run.IsolateRun(rundir_path = fail_path,
                                                       runsheet_path = self.runsheet,
                                                       sample_sheet = pd.DataFrame(),
                                                       configfile = self.test_configfile,
                                                       active_config = self.test_config,
                                                       outdir = self.outdir)
        with pytest.raises(FileNotFoundError, match=re.escape(no_rundir_message)):
            monitor_run.get_nomad_command(fail_params_no_rundir)


class TestStartGenericRun(unittest.TestCase):
    outdir = pathlib.Path(__file__).parent / "data" / "monitor_run" / "test_nanopore_outdir"
    test_configfile = pathlib.Path("/path/to/config.yaml")
    file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "test_nanopore_runsheet.xlsx"
    rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir" / "test1"
    test_config = {"debug": True,
                   "sequencing_mode": "nanopore",
                   "run_on": "nomad",
                   "lab_info_system": {"use_lis_features": True,
                                       "lis_report": str(pathlib.Path(__file__).parent
                                                         / "data"
                                                         / "utilities_test"
                                                         / "mads_for_sheet_prep.txt"),
                                       "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "path/to/output",
                             "metadata_database": "path/to/metadata",
                             "kraken_db": "path/to/kraken/db.gz",
                             "kraken_outdir": "path/to/kraken_output",
                             "gtdb_data": "path/to/gtdb.gz",
                             "gtdb_outdir": "path/to/gtdb/outdir",
                             "mash_data": "path/to/mash_data.xz",
                             "mash_outdir": "path/to/mash/outdir"},
                   "input_names": (pathlib.Path(__file__).parent / "data"
                                   / "localization" / "input_da.yaml"),
                   "cores": 128
                   }

    def test_fail_invalid_mode(self):
        """Fail if an invalid pipeline run mode is specified."""
        bad_config = self.test_config.copy()
        bad_config["run_on"] = "nextflow"
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                            runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = bad_config, outdir = self.outdir)
        error_msg = "Invalid run mode nextflow"
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            monitor_run.start_run_by_mode(run_params, dry_run = True)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_dryrun_nomad(self):
        """Successfully print a Nomad start command"""
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                            runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = self.test_config, outdir = self.outdir)
        expected_command = ['echo', (f'"nomad job dispatch '
                                     f'-meta bactopia_names={str(self.file_of_filenames)} '
                                     f'-meta outdir={str(self.outdir)} '
                                     f'-meta outdir_name={self.outdir.name} '
                                     '-meta node_dir=None '
                                     '-meta use_lis=1 '
                                     f'-meta runsheet={str(self.runsheet)} '
                                     f'-meta sequencing_dir={str(self.rundir)} '
                                     f'staging-snaketopia {str(self.test_configfile)}"')]
        start_msg = "Running analysis pipeline"
        nomad_msg = "Starting pipeline in nomad mode."
        with self._caplog.at_level(level = "INFO", logger = "launch_run"):
            test_command = monitor_run.start_run_by_mode(run_params, dry_run = True).args
            assert ("launch_run", logging.INFO, start_msg) in self._caplog.record_tuples
            assert ("launch_run", logging.INFO, nomad_msg) in self._caplog.record_tuples
        assert expected_command == test_command

    def test_success_dryrun_docker(self):
        """Successfully print a Docker start command"""
        docker_config = self.test_config.copy()
        docker_config["run_on"] = "local"
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                            runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = docker_config, outdir = self.outdir)
        docker_command = " ".join(["docker", "run",
                                   # mount all relevant files and dirs
                                   "-v", f"{str(self.runsheet)}:{str(self.runsheet)}",
                                   "-v", f"{str(self.outdir)}:{str(self.outdir)}",
                                   "-v", f"{self.test_configfile}:"
                                         f"{self.test_configfile}",
                                   "-v", f"{self.file_of_filenames}:{self.file_of_filenames}",
                                   "-v", f"{self.rundir}:{self.rundir}",
                                   "-v", f"{self.test_config['input_names']}:"
                                         f"{self.test_config['input_names']}",
                                   "-v", "path/to/output:path/to/output",
                                   "-v", "path/to/metadata:path/to/metadata",
                                   "-v", "path/to/kraken/db.gz:path/to/kraken/db.gz",
                                   "-v", "path/to/kraken_output:path/to/kraken_output",
                                   "-v", "path/to/gtdb.gz:path/to/gtdb.gz",
                                   "-v", "path/to/gtdb/outdir:path/to/gtdb/outdir",
                                   "-v", "path/to/mash_data.xz:path/to/mash_data.xz",
                                   "-v", "path/to/mash/outdir:path/to/mash/outdir",
                                   "-v", f"{self.test_config['lab_info_system']['lis_report']}:"
                                         f"{self.test_config['lab_info_system']['lis_report']}",
                                   "-v", f"{self.test_config['lab_info_system']['bacteria_codes']}:"
                                         f"{self.test_config['lab_info_system']['bacteria_codes']}",
                                   "rsyd-basic-docker",
                                   "--cores", "128",
                                   "--config",
                                   f'runsheet="{str(self.runsheet)}"',
                                   f'rundir="{str(self.rundir)}"',
                                   f'outdir="{str(self.outdir)}"',
                                   f'config_path="{self.test_configfile}"',
                                   f'input_samples="{self.file_of_filenames}"',
                                   "--configfile", str(self.test_configfile),
                                   "-r",
                                   "--rerun-incomplete",
                                   "--rerun-triggers", "input",
                                   "--shadow-prefix", "/bactopia_datasets"])
        expected_command = ['echo', f'"{docker_command}"']
        docker_msg = "Starting pipeline in local mode."
        with self._caplog.at_level(level = "INFO", logger = "launch_run"):
            test_command = monitor_run.start_run_by_mode(run_params, dry_run = True).args
            assert ("launch_run", logging.INFO, docker_msg) in self._caplog.record_tuples
        assert expected_command == test_command

    # we don't want to run the actual nomad command when testing...
    @mock.patch(f'{monitor_run.__name__}.get_nomad_command',
                wraps = monitor_run.get_nomad_command)
    def test_success_real_run(self, mock_command):
        """Successfully run the output of the pipeline command builder."""
        mock_command.return_value = ["echo", "Hello"]
        expected_command = ["echo", "Hello"]
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                            runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = self.test_config, outdir = self.outdir)
        test_command = monitor_run.start_run_by_mode(run_params).args
        assert test_command == expected_command


class TestWaitForFile(unittest.TestCase):
    outdir = pathlib.Path(__file__).parent /"data"/"monitor_run"/"test_nanopore_outdir"
    test_configfile = pathlib.Path("/path/to/config.yaml")
    file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "test_nanopore_runsheet.xlsx"
    rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir" / "test1"
    test_config = {"debug": True,
                   "sequencing_mode": "nanopore",
                   "run_on": "nomad",
                   "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                    "paths": {"output_base_path": "path/to/output",
                            "metadata_database": "path/to/metadata",
                            "kraken_db": "path/to/kraken/db.gz",
                            "kraken_outdir": "path/to/kraken_output",
                            "gtdb_data": "path/to/gtdb.gz",
                            "gtdb_outdir": "path/to/gtdb/outdir",
                            "mash_data": "path/to/mash_data.xz",
                            "mash_outdir": "path/to/mash/outdir"},
                   "input_names": (pathlib.Path(__file__).parent/ "data"
                                       / "localization" / "input_da.yaml"),
                   "cores": 128
                   }
    run_params = monitor_run.IsolateRun(rundir_path = rundir, runsheet_path = runsheet,
                                        sample_sheet = pd.DataFrame(), configfile = test_configfile,
                                        active_config = test_config, outdir = outdir)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_fail_illumina(self):
        """Alert when using a sequencing mode that isn't supported yet."""
        test_config = self.test_config.copy()
        test_config['sequencing_mode'] = "illumina"
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "runsheet_experiment_name.xlsx"
        ilm_run = monitor_run.IsolateRun(rundir_path = self.rundir, runsheet_path = runsheet,
                                         sample_sheet = pd.DataFrame(),
                                         configfile = self.test_configfile,
                                         active_config = test_config, outdir = self.outdir)
        error_msg = "Delayed start is currently only implemented for Nanopore runs."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            monitor_run.start_on_file_found(ilm_run, "final_summary*.txt", watch_interval = 1,
                                            watch_timeout = 2)

    def test_timeout(self):
        """Stop monitoring when the timeout has been reached."""
        error_msg = ("No file matching pattern test_summary*.txt "
                     f"found in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'}")
        time_log = "Waiting for sequencing to finish..."
        log_error = ("No file matching pattern test_summary*.txt "
                     f"found in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'} "
                     "after 0.0 hours.")
        with (pytest.raises(FileNotFoundError, match=re.escape(error_msg)),
              self._caplog.at_level(level="INFO", logger="launch_run")):
            monitor_run.start_on_file_found(self.run_params, "test_summary*.txt",
                                            watch_interval = 1, watch_timeout = 3, log_interval = 1)
        assert ("launch_run", logging.ERROR, log_error) in self._caplog.record_tuples
        timed_logs = [log_record for log_record in self._caplog.record_tuples
                      if log_record == ("launch_run", logging.INFO, time_log)]
        print(self._caplog.record_tuples)
        assert len(timed_logs) == 3

    def test_fail_parent_dir_not_found(self):
        """Don't start monitoring if the parent directory does not exist."""
        rundir = pathlib.Path(__file__).parent / "data" / "monitor_run" / "no_such_dir"
        test_run = monitor_run.IsolateRun(rundir_path = rundir, runsheet_path = self.runsheet,
                                          sample_sheet = pd.DataFrame(),
                                          configfile = self.test_configfile,
                                          active_config = self.test_config, outdir = self.outdir)
        error_msg = f"Parent directory {test_run.rundir_path} does not exist."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            monitor_run.start_on_file_found(test_run, "final_summary*.txt",
                                            watch_interval = 1,
                                            watch_timeout = 5, dry_run = True)

    def test_fail_invalid_mode(self):
        """Fail if the pipeline mode is invalid."""
        test_config = {"debug": True,
                       "sequencing_mode": "nanopore",
                       "run_on": "nextflow",
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "path/to/output",
                                 "metadata_database": "path/to/metadata",
                                 "kraken_db": "path/to/kraken/db.gz",
                                 "kraken_outdir": "path/to/kraken_output",
                                 "gtdb_data": "path/to/gtdb.gz",
                                 "gtdb_outdir": "path/to/gtdb/outdir",
                                 "mash_data": "path/to/mash_data.xz",
                                 "mash_outdir": "path/to/mash/outdir"},
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml"),
                       "cores": 128
                       }
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                            runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = test_config, outdir = self.outdir)
        error_msg = "Invalid run mode nextflow"
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            monitor_run.start_on_file_found(run_params, "final_summary*.txt",
                                            watch_interval = 1, watch_timeout = 2, dry_run = True)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_success_dryrun(self):
        """Successfully print the nomad start command in dry run mode."""
        expected_command = ['echo', (f'"nomad job dispatch '
                                     f'-meta bactopia_names={str(self.file_of_filenames)} '
                                     f'-meta outdir={str(self.outdir)} '
                                     f'-meta outdir_name={self.outdir.name} '
                                     '-meta node_dir=None '
                                     '-meta use_lis=1 '
                                     f'-meta runsheet={str(self.runsheet)} '
                                     f'-meta sequencing_dir={str(self.rundir)} '
                                     f'staging-snaketopia {str(self.test_configfile)}"')]
        wait_msg = "Pipeline is now waiting for sequencing to finish..."
        expected_log = ("Found final_summary*.txt "
                        f"in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'} "
                        "after 0 seconds.")
        nomad_msg = "Starting pipeline in nomad mode."
        with self._caplog.at_level(level="INFO", logger="launch_run"):
            test_command = monitor_run.start_on_file_found(self.run_params, "final_summary*.txt",
                                                           dry_run = True, watch_interval = 1,
                                                           watch_timeout = 2).args
            assert ("launch_run", logging.INFO, wait_msg) in self._caplog.record_tuples
            assert ("launch_run", logging.INFO, expected_log) in self._caplog.record_tuples
            assert ("launch_run", logging.INFO, nomad_msg) in self._caplog.record_tuples
        assert test_command == expected_command

    def test_success_dryrun_en(self):
        """Successfully print the nomad start command in dry run mode
        with English-language data."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        test_config = {"debug": True,
                       "sequencing_mode": "nanopore",
                       "run_on": "nomad",
                       "lab_info_system": {"use_lis_features": True},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "/path/to/output"},
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_en.yaml")
                       }
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir, runsheet_path = runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = test_config, outdir = self.outdir)
        expected_command = ['echo', (f'"nomad job dispatch '
                                     f'-meta bactopia_names={str(self.file_of_filenames)} '
                                     f'-meta outdir={str(self.outdir)} '
                                     f'-meta outdir_name={self.outdir.name} '
                                     '-meta node_dir=None '
                                     '-meta use_lis=1 '
                                     f'-meta runsheet={str(runsheet)} '
                                     f'-meta sequencing_dir={str(self.rundir)} '
                                     f'staging-snaketopia {str(self.test_configfile)}"')]
        expected_log = ("Found final_summary*.txt "
                        f"in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'} after 0 seconds.")
        with self._caplog.at_level(level="INFO", logger="launch_run"):
            test_command = monitor_run.start_on_file_found(run_params, "final_summary*.txt",
                                                           dry_run = True, watch_interval = 1,
                                                           watch_timeout = 2).args
            assert ("launch_run", logging.INFO, expected_log) in self._caplog.record_tuples
        assert test_command == expected_command

    def test_success_docker(self):
        """Successfully print the docker start command in dry run mode."""
        test_config = {"debug": True,
                       "sequencing_mode": "nanopore",
                       "run_on": "local",
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                                 / "data"
                                                                 / "utilities_test"
                                                                 / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "path/to/output",
                                 "metadata_database": "path/to/metadata",
                                 "kraken_db": "path/to/kraken/db.gz",
                                 "kraken_outdir": "path/to/kraken_output",
                                 "gtdb_data": "path/to/gtdb.gz",
                                 "gtdb_outdir": "path/to/gtdb/outdir",
                                 "mash_data": "path/to/mash_data.xz",
                                 "mash_outdir": "path/to/mash/outdir"},
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml"),
                       "cores": 128
                       }
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir, runsheet_path = self.runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = test_config, outdir = self.outdir)
        docker_command = " ".join(["docker", "run",
                                   # mount all relevant files and dirs
                                   "-v", f"{str(self.runsheet)}:{str(self.runsheet)}",
                                   "-v", f"{str(self.outdir)}:{str(self.outdir)}",
                                   "-v", f"{self.test_configfile}:"
                                         f"{self.test_configfile}",
                                   "-v", f"{self.file_of_filenames}:{self.file_of_filenames}",
                                   "-v", f"{self.rundir}:{self.rundir}",
                                   "-v", f"{test_config['input_names']}:"
                                         f"{test_config['input_names']}",
                                   "-v", "path/to/output:path/to/output",
                                   "-v", "path/to/metadata:path/to/metadata",
                                   "-v", "path/to/kraken/db.gz:path/to/kraken/db.gz",
                                   "-v", "path/to/kraken_output:path/to/kraken_output",
                                   "-v", "path/to/gtdb.gz:path/to/gtdb.gz",
                                   "-v", "path/to/gtdb/outdir:path/to/gtdb/outdir",
                                   "-v", "path/to/mash_data.xz:path/to/mash_data.xz",
                                   "-v", "path/to/mash/outdir:path/to/mash/outdir",
                                   "-v", f"{test_config['lab_info_system']['lis_report']}:"
                                         f"{test_config['lab_info_system']['lis_report']}",
                                   "-v", f"{test_config['lab_info_system']['bacteria_codes']}:"
                                         f"{test_config['lab_info_system']['bacteria_codes']}",
                                   "rsyd-basic-docker",
                                   "--cores", "128",
                                   "--config",
                                   f'runsheet="{str(self.runsheet)}"',
                                   f'rundir="{str(self.rundir)}"',
                                   f'outdir="{str(self.outdir)}"',
                                   f'config_path="{self.test_configfile}"',
                                   f'input_samples="{self.file_of_filenames}"',
                                   "--configfile", str(self.test_configfile),
                                   "-r",
                                   "--rerun-incomplete",
                                   "--rerun-triggers", "input",
                                   "--shadow-prefix", "/bactopia_datasets"])
        expected_command = ['echo', f'"{docker_command}"']
        expected_log = ("Found final_summary*.txt "
                        f"in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'} "
                        "after 0 seconds.")
        docker_msg = "Starting pipeline in local mode."
        with self._caplog.at_level(level = "INFO", logger = "launch_run"):
            test_command = monitor_run.start_on_file_found(run_params, "final_summary*.txt",
                                                           dry_run = True, watch_interval = 1,
                                                           watch_timeout = 2).args
            assert ("launch_run", logging.INFO, expected_log) in self._caplog.record_tuples
            assert ("launch_run", logging.INFO, docker_msg) in self._caplog.record_tuples

        assert test_command == expected_command

    def test_success_dryrun_docker_en(self):
        """Successfully print the Docker command with non-default input names."""
        runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                   / "test_nanopore_runsheet_en.xlsx"
        test_config = {"debug": True,
                       "sequencing_mode": "nanopore",
                       "run_on": "local",
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                       "sample_number_settings": {'sample_number_format':
                                                      '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                                  'sample_numbers_in': 'letter',
                                                  'sample_numbers_out': 'number',
                                                  "sample_numbers_report": "letter",
                                                  'number_to_letter': {"30": "B", "10": "D",
                                                                       "11": "F", "50": "T"},
                                                  "format_in_sheet":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  "format_output":
                                                      r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                                  },
                       "temp_data_dir": None,
                       "seq_run_duration_hours": 1,
                       "paths": {"output_base_path": "path/to/output",
                            "metadata_database": "path/to/metadata",
                            "kraken_db": "path/to/kraken/db.gz",
                            "kraken_outdir": "path/to/kraken_output",
                            "gtdb_data": "path/to/gtdb.gz",
                            "gtdb_outdir": "path/to/gtdb/outdir",
                            "mash_data": "path/to/mash_data.xz",
                            "mash_outdir": "path/to/mash/outdir"},
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_en.yaml"),
                       "cores": 128
                       }
        run_params = monitor_run.IsolateRun(rundir_path = self.rundir, runsheet_path = runsheet,
                                            sample_sheet = pd.DataFrame(),
                                            configfile = self.test_configfile,
                                            active_config = test_config, outdir = self.outdir)
        docker_command = " ".join(["docker", "run",
                                   # mount all relevant files and dirs
                                   "-v", f"{str(runsheet)}:{str(runsheet)}",
                                   "-v", f"{str(self.outdir)}:{str(self.outdir)}",
                                   "-v", f"{self.test_configfile}:"
                                         f"{self.test_configfile}",
                                   "-v", f"{self.file_of_filenames}:{self.file_of_filenames}",
                                   "-v", f"{self.rundir}:{self.rundir}",
                                   "-v", f"{test_config['input_names']}:"
                                         f"{test_config['input_names']}",
                                   "-v", "path/to/output:path/to/output",
                                   "-v", "path/to/metadata:path/to/metadata",
                                   "-v", "path/to/kraken/db.gz:path/to/kraken/db.gz",
                                   "-v", "path/to/kraken_output:path/to/kraken_output",
                                   "-v", "path/to/gtdb.gz:path/to/gtdb.gz",
                                   "-v", "path/to/gtdb/outdir:path/to/gtdb/outdir",
                                   "-v", "path/to/mash_data.xz:path/to/mash_data.xz",
                                   "-v", "path/to/mash/outdir:path/to/mash/outdir",
                                   "-v", f"{test_config['lab_info_system']['lis_report']}:"
                                         f"{test_config['lab_info_system']['lis_report']}",
                                   "-v", f"{test_config['lab_info_system']['bacteria_codes']}:"
                                         f"{test_config['lab_info_system']['bacteria_codes']}",
                                   "rsyd-basic-docker",
                                   "--cores", "128",
                                   "--config",
                                   f'runsheet="{str(runsheet)}"',
                                   f'rundir="{str(self.rundir)}"',
                                   f'outdir="{str(self.outdir)}"',
                                   f'config_path="{self.test_configfile}"',
                                   f'input_samples="{self.file_of_filenames}"',
                                   "--configfile", str(self.test_configfile),
                                   "-r",
                                   "--rerun-incomplete",
                                   "--rerun-triggers", "input",
                                   "--shadow-prefix", "/bactopia_datasets"])
        expected_command = ['echo', f'"{docker_command}"']
        expected_log = ("Found final_summary*.txt "
                        f"in {self.run_params.rundir_path / 'rawdata' / 'test_subdir'} "
                        "after 0 seconds.")
        docker_msg = "Starting pipeline in local mode."
        with self._caplog.at_level(level = "INFO", logger = "launch_run"):
            test_command = monitor_run.start_on_file_found(run_params, "final_summary*.txt",
                                                           dry_run = True, watch_interval = 1,
                                                           watch_timeout = 2).args
            assert ("launch_run", logging.INFO, expected_log) in self._caplog.record_tuples
            assert ("launch_run", logging.INFO, docker_msg) in self._caplog.record_tuples

        assert test_command == expected_command



    # we don't want to run the actual nomad command when testing...
    @mock.patch(f'{monitor_run.__name__}.get_nomad_command',
                wraps = monitor_run.get_nomad_command)
    def test_success_real_run(self, mock_command):
        """Successfully run the output of the pipeline command builder."""
        mock_command.return_value = ["echo", "Hello"]
        expected_command = ["echo", "Hello"]
        test_command = monitor_run.start_on_file_found(self.run_params, "final_summary*.txt",
                                                       dry_run = False, watch_interval = 1,
                                                       watch_timeout = 2).args
        assert test_command == expected_command


class TestGetDockerCommand(unittest.TestCase):
    outdir = pathlib.Path(__file__).parent / "data" / "monitor_run" / "test_nanopore_outdir"
    test_configfile = pathlib.Path("/path/to/config.yaml")
    file_of_filenames = outdir / "ONT_RUN0000_Y20990101_XYZ.tsv"
    runsheet = pathlib.Path(__file__).parent / "data" / "utilities_test" \
               / "test_nanopore_runsheet.xlsx"
    rundir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "miniondir" / "test1"
    test_config = {"debug": True,
                   "sequencing_mode": "nanopore",
                   "lab_info_system": {"use_lis_features": True,
                                           "lis_report": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "mads_for_sheet_prep.txt"),
                                           "bacteria_codes": str(pathlib.Path(__file__).parent
                                                             / "data"
                                                             / "utilities_test"
                                                             / "bacteria_codes_for_prep.csv")},
                   "sample_number_settings": {'sample_number_format':
                                                  '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                              'sample_numbers_in': 'letter',
                                              'sample_numbers_out': 'number',
                                              "sample_numbers_report": "letter",
                                              'number_to_letter': {"30": "B", "10": "D",
                                                                   "11": "F", "50": "T"},
                                              "format_in_sheet":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_in_lis": r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              "format_output":
                                                  r'(?P<sample_type>[BDTF])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                              },
                   "temp_data_dir": None,
                   "seq_run_duration_hours": 1,
                   "paths": {"output_base_path": "path/to/output/data",
                            "metadata_database": "path/to/metadata",
                            "kraken_db": "path/to/kraken/db.gz",
                            "kraken_outdir": "path/to/kraken_output",
                            "gtdb_data": "path/to/gtdb.gz",
                            "gtdb_outdir": "path/to/gtdb/outdir",
                            "mash_data": "path/to/mash_data.xz",
                            "mash_outdir": "path/to/mash/outdir"},
                   "input_names": (pathlib.Path(__file__).parent / "data"
                                   / "localization" / "input_da.yaml"),
                   "cores": 128
                   }
    run_params = monitor_run.IsolateRun(rundir_path = rundir, runsheet_path = runsheet,
                                        sample_sheet = pd.DataFrame(), configfile = test_configfile,
                                        active_config = test_config, outdir = outdir)

    def test_run_test_job(self):
        """Run a basic test job."""
        expected_command = ["docker", "run",
                            # mount all relevant files and dirs
                            "-v", f"{str(self.runsheet)}:{str(self.runsheet)}",
                            "-v", f"{str(self.outdir)}:{str(self.outdir)}",
                            "-v", f"{self.test_configfile}:"
                                  f"{self.test_configfile}",
                            "-v", f"{self.file_of_filenames}:{self.file_of_filenames}",
                            "-v", f"{self.rundir}:{self.rundir}",
                            "-v", f"{self.test_config['input_names']}:"
                                  f"{self.test_config['input_names']}",
                            "-v", "path/to/output/data:path/to/output/data",
                            "-v", "path/to/metadata:path/to/metadata",
                            "-v", "path/to/kraken/db.gz:path/to/kraken/db.gz",
                            "-v", "path/to/kraken_output:path/to/kraken_output",
                            "-v", "path/to/gtdb.gz:path/to/gtdb.gz",
                            "-v", "path/to/gtdb/outdir:path/to/gtdb/outdir",
                            "-v", "path/to/mash_data.xz:path/to/mash_data.xz",
                            "-v", "path/to/mash/outdir:path/to/mash/outdir",
                            "-v",  f"{self.test_config['lab_info_system']['lis_report']}:"
                                   f"{self.test_config['lab_info_system']['lis_report']}",
                            "-v", f"{self.test_config['lab_info_system']['bacteria_codes']}:"
                                  f"{self.test_config['lab_info_system']['bacteria_codes']}",
                            "rsyd-basic-docker",
                            "--cores", "128",
                            "--config",
                            f'runsheet="{str(self.runsheet)}"',
                            f'rundir="{str(self.rundir)}"',
                            f'outdir="{str(self.outdir)}"',
                            f'config_path="{self.test_configfile}"',
                            f'input_samples="{self.file_of_filenames}"',
                            "--configfile", str(self.test_configfile),
                            "-r",
                            "--rerun-incomplete",
                            "--rerun-triggers", "input",
                            "--shadow-prefix", "/bactopia_datasets",
                            ]
        test_command = monitor_run.get_docker_command(self.run_params)
        assert expected_command == test_command

    def test_override_config(self):
        """Check that config can be overridden (so that e.g. LIS report isn't used)"""
        test_config = {"temp_data_dir": "/path/to/node/dir",
                       "lab_info_system": {"use_lis_features": False},
                       "paths":
                           {"output_base_path": "path/to/output/data",
                            "metadata_database": "path/to/metadata",
                            "kraken_db": "path/to/kraken/db.gz",
                            "kraken_outdir": "path/to/kraken_output",
                            "gtdb_data": "path/to/gtdb.gz",
                            "gtdb_outdir": "path/to/gtdb/outdir",
                            "mash_data": "path/to/mash_data.xz",
                            "mash_outdir": "path/to/mash/outdir"},
                       "cores": "128",
                       "seq_run_duration_hours": 1,
                       "check_interval": 30,
                       "sequencing_mode": "nanopore",
                       "input_names": (pathlib.Path(__file__).parent / "data"
                                       / "localization" / "input_da.yaml"),
                       }
        test_params = monitor_run.IsolateRun(rundir_path = self.rundir,
                                             runsheet_path = self.runsheet,
                                             sample_sheet = pd.DataFrame(),
                                             configfile = self.test_configfile,
                                             active_config = test_config, outdir = self.outdir,
                                             test_run = False)
        expected_command = ["docker", "run",
                            # mount all relevant files and dirs
                            "-v", f"{str(self.runsheet)}:{str(self.runsheet)}",
                            "-v", f"{str(self.outdir)}:{str(self.outdir)}",
                            "-v", f"{self.test_configfile}:"
                                  f"{self.test_configfile}",
                            "-v", f"{self.file_of_filenames}:{self.file_of_filenames}",
                            "-v", f"{self.rundir}:{self.rundir}",
                            "-v", f"{self.test_config['input_names']}:"
                                  f"{self.test_config['input_names']}",
                            "-v", "path/to/output/data:path/to/output/data",
                            "-v", "path/to/metadata:path/to/metadata",
                            "-v", "path/to/kraken/db.gz:path/to/kraken/db.gz",
                            "-v", "path/to/kraken_output:path/to/kraken_output",
                            "-v", "path/to/gtdb.gz:path/to/gtdb.gz",
                            "-v", "path/to/gtdb/outdir:path/to/gtdb/outdir",
                            "-v", "path/to/mash_data.xz:path/to/mash_data.xz",
                            "-v", "path/to/mash/outdir:path/to/mash/outdir",
                            "rsyd-basic-docker",
                            "--cores", "128",
                            "--config",
                            f'runsheet="{str(self.runsheet)}"',
                            f'rundir="{str(self.rundir)}"',
                            f'outdir="{str(self.outdir)}"',
                            f'config_path="{self.test_configfile}"',
                            f'input_samples="{self.file_of_filenames}"',
                            "--configfile", str(self.test_configfile),
                            "-r",
                            "--rerun-incomplete",
                            "--rerun-triggers", "input",
                            "--shadow-prefix", "/bactopia_datasets",
                            ]
        test_command = monitor_run.get_docker_command(test_params)
        assert expected_command == test_command

    def test_catch_missing_files(self):
        fail_path = pathlib.Path("no/such/path")
        fail_path_existing = (pathlib.Path(__file__).parent / "data" / "monitor_run"
                              / "test_fail_dir")
        no_fofn_message = ("Could not find file of filenames at"
                           f" {fail_path_existing}/ONT_RUN0000_Y20990101_XYZ.tsv")
        # no_runsheet_message = "Could not find runsheet at no/such/path"
        no_rundir_message = "Could not find run directory at no/such/path"

        fail_params_no_fofn = monitor_run.IsolateRun(rundir_path = self.rundir,
                                                     runsheet_path = self.runsheet,
                                                     sample_sheet = pd.DataFrame(),
                                                     configfile = self.test_configfile,
                                                     active_config = self.test_config,
                                                     outdir = fail_path_existing)

        with pytest.raises(FileNotFoundError, match = re.escape(no_fofn_message)):
            monitor_run.get_docker_command(fail_params_no_fofn)
        #  IsolateRun does its own check now
        # fail_params_no_runsheet = monitor_run.IsolateRun(rundir_path = self.rundir,
        #                                                  runsheet_path = fail_path,
        #                                                  sample_sheet = pd.DataFrame(),
        #                                                  configfile = self.test_configfile,
        #                                                  active_config = self.test_config,
        #                                                  outdir = self.outdir)
        #
        # with pytest.raises(FileNotFoundError, match=re.escape(no_runsheet_message)):
        #     monitor_run.get_start_command(fail_params_no_runsheet)
        fail_params_no_rundir = monitor_run.IsolateRun(rundir_path = fail_path,
                                                       runsheet_path = self.runsheet,
                                                       sample_sheet = pd.DataFrame(),
                                                       configfile = self.test_configfile,
                                                       active_config = self.test_config,
                                                       outdir = self.outdir)
        with pytest.raises(FileNotFoundError, match = re.escape(no_rundir_message)):
            monitor_run.get_docker_command(fail_params_no_rundir)
