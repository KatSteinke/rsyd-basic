import logging
import pathlib
import re
import unittest
from datetime import datetime

from unittest.mock import patch

import pandas as pd
import pytest
import sqlalchemy
from sqlalchemy.future import create_engine
from sqlalchemy.orm import Session
from sqlalchemy_utils import create_view

import get_lis_report
import input_names

LIS_NAMES_EN = input_names.LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                                        isolate_number = "BACT_NR",
                                        bacteria_code = "LOCAL_BACT_CODE",
                                        bacteria_text = "LOCAL_BACT_TEXT")


class TestFetchFromDB(unittest.TestCase):
    dialect = "sqlite+pysqlite"
    database = ""

    @classmethod
    def setUpClass(cls) -> None:
        """Set up database connection."""
        metadata = sqlalchemy.MetaData()
        # create test table
        test_db = sqlalchemy.Table("test_db", metadata,
                                   sqlalchemy.Column("id", sqlalchemy.Integer,
                                                     primary_key = True),
                                   sqlalchemy.Column("PRV_NR", sqlalchemy.Integer),
                                   sqlalchemy.Column("P_TYPE", sqlalchemy.String),
                                   sqlalchemy.Column("BAKT_NR", sqlalchemy.Integer),
                                   sqlalchemy.Column("LOKAL_BAKT_KODE", sqlalchemy.Integer),
                                   sqlalchemy.Column("LOKAL_BAKT_TEKST", sqlalchemy.String),
                                   sqlalchemy.Column("WGS_ISOLAT", sqlalchemy.String))
        # test_db_en = sqlalchemy.Table("test_db_en", metadata,
        #                            sqlalchemy.Column("id", sqlalchemy.Integer,
        #                                              primary_key = True),
        #                            sqlalchemy.Column("SAMPLE_NR", sqlalchemy.Integer),
        #                            sqlalchemy.Column("SAMPLETYPE", sqlalchemy.String),
        #                            sqlalchemy.Column("BACT_NR", sqlalchemy.Integer),
        #                            sqlalchemy.Column("LOCAL_BACT_CODE", sqlalchemy.Integer),
        #                            sqlalchemy.Column("LOCAL_BACT_TEXT", sqlalchemy.String),
        #                            sqlalchemy.Column("WGS_ISOLATE", sqlalchemy.String),
        #                               schema = "table_en")
        wgs_only = sqlalchemy.select(test_db).where(test_db.c.WGS_ISOLAT == "WGS")
        create_view("wgs_only", wgs_only, metadata)
        global engine
        engine = create_engine("sqlite+pysqlite://", echo = True, future = True)
        metadata.create_all(engine)
        global session
        session = Session(engine)
        # set up table
        session.execute(test_db.insert(), [{"PRV_NR": 99123456, "P_TYPE": "F",
                                            "BAKT_NR": 1, "LOKAL_BAKT_KODE": 888,
                                            "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke",
                                            "WGS_ISOLAT": "WGS"},
                                           {"PRV_NR": 99123456, "P_TYPE": "F",
                                            "BAKT_NR": 2, "LOKAL_BAKT_KODE": 888,
                                            "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke",
                                            "WGS_ISOLAT": ""}
                                           ])
        session.commit()

    @classmethod
    def tearDownClass(cls) -> None:
        """Disconnect from database."""
        session.close()

    def test_fail_filter_value_no_col(self):
        """Fail if a value but no filter column is given"""
        test_cols = ["PRV_NR", "P_TYPE", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST"]
        error_msg = "A filter value was given but no column to filter by was specified."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, None, "WGS",
                                                None)

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_missing_col(self):
        """Fail if a column is not found in the database."""
        test_cols = ["PRV_NR", "P_TYPE", "LOKAL_BAKT_KODE", "LOCAL_BACT_TEXT", "WGS_ISOLAT"]
        log_msg = ("Original column(s) ['LOCAL_BACT_TEXT'] not found. Checking for "
                   f"{sorted([colname.lower() for colname in test_cols])}")
        error_msg = (f"Column(s) {sorted([colname.lower() for colname in test_cols])} not found"
                     " in table test_db")
        with (pytest.raises(KeyError, match = re.escape(error_msg)),
              self._caplog.at_level(level = "INFO", logger = "get_lis")):
            get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, "WGS_ISOLAT", "WGS",
                                                None)
        assert ("get_lis", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_missing_cols(self):
        """Fail if multiple columns are not found in the database."""
        test_cols = ["PRV_NR", "P_TYPE", "LOCAL_BACT_CODE", "LOCAL_BACT_TEXT", "WGS_ISOLAT"]
        log_msg = ("Original column(s) ['LOCAL_BACT_CODE', 'LOCAL_BACT_TEXT'] "
                   "not found. Checking for "
                   f"{sorted([colname.lower() for colname in test_cols])}")
        error_msg = (f"Column(s) {sorted([colname.lower() for colname in test_cols])} not found "
                     f"in table test_db")
        with (pytest.raises(KeyError, match=re.escape(error_msg)),
              self._caplog.at_level(level = "INFO", logger = "get_lis")):
            get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, "WGS_ISOLAT", "WGS",
                                                None)
        assert ("get_lis", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_fail_bad_table(self):
        """Fail if the table is missing from the database."""
        test_cols = ["PRV_NR", "P_TYPE", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST", "WGS_ISOLAT"]
        error_msg = "Table test_table not found in database"
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            get_lis_report.get_lis_data_from_db(engine, "test_table", test_cols, "WGS_ISOLAT",
                                                "WGS", None)

    def test_fail_filter_col_not_fetched(self):
        """Fail if the filter column wasn't retrieved from the database."""
        test_cols = ["PRV_NR", "P_TYPE", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST"]
        error_msg = ("The column to filter results by has to be one of the columns"
                     " retrieved from the database.")
        with pytest.raises(KeyError, match = re.escape(error_msg)):
            get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, "WGS_ISOLAT", "WGS",
                                                None)

    def test_success(self):
        """Fetch required data from the database."""
        test_cols = ["PRV_NR", "P_TYPE", "BAKT_NR", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST",
                     "WGS_ISOLAT"]
        expected_data = pd.DataFrame(data = {"PRV_NR": [99123456], "P_TYPE": ["F"],
                                             "BAKT_NR": [1], "LOKAL_BAKT_KODE": [888],
                                             "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke"],
                                             "WGS_ISOLAT": ["WGS"]})
        test_data = get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, "WGS_ISOLAT",
                                                        "WGS", "")
        pd.testing.assert_frame_equal(expected_data, test_data)

    def test_success_change_case(self):
        """Catch and fix errors caused by column names being in the wrong case in the database."""
        test_cols = ["prv_nr", "p_type", "bakt_nr", "lokal_bakt_kode", "lokal_bakt_tekst",
                     "wgs_isolat"]
        expected_data = pd.DataFrame(data = {"prv_nr": [99123456], "p_type": ["F"],
                                             "bakt_nr": [1], "lokal_bakt_kode": [888],
                                             "lokal_bakt_tekst": ["Undersøgt - medtages ikke"],
                                             "wgs_isolat": ["WGS"]})
        log_msg = (f"Original column(s) {sorted(test_cols)} not found. "
                   f"Checking for {sorted([colname.upper() for colname in test_cols])}")
        with self._caplog.at_level(level = "INFO", logger = "get_lis"):
            test_data = get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols,
                                                            "wgs_isolat",
                                                        "WGS", "")
            assert ("get_lis", logging.INFO, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(expected_data, test_data)

    def test_success_no_filter(self):
        """Fetch required data from the database without filtering."""
        test_cols = ["PRV_NR", "P_TYPE", "BAKT_NR", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST",
                     "WGS_ISOLAT"]
        expected_data = pd.DataFrame(data = {"PRV_NR": [99123456, 99123456], "P_TYPE": ["F", "F"],
                                             "BAKT_NR": [1, 2], "LOKAL_BAKT_KODE": [888, 888],
                                             "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke",
                                                                  "Undersøgt - medtages ikke"],
                                             "WGS_ISOLAT": ["WGS", ""]})
        test_data = get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, None,
                                                        None, None)
        pd.testing.assert_frame_equal(expected_data, test_data)

    def test_success_get_view(self):
        """Fetch required data from the database without filtering."""
        test_cols = ["PRV_NR", "P_TYPE", "BAKT_NR", "LOKAL_BAKT_KODE", "LOKAL_BAKT_TEKST",
                     "WGS_ISOLAT"]
        expected_data = pd.DataFrame(data = {"PRV_NR": [99123456], "P_TYPE": ["F"],
                                             "BAKT_NR": [1], "LOKAL_BAKT_KODE": [888],
                                             "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke"],
                                             "WGS_ISOLAT": ["WGS"]})
        test_data = get_lis_report.get_lis_data_from_db(engine, "wgs_only", test_cols, None,
                                                        None, None)
        pd.testing.assert_frame_equal(expected_data, test_data)

    #def test_success_schema(self):
    #    """Fetch required data from the database using a different schema."""
    #    test_cols = ["SAMPLE_NR", "SAMPLETYPE", "BACT_NR", "LOCAL_BACT_CODE", "LOKAL_BAKT_TEKST",
    #                 "WGS_ISOLAT"]
    #    expected_data = pd.DataFrame(data = {"PRV_NR": [99123456], "P_TYPE": ["F"],
    #                                         "BAKT_NR": [1], "LOKAL_BAKT_KODE": [888],
    #                                         "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke"],
    #                                         "WGS_ISOLAT": ["WGS"]})
    #    test_data = get_lis_report.get_lis_data_from_db(engine, "test_db", test_cols, "WGS_ISOLAT",
    #                                                    "WGS", "table_en")
    #    pd.testing.assert_frame_equal(expected_data, test_data)


class TestReadLISReport(unittest.TestCase):
    def test_success_get_lis_report(self):
        """Read the LIS report from the file."""
        test_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                     / "mads_for_sheet_prep.txt")
        test_names = input_names.LISDataNames(sample_number = "PRV_NR", sample_type = "P_TYPE",
                                              isolate_number = "BAKT_NR",
                                              bacteria_code = "LOKAL_BAKT_KODE",
                                              bacteria_text = "LOKAL_BAKT_TEKST")
        expected_data = pd.DataFrame(data={"PRV_NR": ["99123456", "99123456"],
                                           "P_TYPE": ["B", "B"],
                                           "BAKT_NR": ["1", "2"],
                                           "LOKAL_BAKT_KODE": ["1", "2"]})
        test_data = get_lis_report.read_lis_data(test_data, test_names)
        pd.testing.assert_frame_equal(expected_data, test_data)


class TestTranslateReportDF(unittest.TestCase):
    def test_translate_letter_to_number(self):
        lis_report = pd.DataFrame(data={"PRV_NR": ["99123456", "99654321"],
                                       "P_TYPE": ["F", "D"],
                                       "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["1199123456-1", "1099654321-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                        translate_from = "letter",
                                                        translate_to = "number",
                                                        from_format = re.compile(
                                                            r"(?P<sample_type>[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)"),
                                                        to_format = re.compile(
                                                            r"(?P<sample_type>[135]0|11|[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)")

                                                        )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_translate_letter_to_number_en(self):
        """Translate sample type letters to sample type numbers in a LIS report with non-default
        column names"""
        lis_report = pd.DataFrame(data = {"SAMPLENR": ["99123456", "99654321"],
                                          "SAMPLE_TYPE": ["F", "D"],
                                          "BACT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"SAMPLENR": ["99123456", "99654321"],
                                           "SAMPLE_TYPE": ["F", "D"],
                                           "BACT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["1199123456-1", "1099654321-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                        translate_from = "letter",
                                                        translate_to = "number",
                                                        from_format = re.compile(
                                                            r"(?P<sample_type>[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)"),
                                                        to_format = re.compile(
                                                            r"(?P<sample_type>[135]0|11|[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)"),
                                                        lis_data_names = LIS_NAMES_EN
                                                        )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_translate_number_to_letter(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                          "P_TYPE": ["11", "10"],
                                          "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["11", "10"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["1199123456-1", "1099654321-9"],
                                           "proevenr": ["F99123456-1", "D99654321-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                        translate_from = "number",
                                                        translate_to = "letter",
                                                        from_format = re.compile(
                                                            r"(?P<sample_type>[135]0|11)"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)"),
                                                        to_format = re.compile(
                                                            r"(?P<sample_type>[135]0|11|[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)")
                                                        )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_no_translation(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                         "P_TYPE": ["F", "D"],
                                         "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["F99123456-1", "D99654321-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                        translate_from = "letter",
                                                        translate_to = "letter",
                                                        from_format = re.compile(
                                                            r"(?P<sample_type>[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)"),
                                                        to_format = re.compile(
                                                            r"(?P<sample_type>[BDFT])"
                                                            r"(?P<sample_year>\d{2})"
                                                            r"(?P<sample_number>\d{6})"
                                                            r"(?P<bact_number>-\d)")
                                                        )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_handle_leading_zeroes(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                         "P_TYPE": ["F", "D"],
                                         "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F00123456-1", "D00654321-9"],
                                           "proevenr": ["F00123456-1", "D00654321-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                             translate_from = "letter",
                                                             translate_to = "letter",
                                                             from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                             to_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)")
                                                             )
        print(test_result.to_string())
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_handle_move_year(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                         "P_TYPE": ["F", "D"],
                                         "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F00123456-1", "D00654321-9"],
                                           "proevenr": ["F12345600-1", "D65432100-9"]})
        test_result = get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                             translate_from = "letter",
                                                             translate_to = "letter",
                                                             from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                             to_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<bact_number>-\d)"))
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_fail_no_match(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                         "P_TYPE": ["F", "D"],
                                         "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}

        error_msg = "Sample numbers ['D00654321-9', 'F00123456-1'] in LIS report " \
                    "don't match the specified sample number format."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                   translate_from = "letter",
                                                   translate_to = "letter",
                                                   from_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<bact_number>-\d)"),
                                                   to_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<bact_number>-\d)"))

    def test_fail_one_mismatch(self):
        lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                         "P_TYPE": ["F", "D"],
                                         "BAKT_NR": ["1", "9"]})
        sample_number_mapping = {"11": "F", "10": "D"}

        error_msg = "Sample numbers ['D00654321-9'] in LIS report " \
                    "don't match the specified sample number format."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.translate_lis_data(lis_report, sample_number_mapping,
                                                   translate_from = "letter",
                                                   translate_to = "letter",
                                                   from_format = re.compile(
                                                       r"(?P<sample_type>[35]0|11|[BTF])"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<bact_number>-\d)"),
                                                   to_format = re.compile(
                                                       r"(?P<sample_type>[BTF])"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<bact_number>-\d)"))


class TestGetNumberFromLIS(unittest.TestCase):
    def test_translate_letter_to_number(self):
        # lis_report = pd.DataFrame(data={"PRV_NR": ["99123456", "99654321"],
        #                                "P_TYPE": ["F", "D"],
        #                                "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_with_letter.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["1199123456-1", "1099654321-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "letter",
                                                                    translate_to = "number",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[135]0|11|[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)")

                                                                    )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_translate_letter_to_number_en(self):
        """Translate sample type letters to sample type numbers in a LIS report with non-default
        column names"""
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_with_letter_en.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"SAMPLENR": ["99123456", "99654321"],
                                           "SAMPLE_TYPE": ["F", "D"],
                                           "BACT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["1199123456-1", "1099654321-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "letter",
                                                                    translate_to = "number",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[135]0|11|[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    lis_data_names = LIS_NAMES_EN
                                                                    )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)


    def test_translate_number_to_letter(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
        #                                  "P_TYPE": ["11", "10"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_with_number.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["11", "10"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["1199123456-1", "1099654321-9"],
                                           "proevenr": ["F99123456-1", "D99654321-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "number",
                                                                    translate_to = "letter",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[135]0|11)"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[135]0|11|[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)")
                                                                    )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_no_translation(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
        #                                  "P_TYPE": ["F", "D"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_with_letter.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["99123456", "99654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F99123456-1", "D99654321-9"],
                                           "proevenr": ["F99123456-1", "D99654321-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "letter",
                                                                    translate_to = "letter",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)")
                                                                    )
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_handle_leading_zeroes(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "0054321"],
        #                                  "P_TYPE": ["F", "D"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_leading_zeroes.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F00123456-1", "D00654321-9"],
                                           "proevenr": ["F00123456-1", "D00654321-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "letter",
                                                                    translate_to = "letter",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)")
                                                                    )
        print(test_result.to_string())
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_handle_move_year(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "0054321"],
        #                                  "P_TYPE": ["F", "D"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_leading_zeroes.txt"
        sample_number_mapping = {"11": "F", "10": "D"}
        true_result = pd.DataFrame(data = {"PRV_NR": ["00123456", "00654321"],
                                           "P_TYPE": ["F", "D"],
                                           "BAKT_NR": ["1", "9"],
                                           "proevenr_unordered": ["F00123456-1", "D00654321-9"],
                                           "proevenr": ["F12345600-1", "D65432100-9"]})
        test_result = get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                                    translate_from = "letter",
                                                                    translate_to = "letter",
                                                                    from_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<bact_number>-\d)"),
                                                                    to_format = re.compile(
                                                                 r"(?P<sample_type>[BDFT])"
                                                                 r"(?P<sample_number>\d{6})"
                                                                 r"(?P<sample_year>\d{2})"
                                                                 r"(?P<bact_number>-\d)"))
        pd.testing.assert_frame_equal(test_result, true_result, check_like = True)

    def test_fail_no_match(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "0054321"],
        #                                  "P_TYPE": ["F", "D"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_leading_zeroes.txt"
        sample_number_mapping = {"11": "F", "10": "D"}

        error_msg = "Sample numbers ['D00654321-9', 'F00123456-1'] in LIS report " \
                    "don't match the specified sample number format."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                          translate_from = "letter",
                                                          translate_to = "letter",
                                                          from_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<bact_number>-\d)"),
                                                          to_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<bact_number>-\d)"))

    def test_fail_one_mismatch(self):
        # lis_report = pd.DataFrame(data = {"PRV_NR": ["00123456", "0054321"],
        #                                  "P_TYPE": ["F", "D"],
        #                                  "BAKT_NR": ["1", "9"]})
        lis_report = pathlib.Path(
            __file__).parent / "data" / "utilities_test" / "mads_one_mismatch.txt"
        sample_number_mapping = {"11": "F", "10": "D"}

        error_msg = "Sample numbers ['D00654321-9'] in LIS report " \
                    "don't match the specified sample number format."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.get_report_with_isolate_number(lis_report, sample_number_mapping,
                                                          translate_from = "letter",
                                                          translate_to = "letter",
                                                          from_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<bact_number>-\d)"),
                                                          to_format = re.compile(
                                                       r"(?P<sample_type>[135]0|11)"
                                                       r"(?P<sample_number>\d{6})"
                                                       r"(?P<sample_year>\d{2})"
                                                       r"(?P<bact_number>-\d)"))


class TestSetupLISReport(unittest.TestCase):
    def tearDown(self) -> None:
        """Make sure to blank out the text of the output file."""
        (pathlib.Path(__file__).parent
         / "data"
         / "get_lis_report"
         / "test_lis_from_db.txt").write_text(data="", encoding = "latin-1")

    def test_fail_missing_db(self):
        """Fail when dialect has been specified but database hasn't."""
        test_config = {"language": "da",
                       "input_names": pathlib.Path(__file__).parent / "data"
                                      / "localization" / "input_da.yaml",

                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": pathlib.Path(__file__).parent / "data"
                                                         / "get_lis_report" / "fail_lis_file.txt",
                                           "dialect": "sqlite",
                                           "database": "",
                                           "filter_column": "WGS_ISOLAT",
                                           "filter_value": "WGS",
                                           "db_table": "test_db",
                                           "schema": ""
                                           },
                       "sample_number_settings": {
                           "sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                           "format_in_sheet": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_in_lis": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           # optional conversion between sample numbers starting with numbers vs letters (allowed values: number, letter)
                           # if your sample numbers don't need conversion, set _in and _out to be the same - TODO: extra logic on switching
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           # mapping between sample number/letter if needed
                           "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}}
        }
        error_msg = "SQL dialect has been specified but no database to connect to was given."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            get_lis_report.setup_lis_data_from_config(test_config)
        assert not test_config["lab_info_system"]["lis_report"].exists()

    def test_fail_missing_dialect(self):
        """Fail when database has been specified but dialect hasn't."""
        test_config = {"language": "da",
                       "input_names": pathlib.Path(__file__).parent / "data"
                                      / "localization" / "input_da.yaml",
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": pathlib.Path(__file__).parent / "data"
                                                         / "get_lis_report" / "fail_lis_file.txt",
                                           "dialect": "",
                                           "database": "/:memory:",
                                           "filter_column": "WGS_ISOLAT",
                                           "filter_value": "WGS",
                                           "db_table": "test_db",
                                           "schema": ""

                                           },
                       "sample_number_settings": {
                           "sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                           "format_in_sheet": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_in_lis": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           # optional conversion between sample numbers starting with numbers vs letters (allowed values: number, letter)
                           # if your sample numbers don't need conversion, set _in and _out to be the same - TODO: extra logic on switching
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           # mapping between sample number/letter if needed
                           "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}}
                       }
        error_msg = "SQL database has been specified but no dialect for the connection was given."
        with pytest.raises(ValueError, match = re.escape(error_msg)):
            get_lis_report.setup_lis_data_from_config(test_config)
        assert not test_config["lab_info_system"]["lis_report"].exists()

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    # fake time, fake result
    @patch(f'{get_lis_report.__name__}.datetime', wraps=datetime)
    @patch(f"{get_lis_report.__name__}.pd.read_sql_table")
    def test_success_get_data_from_db(self, mock_read, mock_datetime):
        """Fetch data from the database and save it."""
        # TODO: are we skipping over anything here?
        mock_datetime.now.return_value = datetime.fromisoformat("2099-01-01 00:01:23")
        mock_read.return_value = pd.DataFrame([{"PRV_NR": 99123456, "P_TYPE": "F",
                                                "BAKT_NR": 1, "LOKAL_BAKT_KODE": "888",
                                                "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke"},
                                               {"PRV_NR": 99123456, "P_TYPE": "F",
                                                "BAKT_NR": 2, "LOKAL_BAKT_KODE": "1",
                                                "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke"}
                                               ])

        test_config = {"language": "da",
                       "input_names": pathlib.Path(__file__).parent / "data"
                                      / "localization" / "input_da.yaml",

                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": pathlib.Path(__file__).parent / "data"
                                                         / "get_lis_report"
                                                         / "test_lis_from_db.txt",
                                           "dialect": "sqlite",
                                           "database": "/:memory:",
                                           "filter_column": "LOKAL_BAKT_KODE",
                                           "filter_value": "888",
                                           "db_table": "test_db",
                                           "schema": ""
                                           },
                       "sample_number_settings": {
                           "sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                           "format_in_sheet": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_in_lis": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           # optional conversion between sample numbers starting with numbers vs letters (allowed values: number, letter)
                           # if your sample numbers don't need conversion, set _in and _out to be the same - TODO: extra logic on switching
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           # mapping between sample number/letter if needed
                           "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}}
                       }
        # TODO check that LIS input file is empty
        expected_data = pd.DataFrame(data = {"PRV_NR": ["99123456"], "P_TYPE": ["F"],
                                             "BAKT_NR": ["1"], "LOKAL_BAKT_KODE": ["888"],
                                             "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke"],
                                             "proevenr_unordered": ["F99123456-1"],
                                             "proevenr": ["1199123456-1"]})
        log_msg = ("Fetching LIS report from sqlite database /:memory: "
                   "at 2099-01-01 00:01:23 with user test")
        # mock os.environ - https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pythons-unittest/
        with (self._caplog.at_level(level = "INFO", logger = "get_lis"),
              patch.dict(f"{get_lis_report.__name__}.os.environ", {"LIS_DB_USER": "test"})):
            test_data = get_lis_report.setup_lis_data_from_config(test_config)
            assert ("get_lis", logging.INFO, log_msg) in self._caplog.record_tuples
        pd.testing.assert_frame_equal(expected_data, test_data)
        saved_data = pd.read_csv(test_config["lab_info_system"]["lis_report"], sep=";",
                                 encoding="latin-1", dtype = {"PRV_NR": str,
                                                              "P_TYPE": str,
                                                              "BAKT_NR": str,
                                                              "LOKAL_BAKT_KODE": str,
                                                              "proevenr": str})
        pd.testing.assert_frame_equal(expected_data, saved_data)

    @patch(f"{get_lis_report.__name__}.pd.read_sql_table")
    def test_success_get_data_from_extra_col(self, mock_read):
        """Fetch data from the database and save it, filtering by an additional column."""
        mock_read.return_value = pd.DataFrame([{"PRV_NR": 99123456, "P_TYPE": "F",
                                                "BAKT_NR": 1, "LOKAL_BAKT_KODE": "888",
                                                "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke",
                                                "WGS_ISOLAT": "WGS"},
                                               {"PRV_NR": 99123456, "P_TYPE": "F",
                                                "BAKT_NR": 2, "LOKAL_BAKT_KODE": "1",
                                                "LOKAL_BAKT_TEKST": "Undersøgt - medtages ikke",
                                                "WGS_ISOLAT": ""}
                                               ])

        test_config = {"language": "da",
                       "input_names": pathlib.Path(__file__).parent / "data"
                                      / "localization" / "input_da.yaml",

                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": pathlib.Path(__file__).parent / "data"
                                                         / "get_lis_report"
                                                         / "test_lis_from_db.txt",
                                           "dialect": "sqlite",
                                           "database": "/:memory:",
                                           "filter_column": "WGS_ISOLAT",
                                           "filter_value": "WGS",
                                           "db_table": "test_db",
                                           "schema": ""
                                           },
                       "sample_number_settings": {
                           "sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                           "format_in_sheet": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_in_lis": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           # optional conversion between sample numbers starting with numbers vs letters (allowed values: number, letter)
                           # if your sample numbers don't need conversion, set _in and _out to be the same - TODO: extra logic on switching
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           # mapping between sample number/letter if needed
                           "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}}
                       }
        # TODO check that LIS input file is empty
        expected_data = pd.DataFrame(data = {"PRV_NR": ["99123456"], "P_TYPE": ["F"],
                                             "BAKT_NR": ["1"], "LOKAL_BAKT_KODE": ["888"],
                                             "LOKAL_BAKT_TEKST": ["Undersøgt - medtages ikke"],
                                             "WGS_ISOLAT": ["WGS"],
                                             "proevenr_unordered": ["F99123456-1"],
                                             "proevenr": ["1199123456-1"]
                                             })
        test_data = get_lis_report.setup_lis_data_from_config(test_config)
        pd.testing.assert_frame_equal(expected_data, test_data)
        saved_data = pd.read_csv(test_config["lab_info_system"]["lis_report"], sep=";",
                                 encoding = "latin-1", dtype = {"PRV_NR": str,
                                                                "P_TYPE": str,
                                                                "BAKT_NR": str,
                                                                "LOKAL_BAKT_KODE": str,
                                                                "proevenr": str})
        pd.testing.assert_frame_equal(expected_data, saved_data)

    # TODO test engine construction?

    def test_success_get_data_from_file(self):
        """Load data from a file when no database and no dialect have been given."""
        test_config = {"language": "da",
                       "input_names": pathlib.Path(__file__).parent / "data"
                                      / "localization" / "input_da.yaml",
                       "lab_info_system": {"use_lis_features": True,
                                           "lis_report": pathlib.Path(__file__).parent / "data"
                                                         / "utilities_test"
                                                         / "mads_for_sheet_prep.txt",
                                           "dialect": "",
                                           "database": "",
                                           "filter_column": "WGS_ISOLAT",
                                           "filter_value": "WGS",
                                           "db_table": "test_db",
                                           "schema": ""
                                           },
                       "sample_number_settings": {
                           "sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                           "format_in_sheet": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_in_lis": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           "format_output": '(?P<sample_type>[135]0|11|[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                           # optional conversion between sample numbers starting with numbers vs letters (allowed values: number, letter)
                           # if your sample numbers don't need conversion, set _in and _out to be the same - TODO: extra logic on switching
                           "sample_numbers_in": "number",
                           "sample_numbers_out": "letter",
                           "sample_numbers_report": "number",
                           # mapping between sample number/letter if needed
                           "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}}
                       }
        expected_data = pd.DataFrame(data = {"PRV_NR": ["99123456", "99123456"],
                                             "P_TYPE": ["B", "B"],
                                             "BAKT_NR": ["1", "2"],
                                             "LOKAL_BAKT_KODE": ["1", "2"],
                                             "proevenr_unordered": ["B99123456-1",
                                                                    "B99123456-2"],
                                             "proevenr": ["3099123456-1",
                                                          "3099123456-2"]})
        test_data = get_lis_report.setup_lis_data_from_config(test_config)
        pd.testing.assert_frame_equal(expected_data, test_data)
