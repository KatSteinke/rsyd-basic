#  Copyright (c) 2023-2024 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re
import unittest

import pytest

import numpy as np
import pandas as pd

import check_expected_results

from input_names import BacteriaMappingNames

# set up some translations we're going to be using over and over
BACT_NAMES_EN = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                     internal_bacteria_text = "bacteria_text",
                                     bacteria_category = "bacteria_category")


class TestCheckQCResults(unittest.TestCase):
    true_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                   "noter": [pd.NA, pd.NA, pd.NA],
                                   "pipeline_noter": ["",
                                                      "Coverage er lavere end "
                                                      "grænsen for coverage (30). ",
                                                      ""],
                                   "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                   "Bestilt af": ["ABC", "XYZ", "ABC"],
                                   "Indikation": ["Identifikation",
                                                  "Toxingen_ID",
                                                  "Resistens; HAI"],
                                   "N50_basics": [600000, 3000, 200000],
                                   "Avg. coverage depth_basics": [100, 10, 50],
                                   "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                   "MADS species": ["Testococcus",
                                                    "Testobacter fakei",
                                                    "Placeholderia fakeorum"],
                                   "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                   "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                               "Testobacter fakei",
                                                               "Placeholderia fakeorum"],
                                   "Serotypning": [pd.NA, pd.NA, pd.NA],
                                   "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                   "Total length_basics": [245000, 100000, 300100],
                                   "# contigs": [20, 900, 30],
                                   "# predicted genes (unique)": [1000, 20, 1000],
                                   "WGS_species_kraken": ["Testococcus ignotus",
                                                          "Testobacter fakei",
                                                          "Placeholderia fakeorum"],
                                   "WGS_genus_kraken": ["Testococcus",
                                                        "Testobacter",
                                                        "Placeholderia"],
                                   "Completeness": [99.4, 40, 99.9],
                                   "Contamination": [0, 0, 0.5],
                                   "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
    true_df = true_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                      "MADS: kendt længde i bp": float,
                                      "GTDB: kendt længde i bp": float,
                                      "Total length_basics": "Int64",
                                      "# contigs": "Int64",
                                      "# predicted genes (unique)": "Int64"})

    def test_fail_unsupported_language(self):
        """Fail if the report is in an unsupported language."""
        # technically this is just still in Danish but I am not translating all this to Klingon
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        error_msg = "tlh is not a supported language. Supported languages are ['da', 'en']."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            check_expected_results.check_qc_results(test_report = test_df,
                                                    true_report = self.true_df,
                                                    report_language = "tlh")

    def test_check_success(self):
        """Ensure the check passes when the test result is identical to the expected result."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
        assert check_frames_match

    def test_check_success_en(self):
        """Ensure the check passes when the test result is identical to the expected result
        for the English translation of the results."""
        true_df = pd.DataFrame(data = {"sequence approved": [pd.NA, 0, pd.NA],
                                       "notes": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_notes": ["",
                                                          "Coverage is lower than the cutoff"
                                                          " for coverage (30). ",
                                                          ""],
                                       "sample_number": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Requested_by": ["ABC", "XYZ", "ABC"],
                                       "Indication": ["Identification",
                                                      "Toxin gene ID",
                                                      "AMR; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "LIS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "LIS: known length in bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotyping": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: known length in bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        true_df = true_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "LIS: known length in bp": float,
                                          "GTDB: known length in bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        test_df = pd.DataFrame(data = {"sequence approved": [pd.NA, 0, pd.NA],
                                       "notes": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_notes": ["",
                                                          "Coverage is lower than the cutoff"
                                                          " for coverage (30). ",
                                                          ""],
                                       "sample_number": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Requested_by": ["ABC", "XYZ", "ABC"],
                                       "Indication": ["Identification",
                                                      "Toxin gene ID",
                                                      "AMR; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "LIS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "LIS: known length in bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotyping": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: known length in bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "LIS: known length in bp": float,
                                          "GTDB: known length in bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        check_frames_match = check_expected_results.check_qc_results(test_df, true_df,
                                                                     report_language = "EN")
        assert check_frames_match

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_log_version_number(self):
        """Check that the version number is reported"""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            check_expected_results.check_qc_results(test_df, self.true_df)
            log_msg = "Pipeline version reported as ['1.2.3'] " \
                      "- check this is correct before releasing."

            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_check_missing_samples(self):
        """Check that missing samples are reported"""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0],
                                       "noter": [pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). "],
                                       "proevenr": ["1199123456-1", "1199123456-2"],
                                       "Bestilt af": ["ABC", "XYZ"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID"],
                                       "N50_basics": [600000, 3000],
                                       "Avg. coverage depth_basics": [100, 10],
                                       "Grampos/neg": ["Positiv", "Negativ"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei"],
                                       "MADS: kendt længde i bp": [np.nan, 500000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei"],
                                       "Serotypning": [pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000],
                                       "Total length_basics": [245000, 100000],
                                       "# contigs": [20, 900],
                                       "# predicted genes (unique)": [1000, 20],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter"],
                                       "Completeness": [99.4, 40],
                                       "Contamination": [0, 0],
                                       "pipeline_version": ["1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            log_msg = "Missing samples {'1199123456-3'} from QC report"
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert not check_frames_match


    def test_fail_extra_samples(self):
        """Check that additional samples are reported"""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          "", ""],
                                       "proevenr": ["1199123456-1", "1199123456-2",
                                                    "1199123456-3", "1199123456-4"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000,
                                                                   300000],
                                       "Total length_basics": [245000, 100000, 300100, 300100],
                                       "# contigs": [20, 900, 30, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9, 99.9],
                                       "Contamination": [0, 0, 0.5, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            log_msg = "Unexpected samples {'1199123456-4'} in QC report"
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert not check_frames_match

    def test_check_assembly_stats_not_worse(self):
        """Ensure the check notes small fluctuations in assembly stats but doesn't fail."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [599900, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
        assert check_frames_match

    def test_check_assembly_stats_better(self):
        """Ensure the check notes improvements in assembly stats and doesn't fail."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 30000, 200000],
                                       "Avg. coverage depth_basics": [100, 20, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 150000, 300100],
                                       "# contigs": [20, 800, 30],
                                       "# predicted genes (unique)": [1000, 200, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["N50_basics", "N50_basics",
                                                      "Avg. coverage depth_basics",
                                                      "Avg. coverage depth_basics",
                                                      "Total length_basics",
                                                      "Total length_basics",
                                                      "# contigs", "# contigs",
                                                      "# predicted genes (unique)",
                                                      "# predicted genes (unique)"],
                                                     ["expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found"]])
        mismatched_index = pd.Index(["1199123456-2"], name = "proevenr")
        mismatched = pd.DataFrame(data = [[3000, 30000, 10.0, 20.0, 100000, 150000, 900, 800,
                                           20, 200]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            log_msg = f"Assembly quality may have improved compared to the baseline. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert check_frames_match
            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_check_fail_worse_assembly(self):
        """Ensure a significantly worse assembly fails the test."""
        test_df = pd.DataFrame(data = {"godkendt": [0, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [60000, 3000, 200000],
                                       "Avg. coverage depth_basics": [20, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [200000, 100000, 300100],
                                       "# contigs": [100, 900, 30],
                                       "# predicted genes (unique)": [100, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["N50_basics", "N50_basics",
                                                      "Avg. coverage depth_basics",
                                                      "Avg. coverage depth_basics",
                                                      "Total length_basics",
                                                      "Total length_basics",
                                                      "# contigs", "# contigs",
                                                      "# predicted genes (unique)",
                                                      "# predicted genes (unique)"],
                                                     ["expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found"]])
        mismatched_index = pd.Index(["1199123456-1"], name = "proevenr")
        mismatched = pd.DataFrame(data = [[600000, 60000, 100.0, 20.0, 245000, 200000,
                                           20, 100, 1000, 100]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Assembly quality may have decreased compared to the baseline. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            print(log_msg)

            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_check_fail_wrong_size_same_quality(self):
        """Ensure different sample size fails the test."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 90000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["Total length_basics", "Total length_basics"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["1199123456-2"], name = "proevenr")
        mismatched = pd.DataFrame(data = [[100000, 90000]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Assembly size does not match for one or more samples:\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_check_handle_size_fluctuation(self):
        """Ensure small fluctuations doesn't automatically fail when sequencing quality is better."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100010, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
        assert check_frames_match

    def test_check_completeness_stats_not_worse(self):
        """Ensure the check notes small fluctuations in completeness/contamination stats but doesn't fail."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.2, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
        assert check_frames_match

    def test_check_completeness_stats_better(self):
        """Ensure the check notes improvements in completeness/contamination stats and doesn't fail."""
        # TODO: do we need this or is this an overcomplicated headache
        pass
        # test_df = pd.DataFrame(data={"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
        #                              "Bestilt af": ["ABC", "XYZ", "ABC"],
        #                              "Indikation": ["Identifikation",
        #                                             "Toxingen_ID",
        #                                             "Resistens; HAI"],
        #                              "N50_basics": [600000, 3000, 200000],
        #                              "Avg. coverage depth_basics": [100, 10, 50],
        #                              "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
        #                              "MADS species": ["Testococcus",
        #                                               "Testobacter fakei",
        #                                               "Placeholderia fakeorum"],
        #                              "MADS: kendt længde i bp": [np.nan, 500000, 300000],
        #                              "WGS_species_kraken_basics": ["Testococcus ignotus",
        #                                                            "Testobacter fakei",
        #                                                            "Placeholderia fakeorum"],
        #                              "Serotypning": [pd.NA, pd.NA, pd.NA],
        #                              "Kraken: kendt længde i bp": [250000, 500000, 300000],
        #                              "Total length_basics": [245000, 100000, 300100],
        #                              "# contigs": [20, 900, 30],
        #                              "# predicted genes (unique)": [1000, 20, 1000],
        #                              "WGS_genus_kraken": ["Testococcus",
        #                                                   "Testobacter",
        #                                                   "Placeholderia"],
        #                              "Completeness": [99.9, 40, 99.9],
        #                              "Contamination": [0, 0, 0.5],
        #                              "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        # test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
        #                                   "MADS: kendt længde i bp": float,
        #                                   "Kraken: kendt længde i bp": float,
        #                                   "Total length_basics": "Int64",
        #                                   "# contigs": "Int64",
        #                                   "# predicted genes (unique)": "Int64"})
        # mismatched_cols = pd.MultiIndex.from_arrays([["Completeness", "Completeness"],
        #                                              ["expected", "found"]])
        # mismatched_index = pd.Index(["1199123456-1"], name = "proevenr")
        # mismatched = pd.DataFrame(data = [[99.2, 99.9]],
        #                           columns = mismatched_cols,
        #                           index = mismatched_index)
        # with self.assertLogs("QA_Test") as logged:
        #     log_msg = f"INFO:QA_Test:Completeness/contamination may have improved compared to the baseline. " \
        #               f"Relevant sample(s):\n" \
        #               f"{mismatched.to_string()}"
        #     check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
        #     assert check_frames_match
        #     assert log_msg in logged.output

    def test_check_fail_worse_contamination(self):
        """Ensure significantly worse completeness/contamination fails the test."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [3, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["Contamination", "Contamination"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["1199123456-1"], name = "proevenr")
        mismatched = pd.DataFrame(data = [[0.0, 3.0]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Completeness/contamination may have worsened compared to the baseline. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples


    def test_check_fail_worse_completeness(self):
        """Ensure significantly worse completeness fails the test."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [90.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["Completeness", "Completeness"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["1199123456-1"], name = "proevenr")
        mismatched = pd.DataFrame(data = [[99.4, 90.4]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Completeness/contamination may have worsened compared to the baseline. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_check_single_mismatch(self):
        """Check that a single mismatch gets flagged correctly."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Placeholderia fakeorum",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 300000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Placeholderia fakeorum",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Placeholderia",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["WGS_species_GTDB_basics",
                                                      "WGS_species_GTDB_basics",
                                                      "GTDB: kendt længde i bp",
                                                      "GTDB: kendt længde i bp",
                                                      "WGS_species_kraken",
                                                      "WGS_species_kraken",
                                                      "WGS_genus_kraken",
                                                      "WGS_genus_kraken"],
                                                     ["expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found"]])
        mismatched_index = pd.Index(["1199123456-2"], name = "proevenr")
        mismatched = pd.DataFrame(data = [["Testobacter fakei", "Placeholderia fakeorum",
                                           500000.0, 300000.0,
                                           "Testobacter fakei", "Placeholderia fakeorum",
                                           "Testobacter", "Placeholderia"]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"One or more results do not match. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_check_multi_mismatch(self):
        """Check that multiple mismatches get flagged correctly."""
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation; HAI",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Placeholderia fakeorum",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 300000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Placeholderia fakeorum",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Placeholderia",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        mismatched_cols = pd.MultiIndex.from_arrays([["Indikation", "Indikation",
                                                      "WGS_species_GTDB_basics",
                                                      "WGS_species_GTDB_basics",
                                                      "GTDB: kendt længde i bp",
                                                      "GTDB: kendt længde i bp",
                                                      "WGS_species_kraken",
                                                      "WGS_species_kraken",
                                                      "WGS_genus_kraken",
                                                      "WGS_genus_kraken"],
                                                     ["expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found",
                                                      "expected", "found"]])
        mismatched_index = pd.Index(["1199123456-1", "1199123456-2"], name = "proevenr")
        mismatched = pd.DataFrame(data = [["Identifikation", "Identifikation; HAI",
                                           np.nan, np.nan,
                                           np.nan, np.nan,
                                           np.nan, np.nan,
                                           np.nan, np.nan],
                                          [np.nan, np.nan,
                                           "Testobacter fakei", "Placeholderia fakeorum",
                                           500000.0, 300000.0,
                                           "Testobacter fakei", "Placeholderia fakeorum",
                                           "Testobacter", "Placeholderia"]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"One or more results do not match. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            check_frames_match = check_expected_results.check_qc_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples


class TestCheckTyping(unittest.TestCase):
    mads_mapping = pd.DataFrame(data = {"BAKTERIENAVN": ["E. coli (STEC)",
                                                         "Testococcus",
                                                         "Testobacter fakei",
                                                         "Placeholderia fakeorum",
                                                         "Salmonella"],
                                        "bakterie_kategori": ["Escherichia coli",
                                                              "Testococcus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum",
                                                              "Salmonella enterica"
                                                              ]})

    def test_fail_unsupported_language(self):
        """Fail if the report is in an unsupported language."""
        # technically this is just still in Danish but I am not translating all this to Klingon
        test_df = pd.DataFrame(data = {"godkendt": [pd.NA, 0, pd.NA],
                                       "noter": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_noter": ["",
                                                          "Coverage er lavere end "
                                                          "grænsen for coverage (30). ",
                                                          ""],
                                       "proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "MADS: kendt længde i bp": float,
                                          "GTDB: kendt længde i bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        error_msg = "tlh is not a supported language. Supported languages are ['da', 'en']."
        run_results = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "run_no_typing"

        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            check_expected_results.check_serotyping(run_results, test_df, self.mads_mapping,
                                                    report_language = "tlh")

    def test_success_no_typed_species(self):
        """Check that the test doesn't fail when the test set has no species that should be typed."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Positiv", "Negativ", "Positiv"],
                                       "MADS species": ["Testococcus",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Testococcus ignotus",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Testococcus ignotus",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Testococcus",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "run_no_typing"
        check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                               self.mads_mapping)
        assert check_typing

    def test_success_coli(self):
        """Check that the test successfully finds typing results for multiple E. coli samples."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "MADS species": ["E. coli (STEC)",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Escherichia coli",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Escherichia coli",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Escherichia",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(
            __file__).parent / "data" / "check_pipeline_qc" / "run_coli_typing"
        check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                               self.mads_mapping)
        assert check_typing

    def test_success_coli_en(self):
        """Find typing results for E. coli samples when using non-default column names."""
        test_df = pd.DataFrame(data = {"sequence approved": [pd.NA, 0, pd.NA],
                                       "notes": [pd.NA, pd.NA, pd.NA],
                                       "pipeline_notes": ["",
                                                          "Coverage is lower than the cutoff"
                                                          " for coverage (30). ",
                                                          ""],
                                       "sample_number": ["1199123456-1", "1199123456-2",
                                                         "1199123456-3"],
                                       "Requested_by": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identification",
                                                      "Toxin gene ID",
                                                      "AMR; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "LIS species": ["E. coli (STEC)",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "LIS: known length in bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Escherichia coli",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotyping": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: known length in bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Escherichia coli",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Escherichia",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        test_df = test_df.astype(dtype = {"N50_basics": "Int64",  # has to handle NA
                                          "LIS: known length in bp": float,
                                          "GTDB: known length in bp": float,
                                          "Total length_basics": "Int64",
                                          "# contigs": "Int64",
                                          "# predicted genes (unique)": "Int64"})
        run_results = pathlib.Path(
            __file__).parent / "data" / "check_pipeline_qc" / "run_coli_typing"
        mads_mapping_en = pd.DataFrame(data = {"bacteria_text": ["E. coli (STEC)",
                                                             "Testococcus",
                                                             "Testobacter fakei",
                                                             "Placeholderia fakeorum",
                                                             "Salmonella"],
                                            "bacteria_category": ["Escherichia coli",
                                                                  "Testococcus",
                                                                  "Testobacter fakei",
                                                                  "Placeholderia fakeorum",
                                                                  "Salmonella enterica"
                                                                  ]})
        check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                               mads_mapping_en,
                                                               report_language = "EN",
                                                               bacteria_names = BACT_NAMES_EN)
        assert check_typing

    def test_success_salmonella(self):
        """Check that the test successfully finds typing results for multiple Salmonella samples."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "MADS species": ["Salmonella",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Salmonella enterica",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Salmonella enterica",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Salmonella",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(
            __file__).parent / "data" / "check_pipeline_qc" / "run_salmo_typing"
        check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                               self.mads_mapping)
        assert check_typing

    def test_success_multi_types(self):
        """Check that the test successfully finds typing results for a run with both E. coli and Salmonella."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "MADS species": ["E. coli (STEC)",
                                                        "Salmonella",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Escherichia coli",
                                                                   "Salmonella enterica",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Escherichia coli",
                                                              "Salmonella enterica",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Escherichia",
                                                            "Salmonella",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(
            __file__).parent / "data" / "check_pipeline_qc" / "run_multi_typing"
        check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                               self.mads_mapping)
        assert check_typing

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_fail_coli_missing(self):
        """Check that the test successfully identifies missing typing for E. coli samples."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "MADS species": ["E. coli (STEC)",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Escherichia coli",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Escherichia coli",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Escherichia",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "run_no_typing"
        log_msg = "Missing SerotypeFinder results for sample(s) ['1199123456-1']"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                                   self.mads_mapping)
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert not check_typing

    def test_fail_salmo_missing(self):
        """Check that the test successfully identifies missing typing for Salmonella samples."""
        test_df = pd.DataFrame(data = {"proevenr": ["1199123456-1", "1199123456-2", "1199123456-3"],
                                       "Bestilt af": ["ABC", "XYZ", "ABC"],
                                       "Indikation": ["Identifikation",
                                                      "Toxingen_ID",
                                                      "Resistens; HAI"],
                                       "N50_basics": [600000, 3000, 200000],
                                       "Avg. coverage depth_basics": [100, 10, 50],
                                       "Grampos/neg": ["Negativ", "Negativ", "Positiv"],
                                       "MADS species": ["Salmonella",
                                                        "Testobacter fakei",
                                                        "Placeholderia fakeorum"],
                                       "MADS: kendt længde i bp": [np.nan, 500000, 300000],
                                       "WGS_species_GTDB_basics": ["Salmonella enterica",
                                                                   "Testobacter fakei",
                                                                   "Placeholderia fakeorum"],
                                       "Serotypning": [pd.NA, pd.NA, pd.NA],
                                       "GTDB: kendt længde i bp": [250000, 500000, 300000],
                                       "Total length_basics": [245000, 100000, 300100],
                                       "# contigs": [20, 900, 30],
                                       "# predicted genes (unique)": [1000, 20, 1000],
                                       "WGS_species_kraken": ["Salmonella enterica",
                                                              "Testobacter fakei",
                                                              "Placeholderia fakeorum"],
                                       "WGS_genus_kraken": ["Salmonella",
                                                            "Testobacter",
                                                            "Placeholderia"],
                                       "Completeness": [99.4, 40, 99.9],
                                       "Contamination": [0, 0, 0.5],
                                       "pipeline_version": ["1.2.3", "1.2.3", "1.2.3"]})
        run_results = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "run_no_typing"
        log_msg = "Missing SeqSero results for sample(s) ['1199123456-1']"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_typing = check_expected_results.check_serotyping(run_results, test_df,
                                                                   self.mads_mapping)
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert not check_typing


class TestMADSResults(unittest.TestCase):
    true_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                         pd.NA,  # CC type
                                         pd.NA,  # cgMLST
                                         "Testococcus ignotus",  # Kraken ID
                                         "123",  # MLST type
                                         pd.NA,  # other beta-lactamases
                                         "blaNDM-5",  # carbapenemases
                                         pd.NA,  # colistin resistance
                                         pd.NA,  # vancomycin resistance
                                         "FakeSerotype",  # serotyping result
                                         pd.NA,  # Spatype
                                         pd.NA,  # ST
                                         pd.NA,  # supplerende type
                                         pd.NA  # toxins
                                         ],
                                   "1": ["1199123456-2",  # sample number
                                         pd.NA,  # CC type
                                         pd.NA,  # cgMLST
                                         "Testobacter fakei",  # Kraken ID
                                         "-",  # MLST type
                                         pd.NA,  # other beta-lactamases
                                         pd.NA,  # carbapenemases
                                         pd.NA,  # colistin resistance
                                         pd.NA,  # vancomycin resistance
                                         pd.NA,  # serotyping result
                                         pd.NA,  # Spatype
                                         pd.NA,  # ST
                                         pd.NA,  # supplerende type
                                         pd.NA  # toxins
                                         ],
                                   "2": ["1199123456-3",  # sample number
                                         pd.NA,  # CC type
                                         pd.NA,  # cgMLST
                                         "Placeholderia fakeorum",  # Kraken ID
                                         "321",  # MLST type
                                         pd.NA,  # other beta-lactamases
                                         pd.NA,  # carbapenemases
                                         "pmrB_Y358N",  # colistin resistance
                                         pd.NA,  # vancomycin resistance
                                         pd.NA,  # serotyping result
                                         pd.NA,  # Spatype
                                         pd.NA,  # ST
                                         pd.NA,  # supplerende type
                                         "STX2B"  # toxins
                                         ]
                                   },
                           index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                    "O_WGS identifikation", "O_WGS MLST",
                                    "O_WGS res.gen anden beta-lactamase",
                                    "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                    "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                    "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                    "O_WGS Toxinpåvisning_0"])

    def test_catch_invalid_language(self):
        """Fail if an unsupported language has been supplied."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        error_msg = "tlh is not a supported language. Supported languages are ['da', 'en']."
        with pytest.raises(NotImplementedError, match=re.escape(error_msg)):
            check_expected_results.check_mads_results(test_df, self.true_df,
                                                      report_language = "tlh")

    def test_success(self):
        """Ensure the test passes when results are identical."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        check_mads = check_expected_results.check_mads_results(test_df, self.true_df)
        assert check_mads

    def test_success_en(self):
        """Ensure the test passes when results are identical for non-default column names."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["sample_number", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identification", "O_WGS MLST",
                                        "O_WGS res.gene other beta-lactamase",
                                        "O_WGS res.gene carbapenemase", "O_WGS res.gene colistin",
                                        "O_WGS res.gene vancomycin", "O_WGS Serotype/group",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplementary type",
                                        "O_WGS toxin identification_"])
        true_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["sample_number", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identification", "O_WGS MLST",
                                        "O_WGS res.gene other beta-lactamase",
                                        "O_WGS res.gene carbapenemase", "O_WGS res.gene colistin",
                                        "O_WGS res.gene vancomycin", "O_WGS Serotype/group",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplementary type",
                                        "O_WGS toxin identification_"])
        check_mads = check_expected_results.check_mads_results(test_df, true_df,
                                                               report_language = "EN")
        assert check_mads

    def test_success_multiline_results(self):
        """Ensure results on multiple lines (including duplicates) are handled correctly."""
        true_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             "IncFIB(K);IncX3", # plasmid ID 0
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA, # plasmid ID 0
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA, # plasmid ID 0
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS Plasmid-ID_0",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             "IncFIB(K);IncFIB(K)",  # plasmid ID 0
                                             "IncX3", # plasmid ID 1
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # plasmid ID 0
                                             pd.NA,  # plasmid ID 1
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # plasmid ID 0
                                             pd.NA,  # plasmid ID 1
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS Plasmid-ID_0", "O_WGS Plasmid-ID_1",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        check_mads = check_expected_results.check_mads_results(test_df, true_df)
        assert check_mads

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_handle_dynamic_columns(self):
        """Ensure the test handles different amounts of columns gracefully."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA,  # toxins
                                             pd.NA  # more toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA,  # toxins
                                             pd.NA  # more toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B",  # toxins
                                             "eae"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0",
                                        "O_WGS Toxinpåvisning_1"])
        mismatched_cols = pd.MultiIndex.from_arrays([["1199123456-3", "1199123456-3"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["O_WGS Toxinpåvisning"])
        mismatched = pd.DataFrame(data = [["STX2B", "eae;STX2B"]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        log_msg = "One or more analysis results do not match. " \
                  "Relevant sample(s):\n" \
                  f"{mismatched.to_string()}"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_mads = check_expected_results.check_mads_results(test_df, self.true_df)
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

        assert not check_mads

    def test_fail_missing_dynamic_column(self):
        """Alert if one of the column types for which multiple columns can be generated
        is missing."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA  # supplerende type
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA  # supplerende type
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA  # supplerende type
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type"])

        log_msg = "Missing categories {'O_WGS Toxinpåvisning'} from report for LIS."
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_mads = check_expected_results.check_mads_results(test_df, self.true_df)
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

        assert not check_mads

    def test_fail_extra_dynamic_column(self):
        """Alert if there is an additional column."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # plasmid
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # plasmid
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "pXYZ", # plasmid
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin",
                                        "O_WGS plasmid_0",
                                        "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])

        log_msg = "Unexpected categories {'O_WGS plasmid'} in report for LIS."
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_mads = check_expected_results.check_mads_results(test_df, self.true_df)
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

        assert not check_mads

    def test_fail_single_mismatch(self):
        """Check that the test fails when one sample has a mismatch."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "456",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        # columns should be reindexed to sample number instead
        mismatched_cols = pd.MultiIndex.from_arrays([["1199123456-1", "1199123456-1"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["O_WGS MLST"])
        mismatched = pd.DataFrame(data = [["123", "456"]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"One or more analysis results do not match. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            print(log_msg)
            check_frames_match = check_expected_results.check_mads_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_fail_multi_mismatch(self):
        """Check that the test fails and reports multiple mismatches correctly."""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "456",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaOXA-48",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        # columns should be reindexed to sample number instead
        mismatched_cols = pd.MultiIndex.from_arrays([["1199123456-1", "1199123456-1"],
                                                     ["expected", "found"]])
        mismatched_index = pd.Index(["O_WGS MLST", "O_WGS res.gen carbapenemase"])
        mismatched = pd.DataFrame(data = [["123", "456"], ["blaNDM-5", "blaOXA-48"]],
                                  columns = mismatched_cols,
                                  index = mismatched_index)
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"One or more analysis results do not match. " \
                      f"Relevant sample(s):\n" \
                      f"{mismatched.to_string()}"
            print(log_msg)
            check_frames_match = check_expected_results.check_mads_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_fail_missing_sample(self):
        """Check that the test catches a missing sample"""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        log_msg = "Missing samples {'1199123456-3'} from report for LIS"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_frames_match = check_expected_results.check_mads_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_fail_extra_sample(self):
        """Check that the test catches an additional sample"""
        test_df = pd.DataFrame(data = {"0": ["1199123456-1",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testococcus ignotus",  # Kraken ID
                                             "123",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             "blaNDM-5",  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             "FakeSerotype",  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "1": ["1199123456-2",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Testobacter fakei",  # Kraken ID
                                             "-",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             pd.NA,  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             pd.NA  # toxins
                                             ],
                                       "2": ["1199123456-3",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ],
                                       "3": ["1199123456-4",  # sample number
                                             pd.NA,  # CC type
                                             pd.NA,  # cgMLST
                                             "Placeholderia fakeorum",  # Kraken ID
                                             "321",  # MLST type
                                             pd.NA,  # other beta-lactamases
                                             pd.NA,  # carbapenemases
                                             "pmrB_Y358N",  # colistin resistance
                                             pd.NA,  # vancomycin resistance
                                             pd.NA,  # serotyping result
                                             pd.NA,  # Spatype
                                             pd.NA,  # ST
                                             pd.NA,  # supplerende type
                                             "STX2B"  # toxins
                                             ]
                                       },
                               index = ["proevenr", "O_WGS CC-type", "O_WGS cgMLST",
                                        "O_WGS identifikation", "O_WGS MLST",
                                        "O_WGS res.gen anden beta-lactamase",
                                        "O_WGS res.gen carbapenemase", "O_WGS res.gen colistin",
                                        "O_WGS res.gen vancomycin", "O_WGS Serotype/gruppe",
                                        "O_WGS Spatype", "O_WGS ST", "O_WGS supplerende type",
                                        "O_WGS Toxinpåvisning_0"])
        log_msg = "Unexpected samples {'1199123456-4'} in report for LIS"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_frames_match = check_expected_results.check_mads_results(test_df, self.true_df)
            assert not check_frames_match
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples


class TestGatherQC(unittest.TestCase):
    # expected results should contain coli or salmonella to catch a typing fail
    expected_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "clean_results"
    active_config = {"sample_number_settings":
                         {"sample_number_format":
                              r'([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'},
                     "lab_info_system":
                          {"bacteria_codes": "test/data/bacteria_codes.csv"},
                     "sequencing_mode": "illumina",
                         "language": "da"}

    def test_fail_unsupported_language(self):
        """Fail if an unsupported language is requested."""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "clean_results"
        error_msg = "tlh is not a supported language. Supported languages are ['da', 'en']."
        with pytest.raises(NotImplementedError, match = re.escape(error_msg)):
            check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                 active_config = self.active_config,
                                                 report_language = "tlh")

    @pytest.fixture(autouse = True)
    def inject_fixtures(self, caplog):
        self._caplog = caplog

    def test_all_qc_pass(self):
        """Check that the test passes if all steps pass"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "clean_results"
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            log_msg = f"All QC steps passed"
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert check_run
            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_success_nanopore(self):
        """Check that the test passes for a Nanopore run"""
        active_config = {"sample_number_settings":
                             {"sample_number_format":
                                  r'([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'},
                         "lab_info_system":
                             {"bacteria_codes": "test/data/bacteria_codes.csv"},
                         "sequencing_mode": "nanopore",
                         "language": "da"}
        run_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                   / "clean_results_nanopore")
        expected_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                   / "clean_results_nanopore")
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            log_msg = f"All QC steps passed"
            check_run = check_expected_results.check_run_dir(run_dir, expected_dir,
                                                             active_config = active_config)
            assert check_run
            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_success_nanopore_en(self):
        """Check that the test passes for a Nanopore run with non-default columns"""
        active_config = {"sample_number_settings":
                             {"sample_number_format":
                                  r'([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'},
                         "lab_info_system":
                             {"bacteria_codes": "test/data/bacteria_codes_en.csv"},
                         "sequencing_mode": "nanopore",
                         "language": "en"}
        run_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                   / "clean_results_nanopore_en")
        expected_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                        / "clean_results_nanopore_en")
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            log_msg = f"All QC steps passed"
            check_run = check_expected_results.check_run_dir(run_dir, expected_dir,
                                                             active_config = active_config,
                                                             report_language = "EN",
                                                             bacteria_names = BACT_NAMES_EN)
            assert check_run
            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_success_nanopore_use_config_language(self):
        """Check that the language set in the config is used if no report language is given."""
        active_config = {"sample_number_settings":
                             {"sample_number_format":
                                  r'([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}'},
                         "lab_info_system":
                             {"bacteria_codes": "test/data/bacteria_codes_en.csv"},
                         "sequencing_mode": "nanopore",
                         "language": "en"}
        run_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                   / "clean_results_nanopore_en")
        expected_dir = (pathlib.Path(__file__).parent / "data" / "check_pipeline_qc"
                        / "clean_results_nanopore_en")
        with self._caplog.at_level(level="INFO", logger="QA_Test"):
            log_msg = f"All QC steps passed"
            check_run = check_expected_results.check_run_dir(run_dir, expected_dir,
                                                             active_config = active_config,
                                                             bacteria_names = BACT_NAMES_EN)

            assert check_run
            assert ("QA_Test", logging.INFO, log_msg) in self._caplog.record_tuples

    def test_fail_missing_expected_dir(self):
        """Check that the test fails if the quality report fails"""
        unexpected_dir = pathlib.Path(
            __file__).parent / "data" / "check_pipeline_qc" / "no_such_run"
        with pytest.raises(FileNotFoundError,
                           match = "Could not find directory with expected results."):
            check_expected_results.check_run_dir(self.expected_dir, unexpected_dir,
                                                             active_config = self.active_config)

    def test_fail_missing_run_dir(self):
        """Check that the test fails if the quality report fails"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "no_such_run"
        with pytest.raises(FileNotFoundError,
                           match = "Could not find result directory for current run."):
            check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)

    def test_fail_quality_report(self):
        """Check that the test fails if the quality report fails"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "qc_report_fail"
        with self._caplog.at_level(level="ERROR", logger="QA_Test"):
            log_msg = f"One or more QC steps failed. Check log for details."
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.ERROR, log_msg) in self._caplog.record_tuples

    def test_fail_serotyping(self):
        """Check that the test fails if serotyping checks fail"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "run_no_typing"
        with self._caplog.at_level(level="ERROR", logger="QA_Test"):
            log_msg = f"One or more QC steps failed. Check log for details."
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.ERROR, log_msg) in self._caplog.record_tuples

    def test_fail_mads_report(self):
        """Check that the test fails if MADS report checks fail"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "mads_report_fail"
        with self._caplog.at_level(level="ERROR", logger="QA_Test"):
            log_msg = f"One or more QC steps failed. Check log for details."
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.ERROR, log_msg) in self._caplog.record_tuples

    def test_missing_mads_report(self):
        """Check that the test fails if MADS report is missing"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "no_mads_report"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Missing report for LIS from ['{run_dir}']. \n" \
                      f"Skipping comparison of LIS reports."
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples

    def test_missing_qc_report(self):
        """Check that the test fails if QC report is missing"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "no_qc_report"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            log_msg = f"Missing QC report from ['{run_dir}']. \n" \
                      f"Skipping comparison of QC reports."
            skip_serotyping_msg = f"Missing QC report from current run dir. \n" \
                                  f"Cannot infer species for serotyping; skipping."
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.WARNING, log_msg) in self._caplog.record_tuples
            assert ("QA_Test", logging.WARNING, skip_serotyping_msg) in self._caplog.record_tuples

    def test_check_missing_files(self):
        """Check that the test fails if files are missing"""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "missing_files"
        error_msg = "The following file(s) were missing from the results:\n" \
                    "Kraken report\n" \
                    "QUAST report"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.WARNING, error_msg) in self._caplog.record_tuples

    def test_check_extra_sample(self):
        """Check that the test fails if an additional sample is found."""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "extra_sample"
        warn_msg = "Unexpected samples {'1199123456-7'} in result directory"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.WARNING, warn_msg) in self._caplog.record_tuples

    def test_check_missing_sample(self):
        """Check that the test fails if a sample is missing."""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "missing_sample"
        warn_msg = "Missing samples {'1199123456-1'} from result directory"
        with self._caplog.at_level(level="WARNING", logger="QA_Test"):
            check_run = check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)
            assert not check_run
            assert ("QA_Test", logging.WARNING, warn_msg) in self._caplog.record_tuples

    def test_check_no_samples_current_run(self):
        """Check that the test detects missing samples in the current run."""
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "no_samples"
        error_msg = "No sample directories found in current run dir."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            check_expected_results.check_run_dir(run_dir, self.expected_dir,
                                                             active_config = self.active_config)

    def test_check_no_samples_expected(self):
        """Check that the test detects missing samples in the expected results."""
        expected_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "no_samples"
        run_dir = pathlib.Path(__file__).parent / "data" / "check_pipeline_qc" / "clean_results"
        error_msg = "No sample directories found in directory with expected results."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            check_expected_results.check_run_dir(run_dir, expected_dir,
                                                             active_config = self.active_config)
