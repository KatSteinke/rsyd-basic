#  Copyright (c) 2023 Kat Steinke.
#   This program is distributed under version 3 of the GNU General Public License.
#    You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib
import re
import unittest

import pandas as pd
import pytest

import prep_samples_from_sheet
from helpers import MissingReadfilesError
from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames

# set up some translations we're going to be using over and over
SHEET_NAMES_EN = RunsheetNames(sample_info_sheet = "Sample_information",
                               sample_number = "Sample_number",
                               extraction_well_number = "Extraction_well_number",
                               extraction_run_number = "Extraction_run_number",
                               dna_concentration = "DNA_concentration",
                               requested_by = "Requested_by",
                               comment = "Comment",
                               sequencing_run_number = "sequencing_run_number",
                               indications = ["AMR", "Identification", "Relapse", "HAI",
                                              "Toxin gene identification",
                                              "Environmental sample isolate",
                                              "Outbreak sample isolate",
                                              "Surveillance sample isolate", "Research",
                                              "Other"],
                               dna_normalization_sheet = "DNA normalization",
                               dna_normalization_sample_nr = "Sample_number",
                               ilm_well_number = "ILM_well_position",
                               plate_layout = "Plate_layout",
                               runsheet_base = "Runsheet",
                               experiment_name = "RUNxxxx-INI",
                               nanopore_sheet_sample_number = "Sample_number",
                               barcode = "Barcode",
                               protocol_version = "Lab protocol version")
LIS_NAMES_EN = LISDataNames(sample_number = "SAMPLENR", sample_type = "SAMPLE_TYPE",
                            isolate_number = "BACT_NR",
                            bacteria_code = "LOCAL_BACT_CODE",
                            bacteria_text = "LOCAL_BACT_TEXT")
BACT_NAMES_EN = BacteriaMappingNames(bacteria_code = "bacteria_code",
                                     internal_bacteria_text = "bacteria_text",
                                     bacteria_category = "bacteria_category")


class TestGetFromSamplesheet(unittest.TestCase):
    def test_get_samples_success(self):
        workflow_config = {"sample_number_settings":
                               {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "sample_numbers_in": "letter",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "letter",
                                "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                                },
                           "sequencing_mode": "illumina"}
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single.xlsx")
        true_bactopia_filenames = pd.DataFrame(data={"sample": ["B99123456-1"],
                                                     "runtype": ["paired-end"],
                                                     "r1": [rawdata_dir
                                                            / "B99123456-1_L001_R1_001.fastq.gz"],
                                                     "r2": [rawdata_dir
                                                            / "B99123456-1_L001_R2_001.fastq.gz"]})
        test_bactopia_filenames = prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data,
                                                                                          rawdata_dir,
                                                                                          active_config = workflow_config)
        pd.testing.assert_frame_equal(true_bactopia_filenames, test_bactopia_filenames)

    def test_get_samples_success_translate(self):
        """Successfully get samples when using non-default column names in sample sheet"""
        workflow_config = {"sample_number_settings":
                               {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "sample_numbers_in": "letter",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "letter",
                                "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                                },
                           "sequencing_mode": "illumina"}
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single_en.xlsx")
        true_bactopia_filenames = pd.DataFrame(data={"sample": ["B99123456-1"],
                                                     "runtype": ["paired-end"],
                                                     "r1": [rawdata_dir
                                                            / "B99123456-1_L001_R1_001.fastq.gz"],
                                                     "r2": [rawdata_dir
                                                            / "B99123456-1_L001_R2_001.fastq.gz"]})
        test_bactopia_filenames = prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data,
                                                                                          rawdata_dir,
                                                                                          active_config = workflow_config,
                                                                                          runsheet_names = SHEET_NAMES_EN)
        pd.testing.assert_frame_equal(true_bactopia_filenames, test_bactopia_filenames)

    def test_translate_sample_number(self):
        """Translate sample number to output format if required."""
        workflow_config = {"sample_number_settings":
                               {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[BDFT])(?P<sample_number>\d{6})(?P<sample_year>\d{2})(?P<bact_number>-\d)',
                                "sample_numbers_in": "letter",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "letter",
                                "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                                },
                           "sequencing_mode": "illumina"}
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single.xlsx")
        true_bactopia_filenames = pd.DataFrame(data = {"sample": ["B12345699-1"],
                                                       "runtype": ["paired-end"],
                                                       "r1": [rawdata_dir
                                                              / "B99123456-1_L001_R1_001.fastq.gz"],
                                                       "r2": [rawdata_dir
                                                              / "B99123456-1_L001_R2_001.fastq.gz"]})
        test_bactopia_filenames = prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data,
                                                                                          rawdata_dir,
                                                                                          active_config = workflow_config)
        pd.testing.assert_frame_equal(true_bactopia_filenames, test_bactopia_filenames)

    def test_missing_one(self):
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_missing"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single.xlsx")
        error_msg = f"R1 not found in {str(rawdata_dir)} for samples ['B99123456-1']. \n"
        with pytest.raises(MissingReadfilesError, match = re.escape(error_msg)):
            prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data, rawdata_dir)

    def test_missing_both(self):
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_letter.xlsx")
        error_msg = f"R1 not found in {str(rawdata_dir)} for samples ['B99123456-2']. \n" \
                    f"R2 not found in {str(rawdata_dir)} for samples ['B99123456-2']"
        with pytest.raises(MissingReadfilesError, match = re.escape(error_msg)):
            prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data, rawdata_dir)

    def test_not_in_mads(self):
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single.xlsx")
        fake_mads = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99654321-1"]})
        error_msg = "The following numbers were not found in the MADS report:\nB99123456-1"
        with pytest.raises(KeyError, match=error_msg):
            prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data, rawdata_dir,
                                                                    lis_report = fake_mads)

    def test_not_in_mads_translate(self):
        """Catch samples missing from LIS report when sample sheet and LIS report have non-default
        column names."""
        rawdata_dir = pathlib.Path(__file__).parent / "data" / "utilities_test" / "rawdata_correct"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                    / "success_sheet_single_en.xlsx")
        fake_mads = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99654321-1"]})
        error_msg = "The following numbers were not found in the MADS report:\nB99123456-1"
        with pytest.raises(KeyError, match=error_msg):
            prep_samples_from_sheet.get_samples_from_illumina_sheet(run_data, rawdata_dir,
                                                                    lis_report = fake_mads,
                                                                    runsheet_names = SHEET_NAMES_EN,
                                                                    lis_data_names = LIS_NAMES_EN,
                                                                    bacteria_names = BACT_NAMES_EN)


class TestGetFromNanopore(unittest.TestCase):
    workflow_config = {"sample_number_settings":
                           {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                            "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "format_output": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                            "sample_numbers_in": "letter",
                            "sample_numbers_out": "letter",
                            "sample_numbers_report": "letter",
                            "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                            },
                       "sequencing_mode": "nanopore"}

    def test_get_nanopore_success(self):
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                          / "test_nanopore_runsheet.xlsx")
        nanopore_result = pd.DataFrame(data={"sample": ["B99123456-1", "B99123456-2"],
                                             "runtype": ["ont", "ont"],
                                             "extra": [(result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01"),
                                                       (result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode42")]})
        nanopore_test = prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet,
                                                                                result_base,
                                                                                active_config = self.workflow_config)
        pd.testing.assert_frame_equal(nanopore_test, nanopore_result)

    def test_get_nanopore_success_multi_fastq(self):
        """Get the correct directory when a fastq_pass directory is explicitly specified in a dir
        with multiple fastq_pass dirs"""
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir_multi" / "rawdata" / "subdir" / "fastq_pass"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                          / "test_nanopore_runsheet.xlsx")
        nanopore_result = pd.DataFrame(data={"sample": ["B99123456-1", "B99123456-2"],
                                             "runtype": ["ont", "ont"],
                                             "extra": [(result_base / "barcode01"),
                                                       (result_base / "barcode42")]})
        nanopore_test = prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet,
                                                                                result_base,
                                                                                active_config = self.workflow_config)
        pd.testing.assert_frame_equal(nanopore_test, nanopore_result)

    def test_get_nanopore_success_translate(self):
        """Get sample overview from Nanopore runsheet with non-default column names."""
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                          / "test_nanopore_runsheet_en.xlsx")
        nanopore_result = pd.DataFrame(data={"sample": ["B99123456-1", "B99123456-2"],
                                             "runtype": ["ont", "ont"],
                                             "extra": [(result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01"),
                                                       (result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode42")]})
        nanopore_test = prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet,
                                                                                result_base,
                                                                                active_config = self.workflow_config,
                                                                                runsheet_names = SHEET_NAMES_EN,
                                                                                lis_data_names = LIS_NAMES_EN,
                                                                                bacteria_names = BACT_NAMES_EN)
        pd.testing.assert_frame_equal(nanopore_test, nanopore_result)

    def test_translate_sample_number_success(self):
        """Translate sample number to output format if required."""
        workflow_config = {"sample_number_settings":
                               {"sample_number_format": '([135]0|11|[BDFT])[0-9]{8}-[0-9]{1}',
                                "format_in_sheet": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_in_lis": r'(?P<sample_type>[BDFT])(?P<sample_year>\d{2})(?P<sample_number>\d{6})(?P<bact_number>-\d)',
                                "format_output": r'(?P<sample_type>[BDFT])(?P<sample_number>\d{6})(?P<sample_year>\d{2})(?P<bact_number>-\d)',
                                "sample_numbers_in": "letter",
                                "sample_numbers_out": "letter",
                                "sample_numbers_report": "letter",
                                "number_to_letter": {"30": "B", "10": "D", "11": "F", "50": "T"}
                                },
                           "sequencing_mode": "nanopore"}
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_nanopore_runsheet.xlsx")
        nanopore_result = pd.DataFrame(data={"sample": ["B12345699-1", "B12345699-2"],
                                             "runtype": ["ont", "ont"],
                                             "extra": [(result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01"),
                                                       (result_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode42")]})
        nanopore_test = prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet,
                                                                                result_base,
                                                                                active_config = workflow_config)
        pd.testing.assert_frame_equal(nanopore_test, nanopore_result)

    def test_fail_no_barcodes(self):
        """Fail if no barcodes are given."""
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                          / "test_nanopore_runsheet_no_barcodes.xlsx")
        error_msg = f"No barcodes given for sample(s) ['B99123456-1', 'B99123456-2']."
        with pytest.raises(ValueError, match=re.escape(error_msg)):
            prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet, result_base,
                                                                    active_config = self.workflow_config)

    def test_no_nanopore_dir(self):
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "no_such_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_nanopore_runsheet.xlsx")
        with pytest.raises(FileNotFoundError, match=re.escape("Run directory not found.")):
            prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet, result_base,
                                                                    active_config = self.workflow_config)

    def test_no_base_dir(self):
        result_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "no_such_dir"
        nanopore_sheet = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_nanopore_runsheet.xlsx")
        error_msg = "Run directory not found."
        with pytest.raises(FileNotFoundError, match = re.escape(error_msg)):
            prep_samples_from_sheet.get_samples_from_nanopore_sheet(nanopore_sheet, result_base,
                                                                    active_config = self.workflow_config)

    def test_not_in_mads(self):
        rawdata_dir = pathlib.Path(
            __file__).parent / "data" / "prep_sheets" / "test_dir"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_nanopore_runsheet.xlsx")
        fake_mads = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99654321-1"]})
        error_msg = ("The following numbers were not found in the MADS report:"
                     "\nB99123456-1"
                     "\nB99123456-2")
        with pytest.raises(KeyError, match = error_msg):
            prep_samples_from_sheet.get_samples_from_nanopore_sheet(run_data, rawdata_dir,
                                                                    lis_data = fake_mads,
                                                                    active_config = self.workflow_config)

    def test_not_in_mads_translate(self):
        """Complain if samples aren't found in the LIS report when column names are non-default-"""
        rawdata_dir = pathlib.Path(
            __file__).parent / "data" / "prep_sheets" / "test_dir"
        run_data = (pathlib.Path(__file__).parent / "data" / "utilities_test"
                           / "test_nanopore_runsheet_en.xlsx")
        fake_mads = pd.DataFrame(data = {"proevenr": ["F99123456-1", "F99654321-1"]})
        error_msg = ("The following numbers were not found in the MADS report:"
                     "\nB99123456-1"
                     "\nB99123456-2")
        with pytest.raises(KeyError, match = error_msg):
            prep_samples_from_sheet.get_samples_from_nanopore_sheet(run_data, rawdata_dir,
                                                                    lis_data = fake_mads,
                                                                    active_config = self.workflow_config,
                                                                    runsheet_names = SHEET_NAMES_EN,
                                                                    lis_data_names = LIS_NAMES_EN,
                                                                    bacteria_names = BACT_NAMES_EN)


class TestMergeIlluminaNanopore(unittest.TestCase):
    def setUp(self) -> None:
        self.illumina_base = pathlib.Path(__file__).parent / "data" / "utilities_test" \
                             / "rawdata_correct"
        self.nanopore_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" / "test_dir"

    def test_matching_samples(self):
        illumina_samples = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                                "runtype": ["paired-end"],
                                                "r1": [self.illumina_base
                                                       / "B99123456-1_L001_R1_001.fastq.gz"],
                                                "r2": [self.illumina_base
                                                       / "B99123456-1_L001_R2_001.fastq.gz"]})
        nanopore_samples = pd.DataFrame(data={"sample": ["B99123456-1"],
                                              "runtype": ["ont"],
                                              "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                        / "fastq_pass" / "barcode01")]})
        true_merged = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                           "runtype": ["hybrid"],
                                           "r1": [self.illumina_base
                                                  / "B99123456-1_L001_R1_001.fastq.gz"],
                                           "r2": [self.illumina_base
                                                  / "B99123456-1_L001_R2_001.fastq.gz"],
                                           "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                      / "fastq_pass" / "barcode01")]
                                           })
        test_merged = prep_samples_from_sheet.merge_illumina_nanopore_samples(illumina_samples,
                                                                              nanopore_samples)
        pd.testing.assert_frame_equal(test_merged, true_merged)

    def test_extra_nanopore(self):
        illumina_samples = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                                "runtype": ["paired-end"],
                                                "r1": [self.illumina_base
                                                       / "B99123456-1_L001_R1_001.fastq.gz"],
                                                "r2": [self.illumina_base
                                                       / "B99123456-1_L001_R2_001.fastq.gz"]})
        nanopore_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["ont", "ont"],
                                                "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                           / "fastq_pass" / "barcode01"),
                                                          (self.nanopore_base / "rawdata" / "subdir"
                                                           / "fastq_pass" / "barcode42")]})
        true_merged = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                           "runtype": ["hybrid", "ont"],
                                           "r1": [self.illumina_base
                                                  / "B99123456-1_L001_R1_001.fastq.gz", pd.NA],
                                           "r2": [self.illumina_base
                                                  / "B99123456-1_L001_R2_001.fastq.gz", pd.NA],
                                           "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                      / "fastq_pass" / "barcode01"),
                                                     (self.nanopore_base / "rawdata" / "subdir"
                                                      / "fastq_pass" / "barcode42")]
                                           })
        with self.assertLogs("prepare_input_files") as logged:
            test_merged = prep_samples_from_sheet.merge_illumina_nanopore_samples(illumina_samples,
                                                                                  nanopore_samples)
            warn_msg = 'WARNING:prepare_input_files:Hybrid run specified but some samples only have ' \
                       'Nanopore reads: B99123456-2'
            assert warn_msg in logged.output
            pd.testing.assert_frame_equal(true_merged, test_merged)

    def test_extra_illumina(self):
        multi_illumina_base = pathlib.Path(__file__).parent / "data" / "prep_sheets" \
                              / "multi_illumina"
        illumina_samples = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                                "runtype": ["paired-end", "paired-end"],
                                                "r1": [multi_illumina_base
                                                       / "B99123456-1_L001_R1_001.fastq.gz",
                                                       multi_illumina_base
                                                       / "B99123456-2_L001_R1_001.fastq.gz"
                                                       ],
                                                "r2": [multi_illumina_base
                                                       / "B99123456-1_L001_R2_001.fastq.gz",
                                                       multi_illumina_base
                                                       / "B99123456-2_L001_R2_001.fastq.gz"
                                                       ]})
        nanopore_samples = pd.DataFrame(data = {"sample": ["B99123456-1"],
                                                "runtype": ["ont"],
                                                "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                           / "fastq_pass" / "barcode01")]})
        true_merged = pd.DataFrame(data = {"sample": ["B99123456-1", "B99123456-2"],
                                           "runtype": ["hybrid", "paired-end"],
                                           "r1": [multi_illumina_base
                                                  / "B99123456-1_L001_R1_001.fastq.gz",
                                                  multi_illumina_base
                                                  / "B99123456-2_L001_R1_001.fastq.gz"],
                                           "r2": [multi_illumina_base
                                                  / "B99123456-1_L001_R2_001.fastq.gz",
                                                  multi_illumina_base
                                                  / "B99123456-2_L001_R2_001.fastq.gz"],
                                           "extra": [(self.nanopore_base / "rawdata" / "subdir"
                                                      / "fastq_pass" / "barcode01"), pd.NA]
                                           })
        with self.assertLogs("prepare_input_files") as logged:
            test_merged = prep_samples_from_sheet.merge_illumina_nanopore_samples(illumina_samples,
                                                                                  nanopore_samples)
            warn_msg = 'WARNING:prepare_input_files:Hybrid run specified but some samples only have ' \
                       'Illumina reads: B99123456-2'
            assert warn_msg in logged.output
            pd.testing.assert_frame_equal(true_merged, test_merged)
