"""Extract information from Nanopore report."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
import pathlib
import re
from argparse import ArgumentParser
from typing import NamedTuple, Optional

import ijson
import packaging.version as pkg_version
import pandas as pd

import set_log

MEDAKA_VERSIONS = pathlib.Path(__file__).parent / "data" / "nanopore" / "medaka_models.txt"

logger = logging.getLogger("parse_nanopore")


# TODO: do we need something fancier?
class BasecallModel(NamedTuple):
    """A Nanopore basecalling model.

    Attributes:
        flowcell_version:       the flowcell version (e.g. r9.4.1 for "old" chemistry,
                                r10.4.1 for "new")
        basecalling_mode:       the basecalling mode used (fast, high accuracy, super accuracy,...)
        basecaller:             the basecaller used for basecalling
                                (guppy or dorado; inferred from original version string
                                - starts with g for guppy)
        basecaller_version:     the version of the basecaller used for basecalling in a format
                                for easy comparison
        kit_chemistry:          the kit version, if given
        translocation_speed:    the speed of DNA translocation, if given
        flowcell_type:          the type of flowcell (min for MinION, prom for PromethION), if given
    """
    flowcell_version: str
    basecalling_mode: str
    basecaller: str
    basecaller_version: pkg_version.Version
    kit_chemistry: Optional[str] = None
    translocation_speed: Optional[str] = None
    flowcell_type: Optional[str] = None


# TODO: should this also be a BasecallModel? Dataframe is better for handling for now,
#  unless we add a to_frame method
def parse_medaka_version(model_name: str) -> pd.DataFrame:
    """Extract information on flowcell version,  basecalling mode, basecaller version
    and optionally flowcell type, kit chemistry and translocation speed.

    Arguments:
        model_name: name of the model to be used in medaka

    Returns:
        The model with name, flowcell version, basecalling mode, basecaller version,
         and optional specifications

    Raises:
        ValueError: if the model name is missing key information
    """
    model_pattern = re.compile(r"(?P<flowcell_version>r\d{2,4})"
                               r"(?:_(?P<kit_chemistry>e\d{1,2})(?:_(?P<translocation>\d+bps))?)?"
                               r"(?:_(?P<flowcell_type>min|prom))?"
                               r"_(?P<basecalling_mode>h(?:igh|ac)|fast|sup(?:_plant)?)"
                               r"_?(?:(?P<guppy_version>g\d{3,4})"
                               r"|(?P<dorado_version>v\d+\.\d+\.\d+))?")
    model_data = pd.DataFrame(data={"model_name": [model_name]})
    model_data = pd.concat([model_data, model_data["model_name"].str.extract(model_pattern)],
                           axis = 1)
    # check if all required elements are there
    required = ["flowcell_version", "basecalling_mode"]
    missing_required = model_data[required].columns[model_data[required].isna().any()].tolist()
    if missing_required:
        raise ValueError(f"Model name {model_name} is missing required elements.")
    if model_data["guppy_version"].isna().any() and model_data["dorado_version"].isna().any():
        raise ValueError(f"Model name {model_name} does not contain a guppy or dorado version.")
    # we can live with the wonky numbering for flowcell version and kit chemistry
    # but guppy needs proper parseable numbers, if we have guppy
    if model_data["guppy_version"].notna().all():
        model_data["guppy_version"] = model_data["guppy_version"].apply(lambda version:
                                                                        ".".join(
                                                                            re.search(r"(?P<major>\d)"
                                                                                      r"(?P<minor>\d)"
                                                                                      r"(?P<patch>\d{1,2})",
                                                                                      version).groups())
                                                                        if re.search(r"(?P<major>\d)"
                                                                                     r"(?P<minor>\d)"
                                                                                     r"(?P<patch>\d{1,2})",
                                                                                     version)
                                                                        else version)
    return model_data


# get basecalling model from report
def get_current_run_params(run_report: pathlib.Path) -> BasecallModel:
    """Extract basecalling parameters from Nanopore run report.

    Arguments:
        run_report: the run report in JSON format

    Returns:
        Basecalling parameters for the given run.

    Raises:
        ValueError: if the run report is missing required elements or is otherwise malformed.
    """
    current_run = {}
    with open(run_report, mode="r", encoding="utf-8") as gridion_report:
        # get flowcell version and basecalling mode from guppy command (protocol_run_info.args)
        # TODO: more elegant handling of generators!!!
        # TODO: use known structure, don't loop back to the start
        protocol_info = ijson.items(gridion_report, "protocol_run_info.args")

        find_guppy_filename = re.search(r"--guppy_filename=(?P<guppy_name>.+)\.cfg,",
                                        ",".join(next(protocol_info)))
        if not find_guppy_filename:
            raise ValueError("Guppy filename not found in guppy arguments")

        guppy_filename = find_guppy_filename.groupdict().get("guppy_name")
        find_guppy_info = re.search(r"[a-z+]_(?P<flowcell_version>r\d+\.\d+\.\d+)"
                                    r"(?:_(?P<kit_chemistry>e\d\.\d))?"
                                    r"(?:_(?P<translocation_speed>\d+bps))?"
                                    r"(?:_(?P<frequency>\d+khz))?"
                                    r"_(?P<basecall_mode>[a-z]+)", guppy_filename)
        if not find_guppy_info:
            raise ValueError(f"Malformed guppy filename {guppy_filename}"
                             " - data could not be extracted")
        # match flowcell version format in the model list
        current_run["flowcell_version"] = find_guppy_info.groupdict().get("flowcell_version")
        if current_run["flowcell_version"]:
            current_run["flowcell_version"] = current_run["flowcell_version"].replace(".","")

        current_run["kit_chemistry"] = find_guppy_info.groupdict().get("kit_chemistry")
        if current_run["kit_chemistry"]:
            current_run["kit_chemistry"] = current_run["kit_chemistry"].replace(".","")
        current_run["translocation_speed"] = find_guppy_info.groupdict().get("translocation_speed")
        current_run["basecalling_mode"] = find_guppy_info.groupdict().get("basecall_mode")
        # back to the start
        gridion_report.seek(0)
        # get flowcell type from flow_cell.product_code - TODO: seems like it's optional?
        flowcell_code = next(ijson.items(gridion_report,
                                         "protocol_run_info.flow_cell.product_code"))
        if "MIN" in flowcell_code:
            current_run["flowcell_type"] = "min"
        elif "PRO" in flowcell_code:
            current_run["flowcell_type"] = "prom"
        else:
            raise ValueError(f"Flowcell type could not be extracted from flowcell code "
                             f"{flowcell_code}.")

        # back to the start
        gridion_report.seek(0)
        # get guppy version from version information
        # Nanopore has changed their format at least once, account for this
        try:
            version_info = next(ijson.items(gridion_report,
                                            "software_versions.guppy_build_version"))
        except StopIteration:
            logger.info("Software version not detected in old format,"
                        " trying new format.")
            gridion_report.seek(0)
            version_info = next(ijson.items(gridion_report,
                                            "protocol_run_info.software_versions.guppy_build_version"))
        find_guppy_version = re.search(r"(?P<version>\d+\.\d+.\d+)\+.+", version_info)
        if not find_guppy_version:
            raise ValueError(f"Malformed guppy version {version_info}"
                             " - data could not be extracted")
        current_run["basecaller"] = "guppy"
        current_run["basecaller_version"] = pkg_version.parse(find_guppy_version.group("version"))
    run_params = BasecallModel(**current_run)
    return run_params


# get guppy model - piece together from flow cell, guppy version, and guppy command
def get_best_medaka_model(run_report: pathlib.Path) -> str:
    """Output best medaka model for the GridION run in the report.

    Arguments:
        run_report:         the run report in JSON format

    Returns:
        The best medaka model to use for the GridION run.

    Raises:
        ValueError: if the information in the report doesn't match an existing model
    """
    # TODO: move to separate function
    # use list of medaka models as pipeline data
    with open(MEDAKA_VERSIONS, "r", encoding = "utf-8") as medaka_modelfile:
        medaka_models = medaka_modelfile.readline().strip().split()
    # for each medaka model: get flowcell version, flowcell type, basecalling mode, guppy version
    all_medaka_models = pd.concat([parse_medaka_version(medaka_model)
                                   for medaka_model in medaka_models])
    # right now we only support guppy
    all_medaka_models = all_medaka_models.dropna(subset=["guppy_version"])
    all_medaka_models["guppy_version"] = all_medaka_models["guppy_version"].apply(pkg_version.parse)
    run_params = get_current_run_params(run_report)
    # complain if guppy version is too low
    if all(run_params.basecaller_version < all_medaka_models["guppy_version"]):
        raise ValueError(f"Guppy version {run_params.basecaller_version} "
                         f"is too low to be supported.")
    # complain if detected basecalling mode is one we haven't found
    if run_params.basecalling_mode not in all_medaka_models["basecalling_mode"].unique():
        raise ValueError(f"Basecalling type {run_params.basecalling_mode} is not supported.")
    # otherwise find the model with the highest guppy version that has the correct
    # flowcell version/type (if given) and basecalling mode. Flowcell type optional
    # hard filtering criteria: flowcell version, basecalling mode
    applicable_models = all_medaka_models.query("flowcell_version == @run_params.flowcell_version"
                                                " & basecalling_mode"
                                                " == @run_params.basecalling_mode")
    # TODO: filter more nicely!
    # if flowcell type present in those we get, filter by this
    models_no_type = applicable_models[applicable_models["flowcell_type"].isna()]
    models_match_type = applicable_models.query("flowcell_type == @run_params.flowcell_type")
    models_by_type = pd.concat([models_no_type, models_match_type])
    # if translocation speed present, filter by that as well
    models_no_speed = models_by_type[models_by_type["translocation"].isna()]
    models_match_speed = models_by_type.query("translocation == @run_params.translocation_speed")
    models_by_speed = pd.concat([models_match_speed, models_no_speed])
    # check if we still have something
    if models_by_speed.empty:
        raise ValueError("No models with the desired combination of flowcell version,"
                         " basecalling mode, translocation speed and/or flowcell type found.")
    # get all with a version number lesser than or equal to our guppy version and get the best
    models_match_version = models_by_speed.query("guppy_version <= @run_params.basecaller_version")
    # TODO: nicer way to get this?
    best_guppy_model = models_match_version.query("guppy_version "
                                                  "== guppy_version.max()")["model_name"].squeeze()
    return best_guppy_model


if __name__ == "__main__":
    logger = set_log.get_stream_log(level="WARNING")
    logger.setLevel(logging.INFO)
    arg_parser = ArgumentParser("Extract and print best suited Medaka model from Nanopore report.")
    arg_parser.add_argument("nanopore_report", help="Nanopore report in json format")
    args = arg_parser.parse_args()
    nanopore_report = pathlib.Path(args.nanopore_report)
    guppy_model = get_best_medaka_model(nanopore_report)
    print(guppy_model)
