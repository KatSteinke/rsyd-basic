"""Concatenate various reports, with special handling for Quast reports, NG50 data and
 MADS reports."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib

from argparse import ArgumentParser
from typing import List

import pandas as pd
import yaml

import localization_helpers
import pipeline_config
import version

from generate_reports import sort_for_mads

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF


def concat_generic_reports(generic_reports: List[pathlib.Path], delimiter: str) -> pd.DataFrame:
    """Concatenate delimiter-separated reports from the same tool for different samples where no
    additional processing is required.

    Arguments:
        generic_reports:    the paths to the reports to concatenate
        delimiter:          the delimiter used in all reports (usually comma, semicolon or tab)

    Returns:
        All reports joined into a single DataFrame

    Raises:
        ValueError: if not all reports have the same columns
                    (indicating reports from different tools)

    """
    # TODO: what if the wrong delimiter is given? Need to rely on pandas for now
    reports_data = [pd.read_csv(report, sep = delimiter) for report in generic_reports]
    # columns should all be the same - if we have different colnames, something's wrong
    # TODO: optimize?
    # track columns found
    columns_found = set()
    # track mismatches
    column_mismatches = set()
    # go over columns
    # we only need to see it once to call it - no need to loop through everything
    i = 0
    while i < len(reports_data) and not column_mismatches:
        # get the columns
        report_data = reports_data[i]
        current_columns = set(report_data.columns.tolist())
        # the first time, record which columns we've seen
        if not columns_found:
            columns_found = current_columns
        # afterwards, see if the current set of columns matches what we've seen in the first one
        column_mismatches = (current_columns - columns_found).union(columns_found - current_columns)
        i += 1

    # TODO: identify the mismatched columns?
    if column_mismatches:
        raise ValueError("Column names aren't identical for all reports. "
                         "This may indicate trying to concatenate different types of reports.")

    all_reports = pd.concat(reports_data, ignore_index = True)
    return all_reports


def reshape_quast_reads(quast_reads: pd.DataFrame) -> pd.DataFrame:
    """
    Reshape and rename QUAST read stats for ease of merging.

    Arguments:
        quast_reads:    A dataframe of QUAST's read_stats output
    Returns:
        QUAST's read_stats output, transposed to match the other transposed report.
    """
    # set the first column as index so it becomes a header, then reset to get the data out of the
    # index again
    reads_reshaped = quast_reads.set_index("Assembly").transpose(copy = True).reset_index()
    reads_reshaped = reads_reshaped.rename(columns = {"index": "proevenr"})
    reads_reshaped.columns.names = [None]
    return reads_reshaped[["proevenr", "Avg. coverage depth"]]


def get_quast_ng50(single_quast_report: pathlib.Path) -> pd.DataFrame:
    """Get NG50 for a single sample from a transposed QUAST report, skipping the header line.

    Arguments:
        single_quast_report:    transposed single sample QUAST report to extract NG50 value from
    Returns:
        The NG50 value for the sample specified
    """
    single_quast_data = pd.read_csv(single_quast_report, sep = "\t", dtype = {"NG50": "Int64"})
    quast_ng50_singlesample = pd.DataFrame(data = {"proevenr": single_quast_data["Assembly"]})
    if "NG50" not in single_quast_data.columns:
        quast_ng50_singlesample["NG50"] = pd.NA
    else:
        quast_ng50_singlesample["NG50"] = single_quast_data["NG50"]
    return quast_ng50_singlesample


def concat_quast_coverage(quast_coverage_reports: List[pathlib.Path]) -> pd.DataFrame:
    """Reshape multiple QUAST read_stats files and merge the reshaped data.

    Arguments:
        quast_coverage_reports: paths to QUAST reads_stats files

    Returns:
        All read_stats reports in wide format combined
    """
    illumina_read_results = []
    for quast_coverage_file in quast_coverage_reports:
        quast_read_data = pd.read_csv(quast_coverage_file, sep = "\t")
        readfile_transposed = reshape_quast_reads(quast_read_data)
        illumina_read_results.append(readfile_transposed)
    all_illumina_results = pd.concat(illumina_read_results, ignore_index = True)
    return all_illumina_results


def concat_ng50(ng50_reports: List[pathlib.Path]) -> pd.DataFrame:
    """Extract NG50 from QUAST reports for multiple samples and combine them.

    Arguments:
        ng50_reports:   all transposed single sample QUAST reports to extract NG50 value from

    Returns:
        NG50 values for all samples
    """
    quast_ng50 = []
    for quast_report in ng50_reports:
        quast_ng50_singlesample = get_quast_ng50(quast_report)
        quast_ng50.append(quast_ng50_singlesample)
    all_quast_ng50 = pd.concat(quast_ng50, ignore_index = True)
    return all_quast_ng50


# TODO: language needed!
def merge_and_make_wide(results_for_lis: List[pathlib.Path],
                        report_language: str = workflow_config["language"]) -> pd.DataFrame:
    """Merge specific analysis reports, sort them by sample number and return them in wide format.

    Arguments:
        results_for_lis:    result files of all species-/indication-specific analyses for multiple
                            samples in long format
        report_language:    the language the reports are in (valid options: en, da)

    Returns:
        All results of species-/indication-specific analyses for the samples gathered in long
        format, with columns sorted by sample number.
    """
    _ = localization_helpers.set_language(report_language)
    lis_reports = []
    for sample_report in results_for_lis:
        report_data = pd.read_csv(sample_report, sep = "\t", dtype = {"O_WGS MLST": str})
        # if we missed any typing placeholders, insert them here
        report_data["O_WGS MLST"] = report_data["O_WGS MLST"].fillna(value="-")
        lis_reports.append(report_data)

    all_reports = pd.concat(lis_reports)
    report_columns = sort_for_mads(all_reports.columns, report_language = report_language)
    all_reports = all_reports[report_columns]
    # ensure MLST line is a string (to work with placeholders)
    all_reports = all_reports.astype({"O_WGS MLST": str})
    # sort by sample number so the order matches that in the result file
    all_reports = all_reports.sort_values(by=[_("proevenr")])
    all_reports = all_reports.reset_index(drop=True)
    mads_wide = all_reports.transpose()
    return mads_wide


if __name__ == "__main__":
    arg_parser = ArgumentParser(description = "Concatenate multiple reports of the same type,"
                                              " with special handling for NG50 and coverage data"
                                              "and data for entry in LIS")
    arg_parser.add_argument("reports", help="Reports to concatenate", nargs="+")
    arg_parser.add_argument("-o", "--outfile", help="File to write summarized report to "
                                                    "(default: summarized.tsv)",
                            default="summarized.tsv")
    arg_parser.add_argument("--out_sheet", help="Excel spreadsheet to write summarized report to"
                                                "(default: none)", default = None)
    arg_parser.add_argument("--report_type", help="Type of reports to summarize "
                                                  "(options: generic, coverage, ng50, mads;"
                                                  "default: generic)",
                            choices = ["generic", "coverage", "ng50", "mads"],
                            default = "generic")
    arg_parser.add_argument("--delim",
                            help = "Delimiter used in reports (typically comma, semicolon or tab); "
                                   "default: \\t", default = "\t")
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script, "
                                   "can be overridden by commandline options)")
    args = arg_parser.parse_args()
    reports = [pathlib.Path(report_name) for report_name in args.reports]
    outfile = pathlib.Path(args.outfile)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file).resolve()
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
    # TODO: wrap this into a function as well?
    # we don't need to output the index for most of them, but we do for the MADS report
    save_with_index = False
    if args.report_type == "coverage":
        combined_report = concat_quast_coverage(reports)
    elif args.report_type == "ng50":
        combined_report = concat_ng50(reports)
    elif args.report_type == "mads":
        combined_report = merge_and_make_wide(reports, workflow_config["language"])
        save_with_index = True
    else:
        combined_report = concat_generic_reports(reports, delimiter = args.delim)

    combined_report.to_csv(path_or_buf = outfile, sep = args.delim, index = save_with_index)
    if args.out_sheet:
        out_sheet = pathlib.Path(args.out_sheet)
        combined_report.to_excel(out_sheet, index = save_with_index)
