FROM condaforge/mambaforge:latest

RUN mamba install -c defaults -c bioconda -c conda-forge -n base openpyxl "pandas<2.2.0" rsync pyyaml sqlalchemy && \
	mamba clean --all -y

WORKDIR /bactopia_datasets
COPY config/input_settings/ /bactopia_datasets/config/input_settings/

WORKDIR /pipeline_utils

COPY *.py /pipeline_utils/
COPY locales/ /pipeline_utils/locales/
COPY config/ /pipeline_utils/config/
