"""Helper functions for localization"""

__author__ = "Kat Steinke"

import gettext
import logging
import pathlib

from typing import Any, Dict, Set, Tuple

import yaml

import input_names

from input_names import BacteriaMappingNames, LISDataNames, RunsheetNames

logger = logging.getLogger(__name__)


def get_supported_languages() -> Set[str]:
    """Identify the languages currently supported by the module.

    Returns:
        The languages currently supported by the module.

    Raises:
        FileNotFoundError:  if no translation files were found
    """
    translation_dirs = (pathlib.Path(__file__).parent / "locales").glob("*/LC_MESSAGES/base.mo")
    # we're looking for the language abbreviation, two steps under the respective "base.mo" file
    supported_languages = {translation_dir.parts[-3] for translation_dir in translation_dirs}
    if not supported_languages:
        raise FileNotFoundError("No languages found in locales directory.")
    return supported_languages


def set_language(target_language: str) -> gettext.NullTranslations.gettext:
    """Set gettext translation for a language if it's part of the set of supported languages.

    Arguments:
        target_language:

    Returns:
        gettext.translation.gettext for the given language if it's supported

    Raises:
         NotImplementedError:   if the target language is not supported
    """
    supported_languages = get_supported_languages()
    target_language = target_language.lower()
    if target_language not in supported_languages:
        raise NotImplementedError(f"{target_language} is not a supported language. "
                                  f"Supported languages are {sorted(list(supported_languages))}.")
    translation = gettext.translation('base',
                                      localedir = pathlib.Path(__file__).parent / "locales",
                                      languages = [target_language])
    translation_gettext = translation.gettext
    return translation_gettext


def load_input_settings(settings_file: pathlib.Path) \
        -> Tuple[RunsheetNames, LISDataNames, BacteriaMappingNames]:
    """Load input settings from an input settings YAML file and return settings for each input type.

    Arguments:
        settings_file:   an input settings .yaml file
                         following config/input_settings/input_template.yaml

    Returns:
        RunsheetNames, LISDataNames, and BacteriaMappingNames extracted from the settings file.
    Raises:
         FileNotFoundError:  if the settings file doesn't exist
    """
    if not settings_file.exists():
        raise FileNotFoundError(f"Input config file {settings_file} does not exist.")
    with open(settings_file, "r", encoding = "utf-8") as read_settings:
        all_name_settings = yaml.safe_load(read_settings)
        sheet_names = input_names.set_up_runsheet_names(all_name_settings["metadata_sheet"])
        lis_names = input_names.set_up_lis_names(all_name_settings["lis_columns"])
        bact_names = input_names.set_up_bact_names(all_name_settings["bact_translation"])

    return sheet_names, lis_names, bact_names


def load_input_from_config(active_config: Dict[str, Any]) \
        -> Tuple[RunsheetNames, LISDataNames, BacteriaMappingNames]:
    """Load input settings from the input file given in the workflow config file.
     If the path to the input file is a relative path, it's assumed to be relative to the
      repository root.

    Arguments:
        active_config:  The config to use

    Returns:
        RunsheetNames, LISDataNames, and BacteriaMappingNames extracted from the settings file
         specified in the config.

    """
    input_settings = pathlib.Path(active_config["input_names"])
    if not input_settings.is_absolute():
        input_settings = pathlib.Path(__file__).parent / input_settings
    (sheet_names, lis_names, bact_names) = load_input_settings(input_settings)
    logger.debug(f"Input settings loaded from {input_settings}")
    return sheet_names, lis_names, bact_names
