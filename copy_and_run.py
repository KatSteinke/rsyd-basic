"""Helper script to copy data to local node, run Snakemake and copy data back. """

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import logging
import pathlib
import re
import shutil
import subprocess

from argparse import ArgumentParser
from subprocess import CalledProcessError, SubprocessError
from typing import Any, Dict


import pandas as pd
import yaml

import get_lis_report
import helpers
import input_names
import localization_helpers
import pipeline_config
import prep_samples_from_sheet
import set_log

from run_pipeline import find_illumina_path  # TODO: move to helpers?

# set logging
logger = logging.getLogger("run_snaketopia")


default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(workflow_config)


# rsync data with safeguard: up to three retries if it fails
def rsync_with_check(source: pathlib.Path, dest: pathlib.Path) -> None:
    """
    Update a destination with files from the source,
    taking only files that are missing or older in the destination;
    if rsyncing fails, retry up to three times.

    Arguments:
        source: the path to copy files from
        dest:   the path to copy files to

    Raises:
        SubprocessError: if rsyncing fails after the maximum amount of retries
    """
    tries = 1
    max_tries = 3
    rsync_return = 1
    # don't create a subdir if we're copying a dir
    if source.is_dir():
        source_path = f"{str(source)}/"
    else:
        source_path = str(source)
    rsync_command = ["rsync", "--recursive", "--update", source_path, str(dest)]
    while tries <= max_tries and rsync_return != 0:
        try:
            rsync_process = subprocess.run(rsync_command, check = True)
            rsync_return = rsync_process.returncode
        except CalledProcessError as process_err:
            tries += 1
            rsync_return = process_err.returncode

    if rsync_return != 0:
        raise SubprocessError(f"Rsyncing of {source} to {dest} failed "
                              f"with error code {rsync_return} after {max_tries} tries")

    logger.info(f"Successfully rsynced {source} to {dest} (tries: {tries})")


def set_up_inputs(sequencing_dir: pathlib.Path, source_outdir: pathlib.Path, runsheet: pathlib.Path,
                  dest_dir: pathlib.Path, active_config: Dict[str, Any] = workflow_config,
                  runsheet_names: input_names.RunsheetNames = sheet_names,
                  bacteria_names: input_names.BacteriaMappingNames = bact_names,
                  lis_data_names: input_names.LISDataNames = lis_names) -> None:
    """
    Copy inputs from different sources to a single directory in which Snakemake will run.

    Arguments:
        sequencing_dir: the directory containing reads (base dir or directory with fastq files)
        source_outdir:  the desired final output directory
        runsheet:       path to the Excel Illumina or Nanopore runsheet
        dest_dir:       the base directory to which content should be copied (local on the node)
        active_config:  the pipeline's configuration
        runsheet_names: the column and sheet names in use
        bacteria_names: Column names in the code -> species name mapping
        lis_data_names: Column names in LIS data

    """
    # find dir with reads to start with
    # TODO: we can replace this and just read the file of filenames since we already have this
    if active_config["sequencing_mode"] == "illumina":
        sheet_data = pd.read_excel(runsheet, sheet_name = runsheet_names.sample_info_sheet)
        read_dir = find_illumina_path(sequencing_dir, sheet_data,
                                      sequencer = active_config["platform"],
                                      runsheet_names = runsheet_names)
        helpers.check_for_samples(read_dir, sheet_data, runsheet_names = runsheet_names)
        read_outdir = dest_dir / "reads"
    elif active_config["sequencing_mode"] == "nanopore":
        sheet_data = pd.read_excel(runsheet, sheet_name = f"{runsheet_names.runsheet_base}"
                                                          "_Nanopore",
                                   usecols = "A:C", skiprows = 3,
                                   dtype = {runsheet_names.nanopore_sheet_sample_number: str,
                                            runsheet_names.barcode: str})
        read_dir = helpers.get_fastq_pass_parent(sequencing_dir, sheet_data
                                              , runsheet_names = runsheet_names) / "fastq_pass"
        # we need the structure with fastq_pass but don't want to have to upload everything
        read_outdir = dest_dir / "reads" / "rawdata" / "subdir" / "fastq_pass"
        # we do need the report
        nanopore_reports = list(read_dir.parent.glob("report*.json"))
        if not nanopore_reports:
            raise FileNotFoundError("No nanopore report found")
        if len(nanopore_reports) > 1:
            raise ValueError("Too many nanopore reports found")
        nanopore_report = nanopore_reports[0]  # type: pathlib.Path
    else:
        raise ValueError(f"Sequencing mode {active_config['sequencing_mode']} is not supported.")

    # initialize blank LIS report data
    lis_data = None
    dir_mapping = {"reads": {"in": read_dir,
                             "out": read_outdir},
                   "outdir": {"in": source_outdir,
                              "out": dest_dir / "current_outdir"}, # TODO: are there any assumptions on parent dirnames this breaks?
                   "runsheet": {"in": runsheet,
                                "out": dest_dir / "current_runsheet.xlsx"}}
    if active_config["sequencing_mode"] == "nanopore":
        dir_mapping["nanopore_report"] = {"in": nanopore_report,
                                          "out": read_outdir.parent / nanopore_report.name}
    if active_config["lab_info_system"]["use_lis_features"]:
        dir_mapping["lis_report"] = {"in": pathlib.Path(active_config["lab_info_system"][
                                                            "lis_report"]),
                                     "out": dest_dir / "madsdata" / "current_mads.txt"}
        dir_mapping["bacteria_codes"] = {"in": pathlib.Path(active_config["lab_info_system"][
                                                                "bacteria_codes"]),
                                         "out": dest_dir / "madsdata" / "bacteria_codes.csv"}

        lis_data = get_lis_report.get_report_with_isolate_number(pathlib.Path(active_config[
                                                                           "lab_info_system"][
                                                                           "lis_report"]),
                                                                 mapping = active_config[
                                                              "sample_number_settings"][
                                                              "number_to_letter"],
                                                                 translate_from = active_config[
                                                              "sample_number_settings"][
                                                              "sample_numbers_out"],
                                                                 translate_to = active_config[
                                                              "sample_number_settings"][
                                                              "sample_numbers_in"],
                                                                 from_format = re.compile(active_config[
                                                              "sample_number_settings"][
                                                              "format_in_lis"]),
                                                                 to_format = re.compile(active_config[
                                                              "sample_number_settings"][
                                                              "format_in_sheet"]),
                                                                 lis_data_names = lis_data_names
                                                                 )
    # record what we copy where to
    copied_inputs = []
    for input_to_copy, paths in dir_mapping.items():
        if not paths["out"].parent.exists():
            paths["out"].parent.mkdir(parents=True, exist_ok = True)
        rsync_with_check(paths["in"], paths["out"])
        copied_inputs.append(f"- {input_to_copy}: {paths['in']}")

    copied_inputs_prettyprint = "\n".join(sorted(copied_inputs))
    logger.info(f"Successfully copied all inputs to {str(dest_dir)}.\n"
                f"Inputs:\n"
                f"{copied_inputs_prettyprint}")

    # generate new FOFN - we don't need a heads-up for missing samples here anymore
    if active_config["sequencing_mode"] == "illumina":
        moved_read_overview = prep_samples_from_sheet.get_samples_from_illumina_sheet(
            runsheet, dest_dir / "reads", lis_data, active_config = active_config,
        runsheet_names = runsheet_names, lis_data_names = lis_data_names,
            bacteria_names = bacteria_names)
        # fill the missing columns
        moved_read_overview["extra"] = ""
    else:  # if it's not Nanopore we failed already
        moved_read_overview = prep_samples_from_sheet.get_samples_from_nanopore_sheet(runsheet,
                                                                                      dest_dir
                                                                                      / "reads"
                                                                                      / "rawdata"
                                                                                      / "subdir"
                                                                                      / "fastq_pass",
                                                                                      lis_data,
                                                                                      active_config = active_config,
                                                                                      runsheet_names = runsheet_names,
                                                                                      lis_data_names = lis_data_names,
                                                                                      bacteria_names = bacteria_names)
        moved_read_overview["r1"] = ""
        moved_read_overview["r2"] = ""

    moved_read_overview[["sample",
                         "runtype",
                         "r1",
                         "r2",
                         "extra"]].to_csv((dest_dir / "current_outdir" / "reads_moved.tsv"),
                                          sep = "\t",
                                          index = False)


if __name__ == "__main__":
    logger = set_log.get_stream_log()
    arg_parser = ArgumentParser(description = "Move input files to a temporary directory and adapt"
                                              " input file of filenames")
    arg_parser.add_argument("read_overview",
                            help="Path to file of filenames for Bactopia/Snaketopia")
    arg_parser.add_argument("sequence_dir", help="Path to directory containing Illumina sequences")
    arg_parser.add_argument("outdir", help="Path to output directory")
    arg_parser.add_argument("runsheet", help="Path to Illumina sample sheet")
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script, "
                                   "can be overridden by commandline options)")
    args = arg_parser.parse_args()
    read_overview = pathlib.Path(args.read_overview)
    sequence_dir = pathlib.Path(args.sequence_dir)
    out_dir = pathlib.Path(args.outdir)
    run_sheet = pathlib.Path(args.runsheet)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file).resolve()
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        (sheet_names,
         lis_names,
         bact_names) = localization_helpers.load_input_from_config(workflow_config)

    # output dir may be missing if we're starting this through a test job and not run_pipeline
    # create this if missing (and copy the input file to there?)
    if workflow_config["debug"]:
        out_dir.mkdir(parents=True, exist_ok = True)
        if not (out_dir/read_overview.name).exists():
            shutil.copyfile(read_overview, (out_dir/read_overview.name))

    # handle potential missing setting for node data dir
    if "temp_data_dir" not in workflow_config:
        logger.warning("No specification for temporary data directory given."
                       " Defaulting to working with inputs in place.")
        temp_data_dir = None
    else:
        temp_data_dir = pathlib.Path(workflow_config["temp_data_dir"])

    # copy to node dir
    if temp_data_dir:
        set_up_inputs(sequence_dir, out_dir, run_sheet, temp_data_dir,
                      active_config = workflow_config, runsheet_names = sheet_names,
                      lis_data_names = lis_names, bacteria_names = bact_names)
    else:
        logger.info("No temp dir specified. Exiting.")
