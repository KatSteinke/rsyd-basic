# Genome size database

The genome size database was originally taken from the NCBI's RefSeq list using the parameters
used by John Christian Gaby for[ calculating procaryotic genome size statistics](https://chrisgaby.github.io/post/prokaryotic-genome-size/),
accounting for the changes to NCBI's download process (using the [new portal](https://www.ncbi.nlm.nih.gov/data-hub/genome/?taxon=2&reference_only=true)).

Species names were then extracted using the `get_names_in_db.py` helper script. 

The current version of the database has been downloaded on 2022-11-08.
