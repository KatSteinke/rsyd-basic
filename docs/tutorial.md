# Tutorial

## Getting the example data
Download the example data from [Zenodo](https://zenodo.org/records/13881353) and unpack
`2024-10-02-rsyd-basic-inputdata-en.zip`.

## Setting the config file
Before the pipeline can be started, the default config file needs to be adapted.
Open `pipeline_routine.yml` and set the following values:
* `language`: set to "en"
* `input_names`: set to "config/input_settings/input_en.yaml"
* `paths`:
  * `output_base_path`: set your desired base path for outputs - 
  if you want results to be saved under `result_data/experiment01`, `result_data/experiment02`,...
    (relative to the directory you are running the pipeline in), for example,
  set this value to `result_data`
  * `kraken_db`: set to the path you have saved the gz compressed Kraken database to
  * `kraken_outdir`: set to the path you want the pipeline to extract the Kraken database to
  * `gtdb_data`: set to the path you have saved the gz compressed GTDB database to
  * `gtdb_outdir`: set to the path you want the pipeline to extract the GTDB database to.
  **NOTE**: make sure you have at least 200 GB of space in this location 
  * `mash_data`: set to the path you have saved the xz compressed Mash database to
  * `mash_outdir`: set to the path you want the pipeline to extract the Mash database to
* `lab_info_system`:
  * `lis_report`: set to `madsdata/madsdata_en.txt` in the unpacked example data. 
  For example, if the path to the unpacked example data is 
  `/data/test_data/2024-10-02-rsyd-basic-inputdata-en/`, set this to
  `/data/test_data/2024-10-02-rsyd-basic-inputdata-en/madsdata/madsdata_en.txt`
  * `bacteria_codes`: set to `madsdata/bakteria_categories_en.csv` in the unpacked example data 
  (see above) 

## Running the pipeline in classic mode
Activate the environment and run the script (in the directory where you have cloned the repository):
```shell
conda activate rsyd_basic
python3 run_pipeline.py
```
The pipeline will prompt you to enter the path to the Illumina sequencing directory. 

Type the path to the unpacked example data (for the example above, this would be 
`/data/test_data/2024-10-02-rsyd-basic-inputdata-en/`) and confirm with <kbd>Enter</kbd>.

The pipeline then prompts for the runsheet. Type the path to the runsheet (`ILM_Run0000_Y20240911_kts_en.xlsm`) in the unpacked example data -
for the example above, this would be 
`/data/test_data/2024-10-02-rsyd-basic-inputdata-en/ILM_Run0000_Y20240911_kts_en.xlsm` - and confirm
with <kbd>Enter</kbd>.

The pipeline now asks if it should save the results in a directory named `ILM_Run0001_Y20230731_kts`
in the directory you have specified as the result base directory. \
If you want this, confirm with <kbd>y</kbd> followed by <kbd>Enter</kbd>. \
If you want to use a different path, reject the suggestion by typing <kbd>n</kbd>
followed by <kbd>Enter</kbd>, then type the new path and confirm with <kbd>Enter</kbd>. 
Note that for compatibility with Windows filesystems, only characters that are allowed in Windows
paths are allowed. Paths cannot contain spaces (because the maintainer is an old person yelling 
at clouds about them.)

The pipeline will now run.

## Running the pipeline in commandline mode
Assuming the same paths as above, the pipeline can be run in commandline mode as follows:
```shell
conda activate rsyd_basic
python3 run_pipeline.py --illumina_sheet /data/test_data/2024-10-02-rsyd-basic-inputdata-en/ILM_Run0000_Y20240911_kts_en.xlsm \
--illumina_dir /data/test_data/2024-10-02-rsyd-basic-inputdata-en
```
To use a custom output directory, add `--outdir` and specify the desired output directory.

## Expected results
Expected results are included in the Zenodo dataset as `20241001_demo_illumina_en.zip`.  