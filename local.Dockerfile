FROM condaforge/mambaforge:22.9.0-1 AS fetch_dbs

RUN mamba install -c bioconda -c conda-forge -n base git kma wget && \
    mamba clean --all -y && \
	mkdir -p /bactopia_datasets/minmer && \
	git clone https://bitbucket.org/genomicepidemiology/serotypefinder_db.git /bactopia_datasets/serotypefinder && \
    git clone https://bitbucket.org/genomicepidemiology/plasmidfinder_db.git /bactopia_datasets/plasmidfinder


WORKDIR /bactopia_datasets/serotypefinder

RUN python3 INSTALL.py

WORKDIR /bactopia_datasets/plasmidfinder

RUN python3 INSTALL.py

FROM condaforge/mambaforge:latest

RUN mkdir "/kraken_db"
RUN mkdir "/gtdbtk_data/"
COPY --from=fetch_dbs /bactopia_datasets /bactopia_datasets

RUN mkdir /conda-envs
COPY envs/ /conda-envs

# NOTE: assembly env only contains Illumina assemblers for now
WORKDIR /bactopia_datasets

RUN mamba env update -n base --file /conda-envs/base_env.yml && \
    mamba env create -f /conda-envs/abritamr_env.yml && \
    mamba env create -f /conda-envs/annotate_env.yml && \
    mamba env create -f /conda-envs/assembly_env.yml && \
    mamba env create -f /conda-envs/gtdb_env.yml && \
    mamba env create -f /conda-envs/hybrid_env.yml && \
    mamba env create -f /conda-envs/kraken_env.yml && \
    mamba env create -f /conda-envs/minmers_env.yml && \
    mamba env create -f /conda-envs/mlst_env.yml && \
    mamba env create -f /conda-envs/nanopore_only.yml && \
    mamba env create -f /conda-envs/nanopore_qc_env.yml && \
	mamba env create -f /conda-envs/plasmids_env.yml && \
    mamba env create -f /conda-envs/qc_env.yml && \
    mamba env create -f /conda-envs/ilm_polishing_env.yml && \
    mamba env create -f /conda-envs/ilm_read_qc.yml && \
    mamba env create -f /conda-envs/typing_env.yml && \
    mamba clean --all -y


ARG MAMBA_DOCKERFILE_ACTIVATE=1

WORKDIR /snake_data

COPY . /snake_data


ENTRYPOINT ["snakemake", "-s", "/snake_data/Snakefile", "--keep-going", "--use-conda", "--conda-frontend", "mamba", "--conda-prefix", "/conda-envs", "--rerun-triggers", "mtime"]