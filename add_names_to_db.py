"""Helper script to add species names to a newly downloaded version of the NCBI's list of RefSeq
prokaryotic genomes.
"""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib
import re

from argparse import ArgumentParser

import pandas as pd

import set_log
import version

__version__ = version.__version__

logger = logging.getLogger(__name__)

# TODO: this can probably be prettier using some accession to taxid conversion + keeping samples'
# Kraken taxid... but that may take longer

def extract_species_name(ncbi_species: str) -> str:
    """Extract species name from NCBI's organism name: remove strain name,
    strip out brackets if needed.

    Arguments:
        ncbi_species:   The organism name as listed in NCBI's list of prokaryotic genome sequences.

    Returns:
        The species name of the relevant organism only.
    """
    species_name = r"^'?(?P<species_name>\[?[A-Za-z]+\]? [a-z-]+)'?( |$)"
    species_match = re.match(species_name, ncbi_species)
    # if we can't extract the name, we may still get *something* out of the organism name
    if not species_match:
        logger.warning(f"No species name found in {ncbi_species}. "
                       f"Using NCBI organism name instead.")
        return ncbi_species
    extracted_name = species_match.group("species_name")
    # there may be brackets around the genus name in cases such as "Clostridium"
    extracted_name = extracted_name.replace("[", "").replace("]", "")
    return extracted_name


def add_species_names(ncbi_db: pd.DataFrame) -> pd.DataFrame:
    """Infer species names from NCBI organism names and add them to the overview of genome
    sequences.

    Arguments:
        ncbi_db:    the database to which names should be added. Must contain "#Organism name"
                    column.

    Returns:
        The database, with inferred organism names added.
    """
    db_with_names = ncbi_db.copy()
    db_with_names["species_name"] = db_with_names["Organism Name"].apply(extract_species_name)
    return db_with_names


if __name__ == "__main__":
    logger = set_log.get_stream_log(level="WARNING")
    logger.setLevel(logging.INFO)
    arg_parser = ArgumentParser(description = "Add species names to a list of prokaryotic "
                                              "sequences with organism names and sizes from NCBI.")
    arg_parser.add_argument("ncbi_db", help = 'the database to which names should be added. '
                                              'Must contain "Organism Name" column.')
    arg_parser.add_argument("--outfile",
                            help = f'File to save updated database to. '
                                   f'Default: '
                                   f'{(pathlib.Path(__file__).parent / "data" / "prokaryote_genome_sizes.tsv")}',
                            default = (pathlib.Path(__file__).parent / "data"
                                       / "prokaryote_genome_sizes.tsv"))
    args = arg_parser.parse_args()
    ncbi_database = pathlib.Path(args.ncbi_db)
    outfile = pathlib.Path(args.outfile)

    ncbi_data = pd.read_csv(ncbi_database, sep="\t")
    ncbi_data = add_species_names(ncbi_data)

    ncbi_data.to_csv(outfile, sep="\t", index=False)

