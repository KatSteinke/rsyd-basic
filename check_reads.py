"""Check read files for corruption."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import pathlib

from argparse import ArgumentParser

import magic

from Bio import SeqIO

import set_log

# TODO: is this overkill?
logger = logging.getLogger("check_reads")


class NoReadsError(Exception):
    """Exception raised when a read file is empty."""


class BrokenReadFileError(Exception):
    """Exception raised when a read file is malformed (wrong file format, wrong line count.)"""


class ReadMismatchError(Exception):
    """Exception raised when amount of forward and reverse reads in two paired read files don't
     match."""


def check_read_file_type(read_file: pathlib.Path) -> None:
    """Check whether a read file's format is corrupted. Files should read as ASCII text; anything
    else is an issue.

    Arguments:
        read_file:  Path to the read file to check

    Raises:
        BrokenReadFileError:    if the file is not an ASCII text file
        NoReadsError:           if the file is empty
    """
    expected_type = "text/plain; charset=us-ascii"
    magic_with_encoding = magic.Magic(mime=True, mime_encoding=True)
    type_and_encoding = magic_with_encoding.from_file(read_file)
    # fail early if the file is empty
    if type_and_encoding == "inode/x-empty; charset=binary":
        raise NoReadsError(f"No reads found in {read_file}.")
    if type_and_encoding != expected_type:
        raise BrokenReadFileError(f"Read file {read_file} should be ASCII plain text,"
                                  f" is {type_and_encoding}.")
    logger.info(f"File format for read file {read_file} is correct.")


def count_reads(file_to_count: pathlib.Path) -> int:
    """Count the amount of reads in the given file.
    Line count adapted from https://stackoverflow.com/q/845058/15704972.

    Arguments:
        file_to_count:  Path to the file in which reads should be counted.

    Returns:
        The amount of reads in the file.

    Raises:
        BrokenReadFileError:    if the file doesn't contain four lines per read
        NoReadsError:           if the file is empty
    """
    line_count = 0
    with open(file_to_count, "r", encoding="utf-8") as count_file:
        for line_count, _ in enumerate(count_file, start = 1):
            pass
    # if a file is blank we have a problem
    if not line_count:
        raise NoReadsError(f"No reads found in {file_to_count}.")
    # in a fastq file each read has four lines - if this doesn't apply we have a problem
    if line_count % 4:
        raise BrokenReadFileError(f"Wrong line count in {file_to_count}"
                                  f" - FASTQ files should have four lines per read.")
    read_count = int(line_count / 4)
    logger.info(f"Line count for read file {file_to_count} is correct.")
    return read_count


def compare_read_counts(forward_read: pathlib.Path, reverse_read: pathlib.Path) -> None:
    """Check whether forward and reverse read files contain the same amount of reads.

    Arguments:
        forward_read:   Path to the file containing forward reads
        reverse_read:   Path to the file containing reverse reads

    Raises:
        BrokenReadFileError:    if a file doesn't contain four lines per read
        NoReadsError:           if a file is empty
        ReadMismatchError:      if amount of forward and reverse reads doesn't match
    """
    forward_reads = count_reads(forward_read)
    reverse_reads = count_reads(reverse_read)

    if forward_reads != reverse_reads:
        raise ReadMismatchError(f"Amount of forward and reverse reads don't match. "
                                f"There are {forward_reads} in {forward_read} (forward) but "
                                f"{reverse_reads} in {reverse_read} (reverse).")
    logger.info(f"Read counts for {forward_read} and {reverse_read} match "
                f"({forward_reads} and {reverse_reads}).")


# TODO: just borrow their checks entirely?
def check_if_parseable(read_file: pathlib.Path) -> None:
    """Check if the read file can be parsed in Biopython (borrow their checks if the rest looks
     good)

     Arguments:
         read_file: the read file to check for Biopython parsing errors

     Raises:
         BrokenReadFileError:   if the file could not be parsed with Biopython
     """
    with open(read_file, "r") as reads:
        fastqs = SeqIO.parse(reads, "fastq")
        try:
            any(fastqs)
        # if it won't parse, wrap it in our catchall broken read class and reraise
        except ValueError as err:
            error_msg = f"Biopython error parsing {read_file}:\n" \
                        f"{err}"
            raise BrokenReadFileError(error_msg)
    logger.info(f"Successfully parsed {read_file} in Biopython.")


def validate_paired_reads(forward_read: pathlib.Path, reverse_read: pathlib.Path) -> None:
    """Validate paired-end read files' format and read count.

    Arguments:
        forward_read:   Path to the file containing forward reads
        reverse_read:   Path to the file containing reverse reads

    Raises:
        BrokenReadFileError:    if a file is malformed (wrong format or amount of lines per read)
        NoReadsError:           if a file is empty
        ReadMismatchError:      if amount of forward and reverse reads doesn't match
    """
    # TODO: log and reraise? Or is it enough to see that they work?
    # if there is an individual issue with the reads, fail before we get all the way to counting lines
    for read_file in [forward_read, reverse_read]:
        check_read_file_type(read_file)
        check_if_parseable(read_file)
    compare_read_counts(forward_read, reverse_read)
    # if we haven't failed here, the files should be good
    logger.info(f"No issues found for read files {forward_read} and {reverse_read}.")


if __name__ == "__main__":
    logger = set_log.get_stream_log()
    arg_parser = ArgumentParser(description = "Check whether two paired-end read files contain "
                                              "matching reads and are properly formatted.")
    arg_parser.add_argument("forward", help="Path to forward read file")
    arg_parser.add_argument("reverse", help="Path to reverse read file")
    # TODO: add logging to file?
    args = arg_parser.parse_args()
    forward = pathlib.Path(args.forward)
    reverse = pathlib.Path(args.reverse)
    validate_paired_reads(forward, reverse)
