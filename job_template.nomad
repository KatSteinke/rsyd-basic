job "${NOMAD_ENV}-snaketopia" {
  namespace   = "ouh-kma"
  datacenters = ["DCK-A", "DCK-B"]
  type        = "batch"

  parameterized {
    meta_required = ["bactopia_names", "outdir", "outdir_name", "runsheet", "sequencing_dir", "node_dir", "use_lis"]
    payload       = "required"
  }

  group "compute" {
    # where can this job be deployed
    constraint {
      attribute = "${node.class}"
      value     = "compute"
    }
  # add more as needed

    /* set up volumes here
    volume "VOLUMENAME" {
      type            = "csi"
      source          = "VOLUMENAME"
      access_mode     = "multi-node-multi-writer"
      attachment_mode = "file-system"
    }
*/


    task "copy_files" {
      driver = "docker"
       lifecycle {
          hook = "prestart"
        }
      # populate env variables
      env {
        TZ     = "Europe/Copenhagen"
	COMMIT = "${CI_COMMIT_SHORT_SHA}"
      }

      /* add volume to container
      volume_mount {
        volume      = "VOLUMENAME"
        destination = "/dest_path"
      } */
      dispatch_payload {
        file = "config.yaml"
      }


      config {
        image   = "${CI_REGISTRY_IMAGE}:copy_base"
        image_pull_timeout = "10m"
        force_pull = true
        command = "python3"
        args = [
          "/pipeline_utils/copy_and_run.py",
          "${NOMAD_META_bactopia_names}",
          "${NOMAD_META_sequencing_dir}",
          "${NOMAD_META_outdir}",
          "${NOMAD_META_runsheet}",
          "--workflow_config_file", "/local/config.yaml"
        ]

        auth {
          username = "${DEPLOY_USER}"
          password = "${DEPLOY_PASS}"
        }
      }

      resources {
        cpu    = 1500
        memory = 12800
      }

    } # prestart ends here
    task "set_rsync_pass" {
      driver = "docker"
      lifecycle {
          hook = "prestart"
        }
      # store rsync password
      template {
        destination = "${NOMAD_SECRETS_DIR}/rsync_pw.env"
        change_mode = "restart"
        data        = <<EOH
"{{ with nomadVar "nomad/jobs/${NOMAD_ENV}-snaketopia" }}{{ .rsync_pass }}{{ end }}"
EOH
      }
      config {
        image   = "${CI_REGISTRY_IMAGE}:copy_base"
        image_pull_timeout = "10m"
        command = "/bin/bash"
        args = ["-c", "cat ${NOMAD_SECRETS_DIR}/rsync_pw.env | xargs > ${NOMAD_SECRETS_DIR}/rsync_pw_clean.env && chmod 600 ${NOMAD_SECRETS_DIR}/rsync_pw_clean.env"]
      }
    }

    task "compute" {
      driver = "docker"

      # populate env variables
      env {
        TZ     = "Europe/Copenhagen"
	COMMIT = "${CI_COMMIT_SHORT_SHA}"
      }

       /* add volume to container
      volume_mount {
        volume      = "VOLUMENAME"
        destination = "/dest_path"
      } */


      dispatch_payload {
        file = "config.yaml"
      }


      config {
        image   = "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
        image_pull_timeout = "10m"
        command = "snakemake"
        # TODO: don't hardcode envs
        args = [
          "-s", "/snake_data/Snakefile",
          "--cores", "${attr.cpu.numcores}",
          "--conda-prefix", "/conda-envs",
          "--keep-going",
          "--configfile", "/local/config.yaml",
          "--config",
          "input_samples=/alloc/data/current_outdir/reads_moved.tsv",
          "outdir=/alloc/data/current_outdir",
          "config_path=/local/config.yaml",
          "runsheet=/alloc/data/current_runsheet.xlsx",
          "lab_info_system={'use_lis_features': ${NOMAD_META_use_lis},'lis_report':/alloc/data/madsdata/current_mads.txt,'bacteria_codes':/alloc/data/madsdata/bacteria_codes.csv}", # TODO: don't hardcode!
          "--use-conda",
          "--conda-frontend", "mamba",
          "-r",
          "--rerun-incomplete",
          "--rerun-triggers", "input",
          "--shadow-prefix", "/bactopia_datasets",
          "--resources",
          "mem_mib=160000"
        ]

        auth {
          username = "${DEPLOY_USER}"
          password = "${DEPLOY_PASS}"
        }
      }

      resources {
        cpu    = 15000
        memory = 160000
        memory_max = 180000
      }

    } // task ends here
    task "copy_back" {
      driver = "docker"
       lifecycle {
          hook = "poststop"
        }
      # populate env variables
      env {
        TZ     = "Europe/Copenhagen"
	COMMIT = "${CI_COMMIT_SHORT_SHA}"
      }




      dispatch_payload {
        file = "config.yaml"
      }
      # where this should rsync to depends on whether this is routine or test
      meta {
        copy_target = "${NOMAD_ENV}" == "staging" ? "RSYNC_SERVER::test-results" : "RSYNC_SERVER::routine-results"
      }

      config {
        image   = "${CI_REGISTRY_IMAGE}:copy_base"
        image_pull_timeout = "10m"
        volumes = [
          "../set_rsync_pass/secrets/rsync_pw_clean.env:/secrets/rsync_pw.env:ro"
        ]
        command = "rsync"
        args = [
         "--recursive", "--update", "/alloc/data/current_outdir/",
          "${NOMAD_META_copy_target}/${NOMAD_META_outdir_name}",
          "--password-file=/secrets/rsync_pw.env"
        ]

        auth {
          username = "${DEPLOY_USER}"
          password = "${DEPLOY_PASS}"
        }
      }

      resources {
        cpu    = 150
        memory = 12800
      }

    } # prestart ends here

    dynamic "task" {
      for_each = "${NOMAD_ENV}" == "staging" ? [1] : []
      labels   = ["poststop_test"]
      content {
        lifecycle {
          hook = "poststop"
        }
        driver = "docker"

        # populate env variables
        env {
          TZ        = "Europe/Copenhagen"
          COMMIT_ID = "${CI_COMMIT_SHORT_SHA}"
        }
        # don't restart if failed
        restart {
          attempts = 0
        }
         /* add volume to container
      volume_mount {
        volume      = "VOLUMENAME"
        destination = "/dest_path"
      } */

        dispatch_payload {
        file = "config.yaml"
      }

        config {
          image              = "${CI_REGISTRY_IMAGE}:qa_base"
          image_pull_timeout = "10m"
          command            = "python3"
          force_pull = true
          args               = [
            "/snake_qc/check_expected_results.py", "/alloc/data/current_outdir",
            "/path/to/testset",
            "--workflow_config_file", "/local/config.yaml"
          ]


          auth {
            username = "${DEPLOY_USER}"
            password = "${DEPLOY_PASS}"
          }
        }
        resources {
          cpu    = 150
          memory = 12800
        }
      }

    }
    // test task ends here
  }   // group ends here
}     // job ends here
