"""Look up the genome size of a known organism in an overview table."""

__author__ = "Kat Steinke"

import pathlib
import statistics

from argparse import ArgumentParser

import pandas as pd


# read in NCBI organism database - we'll need this a lot
NCBI_DATA = pd.read_csv(pathlib.Path(__file__).parent / "data" / "prokaryote_genome_sizes.tsv",
                        sep="\t")


# TODO: what do we do with NCBI data? Easier to test if we can feed it a fake thing... just
# take the dataframe instead?
def get_expected_size_by_name(organism_name: str) -> float:
    """Extract the expected size of an organism by species name from the NCBI's list of prokaryotic
    genome sequences.

    Arguments:
        organism_name:  the name of the organism whose expected genome size should be extracted.

    Returns:
        The average genome size for all organisms of the relevant species in basepairs.

    Raises:
        KeyError:   if the provided organism was not found in the database
        ValueError: if no species was given
    """
    # if we don't get a name, we can't get a size
    if pd.isna(organism_name):
        raise ValueError("No species given.")
    hits_for_species = NCBI_DATA.query("species_name == @organism_name")
    # if we don't get any matches, we can't use them
    if hits_for_species.empty:
        raise KeyError(f"No expected size found for organism {organism_name}.")
    sizes_for_species = hits_for_species["Assembly Stats Total Sequence Length"].tolist()
    mean_size = statistics.fmean(sizes_for_species)
    return mean_size


if __name__ == "__main__":
    parser = ArgumentParser("Look up the genome size of a known organism")
    parser.add_argument("organism_name",
                        help="The name of the organism whose genome size should be identified")
    args = parser.parse_args()
    print(get_expected_size_by_name(args.organism_name))
