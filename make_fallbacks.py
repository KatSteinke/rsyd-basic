"""Generate blank/NA fallback files for failed Quast runs."""

__author__ = "Kat Steinke"

#  Copyright (c) 2023-2024 Kat Steinke
#     This program is distributed under version 3 of the GNU General Public License.
#      You should have received a copy of the GNU General Public License
#        along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import pathlib

from argparse import ArgumentParser

import pandas as pd


def make_blank_checkm(sample_number: str) -> pd.DataFrame:
    """Generate a blank CheckM read report with 0% completeness for a failed sample.

    Arguments:
        sample_number:  the sample number of the failed sample.

    Returns:
        A blank CheckM report with 0% completeness.
    """
    blank_checkm = pd.DataFrame(data={"Bin Id": [sample_number],
                                      "Marker lineage": [pd.NA],
                                      "# genomes": [pd.NA],
                                      "# markers": [pd.NA],
                                      "# marker sets": [pd.NA],
                                      "0": [pd.NA],
                                      "1": [pd.NA],
                                      "2": [pd.NA],
                                      "3": [pd.NA],
                                      "4": [pd.NA],
                                      "5+": [pd.NA],
                                      "Completeness": [0.00],
                                      "Contamination": [pd.NA],
                                      "Strain heterogeneity": [pd.NA]})
    return blank_checkm


def make_blank_quast_reads(sample_number: str) -> pd.DataFrame:
    """Generate a blank QUAST read report for a failed sample.

    Arguments:
        sample_number:  the sample number of the failed sample

    Returns:
        A blank QUAST read report for the sample.
    """
    # for now fill everything except the name with NA
    blank_reads = pd.DataFrame(data={"Assembly": [sample_number],
                                     "# total reads": [pd.NA],
                                     "# left": [pd.NA],
                                     "# right": [pd.NA],
                                     "# mapped": [pd.NA],
                                     "Mapped (%)": [pd.NA],
                                     "# properly paired": [pd.NA],
                                     "Properly paired (%)": [pd.NA],
                                     "# singletons": [pd.NA],
                                     "# misjoint mates": [pd.NA],
                                     "Misjoint mates (%)": [pd.NA],
                                     "Avg. coverage depth": [pd.NA],
                                     "Coverage >= 1x (%)": [pd.NA],
                                     "Coverage >= 5x (%)": [pd.NA],
                                     "Coverage >= 10x (%)": [pd.NA]})
    # the default read output is transposed
    blank_reads = blank_reads.set_index("Assembly").transpose().reset_index()
    blank_reads = blank_reads.rename(columns = {"index": "Assembly"})
    blank_reads.columns.names = [None]
    return blank_reads


# create blank general output
def make_blank_quast_report(sample_number: str) -> pd.DataFrame:
    """Generate a blank QUAST report for a failed sample.

    Arguments:
        sample_number:  the sample number of the failed sample

    Returns:
        A blank QUAST report for the sample.
    """
    # for now fill everything except the name with NA
    blank_report = pd.DataFrame(data= {"Assembly": [sample_number], "# contigs (>= 0 bp)": [pd.NA],
                                       "# contigs (>= 1000 bp)": [pd.NA],
                                       "# contigs (>= 5000 bp)": [pd.NA],
                                       "# contigs (>= 10000 bp)": [pd.NA],
                                       "# contigs (>= 25000 bp)": [pd.NA],
                                       "# contigs (>= 50000 bp)": [pd.NA],
                                       "Total length (>= 0 bp)": [pd.NA],
                                       "Total length (>= 1000 bp)": [pd.NA],
                                       "Total length (>= 5000 bp)": [pd.NA],
                                       "Total length (>= 10000 bp)": [pd.NA],
                                       "Total length (>= 25000 bp)": [pd.NA],
                                       "Total length (>= 50000 bp)": [pd.NA],
                                       "# contigs": [pd.NA],
                                       "Largest contig": [pd.NA],
                                       "Total length": [pd.NA],
                                       "Estimated reference length": [pd.NA],
                                       "GC (%)": [pd.NA],
                                       "N50": [pd.NA],
                                       "NG50": [pd.NA],
                                       "N90": [pd.NA],
                                       "auN": [pd.NA],
                                       "auNG": [pd.NA],
                                       "L50": [pd.NA],
                                       "LG50": [pd.NA],
                                       "L90": [pd.NA],
                                       "# total reads": [pd.NA],
                                       "# left": [pd.NA],
                                       "# right": [pd.NA],
                                       "Mapped (%)": [pd.NA],
                                       "Properly paired (%)": [pd.NA],
                                       "Avg. coverage depth": [pd.NA],
                                       "Coverage >= 1x (%)": [pd.NA],
                                       "# N's per 100 kbp": [pd.NA],
                                       "# predicted genes (unique)": [pd.NA],
                                       "# predicted genes (>= 0 bp)": [pd.NA],
                                       "# predicted genes (>= 300 bp)": [pd.NA],
                                       "# predicted genes (>= 1500 bp)": [pd.NA],
                                       "# predicted genes (>= 3000 bp)": [pd.NA]})
    return blank_report


if __name__ == "__main__":
    arg_parser = ArgumentParser(description = "Create blank reports for failed samples")
    arg_parser.add_argument("--sample", help="Sample number of failed sample", required = True)
    arg_parser.add_argument("--outdir",
                            help="Directory to save blank reports to",
                            required = True)
    arg_parser.add_argument("--mode", help="Tool for which blank output should be provided",
                            choices = {"checkm", "quast"})
    args = arg_parser.parse_args()
    sample = args.sample
    tool_to_mockup = args.mode
    output_basedir = pathlib.Path(args.outdir)
    if not output_basedir.exists():
        raise FileNotFoundError(f"Output directory for sample {sample} does not exist")
    if tool_to_mockup == "quast":
        blank_quast_report = make_blank_quast_report(sample)
        blank_quast_report.to_csv(path_or_buf = (output_basedir / "transposed_report.tsv"),
                                  index = False, sep="\t")
        reads_dir = output_basedir / "reads_stats"
        reads_dir.mkdir(exist_ok = True)
        blank_quast_reads = make_blank_quast_reads(sample)
        blank_quast_reads.to_csv(path_or_buf = (reads_dir / "reads_report.tsv"),
                                 index = False, sep="\t")
    elif tool_to_mockup == "checkm":
        blank_checkm_report = make_blank_checkm(sample)
        blank_checkm_report.to_csv(path_or_buf = (output_basedir / "checkm-results.txt"),
                                   index=False, sep="\t")
    else:
        raise ValueError(f"Unknown tool {tool_to_mockup}, cannot generate blank report.")
    
    
