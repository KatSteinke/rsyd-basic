"""Update a result database with data on a run based on a QC result sheet annotated with
pass/fail for each sample"""

__author__ = "Kat Steinke"

import logging
import os
import pathlib
import re
import readline
import sys
import urllib.parse
import warnings

from argparse import ArgumentParser
from datetime import date, datetime, MINYEAR
from typing import Any, Dict, List, Tuple

import numpy as np
import pandas as pd
import yaml

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

import database_tables
import helpers
import pipeline_config
import set_log
import version

from database_tables import Sample, Isolate, SequencingRun, SeqRequest, ExtractRun, Eluate, \
    ReadFile, AssemblyReadFiles, Assembly

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging - TODO: extend?
logger = logging.getLogger("update_database")

# TODO: should this handle translated names?


# TODO: handle the checks as true/false or true/fail?
# load result sheet
# check format: includes verifying that all samples have pass/fail annotations and that those are
# 1 or 0 (no "1/2")
def check_result_sheet(result_sheet: pd.DataFrame) -> bool:
    """Check whether pass/fail annotations for all isolates are present
    and no isolate numbers are duplicated.

    Arguments:
        result_sheet:   the QC results for the run in question

    Returns:
        True if pass/fail annotations are present and good and no isolate numbers are duplicated

    Raises:
        ValueError: if pass/fail is anything else than 1 (pass) or 0 (fail)
                    or isolate numbers are duplicated
    """
    duplicates = sorted(result_sheet.loc[result_sheet.duplicated(subset = "proevenr"),
                                         "proevenr"].tolist())
    if duplicates:
        error_msg = f"Result sheet contains multiple lines with isolate number(s)" \
                    f" {duplicates}. " \
                    "Please remove the duplicated lines so only the one you want to use remains."
        raise ValueError(error_msg)
    no_status = sorted(result_sheet.loc[result_sheet["godkendt"].isna(), "proevenr"].tolist())
    if no_status:
        error_msg = f"Missing approval status for sample(s) {no_status}." \
                    f" Please note approval status as 1 (approved) or 0 (failed)."
        raise ValueError(error_msg)
    wrong_status = sorted(result_sheet.loc[~result_sheet["godkendt"].isin({0, 1}),
                                           "proevenr"].tolist())
    if wrong_status:
        error_msg = f"Incorrect approval status for sample(s) {wrong_status}." \
                    f" Approval status can only be 1 (approved) or 0 (failed)."
        raise ValueError(error_msg)
    return True

# check if samples in FOFN match samples in sheet - do we do this in the general check?


# check the required information is present
def check_files_present(result_dir: pathlib.Path, experiment_name: str) -> bool:
    """Check whether all required files (file of filenames, assemblies for each sample) are present
    in the directory supplied for the experiment name given,
    and check whether the read files in the FOFN exist.

    Arguments:
        result_dir:         a directory containing the pipeline's analysis results
        experiment_name:    the name of the run for which results should be added

    Returns:
        True if all files are present

    Raises:
        FileNotFoundError:  if file of filenames, assemblies, or raw reads are missing
        NotADirectoryError: if the specified path is not a directory
    """
    # if this isn't a dir we'll need to nope out
    if not result_dir.is_dir():
        raise NotADirectoryError("The path you have given is not a folder."
                                 " Please try again and enter the path to the folder with "
                                 "all analysis results.")
    # check for FOFN
    if not (result_dir / f"{experiment_name}.tsv").exists():
        raise FileNotFoundError(f"No file with paths to reads found for run {experiment_name}"
                                f" in folder {result_dir}.")
    reads_with_paths = pd.read_csv((result_dir / f"{experiment_name}.tsv"), sep="\t")
    # TODO: handle empty file?
    # check forward and reverse reads for Illumina or hybrid data
    # TODO: rework to use MissingSampleError!
    missing_read_msg = ""
    missing_r1 = None
    missing_r2 = None
    missing_long = None
    reads_with_ilm = reads_with_paths.loc[reads_with_paths["runtype"].isin({"paired-end",
                                                                            "hybrid"})]
    if not reads_with_ilm.empty:
        missing_r1 = reads_with_ilm.loc[~reads_with_ilm["r1"].apply(lambda reads:
                                                                pathlib.Path(reads).exists()),
                                        "sample"].tolist()
        if missing_r1:
            missing_read_msg += f"\nMissing forward reads for sample(s) {sorted(missing_r1)}."
        missing_r2 = reads_with_ilm.loc[~reads_with_ilm["r2"].apply(lambda reads:
                                                                pathlib.Path(reads).exists()),
                                        "sample"].tolist()
        if missing_r2:
            missing_read_msg += f"\nMissing reverse reads for sample(s) {sorted(missing_r2)}."
    # check extra reads for ONT or hybrid data
    reads_with_ont = reads_with_paths.loc[reads_with_paths["runtype"].isin({"ont",
                                                                            "hybrid"})]
    if not reads_with_ont.empty:
        missing_long = reads_with_ont.loc[~reads_with_ont["extra"].apply(lambda reads:
                                                                     pathlib.Path(reads).exists()),
                                          "sample"].tolist()
        if missing_long:
            missing_read_msg += f"\nMissing long reads for sample(s) {sorted(missing_long)}."

    if missing_read_msg:
        raise helpers.MissingReadfilesError(f"The following issues were found "
                                            f"with the file with paths to reads:"
                                            f"{missing_read_msg}",
                                            missing_r1 = missing_r1,
                                            missing_r2 = missing_r2,
                                            missing_long = missing_long)
    # assemblies for every sample given in FOFN
    missing_assemblies = []
    # check illumina/nanopore/hybrid separately - TODO: this is fairly repetitive, optimize?
    # we want to be fairly flexible here:
    # older assemblies don't have the experiment name in the filename
    missing_illumina = [sample
                        for sample in reads_with_paths.query("runtype "
                                                             "== 'paired-end'")["sample"].tolist()
                        if not list((result_dir
                                     / sample
                                     / "assembly").glob(f"*{sample}.fna"))]

    missing_assemblies.extend(missing_illumina)
    missing_nanopore = [sample
                        for sample in reads_with_paths.query("runtype == 'ont'")["sample"].tolist()
                        if not list((result_dir
                                / sample
                                / "nanopore_assembly").glob(f"*{sample}.fasta"))]

    missing_assemblies.extend(missing_nanopore)

    missing_hybrid = [sample
                      for sample in reads_with_paths.query("runtype "
                                                             "== 'hybrid'")["sample"].tolist()
                      if not (result_dir
                              / sample
                              / "hybrid_assembly"
                              / f"{sample}.fasta").exists()]

    missing_assemblies.extend(missing_hybrid)
    if missing_assemblies:
        raise FileNotFoundError(f"Missing assemblies for sample(s) {missing_assemblies}.")

    # if we haven't crashed here everything is fine
    return True


def get_pipeline_species_call(isolate_number: str, qc_results: pd.DataFrame) -> str:
    """Get the pipeline's species call for a given isolate in a given set of QC results, accounting
    for older result formats (where final_id may not exist or a different tool was used for
    species ID).

    Arguments:
        isolate_number: the isolate number of the isolate for which to find the species call
        qc_results:     the result sheet in which to find the species call

    Returns:
        The species call given in the result sheet for the given isolate.
        In order of preference, species calls are taken from
        1. final ID
        2. GTDB ID
        3. Kraken ID
    """
    # NAN handling everywhere
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(np.nan, None)
    # get results only for the isolate we're interested in
    if isolate_number not in qc_results['proevenr'].tolist():
        err_msg = helpers.PrettyKeyErrorMessage(f"Isolate number {isolate_number} not found"
                                                f" in results.")
        raise KeyError(err_msg)
    results_for_isolate = qc_results.query("proevenr == @isolate_number")
    # convert to dict to allow more graceful handling of missing values
    results_for_isolate = results_for_isolate.to_dict(orient="list")
    # we should have only one entry per isolate
    results_for_isolate = {key: value[0] for key, value in results_for_isolate.items()}
    # get the results of the final ID column if present
    species_id = results_for_isolate.get("final_id", "")
    # if no, check if there is a GTDB column, and return its result if so
    if not species_id:
        # GTDB species assignment can legitimately be blank
        if "WGS_species_GTDB_basics" in results_for_isolate:
            species_id = results_for_isolate['WGS_species_GTDB_basics']
        # if no, return Kraken results
        else:
            species_id = results_for_isolate['WGS_species_kraken']
    return species_id


# TODO get run ID from run number? or use run name as key?
#  MySQL autoincrements int columns -> counterintuitive if we have a mismatch
def get_run_for_db(qc_results: pd.DataFrame, runsheet: pathlib.Path, sequence_dir: pathlib.Path,
                   active_config: Dict[str, Any] = workflow_config) -> SequencingRun:
    """Extract and gather information on run name, platform, sequencing type, data paths,
    date and user for the current sequencing run.

    Arguments:
        qc_results:     the QC results for the run in question
        runsheet:       the run's full runsheet (containing consolidated data from the
                        "master" sheet)
        sequence_dir:   the directory containing sequencing data for the run
        active_config:  the config to use

    Returns:
        The run's name, platform, sequencing type, data paths, date and user gathered for entry
        into an SQL database.
    """
    # NAN handling everywhere
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(np.nan, None)
    # set up known platform to technique mappings - TODO script level?
    platform_to_technique = {'illumina_platforms': {'MiSeq', 'NextSeq'},
                             'nanopore_platforms': {'MinION', 'GridION'}}
    # get platform from QC sheet - but also handle its absence
    if "platform" in qc_results.columns:
        if len(qc_results["platform"].unique()) > 1:
            logger.info('Multiple sequencing platforms found for the run.')
        seq_platform = ";".join(qc_results["platform"].unique())
    else:
        seq_platform = ""
    # infer technique from platform - TODO: optimize/iterate over dict?
    if seq_platform in platform_to_technique['illumina_platforms']:
        technique = 'illumina'
    elif seq_platform in platform_to_technique['nanopore_platforms']:
        technique = 'nanopore'
    else:
        logger.warning(f'No sequencing technique could be inferred for platform {seq_platform}. '
                       f'Valid Illumina platforms are '
                       f'{platform_to_technique["illumina_platforms"]}. '
                       f'Valid Nanopore platforms are '
                       f'{platform_to_technique["nanopore_platforms"]}. '
                       f'Sequencing technique will be inferred from the config file.')
        technique = active_config["sequencing_mode"]
    # now we have a sequencing technique we should have an idea of what to expect
    if technique == "illumina":
        run_name = helpers.extract_illumina_run_name(runsheet)
        # get run user from sheet - TODO is this level of reuse fine
        plate_layout = pd.read_excel(runsheet, sheet_name = "Pladelayout", usecols = "A:B",
                                     skiprows = 2, nrows = 2, index_col=0)
        # reshape plate layout
        plate_layout = plate_layout.transpose().reset_index()
        plate_layout = plate_layout.rename(columns={"index": "Run name"})
        plate_layout.columns.names = [None]
        # fill NA values with None
        plate_layout = plate_layout.replace(pd.NA, None)
        plate_layout = plate_layout.replace(np.nan, None)
        # get experiment name: first and only entry under run name
        run_user = plate_layout.at[0, "Initialer"]
        if run_user:
            run_user = run_user.upper()
    # otherwise it has to be nanopore
    else:
        run_name = helpers.extract_nanopore_run_name(runsheet)
        run_user_found = re.search(r"[-_](?P<run_user>[A-Za-zæøåÆØÅ]{2,3})$", run_name)
        if run_user_found:
            run_user = run_user_found.group("run_user").upper()
        else:
            logger.warning(f"No initials found in run name {run_name}."
                            " Run user will have to be added manually.")
            run_user = "Unknown"
    # parse date out of run name
    run_date_found = re.search(r"Y(\d{8})", run_name)
    if not run_date_found:
        logger.warning(f"Run date could not be inferred for run {run_name}. "
                       f"Date will have to be entered manually.")
        run_date = None  # -> NULL on insert
    else:
        run_date = datetime.strptime(run_date_found.group(1), "%Y%m%d")
        # all we want is a date
        run_date = datetime.date(run_date)

    # assume institution is the one registering
    institution = active_config["project_owner"]
    # flowcell isn't recorded for illumina so skip it for now
    flowcell = None
    seq_run = SequencingRun(seq_run_name = run_name, platform = seq_platform,
                            sequencing_type = technique, flowcell_id = flowcell,
                            runsheet_path = str(runsheet), data_path = str(sequence_dir),
                            run_user_id = run_user, run_date = run_date, institution = institution)
    return seq_run


# TODO: for most of this we need to discuss it with people
# TODO: what do we do for multiple extraction runs in one sheet?
def get_extract_runs(runsheet_sample_info: pd.DataFrame,
                     active_config: Dict[str, Any] = workflow_config) -> List[ExtractRun]:
    """Extract and gather information on extraction run name, date, user, method, method version
    and institution for the extraction run(s) associated with a sequencing run for
    entry into the result database.

    Arguments:
        runsheet_sample_info:   sample information from the "Prøveoplysninger" tab of the full
                                runsheet
        active_config:          the config to use

    Returns:
        Extraction run name, date, user, method, method version and institution
        for each extraction run associated with the database

    Raises:
        ValueError: if a run name cannot be converted to an integer
    """
    # TODO: get date, user, method, method version
    extract_runs = []
    for extract_run_name in runsheet_sample_info['Oprensnings_kørselsnr'].unique():
        try:
            run_id = int(extract_run_name)
        except ValueError as value_err:
            # reraise with nicer message
            raise ValueError(f'Cannot extract run number from extraction run name '
                             f'{extract_run_name}') from value_err
        extract_runs.append(ExtractRun(extract_run_id = run_id,
                                       extract_run_name = extract_run_name,
                                       institution = active_config["project_owner"]))
    return extract_runs


def get_sample_and_isolate(isolate_number: str, qc_results: pd.DataFrame,
                           active_config: Dict[str, Any] = workflow_config) \
        -> Tuple[Sample, Isolate]:
    """Gather sample and isolate information on an isolate.

    Arguments:
        isolate_number: the isolate number to parse
        qc_results:     the QC results for the run in question
        active_config:  the config to use

    Returns:
        LIS sample number, lab section and responsible institution for the sample.
        LIS sample and isolate number, preliminary organism name (if given) and institution
        for the isolate, as well as WGS organism name and serovar (if applicable)
        if the sequence was approved.

    Raises:
        ValueError: if the isolate number doesn't match the format specified in the config
    """
    # NAN handling everywhere
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(np.nan, None)
    # get type translation setup - it'll have to be output as letter
    type_translation = helpers.get_number_letter_combination(active_config[
                                                                 "sample_number_settings"][
                                                                 "number_to_letter"],
                                                             active_config[
                                                                 "sample_number_settings"][
                                                                 "sample_numbers_in"],
                                                             "letter")

    #   get the sample number and sample type, as well as institution
    institution = active_config["project_owner"]
    #   TODO: rework this to only search once if we need more?
    sample_number_hit = re.search(active_config["sample_number_settings"]["format_in_sheet"],
                                  isolate_number)
    if not sample_number_hit:
        raise ValueError(f"Isolate number {isolate_number} does not match "
                         f"the isolate number format in the config.")
    # TODO more sanity checks?
    # sample number is everything but the bacteria number
    sample_number = "".join([group_hit
                             for group, group_hit in sample_number_hit.groupdict().items()
                             if group != "bact_number"])
    sample_type = sample_number_hit.groupdict()["sample_type"]
    # if there is no match, sample type is None and therefore not in the type translation dict
    if sample_type not in type_translation:
        logger.warning(f'Lab section could not be inferred from sample type {sample_type}. '
                       f'Lab section will have to be entered manually.')
        lab_section = None
    # otherwise there must be a hit to begin with
    else:
        lab_section = type_translation[sample_type]
    sample = Sample(mads_proevenr = sample_number, institution = institution,
                    lab_section = lab_section)

    # add isolate data
    # assume species and serovar are None unless the isolate passed QC and has a serovar
    wgs_species = None
    serovar = None
    # we'll always need the MADS species ID (if present)
    results_for_isolate = qc_results.query("proevenr == @isolate_number").squeeze()
    lis_species = results_for_isolate["MADS species"]
    #   check if the result has been approved
    if results_for_isolate["godkendt"]:
        # get species and serovar (won't need this otherwise)
        # there may or may not be a final ID assigned
        wgs_species = get_pipeline_species_call(isolate_number, qc_results)
        # TODO: are we getting too branchy?
        if "Serotypning" in results_for_isolate.index:
            serovar = results_for_isolate["Serotypning"]

    isolate = Isolate(mads_isolatnr = isolate_number, mads_proevenr = sample_number,
                      preliminary_organism_name = lis_species,
                      final_organism_name = wgs_species,
                      wgs_serotyping = serovar, institution = institution)
    return sample, isolate


def get_eluate(isolate_number: str, qc_results: pd.DataFrame) -> Eluate:
    """Extract the extraction run for a given isolate.

    Arguments:
        isolate_number: the isolate number to parse
        qc_results:     the QC results for the run in question

    Returns:
        An Eluate with the isolate number matched with the extraction run.

    Raises:
        ValueError: if the Chemagic run name cannot be converted into a number.
        KeyError:   if the given isolate number is not in the result sheet given.
    """
    # NAN handling everywhere
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(np.nan, None)
    if isolate_number not in qc_results["proevenr"].values:
        error_msg = helpers.PrettyKeyErrorMessage(f"Isolate {isolate_number} "
                                                  f"was not found in results given.")
        raise KeyError(error_msg)
    extract_run_name = qc_results.query("proevenr "
                                        "== @isolate_number")["Oprensnings_kørselsnr"].squeeze()
    try:
        run_id = int(extract_run_name)
    except ValueError as value_err:
        # reraise with nicer message
        raise ValueError(f'Cannot extract run number from extraction run name '
                         f'{extract_run_name}') from value_err
    eluate = Eluate(mads_isolatnr = isolate_number, extract_run_id = run_id)
    return eluate


def get_assembly_readfiles(isolate_number: str, reads_with_paths: pd.DataFrame,
                           seq_run: SequencingRun, eluate: Eluate,
                           active_config: Dict[str, Any] = workflow_config) -> AssemblyReadFiles:
    """Get all reads involved in the assembly for a given isolate in a given sequencing run.

    Arguments:
        isolate_number:     the isolate number of the isolate for which reads should be retrieved
        reads_with_paths:   a mapping of isolate number to paths to reads
                            (forward, reverse and/or long)
        seq_run:            the sequencing run from which the reads come
        eluate:             the eluate which was sequenced in the sequencing run
        active_config:      the config to use

    Returns:
        All reads involved in the assembly for a given isolate, consolidated as AssemblyReadFiles.

    Raises:
        KeyError:   if the isolate number isn't in the FOFN
        ValueError: if the required reads are missing
        TODO: more specific error type?
    """
    # check if the isolate is present in the FOFN
    if isolate_number not in reads_with_paths["sample"].values:
        error_msg = helpers.PrettyKeyErrorMessage(f"Isolate {isolate_number} "
                                                  f"was not found in read overview.")
        raise KeyError(error_msg)
    # initialize all readfiles to blank
    forward_readfile = None
    reverse_readfile = None
    long_readfile = None
    # check if it has any reads
    # get sequence type: paired-end, nanopore or hybrid
    reads_for_isolate = reads_with_paths.query("sample == @isolate_number").squeeze()
    sequence_type = reads_for_isolate["runtype"]
    # if it's paired-end or hybrid it needs to have Illumina reads
    if sequence_type in {"paired-end", "hybrid"}:
        forward_read = reads_for_isolate["r1"]
        reverse_read = reads_for_isolate["r2"]
        missing_reads = ""
        if not forward_read:
            missing_reads += f"No forward reads given for isolate {isolate_number}. "
        if not reverse_read:
            missing_reads += f"No reverse reads given for isolate {isolate_number}. "
        if missing_reads:
            raise ValueError(missing_reads)
        # add forward reads if given - we only have Illumina paired-end reads,
        # so it's safe to infer forward/reverse reads are Illumina
        forward_readfile = ReadFile(read_direction = "forward", read_file = forward_read,
                                    read_type = "Illumina",
                                    mads_isolatnummer = isolate_number,
                                    institution = active_config["project_owner"])
        forward_readfile.eluate = eluate
        forward_readfile.sequencing_run = seq_run
        # add reverse reads if given
        reverse_readfile = ReadFile(read_direction = "reverse", read_file = reverse_read,
                                    read_type = "Illumina",
                                    mads_isolatnummer = isolate_number,
                                    institution = active_config["project_owner"])
        reverse_readfile.eluate = eluate
        reverse_readfile.sequencing_run = seq_run
    # if it's ONT or hybrid it needs to have long reads - our long reads will always be Nanopore
    if sequence_type in {"hybrid", "ont"}:
        nanopore_read = reads_for_isolate["extra"]
        if not nanopore_read:
            raise ValueError(f"No long reads given for isolate {isolate_number}. ")
        # add long reads if given
        long_readfile = ReadFile(read_file = nanopore_read, read_type = "Nanopore",
                                 mads_isolatnummer = isolate_number,
                                 institution = active_config["project_owner"])
        long_readfile.eluate = eluate
        long_readfile.sequencing_run = seq_run
    # add all reads to AssemblyReadFiles
    isolate_assembly_reads = AssemblyReadFiles()
    isolate_assembly_reads.forward_read = forward_readfile
    isolate_assembly_reads.reverse_read = reverse_readfile
    isolate_assembly_reads.long_read = long_readfile
    return isolate_assembly_reads


def get_sequencing_request_for_isolate(isolate: Isolate,
                                       runsheet_sample_data: pd.DataFrame,
                                       active_config: Dict[str, Any] = workflow_config) \
        -> SeqRequest:
    """Extract information on indication, Gram status, who ordered and who registered the isolate.

    Arguments:
        isolate:                the isolate for which request data should be extracted
        runsheet_sample_data:   sample information from the full Excel runsheet
        active_config:          the config to use

    Returns:
        Indication, Gram status, who ordered and who registered the isolate.
    Raises:
        KeyError:   if the isolate number isn't in the sample sheet
    """
    isolate_number = isolate.mads_isolatnr
    if isolate_number not in runsheet_sample_data["Prøvenr"].values:
        error_msg = helpers.PrettyKeyErrorMessage(f"Isolate {isolate_number} "
                                                  f"not found in runsheet.")
        raise KeyError(error_msg)
    # only get the columns we want in the request
    columns_to_use = ["Prøvenr", "Bestilt af", "Resistens",
                      "Identifikation", "Relaps", "Toxingen_ID", "HAI",
                      "Miljøprøveisolat", "Udbrudsisolat", "Overvågningsisolat", "Forskning",
                      "Andet", "Registrering_udført_dato", "Registreret_af", "Kommentar",
                      "Gram_Positiv", "Gram_negativ", "ID2"]
    data_for_isolate = runsheet_sample_data.query("Prøvenr == @isolate_number") # type: pd.DataFrame
    data_for_isolate = data_for_isolate.reindex(columns = columns_to_use)
    # reindexing fills missing cols with NaN - do all the NaN handling here
    data_for_isolate = data_for_isolate.replace(pd.NA, None)
    data_for_isolate = data_for_isolate.replace(pd.NaT, None)
    data_for_isolate = data_for_isolate.replace(np.nan, None)
    # tweak the easy ones: everything to lowercase
    data_for_isolate = data_for_isolate.rename(str.lower, axis = "columns")
    # rename the things that need to be renamed manually - shortened names, translations
    data_for_isolate = data_for_isolate.rename({"prøvenr": "mads_isolatnr",
                                                "bestilt af": "requested_by",
                                                "registrering_udført_dato": "request_date",
                                                "registreret_af": "registered_by",
                                                "kommentar": "request_comment",
                                                "toxingen_id": "toxingen_identifikation",
                                                "miljøprøveisolat": "miljoeproeve",
                                                "udbrudsisolat": "udbrud",
                                                "overvågningsisolat": "overvaagning",
                                                "id2": "secondary_id"}, axis="columns")
    # ensure consistent initial formatting
    data_for_isolate['requested_by'] = data_for_isolate['requested_by'].str.upper()
    data_for_isolate['registered_by'] = data_for_isolate['registered_by'].str.upper()
    data_for_isolate["institution"] = active_config["project_owner"]
    # manually add the parent sample's LIS number
    data_for_isolate["mads_proevenr"] = isolate.mads_proevenr
    data_for_isolate['request_date'] = pd.to_datetime(data_for_isolate['request_date'])
    data_for_request = data_for_isolate.to_dict(orient = "list")
    # this gives us a list of values for each column but we should only have the one
    data_for_request = {col: value[0] for col, value in data_for_request.items()}
    data_for_request['request_date'] = pd.to_datetime(data_for_request['request_date'])
    # coerce date to DateTime here - pandas can't
    # if not isinstance(data_for_request["request_date"], datetime):
    #     # we should have a date or a string on our hands here
    #     if isinstance(data_for_request["request_date"], str):
    #         data_for_request["request_date"] = datetime.fromisoformat(data_for_request[
    #                                                                       "request_date"])
    #     elif isinstance(data_for_request['request_date'], date):
    #         request_date = data_for_request['request_date']  # type: date
    #         data_for_request['request_date'] = datetime(request_date.year,
    #                                                     request_date.month,
    #                                                     request_date.day)
    #     else:
    #         raise TypeError(f"Cannot convert {data_for_request['request_date']} to datetime.")
    sequencing_request = SeqRequest(**data_for_request)
    return sequencing_request


def get_assembly_for_isolate(isolate: Isolate, qc_results: pd.DataFrame, result_dir: pathlib.Path,
                             readfiles: AssemblyReadFiles, seq_request: SeqRequest,
                             active_config: Dict[str, Any] = workflow_config) -> Assembly:
    """Get path to assembly and read files, sequencing request, analysis results,
    QC parameters and QC check result for an isolate.

    Arguments:
        isolate:            the isolate whose assembly should be added
        qc_results:         the QC results for the run in question
        result_dir:         the directory containing sequencing results for the sample
        readfiles:          the read files used for the assembly
        seq_request:        the sequencing request for the isolate
        active_config:      the config to use

    Returns:
        Path to assembly and read files, sequencing request, analysis results, QC parameters
        and QC check result for an isolate.

    Raises:
        KeyError:   if the isolate isn't in the QC result sheet
        ValueError: if no reads are given for the isolate - TODO more specific?
    """
    # NAN handling everywhere
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(pd.NaT, None)
    qc_results = qc_results.replace(np.nan, None)
    # assembler defaults
    illumina_assembler = "shovill/skesa"
    nanopore_assembler = "flye+medaka"
    hybrid_assembler = "unicycler"
    isolate_number = isolate.mads_isolatnr
    sample_number = isolate.mads_proevenr
    if isolate_number not in qc_results["proevenr"].values:
        error_msg = helpers.PrettyKeyErrorMessage(f"Isolate {isolate_number} "
                                                  f"not found in QC result sheet.")
        raise KeyError(error_msg)
    # TODO: check matching isolate numbers?
    # TODO: assemblies?
    # TODO: nicer way of getting at experiment name? Can we follow the reads back to the run?
    # TODO more data integrity checks?
    if readfiles.forward_read:
        # TODO: what is the experiment name in a hybrid setup?
        # if we also have nanopore reads we have a hybrid assembly
        if readfiles.long_read:
            assembly_type = "hybrid"
            assembly_path = list((result_dir / isolate_number
                                  / "hybrid_assembly").glob(f"{isolate_number}.fasta"))[0]
            assembler = hybrid_assembler
        else:
            assembly_type = "illumina"
            assembly_path = list((result_dir / isolate_number /
                                  "assembly").glob(f"*{isolate_number}.fna"))[0]
            assembler = illumina_assembler
    else:
        if readfiles.long_read:
            assembly_type = "nanopore"
            assembly_path = list((result_dir / isolate_number
                                  / "nanopore_assembly").glob(f"*{isolate_number}.fasta"))[0]
            assembler = nanopore_assembler
        else:
            raise ValueError(f"No reads recorded for isolate {isolate_number}.")
    assembly_dir = assembly_path.parent
    # get QC results - TODO: simplify this?
    results_for_isolate = qc_results.query('proevenr == @isolate_number').squeeze()
    n50 = results_for_isolate['N50_basics']
    num_contigs = results_for_isolate['# contigs']
    genome_size = results_for_isolate['Total length_basics']
    coverage_depth = results_for_isolate['Avg. coverage depth_basics']
    # there may or may not be a final ID assigned
    pipeline_organism_name = get_pipeline_species_call(isolate_number, qc_results)

    release = results_for_isolate['godkendt']
    if "pipeline_version" in results_for_isolate.index:
        pipeline_version = results_for_isolate['pipeline_version']
    else:
        pipeline_version = None
        logger.warning("No pipeline version found. "
                       "Pipeline version will have to be entered manually.")
    # TODO: how to get analysis / release users?
    institution = active_config["project_owner"]
    db_timestamp = datetime.now()
    assembly = Assembly(mads_proevenr = sample_number, mads_isolatnr = isolate_number,
                        assembly_type = assembly_type, assembler = assembler,
                        pipeline_version = pipeline_version, assembly_file = str(assembly_path),
                        assembly_dir = str(assembly_dir), n50 = n50, num_contigs = num_contigs,
                        genome_size = genome_size, coverage = coverage_depth,
                        release = bool(release), timestamp = db_timestamp,
                        institution = institution, pipeline_organism_name = pipeline_organism_name,
                        assembly_readfiles = readfiles, seq_request = seq_request)
    return assembly


def find_newer_assemblies(current_isolate: Isolate, current_run: SequencingRun,
                          db_session: Session) -> bool:
    """Check if there are assemblies for the current isolate that are newer than the current run.

    Arguments:
        current_isolate:    the Isolate to check
        current_run:        the SequencingRun the isolate comes from
        db_session:         the database session in which to check

    Returns:
        True if there are newer assemblies, False otherwise.
    """
    assemblies_for_isolate = db_session.query(Assembly).filter_by(mads_isolatnr = current_isolate.mads_isolatnr).all()
    # check if the reads come from a sequencing run with a newer date
    for found_assembly in assemblies_for_isolate:
        # if there are both short and long reads take the newer
        if (found_assembly.assembly_readfiles.forward_read
                and found_assembly.assembly_readfiles.long_read):
            assembly_date = max(
                found_assembly.assembly_readfiles.forward_read.sequencing_run.run_date,
                found_assembly.assembly_readfiles.long_read.sequencing_run.run_date
                )
        # else check which we have
        else:
            if found_assembly.assembly_readfiles.forward_read:
                assembly_date = found_assembly.assembly_readfiles.forward_read.sequencing_run.run_date
            elif found_assembly.assembly_readfiles.long_read:
                assembly_date = found_assembly.assembly_readfiles.long_read.sequencing_run.run_date
            else:
                logger.warning(f"No run date found for assembly {found_assembly.assembly_file}")
                assembly_date = date(year = MINYEAR, month=1, day=1)
        if assembly_date > current_run.run_date:
            logger.info(f"There is a result for isolate {current_isolate.mads_isolatnr}"
                        " from a newer sequencing run in the database."
                        " Final ID will not be updated.")
            return True
    return False


def read_result_sheet(results: pathlib.Path) -> pd.DataFrame:
    """Read the result sheet into a dataframe, removing the outer MultiIndex layer
    (experiment name "header").

    Arguments:
        results:    the Excel file containing QC results for the run in question

    Returns:
        The results without the outer layer of MultiIndex.
    """
    results_read = pd.read_excel(results,
                                 skiprows = [0, 2],  # multiindex header and blank line
                                 index_col = 0,
                                 dtype = {"godkendt": bool,
                                          "køres om": str,
                                          "noter": str,
                                          "pipeline_noter": str,
                                          "final_id": str,
                                          "proevenr": str,
                                          "MADS: kendt længde i bp": "Float64",
                                          "GTDB: kendt længde i bp": "Float64",
                                          "GC (%)": float,
                                          "Oprensnings_kørselsnr": str,
                                          "SEK_Run_nr": str,
                                          })
    # TODO: handle sanity checks here?
    return results_read


def load_run_into_db(qc_results: pd.DataFrame, run_dir: pathlib.Path, runsheet: pathlib.Path,
                     db_session: Session, active_config: Dict[str, Any] = workflow_config) -> None:
    """Add results for a run into the result database.

    Arguments:
        qc_results:         the QC results for the run in question
        run_dir:            the directory containing analysis results for the run
        runsheet:           the run's full runsheet (containing consolidated data from the
                            "master" sheet)
        db_session:         the Session connecting to the result database that should be updated
        active_config:      the config to use


    """
    # drop any rows missing sample numbers - assume they are comments
    qc_results = qc_results.dropna(subset = "proevenr")
    check_result_sheet(qc_results)
    # fill NA values with None before we do anything for MySQL compatibility
    qc_results = qc_results.replace(pd.NA, None)
    qc_results = qc_results.replace(pd.NaT, None)
    qc_results = qc_results.replace(np.nan, None)
    # get experiment name from runsheet - depends on the sequencing type
    # also note where to look for paths to reads here
    # for paired-end, take the path that's common for all R1; for Nanopore, ditto for extra
    if active_config["sequencing_mode"] == "illumina":
        experiment_name = helpers.extract_illumina_run_name(runsheet)
        reads_for_dirs = "r1"
    else:
        experiment_name = helpers.extract_nanopore_run_name(runsheet)
        reads_for_dirs = "extra"
    # sanity check if we have everything before we do anything else
    check_files_present(run_dir, experiment_name)
    # find sequence directory:
    # check FOFN now we know it ought to be there
    file_of_filenames = run_dir / f"{experiment_name}.tsv"
    paths_to_reads = pd.read_csv(file_of_filenames, sep="\t")
    read_dirs = paths_to_reads[reads_for_dirs].apply(lambda read_path:
                                                     pathlib.Path(read_path).parent).unique()
    # TODO: how should we behave for a run with multiple dirs?
    sequence_dir = read_dirs[0]
    # for long read, take the path that's common for all dirs
    # TODO: discuss behavior for mixed runs?
    # read the sample information (needed for extraction run, eluate, request)
    sample_information_from_sheet = pd.read_excel(runsheet, sheet_name = "Prøveoplysninger",
                                                  dtype = {"Oprensnings_kørselsnr": str,
                                                           "SEK_Run_nr": str})
    # translate sample numbers if needed
    from_format = re.compile(active_config["sample_number_settings"]["format_in_sheet"])
    to_format = re.compile(active_config["sample_number_settings"]["format_output"])
    translate_from = active_config["sample_number_settings"]["sample_numbers_in"]
    translate_to = active_config["sample_number_settings"]["sample_numbers_report"]
    mapping = active_config["sample_number_settings"]["number_to_letter"]
    sample_information_from_sheet["Prøvenr"] = sample_information_from_sheet["Prøvenr"].apply(
        lambda sample_name: helpers.rearrange_sample_number(sample_name,
                                                            from_format,
                                                            to_format.groupindex)
    )
    # translate start if needed
    sample_information_from_sheet["Prøvenr"] = sample_information_from_sheet["Prøvenr"].apply(lambda sample_nr:
                                                                  helpers.translate_sample_category(
                                                                      sample_nr,
                                                                      to_format,
                                                                      translate_from,
                                                                      translate_to,
                                                                      mapping))
    # load extraction run(s)
    # technically (for old runs) we can have multiple extraction runs in one sequencing run
    extract_runs = get_extract_runs(sample_information_from_sheet, active_config)
    for extract_run in extract_runs:
        extract_run_exists = db_session.query(ExtractRun).filter_by(
            extract_run_id = extract_run.extract_run_id).one_or_none()
        if not extract_run_exists:
            db_session.add(extract_run)
    # set up run-level tables
    sequencing_run = get_run_for_db(qc_results, runsheet, sequence_dir, active_config)
    db_session.add(sequencing_run)
    # set up isolate-level tables - TODO: handle all of these in one?
    samples = []
    isolates = []
    assemblies = []
    # for each result in the result sheet:
    with db_session.no_autoflush:
        for isolate in qc_results["proevenr"]:
            sample_data, isolate_data = get_sample_and_isolate(isolate, qc_results, active_config)
            sample_number = sample_data.mads_proevenr
        #   check if the sample number already is present in the database
            sample_in_table = db_session.query(Sample).filter_by(
                mads_proevenr=sample_number).one_or_none()
        #   if it isn't, add it
            if not sample_in_table:
                current_sample_numbers = [current_sample.mads_proevenr
                                          for current_sample in samples]
                if sample_data.mads_proevenr not in current_sample_numbers:
                    samples.append(sample_data)
            #   check if the isolate already is present in the database
            isolate_in_table = db_session.query(Isolate).filter_by(mads_isolatnr=isolate).one_or_none()
            #   if it isn't, add isolate number, sample ID, WGS species ID and serovar
            if not isolate_in_table:
                isolates.append(isolate_data)
            #   if it is, update final id and serotype if the results are approved -
            #   TODO: can we avoid the double check?
            else:
                if qc_results.query("proevenr == @isolate")["godkendt"].squeeze():
                    # check if there already is something approved with a newer date
                    # for each assembly with this isolate number
                    if not find_newer_assemblies(isolate_data, sequencing_run, db_session):
                        isolate_in_table.final_organism_name = isolate_data.final_organism_name
                        isolate_in_table.wgs_serotyping = isolate_data.wgs_serotyping
            # get eluate for sample
            isolate_eluate = get_eluate(isolate, qc_results)
            # we'll need to add  the eluate data etc. to the relevant columns -
            # see https://stackoverflow.com/a/50069525/15704972?
            isolate_assembly_readfiles = get_assembly_readfiles(isolate, paths_to_reads,
                                                                sequencing_run,
                                                                isolate_eluate,
                                                                active_config = active_config)
            isolate_request = get_sequencing_request_for_isolate(isolate_data,
                                                                 sample_information_from_sheet,
                                                                 active_config = active_config)
            isolate_assembly = get_assembly_for_isolate(isolate_data, qc_results, run_dir,
                                                        isolate_assembly_readfiles, isolate_request,
                                                        active_config = active_config)
            assemblies.append(isolate_assembly)
    db_session.add_all(samples)
    db_session.add_all(isolates)
    db_session.add_all(assemblies)


if __name__ == "__main__":
    arg_parser = ArgumentParser(description = "Add the results for a run to the result database.")
    arg_parser.add_argument("--result_sheet",
                            help = "Path to file with "
                                   "QC results (with pass/fail information) "
                                   "for the run you want to add")
    arg_parser.add_argument("--runsheet",
                            help = "Path to sample sheet (currently only supports Illumina)")
    arg_parser.add_argument("--resultdir",
                            help = "Path to the directory containing the results you want to add"
                                   " (default: the directory containing analysis results)",
                            default = None)
    arg_parser.add_argument("--workflow_config_file", help = "Config file for run "
                                                             "(overrides default config given in "
                                                             "script, can be overridden by "
                                                             "commandline options)")
    args = arg_parser.parse_args()

    # initialize results and run dir to blanks
    results_sheet = None
    rundir = None
    # manual mode if no args given
    if len(sys.argv) == 1:
        # lots of typing, so  allow tab completion of paths
        readline.set_completer_delims('\t\n=')
        readline.parse_and_bind("tab: complete")
        # ...and greet the user nicely
        print("### Upload results to database")  # TODO: log instead?
        results_sheet = pathlib.Path(input("Please input path to QC result file."
                                           " Note that QC pass/fail (in column 'godkendt')"
                                           " must be filled out. ").strip().strip("'"))
        rundir = results_sheet.parent
        rundir_accept = input(f"Is {rundir} the folder containing all results for the run? [y/n]")
        # TODO: is there a nicer control flow here?
        if rundir_accept == "n":
            rundir = pathlib.Path(input("Type full path of folder to take results and files from"
                                        " and press enter: ").strip().strip("'"))
        elif rundir_accept == "y":
            pass
        else:
            raise ValueError("Result folder not entered. Aborting")
        run_sheet = pathlib.Path(input("Enter path to Illumina runsheet: ").strip().strip("'")).resolve()
    # otherwise we need the result sheet but not necessarily the directory
    else:
        if args.workflow_config_file:
            default_config_file = pathlib.Path(args.workflow_config_file).resolve()
            with open(default_config_file, "r", encoding = "utf-8") as config_file:
                workflow_config = yaml.safe_load(config_file)
        if not args.result_sheet:
            raise ValueError("No result sheet given.")
        if not args.runsheet:
            raise ValueError("No runsheet given.")
        results_sheet = pathlib.Path(args.result_sheet)
        run_sheet = pathlib.Path(args.runsheet)
        # if the directory isn't given, infer
        if args.resultdir:
            rundir = pathlib.Path(args.resultdir)
        else:
            rundir = results_sheet.parent
    # now we're sure to have the paths we can sanity check and upload
    result_data = pd.read_excel(results_sheet, skiprows = 1).dropna(how="all")
    # check if we can connect - TODO: separate function?
    # in debug mode, we're never adding to the real DB
    if workflow_config["debug"]:
        db_string = "sqlite+pysqlite://"
        echo_database_actions = True
        logger = set_log.get_stream_log(level = "DEBUG")
    else:
        logger = set_log.get_stream_log(level="WARNING")
        logger.setLevel(logging.INFO)
        sql_dialect = workflow_config['result_database']['sql_dialect']
        database_path = workflow_config['result_database']['database_path']
        db_user = urllib.parse.quote_plus(os.getenv("SAMPLE_DB_USER", ""))
        db_pass = urllib.parse.quote_plus(os.getenv("SAMPLE_DB_PASS", ""))
        user_and_pass = f"{db_user}:{db_pass}@" if (db_user and db_pass) else ""
        db_string = f"{sql_dialect}://{user_and_pass}{database_path}"
        echo_database_actions = False
    database_engine = create_engine(db_string,
                                    future = True, echo = echo_database_actions)

    with Session(database_engine) as session:
        if workflow_config["debug"]:
            database_tables.Base.metadata.create_all(database_engine)
        load_run_into_db(result_data, rundir, run_sheet, session, active_config = workflow_config)
        if not workflow_config["debug"]:
            session.commit()
