"""Fetch and save LIS report directly from the underlying database or an existing file."""

__author__ = "Kat Steinke"

import logging
import os
import pathlib
import re
import urllib.parse

from datetime import datetime
from typing import Any, Dict, List, Optional

import pandas as pd
import sqlalchemy

import helpers
import input_names
import localization_helpers
import pipeline_config

from helpers import PrettyKeyErrorMessage
from input_names import LISDataNames

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# set sqlalchemy's logging
sql_logger = logging.getLogger("sqlalchemy.engine")
sql_logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
sql_logger.addHandler(console_log)
# set own logging
own_logger = logging.getLogger("get_lis")


sheet_names, lis_names, bact_names = localization_helpers.load_input_from_config(workflow_config)


# TODO: make filtering optional?
# TODO: there has to be a less ugly way
def get_lis_data_from_db(engine: sqlalchemy.engine.Engine, table_name: str, cols_to_get: List[str],
                         filter_col: Optional[str], filter_value: Optional[str],
                         db_schema: Optional[str]) -> pd.DataFrame:
    """Get LIS data from a database.

    Arguments:
        engine:         the engine for connecting to the database
        table_name:     the name of the table to get data from
        cols_to_get:    the columns to get from the database
        filter_col:     the column marking which samples should be sequenced
        filter_value:   the value for samples that should be sequenced
        db_schema:      the schema to use for the database


    Returns:
        The specified columns from the database.

    Raises:
        KeyError:   if one or more of the specified columns are missing in the database;
                    if the table is missing;
                    if the column to filter by isn't in the columns to fetch from the database.
        ValueError: if a filter value has been given but no column to filter was supplied
    """
    # we'll only be referencing the filter value in a query so pylint will complain
    # pylint: disable=unused-argument
    if filter_col:
        # check if the filter column is in the columns to fetch
        if filter_col not in cols_to_get:
            raise KeyError("The column to filter results by has to be one of the columns"
                           " retrieved from the database.")
    else:
        if filter_value:
            raise ValueError("A filter value was given but no column to filter by was specified.")
    # config may have specified the schema as an empty string - make sure it's usable (None)
    if not db_schema:
        db_schema = None
    with engine.connect() as conn:
        try:
            result_data = pd.read_sql_table(table_name, conn, columns = cols_to_get,
                                            schema = db_schema)
        except KeyError:
            # the actual error from the DB only returns one missing column, we want all
            cols_found = set(pd.read_sql_table(table_name, conn, schema = db_schema).columns)
            # just in case, let's see if this is a case issue
            case_swapped = sorted([colname.swapcase() for colname in cols_to_get])
            missing_cols = sorted(list(set(cols_to_get) - cols_found))

            own_logger.info(f"Original column(s) {missing_cols}"
                            f" not found. Checking for {case_swapped}")
            missing_case_swapped = sorted(list(set(case_swapped) - cols_found))
            if missing_case_swapped:
                error_msg = PrettyKeyErrorMessage(f"Column(s) {missing_case_swapped} "
                                                  f"not found in table {table_name}")
                raise KeyError(error_msg)
            # if all case-swapped columns are found, that means we should be safe to load them
            result_data = pd.read_sql_table(table_name, conn, columns = case_swapped,
                                            schema = db_schema)
            # ...and do our best to restore them
            result_data = result_data.rename(mapper = lambda colname: colname.swapcase(),
                                             axis = "columns")
        # read_sql_table gives a value error instead of the key error we want to raise
        except ValueError as value_err:
            if re.search(f"Table {table_name} not found", str(value_err), re.IGNORECASE):
                raise KeyError(f"Table {table_name} not found in database")

    # TODO: convert later
    # now we're no longer in the database, filter if needed
    if filter_col:
        result_data = result_data.query(f"{filter_col} == @filter_value")
    return result_data[cols_to_get]


def read_lis_data(lis_report: pathlib.Path, lis_data_names: LISDataNames) -> pd.DataFrame:
    """Read LIS report, ensuring that numeric bacteria code gets read as a string.

    Arguments:
        lis_report:     the path to the LIS report
        lis_data_names: name mappings for the relevant columns in the LIS report

    Returns:
        The LIS report for further processing in the pipeline.
    """
    lis_data = pd.read_csv(lis_report, sep = ";", encoding = "latin-1",
                           dtype = {lis_data_names.bacteria_code: str,
                                    lis_data_names.sample_number: str,
                                    lis_data_names.isolate_number: str,
                                    lis_data_names.sample_type: str})
    return lis_data


def translate_lis_data(lis_data: pd.DataFrame,
                       mapping: Dict[str, str] = workflow_config[
                           "sample_number_settings"]["number_to_letter"],
                       translate_from: str = workflow_config["sample_number_settings"][
                           "sample_numbers_out"],
                       translate_to: str = workflow_config["sample_number_settings"][
                           "sample_numbers_in"],
                       from_format: re.Pattern = re.compile(workflow_config[
                                                                "sample_number_settings"][
                                                                "format_in_lis"]),
                       to_format: re.Pattern = re.compile(workflow_config[
                                                              "sample_number_settings"][
                                                              "format_in_sheet"
                                                          ]),
                       lis_data_names: input_names.LISDataNames = lis_names) -> pd.DataFrame:
    """

    Arguments:
        lis_data:       the LIS report for which to translate sample numbers
        mapping:        a mapping of sample type numbers to letters
        translate_from: the sample type format (letter or number) used in the laboratory
                        information system
        translate_to:   the sample type format (letter or number) used in the runsheet
        from_format:    the sample number format used in the LIS, with the components denoted by
                        match groups
        to_format:      the sample number format used in the runsheet, with the components denoted
                        by match groups
        lis_data_names: Column names in LIS data

    Raises:
        ValueError: if one or more samples don't match the specified format
    Returns:
        The LIS report with an additional column giving the isolate number in a format matching the
        one in the runsheet.
    """
    report_with_number = lis_data.astype({lis_data_names.sample_type: str,
                                          lis_data_names.sample_number: str,
                                          lis_data_names.isolate_number: str})
    # TODO: how do we move around blocks of sample information most clearly? rearrange by pattern?
    report_with_number["proevenr_unordered"] = report_with_number[lis_data_names.sample_type] \
                                               + report_with_number[lis_data_names.sample_number] \
                                               + "-" \
                                               + report_with_number[lis_data_names.isolate_number]
    component_index_out = dict(to_format.groupindex)
    # TODO: error handling - can this be done better?
    wrong_format = report_with_number[
        ~report_with_number["proevenr_unordered"].str.contains(from_format)]
    numbers_wrong_format = wrong_format["proevenr_unordered"]  # type: pd.Series
    if numbers_wrong_format.any():
        wrong_number_error = f"Sample numbers {sorted(numbers_wrong_format.tolist())} " \
                             f"in LIS report don't match the specified sample number format."
        raise ValueError(wrong_number_error)
    # reorder and translate the sample category
    report_with_number["proevenr"] = report_with_number[
        "proevenr_unordered"].apply(lambda sample_nr:
                                    helpers.rearrange_sample_number(sample_nr,
                                                                    from_format,
                                                                    component_index_out)).apply(
        lambda sample_nr:
        helpers.translate_sample_category(
            sample_nr,
            to_format,
            translate_from,
            translate_to,
            mapping))
    return report_with_number


def get_report_with_isolate_number(lis_report: pathlib.Path,
                                   mapping: Dict[str, str] = workflow_config[
                                       "sample_number_settings"]["number_to_letter"],
                                   translate_from: str = workflow_config["sample_number_settings"][
                                       "sample_numbers_out"],
                                   translate_to: str = workflow_config["sample_number_settings"][
                                       "sample_numbers_in"],
                                   from_format: re.Pattern = re.compile(workflow_config[
                                                                            "sample_number_settings"][
                                                                            "format_in_lis"]),
                                   to_format: re.Pattern = re.compile(workflow_config[
                                                                          "sample_number_settings"][
                                                                          "format_in_sheet"
                                                                      ]),
                                   lis_data_names: input_names.LISDataNames = lis_names) \
        -> pd.DataFrame:
    """Read a LIS report and construct the isolate number in a format matching the one in the
    runsheet (translate "back" from LIS format to runsheet format).

    Arguments:
        lis_report:     the path to the LIS report to translate
        mapping:        a mapping of sample type numbers to letters
        translate_from: the sample type format (letter or number) used in the laboratory
                        information system
        translate_to:   the sample type format (letter or number) used in the runsheet
        from_format:    the sample number format used in the LIS, with the components denoted by
                        match groups
        to_format:      the sample number format used in the runsheet, with the components denoted
                        by match groups
        lis_data_names: Column names in LIS data

    Returns:
        The LIS report with an additional column giving the isolate number in a format matching the
        one in the runsheet.
    """
    lis_data = read_lis_data(lis_report, lis_data_names)
    report_with_number = translate_lis_data(lis_data, mapping, translate_from, translate_to,
                                            from_format, to_format, lis_data_names)
    return report_with_number


# get LIS data from DB if specified, file otherwise
# TODO: at what step do we save LIS data? We only want to do so when necessary, but that might mean
#  having to have a function with a return and a side effect (so we avoid having to *load*
#  unnecessarily as well)

def setup_lis_data_from_config(active_config: Dict[str, Any] = workflow_config) -> pd.DataFrame:
    """Get LIS data from the specifications in the workflow configuration.

    If a database has been specified, connect to the database using the given dialect and retrieve
    columns given in the specified input configuration, then save these to the path specified in the
    config.
    Username and password must be given as envvars.

    If no database has been specified, retrieve data from the file specified in the config.

    Arguments:
        active_config:  the config to use for connecting to the database

    Returns:
        A LIS report as specified from the workflow configuration - either fetched from a database
        or read from the file given if no database was specified.

    Raises:
        ValueError: if database has been specified but dialect hasn't or vice versa
    """
    # load column names
    (runsheet_names,
     lis_data_names,
     bacteria_names) = localization_helpers.load_input_from_config(active_config)

    # we'll be referring to the LIS features a lot, less so to the rest
    lis_features = active_config["lab_info_system"]
    database = lis_features["database"]
    dialect = lis_features["dialect"]
    if database and dialect:
        db_user = urllib.parse.quote_plus(os.getenv("LIS_DB_USER", ""))
        db_pass = urllib.parse.quote_plus(os.getenv("LIS_DB_PASS", ""))
        user_and_pass = f"{db_user}:{db_pass}@" if (db_user and db_pass) else ""
        db_string = f"{dialect}://{user_and_pass}{database}"
        # TODO: how to test this properly?
        if dialect == "oracle+oracledb":
            engine = sqlalchemy.create_engine(db_string, thick_mode = True)
        else:
            engine = sqlalchemy.create_engine(db_string)
        # the filter column could be one of the ones we already have, or an external one
        columns_to_get = list(lis_data_names)
        if lis_features["filter_column"] not in columns_to_get:
            columns_to_get.append(lis_features["filter_column"])
        own_logger.info(f"Fetching LIS report from {dialect} database {database}"
                        f" at {datetime.now()} with user {db_user}")
        lis_data = get_lis_data_from_db(engine, lis_features["db_table"], columns_to_get,
                                        lis_features["filter_column"], lis_features["filter_value"],
                                        lis_features["schema"])
        # make sure we've got the same dtypes as when we're reading it in
        lis_data = lis_data.astype({lis_data_names.sample_type: str,
                                    lis_data_names.sample_number: str,
                                    lis_data_names.isolate_number: str,
                                    lis_data_names.bacteria_code: str})
        # ...and translate it as we do for files
        lis_data = translate_lis_data(lis_data,
                                      mapping = active_config[
                                          "sample_number_settings"]["number_to_letter"],
                                      translate_from = active_config["sample_number_settings"][
                                          "sample_numbers_out"],
                                      translate_to = active_config["sample_number_settings"][
                                          "sample_numbers_in"],
                                      from_format = re.compile(active_config[
                                                                   "sample_number_settings"][
                                                                   "format_in_lis"]),
                                      to_format = re.compile(active_config[
                                                                 "sample_number_settings"][
                                                                 "format_in_sheet"
                                                             ]),
                                      lis_data_names = lis_data_names)
        lis_data.to_csv(lis_features["lis_report"], index=False, sep=";", encoding = "latin-1")
    else:
        if database:  # dialect can't have been specified or we'd be in the other branch
            raise ValueError("SQL database has been specified but no dialect for the connection "
                             "was given.")
        if dialect:  # database can't be specified
            raise ValueError("SQL dialect has been specified but no database to connect to"
                             " was given.")
        # if we haven't failed yet, there's only a file
        lis_data = get_report_with_isolate_number(lis_features["lis_report"],
                                                  mapping = active_config[
                                                      "sample_number_settings"]["number_to_letter"],
                                                  translate_from =
                                                  active_config["sample_number_settings"][
                                                      "sample_numbers_out"],
                                                  translate_to =
                                                  active_config["sample_number_settings"][
                                                      "sample_numbers_in"],
                                                  from_format = re.compile(active_config[
                                                                               "sample_number_settings"][
                                                                               "format_in_lis"]),
                                                  to_format = re.compile(active_config[
                                                                             "sample_number_settings"][
                                                                             "format_in_sheet"
                                                                         ]),
                                                  lis_data_names = lis_data_names)
    return lis_data
