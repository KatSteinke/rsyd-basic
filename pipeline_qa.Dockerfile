FROM registry.gitlab.com/regionsyd/container-management/namespaces/ouh-kma/autoqc-base:latest

RUN mamba install -c conda-forge -c defaults sqlalchemy=2.0.30

WORKDIR /bactopia_datasets
COPY config/input_settings/ /bactopia_datasets/config/input_settings/

WORKDIR /snake_qc

COPY . /snake_qc